//
//  FAQQuestion.swift
//  Myles
//
//  Created by Itteam2 on 09/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD

class FAQQuestion: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    var faqModal = FAQModel(responce: [:])
    var arrFaqCount = [AnyObject]()
    var arrFaqQuestion : Array<FAQSubModelQuestion> = []
    var faqQuestionAns = FAQSubModelQuestion()
    
    var isSelected = Bool()
    
    var strQuestionSection = String()
    @IBOutlet weak var tblFAQQUS: UITableView!

    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationBar("FAQ", present: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        
//        self.customizeLeftRightNavigationBar("", rightTItle: "FAQ", controller: self, present: true)
        
        //self.customizeNavigationBarwithImage("FAQ", leftImage: nil, rightImage: nil, caller: nil, controller: self, present: true)

        
        showLoader()
        //Call Faq api
        let objOperationalApi = APIOperation()
        objOperationalApi.getFaqData  { [weak weakSelf = self] (coordinate)  in
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            //pass data on model calss
            weakSelf!.faqModal = FAQModel(responce: coordinate?["response"] as! Dictionary)
//            print(weakSelf!.faqModal.dictFaqSection)
            
            weakSelf!.arrFaqCount  =  (weakSelf!.faqModal.dictFaqSection[weakSelf!.strQuestionSection] as? Array<AnyObject>)!
            
            for var dict in weakSelf!.arrFaqCount
            {
                weakSelf!.arrFaqQuestion.append(FAQSubModelQuestion(responce: dict as! Dictionary<String, String>))
            }
            weakSelf!.tblFAQQUS.reloadData()
            
            // Stop loader
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
    return 0.1;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50;
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
//        return 1;
               return arrFaqCount.count ;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       

        return arrFaqQuestion[section].isOpen ? 1 : 0
//        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let vwHeader = UIView(frame: CGRect.zero)
        let btnHeader = UIButton(frame: CGRect(x: 0, y: 0, width: self.tblFAQQUS.frame.size.width, height: 50))
        btnHeader.layer.borderWidth = 0.5;
        btnHeader.layer.borderColor = UIColor(red: 217.0 / 255.0, green: 217.0 / 255.0, blue: 217.0 / 255.0, alpha: 1.0).cgColor
        btnHeader.tag = section
        btnHeader.backgroundColor = UIColor.white
        btnHeader.titleLabel!.font = UIFont(name: "Helvetica", size: 15)
//        btnHeader.setTitleColor(UIColor(red: 91.0 / 255.0, green: 91.0 / 255.0, blue: 91.0 / 255.0, alpha: 1), forState: .Normal)
//
//        let faqQuse = FAQSubModelQuestion(responce: arrFaqCount[section] as! Dictionary<String, String>)
         faqQuestionAns = arrFaqQuestion[section] 
        if faqQuestionAns.isOpen {
            btnHeader.setImage(UIImage(named: "less_icn")!, for: UIControl.State())
        }
        else {
            btnHeader.setImage(UIImage(named: "more_less_icn")!, for: UIControl.State())
        }


        
        //btnHeader.addTarget(self, action: #selector(tapOnQuestion), forControlEvents: .TouchUpInside)
        btnHeader.addTarget(self, action: #selector(FAQQuestion.tapOnQuestion(_:)), for: .touchUpInside)

        
        btnHeader.setTitleColor(UIColor.black, for: UIControl.State())
        btnHeader.contentHorizontalAlignment = .left
//       btnHeader.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        btnHeader.setTitle(faqQuestionAns.FaqSubSectionQes, for: UIControl.State())
      btnHeader.titleLabel?.numberOfLines = 3
        btnHeader.titleLabel?.sizeToFit()
        
//        btnHeader.titleLabel?.textAlignment = NSTextAlignment.Justified

//       btnHeader.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        vwHeader.addSubview(btnHeader)
        return vwHeader
    }
    
    @objc func tapOnQuestion(_ sender: UIButton!) {
        
       arrFaqQuestion[sender.tag].isOpen = !arrFaqQuestion[sender.tag].isOpen
//        self.tblFAQQUS.reloadData()
        let sections = IndexSet(integer: sender.tag)
      
      
        tblFAQQUS.reloadSections(sections, with: .automatic)
        

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
//        if cell == nil {
//            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "cellFAQ")
//        }
        let faqQuse = FAQSubModelQuestion(responce: arrFaqCount[indexPath.section] as! Dictionary<String, String>)
//        print(faqQuse.FaqSubSectionAns)
       

    
        let identifier = "cellFAQ"
        var cell: FAQQuestionCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? FAQQuestionCell
        if cell == nil {
            tableView.register(UINib(nibName: "FAQQuestionCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FAQQuestionCell
        }
//        let sDecode = faqQuse.FaqSubSectionAns.stringByRemovingPercentEncoding
       // cell.webView.loadRequest(NSURLRequest(URL: NSURL(string: "about:blank")!))
        cell.webView.loadHTMLString(faqQuse.FaqSubSectionAns.uriDecodedString()!, baseURL: nil)
        cell.webView.tag = indexPath.section
        cell.webView.scrollView.isScrollEnabled = false;
//        self.webViewDidFinishLoad(cell.webView) { (result) in
//            if result
//            {
//                self.arrFaqQuestion[indexPath.section].Cellheight = Double(cell.webView.scrollView.contentSize.height)+5
//
//                cell.webView.frame.size.height  = CGFloat(self.arrFaqQuestion[indexPath.section].Cellheight)
//                cell.webView.layoutIfNeeded()
//
//            }
//        }
        

        return cell
    }
//    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool
//    {
//        return true
//
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {

        if arrFaqQuestion[indexPath.section].isOpen {
//            let cell = tblFAQQUS.cellForRowAtIndexPath(indexPath) as! FAQQuestionCell
           
            return CGFloat(arrFaqQuestion[indexPath.section].Cellheight)
        }
        else
        {
            return 0.1
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        self.tblFAQQUS.beginUpdates()
        self.arrFaqQuestion[webView.tag].Cellheight = Double(webView.scrollView.contentSize.height)
        tblFAQQUS.endUpdates()
        if let heightString = webView.stringByEvaluatingJavaScript(from: "document.height"),
            let widthString = webView.stringByEvaluatingJavaScript(from: "document.width"),
            let height = Float(heightString),
            let width = Float(widthString) {
            
            var rect = webView.frame
            rect.size.height = CGFloat(height)
            rect.size.width = CGFloat(width)
            webView.frame = rect
            webView.scrollView.isScrollEnabled = false
            
            let indexPath:IndexPath = IndexPath(row: 0, section: webView.tag)
            self.tblFAQQUS.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)

        }

    }
    func webViewDidFinishLoad(_ webView: UIWebView, completion: (_ result: Bool) -> Void) {
        let textSize: Int = 30
        
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(textSize)%%'")
        completion(true)
    }

}


extension String  {
    
    func uriDecodedString() -> String? {
        return self.replacingOccurrences(of: "+", with: " ").removingPercentEncoding
    }
    
}

