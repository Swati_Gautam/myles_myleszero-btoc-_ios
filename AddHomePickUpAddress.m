//
//  AddHomePickUpAddress.m
//  Myles
//
//  Created by Itteam2 on 30/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "AddHomePickUpAddress.h"
#import "AddressCustomCell.h"
#import "CommonFunctions.h"
#import "PickUpAddress.h"
#import "PickUpAddressList.h"
#import "Myles-Swift.h"

@interface AddHomePickUpAddress ()

@end

@implementation AddHomePickUpAddress
{
    NSMutableArray *placeHolder;
    NSMutableDictionary *dictAddress,*dictLatLng;
    bool bCurrentLoc;
}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    //[self customizeLeftRightNavigationBar:@"Pick Up/Drop Off Address" RightTItle:nil Controller:self Present:false];

    self.leftbackButton.hidden = false;
    self.leftmenuButton.hidden = true;
    [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    
    placeHolder =[[NSMutableArray alloc]initWithObjects:@"City*",@"Locality*",@"House/Flat Number*",@"Street*",@"Landmark", nil];
    dictAddress = [[NSMutableDictionary alloc]init];
    if (_dictEditData != nil) {
        [dictAddress addEntriesFromDictionary:_dictEditData];
    }
    
   _btnSave.layer.cornerRadius = _btnSearchCurrentLoc.layer.cornerRadius =_btnSearchOnMap.layer.cornerRadius = 5;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark- Right Button
- (void)rightButton : (UIButton *)sender
{
    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeHolder.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"pickUpAddressCell";
    AddressCustomCell *customCell;
    
    customCell = (AddressCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    // If there is no cell to reuse, create a new one
    if(customCell == nil)
    {
        customCell = [[AddressCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    customCell.addressTF.inputAccessoryView = toolbar;
    
    if ([customCell.addressHeadingLabel.text respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor darkGrayColor];
        customCell.addressTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customCell.addressTF.text attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    [CommonFunctions setLeftPaddingToTextField:customCell.addressTF andPadding:10];
    customCell.tag = indexPath.row;
    customCell.addressTF.tag = indexPath.row;
    customCell.addressHeadingLabel.text = placeHolder[indexPath.row];

    if (placeHolder.count == indexPath.row+1) {
        customCell.addressTF.returnKeyType = UIReturnKeyDone;
    }
    else
        customCell.addressTF.returnKeyType = UIReturnKeyNext;

//    if (_dictEditData != nil && !bCurrentLoc) {
//         customCell.addressTF.text = indexPath.row == 0 ?  : indexPath.row == 1 ? _dictEditData[@"Locality"] : indexPath.row == 2 ? _dictEditData[@"Housenumber"] : indexPath.row == 3 ? _dictEditData[@"Street"] : _dictEditData[@"Landmark"];
//    }
//    else if (bCurrentLoc)
//    {
    
    
        customCell.addressTF.text = indexPath.row == 0 ? _locationDataObj.doorStepDeliveryObj.city : indexPath.row == 1 ? _locationDataObj.doorStepDeliveryObj.locality : indexPath.row == 2 ? _locationDataObj.doorStepDeliveryObj.completeAddress : indexPath.row == 3 ? _locationDataObj.doorStepDeliveryObj.street : _locationDataObj.doorStepDeliveryObj.landmark;
    
    
    //}
   
    return customCell;

}
#define ACCEPTABLE_CHARECTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\,/-"

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    
    return YES;
}

-(void)dismiss
{
//    [self.tblAddAddress setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [self.tblAddAddress endEditing:YES];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setTintColor:[UIColor blackColor]];
    [textField becomeFirstResponder ];

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    switch (textField.tag) {
        case 0:
            return NO;
            break;
        case 1:
            return NO;
            
        default:
            return YES;
            break;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceCharacterSet];
    switch (textField.tag) {
        case 0:
            if ([textField.text stringByTrimmingCharactersInSet:whitespace].length > 0)
                _locationDataObj.doorStepDeliveryObj.city = textField.text;
            break;
        case 1:
            if ([textField.text stringByTrimmingCharactersInSet:whitespace].length > 0)
                _locationDataObj.doorStepDeliveryObj.locality = textField.text;

            break;
        case 2:
            if ([textField.text stringByTrimmingCharactersInSet:whitespace].length > 0)
                _locationDataObj.doorStepDeliveryObj.houseFlatNumber = textField.text;
            break;
        case 3:
            if ([textField.text stringByTrimmingCharactersInSet:whitespace].length > 0)
                _locationDataObj.doorStepDeliveryObj.street = textField.text;
            break;
        case 4:
            if ([textField.text stringByTrimmingCharactersInSet:whitespace].length > 0)
                _locationDataObj.doorStepDeliveryObj.landmark = textField.text;
            break;
            
        default:
            break;
    }
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
   
    return YES;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.returnKeyType==UIReturnKeyNext) {
//        UITextField *next = [[textField superview] viewWithTag:textField.tag+1];
//        [next becomeFirstResponder];
        
       AddressCustomCell *addcell = [[[[[textField superview] superview] superview] superview] viewWithTag:textField.tag+1];
        [addcell.addressTF becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
//    [textField resignFirstResponder];
//    return YES;
}
#pragma mark Keyboard notifications
- (void)keyboardWillShow:(NSNotification *)sender
{
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [_tblAddAddress setContentInset:edgeInsets];
        [_tblAddAddress setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender
{
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        // Add padding
        _tblAddAddress.contentInset = UIEdgeInsetsMake(0, 0,  0 , 0);
        _tblAddAddress.scrollIndicatorInsets = _tblAddAddress.contentInset;
    }];
}


/**
  save address.
 */
- (IBAction)saveBtnTouched:(UIButton *)sender
{
    
    
    if ([CommonFunctions reachabiltyCheck]) {
        [dictAddress setValue:_locationDataObj.doorStepDeliveryObj.city forKey:@"City"];
        [dictAddress setValue:_locationDataObj.doorStepDeliveryObj.locality forKey:@"Locality"];
        [dictAddress setValue:_locationDataObj.doorStepDeliveryObj.houseFlatNumber forKey:@"Housenumber"];
        [dictAddress setValue:_locationDataObj.doorStepDeliveryObj.street forKey:@"Street"];
        [dictAddress setValue:_locationDataObj.doorStepDeliveryObj.landmark forKey:@"Landmark"];

    if (dictAddress[@"City"] != nil && [dictAddress[@"City"] length] > 0 && dictAddress[@"Locality"] != nil && [dictAddress[@"Locality"] length] > 0 && dictAddress[@"Housenumber"] != nil && [dictAddress[@"Housenumber"] length] > 0 && dictAddress[@"Street"] != nil && [dictAddress[@"Street"] length] > 0) {
        
        [dictAddress setValue:[DEFAULTS objectForKey:KUSER_PHONENUMBER] forKey:@"PhoneNumber"];
        [dictAddress setValue:[NSString stringWithFormat:@"%lf",_locationDataObj.location.coordinate.latitude] forKey:@"Latitude"];
        [dictAddress setValue:[NSString stringWithFormat:@"%lf",_locationDataObj.location.coordinate.longitude] forKey:@"Longitude"];
       
        if (_dictEditData != nil)
            [dictAddress setValue:_dictEditData[@"AddressId"] forKey:@"AddressId"];
        else
            [dictAddress setValue:@"1" forKey:@"AddressId"];
        
        if (dictAddress[@"Landmark"] == nil) {
            [dictAddress setValue:@"" forKey:@"Landmark"];
        }

        [self showLoader];

        [CommonFunctions SavePickUpAddress:dictAddress PassUrl:insertAddress :^(NSDictionary *DictSaveAddress) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            NSLog(@"DictSaveAddress %@",DictSaveAddress);
            dispatch_async(dispatch_get_main_queue(), ^{
                [DEFAULTS setObject:dictAddress forKey:k_PickupAddress];
                [self.navigationController popViewControllerAnimated:TRUE];

            });
        }];
    }
    else
    {
        if (dictAddress.count == 0) {
            [self showALert:@"Please fill all mandatory fields" withTitle:@"MYLES"];
//            [CommonFunctions alertTitle:nil withMessage:@"Please fill all mandatory fields"];
        }
        else
        {
            NSString *strErrorMsg = dictAddress[@"City"] == nil && [dictAddress[@"City"] length] == 0 ? placeHolder[0]:dictAddress[@"Locality"] == nil && [dictAddress[@"Locality"] length] == 0 ? placeHolder[1] :dictAddress[@"Housenumber"] == nil && [dictAddress[@"Housenumber"] length] == 0 ? placeHolder[2]:placeHolder[3];
            [self showALert:[NSString stringWithFormat:@"Please fill %@",strErrorMsg] withTitle:@"MYLES"];
        }
        
    }
    }
    else
    {
        [self showALert:KAInternet withTitle:@"MYLES"];

        //[CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}

- (UIViewController *)backViewController
{
    NSInteger myIndex = [self.navigationController.viewControllers indexOfObject:self];
    
    if ( myIndex != 0 && myIndex != NSNotFound )
    {
        return [self.navigationController.viewControllers objectAtIndex:myIndex-1];
    }
    else
    {
        return nil;
    }
}
/**
 call search address page through google api.
 */
-(IBAction)SearchLoctoGoogleApi:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    PickUpAddress *vcPickUpAddress = [storyboard instantiateViewControllerWithIdentifier:@"PickUpAddress"];
    vcPickUpAddress.delegate = self;
    [self.navigationController pushViewController:vcPickUpAddress animated:YES];
    
}
/**
     click button to get current address.
     */
-(IBAction)btnCurrentAddress:(id)sender
{
    if ([CommonFunctions reachabiltyCheck]) {
        [self showLoader];
    [CommonFunctions GetCurrentLocation:^(NSDictionary *dictData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        NSLog(@"Current Location %@",dictData);
        //        dsd
        if (dictLatLng != nil) {
            dictLatLng = [[NSMutableDictionary alloc]init];
            
        }
        else
            [dictLatLng removeAllObjects];
        
        [dictAddress removeAllObjects];
        
        [dictLatLng addEntriesFromDictionary:dictData[@"latlang"] != nil ? dictData[@"latlang"]:nil];
        
        [dictAddress setValue:dictData[@"City"] != nil ? dictData[@"City"]:nil forKey:@"City"];
        [dictAddress setValue:dictData[@"SubLocality"] forKey:@"Locality"];
        [dictAddress setValue:dictData[@"Name"] != nil ? dictData[@"Name"] :nil forKey:@"Housenumber"];
        [dictAddress setValue:dictData[@"Street"] != nil ? dictData[@"Street"] : dictData[@"SubLocality"] != nil ? dictData[@"SubLocality"] : nil forKey:@"Street"];
        [dictAddress setValue:dictData[@"Thoroughfare"] != nil ? dictData[@"Thoroughfare"] : nil forKey:@"Landmark"];
//        _dictEditData= nil;
        bCurrentLoc = true;
        [_tblAddAddress reloadData];

       
        
    }];
    }
    else
    {
        [self showALert:KAInternet withTitle:@"MYLES"];
//        [CommonFunctions alertTitle:nil withMessage:KAInternet];
  
    }
}

/**
     Set Address through google api-(Protocol)
     - parameter get Address on 'dictData'.
     */
-(void)setAddressThroughApi :(NSDictionary*)dictData
{
    dictLatLng = [[NSMutableDictionary alloc]init];
    [dictLatLng addEntriesFromDictionary:dictData[@"latlang"] != nil ? dictData[@"latlang"]:nil];
    
    [dictAddress setValue:dictData[@"City"] != nil ? dictData[@"City"]:nil forKey:@"City"];
    [dictAddress setValue:dictData[@"SubLocality"] forKey:@"Locality"];
    [dictAddress setValue:dictData[@"Name"] != nil ? dictData[@"Name"] :nil forKey:@"Housenumber"];
    [dictAddress setValue:dictData[@"Street"] != nil ? dictData[@"Street"] : dictData[@"SubLocality"] != nil ? dictData[@"SubLocality"] : nil forKey:@"Street"];
    [dictAddress setValue:dictData[@"Thoroughfare"] != nil ? dictData[@"Thoroughfare"] : nil forKey:@"Landmark"];
//    _dictEditData= nil;
    bCurrentLoc = true;
    [_tblAddAddress reloadData];
}

-(void)showALert :(NSString *)strMessage withTitle:(NSString *)titleStr
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleStr message:strMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    });
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
