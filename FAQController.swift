//
//  FAQController.swift
//  Myles
//
//  Created by Itteam2 on 08/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import UIKit

class FAQController: UIViewController,UITableViewDelegate,UITableViewDataSource,LoginProtocol {

   @IBOutlet weak var tblFAQ: UITableView!
    
      override func viewDidLoad()
    {
        super.viewDidLoad()
        // Start loader
        
        //self.customizeLeftRightNavigationBar("FAQ", rightTItle: nil, controller: self, present: true)
        
        // open login screen through menu

        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveTestNotification), name: NSNotification.Name(rawValue: "openLogin"), object: nil)
        
        
    }
    
    @objc func receiveTestNotification(notification: NSNotification) {
        var userInfo = notification.userInfo!
        let index = userInfo["Selectedindex"] as! String
        
        
        if (index == "8") {
            let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
            
            let editController = (storyBoard.instantiateViewController(withIdentifier: "LoginProcess") as! LoginProcess)
          
let navController = UINavigationController(rootViewController: editController)
            if userInfo["otherdestination"] != nil
            {
                editController.delegate = self;
                editController.strOtherDestination = userInfo["otherdestination"] as! String!;
            }
            
            navController.viewControllers = [editController]
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    
    func tableView(_ tableView : UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "CELL")
          }
        cell!.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator;
        cell!.tintColor = UIColor.red
//[cell setTintColor:[UIColor whiteColor]];
        switch indexPath.row {
        case 0:
            cell!.textLabel!.text = "Eligibilty & Document"
            break
        case 1:
            cell!.textLabel!.text = "Product Information"
            break
        case 2:
            cell!.textLabel!.text = "Fee Policy"
            break
        case 3:
            cell!.textLabel!.text = "Rules & Regulations"
            break
        case 4:
            cell!.textLabel!.text = "Help & Support"
            break
            
        default: break
            
        }
//              cell!.textLabel!.text = faqModal.dictFaqSection.allKeys[indexPath.row] as? String
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let vc = UIStoryboard(name:"FAQ", bundle:nil).instantiateViewController(withIdentifier: "FAQQUS") as? FAQQuestion else {
            return
        }
        var strSectionType = ""
        
        switch indexPath.row {
        case 0:
            strSectionType = "eligibiltyDocuments"
            break
        case 1:
            strSectionType = "productInformation"
            break
        case 2:
            strSectionType = "feePolicy"
            break
        case 3:
            strSectionType = "rulesRegulations"
            break
        case 4:
            strSectionType = "helpSupport"
            break
            
        default: break
            
        }
       
        vc.strQuestionSection = strSectionType
        self.navigationController?.pushViewController(vc, animated:true)
    }
    //MARKS
    
    func afterLoginChangeDestination(strDestination: String) {
        //[revealController pushFrontViewController:navController animated:YES];
        let revealController = self.revealViewController()
        let storyBoard = UIStoryboard(name: (CInt(strDestination) == 1) ? k_MyRideStoryBoard : k_profileStoryBoard, bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: (Int(strDestination) == 1) ? k_RidesNavigation : k_profileNavigation) as! UINavigationController
        (revealController?.rearViewController as! MenuViewController).selectedIndex = Int(strDestination)!
        if CInt(strDestination) == 2 {
            let homeController = storyBoard.instantiateViewController(withIdentifier: k_profileController)
            navController.viewControllers = [homeController]
        }
        else if CInt(strDestination) == 1{
            let homeController = storyBoard.instantiateViewController(withIdentifier: "rideHistory")
            navController.viewControllers = [homeController]
        }
        revealController?.pushFrontViewController(navController, animated: true)
    }
}
