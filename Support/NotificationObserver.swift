//
//  NotificationObserver.swift
//  Myles
//
//  Created by iOS Team on 18/04/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation

class NotificationObserver<T: AnyObject> : NSObject {
    private weak var target: T?
    private let action : (T) -> (NSNotification) -> Void
    
    /// Create a new NotificationObserver with the specified notification name, target and action. The
    /// method action on target will be called with a NSNotification object anytime a notification
    /// is fired.
    ///
    /// The target is be weakly referenced and therefore instances of this class can safely be
    /// stored in instance variables, and do not need to be nilled out.
    ///
    /// - parameter name The name of the notification to listen for.
    /// - parameter target The object to call the method on whenever the notification is posted.
    /// - parameter action The method to call.
    init(name: NSNotification.Name, target: T, action: @escaping (T) -> (NSNotification) -> Void) {
        self.target = target
        self.action = action
        
        super.init()
        
        let sel = #selector(self.notificationFired(notification:))
        NotificationCenter.default.addObserver(self, selector: sel, name: name, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func notificationFired(notification: NSNotification) {
        guard let target = target else {
            // May as well deregister from notifications if our target has gone away.
            NotificationCenter.default.removeObserver(self)
            return
        }
        
        action(target)(notification)
    }

}


import CoreGraphics

extension CGRect {
    var center: CGPoint {
        get {
            return CGPoint(x: origin.x+size.width/2, y: origin.y+size.height/2)
        }
    }
}

