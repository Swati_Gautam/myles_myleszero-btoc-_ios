//
//  EMAccordionTableViewController.m
//  UChat
//
//  Created by Ennio Masi on 10/01/14.
//  Copyright (c) 2014 Hippocrates Sintech. All rights reserved.
//

#import "EMAccordionTableViewController.h"
#import "EMAccordionTableParallaxHeaderView.h"

#import <QuartzCore/QuartzCore.h>

#define kSectionTag 1110
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface EMAccordionTableViewController () {
    UITableViewStyle emTableStyle;
    
    NSMutableArray *sections;
    
    
    NSObject <EMAccordionTableDelegate> *emDelegate;
    
    NSUInteger openedSection;
    EMAnimationType animationType;
    
    NSInteger showedCell;
}

@end

@implementation EMAccordionTableViewController

@synthesize closedSectionIcon = _closedSectionIcon;
@synthesize openedSectionIcon = _openedSectionIcon;
@synthesize parallaxHeaderView = _parallaxHeaderView;
@synthesize tableView = _tableView;
@synthesize sectionsHeaders = _sectionsHeaders;
@synthesize defaultOpenedSection = _defaultOpenedSection;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    showedCell = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// Exposed Methods
- (void) setEmTableView:(UITableView *)tv {
    self.view = [[UIView alloc] initWithFrame:tv.frame];
    
    _tableView = tv;
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    
    [self.view addSubview:_tableView];
}

- (id) initWithTable:(UITableView *)tableView withAnimationType:(EMAnimationType) type {
    if (self = [super init]) {
        self.view = [[UIView alloc] initWithFrame:tableView.frame];
        
        _tableView = tableView;
        [_tableView setDataSource:self];
        [_tableView setDelegate:self];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        animationType = type;
        //animationType = EMAnimationTypeNone;
        sections = [[NSMutableArray alloc] initWithCapacity:0];
        self.sectionsOpened = [[NSMutableArray alloc] initWithCapacity:0];
        openedSection = -1;
        
        self.sectionsHeaders = [[NSMutableArray alloc] initWithCapacity:0];
    }
    
    return self;
}

- (void) setDelegate: (NSObject <EMAccordionTableDelegate> *) delegate {
    emDelegate = delegate;
}

- (void) addAccordionSection: (EMAccordionSection *) section initiallyOpened:(BOOL)opened {
    [sections addObject:section];
    
    NSInteger index = sections.count - 1;
    
    [self.sectionsOpened addObject:[NSNumber numberWithBool:opened]];
    if (index == self.defaultOpenedSection) {
        [self.sectionsOpened setObject:[NSNumber numberWithBool:YES] atIndexedSubscript:index];
    }
    
}

- (void) setParallaxHeaderView:(EMAccordionTableParallaxHeaderView *)parallaxHeaderView {
    _parallaxHeaderView = parallaxHeaderView;
    
    if (_tableView) {
        [_tableView setTableHeaderView:_parallaxHeaderView];
    }
}

#pragma mark UITableViewDataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return sections.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    EMAccordionSection *emSection = [sections objectAtIndex:section];
    
    BOOL value = [[self.sectionsOpened objectAtIndex:section] boolValue];
    
    if (value)
        return emSection.items.count;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([emDelegate respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)])
        return [emDelegate tableView:tableView cellForRowAtIndexPath:indexPath];
    else
        [NSException raise:@"The delegate doesn't respond tableView:cellForRowAtIndexPath:" format:@"The delegate doesn't respond tableView:cellForRowAtIndexPath:"];
    
    return NULL;
}


- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == openedSection && animationType != EMAnimationTypeNone) {
        CGPoint offsetPositioning = CGPointMake(cell.frame.size.width / 2.0f, 20.0f);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, 0.0);
        
        UIView *card = (UITableViewCell * )cell ;
        card.layer.transform = transform;
        
        card.layer.opacity = 0.5;
        
     //   [UIView animateWithDuration:0.5f delay:0.0f usingSpringWithDamping:0.2f initialSpringVelocity:0.2f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            card.layer.transform = CATransform3DIdentity;
            card.layer.opacity = 1;
      //  } completion:NULL];
    }
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([emDelegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        return [emDelegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    else
        [NSException raise:@"The delegate doesn't respond tableView:didSelectRowAtIndexPath:" format:@"The delegate doesn't respond tableView:didSelectRowAtIndexPath:"];
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return tableView.sectionHeaderHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    NSLog(@"num : %ld",(long)tableView.numberOfSections);
//    if (section == tableView.numberOfSections-1)
//    {
//        return 0;
//    }
//    else
//        return 0;
//}



- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    EMAccordionSection *emAccordionSection = [sections objectAtIndex:section];
    
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.frame.size.width, tableView.sectionHeaderHeight)];
    [sectionView setBackgroundColor:emAccordionSection.backgroundColor];
    
    UIButton *sectionBtn = [[UIButton alloc] initWithFrame:CGRectMake(sectionView.frame.size.width - 40.0f, (sectionView.frame.size.height / 2) - 15.0f, 60.0f, 40.0f)];
    [sectionBtn addTarget:self action:@selector(openTheSection:) forControlEvents:UIControlEventTouchDown];
    
    sectionBtn.frame = sectionView.frame;
    [sectionBtn setBackgroundColor:[UIColor clearColor]];
    
    [sectionBtn setTag:(kSectionTag + section)];
    [sectionView addSubview:sectionBtn];
    
    
    
    UILabel *cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(13.0f, (sectionView.frame.size.height / 2) - 7, self.tableView.frame.size.width - 50.0f, 14)];
    [cellTitle setText:emAccordionSection.title];
    [cellTitle setTextColor:emAccordionSection.titleColor];
    [cellTitle setFont:[UIFont fontWithName:@"OpenSans-Bold" size:12.5]];
    [cellTitle setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:cellTitle];
    
    UIImageView *accessoryIV = [[UIImageView alloc] initWithFrame:CGRectMake(sectionView.frame.size.width - 30.0f, (sectionView.frame.size.height / 2) - 6.50f, 13.67f, 13.33f)];
    BOOL value = [[self.sectionsOpened objectAtIndex:section] boolValue];
    [accessoryIV setContentMode:UIViewContentModeScaleAspectFit];
    [accessoryIV setBackgroundColor:[UIColor clearColor]];
    if (value)
        [accessoryIV setImage:self.openedSectionIcon];
    else
        [accessoryIV setImage:self.closedSectionIcon];
    
    [sectionView addSubview:accessoryIV];
    
    if (self.sectionsHeaders.count <= 2)
    {
        [self.sectionsHeaders insertObject:sectionView atIndex:section];
    }
    
    if ([emDelegate respondsToSelector:@selector(tableView:viewForHeaderInSection:)])
        return [emDelegate tableView:tableView viewForHeaderInSection:section];
    
    return sectionView;
}

//- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    if (section == tableView.numberOfSections-1)
//    {
//
//    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.frame.size.width, tableView.sectionFooterHeight)];
//    [sectionView setBackgroundColor:[UIColor greenColor]];
//
//    UIColor *textColor  = [UIColor colorWithRed:128.0f/255.0f green:130.0f/255.0f blue:133.0f/255.0f alpha:1.0f];
//
//    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 10, 175, 30)];
//    [addressLabel setText:@"Sukhna Lake, Myles Center Gurgaon Haryana"];
//    addressLabel.font = [UIFont fontWithName:@"Motor Oil 1937 M54" size:13.33];
//    addressLabel.textColor = textColor;
//    addressLabel.numberOfLines = 2;
//    addressLabel.textAlignment = NSTextAlignmentLeft;
//    addressLabel.backgroundColor = [UIColor whiteColor];
//    [sectionView addSubview:addressLabel];
//
//    return sectionView;
//    }
//    return nil;
//}
//
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([emDelegate respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)])
        return [emDelegate tableView:tableView heightForRowAtIndexPath:indexPath];
    else
        [NSException raise:@"The delegate doesn't respond ew:heightForRowAtIndexP:" format:@"The delegate doesn't respond ew:heightForRowAtIndexP:"];
    
    return 0.0;
}


- (IBAction)openTheSection:(id)sender
{
    
    
    int index = (int)[sender tag] - kSectionTag;
    
    BOOL value = [[self.sectionsOpened objectAtIndex:index] boolValue];
    NSNumber *updatedValue = [NSNumber numberWithBool:!value];
    
    [self.sectionsOpened setObject:updatedValue atIndexedSubscript:index];
    
    openedSection = index;
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    if (!value)
        [self showCellsWithAnimation];
    
    
    [emDelegate latestSectionOpened];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    //    [self.parallaxHeaderView updateLayout:scrollView];
}

- (void) showCellsWithAnimation {
    NSArray *cells = self.tableView.visibleCells;
    
    if (showedCell >= cells.count)
        return;
    //    for (UIView *card in cells) {
    /*
    UIView *card = [cells objectAtIndex:showedCell];
    
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -200.0;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, DEGREES_TO_RADIANS(90), 1.0f, 0.0f, 0.0f);
    card.layer.transform = rotationAndPerspectiveTransform;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, DEGREES_TO_RADIANS(-90), 1.0f, 0.0f, 0.0f);
    [UIView animateWithDuration:.4 animations:^{
        card.alpha = 1.0f;
        card.layer.transform = rotationAndPerspectiveTransform;
    } completion:^(BOOL finished){
        showedCell++;
        [self showCellsWithAnimation];
    }];
     */
    
    
    
    //    }
}

@end
