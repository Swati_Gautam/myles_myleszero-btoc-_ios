//
//  SearchCarResponse.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 11, 2019

import Foundation


class SearchCarResponse : NSObject, NSCoding{

    var cars : [SearchCarList]!
    var fitlers : SearchCarFilter!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let fitlersData = dictionary["Fitlers"] as? [String:Any]{
            fitlers = SearchCarFilter(fromDictionary: fitlersData)
        }
        cars = [SearchCarList]()
        if let carsArray = dictionary["Cars"] as? [[String:Any]]{
            for dic in carsArray{
                let value = SearchCarList(fromDictionary: dic)
                cars.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if fitlers != nil{
            dictionary["fitlers"] = fitlers.toDictionary()
        }
        if cars != nil{
            var dictionaryElements = [[String:Any]]()
            for carsElement in cars {
                dictionaryElements.append(carsElement.toDictionary())
            }
            dictionary["cars"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cars = aDecoder.decodeObject(forKey: "Cars") as? [SearchCarList]
        fitlers = aDecoder.decodeObject(forKey: "Fitlers") as? SearchCarFilter
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cars != nil{
            aCoder.encode(cars, forKey: "Cars")
        }
        if fitlers != nil{
            aCoder.encode(fitlers, forKey: "Fitlers")
        }
    }
}
