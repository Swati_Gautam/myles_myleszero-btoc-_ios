//
//  SearchCarFilter.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 11, 2019

import Foundation


class SearchCarFilter : NSObject, NSCoding{

    var carcategory : [String]!
    var cartype : [String]!
    var fuelname : [String]!
    var seatingCap : [String]!
    var transmissiontype : [String]!
    var variant : [SearchCarVariant]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        variant = [SearchCarVariant]()
        if let variantArray = dictionary["Variant"] as? [[String:Any]]{
            for dic in variantArray{
                let value = SearchCarVariant(fromDictionary: dic)
                variant.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if variant != nil{
            var dictionaryElements = [[String:Any]]()
            for variantElement in variant {
                dictionaryElements.append(variantElement.toDictionary())
            }
            dictionary["variant"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        carcategory = aDecoder.decodeObject(forKey: "carcategory") as? [String]
        cartype = aDecoder.decodeObject(forKey: "cartype") as? [String]
        fuelname = aDecoder.decodeObject(forKey: "fuelname") as? [String]
        seatingCap = aDecoder.decodeObject(forKey: "SeatingCap") as? [String]
        transmissiontype = aDecoder.decodeObject(forKey: "Transmissiontype") as? [String]
        variant = aDecoder.decodeObject(forKey: "Variant") as? [SearchCarVariant]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if carcategory != nil{
            aCoder.encode(carcategory, forKey: "carcategory")
        }
        if cartype != nil{
            aCoder.encode(cartype, forKey: "cartype")
        }
        if fuelname != nil{
            aCoder.encode(fuelname, forKey: "fuelname")
        }
        if seatingCap != nil{
            aCoder.encode(seatingCap, forKey: "SeatingCap")
        }
        if transmissiontype != nil{
            aCoder.encode(transmissiontype, forKey: "Transmissiontype")
        }
        if variant != nil{
            aCoder.encode(variant, forKey: "Variant")
        }
    }
}
