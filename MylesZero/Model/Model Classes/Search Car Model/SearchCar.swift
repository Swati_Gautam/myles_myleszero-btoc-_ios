//
//  SearchCar.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 11, 2019

import Foundation


class SearchCar : NSObject, NSCoding{

    var message : String!
    var response : SearchCarResponse!
    var status : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        message = dictionary["message"] as? String
        status = dictionary["status"] as? Int
        if let responseData = dictionary["response"] as? [String:Any]{
            response = SearchCarResponse(fromDictionary: responseData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if response != nil{
            dictionary["response"] = response.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        message = aDecoder.decodeObject(forKey: "message") as? String
        response = aDecoder.decodeObject(forKey: "response") as? SearchCarResponse
        status = aDecoder.decodeObject(forKey: "status") as? Int
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if response != nil{
            aCoder.encode(response, forKey: "response")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }
}