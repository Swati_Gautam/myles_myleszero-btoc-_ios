//
//  SearchCarVariant.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 11, 2019

import Foundation


class SearchCarVariant : NSObject, NSCoding{

    var key : String!
    var value : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        key = dictionary["Key"] as? String
        value = dictionary["Value"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if key != nil{
            dictionary["Key"] = key
        }
        if value != nil{
            dictionary["Value"] = value
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        key = aDecoder.decodeObject(forKey: "Key") as? String
        value = aDecoder.decodeObject(forKey: "Value") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if key != nil{
            aCoder.encode(key, forKey: "Key")
        }
        if value != nil{
            aCoder.encode(value, forKey: "Value")
        }
    }
}