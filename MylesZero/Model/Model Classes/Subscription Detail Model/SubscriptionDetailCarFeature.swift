//
//  SubscriptionDetailCarFeature.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 28, 2020

import Foundation


class SubscriptionDetailCarFeature : NSObject, NSCoding{

    var featureName : String!
    var featureType : String!
    var value : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        featureName = dictionary["FeatureName"] as? String
        featureType = dictionary["FeatureType"] as? String
        value = dictionary["Value"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if featureName != nil{
            dictionary["FeatureName"] = featureName
        }
        if featureType != nil{
            dictionary["FeatureType"] = featureType
        }
        if value != nil{
            dictionary["Value"] = value
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        featureName = aDecoder.decodeObject(forKey: "FeatureName") as? String
        featureType = aDecoder.decodeObject(forKey: "FeatureType") as? String
        value = aDecoder.decodeObject(forKey: "Value") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if featureName != nil{
            aCoder.encode(featureName, forKey: "FeatureName")
        }
        if featureType != nil{
            aCoder.encode(featureType, forKey: "FeatureType")
        }
        if value != nil{
            aCoder.encode(value, forKey: "Value")
        }
    }
}