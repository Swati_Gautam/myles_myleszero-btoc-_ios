//
//  SubscriptionDetailCarDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 28, 2020

import Foundation


class SubscriptionDetailCarDetail : NSObject, NSCoding{

    var additionalCar : [AnyObject]!
    var carAge : String!
    var carAgeInMonth : AnyObject!
    var carCategory : String!
    var carCatID : Int!
    var carColorCode : String!
    var carColorName : String!
    var carCompany : AnyObject!
    var carCompName : String!
    var carFeature : [SubscriptionDetailCarFeature]!
    var carID : Int!
    var carImageMobile : String!
    var carImages : [String]!
    var carImageWeb : String!
    var carModelID : Int!
    var carModelName : String!
    var carOwnerShip : String!
    var carProviderAddressOne : String!
    var carProviderAddressSecond : String!
    var carProviderName : String!
    var carType : String!
    var carVariantID : String!
    var carVariantName : String!
    var cityID : Int!
    var cityName : String!
    var colourandprice : AnyObject!
    var depositAmt : Int!
    var extraKMRate : String!
    var fuelTypeName : String!
    var insuranceAmount : Int!
    var isAutoTransmission : Bool!
    var kmRun : String!
    var luggageCapacity : Int!
    var maxTenureMonths : Int!
    var minTenureMonths : Int!
    var payToInsuranceCompany : Int!
    var pkgID : Int!
    var pkgKmLimit : Int!
    var pkgKMs : String!
    var pkgRate : String!
    var pkgRateWeekEnd : Int!
    var pkgType : String!
    var price : Double!
    var seatingCapacity : Int!
    var sublocation : String!
    var sublocationID : String!
    var taxAmount : Int!
    var tentativeDeliveryDay : String!
    var transmissiontype : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        carAge = dictionary["CarAge"] as? String
        carAgeInMonth = dictionary["CarAgeInMonth"] as? AnyObject
        carCategory = dictionary["CarCategory"] as? String
        carCatID = dictionary["CarCatID"] as? Int
        carColorCode = dictionary["CarColorCode"] as? String
        carColorName = dictionary["CarColorName"] as? String
        carCompany = dictionary["CarCompany"] as? AnyObject
        carCompName = dictionary["CarCompName"] as? String
        carID = dictionary["CarID"] as? Int
        carImageMobile = dictionary["CarImageMobile"] as? String
        carImageWeb = dictionary["CarImageWeb"] as? String
        carImages = dictionary["carImages"] as? [String]
        carModelID = dictionary["CarModelID"] as? Int
        carModelName = dictionary["CarModelName"] as? String
        carOwnerShip = dictionary["CarOwnerShip"] as? String
        carProviderAddressOne = dictionary["CarProviderAddressOne"] as? String
        carProviderAddressSecond = dictionary["CarProviderAddressSecond"] as? String
        carProviderName = dictionary["CarProviderName"] as? String
        carType = dictionary["CarType"] as? String
        carVariantID = dictionary["CarVariantID"] as? String
        carVariantName = dictionary["CarVariantName"] as? String
        cityID = dictionary["CityID"] as? Int
        cityName = dictionary["CityName"] as? String
        colourandprice = dictionary["Colourandprice"] as? AnyObject
        depositAmt = dictionary["DepositAmt"] as? Int
        extraKMRate = dictionary["ExtraKMRate"] as? String
        fuelTypeName = dictionary["FuelTypeName"] as? String
        insuranceAmount = dictionary["InsuranceAmount"] as? Int
        isAutoTransmission = dictionary["IsAutoTransmission"] as? Bool
        kmRun = dictionary["KmRun"] as? String
        luggageCapacity = dictionary["LuggageCapacity"] as? Int
        maxTenureMonths = dictionary["MaxTenureMonths"] as? Int
        minTenureMonths = dictionary["MinTenureMonths"] as? Int
        payToInsuranceCompany = dictionary["PayToInsuranceCompany"] as? Int
        pkgID = dictionary["PkgID"] as? Int
        pkgKmLimit = dictionary["PkgKmLimit"] as? Int
        pkgKMs = dictionary["PkgKMs"] as? String
        pkgRate = dictionary["PkgRate"] as? String
        pkgRateWeekEnd = dictionary["PkgRate_WeekEnd"] as? Int
        pkgType = dictionary["PkgType"] as? String
        price = dictionary["Price"] as? Double
        seatingCapacity = dictionary["SeatingCapacity"] as? Int
        sublocation = dictionary["Sublocation"] as? String
        sublocationID = dictionary["SublocationID"] as? String
        taxAmount = dictionary["TaxAmount"] as? Int
        tentativeDeliveryDay = dictionary["TentativeDeliveryDay"] as? String
        transmissiontype = dictionary["Transmissiontype"] as? String
        carFeature = [SubscriptionDetailCarFeature]()
        if let carFeatureArray = dictionary["CarFeature"] as? [[String:Any]]{
            for dic in carFeatureArray{
                let value = SubscriptionDetailCarFeature(fromDictionary: dic)
                carFeature.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if carAge != nil{
            dictionary["CarAge"] = carAge
        }
        if carAgeInMonth != nil{
            dictionary["CarAgeInMonth"] = carAgeInMonth
        }
        if carCategory != nil{
            dictionary["CarCategory"] = carCategory
        }
        if carCatID != nil{
            dictionary["CarCatID"] = carCatID
        }
        if carColorCode != nil{
            dictionary["CarColorCode"] = carColorCode
        }
        if carColorName != nil{
            dictionary["CarColorName"] = carColorName
        }
        if carCompany != nil{
            dictionary["CarCompany"] = carCompany
        }
        if carCompName != nil{
            dictionary["CarCompName"] = carCompName
        }
        if carID != nil{
            dictionary["CarID"] = carID
        }
        if carImageMobile != nil{
            dictionary["CarImageMobile"] = carImageMobile
        }
        if carImageWeb != nil{
            dictionary["CarImageWeb"] = carImageWeb
        }
        if carModelID != nil{
            dictionary["CarModelID"] = carModelID
        }
        if carModelName != nil{
            dictionary["CarModelName"] = carModelName
        }
        if carOwnerShip != nil{
            dictionary["CarOwnerShip"] = carOwnerShip
        }
        if carProviderAddressOne != nil{
            dictionary["CarProviderAddressOne"] = carProviderAddressOne
        }
        if carProviderAddressSecond != nil{
            dictionary["CarProviderAddressSecond"] = carProviderAddressSecond
        }
        if carProviderName != nil{
            dictionary["CarProviderName"] = carProviderName
        }
        if carType != nil{
            dictionary["CarType"] = carType
        }
        if carVariantID != nil{
            dictionary["CarVariantID"] = carVariantID
        }
        if carVariantName != nil{
            dictionary["CarVariantName"] = carVariantName
        }
        if cityID != nil{
            dictionary["CityID"] = cityID
        }
        if cityName != nil{
            dictionary["CityName"] = cityName
        }
        if colourandprice != nil{
            dictionary["Colourandprice"] = colourandprice
        }
        if depositAmt != nil{
            dictionary["DepositAmt"] = depositAmt
        }
        if extraKMRate != nil{
            dictionary["ExtraKMRate"] = extraKMRate
        }
        if fuelTypeName != nil{
            dictionary["FuelTypeName"] = fuelTypeName
        }
        if insuranceAmount != nil{
            dictionary["InsuranceAmount"] = insuranceAmount
        }
        if isAutoTransmission != nil{
            dictionary["IsAutoTransmission"] = isAutoTransmission
        }
        if kmRun != nil{
            dictionary["KmRun"] = kmRun
        }
        if luggageCapacity != nil{
            dictionary["LuggageCapacity"] = luggageCapacity
        }
        if maxTenureMonths != nil{
            dictionary["MaxTenureMonths"] = maxTenureMonths
        }
        if minTenureMonths != nil{
            dictionary["MinTenureMonths"] = minTenureMonths
        }
        if payToInsuranceCompany != nil{
            dictionary["PayToInsuranceCompany"] = payToInsuranceCompany
        }
        if pkgID != nil{
            dictionary["PkgID"] = pkgID
        }
        if pkgKmLimit != nil{
            dictionary["PkgKmLimit"] = pkgKmLimit
        }
        if pkgKMs != nil{
            dictionary["PkgKMs"] = pkgKMs
        }
        if pkgRate != nil{
            dictionary["PkgRate"] = pkgRate
        }
        if pkgRateWeekEnd != nil{
            dictionary["PkgRate_WeekEnd"] = pkgRateWeekEnd
        }
        if pkgType != nil{
            dictionary["PkgType"] = pkgType
        }
        if price != nil{
            dictionary["Price"] = price
        }
        if seatingCapacity != nil{
            dictionary["SeatingCapacity"] = seatingCapacity
        }
        if sublocation != nil{
            dictionary["Sublocation"] = sublocation
        }
        if sublocationID != nil{
            dictionary["SublocationID"] = sublocationID
        }
        if taxAmount != nil{
            dictionary["TaxAmount"] = taxAmount
        }
        if tentativeDeliveryDay != nil{
            dictionary["TentativeDeliveryDay"] = tentativeDeliveryDay
        }
        if transmissiontype != nil{
            dictionary["Transmissiontype"] = transmissiontype
        }
        if carFeature != nil{
            var dictionaryElements = [[String:Any]]()
            for carFeatureElement in carFeature {
                dictionaryElements.append(carFeatureElement.toDictionary())
            }
            dictionary["carFeature"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        additionalCar = aDecoder.decodeObject(forKey: "AdditionalCar") as? [AnyObject]
        carAge = aDecoder.decodeObject(forKey: "CarAge") as? String
        carAgeInMonth = aDecoder.decodeObject(forKey: "CarAgeInMonth") as? AnyObject
        carCategory = aDecoder.decodeObject(forKey: "CarCategory") as? String
        carCatID = aDecoder.decodeObject(forKey: "CarCatID") as? Int
        carColorCode = aDecoder.decodeObject(forKey: "CarColorCode") as? String
        carColorName = aDecoder.decodeObject(forKey: "CarColorName") as? String
        carCompany = aDecoder.decodeObject(forKey: "CarCompany") as? AnyObject
        carCompName = aDecoder.decodeObject(forKey: "CarCompName") as? String
        carFeature = aDecoder.decodeObject(forKey: "CarFeature") as? [SubscriptionDetailCarFeature]
        carID = aDecoder.decodeObject(forKey: "CarID") as? Int
        carImageMobile = aDecoder.decodeObject(forKey: "CarImageMobile") as? String
        carImages = aDecoder.decodeObject(forKey: "carImages") as? [String]
        carImageWeb = aDecoder.decodeObject(forKey: "CarImageWeb") as? String
        carModelID = aDecoder.decodeObject(forKey: "CarModelID") as? Int
        carModelName = aDecoder.decodeObject(forKey: "CarModelName") as? String
        carOwnerShip = aDecoder.decodeObject(forKey: "CarOwnerShip") as? String
        carProviderAddressOne = aDecoder.decodeObject(forKey: "CarProviderAddressOne") as? String
        carProviderAddressSecond = aDecoder.decodeObject(forKey: "CarProviderAddressSecond") as? String
        carProviderName = aDecoder.decodeObject(forKey: "CarProviderName") as? String
        carType = aDecoder.decodeObject(forKey: "CarType") as? String
        carVariantID = aDecoder.decodeObject(forKey: "CarVariantID") as? String
        carVariantName = aDecoder.decodeObject(forKey: "CarVariantName") as? String
        cityID = aDecoder.decodeObject(forKey: "CityID") as? Int
        cityName = aDecoder.decodeObject(forKey: "CityName") as? String
        colourandprice = aDecoder.decodeObject(forKey: "Colourandprice") as? AnyObject
        depositAmt = aDecoder.decodeObject(forKey: "DepositAmt") as? Int
        extraKMRate = aDecoder.decodeObject(forKey: "ExtraKMRate") as? String
        fuelTypeName = aDecoder.decodeObject(forKey: "FuelTypeName") as? String
        insuranceAmount = aDecoder.decodeObject(forKey: "InsuranceAmount") as? Int
        isAutoTransmission = aDecoder.decodeObject(forKey: "IsAutoTransmission") as? Bool
        kmRun = aDecoder.decodeObject(forKey: "KmRun") as? String
        luggageCapacity = aDecoder.decodeObject(forKey: "LuggageCapacity") as? Int
        maxTenureMonths = aDecoder.decodeObject(forKey: "MaxTenureMonths") as? Int
        minTenureMonths = aDecoder.decodeObject(forKey: "MinTenureMonths") as? Int
        payToInsuranceCompany = aDecoder.decodeObject(forKey: "PayToInsuranceCompany") as? Int
        pkgID = aDecoder.decodeObject(forKey: "PkgID") as? Int
        pkgKmLimit = aDecoder.decodeObject(forKey: "PkgKmLimit") as? Int
        pkgKMs = aDecoder.decodeObject(forKey: "PkgKMs") as? String
        pkgRate = aDecoder.decodeObject(forKey: "PkgRate") as? String
        pkgRateWeekEnd = aDecoder.decodeObject(forKey: "PkgRate_WeekEnd") as? Int
        pkgType = aDecoder.decodeObject(forKey: "PkgType") as? String
        price = aDecoder.decodeObject(forKey: "Price") as? Double
        seatingCapacity = aDecoder.decodeObject(forKey: "SeatingCapacity") as? Int
        sublocation = aDecoder.decodeObject(forKey: "Sublocation") as? String
        sublocationID = aDecoder.decodeObject(forKey: "SublocationID") as? String
        taxAmount = aDecoder.decodeObject(forKey: "TaxAmount") as? Int
        tentativeDeliveryDay = aDecoder.decodeObject(forKey: "TentativeDeliveryDay") as? String
        transmissiontype = aDecoder.decodeObject(forKey: "Transmissiontype") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if additionalCar != nil{
            aCoder.encode(additionalCar, forKey: "AdditionalCar")
        }
        if carAge != nil{
            aCoder.encode(carAge, forKey: "CarAge")
        }
        if carAgeInMonth != nil{
            aCoder.encode(carAgeInMonth, forKey: "CarAgeInMonth")
        }
        if carCategory != nil{
            aCoder.encode(carCategory, forKey: "CarCategory")
        }
        if carCatID != nil{
            aCoder.encode(carCatID, forKey: "CarCatID")
        }
        if carColorCode != nil{
            aCoder.encode(carColorCode, forKey: "CarColorCode")
        }
        if carColorName != nil{
            aCoder.encode(carColorName, forKey: "CarColorName")
        }
        if carCompany != nil{
            aCoder.encode(carCompany, forKey: "CarCompany")
        }
        if carCompName != nil{
            aCoder.encode(carCompName, forKey: "CarCompName")
        }
        if carFeature != nil{
            aCoder.encode(carFeature, forKey: "CarFeature")
        }
        if carID != nil{
            aCoder.encode(carID, forKey: "CarID")
        }
        if carImageMobile != nil{
            aCoder.encode(carImageMobile, forKey: "CarImageMobile")
        }
        if carImages != nil{
            aCoder.encode(carImages, forKey: "carImages")
        }
        if carImageWeb != nil{
            aCoder.encode(carImageWeb, forKey: "CarImageWeb")
        }
        if carModelID != nil{
            aCoder.encode(carModelID, forKey: "CarModelID")
        }
        if carModelName != nil{
            aCoder.encode(carModelName, forKey: "CarModelName")
        }
        if carOwnerShip != nil{
            aCoder.encode(carOwnerShip, forKey: "CarOwnerShip")
        }
        if carProviderAddressOne != nil{
            aCoder.encode(carProviderAddressOne, forKey: "CarProviderAddressOne")
        }
        if carProviderAddressSecond != nil{
            aCoder.encode(carProviderAddressSecond, forKey: "CarProviderAddressSecond")
        }
        if carProviderName != nil{
            aCoder.encode(carProviderName, forKey: "CarProviderName")
        }
        if carType != nil{
            aCoder.encode(carType, forKey: "CarType")
        }
        if carVariantID != nil{
            aCoder.encode(carVariantID, forKey: "CarVariantID")
        }
        if carVariantName != nil{
            aCoder.encode(carVariantName, forKey: "CarVariantName")
        }
        if cityID != nil{
            aCoder.encode(cityID, forKey: "CityID")
        }
        if cityName != nil{
            aCoder.encode(cityName, forKey: "CityName")
        }
        if colourandprice != nil{
            aCoder.encode(colourandprice, forKey: "Colourandprice")
        }
        if depositAmt != nil{
            aCoder.encode(depositAmt, forKey: "DepositAmt")
        }
        if extraKMRate != nil{
            aCoder.encode(extraKMRate, forKey: "ExtraKMRate")
        }
        if fuelTypeName != nil{
            aCoder.encode(fuelTypeName, forKey: "FuelTypeName")
        }
        if insuranceAmount != nil{
            aCoder.encode(insuranceAmount, forKey: "InsuranceAmount")
        }
        if isAutoTransmission != nil{
            aCoder.encode(isAutoTransmission, forKey: "IsAutoTransmission")
        }
        if kmRun != nil{
            aCoder.encode(kmRun, forKey: "KmRun")
        }
        if luggageCapacity != nil{
            aCoder.encode(luggageCapacity, forKey: "LuggageCapacity")
        }
        if maxTenureMonths != nil{
            aCoder.encode(maxTenureMonths, forKey: "MaxTenureMonths")
        }
        if minTenureMonths != nil{
            aCoder.encode(minTenureMonths, forKey: "MinTenureMonths")
        }
        if payToInsuranceCompany != nil{
            aCoder.encode(payToInsuranceCompany, forKey: "PayToInsuranceCompany")
        }
        if pkgID != nil{
            aCoder.encode(pkgID, forKey: "PkgID")
        }
        if pkgKmLimit != nil{
            aCoder.encode(pkgKmLimit, forKey: "PkgKmLimit")
        }
        if pkgKMs != nil{
            aCoder.encode(pkgKMs, forKey: "PkgKMs")
        }
        if pkgRate != nil{
            aCoder.encode(pkgRate, forKey: "PkgRate")
        }
        if pkgRateWeekEnd != nil{
            aCoder.encode(pkgRateWeekEnd, forKey: "PkgRate_WeekEnd")
        }
        if pkgType != nil{
            aCoder.encode(pkgType, forKey: "PkgType")
        }
        if price != nil{
            aCoder.encode(price, forKey: "Price")
        }
        if seatingCapacity != nil{
            aCoder.encode(seatingCapacity, forKey: "SeatingCapacity")
        }
        if sublocation != nil{
            aCoder.encode(sublocation, forKey: "Sublocation")
        }
        if sublocationID != nil{
            aCoder.encode(sublocationID, forKey: "SublocationID")
        }
        if taxAmount != nil{
            aCoder.encode(taxAmount, forKey: "TaxAmount")
        }
        if tentativeDeliveryDay != nil{
            aCoder.encode(tentativeDeliveryDay, forKey: "TentativeDeliveryDay")
        }
        if transmissiontype != nil{
            aCoder.encode(transmissiontype, forKey: "Transmissiontype")
        }
    }
}
