//
//  SubscriptionDetailResponse.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 28, 2020

import Foundation


class SubscriptionDetailResponse : NSObject, NSCoding{

    var carDetail : SubscriptionDetailCarDetail!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let carDetailData = dictionary["CarDetail"] as? [String:Any]{
            carDetail = SubscriptionDetailCarDetail(fromDictionary: carDetailData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if carDetail != nil{
            dictionary["carDetail"] = carDetail.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        carDetail = aDecoder.decodeObject(forKey: "CarDetail") as? SubscriptionDetailCarDetail
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if carDetail != nil{
            aCoder.encode(carDetail, forKey: "CarDetail")
        }
    }
}