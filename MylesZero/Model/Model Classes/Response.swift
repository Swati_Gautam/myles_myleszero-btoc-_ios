//
//  Response.swift
//
//  Created by skpissay on 06/09/19
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Response: Codable {

  enum CodingKeys: String, CodingKey {
    case cityName = "CityName"
    case latitude = "Latitude"
    case cityShortName = "CityShortName"
    case cityId = "CityId"
    case longitude = "Longitude"
    case isHomePickUpDropOffAvailable = "IsHomePickUpDropOffAvailable"
  }

  var cityName: String?
  var latitude: String?
  var cityShortName: String?
  var cityId: String?
  var longitude: String?
  var isHomePickUpDropOffAvailable: Int?

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    cityName = try container.decodeIfPresent(String.self, forKey: .cityName)
    latitude = try container.decodeIfPresent(String.self, forKey: .latitude)
    cityShortName = try container.decodeIfPresent(String.self, forKey: .cityShortName)
    cityId = try container.decodeIfPresent(String.self, forKey: .cityId)
    longitude = try container.decodeIfPresent(String.self, forKey: .longitude)
    isHomePickUpDropOffAvailable = try container.decodeIfPresent(Int.self, forKey: .isHomePickUpDropOffAvailable)
  }

}
