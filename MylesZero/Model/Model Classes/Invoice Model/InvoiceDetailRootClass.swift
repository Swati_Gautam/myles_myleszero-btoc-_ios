//
//  InvoiceDetailRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 27, 2020

import Foundation


class InvoiceDetailRootClass : NSObject, NSCoding{

    var accountingDate : String!
    var basicCost : String!
    var bookingId : String!
    var carModel : String!
    var carNo : String!
    var cGSTAmount : String!
    var cGSTRate : String!
    var dlexpirydate : AnyObject!
    var emailID : String!
    var fromdate : String!
    var iGSTAmount : String!
    var iGSTRate : String!
    var invoiceID : String!
    var invoiceNumber : String!
    var loginid : AnyObject!
    var logintype : AnyObject!
    var monthsRemaining : String!
    var name : String!
    var nextPaymentDate : String!
    var paidStatus : String!
    var phone : String!
    var sGSTAmount : String!
    var sGSTRate : String!
    var subscriptionEndDate : String!
    var subscriptionId : String!
    var subscriptionStartDate : String!
    var taxes : String!
    var tenure : String!
    var todate : String!
    var totalCost : String!
    var url : String!
    var uTGSTAmount : String!
    var uTGSTRate : String!
    var variant : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        accountingDate = dictionary["AccountingDate"] as? String
        basicCost = dictionary["BasicCost"] as? String
        bookingId = dictionary["BookingId"] as? String
        carModel = dictionary["CarModel"] as? String
        carNo = dictionary["CarNo"] as? String
        cGSTAmount = dictionary["CGSTAmount"] as? String
        cGSTRate = dictionary["CGSTRate"] as? String
        dlexpirydate = dictionary["dlexpirydate"] as? AnyObject
        emailID = dictionary["EmailID"] as? String
        fromdate = dictionary["fromdate"] as? String
        iGSTAmount = dictionary["IGSTAmount"] as? String
        iGSTRate = dictionary["IGSTRate"] as? String
        invoiceID = dictionary["InvoiceID"] as? String
        invoiceNumber = dictionary["InvoiceNumber"] as? String
        loginid = dictionary["loginid"] as? AnyObject
        logintype = dictionary["logintype"] as? AnyObject
        monthsRemaining = dictionary["MonthsRemaining"] as? String
        name = dictionary["Name"] as? String
        nextPaymentDate = dictionary["NextPaymentDate"] as? String
        paidStatus = dictionary["PaidStatus"] as? String
        phone = dictionary["Phone"] as? String
        sGSTAmount = dictionary["SGSTAmount"] as? String
        sGSTRate = dictionary["SGSTRate"] as? String
        subscriptionEndDate = dictionary["SubscriptionEndDate"] as? String
        subscriptionId = dictionary["SubscriptionId"] as? String
        subscriptionStartDate = dictionary["SubscriptionStartDate"] as? String
        taxes = dictionary["Taxes"] as? String
        tenure = dictionary["Tenure"] as? String
        todate = dictionary["todate"] as? String
        totalCost = dictionary["TotalCost"] as? String
        url = dictionary["Url"] as? String
        uTGSTAmount = dictionary["UTGSTAmount"] as? String
        uTGSTRate = dictionary["UTGSTRate"] as? String
        variant = dictionary["Variant"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if accountingDate != nil{
            dictionary["AccountingDate"] = accountingDate
        }
        if basicCost != nil{
            dictionary["BasicCost"] = basicCost
        }
        if bookingId != nil{
            dictionary["BookingId"] = bookingId
        }
        if carModel != nil{
            dictionary["CarModel"] = carModel
        }
        if carNo != nil{
            dictionary["CarNo"] = carNo
        }
        if cGSTAmount != nil{
            dictionary["CGSTAmount"] = cGSTAmount
        }
        if cGSTRate != nil{
            dictionary["CGSTRate"] = cGSTRate
        }
        if dlexpirydate != nil{
            dictionary["dlexpirydate"] = dlexpirydate
        }
        if emailID != nil{
            dictionary["EmailID"] = emailID
        }
        if fromdate != nil{
            dictionary["fromdate"] = fromdate
        }
        if iGSTAmount != nil{
            dictionary["IGSTAmount"] = iGSTAmount
        }
        if iGSTRate != nil{
            dictionary["IGSTRate"] = iGSTRate
        }
        if invoiceID != nil{
            dictionary["InvoiceID"] = invoiceID
        }
        if invoiceNumber != nil{
            dictionary["InvoiceNumber"] = invoiceNumber
        }
        if loginid != nil{
            dictionary["loginid"] = loginid
        }
        if logintype != nil{
            dictionary["logintype"] = logintype
        }
        if monthsRemaining != nil{
            dictionary["MonthsRemaining"] = monthsRemaining
        }
        if name != nil{
            dictionary["Name"] = name
        }
        if nextPaymentDate != nil{
            dictionary["NextPaymentDate"] = nextPaymentDate
        }
        if paidStatus != nil{
            dictionary["PaidStatus"] = paidStatus
        }
        if phone != nil{
            dictionary["Phone"] = phone
        }
        if sGSTAmount != nil{
            dictionary["SGSTAmount"] = sGSTAmount
        }
        if sGSTRate != nil{
            dictionary["SGSTRate"] = sGSTRate
        }
        if subscriptionEndDate != nil{
            dictionary["SubscriptionEndDate"] = subscriptionEndDate
        }
        if subscriptionId != nil{
            dictionary["SubscriptionId"] = subscriptionId
        }
        if subscriptionStartDate != nil{
            dictionary["SubscriptionStartDate"] = subscriptionStartDate
        }
        if taxes != nil{
            dictionary["Taxes"] = taxes
        }
        if tenure != nil{
            dictionary["Tenure"] = tenure
        }
        if todate != nil{
            dictionary["todate"] = todate
        }
        if totalCost != nil{
            dictionary["TotalCost"] = totalCost
        }
        if url != nil{
            dictionary["Url"] = url
        }
        if uTGSTAmount != nil{
            dictionary["UTGSTAmount"] = uTGSTAmount
        }
        if uTGSTRate != nil{
            dictionary["UTGSTRate"] = uTGSTRate
        }
        if variant != nil{
            dictionary["Variant"] = variant
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        accountingDate = aDecoder.decodeObject(forKey: "AccountingDate") as? String
        basicCost = aDecoder.decodeObject(forKey: "BasicCost") as? String
        bookingId = aDecoder.decodeObject(forKey: "BookingId") as? String
        carModel = aDecoder.decodeObject(forKey: "CarModel") as? String
        carNo = aDecoder.decodeObject(forKey: "CarNo") as? String
        cGSTAmount = aDecoder.decodeObject(forKey: "CGSTAmount") as? String
        cGSTRate = aDecoder.decodeObject(forKey: "CGSTRate") as? String
        dlexpirydate = aDecoder.decodeObject(forKey: "dlexpirydate") as? AnyObject
        emailID = aDecoder.decodeObject(forKey: "EmailID") as? String
        fromdate = aDecoder.decodeObject(forKey: "fromdate") as? String
        iGSTAmount = aDecoder.decodeObject(forKey: "IGSTAmount") as? String
        iGSTRate = aDecoder.decodeObject(forKey: "IGSTRate") as? String
        invoiceID = aDecoder.decodeObject(forKey: "InvoiceID") as? String
        invoiceNumber = aDecoder.decodeObject(forKey: "InvoiceNumber") as? String
        loginid = aDecoder.decodeObject(forKey: "loginid") as? AnyObject
        logintype = aDecoder.decodeObject(forKey: "logintype") as? AnyObject
        monthsRemaining = aDecoder.decodeObject(forKey: "MonthsRemaining") as? String
        name = aDecoder.decodeObject(forKey: "Name") as? String
        nextPaymentDate = aDecoder.decodeObject(forKey: "NextPaymentDate") as? String
        paidStatus = aDecoder.decodeObject(forKey: "PaidStatus") as? String
        phone = aDecoder.decodeObject(forKey: "Phone") as? String
        sGSTAmount = aDecoder.decodeObject(forKey: "SGSTAmount") as? String
        sGSTRate = aDecoder.decodeObject(forKey: "SGSTRate") as? String
        subscriptionEndDate = aDecoder.decodeObject(forKey: "SubscriptionEndDate") as? String
        subscriptionId = aDecoder.decodeObject(forKey: "SubscriptionId") as? String
        subscriptionStartDate = aDecoder.decodeObject(forKey: "SubscriptionStartDate") as? String
        taxes = aDecoder.decodeObject(forKey: "Taxes") as? String
        tenure = aDecoder.decodeObject(forKey: "Tenure") as? String
        todate = aDecoder.decodeObject(forKey: "todate") as? String
        totalCost = aDecoder.decodeObject(forKey: "TotalCost") as? String
        url = aDecoder.decodeObject(forKey: "Url") as? String
        uTGSTAmount = aDecoder.decodeObject(forKey: "UTGSTAmount") as? String
        uTGSTRate = aDecoder.decodeObject(forKey: "UTGSTRate") as? String
        variant = aDecoder.decodeObject(forKey: "Variant") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if accountingDate != nil{
            aCoder.encode(accountingDate, forKey: "AccountingDate")
        }
        if basicCost != nil{
            aCoder.encode(basicCost, forKey: "BasicCost")
        }
        if bookingId != nil{
            aCoder.encode(bookingId, forKey: "BookingId")
        }
        if carModel != nil{
            aCoder.encode(carModel, forKey: "CarModel")
        }
        if carNo != nil{
            aCoder.encode(carNo, forKey: "CarNo")
        }
        if cGSTAmount != nil{
            aCoder.encode(cGSTAmount, forKey: "CGSTAmount")
        }
        if cGSTRate != nil{
            aCoder.encode(cGSTRate, forKey: "CGSTRate")
        }
        if dlexpirydate != nil{
            aCoder.encode(dlexpirydate, forKey: "dlexpirydate")
        }
        if emailID != nil{
            aCoder.encode(emailID, forKey: "EmailID")
        }
        if fromdate != nil{
            aCoder.encode(fromdate, forKey: "fromdate")
        }
        if iGSTAmount != nil{
            aCoder.encode(iGSTAmount, forKey: "IGSTAmount")
        }
        if iGSTRate != nil{
            aCoder.encode(iGSTRate, forKey: "IGSTRate")
        }
        if invoiceID != nil{
            aCoder.encode(invoiceID, forKey: "InvoiceID")
        }
        if invoiceNumber != nil{
            aCoder.encode(invoiceNumber, forKey: "InvoiceNumber")
        }
        if loginid != nil{
            aCoder.encode(loginid, forKey: "loginid")
        }
        if logintype != nil{
            aCoder.encode(logintype, forKey: "logintype")
        }
        if monthsRemaining != nil{
            aCoder.encode(monthsRemaining, forKey: "MonthsRemaining")
        }
        if name != nil{
            aCoder.encode(name, forKey: "Name")
        }
        if nextPaymentDate != nil{
            aCoder.encode(nextPaymentDate, forKey: "NextPaymentDate")
        }
        if paidStatus != nil{
            aCoder.encode(paidStatus, forKey: "PaidStatus")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "Phone")
        }
        if sGSTAmount != nil{
            aCoder.encode(sGSTAmount, forKey: "SGSTAmount")
        }
        if sGSTRate != nil{
            aCoder.encode(sGSTRate, forKey: "SGSTRate")
        }
        if subscriptionEndDate != nil{
            aCoder.encode(subscriptionEndDate, forKey: "SubscriptionEndDate")
        }
        if subscriptionId != nil{
            aCoder.encode(subscriptionId, forKey: "SubscriptionId")
        }
        if subscriptionStartDate != nil{
            aCoder.encode(subscriptionStartDate, forKey: "SubscriptionStartDate")
        }
        if taxes != nil{
            aCoder.encode(taxes, forKey: "Taxes")
        }
        if tenure != nil{
            aCoder.encode(tenure, forKey: "Tenure")
        }
        if todate != nil{
            aCoder.encode(todate, forKey: "todate")
        }
        if totalCost != nil{
            aCoder.encode(totalCost, forKey: "TotalCost")
        }
        if url != nil{
            aCoder.encode(url, forKey: "Url")
        }
        if uTGSTAmount != nil{
            aCoder.encode(uTGSTAmount, forKey: "UTGSTAmount")
        }
        if uTGSTRate != nil{
            aCoder.encode(uTGSTRate, forKey: "UTGSTRate")
        }
        if variant != nil{
            aCoder.encode(variant, forKey: "Variant")
        }
    }
}