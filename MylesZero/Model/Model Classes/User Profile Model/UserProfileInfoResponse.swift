//
//  UserProfileInfoResponse.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 14, 2020

import Foundation


class UserProfileInfoResponse : NSObject, NSCoding{

    var aadhardob : String!
    var aadharfile : String!
    var aadharname : String!
    var aadharno : String!
    var aadharverify : String!
    var address : String!
    var address1delivery : String!
    var address1permanent : String!
    var address2delivery : String!
    var address2permanent : String!
    var city : String!
    var citydelivery : String!
    var cityID : String!
    var company : String!
    var deliverydate : String!
    var dlexpirydate : String!
    var dlfile : String!
    var dlNo : String!
    var dlverify : String!
    var doctype : String!
    var emailID : String!
    var employeeID : String!
    var employeeName : String!
    var iD : String!
    var isAddressSame : String!
    var panfile : String!
    var panNo : String!
    var panverify : String!
    var passportcountryco : String!
    var passportDOb : String!
    var passportexpirydate : String!
    var passportfile : String!
    //var passportfile : String!
    var passportfirstname : String!
    var passportGender : String!
    var passportMRZ1 : String!
    var passportMRZ2 : String!
    var passportNo : String!
    var passportsurname : String!
    var passporttype : String!
    var passportverify : String!
    var permanantAddress : String!
    var phone1 : String!
    var pincode : String!
    var pincodedelivery : String!
    var rentallimit : String!
    var sameaddress : Bool!
    var state : String!
    var statedelivery : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        aadhardob = dictionary["aadhardob"] as? String
        aadharfile = dictionary["aadharfile"] as? String
        aadharname = dictionary["aadharname"] as? String
        aadharno = dictionary["aadharno"] as? String
        aadharverify = dictionary["aadharverify"] as? String
        address = dictionary["Address"] as? String
        address1delivery = dictionary["address1delivery"] as? String
        address1permanent = dictionary["address1permanent"] as? String
        address2delivery = dictionary["address2delivery"] as? String
        address2permanent = dictionary["address2permanent"] as? String
        city = dictionary["city"] as? String
        citydelivery = dictionary["citydelivery"] as? String
        cityID = dictionary["cityID"] as? String
        company = dictionary["company"] as? String
        deliverydate = dictionary["deliverydate"] as? String
        dlexpirydate = dictionary["dlexpirydate"] as? String
        dlfile = dictionary["dlfile"] as? String
        dlNo = dictionary["dlNo"] as? String
        dlverify = dictionary["dlverify"] as? String
        doctype = dictionary["doctype"] as? String
        emailID = dictionary["EmailID"] as? String
        employeeID = dictionary["EmployeeID"] as? String
        employeeName = dictionary["EmployeeName"] as? String
        iD = dictionary["ID"] as? String
        isAddressSame = dictionary["IsAddressSame"] as? String
        panfile = dictionary["panfile"] as? String
        panNo = dictionary["PanNo"] as? String
        panverify = dictionary["Panverify"] as? String
        passportcountryco = dictionary["Passportcountryco"] as? String
        passportDOb = dictionary["PassportDOb"] as? String
        passportexpirydate = dictionary["passportexpirydate"] as? String
        passportfile = dictionary["passportfile"] as? String
        passportfile = dictionary["Passportfile"] as? String
        passportfirstname = dictionary["Passportfirstname"] as? String
        passportGender = dictionary["PassportGender"] as? String
        passportMRZ1 = dictionary["PassportMRZ1"] as? String
        passportMRZ2 = dictionary["PassportMRZ2"] as? String
        passportNo = dictionary["passportNo"] as? String
        passportsurname = dictionary["Passportsurname"] as? String
        passporttype = dictionary["Passporttype"] as? String
        passportverify = dictionary["Passportverify"] as? String
        permanantAddress = dictionary["PermanantAddress"] as? String
        phone1 = dictionary["Phone1"] as? String
        pincode = dictionary["pincode"] as? String
        pincodedelivery = dictionary["pincodedelivery"] as? String
        rentallimit = dictionary["rentallimit"] as? String
        sameaddress = dictionary["sameaddress"] as? Bool
        state = dictionary["state"] as? String
        statedelivery = dictionary["statedelivery"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if aadhardob != nil{
            dictionary["aadhardob"] = aadhardob
        }
        if aadharfile != nil{
            dictionary["aadharfile"] = aadharfile
        }
        if aadharname != nil{
            dictionary["aadharname"] = aadharname
        }
        if aadharno != nil{
            dictionary["aadharno"] = aadharno
        }
        if aadharverify != nil{
            dictionary["aadharverify"] = aadharverify
        }
        if address != nil{
            dictionary["Address"] = address
        }
        if address1delivery != nil{
            dictionary["address1delivery"] = address1delivery
        }
        if address1permanent != nil{
            dictionary["address1permanent"] = address1permanent
        }
        if address2delivery != nil{
            dictionary["address2delivery"] = address2delivery
        }
        if address2permanent != nil{
            dictionary["address2permanent"] = address2permanent
        }
        if city != nil{
            dictionary["city"] = city
        }
        if citydelivery != nil{
            dictionary["citydelivery"] = citydelivery
        }
        if cityID != nil{
            dictionary["cityID"] = cityID
        }
        if company != nil{
            dictionary["company"] = company
        }
        if deliverydate != nil{
            dictionary["deliverydate"] = deliverydate
        }
        if dlexpirydate != nil{
            dictionary["dlexpirydate"] = dlexpirydate
        }
        if dlfile != nil{
            dictionary["dlfile"] = dlfile
        }
        if dlNo != nil{
            dictionary["dlNo"] = dlNo
        }
        if dlverify != nil{
            dictionary["dlverify"] = dlverify
        }
        if doctype != nil{
            dictionary["doctype"] = doctype
        }
        if emailID != nil{
            dictionary["EmailID"] = emailID
        }
        if employeeID != nil{
            dictionary["EmployeeID"] = employeeID
        }
        if employeeName != nil{
            dictionary["EmployeeName"] = employeeName
        }
        if iD != nil{
            dictionary["ID"] = iD
        }
        if isAddressSame != nil{
            dictionary["IsAddressSame"] = isAddressSame
        }
        if panfile != nil{
            dictionary["panfile"] = panfile
        }
        if panNo != nil{
            dictionary["PanNo"] = panNo
        }
        if panverify != nil{
            dictionary["Panverify"] = panverify
        }
        if passportcountryco != nil{
            dictionary["Passportcountryco"] = passportcountryco
        }
        if passportDOb != nil{
            dictionary["PassportDOb"] = passportDOb
        }
        if passportexpirydate != nil{
            dictionary["passportexpirydate"] = passportexpirydate
        }
        if passportfile != nil{
            dictionary["passportfile"] = passportfile
        }
        if passportfile != nil{
            dictionary["Passportfile"] = passportfile
        }
        if passportfirstname != nil{
            dictionary["Passportfirstname"] = passportfirstname
        }
        if passportGender != nil{
            dictionary["PassportGender"] = passportGender
        }
        if passportMRZ1 != nil{
            dictionary["PassportMRZ1"] = passportMRZ1
        }
        if passportMRZ2 != nil{
            dictionary["PassportMRZ2"] = passportMRZ2
        }
        if passportNo != nil{
            dictionary["passportNo"] = passportNo
        }
        if passportsurname != nil{
            dictionary["Passportsurname"] = passportsurname
        }
        if passporttype != nil{
            dictionary["Passporttype"] = passporttype
        }
        if passportverify != nil{
            dictionary["Passportverify"] = passportverify
        }
        if permanantAddress != nil{
            dictionary["PermanantAddress"] = permanantAddress
        }
        if phone1 != nil{
            dictionary["Phone1"] = phone1
        }
        if pincode != nil{
            dictionary["pincode"] = pincode
        }
        if pincodedelivery != nil{
            dictionary["pincodedelivery"] = pincodedelivery
        }
        if rentallimit != nil{
            dictionary["rentallimit"] = rentallimit
        }
        if sameaddress != nil{
            dictionary["sameaddress"] = sameaddress
        }
        if state != nil{
            dictionary["state"] = state
        }
        if statedelivery != nil{
            dictionary["statedelivery"] = statedelivery
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        aadhardob = aDecoder.decodeObject(forKey: "aadhardob") as? String
        aadharfile = aDecoder.decodeObject(forKey: "aadharfile") as? String
        aadharname = aDecoder.decodeObject(forKey: "aadharname") as? String
        aadharno = aDecoder.decodeObject(forKey: "aadharno") as? String
        aadharverify = aDecoder.decodeObject(forKey: "aadharverify") as? String
        address = aDecoder.decodeObject(forKey: "Address") as? String
        address1delivery = aDecoder.decodeObject(forKey: "address1delivery") as? String
        address1permanent = aDecoder.decodeObject(forKey: "address1permanent") as? String
        address2delivery = aDecoder.decodeObject(forKey: "address2delivery") as? String
        address2permanent = aDecoder.decodeObject(forKey: "address2permanent") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        citydelivery = aDecoder.decodeObject(forKey: "citydelivery") as? String
        cityID = aDecoder.decodeObject(forKey: "cityID") as? String
        company = aDecoder.decodeObject(forKey: "company") as? String
        deliverydate = aDecoder.decodeObject(forKey: "deliverydate") as? String
        dlexpirydate = aDecoder.decodeObject(forKey: "dlexpirydate") as? String
        dlfile = aDecoder.decodeObject(forKey: "dlfile") as? String
        dlNo = aDecoder.decodeObject(forKey: "dlNo") as? String
        dlverify = aDecoder.decodeObject(forKey: "dlverify") as? String
        doctype = aDecoder.decodeObject(forKey: "doctype") as? String
        emailID = aDecoder.decodeObject(forKey: "EmailID") as? String
        employeeID = aDecoder.decodeObject(forKey: "EmployeeID") as? String
        employeeName = aDecoder.decodeObject(forKey: "EmployeeName") as? String
        iD = aDecoder.decodeObject(forKey: "ID") as? String
        isAddressSame = aDecoder.decodeObject(forKey: "IsAddressSame") as? String
        panfile = aDecoder.decodeObject(forKey: "panfile") as? String
        panNo = aDecoder.decodeObject(forKey: "PanNo") as? String
        panverify = aDecoder.decodeObject(forKey: "Panverify") as? String
        passportcountryco = aDecoder.decodeObject(forKey: "Passportcountryco") as? String
        passportDOb = aDecoder.decodeObject(forKey: "PassportDOb") as? String
        passportexpirydate = aDecoder.decodeObject(forKey: "passportexpirydate") as? String
        passportfile = aDecoder.decodeObject(forKey: "passportfile") as? String
        passportfile = aDecoder.decodeObject(forKey: "Passportfile") as? String
        passportfirstname = aDecoder.decodeObject(forKey: "Passportfirstname") as? String
        passportGender = aDecoder.decodeObject(forKey: "PassportGender") as? String
        passportMRZ1 = aDecoder.decodeObject(forKey: "PassportMRZ1") as? String
        passportMRZ2 = aDecoder.decodeObject(forKey: "PassportMRZ2") as? String
        passportNo = aDecoder.decodeObject(forKey: "passportNo") as? String
        passportsurname = aDecoder.decodeObject(forKey: "Passportsurname") as? String
        passporttype = aDecoder.decodeObject(forKey: "Passporttype") as? String
        passportverify = aDecoder.decodeObject(forKey: "Passportverify") as? String
        permanantAddress = aDecoder.decodeObject(forKey: "PermanantAddress") as? String
        phone1 = aDecoder.decodeObject(forKey: "Phone1") as? String
        pincode = aDecoder.decodeObject(forKey: "pincode") as? String
        pincodedelivery = aDecoder.decodeObject(forKey: "pincodedelivery") as? String
        rentallimit = aDecoder.decodeObject(forKey: "rentallimit") as? String
        sameaddress = aDecoder.decodeObject(forKey: "sameaddress") as? Bool
        state = aDecoder.decodeObject(forKey: "state") as? String
        statedelivery = aDecoder.decodeObject(forKey: "statedelivery") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if aadhardob != nil{
            aCoder.encode(aadhardob, forKey: "aadhardob")
        }
        if aadharfile != nil{
            aCoder.encode(aadharfile, forKey: "aadharfile")
        }
        if aadharname != nil{
            aCoder.encode(aadharname, forKey: "aadharname")
        }
        if aadharno != nil{
            aCoder.encode(aadharno, forKey: "aadharno")
        }
        if aadharverify != nil{
            aCoder.encode(aadharverify, forKey: "aadharverify")
        }
        if address != nil{
            aCoder.encode(address, forKey: "Address")
        }
        if address1delivery != nil{
            aCoder.encode(address1delivery, forKey: "address1delivery")
        }
        if address1permanent != nil{
            aCoder.encode(address1permanent, forKey: "address1permanent")
        }
        if address2delivery != nil{
            aCoder.encode(address2delivery, forKey: "address2delivery")
        }
        if address2permanent != nil{
            aCoder.encode(address2permanent, forKey: "address2permanent")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if citydelivery != nil{
            aCoder.encode(citydelivery, forKey: "citydelivery")
        }
        if cityID != nil{
            aCoder.encode(cityID, forKey: "cityID")
        }
        if company != nil{
            aCoder.encode(company, forKey: "company")
        }
        if deliverydate != nil{
            aCoder.encode(deliverydate, forKey: "deliverydate")
        }
        if dlexpirydate != nil{
            aCoder.encode(dlexpirydate, forKey: "dlexpirydate")
        }
        if dlfile != nil{
            aCoder.encode(dlfile, forKey: "dlfile")
        }
        if dlNo != nil{
            aCoder.encode(dlNo, forKey: "dlNo")
        }
        if dlverify != nil{
            aCoder.encode(dlverify, forKey: "dlverify")
        }
        if doctype != nil{
            aCoder.encode(doctype, forKey: "doctype")
        }
        if emailID != nil{
            aCoder.encode(emailID, forKey: "EmailID")
        }
        if employeeID != nil{
            aCoder.encode(employeeID, forKey: "EmployeeID")
        }
        if employeeName != nil{
            aCoder.encode(employeeName, forKey: "EmployeeName")
        }
        if iD != nil{
            aCoder.encode(iD, forKey: "ID")
        }
        if isAddressSame != nil{
            aCoder.encode(isAddressSame, forKey: "IsAddressSame")
        }
        if panfile != nil{
            aCoder.encode(panfile, forKey: "panfile")
        }
        if panNo != nil{
            aCoder.encode(panNo, forKey: "PanNo")
        }
        if panverify != nil{
            aCoder.encode(panverify, forKey: "Panverify")
        }
        if passportcountryco != nil{
            aCoder.encode(passportcountryco, forKey: "Passportcountryco")
        }
        if passportDOb != nil{
            aCoder.encode(passportDOb, forKey: "PassportDOb")
        }
        if passportexpirydate != nil{
            aCoder.encode(passportexpirydate, forKey: "passportexpirydate")
        }
        if passportfile != nil{
            aCoder.encode(passportfile, forKey: "passportfile")
        }
        if passportfile != nil{
            aCoder.encode(passportfile, forKey: "Passportfile")
        }
        if passportfirstname != nil{
            aCoder.encode(passportfirstname, forKey: "Passportfirstname")
        }
        if passportGender != nil{
            aCoder.encode(passportGender, forKey: "PassportGender")
        }
        if passportMRZ1 != nil{
            aCoder.encode(passportMRZ1, forKey: "PassportMRZ1")
        }
        if passportMRZ2 != nil{
            aCoder.encode(passportMRZ2, forKey: "PassportMRZ2")
        }
        if passportNo != nil{
            aCoder.encode(passportNo, forKey: "passportNo")
        }
        if passportsurname != nil{
            aCoder.encode(passportsurname, forKey: "Passportsurname")
        }
        if passporttype != nil{
            aCoder.encode(passporttype, forKey: "Passporttype")
        }
        if passportverify != nil{
            aCoder.encode(passportverify, forKey: "Passportverify")
        }
        if permanantAddress != nil{
            aCoder.encode(permanantAddress, forKey: "PermanantAddress")
        }
        if phone1 != nil{
            aCoder.encode(phone1, forKey: "Phone1")
        }
        if pincode != nil{
            aCoder.encode(pincode, forKey: "pincode")
        }
        if pincodedelivery != nil{
            aCoder.encode(pincodedelivery, forKey: "pincodedelivery")
        }
        if rentallimit != nil{
            aCoder.encode(rentallimit, forKey: "rentallimit")
        }
        if sameaddress != nil{
            aCoder.encode(sameaddress, forKey: "sameaddress")
        }
        if state != nil{
            aCoder.encode(state, forKey: "state")
        }
        if statedelivery != nil{
            aCoder.encode(statedelivery, forKey: "statedelivery")
        }
    }
}
