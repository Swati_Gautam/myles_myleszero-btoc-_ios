//
//  UserProfileInfoRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 14, 2020

import Foundation


class UserProfileInfoRootClass : NSObject, NSCoding{

    var count : AnyObject!
    var message : String!
    var response : [UserProfileInfoResponse]!
    var status : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        count = dictionary["count"] as? AnyObject
        message = dictionary["message"] as? String
        status = dictionary["status"] as? Int
        response = [UserProfileInfoResponse]()
        if let responseArray = dictionary["response"] as? [[String:Any]]{
            for dic in responseArray{
                let value = UserProfileInfoResponse(fromDictionary: dic)
                response.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if count != nil{
            dictionary["count"] = count
        }
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if response != nil{
            var dictionaryElements = [[String:Any]]()
            for responseElement in response {
                dictionaryElements.append(responseElement.toDictionary())
            }
            dictionary["response"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        count = aDecoder.decodeObject(forKey: "count") as? AnyObject
        message = aDecoder.decodeObject(forKey: "message") as? String
        response = aDecoder.decodeObject(forKey: "response") as? [UserProfileInfoResponse]
        status = aDecoder.decodeObject(forKey: "status") as? Int
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if count != nil{
            aCoder.encode(count, forKey: "count")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if response != nil{
            aCoder.encode(response, forKey: "response")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }
}