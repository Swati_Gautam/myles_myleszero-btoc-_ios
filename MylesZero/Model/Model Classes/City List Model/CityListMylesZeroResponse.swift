//
//  CityListMylesZeroResponse.swift
//  Myles
//
//  Created by Swati Gautam on 08/03/21.
//  Copyright © 2021 Myles. All rights reserved.
//

import UIKit

class CityListMylesZeroResponse: NSObject, NSCoding {

    var cityId : String!
    var cityName : String!
    var cityShortName : String!
    var isHomePickUpDropOffAvailable : Int!
    var latitude : String!
    var longitude : String!
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [CityListMylesZeroResponse]
    {
        var models:[CityListMylesZeroResponse] = []
        for item in array
        {
            models.append(CityListMylesZeroResponse(fromDictionary: (item as! NSDictionary) as! [String : Any]))
        }
        return models
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        cityId = dictionary["CityId"] as? String
        cityName = dictionary["CityName"] as? String
        cityShortName = dictionary["CityShortName"] as? String
        isHomePickUpDropOffAvailable = dictionary["IsHomePickUpDropOffAvailable"] as? Int
        latitude = dictionary["Latitude"] as? String
        longitude = dictionary["Longitude"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cityId != nil{
            dictionary["CityId"] = cityId
        }
        if cityName != nil{
            dictionary["CityName"] = cityName
        }
        if cityShortName != nil{
            dictionary["CityShortName"] = cityShortName
        }
        if isHomePickUpDropOffAvailable != nil{
            dictionary["IsHomePickUpDropOffAvailable"] = isHomePickUpDropOffAvailable
        }
        if latitude != nil{
            dictionary["Latitude"] = latitude
        }
        if longitude != nil{
            dictionary["Longitude"] = longitude
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cityId = aDecoder.decodeObject(forKey: "CityId") as? String
        cityName = aDecoder.decodeObject(forKey: "CityName") as? String
        cityShortName = aDecoder.decodeObject(forKey: "CityShortName") as? String
        isHomePickUpDropOffAvailable = aDecoder.decodeObject(forKey: "IsHomePickUpDropOffAvailable") as? Int
        latitude = aDecoder.decodeObject(forKey: "Latitude") as? String
        longitude = aDecoder.decodeObject(forKey: "Longitude") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cityId != nil{
            aCoder.encode(cityId, forKey: "CityId")
        }
        if cityName != nil{
            aCoder.encode(cityName, forKey: "CityName")
        }
        if cityShortName != nil{
            aCoder.encode(cityShortName, forKey: "CityShortName")
        }
        if isHomePickUpDropOffAvailable != nil{
            aCoder.encode(isHomePickUpDropOffAvailable, forKey: "IsHomePickUpDropOffAvailable")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "Latitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "Longitude")
        }
    }
}
