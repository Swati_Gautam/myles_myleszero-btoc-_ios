//
//  StateList.swift
//  Myles Zero
//
//  Created by Chetan Rajauria on 09/09/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class StateList: NSObject
{
    var stateCode : String!
    var stateName : String!
    
    init(fromDictionary dictionary: [String:Any])
    {
        stateCode = dictionary["StateCode"] as? String
        stateName = dictionary["StateName"] as? String
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if stateCode != nil{
            dictionary["StateCode"] = stateCode
        }
        if stateName != nil{
            dictionary["StateName"] = stateName
        }
        return dictionary
    }

}
