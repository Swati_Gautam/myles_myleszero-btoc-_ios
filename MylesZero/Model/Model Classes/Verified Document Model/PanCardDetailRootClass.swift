//
//  PanCardDetailRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on July 15, 2020

import Foundation


class PanCardDetailRootClass : NSObject, NSCoding{

    var lastUpdate : String!
    var name : String!
    var nameOnTheCard : AnyObject!
    var panNumber : String!
    var sourceId : Int!
    var sTATUS : String!
    var statusDescription : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        lastUpdate = dictionary["LastUpdate"] as? String
        name = dictionary["Name"] as? String
        nameOnTheCard = dictionary["NameOnTheCard"] as? AnyObject
        panNumber = dictionary["PanNumber"] as? String
        sourceId = dictionary["source_id"] as? Int
        sTATUS = dictionary["STATUS"] as? String
        statusDescription = dictionary["StatusDescription"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if lastUpdate != nil{
            dictionary["LastUpdate"] = lastUpdate
        }
        if name != nil{
            dictionary["Name"] = name
        }
        if nameOnTheCard != nil{
            dictionary["NameOnTheCard"] = nameOnTheCard
        }
        if panNumber != nil{
            dictionary["PanNumber"] = panNumber
        }
        if sourceId != nil{
            dictionary["source_id"] = sourceId
        }
        if sTATUS != nil{
            dictionary["STATUS"] = sTATUS
        }
        if statusDescription != nil{
            dictionary["StatusDescription"] = statusDescription
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        lastUpdate = aDecoder.decodeObject(forKey: "LastUpdate") as? String
        name = aDecoder.decodeObject(forKey: "Name") as? String
        nameOnTheCard = aDecoder.decodeObject(forKey: "NameOnTheCard") as? AnyObject
        panNumber = aDecoder.decodeObject(forKey: "PanNumber") as? String
        sourceId = aDecoder.decodeObject(forKey: "source_id") as? Int
        sTATUS = aDecoder.decodeObject(forKey: "STATUS") as? String
        statusDescription = aDecoder.decodeObject(forKey: "StatusDescription") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if lastUpdate != nil{
            aCoder.encode(lastUpdate, forKey: "LastUpdate")
        }
        if name != nil{
            aCoder.encode(name, forKey: "Name")
        }
        if nameOnTheCard != nil{
            aCoder.encode(nameOnTheCard, forKey: "NameOnTheCard")
        }
        if panNumber != nil{
            aCoder.encode(panNumber, forKey: "PanNumber")
        }
        if sourceId != nil{
            aCoder.encode(sourceId, forKey: "source_id")
        }
        if sTATUS != nil{
            aCoder.encode(sTATUS, forKey: "STATUS")
        }
        if statusDescription != nil{
            aCoder.encode(statusDescription, forKey: "StatusDescription")
        }
    }
}