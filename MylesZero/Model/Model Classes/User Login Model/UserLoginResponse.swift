//
//  UserLoginResponse.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 10, 2019

import Foundation


class UserLoginResponse : NSObject, NSCoding{

    var canApproveSubscriptionRequests : String!
    var clientcoID : String!
    var clientID : String!
    var clientType : String!
    var dOB : String!
    var emailId : String!
    var fname : String!
    var isAdminForSubscription : String!
    var isAgreement : AnyObject!
    var iscompanySI : AnyObject!
    var isSI : AnyObject!
    var lname : String!
    var phone : String!
    var rentallimit : String!
    var subscribe : AnyObject!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        canApproveSubscriptionRequests = dictionary["CanApproveSubscriptionRequests"] as? String
        clientcoID = dictionary["ClientcoID"] as? String
        clientID = dictionary["ClientID"] as? String
        clientType = dictionary["ClientType"] as? String
        dOB = dictionary["DOB"] as? String
        emailId = dictionary["emailId"] as? String
        fname = dictionary["fname"] as? String
        isAdminForSubscription = dictionary["IsAdminForSubscription"] as? String
        isAgreement = dictionary["IsAgreement"] as? AnyObject
        iscompanySI = dictionary["IscompanySI"] as? AnyObject
        isSI = dictionary["IsSI"] as? AnyObject
        lname = dictionary["lname"] as? String
        phone = dictionary["phone"] as? String
        rentallimit = dictionary["rentallimit"] as? String
        subscribe = dictionary["subscribe"] as? AnyObject
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if canApproveSubscriptionRequests != nil{
            dictionary["CanApproveSubscriptionRequests"] = canApproveSubscriptionRequests
        }
        if clientcoID != nil{
            dictionary["ClientcoID"] = clientcoID
        }
        if clientID != nil{
            dictionary["ClientID"] = clientID
        }
        if clientType != nil{
            dictionary["ClientType"] = clientType
        }
        if dOB != nil{
            dictionary["DOB"] = dOB
        }
        if emailId != nil{
            dictionary["emailId"] = emailId
        }
        if fname != nil{
            dictionary["fname"] = fname
        }
        if isAdminForSubscription != nil{
            dictionary["IsAdminForSubscription"] = isAdminForSubscription
        }
        if isAgreement != nil{
            dictionary["IsAgreement"] = isAgreement
        }
        if iscompanySI != nil{
            dictionary["IscompanySI"] = iscompanySI
        }
        if isSI != nil{
            dictionary["IsSI"] = isSI
        }
        if lname != nil{
            dictionary["lname"] = lname
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if rentallimit != nil{
            dictionary["rentallimit"] = rentallimit
        }
        if subscribe != nil{
            dictionary["subscribe"] = subscribe
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        canApproveSubscriptionRequests = aDecoder.decodeObject(forKey: "CanApproveSubscriptionRequests") as? String
        clientcoID = aDecoder.decodeObject(forKey: "ClientcoID") as? String
        clientID = aDecoder.decodeObject(forKey: "ClientID") as? String
        clientType = aDecoder.decodeObject(forKey: "ClientType") as? String
        dOB = aDecoder.decodeObject(forKey: "DOB") as? String
        emailId = aDecoder.decodeObject(forKey: "emailId") as? String
        fname = aDecoder.decodeObject(forKey: "fname") as? String
        isAdminForSubscription = aDecoder.decodeObject(forKey: "IsAdminForSubscription") as? String
        isAgreement = aDecoder.decodeObject(forKey: "IsAgreement") as? AnyObject
        iscompanySI = aDecoder.decodeObject(forKey: "IscompanySI") as? AnyObject
        isSI = aDecoder.decodeObject(forKey: "IsSI") as? AnyObject
        lname = aDecoder.decodeObject(forKey: "lname") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        rentallimit = aDecoder.decodeObject(forKey: "rentallimit") as? String
        subscribe = aDecoder.decodeObject(forKey: "subscribe") as? AnyObject
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if canApproveSubscriptionRequests != nil{
            aCoder.encode(canApproveSubscriptionRequests, forKey: "CanApproveSubscriptionRequests")
        }
        if clientcoID != nil{
            aCoder.encode(clientcoID, forKey: "ClientcoID")
        }
        if clientID != nil{
            aCoder.encode(clientID, forKey: "ClientID")
        }
        if clientType != nil{
            aCoder.encode(clientType, forKey: "ClientType")
        }
        if dOB != nil{
            aCoder.encode(dOB, forKey: "DOB")
        }
        if emailId != nil{
            aCoder.encode(emailId, forKey: "emailId")
        }
        if fname != nil{
            aCoder.encode(fname, forKey: "fname")
        }
        if isAdminForSubscription != nil{
            aCoder.encode(isAdminForSubscription, forKey: "IsAdminForSubscription")
        }
        if isAgreement != nil{
            aCoder.encode(isAgreement, forKey: "IsAgreement")
        }
        if iscompanySI != nil{
            aCoder.encode(iscompanySI, forKey: "IscompanySI")
        }
        if isSI != nil{
            aCoder.encode(isSI, forKey: "IsSI")
        }
        if lname != nil{
            aCoder.encode(lname, forKey: "lname")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if rentallimit != nil{
            aCoder.encode(rentallimit, forKey: "rentallimit")
        }
        if subscribe != nil{
            aCoder.encode(subscribe, forKey: "subscribe")
        }
    }
}