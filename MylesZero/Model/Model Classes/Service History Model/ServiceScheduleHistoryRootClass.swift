//
//  ServiceScheduleHistoryRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 28, 2021

import Foundation


class ServiceScheduleHistoryRootClass : NSObject, NSCoding{

    var approvedBy : String!
    var carID : Int!
    var isActive : String!
    var requestedBy : String!
    var scheduledServiceDate : String!
    var serviceKm : String!
    var serviceMonth : String!
    var serviceScheduleID : String!
    var serviceType : String!
    var serviceYN : String!
    var slots : [ServiceScheduleHistorySlot]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        approvedBy = dictionary["ApprovedBy"] as? String
        carID = dictionary["CarID"] as? Int
        isActive = dictionary["IsActive"] as? String
        requestedBy = dictionary["RequestedBy"] as? String
        scheduledServiceDate = dictionary["ScheduledServiceDate"] as? String
        serviceKm = dictionary["ServiceKm"] as? String
        serviceMonth = dictionary["ServiceMonth"] as? String
        serviceScheduleID = dictionary["ServiceScheduleID"] as? String
        serviceType = dictionary["ServiceType"] as? String
        serviceYN = dictionary["ServiceYN"] as? String
        slots = [ServiceScheduleHistorySlot]()
        if let slotsArray = dictionary["slots"] as? [[String:Any]]{
            for dic in slotsArray{
                let value = ServiceScheduleHistorySlot(fromDictionary: dic)
                slots.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if approvedBy != nil{
            dictionary["ApprovedBy"] = approvedBy
        }
        if carID != nil{
            dictionary["CarID"] = carID
        }
        if isActive != nil{
            dictionary["IsActive"] = isActive
        }
        if requestedBy != nil{
            dictionary["RequestedBy"] = requestedBy
        }
        if scheduledServiceDate != nil{
            dictionary["ScheduledServiceDate"] = scheduledServiceDate
        }
        if serviceKm != nil{
            dictionary["ServiceKm"] = serviceKm
        }
        if serviceMonth != nil{
            dictionary["ServiceMonth"] = serviceMonth
        }
        if serviceScheduleID != nil{
            dictionary["ServiceScheduleID"] = serviceScheduleID
        }
        if serviceType != nil{
            dictionary["ServiceType"] = serviceType
        }
        if serviceYN != nil{
            dictionary["ServiceYN"] = serviceYN
        }
        if slots != nil{
            var dictionaryElements = [[String:Any]]()
            for slotsElement in slots {
                dictionaryElements.append(slotsElement.toDictionary())
            }
            dictionary["slots"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        approvedBy = aDecoder.decodeObject(forKey: "ApprovedBy") as? String
        carID = aDecoder.decodeObject(forKey: "CarID") as? Int
        isActive = aDecoder.decodeObject(forKey: "IsActive") as? String
        requestedBy = aDecoder.decodeObject(forKey: "RequestedBy") as? String
        scheduledServiceDate = aDecoder.decodeObject(forKey: "ScheduledServiceDate") as? String
        serviceKm = aDecoder.decodeObject(forKey: "ServiceKm") as? String
        serviceMonth = aDecoder.decodeObject(forKey: "ServiceMonth") as? String
        serviceScheduleID = aDecoder.decodeObject(forKey: "ServiceScheduleID") as? String
        serviceType = aDecoder.decodeObject(forKey: "ServiceType") as? String
        serviceYN = aDecoder.decodeObject(forKey: "ServiceYN") as? String
        slots = aDecoder.decodeObject(forKey: "slots") as? [ServiceScheduleHistorySlot]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if approvedBy != nil{
            aCoder.encode(approvedBy, forKey: "ApprovedBy")
        }
        if carID != nil{
            aCoder.encode(carID, forKey: "CarID")
        }
        if isActive != nil{
            aCoder.encode(isActive, forKey: "IsActive")
        }
        if requestedBy != nil{
            aCoder.encode(requestedBy, forKey: "RequestedBy")
        }
        if scheduledServiceDate != nil{
            aCoder.encode(scheduledServiceDate, forKey: "ScheduledServiceDate")
        }
        if serviceKm != nil{
            aCoder.encode(serviceKm, forKey: "ServiceKm")
        }
        if serviceMonth != nil{
            aCoder.encode(serviceMonth, forKey: "ServiceMonth")
        }
        if serviceScheduleID != nil{
            aCoder.encode(serviceScheduleID, forKey: "ServiceScheduleID")
        }
        if serviceType != nil{
            aCoder.encode(serviceType, forKey: "ServiceType")
        }
        if serviceYN != nil{
            aCoder.encode(serviceYN, forKey: "ServiceYN")
        }
        if slots != nil{
            aCoder.encode(slots, forKey: "slots")
        }
    }
}