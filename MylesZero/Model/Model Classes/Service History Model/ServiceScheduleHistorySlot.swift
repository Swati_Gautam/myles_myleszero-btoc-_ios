//
//  ServiceScheduleHistorySlot.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 28, 2021

import Foundation


class ServiceScheduleHistorySlot : NSObject, NSCoding{

    var dateSlot : String!
    var timeSlot : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dateSlot = dictionary["DateSlot"] as? String
        timeSlot = dictionary["TimeSlot"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dateSlot != nil{
            dictionary["DateSlot"] = dateSlot
        }
        if timeSlot != nil{
            dictionary["TimeSlot"] = timeSlot
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        dateSlot = aDecoder.decodeObject(forKey: "DateSlot") as? String
        timeSlot = aDecoder.decodeObject(forKey: "TimeSlot") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if dateSlot != nil{
            aCoder.encode(dateSlot, forKey: "DateSlot")
        }
        if timeSlot != nil{
            aCoder.encode(timeSlot, forKey: "TimeSlot")
        }
    }
}