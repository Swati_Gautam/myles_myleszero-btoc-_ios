//
//  SubscriptionStatusDetailDoc.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 7, 2021

import Foundation


class SubscriptionStatusDetailDoc : NSObject, NSCoding{

    var doctype : String!
    var url : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        doctype = dictionary["Doctype"] as? String
        url = dictionary["Url"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if doctype != nil{
            dictionary["Doctype"] = doctype
        }
        if url != nil{
            dictionary["Url"] = url
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        doctype = aDecoder.decodeObject(forKey: "Doctype") as? String
        url = aDecoder.decodeObject(forKey: "Url") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if doctype != nil{
            aCoder.encode(doctype, forKey: "Doctype")
        }
        if url != nil{
            aCoder.encode(url, forKey: "Url")
        }
    }
}