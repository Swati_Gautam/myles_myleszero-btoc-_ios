//
//  SubscriptionStatusDetailRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 7, 2021

import Foundation


class SubscriptionStatusDetailRootClass : NSObject, NSCoding{

    var additionalExp : String!
    var adharNo : String!
    var advancePaymentAmt : String!
    var agreementupload : String!
    var agreementUrl : String!
    var beneficiaryAccno : String!
    var carAge : String!
    var carColor : String!
    var carCompName : String!
    var carId : String!
    var carImage : String!
    var carModelId : String!
    var carModelName : String!
    var carProviderAddressOne : String!
    var carProviderAddressSecond : String!
    var carProviderName : String!
    var carSubscriptionStatus : String!
    var cartype : String!
    var carVariantId : String!
    var carVariantName : String!
    var cityId : String!
    var cityName : String!
    var clientID : String!
    var contact : String!
    var coricId : String!
    var createDate : String!
    var depositAmt : String!
    var dLNo : String!
    var doc : [SubscriptionStatusDetailDoc]!
    var earlyClosure : [SubscriptionStatusDetailEarlyClosure]!
    var emailID : String!
    var employeeName : String!
    var extraKmRate : String!
    var fuelType : String!
    var indicatedMonthlyRental : String!
    var invoice : AnyObject!
    var km : String!
    var kmRun : String!
    var maxAmtLimit : String!
    var panaltyAmt : String!
    var panNo : String!
    var passportNo : String!
    var paymentAmt : String!
    var paymentDetails : AnyObject!
    var permanentAddress : String!
    var pickUpDate : String!
    var pkgId : String!
    var profileDetails : [SubscriptionStatusDetailProfileDetail]!
    var purchaseYear : String!
    var regnno : String!
    var rentallimit : String!
    var requestDate : String!
    var requestId : String!
    var seatingCapacity : String!
    var serviceDetails : [SubscriptionStatusDetailServiceDetail]!
    var subscriberAccno : String!
    var subscriptionApprovedDate : String!
    var subscriptionBookingid : String!
    var subscriptionEndDate : String!
    var subscriptionId : String!
    var subscriptionRequestDate : String!
    var subscriptionTenureMonths : String!
    var supportingDocumentPath : String!
    var taxAmount : String!
    var tentativeDeliveryDate : String!
    var terminationDetails : AnyObject!
    var toBePaidAmt : String!
    var transmission : String!
    var upComingService : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        additionalExp = dictionary["AdditionalExp"] as? String
        adharNo = dictionary["AdharNo"] as? String
        advancePaymentAmt = dictionary["AdvancePaymentAmt"] as? String
        agreementupload = dictionary["Agreementupload"] as? String
        agreementUrl = dictionary["AgreementUrl"] as? String
        beneficiaryAccno = dictionary["beneficiary_accno"] as? String
        carAge = dictionary["CarAge"] as? String
        carColor = dictionary["CarColor"] as? String
        carCompName = dictionary["CarCompName"] as? String
        carId = dictionary["CarId"] as? String
        carImage = dictionary["CarImage"] as? String
        carModelId = dictionary["CarModelId"] as? String
        carModelName = dictionary["CarModelName"] as? String
        carProviderAddressOne = dictionary["CarProviderAddressOne"] as? String
        carProviderAddressSecond = dictionary["CarProviderAddressSecond"] as? String
        carProviderName = dictionary["CarProviderName"] as? String
        carSubscriptionStatus = dictionary["CarSubscriptionStatus"] as? String
        cartype = dictionary["Cartype"] as? String
        carVariantId = dictionary["CarVariantId"] as? String
        carVariantName = dictionary["CarVariantName"] as? String
        cityId = dictionary["CityId"] as? String
        cityName = dictionary["CityName"] as? String
        clientID = dictionary["ClientID"] as? String
        contact = dictionary["Contact"] as? String
        coricId = dictionary["CoricId"] as? String
        createDate = dictionary["CreateDate"] as? String
        depositAmt = dictionary["DepositAmt"] as? String
        dLNo = dictionary["DLNo"] as? String
        emailID = dictionary["EmailID"] as? String
        employeeName = dictionary["EmployeeName"] as? String
        extraKmRate = dictionary["ExtraKmRate"] as? String
        fuelType = dictionary["FuelType"] as? String
        indicatedMonthlyRental = dictionary["IndicatedMonthlyRental"] as? String
        invoice = dictionary["Invoice"] as? AnyObject
        km = dictionary["Km"] as? String
        kmRun = dictionary["KmRun"] as? String
        maxAmtLimit = dictionary["MaxAmtLimit"] as? String
        panaltyAmt = dictionary["PanaltyAmt"] as? String
        panNo = dictionary["PanNo"] as? String
        passportNo = dictionary["PassportNo"] as? String
        paymentAmt = dictionary["PaymentAmt"] as? String
        paymentDetails = dictionary["PaymentDetails"] as? AnyObject
        permanentAddress = dictionary["permanentAddress"] as? String
        pickUpDate = dictionary["PickUpDate"] as? String
        pkgId = dictionary["PkgId"] as? String
        purchaseYear = dictionary["PurchaseYear"] as? String
        regnno = dictionary["Regnno"] as? String
        rentallimit = dictionary["rentallimit"] as? String
        requestDate = dictionary["RequestDate"] as? String
        requestId = dictionary["RequestId"] as? String
        seatingCapacity = dictionary["seatingCapacity"] as? String
        subscriberAccno = dictionary["subscriber_accno"] as? String
        subscriptionApprovedDate = dictionary["SubscriptionApprovedDate"] as? String
        subscriptionBookingid = dictionary["SubscriptionBookingid"] as? String
        subscriptionEndDate = dictionary["SubscriptionEndDate"] as? String
        subscriptionId = dictionary["SubscriptionId"] as? String
        subscriptionRequestDate = dictionary["SubscriptionRequestDate"] as? String
        subscriptionTenureMonths = dictionary["SubscriptionTenureMonths"] as? String
        supportingDocumentPath = dictionary["SupportingDocumentPath"] as? String
        taxAmount = dictionary["TaxAmount"] as? String
        tentativeDeliveryDate = dictionary["TentativeDeliveryDate"] as? String
        terminationDetails = dictionary["TerminationDetails"] as? AnyObject
        toBePaidAmt = dictionary["ToBePaidAmt"] as? String
        transmission = dictionary["Transmission"] as? String
        upComingService = dictionary["UpComingService"] as? String
        doc = [SubscriptionStatusDetailDoc]()
        if let docArray = dictionary["Doc"] as? [[String:Any]]{
            for dic in docArray{
                let value = SubscriptionStatusDetailDoc(fromDictionary: dic)
                doc.append(value)
            }
        }
        earlyClosure = [SubscriptionStatusDetailEarlyClosure]()
        if let earlyClosureArray = dictionary["EarlyClosure"] as? [[String:Any]]{
            for dic in earlyClosureArray{
                let value = SubscriptionStatusDetailEarlyClosure(fromDictionary: dic)
                earlyClosure.append(value)
            }
        }
        profileDetails = [SubscriptionStatusDetailProfileDetail]()
        if let profileDetailsArray = dictionary["ProfileDetails"] as? [[String:Any]]{
            for dic in profileDetailsArray{
                let value = SubscriptionStatusDetailProfileDetail(fromDictionary: dic)
                profileDetails.append(value)
            }
        }
        serviceDetails = [SubscriptionStatusDetailServiceDetail]()
        if let serviceDetailsArray = dictionary["ServiceDetails"] as? [[String:Any]]{
            for dic in serviceDetailsArray{
                let value = SubscriptionStatusDetailServiceDetail(fromDictionary: dic)
                serviceDetails.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if additionalExp != nil{
            dictionary["AdditionalExp"] = additionalExp
        }
        if adharNo != nil{
            dictionary["AdharNo"] = adharNo
        }
        if advancePaymentAmt != nil{
            dictionary["AdvancePaymentAmt"] = advancePaymentAmt
        }
        if agreementupload != nil{
            dictionary["Agreementupload"] = agreementupload
        }
        if agreementUrl != nil{
            dictionary["AgreementUrl"] = agreementUrl
        }
        if beneficiaryAccno != nil{
            dictionary["beneficiary_accno"] = beneficiaryAccno
        }
        if carAge != nil{
            dictionary["CarAge"] = carAge
        }
        if carColor != nil{
            dictionary["CarColor"] = carColor
        }
        if carCompName != nil{
            dictionary["CarCompName"] = carCompName
        }
        if carId != nil{
            dictionary["CarId"] = carId
        }
        if carImage != nil{
            dictionary["CarImage"] = carImage
        }
        if carModelId != nil{
            dictionary["CarModelId"] = carModelId
        }
        if carModelName != nil{
            dictionary["CarModelName"] = carModelName
        }
        if carProviderAddressOne != nil{
            dictionary["CarProviderAddressOne"] = carProviderAddressOne
        }
        if carProviderAddressSecond != nil{
            dictionary["CarProviderAddressSecond"] = carProviderAddressSecond
        }
        if carProviderName != nil{
            dictionary["CarProviderName"] = carProviderName
        }
        if carSubscriptionStatus != nil{
            dictionary["CarSubscriptionStatus"] = carSubscriptionStatus
        }
        if cartype != nil{
            dictionary["Cartype"] = cartype
        }
        if carVariantId != nil{
            dictionary["CarVariantId"] = carVariantId
        }
        if carVariantName != nil{
            dictionary["CarVariantName"] = carVariantName
        }
        if cityId != nil{
            dictionary["CityId"] = cityId
        }
        if cityName != nil{
            dictionary["CityName"] = cityName
        }
        if clientID != nil{
            dictionary["ClientID"] = clientID
        }
        if contact != nil{
            dictionary["Contact"] = contact
        }
        if coricId != nil{
            dictionary["CoricId"] = coricId
        }
        if createDate != nil{
            dictionary["CreateDate"] = createDate
        }
        if depositAmt != nil{
            dictionary["DepositAmt"] = depositAmt
        }
        if dLNo != nil{
            dictionary["DLNo"] = dLNo
        }
        if emailID != nil{
            dictionary["EmailID"] = emailID
        }
        if employeeName != nil{
            dictionary["EmployeeName"] = employeeName
        }
        if extraKmRate != nil{
            dictionary["ExtraKmRate"] = extraKmRate
        }
        if fuelType != nil{
            dictionary["FuelType"] = fuelType
        }
        if indicatedMonthlyRental != nil{
            dictionary["IndicatedMonthlyRental"] = indicatedMonthlyRental
        }
        if invoice != nil{
            dictionary["Invoice"] = invoice
        }
        if km != nil{
            dictionary["Km"] = km
        }
        if kmRun != nil{
            dictionary["KmRun"] = kmRun
        }
        if maxAmtLimit != nil{
            dictionary["MaxAmtLimit"] = maxAmtLimit
        }
        if panaltyAmt != nil{
            dictionary["PanaltyAmt"] = panaltyAmt
        }
        if panNo != nil{
            dictionary["PanNo"] = panNo
        }
        if passportNo != nil{
            dictionary["PassportNo"] = passportNo
        }
        if paymentAmt != nil{
            dictionary["PaymentAmt"] = paymentAmt
        }
        if paymentDetails != nil{
            dictionary["PaymentDetails"] = paymentDetails
        }
        if permanentAddress != nil{
            dictionary["permanentAddress"] = permanentAddress
        }
        if pickUpDate != nil{
            dictionary["PickUpDate"] = pickUpDate
        }
        if pkgId != nil{
            dictionary["PkgId"] = pkgId
        }
        if purchaseYear != nil{
            dictionary["PurchaseYear"] = purchaseYear
        }
        if regnno != nil{
            dictionary["Regnno"] = regnno
        }
        if rentallimit != nil{
            dictionary["rentallimit"] = rentallimit
        }
        if requestDate != nil{
            dictionary["RequestDate"] = requestDate
        }
        if requestId != nil{
            dictionary["RequestId"] = requestId
        }
        if seatingCapacity != nil{
            dictionary["seatingCapacity"] = seatingCapacity
        }
        if subscriberAccno != nil{
            dictionary["subscriber_accno"] = subscriberAccno
        }
        if subscriptionApprovedDate != nil{
            dictionary["SubscriptionApprovedDate"] = subscriptionApprovedDate
        }
        if subscriptionBookingid != nil{
            dictionary["SubscriptionBookingid"] = subscriptionBookingid
        }
        if subscriptionEndDate != nil{
            dictionary["SubscriptionEndDate"] = subscriptionEndDate
        }
        if subscriptionId != nil{
            dictionary["SubscriptionId"] = subscriptionId
        }
        if subscriptionRequestDate != nil{
            dictionary["SubscriptionRequestDate"] = subscriptionRequestDate
        }
        if subscriptionTenureMonths != nil{
            dictionary["SubscriptionTenureMonths"] = subscriptionTenureMonths
        }
        if supportingDocumentPath != nil{
            dictionary["SupportingDocumentPath"] = supportingDocumentPath
        }
        if taxAmount != nil{
            dictionary["TaxAmount"] = taxAmount
        }
        if tentativeDeliveryDate != nil{
            dictionary["TentativeDeliveryDate"] = tentativeDeliveryDate
        }
        if terminationDetails != nil{
            dictionary["TerminationDetails"] = terminationDetails
        }
        if toBePaidAmt != nil{
            dictionary["ToBePaidAmt"] = toBePaidAmt
        }
        if transmission != nil{
            dictionary["Transmission"] = transmission
        }
        if upComingService != nil{
            dictionary["UpComingService"] = upComingService
        }
        if doc != nil{
            var dictionaryElements = [[String:Any]]()
            for docElement in doc {
                dictionaryElements.append(docElement.toDictionary())
            }
            dictionary["doc"] = dictionaryElements
        }
        if earlyClosure != nil{
            var dictionaryElements = [[String:Any]]()
            for earlyClosureElement in earlyClosure {
                dictionaryElements.append(earlyClosureElement.toDictionary())
            }
            dictionary["earlyClosure"] = dictionaryElements
        }
        if profileDetails != nil{
            var dictionaryElements = [[String:Any]]()
            for profileDetailsElement in profileDetails {
                dictionaryElements.append(profileDetailsElement.toDictionary())
            }
            dictionary["profileDetails"] = dictionaryElements
        }
        if serviceDetails != nil{
            var dictionaryElements = [[String:Any]]()
            for serviceDetailsElement in serviceDetails {
                dictionaryElements.append(serviceDetailsElement.toDictionary())
            }
            dictionary["serviceDetails"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        additionalExp = aDecoder.decodeObject(forKey: "AdditionalExp") as? String
        adharNo = aDecoder.decodeObject(forKey: "AdharNo") as? String
        advancePaymentAmt = aDecoder.decodeObject(forKey: "AdvancePaymentAmt") as? String
        agreementupload = aDecoder.decodeObject(forKey: "Agreementupload") as? String
        agreementUrl = aDecoder.decodeObject(forKey: "AgreementUrl") as? String
        beneficiaryAccno = aDecoder.decodeObject(forKey: "beneficiary_accno") as? String
        carAge = aDecoder.decodeObject(forKey: "CarAge") as? String
        carColor = aDecoder.decodeObject(forKey: "CarColor") as? String
        carCompName = aDecoder.decodeObject(forKey: "CarCompName") as? String
        carId = aDecoder.decodeObject(forKey: "CarId") as? String
        carImage = aDecoder.decodeObject(forKey: "CarImage") as? String
        carModelId = aDecoder.decodeObject(forKey: "CarModelId") as? String
        carModelName = aDecoder.decodeObject(forKey: "CarModelName") as? String
        carProviderAddressOne = aDecoder.decodeObject(forKey: "CarProviderAddressOne") as? String
        carProviderAddressSecond = aDecoder.decodeObject(forKey: "CarProviderAddressSecond") as? String
        carProviderName = aDecoder.decodeObject(forKey: "CarProviderName") as? String
        carSubscriptionStatus = aDecoder.decodeObject(forKey: "CarSubscriptionStatus") as? String
        cartype = aDecoder.decodeObject(forKey: "Cartype") as? String
        carVariantId = aDecoder.decodeObject(forKey: "CarVariantId") as? String
        carVariantName = aDecoder.decodeObject(forKey: "CarVariantName") as? String
        cityId = aDecoder.decodeObject(forKey: "CityId") as? String
        cityName = aDecoder.decodeObject(forKey: "CityName") as? String
        clientID = aDecoder.decodeObject(forKey: "ClientID") as? String
        contact = aDecoder.decodeObject(forKey: "Contact") as? String
        coricId = aDecoder.decodeObject(forKey: "CoricId") as? String
        createDate = aDecoder.decodeObject(forKey: "CreateDate") as? String
        depositAmt = aDecoder.decodeObject(forKey: "DepositAmt") as? String
        dLNo = aDecoder.decodeObject(forKey: "DLNo") as? String
        doc = aDecoder.decodeObject(forKey: "Doc") as? [SubscriptionStatusDetailDoc]
        earlyClosure = aDecoder.decodeObject(forKey: "EarlyClosure") as? [SubscriptionStatusDetailEarlyClosure]
        emailID = aDecoder.decodeObject(forKey: "EmailID") as? String
        employeeName = aDecoder.decodeObject(forKey: "EmployeeName") as? String
        extraKmRate = aDecoder.decodeObject(forKey: "ExtraKmRate") as? String
        fuelType = aDecoder.decodeObject(forKey: "FuelType") as? String
        indicatedMonthlyRental = aDecoder.decodeObject(forKey: "IndicatedMonthlyRental") as? String
        invoice = aDecoder.decodeObject(forKey: "Invoice") as? AnyObject
        km = aDecoder.decodeObject(forKey: "Km") as? String
        kmRun = aDecoder.decodeObject(forKey: "KmRun") as? String
        maxAmtLimit = aDecoder.decodeObject(forKey: "MaxAmtLimit") as? String
        panaltyAmt = aDecoder.decodeObject(forKey: "PanaltyAmt") as? String
        panNo = aDecoder.decodeObject(forKey: "PanNo") as? String
        passportNo = aDecoder.decodeObject(forKey: "PassportNo") as? String
        paymentAmt = aDecoder.decodeObject(forKey: "PaymentAmt") as? String
        paymentDetails = aDecoder.decodeObject(forKey: "PaymentDetails") as? AnyObject
        permanentAddress = aDecoder.decodeObject(forKey: "permanentAddress") as? String
        pickUpDate = aDecoder.decodeObject(forKey: "PickUpDate") as? String
        pkgId = aDecoder.decodeObject(forKey: "PkgId") as? String
        profileDetails = aDecoder.decodeObject(forKey: "ProfileDetails") as? [SubscriptionStatusDetailProfileDetail]
        purchaseYear = aDecoder.decodeObject(forKey: "PurchaseYear") as? String
        regnno = aDecoder.decodeObject(forKey: "Regnno") as? String
        rentallimit = aDecoder.decodeObject(forKey: "rentallimit") as? String
        requestDate = aDecoder.decodeObject(forKey: "RequestDate") as? String
        requestId = aDecoder.decodeObject(forKey: "RequestId") as? String
        seatingCapacity = aDecoder.decodeObject(forKey: "seatingCapacity") as? String
        serviceDetails = aDecoder.decodeObject(forKey: "ServiceDetails") as? [SubscriptionStatusDetailServiceDetail]
        subscriberAccno = aDecoder.decodeObject(forKey: "subscriber_accno") as? String
        subscriptionApprovedDate = aDecoder.decodeObject(forKey: "SubscriptionApprovedDate") as? String
        subscriptionBookingid = aDecoder.decodeObject(forKey: "SubscriptionBookingid") as? String
        subscriptionEndDate = aDecoder.decodeObject(forKey: "SubscriptionEndDate") as? String
        subscriptionId = aDecoder.decodeObject(forKey: "SubscriptionId") as? String
        subscriptionRequestDate = aDecoder.decodeObject(forKey: "SubscriptionRequestDate") as? String
        subscriptionTenureMonths = aDecoder.decodeObject(forKey: "SubscriptionTenureMonths") as? String
        supportingDocumentPath = aDecoder.decodeObject(forKey: "SupportingDocumentPath") as? String
        taxAmount = aDecoder.decodeObject(forKey: "TaxAmount") as? String
        tentativeDeliveryDate = aDecoder.decodeObject(forKey: "TentativeDeliveryDate") as? String
        terminationDetails = aDecoder.decodeObject(forKey: "TerminationDetails") as? AnyObject
        toBePaidAmt = aDecoder.decodeObject(forKey: "ToBePaidAmt") as? String
        transmission = aDecoder.decodeObject(forKey: "Transmission") as? String
        upComingService = aDecoder.decodeObject(forKey: "UpComingService") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if additionalExp != nil{
            aCoder.encode(additionalExp, forKey: "AdditionalExp")
        }
        if adharNo != nil{
            aCoder.encode(adharNo, forKey: "AdharNo")
        }
        if advancePaymentAmt != nil{
            aCoder.encode(advancePaymentAmt, forKey: "AdvancePaymentAmt")
        }
        if agreementupload != nil{
            aCoder.encode(agreementupload, forKey: "Agreementupload")
        }
        if agreementUrl != nil{
            aCoder.encode(agreementUrl, forKey: "AgreementUrl")
        }
        if beneficiaryAccno != nil{
            aCoder.encode(beneficiaryAccno, forKey: "beneficiary_accno")
        }
        if carAge != nil{
            aCoder.encode(carAge, forKey: "CarAge")
        }
        if carColor != nil{
            aCoder.encode(carColor, forKey: "CarColor")
        }
        if carCompName != nil{
            aCoder.encode(carCompName, forKey: "CarCompName")
        }
        if carId != nil{
            aCoder.encode(carId, forKey: "CarId")
        }
        if carImage != nil{
            aCoder.encode(carImage, forKey: "CarImage")
        }
        if carModelId != nil{
            aCoder.encode(carModelId, forKey: "CarModelId")
        }
        if carModelName != nil{
            aCoder.encode(carModelName, forKey: "CarModelName")
        }
        if carProviderAddressOne != nil{
            aCoder.encode(carProviderAddressOne, forKey: "CarProviderAddressOne")
        }
        if carProviderAddressSecond != nil{
            aCoder.encode(carProviderAddressSecond, forKey: "CarProviderAddressSecond")
        }
        if carProviderName != nil{
            aCoder.encode(carProviderName, forKey: "CarProviderName")
        }
        if carSubscriptionStatus != nil{
            aCoder.encode(carSubscriptionStatus, forKey: "CarSubscriptionStatus")
        }
        if cartype != nil{
            aCoder.encode(cartype, forKey: "Cartype")
        }
        if carVariantId != nil{
            aCoder.encode(carVariantId, forKey: "CarVariantId")
        }
        if carVariantName != nil{
            aCoder.encode(carVariantName, forKey: "CarVariantName")
        }
        if cityId != nil{
            aCoder.encode(cityId, forKey: "CityId")
        }
        if cityName != nil{
            aCoder.encode(cityName, forKey: "CityName")
        }
        if clientID != nil{
            aCoder.encode(clientID, forKey: "ClientID")
        }
        if contact != nil{
            aCoder.encode(contact, forKey: "Contact")
        }
        if coricId != nil{
            aCoder.encode(coricId, forKey: "CoricId")
        }
        if createDate != nil{
            aCoder.encode(createDate, forKey: "CreateDate")
        }
        if depositAmt != nil{
            aCoder.encode(depositAmt, forKey: "DepositAmt")
        }
        if dLNo != nil{
            aCoder.encode(dLNo, forKey: "DLNo")
        }
        if doc != nil{
            aCoder.encode(doc, forKey: "Doc")
        }
        if earlyClosure != nil{
            aCoder.encode(earlyClosure, forKey: "EarlyClosure")
        }
        if emailID != nil{
            aCoder.encode(emailID, forKey: "EmailID")
        }
        if employeeName != nil{
            aCoder.encode(employeeName, forKey: "EmployeeName")
        }
        if extraKmRate != nil{
            aCoder.encode(extraKmRate, forKey: "ExtraKmRate")
        }
        if fuelType != nil{
            aCoder.encode(fuelType, forKey: "FuelType")
        }
        if indicatedMonthlyRental != nil{
            aCoder.encode(indicatedMonthlyRental, forKey: "IndicatedMonthlyRental")
        }
        if invoice != nil{
            aCoder.encode(invoice, forKey: "Invoice")
        }
        if km != nil{
            aCoder.encode(km, forKey: "Km")
        }
        if kmRun != nil{
            aCoder.encode(kmRun, forKey: "KmRun")
        }
        if maxAmtLimit != nil{
            aCoder.encode(maxAmtLimit, forKey: "MaxAmtLimit")
        }
        if panaltyAmt != nil{
            aCoder.encode(panaltyAmt, forKey: "PanaltyAmt")
        }
        if panNo != nil{
            aCoder.encode(panNo, forKey: "PanNo")
        }
        if passportNo != nil{
            aCoder.encode(passportNo, forKey: "PassportNo")
        }
        if paymentAmt != nil{
            aCoder.encode(paymentAmt, forKey: "PaymentAmt")
        }
        if paymentDetails != nil{
            aCoder.encode(paymentDetails, forKey: "PaymentDetails")
        }
        if permanentAddress != nil{
            aCoder.encode(permanentAddress, forKey: "permanentAddress")
        }
        if pickUpDate != nil{
            aCoder.encode(pickUpDate, forKey: "PickUpDate")
        }
        if pkgId != nil{
            aCoder.encode(pkgId, forKey: "PkgId")
        }
        if profileDetails != nil{
            aCoder.encode(profileDetails, forKey: "ProfileDetails")
        }
        if purchaseYear != nil{
            aCoder.encode(purchaseYear, forKey: "PurchaseYear")
        }
        if regnno != nil{
            aCoder.encode(regnno, forKey: "Regnno")
        }
        if rentallimit != nil{
            aCoder.encode(rentallimit, forKey: "rentallimit")
        }
        if requestDate != nil{
            aCoder.encode(requestDate, forKey: "RequestDate")
        }
        if requestId != nil{
            aCoder.encode(requestId, forKey: "RequestId")
        }
        if seatingCapacity != nil{
            aCoder.encode(seatingCapacity, forKey: "seatingCapacity")
        }
        if serviceDetails != nil{
            aCoder.encode(serviceDetails, forKey: "ServiceDetails")
        }
        if subscriberAccno != nil{
            aCoder.encode(subscriberAccno, forKey: "subscriber_accno")
        }
        if subscriptionApprovedDate != nil{
            aCoder.encode(subscriptionApprovedDate, forKey: "SubscriptionApprovedDate")
        }
        if subscriptionBookingid != nil{
            aCoder.encode(subscriptionBookingid, forKey: "SubscriptionBookingid")
        }
        if subscriptionEndDate != nil{
            aCoder.encode(subscriptionEndDate, forKey: "SubscriptionEndDate")
        }
        if subscriptionId != nil{
            aCoder.encode(subscriptionId, forKey: "SubscriptionId")
        }
        if subscriptionRequestDate != nil{
            aCoder.encode(subscriptionRequestDate, forKey: "SubscriptionRequestDate")
        }
        if subscriptionTenureMonths != nil{
            aCoder.encode(subscriptionTenureMonths, forKey: "SubscriptionTenureMonths")
        }
        if supportingDocumentPath != nil{
            aCoder.encode(supportingDocumentPath, forKey: "SupportingDocumentPath")
        }
        if taxAmount != nil{
            aCoder.encode(taxAmount, forKey: "TaxAmount")
        }
        if tentativeDeliveryDate != nil{
            aCoder.encode(tentativeDeliveryDate, forKey: "TentativeDeliveryDate")
        }
        if terminationDetails != nil{
            aCoder.encode(terminationDetails, forKey: "TerminationDetails")
        }
        if toBePaidAmt != nil{
            aCoder.encode(toBePaidAmt, forKey: "ToBePaidAmt")
        }
        if transmission != nil{
            aCoder.encode(transmission, forKey: "Transmission")
        }
        if upComingService != nil{
            aCoder.encode(upComingService, forKey: "UpComingService")
        }
    }
}