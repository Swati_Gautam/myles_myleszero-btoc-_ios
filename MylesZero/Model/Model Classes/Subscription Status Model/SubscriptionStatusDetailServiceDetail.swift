//
//  SubscriptionStatusDetailServiceDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 7, 2021

import Foundation


class SubscriptionStatusDetailServiceDetail : NSObject, NSCoding{

    var duration : String!
    var km : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        duration = dictionary["Duration"] as? String
        km = dictionary["Km"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if duration != nil{
            dictionary["Duration"] = duration
        }
        if km != nil{
            dictionary["Km"] = km
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        duration = aDecoder.decodeObject(forKey: "Duration") as? String
        km = aDecoder.decodeObject(forKey: "Km") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if duration != nil{
            aCoder.encode(duration, forKey: "Duration")
        }
        if km != nil{
            aCoder.encode(km, forKey: "Km")
        }
    }
}