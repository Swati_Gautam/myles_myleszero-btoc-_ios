//
//  SubscriptionStatusDetailEarlyClosure.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 7, 2021

import Foundation


class SubscriptionStatusDetailEarlyClosure : NSObject, NSCoding{

    var amount : String!
    var km : String!
    var tenure : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        amount = dictionary["Amount"] as? String
        km = dictionary["Km"] as? String
        tenure = dictionary["Tenure"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amount != nil{
            dictionary["Amount"] = amount
        }
        if km != nil{
            dictionary["Km"] = km
        }
        if tenure != nil{
            dictionary["Tenure"] = tenure
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        amount = aDecoder.decodeObject(forKey: "Amount") as? String
        km = aDecoder.decodeObject(forKey: "Km") as? String
        tenure = aDecoder.decodeObject(forKey: "Tenure") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if amount != nil{
            aCoder.encode(amount, forKey: "Amount")
        }
        if km != nil{
            aCoder.encode(km, forKey: "Km")
        }
        if tenure != nil{
            aCoder.encode(tenure, forKey: "Tenure")
        }
    }
}