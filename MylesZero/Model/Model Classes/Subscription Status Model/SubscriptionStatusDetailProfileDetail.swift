//
//  SubscriptionStatusDetailProfileDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 7, 2021

import Foundation


class SubscriptionStatusDetailProfileDetail : NSObject, NSCoding{

    var adharFile : String!
    var adharID : String!
    var adharStatus : String!
    var adharUrl : String!
    var company : AnyObject!
    var deviceToken : String!
    var dlExpiry : String!
    var dlIssueCity : String!
    var dlNo : String!
    var dlUrl : String!
    var dob : AnyObject!
    var dOB : String!
    var emailId : String!
    var fName : AnyObject!
    var fullName : String!
    var gender : String!
    var liecenceFile : String!
    var liecenceStatus : String!
    var lName : AnyObject!
    var mName : AnyObject!
    var panFile : String!
    var panNo : String!
    var panStatus : String!
    var panUrl : String!
    var passIssuingCity : String!
    var passportFile : String!
    var passportNo : String!
    var passportStatus : String!
    var passportUrl : String!
    var passValidity : String!
    var password : AnyObject!
    var perAdd : String!
    var phoneNumber : String!
    var profileImageName : String!
    var profileImageStatus : String!
    var profileImageURL : String!
    var resAdd : String!
    var voterExpiry : String!
    var voterFile : String!
    var voterID : String!
    var voterIssuingCity : String!
    var voterStatus : String!
    var voterUrl : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        adharFile = dictionary["AdharFile"] as? String
        adharID = dictionary["AdharID"] as? String
        adharStatus = dictionary["AdharStatus"] as? String
        adharUrl = dictionary["AdharUrl"] as? String
        company = dictionary["Company"] as? AnyObject
        deviceToken = dictionary["deviceToken"] as? String
        dlExpiry = dictionary["dlExpiry"] as? String
        dlIssueCity = dictionary["dlIssueCity"] as? String
        dlNo = dictionary["dlNo"] as? String
        dlUrl = dictionary["DlUrl"] as? String
        dob = dictionary["Dob"] as? AnyObject
        dOB = dictionary["DOB"] as? String
        emailId = dictionary["emailId"] as? String
        fName = dictionary["FName"] as? AnyObject
        fullName = dictionary["fullName"] as? String
        gender = dictionary["Gender"] as? String
        liecenceFile = dictionary["LiecenceFile"] as? String
        liecenceStatus = dictionary["LiecenceStatus"] as? String
        lName = dictionary["LName"] as? AnyObject
        mName = dictionary["MName"] as? AnyObject
        panFile = dictionary["PanFile"] as? String
        panNo = dictionary["PanNo"] as? String
        panStatus = dictionary["PanStatus"] as? String
        panUrl = dictionary["PanUrl"] as? String
        passIssuingCity = dictionary["passIssuingCity"] as? String
        passportFile = dictionary["PassportFile"] as? String
        passportNo = dictionary["passportNo"] as? String
        passportStatus = dictionary["PassportStatus"] as? String
        passportUrl = dictionary["PassportUrl"] as? String
        passValidity = dictionary["passValidity"] as? String
        password = dictionary["password"] as? AnyObject
        perAdd = dictionary["PerAdd"] as? String
        phoneNumber = dictionary["phoneNumber"] as? String
        profileImageName = dictionary["ProfileImageName"] as? String
        profileImageStatus = dictionary["ProfileImageStatus"] as? String
        profileImageURL = dictionary["ProfileImageURL"] as? String
        resAdd = dictionary["ResAdd"] as? String
        voterExpiry = dictionary["VoterExpiry"] as? String
        voterFile = dictionary["VoterFile"] as? String
        voterID = dictionary["VoterID"] as? String
        voterIssuingCity = dictionary["VoterIssuingCity"] as? String
        voterStatus = dictionary["VoterStatus"] as? String
        voterUrl = dictionary["VoterUrl"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if adharFile != nil{
            dictionary["AdharFile"] = adharFile
        }
        if adharID != nil{
            dictionary["AdharID"] = adharID
        }
        if adharStatus != nil{
            dictionary["AdharStatus"] = adharStatus
        }
        if adharUrl != nil{
            dictionary["AdharUrl"] = adharUrl
        }
        if company != nil{
            dictionary["Company"] = company
        }
        if deviceToken != nil{
            dictionary["deviceToken"] = deviceToken
        }
        if dlExpiry != nil{
            dictionary["dlExpiry"] = dlExpiry
        }
        if dlIssueCity != nil{
            dictionary["dlIssueCity"] = dlIssueCity
        }
        if dlNo != nil{
            dictionary["dlNo"] = dlNo
        }
        if dlUrl != nil{
            dictionary["DlUrl"] = dlUrl
        }
        if dob != nil{
            dictionary["Dob"] = dob
        }
        if dOB != nil{
            dictionary["DOB"] = dOB
        }
        if emailId != nil{
            dictionary["emailId"] = emailId
        }
        if fName != nil{
            dictionary["FName"] = fName
        }
        if fullName != nil{
            dictionary["fullName"] = fullName
        }
        if gender != nil{
            dictionary["Gender"] = gender
        }
        if liecenceFile != nil{
            dictionary["LiecenceFile"] = liecenceFile
        }
        if liecenceStatus != nil{
            dictionary["LiecenceStatus"] = liecenceStatus
        }
        if lName != nil{
            dictionary["LName"] = lName
        }
        if mName != nil{
            dictionary["MName"] = mName
        }
        if panFile != nil{
            dictionary["PanFile"] = panFile
        }
        if panNo != nil{
            dictionary["PanNo"] = panNo
        }
        if panStatus != nil{
            dictionary["PanStatus"] = panStatus
        }
        if panUrl != nil{
            dictionary["PanUrl"] = panUrl
        }
        if passIssuingCity != nil{
            dictionary["passIssuingCity"] = passIssuingCity
        }
        if passportFile != nil{
            dictionary["PassportFile"] = passportFile
        }
        if passportNo != nil{
            dictionary["passportNo"] = passportNo
        }
        if passportStatus != nil{
            dictionary["PassportStatus"] = passportStatus
        }
        if passportUrl != nil{
            dictionary["PassportUrl"] = passportUrl
        }
        if passValidity != nil{
            dictionary["passValidity"] = passValidity
        }
        if password != nil{
            dictionary["password"] = password
        }
        if perAdd != nil{
            dictionary["PerAdd"] = perAdd
        }
        if phoneNumber != nil{
            dictionary["phoneNumber"] = phoneNumber
        }
        if profileImageName != nil{
            dictionary["ProfileImageName"] = profileImageName
        }
        if profileImageStatus != nil{
            dictionary["ProfileImageStatus"] = profileImageStatus
        }
        if profileImageURL != nil{
            dictionary["ProfileImageURL"] = profileImageURL
        }
        if resAdd != nil{
            dictionary["ResAdd"] = resAdd
        }
        if voterExpiry != nil{
            dictionary["VoterExpiry"] = voterExpiry
        }
        if voterFile != nil{
            dictionary["VoterFile"] = voterFile
        }
        if voterID != nil{
            dictionary["VoterID"] = voterID
        }
        if voterIssuingCity != nil{
            dictionary["VoterIssuingCity"] = voterIssuingCity
        }
        if voterStatus != nil{
            dictionary["VoterStatus"] = voterStatus
        }
        if voterUrl != nil{
            dictionary["VoterUrl"] = voterUrl
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        adharFile = aDecoder.decodeObject(forKey: "AdharFile") as? String
        adharID = aDecoder.decodeObject(forKey: "AdharID") as? String
        adharStatus = aDecoder.decodeObject(forKey: "AdharStatus") as? String
        adharUrl = aDecoder.decodeObject(forKey: "AdharUrl") as? String
        company = aDecoder.decodeObject(forKey: "Company") as? AnyObject
        deviceToken = aDecoder.decodeObject(forKey: "deviceToken") as? String
        dlExpiry = aDecoder.decodeObject(forKey: "dlExpiry") as? String
        dlIssueCity = aDecoder.decodeObject(forKey: "dlIssueCity") as? String
        dlNo = aDecoder.decodeObject(forKey: "dlNo") as? String
        dlUrl = aDecoder.decodeObject(forKey: "DlUrl") as? String
        dob = aDecoder.decodeObject(forKey: "Dob") as? AnyObject
        dOB = aDecoder.decodeObject(forKey: "DOB") as? String
        emailId = aDecoder.decodeObject(forKey: "emailId") as? String
        fName = aDecoder.decodeObject(forKey: "FName") as? AnyObject
        fullName = aDecoder.decodeObject(forKey: "fullName") as? String
        gender = aDecoder.decodeObject(forKey: "Gender") as? String
        liecenceFile = aDecoder.decodeObject(forKey: "LiecenceFile") as? String
        liecenceStatus = aDecoder.decodeObject(forKey: "LiecenceStatus") as? String
        lName = aDecoder.decodeObject(forKey: "LName") as? AnyObject
        mName = aDecoder.decodeObject(forKey: "MName") as? AnyObject
        panFile = aDecoder.decodeObject(forKey: "PanFile") as? String
        panNo = aDecoder.decodeObject(forKey: "PanNo") as? String
        panStatus = aDecoder.decodeObject(forKey: "PanStatus") as? String
        panUrl = aDecoder.decodeObject(forKey: "PanUrl") as? String
        passIssuingCity = aDecoder.decodeObject(forKey: "passIssuingCity") as? String
        passportFile = aDecoder.decodeObject(forKey: "PassportFile") as? String
        passportNo = aDecoder.decodeObject(forKey: "passportNo") as? String
        passportStatus = aDecoder.decodeObject(forKey: "PassportStatus") as? String
        passportUrl = aDecoder.decodeObject(forKey: "PassportUrl") as? String
        passValidity = aDecoder.decodeObject(forKey: "passValidity") as? String
        password = aDecoder.decodeObject(forKey: "password") as? AnyObject
        perAdd = aDecoder.decodeObject(forKey: "PerAdd") as? String
        phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
        profileImageName = aDecoder.decodeObject(forKey: "ProfileImageName") as? String
        profileImageStatus = aDecoder.decodeObject(forKey: "ProfileImageStatus") as? String
        profileImageURL = aDecoder.decodeObject(forKey: "ProfileImageURL") as? String
        resAdd = aDecoder.decodeObject(forKey: "ResAdd") as? String
        voterExpiry = aDecoder.decodeObject(forKey: "VoterExpiry") as? String
        voterFile = aDecoder.decodeObject(forKey: "VoterFile") as? String
        voterID = aDecoder.decodeObject(forKey: "VoterID") as? String
        voterIssuingCity = aDecoder.decodeObject(forKey: "VoterIssuingCity") as? String
        voterStatus = aDecoder.decodeObject(forKey: "VoterStatus") as? String
        voterUrl = aDecoder.decodeObject(forKey: "VoterUrl") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if adharFile != nil{
            aCoder.encode(adharFile, forKey: "AdharFile")
        }
        if adharID != nil{
            aCoder.encode(adharID, forKey: "AdharID")
        }
        if adharStatus != nil{
            aCoder.encode(adharStatus, forKey: "AdharStatus")
        }
        if adharUrl != nil{
            aCoder.encode(adharUrl, forKey: "AdharUrl")
        }
        if company != nil{
            aCoder.encode(company, forKey: "Company")
        }
        if deviceToken != nil{
            aCoder.encode(deviceToken, forKey: "deviceToken")
        }
        if dlExpiry != nil{
            aCoder.encode(dlExpiry, forKey: "dlExpiry")
        }
        if dlIssueCity != nil{
            aCoder.encode(dlIssueCity, forKey: "dlIssueCity")
        }
        if dlNo != nil{
            aCoder.encode(dlNo, forKey: "dlNo")
        }
        if dlUrl != nil{
            aCoder.encode(dlUrl, forKey: "DlUrl")
        }
        if dob != nil{
            aCoder.encode(dob, forKey: "Dob")
        }
        if dOB != nil{
            aCoder.encode(dOB, forKey: "DOB")
        }
        if emailId != nil{
            aCoder.encode(emailId, forKey: "emailId")
        }
        if fName != nil{
            aCoder.encode(fName, forKey: "FName")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "fullName")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "Gender")
        }
        if liecenceFile != nil{
            aCoder.encode(liecenceFile, forKey: "LiecenceFile")
        }
        if liecenceStatus != nil{
            aCoder.encode(liecenceStatus, forKey: "LiecenceStatus")
        }
        if lName != nil{
            aCoder.encode(lName, forKey: "LName")
        }
        if mName != nil{
            aCoder.encode(mName, forKey: "MName")
        }
        if panFile != nil{
            aCoder.encode(panFile, forKey: "PanFile")
        }
        if panNo != nil{
            aCoder.encode(panNo, forKey: "PanNo")
        }
        if panStatus != nil{
            aCoder.encode(panStatus, forKey: "PanStatus")
        }
        if panUrl != nil{
            aCoder.encode(panUrl, forKey: "PanUrl")
        }
        if passIssuingCity != nil{
            aCoder.encode(passIssuingCity, forKey: "passIssuingCity")
        }
        if passportFile != nil{
            aCoder.encode(passportFile, forKey: "PassportFile")
        }
        if passportNo != nil{
            aCoder.encode(passportNo, forKey: "passportNo")
        }
        if passportStatus != nil{
            aCoder.encode(passportStatus, forKey: "PassportStatus")
        }
        if passportUrl != nil{
            aCoder.encode(passportUrl, forKey: "PassportUrl")
        }
        if passValidity != nil{
            aCoder.encode(passValidity, forKey: "passValidity")
        }
        if password != nil{
            aCoder.encode(password, forKey: "password")
        }
        if perAdd != nil{
            aCoder.encode(perAdd, forKey: "PerAdd")
        }
        if phoneNumber != nil{
            aCoder.encode(phoneNumber, forKey: "phoneNumber")
        }
        if profileImageName != nil{
            aCoder.encode(profileImageName, forKey: "ProfileImageName")
        }
        if profileImageStatus != nil{
            aCoder.encode(profileImageStatus, forKey: "ProfileImageStatus")
        }
        if profileImageURL != nil{
            aCoder.encode(profileImageURL, forKey: "ProfileImageURL")
        }
        if resAdd != nil{
            aCoder.encode(resAdd, forKey: "ResAdd")
        }
        if voterExpiry != nil{
            aCoder.encode(voterExpiry, forKey: "VoterExpiry")
        }
        if voterFile != nil{
            aCoder.encode(voterFile, forKey: "VoterFile")
        }
        if voterID != nil{
            aCoder.encode(voterID, forKey: "VoterID")
        }
        if voterIssuingCity != nil{
            aCoder.encode(voterIssuingCity, forKey: "VoterIssuingCity")
        }
        if voterStatus != nil{
            aCoder.encode(voterStatus, forKey: "VoterStatus")
        }
        if voterUrl != nil{
            aCoder.encode(voterUrl, forKey: "VoterUrl")
        }
    }
}