//
//  SideMenuFunctions.swift
//  Myles Zero
//
//  Created by Chetan Rajauria on 03/09/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuFunctions: UIViewController
{
    static let customSideMenuManager = SideMenuManager()
    
    fileprivate static var __once: () = {
        Static.instanceSideMenu = SideMenuFunctions()
    }()
    
    struct Static {
        static var instanceSideMenu: SideMenuFunctions? = nil
    }
    
    class var sharedInstance: SideMenuFunctions {
        
        _ = SideMenuFunctions.__once
        return Static.instanceSideMenu!
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Side Menu Function
    
    func setupSideMenu()
    {
        // Define the menus
        
        guard let objSideMenuVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SideMenu) as? SideMenuVC else { return }
        
        let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objSideMenuVC)
        //let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: SideMenuVC)
        SideMenuFunctions.customSideMenuManager.menuLeftNavigationController = leftMenuNavigationController
        let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        SideMenuFunctions.customSideMenuManager.menuAddPanGestureToPresent(toView: navController!.navigationBar)
    SideMenuFunctions.customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        //customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuFunctions.customSideMenuManager.menuWidth = 300.0
        SideMenuFunctions.customSideMenuManager.menuFadeStatusBar = false
        
        //leftMenuNavigationController.statusBarEndAlpha = 0
        
        present(leftMenuNavigationController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let sideMenuNavigationController = segue.destination as? UISideMenuNavigationController
        {
            sideMenuNavigationController.sideMenuManager = SideMenuFunctions.customSideMenuManager
        }
    }
}
