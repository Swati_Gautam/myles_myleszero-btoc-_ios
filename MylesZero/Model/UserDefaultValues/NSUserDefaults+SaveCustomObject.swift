//
//  NSUserDefaults+SaveCustomObject.swift
//  Locatem
//
//  Created by Mac admin on 17/05/18.
//  Copyright © 2018 Swati. All rights reserved.
//
import Foundation
import UIKit

extension UserDefaults
{
    func setCustomObject(_ obj: Any?, forKey key: String?) {
        defer {
        }
        do
        {
            if (obj as AnyObject).responds(to: #selector(NSCoding.encode(with:))) == false
            {
                print("Error save object to NSUserDefaults. Object must respond to encodeWithCoder: message")
                //throw exception
                return
            }
            
            let userDefaults = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: obj as Any)
            userDefaults.set(encodedData, forKey: key!)
            userDefaults.synchronize()
            
            
            
            /*do {
             let archived = try NSKeyedArchiver.archivedData(withRootObject: defaultRecord, requiringSecureCoding: false)
             
             let record = try NSKeyedUnarchiver.unarchivedObject(ofClass: Record.self, from: archived)
             let records = [record]
             print(records)
             } catch { print(error) }*/
            
            
            
            //            var encodedObject: Data? = nil
            //            if let anObj = obj {
            //                encodedObject = NSKeyedArchiver.archivedData(withRootObject: anObj)
            //            }
            //            let defaults = UserDefaults.standard
            //            defaults.set(encodedObject, forKey: key ?? "")
            //defaults.synchronize()
        }
        /*catch let exception
         {
         print("error in defaults", exception)
         }*/
    }
    
    func customObject(forKey key: String?) -> Any?
    {
        let defaults = UserDefaults.standard
        let encodedObject = defaults.object(forKey: key ?? "") as? Data
        var obj: Any? = nil
        if let anObject = encodedObject {
            obj = NSKeyedUnarchiver.unarchiveObject(with: anObject)
        }
        return obj
    }
}


/*extension UserDefaults {
    
    func save<T:Encodable>(customObject object: T, inKey key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object) {
            self.set(encoded, forKey: key)
        }
    }
    
    func retrieve<T:Decodable>(object type:T.Type, fromKey key: String) -> T? {
        if let data = self.data(forKey: key) {
            let decoder = JSONDecoder()
            if let object = try? decoder.decode(type, from: data) {
                return object
            }else {
                print("Couldnt decode object")
                return nil
            }
        }else {
            print("Couldnt find key")
            return nil
        }
    }
    
}*/
