//
//  WebService.swift
//  COR
//
//  Created by Faraz Habib on 14/05/16.
//  Copyright © 2016 Nitesh Kant. All rights reserved.
//

import UIKit
import Alamofire

class WebService: NSObject {
    
    fileprivate static var __once: () = {
        Static.instance = WebService()
    }()
    
    var alamoFireManager = Alamofire.SessionManager.default
    var request : Alamofire.Request?
    struct Static {
        static var onceToken: Int = 0
        static var instance: WebService? = nil
    }
    class var sharedInstance: WebService {
        
        _ = WebService.__once
        return Static.instance!
    }
    
    override init() {
        let configuration = URLSessionConfiguration.default
//        configuration.timeoutIntervalForRequest = 30
//        configuration.timeoutIntervalForResource = 30
        
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 120
        self.alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func hitWebServiceWithGET(_ url:String!, completion: @escaping (DataResponse<Any>) -> Void) {
        self.jsonFormatForRequest(url)
        print("url === \(String(describing: url))")
        self.request = self.alamoFireManager.request(url , method: .get).responseJSON { (response) in
            self.jsonFormatForResponse(nil, response: response)
            completion(response)
        }
    }
    
    func hitWebServiceWithPost1(_ url:String!, postData:[String:AnyObject]?, header:[String:String]?, completion: @escaping (DataResponse<Any>) -> Void) {
        print("url ===\(String(describing: url))")
        print("postData = \(String(describing: postData!))")
        self.request = self.alamoFireManager.request( url, method: .post , parameters: postData, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            self.jsonFormatForResponse(postData, response: response)
            completion(response)
        }
        self.jsonFormatForRequest(url)
    }
    
    func hitWebServiceWithPost(_ url:String!, postData:[String:AnyObject]?, header:[String:String]?, completion: @escaping (DataResponse<Any>) -> Void) {
        print("url ===\(String(describing: url))")
        print("postData = \(String(describing: postData))")
        //let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        //let strSession = DefaultsValues.getStringValueFromUserDefaults_(forKey: kSessionID)
        //let strAccess = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAccessToken)
        
        /*let headers: [String:String] = ["Content-Type":"application/json",
                                        "Accept":"application/json",
                                    "sessionID": strSession!,
                                    "customerID":kCustomerID,
                                    "hashVal":strAccess!]
        print("Headers: ", headers)*/

        self.request = self.alamoFireManager.request( url, method: .post , parameters: postData, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            self.jsonFormatForResponse(postData, response: response)
            completion(response)
        }
        self.jsonFormatForRequest(url)
    }
    
    func hitWebServiceWithPost11(_ url:String!, postData:[String:AnyObject]?, header:[String:String]?, completion: @escaping (DataResponse<Any>) -> Void) {
        print("url ===\(String(describing: url))")
        print("postData = \(String(describing: postData))")
        self.request = self.alamoFireManager.request( url, method: .post , parameters: postData, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            self.jsonFormatForResponse(postData, response: response)
            completion(response)
        }
        self.jsonFormatForRequest(url)
    }
    
    func hitWebServiceWithPUT(_ url:String!, postData:[String:AnyObject]?, header:[String:String]?, completion: @escaping (DataResponse<Any>) -> Void) {
        self.request = self.alamoFireManager.request(url, method :.put, parameters: postData, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            self.jsonFormatForResponse(postData, response: response)
            completion(response)
        }
        self.jsonFormatForRequest(url)
    }
    
    func hitWebServiceWithData( method:Alamofire.HTTPMethod, url:String!, postData:[String:AnyObject]?, header:[String:String]?, completion: @escaping (DataResponse<Any>) -> Void) {
        self.request = self.alamoFireManager.request(url, method: method, parameters: postData, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            self.jsonFormatForResponse(postData, response: response)
            completion(response)
        }
        self.jsonFormatForRequest(url)
    }
    
    func hitWebServiceWithListData(_ request:URLRequest, completion: @escaping (DataResponse<Any>) -> Void) {
        self.request = self.alamoFireManager.request(request as URLRequestConvertible).responseJSON { (response) in
            //            self.jsonFormatForListData(url, dataList: request.HTTPBody, response: response)
            completion(response)
        }
    }
    
    func cancelRequest() {
        if self.request != nil {
            self.request?.cancel()
        }
    }
    
    func cancelAllRequest() {
        if self.request != nil {
            self.alamoFireManager.session.invalidateAndCancel()
            
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30
            configuration.timeoutIntervalForResource = 30
            self.alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        }
    }
    
    func jsonFormatForRequest(_ url:String) {
        print("\n\(Date()) URL --> \(url)")
        
    }
    
    func jsonFormatForResponse(_ dataDict:[String:AnyObject]?, response: DataResponse<Any>) {
        if dataDict != nil {
            let data = try! JSONSerialization.data(withJSONObject: dataDict!, options: [])
            print("\n\(Date()) POST DATA --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
        }
        switch response.result {
        case .success(let JSON):
            print("\n\(Date()) RESPONSE --> \(JSON)")
            break
        case .failure(let error):
            print("\n\(Date()) ERROR --> \(error.localizedDescription)")
            break
        }
    }
    
}
