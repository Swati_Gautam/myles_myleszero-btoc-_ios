//
//  APIConstants.swift
//  Myles Zero
//
//  Created by skpissay on 02/09/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class APIConstants: NSObject
{
    
    #if DEBUG
    //Test URL'S
    static let QA1FolderUrl:String = "Services/LongRental.svc"
    static let GetAccessToken:String = "V1_RestW1AuthenticateUser"
    static let API_Basee1 : String = "https://mobileappservice.carzonrent.com/"
    
    static let API_UserLogin:String = String.init(format: "%@%@/RestSubscription_LoginNew", API_Basee1, QA1FolderUrl)
    
    //static let API_UserLogin:String = String.init(format: "%@%@/RestSubscription_LoginNew", Config.serverBaseUrl, QA1FolderUrl)
    
    static let API_BaseURL1 : String = String.init(format: "%@", "https://mylesaccesstoken.carzonrent.com/Services/MylesTokenService.svc/")
    
    static let API_BaseURL2 : String = String.init(format: "%@", "https://mobileappservice.carzonrent.com/Services/LongRental.svc/")
    
    static let API_BaseURL3 : String = String.init(format: "%@", "https://myles.carzonrent.com/services/mylesservice.svc/")
    
    static let API_BaseURL4 : String = String.init(format: "%@", "https://testmgmotors.mylescars.com/Experian/")
    
    static let API_BaseURL5 : String = String.init(format: "%@", "https://testb2c.mylescars.com/")
    
    static let API_GetAccessToken : String = String.init(format: "%@%@",API_BaseURL1, GetAccessToken)
    
    //static let API_FetchCityList : String = String.init(format: "%@%@",API_BaseURL3, "fetchCities_Subscription")
    
    static let API_FetchCityList : String = String.init(format: "%@%@",API_BaseURL2, "fetchCities_Subscription")
    
    //static let API_RegisterUser : String = String.init(format: "%@%@",API_BaseURL2, "RegisterUser_new")
    static let API_RegisterUser : String = String.init(format: "%@%@",API_BaseURL2, "RegisterUser_new")
    static let API_GetOTPForRegister : String = String.init(format: "%@%@",API_BaseURL2, "SendEmailOTPclientB2c")
    static let API_VerifyOTPForRegister : String = String.init(format: "%@%@",API_BaseURL2, "verifyEmailclient")
    
    static let API_SearchCarForSubscribe : String = String.init(format: "%@%@",API_BaseURL2, "Search_SubscriptionNew")
    static let API_GetOTPForgotPassword : String = String.init(format: "%@%@",API_BaseURL2, "ResendEmailOTP")
    static let API_VerifyOTPForForgotPwd : String = String.init(format: "%@%@",API_BaseURL2, "ResetPasswordlongrental")
    static let API_ResetPassword : String = String.init(format: "%@%@",API_BaseURL2, "ResetPasswordlongrental")
    
    static let API_SocialRegistration : String = String.init(format: "%@%@",API_BaseURL2, "Registration_Social")
    static let API_SocialLogin : String = String.init(format: "%@%@",API_BaseURL2, "customerdetailbySocialmedia")
    
    static let API_SearchSubscriptionByCarID : String = String.init(format: "%@%@",API_BaseURL2, "Search_SubscriptionByCarID")
    
    static let API_GetInvoiceList : String = String.init(format: "%@%@",API_BaseURL2, "GetInvoiceList")
    
    static let API_pancard_validity : String = String.init(format: "%@%@",API_BaseURL4, "pancard")
    static let API_drivingLicence_Validity : String = String.init(format: "%@%@",API_BaseURL4, "drivinglicence")
    static let API_passport_validity : String = String.init(format: "%@%@",API_BaseURL4, "passport")
    static let API_adhaarCard_validity : String = String.init(format: "%@%@",API_BaseURL4, "aadhaarcard")
    static let API_creditscore : String = String.init(format: "%@%@",API_BaseURL4, "creditscore")
    static let API_UploadImageOnAWS : String = String.init(format: "%@","https://s3-ap-southeast-1.amazonaws.com/")
    
    static let API_GetEmployeeDetail : String = String.init(format: "%@%@",API_BaseURL2,"GetEmployeeDetailsnew")
    
    static let API_FetchSubscriptionDashboard : String = String.init(format: "%@%@",API_BaseURL2,"FetchSubscription_DashBoard")
    
    static let API_UploadDocumentAuthBridgeExperian : String = String.init(format: "%@%@",API_BaseURL2,"Documentupload_Employee_authbridge_Experian")
    
    static let API_RecheckCarAvailabilityLTR : String = String.init(format: "%@%@",API_BaseURL2,"RecheckCarAvailabilityLTR")
    
    static let API_CreateNewSubscription : String = String.init(format: "%@%@",API_BaseURL2,"Employee_CreateSubscription_New")
    
    //static let API_Payment_Url : String = String.init(format: "%@%@",API_BaseURL5,"PayApp/getSubsDataForPayment")
    
    //static let API_Payment_Url : String = String.init(format: "%@%@","http://testb2c.mylescars.com/","PayApp/getSubsDataForPayment")
    
    static let API_Payment_Url : String = String.init(format: "https://testb2c.mylescars.com/PayApp/getSubsDataForPayment")
    static let PAYMENT_MAIN_URL : String = "https://testb2c.mylescars.com/PayApp/"//"https://mylesb2c.mylescars.com/PayApp/"
    
    static let API_CarSubscriptionAgreement : String = String.init(format: "%@%@",API_BaseURL5,"appagreement?ClientId=%@&SubscriptionId=%@")
    
    static let API_GetLostItemList : String = String.init(format: "%@%@",API_BaseURL2,"GetLostItemList")
    
    static let API_ReportLostItem : String = String.init(format: "%@%@",API_BaseURL2,"ReportLostItem")
    
    static let API_GetSubscriptionDocsData : String = String.init(format: "%@%@",API_BaseURL2,"GetSubscriptionDocsData")
    
    static let API_UserDocumentList : String = String.init(format: "%@%@",API_BaseURL2,"UserDocumentList")
    
    static let API_GetCallbackReason : String = String.init(format: "%@%@",API_BaseURL2,"GetCallbackReason")
    
    static let API_GetScheduleServiceHistory : String = String.init(format: "%@%@",API_BaseURL2,"ServiceScheduleHistory")
    
    static let API_UpdateSlotsForUpcomingService : String = String.init(format: "%@%@",API_BaseURL2,"UpdateSlotsforUpcomingService")
    
    static let API_SubsriptionCallBackRequest : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionCallBackRequest")
    
    static let API_RenewSubscriptionRequest : String = String.init(format: "%@%@",API_BaseURL2,"RenewSubsriptionrequest")
    
    static let API_CheckPriceForRenewSubscription : String = String.init(format: "%@%@",API_BaseURL2,"CheckPriceForRenewSubsription")
    
    static let API_SubscriptionTermination : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionTermination")
    
    static let API_SubscriptionTerminationRequest : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionTerminationRequest")
    
    static let API_CreateServiceSubscription : String = String.init(format: "%@%@",API_BaseURL2,"CreateServiceSubscription")
    
    static let API_CreateUpgradeSubscription : String = String.init(format: "%@%@",API_BaseURL2,"Employee_CreateSubscription_Upgrade")
    
    static let API_UploadSingleDocument : String = String.init(format: "%@%@",API_BaseURL2,"UploadSingleDoc")
    
    static let API_GetKmAndTenureForUpgrade : String = String.init(format: "%@%@",API_BaseURL2,"GetKmAndTenureForUpgrade")
    
    static let API_SubsriptionUpgradeRequest : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionUpgradeRequest")
    
    static let API_SearchSubscriptionForUpgrade : String = String.init(format: "%@%@",API_BaseURL2,"Search_SubscriptionForUpgrade")
    
    static let API_FAQ : String = "https://myleszero.mylescars.com/faq?q=app"
    
    #else
    //LIVE URL'S
    
    static let API_BaseURL2 : String = "https://mapiv2.mylescars.com/Services/LongRental.svc/" //BASE_URL
    static let TOKEN_URL : String = "https://oauth.mylescars.com/Services/MylesTokenService.svc/V1_RestW1AuthenticateUser"
    static let AUTH_BRIDGE_URL : String = "https://mylesb2c.mylescars.com/Experian/" //"https://mgsubscribe.mylescars.com/Experian/"
    static let AGREEMENT_URL : String = "https://mylesb2c.mylescars.com/appagreement?"
    static let PAYMENT_MAIN_URL : String = "https://mylesb2c.mylescars.com/PayApp/"
    static let PAYMENT_URL : String = "https://mylesb2c.mylescars.com/PayApp/getSubsDataForPayment"
    //static let FAQ : String = "https://myleszero.mylescars.com/faq?q=app"
    
    static let API_GetAccessToken : String = String.init(format: "%@",TOKEN_URL)
    
    //static let API_FetchCityList : String = String.init(format: "%@%@",API_BaseURL3, "fetchCities_Subscription")
    
    static let API_FAQ : String = "https://myleszero.mylescars.com/faq?q=app"
    
    static let API_FetchCityList : String = String.init(format: "%@%@",API_BaseURL2, "fetchCities_Subscription")
    
    //static let API_RegisterUser : String = String.init(format: "%@%@",API_BaseURL2, "RegisterUser_new")
    static let API_RegisterUser : String = String.init(format: "%@%@",API_BaseURL2, "RegisterUser_new")
    static let API_GetOTPForRegister : String = String.init(format: "%@%@",API_BaseURL2, "SendEmailOTPclientB2c")
    static let API_VerifyOTPForRegister : String = String.init(format: "%@%@",API_BaseURL2, "verifyEmailclient")
    
    static let API_SearchCarForSubscribe : String = String.init(format: "%@%@",API_BaseURL2, "Search_SubscriptionNew")
    static let API_GetOTPForgotPassword : String = String.init(format: "%@%@",API_BaseURL2, "ResendEmailOTP")
    static let API_VerifyOTPForForgotPwd : String = String.init(format: "%@%@",API_BaseURL2, "ResetPasswordlongrental")
    static let API_ResetPassword : String = String.init(format: "%@%@",API_BaseURL2, "ResetPasswordlongrental")
    
    static let API_SocialRegistration : String = String.init(format: "%@%@",API_BaseURL2, "Registration_Social")
    static let API_SocialLogin : String = String.init(format: "%@%@",API_BaseURL2, "customerdetailbySocialmedia")
    
    static let API_SearchSubscriptionByCarID : String = String.init(format: "%@%@",API_BaseURL2, "Search_SubscriptionByCarID")
    
    static let API_GetInvoiceList : String = String.init(format: "%@%@",API_BaseURL2, "GetInvoiceList")
    
    static let API_pancard_validity : String = String.init(format: "%@%@",AUTH_BRIDGE_URL, "pancard")
    static let API_drivingLicence_Validity : String = String.init(format: "%@%@",AUTH_BRIDGE_URL, "drivinglicence")
    static let API_passport_validity : String = String.init(format: "%@%@",AUTH_BRIDGE_URL, "passport")
    static let API_adhaarCard_validity : String = String.init(format: "%@%@",AUTH_BRIDGE_URL, "aadhaarcard")
    static let API_creditscore : String = String.init(format: "%@%@",AUTH_BRIDGE_URL, "creditscore")
    static let API_UploadImageOnAWS : String = String.init(format: "%@","https://s3-ap-southeast-1.amazonaws.com/")
    
    static let API_GetEmployeeDetail : String = String.init(format: "%@%@",API_BaseURL2,"GetEmployeeDetailsnew")
    
    static let API_FetchSubscriptionDashboard : String = String.init(format: "%@%@",API_BaseURL2,"FetchSubscription_DashBoard")
    
    static let API_UploadDocumentAuthBridgeExperian : String = String.init(format: "%@%@",API_BaseURL2,"Documentupload_Employee_authbridge_Experian")
    
    static let API_RecheckCarAvailabilityLTR : String = String.init(format: "%@%@",API_BaseURL2,"RecheckCarAvailabilityLTR")
    
    static let API_CreateNewSubscription : String = String.init(format: "%@%@",API_BaseURL2,"Employee_CreateSubscription_New")
    
    //static let API_Payment_Url : String = String.init(format: "%@%@",API_BaseURL5,"PayApp/getSubsDataForPayment")
    
    //static let API_Payment_Url : String = String.init(format: "%@%@","http://testb2c.mylescars.com/","PayApp/getSubsDataForPayment")
    
    static let API_Payment_Url : String = String.init(format: "%@", PAYMENT_URL)
    
    static let API_CarSubscriptionAgreement : String = String.init(format: "%@%@",AGREEMENT_URL,"ClientId=%@&SubscriptionId=%@")
    
    static let API_GetLostItemList : String = String.init(format: "%@%@",API_BaseURL2,"GetLostItemList")
    
    static let API_ReportLostItem : String = String.init(format: "%@%@",API_BaseURL2,"ReportLostItem")
    
    static let API_GetSubscriptionDocsData : String = String.init(format: "%@%@",API_BaseURL2,"GetSubscriptionDocsData")
    
    static let API_UserDocumentList : String = String.init(format: "%@%@",API_BaseURL2,"UserDocumentList")
    
    static let API_GetCallbackReason : String = String.init(format: "%@%@",API_BaseURL2,"GetCallbackReason")
    
    static let API_GetScheduleServiceHistory : String = String.init(format: "%@%@",API_BaseURL2,"ServiceScheduleHistory")
    
    static let API_UpdateSlotsForUpcomingService : String = String.init(format: "%@%@",API_BaseURL2,"UpdateSlotsforUpcomingService")
    
    static let API_SubsriptionCallBackRequest : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionCallBackRequest")
    
    static let API_RenewSubscriptionRequest : String = String.init(format: "%@%@",API_BaseURL2,"RenewSubsriptionrequest")
    
    static let API_CheckPriceForRenewSubscription : String = String.init(format: "%@%@",API_BaseURL2,"CheckPriceForRenewSubsription")
    
    static let API_SubscriptionTermination : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionTermination")
    
    static let API_SubscriptionTerminationRequest : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionTerminationRequest")
    
    static let API_CreateServiceSubscription : String = String.init(format: "%@%@",API_BaseURL2,"CreateServiceSubscription")
    
    static let API_CreateUpgradeSubscription : String = String.init(format: "%@%@",API_BaseURL2,"Employee_CreateSubscription_Upgrade")
    
    static let API_UploadSingleDocument : String = String.init(format: "%@%@",API_BaseURL2,"UploadSingleDoc")
    
    static let API_GetKmAndTenureForUpgrade : String = String.init(format: "%@%@",API_BaseURL2,"GetKmAndTenureForUpgrade")
    
    static let API_SubsriptionUpgradeRequest : String = String.init(format: "%@%@",API_BaseURL2,"SubsriptionUpgradeRequest")
    
    static let API_SearchSubscriptionForUpgrade : String = String.init(format: "%@%@",API_BaseURL2,"Search_SubscriptionForUpgrade")
    
    #endif

    //https://testb2c.mylescars.com/appagreement?ClientId=2700125&SubscriptionId=119611
    //https://testb2c.mylescars.com/PayApp/getSubsDataForPayment
}

 
