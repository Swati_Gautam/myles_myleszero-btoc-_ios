//
//  APIFunctions.swift
//  Myles Zero
//
//  Created by skpissay on 02/09/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import Alamofire

class APIFunctions: NSObject {
    
    fileprivate static var __once: () = {
        Static.instance = APIFunctions()
    }()
    let networkReachability = NetworkReachabilityManager()
    let webServiceClient:WebService!
    //let responseData: DataResponse<Any>
    
    var baseURL:String!
    var isOngoingCall:Bool!
    static var dictHeaders : [String:String]?
    static var dictGlobalCarFilter : NSDictionary?
    static var dictSubscriptionDetailModel : NSDictionary?
    static var arrGlobalCarList = NSArray()
    
//    var dictHeaders : [String:String] = ["Content-Type":"application/json",
//                                         "sessionID": strDefaultSessionID ?? "",
//                                         "customerID":kCustomerID,
//                                         "hashVal":strDefaultAccessToken ?? "",
//                                         "cache-control": "no-cache"]
    //var prebookingDetailApi: Bool!
    
    struct Static {
        static var onceToken: Int = 0
        static var instance: APIFunctions? = nil
        static var isOngoingCall:Bool!
        static var dictHeaders : [String:String]?
        
    }
    
    class var sharedInstance: APIFunctions {
        
        _ = APIFunctions.__once
        return Static.instance!
    }
    
    public override init() {
        //self.prebookingDetailApi = false
        self.webServiceClient = WebService.sharedInstance
        //self.cabsModel = Cabs_Model.sharedInstance
        
        let targetName = Bundle.main.infoDictionary?["target_name"] as! String
        print(targetName)
        //let environmentListPath = Bundle.main.path(forResource: "Environment", ofType: "plist")
        //let environmentList = NSDictionary(contentsOfFile:environmentListPath!)
        //let environment = environmentList?.object(forKey: targetName) as! NSDictionary
        //print("ServerUrl: ", Config.serverBaseUrl)
        self.baseURL = Config.serverBaseUrl
        //self.baseURL = environment.value(forKey: "myAPIURL") as! String
//        if kDictHeaders != nil
//        {
//            APIFunctions.dictHeaders = kDictHeaders
//        }
        self.isOngoingCall = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }    
    
    //MARK: Generic Methods
    
    func isRequestFailedForAccessToken(_ JSON:AnyObject) -> NSError? {
        if let metaData = JSON as? NSDictionary {
            if let statusCode = metaData.object(forKey: Constants.kHTTPStatusConstant) as? Int {
                if statusCode == 200 || statusCode == 202 {
                    let response_object = JSON.object(forKey: "response_object") as? NSDictionary
                    if response_object == nil{
                        return NSError(domain: "Failed. Please try again.", code: statusCode, userInfo: nil)
                    }
                    /*if self.prebookingDetailApi{
                     self.prebookingDetailApi = !self.prebookingDetailApi
                     if JSON.object(forKey: "response_object") == nil{
                     return NSError(domain: "Failed. Please try again.", code: statusCode, userInfo: nil)
                     }
                     }*/
                    return nil
                }
                else
                {
                    /*if let errorMsg = (JSON.object(forKey: COR_Constants.kMetaDataKeyConstant)! as AnyObject).object(forKey: "error_message") as? String {
                     return NSError(domain: errorMsg, code: statusCode, userInfo: nil)
                     } else {
                     return NSError(domain: "Failed. Please try again.", code: statusCode, userInfo: nil)
                     }*/
                }
            }
        }
        
        return NSError(domain: "Failed. Please try again.", code: 200, userInfo: nil) // Default error
    }
    
    func isRequestFailed(_ JSON:AnyObject) -> NSError? {
        if let metaData = JSON.object(forKey: Constants.kMetaDataKeyConstant) as? NSDictionary {
            if let statusCode = metaData.object(forKey: Constants.kHTTPStatusConstant) as? Int {
                if statusCode == 200 || statusCode == 202 {
                                        let response_object = JSON.object(forKey:kResponseInfo) as? NSDictionary //"response_object"
                                        if response_object == nil{
                                           return NSError(domain: "Failed. Please try again.", code: statusCode, userInfo: nil)
                                        }
                    /*if self.prebookingDetailApi{
                        self.prebookingDetailApi = !self.prebookingDetailApi
                        if JSON.object(forKey: "response_object") == nil{
                            return NSError(domain: "Failed. Please try again.", code: statusCode, userInfo: nil)
                        }
                    }*/
                    return nil
                }
                else
                {
                    /*if let errorMsg = (JSON.object(forKey: COR_Constants.kMetaDataKeyConstant)! as AnyObject).object(forKey: "error_message") as? String {
                        return NSError(domain: errorMsg, code: statusCode, userInfo: nil)
                    } else {
                        return NSError(domain: "Failed. Please try again.", code: statusCode, userInfo: nil)
                    }*/
                }
            }
        }
        else if (JSON.object(forKey: "results") as? NSArray) != nil {
            return nil
        }
        else if (JSON.object(forKey: "routes") as? NSArray) != nil {
            return nil
        }
        
        return NSError(domain: "Failed. Please try again.", code: 200, userInfo: nil) // Default error
    }
    
    func verifyResponse(_ response:DataResponse<Any>) -> NSError? {
        switch response.result {
        case .success(let JSON):
            self.saveDataForRequest(JSON as AnyObject)
            return nil
            /*if let error = self.isRequestFailed(JSON as AnyObject)  {
                return error
            } else {
                return nil
            }*/
        case .failure(let error):
            
            let desc =  "Failed to connect to server. Please try again"
            if let code = response.response?.statusCode{
                return NSError(domain: desc, code:code, userInfo: nil)
                
            }
            else{
                if error.localizedDescription == "cancelled"{
                    return error as NSError
                }
                return NSError(domain: desc, code:500, userInfo: nil)
                
            }
        }
    }
    
    //MARK:- Reset Password API Function
    
    func ForgotPasswordApiRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void) {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            let JSON = response.result.value as AnyObject
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                //completion(JSON as AnyObject,nil)
                let userDetails  = UserLoginResponse.init(fromDictionary:(JSON.object(forKey: kResponseInfo) as! NSDictionary) as! [String : Any])
                 DefaultsValues.setCustomObjToUserDefaults(userDetails, forKey: kUserInfo)
                 completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK:- Reset Password API Function
    
    func ResetPasswordApiRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void) {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            let JSON = response.result.value as AnyObject
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
                /*let userDetails  = UserLoginResponse.init(fromDictionary:(JSON.object(forKey: kResponseInfo) as! NSDictionary) as! [String : Any])
                 DefaultsValues.setCustomObjToUserDefaults(userDetails, forKey: kUserInfo)
                 completion(JSON as AnyObject,nil)*/
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK:- Send OTP On Forgot Password API Function
    
    func GetOTPApiRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void) {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            let JSON = response.result.value as AnyObject
            if response.result.value != nil
            {
                /*if let error = self.verifyResponse(response)
                {
                    completion(JSON as AnyObject,error)
                }
                else
                {
                    completion(JSON as AnyObject,nil)
                }*/
                completion(JSON as AnyObject,nil)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK:- Registration Verify OTP API Function
    
    func GetInvoiceList(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            //completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            if let error = self.verifyResponse(response)
            {
                let JSON = response.result.value as AnyObject
                completion(JSON as AnyObject,error)
            }
            else
            {
                //let dataDict = response.result.value! as! NSDictionary
                
                let JSON = response.result.value! as AnyObject
                let dataDict = response.result.value! as! NSDictionary
                let dict1 = dataDict.value(forKey: "response") as? NSArray
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA Of Invoice --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK:- Registration Verify OTP API Function //9826272531
    
    func VerifyOTPApiRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            //completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            let JSON = response.result.value as AnyObject
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    
    //MARK:- Registration Get OTP API Function
    
    func RegisterGetOTPApiRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (String) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            //completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in

            if let data = response.result.value as? [[String:Any]], !data.isEmpty,
            let strMessage = data[0]["Value"] as? String
            {
                completion(strMessage)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK:- Registration API Function
    
    func RegisterApiRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void) {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            let JSON = response.result.value as AnyObject
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                /*let userDetails  = UserLoginResponse.init(fromDictionary:(JSON.object(forKey: kResponseInfo) as! NSDictionary) as! [String : Any])
                DefaultsValues.setCustomObjToUserDefaults(userDetails, forKey: kUserInfo)*/
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    // MARK:- LOGIN API Function
    func loginApiRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        //self.webServiceClient.hitWebServiceWithGET(String(format: "%@%@",self.baseURL,url)) { (response) in
        //print("BaseUrl: %@%@", self.baseURL,url)
            
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("dictHeaders: ", dataHeader!)
            
            let JSON = response.result.value as AnyObject
            //if response != nil
            //{
                if let error = self.verifyResponse(response)
                {
                    completion(JSON as AnyObject,error)
                }
                else
                {
                    completion(JSON as AnyObject,nil)
                }
            //}
            self.isOngoingCall = false
        }
    }
    
    func userSocialRegistrationRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            completion(response as AnyObject,nil)
            //let JSON = response.result.value! as AnyObject
            /*if let error = self.verifyResponse(response)
            {
                completion(response as AnyObject,error)
            }
            else
            {
                completion(response as AnyObject,nil)
            }*/
            self.isOngoingCall = false
        }
    }
    
    func userSocialLoginRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            
            let JSON = response.result.value as AnyObject
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let userDetails  = UserLoginResponse.init(fromDictionary:(JSON.object(forKey: kResponseInfo) as! NSDictionary) as! [String : Any])
                DefaultsValues.setCustomObjToUserDefaults(userDetails, forKey: kUserInfo)
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getEmployeeProfileDetail(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            //completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        //self.webServiceClient.hitWebServiceWithGET(String(format: "%@%@",self.baseURL,url)) { (response) in
        //print("BaseUrl: %@%@", self.baseURL,url)
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("dictHeaders: ", dataHeader!)
            
            let JSON = response.result.value as AnyObject
            //if response != nil
            //{
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dataDict = response.result.value! as! NSDictionary
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                completion(JSON as AnyObject,nil)
            }
            //}
            self.isOngoingCall = false
        }
    }
    
    //MARK: - API For Subscription Dashboard
    
    func fetchSubscriptionList(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        print("url: ", url)
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            
            if let error = self.verifyResponse(response)
            {
                //                let JSON = response.result.value! as AnyObject
                //                completion(JSON as AnyObject,error)
                completion(response as AnyObject,error)
            }
            else
            {
                let JSON = response.result.value! as AnyObject
                let dataDict = response.result.value! as! NSDictionary
                let dict1 = dataDict.value(forKey: "response") as? NSArray
                
                let dataDict1 = response.result.value! as! NSDictionary
                
                if dataDict1 != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict1.value(forKey: "response")!, options: [])
                    print("\n\(Date()) JSON DATA --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                completion(dataDict1 as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    
    // MARK:- API For Validity Of Documents
    
    func checkValidityOfAdhaarCard(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dataDict = response.result.value! as! NSDictionary
                
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                let strJson = self.jsonToString(json: dataDict)
                DefaultsValues.setStringValueToUserDefaults(strJson, forKey: kAdhaarCardJsonStr)
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func checkValidityOfPassport(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dataDict = response.result.value! as! NSDictionary
                 
                 if dataDict != nil {
                 let data = try! JSONSerialization.data(withJSONObject: dataDict, options: []) //dataDict.value(forKey: "msg")!
                 print("\n\(Date()) JSON DATA --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                 }
                 let strJson = self.jsonToString(json: dataDict)
                 DefaultsValues.setStringValueToUserDefaults(strJson, forKey: kPassportJsonStr)
                
                 completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func checkValidityOfDrivingLicence(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dataDict = response.result.value! as! NSDictionary
                
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                let strJson = self.jsonToString(json: dataDict)
                DefaultsValues.setStringValueToUserDefaults(strJson, forKey: kDrivingLicenceJsonStr)
                
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func checkValidityOfPanCard(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
                        
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dataDict = response.result.value! as! NSDictionary
                
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                let strJson = self.jsonToString(json: dataDict)
                DefaultsValues.setStringValueToUserDefaults(strJson, forKey: kPanCardJsonStr)
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getCreditScoreAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        print("url: ", url)
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)            
            
            if let error = self.verifyResponse(response)
            {
//                let JSON = response.result.value! as AnyObject
//                completion(JSON as AnyObject,error)                
                completion(response as AnyObject,error)
            }
            else
            {
                let JSON = response.result.value! as AnyObject
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    // MARK:- API For Document Upload
    
    func  uploadDocumentAuthBridgeExperian(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK: Authorize Payment Api
    
    func callPaymentAPI(_ url:String!,completion: @escaping (AnyObject, NSError?) -> Void) {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: nil, header: nil) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                self.saveDataForRequest(response.result.value! as AnyObject)
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    
    func  reCheckCarAvailability(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func CreateNewSubscriptionForEmployee(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK :- Authorize Payment
    
    func authorizePaymentUrl(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
       {
           if self.networkReachability?.isReachable == false {
               completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
               return
           }
           if self.isOngoingCall == true
           {
               self.cancelLastRequest()
           }
           self.isOngoingCall = true
           
           self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
               //print("response: ", response)
               let JSON = response.result.value as AnyObject
               
               if let error = self.verifyResponse(response)
               {
                   completion(JSON as AnyObject,error)
               }
               else
               {
                   //self.saveDataForRequest(response.result.value! as AnyObject)
               
                   //let userDetails  = UserLoginResponse.init(fromDictionary:(JSON.object(forKey: kResponseInfo) as! NSDictionary) as! [String : Any])
                   //DefaultsValues.setCustomObjToUserDefaults(userDetails, forKey: kSearchCarList)
                   /*let dict = response.result.value! as! NSDictionary
                   if dict != nil
                   {
                       let Json = dict.value(forKey: "response") as! NSDictionary
                       let arrCarList = Json.value(forKey: "Cars") as! NSArray
                       APIFunctions.arrGlobalCarList = arrCarList
                       let dictFilterList = Json.value(forKey: "Fitlers") as! NSDictionary
                       
                       DefaultsValues.setDictionaryValueToUserDefaults(dictFilterList, forKey: kFilterData)
                       APIFunctions.dictGlobalCarFilter = dictFilterList
                   }*/
                   
                   completion(JSON as AnyObject,nil)
               }
               self.isOngoingCall = false
           }
       }
    
    // MARK:- API Function

    func searchCarForSubscribeRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true
        {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                
                let dataDict = response.result.value! as! NSDictionary
                
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA  Of Search Car --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                //self.saveDataForRequest(response.result.value! as AnyObject)
            
                //let userDetails  = UserLoginResponse.init(fromDictionary:(JSON.object(forKey: kResponseInfo) as! NSDictionary) as! [String : Any])
                //DefaultsValues.setCustomObjToUserDefaults(userDetails, forKey: kSearchCarList)
                /*let dict = response.result.value! as! NSDictionary
                if dict != nil
                {
                    let Json = dict.value(forKey: "response") as! NSDictionary
                    let arrCarList = Json.value(forKey: "Cars") as! NSArray
                    APIFunctions.arrGlobalCarList = arrCarList
                    let dictFilterList = Json.value(forKey: "Fitlers") as! NSDictionary
                    
                    DefaultsValues.setDictionaryValueToUserDefaults(dictFilterList, forKey: kFilterData)
                    APIFunctions.dictGlobalCarFilter = dictFilterList
                }*/
                
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func CarSubcriptionCompleteDetailRequest(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            let dataDict = response.result.value! as! NSDictionary
           // let dataDict1 = dataDict.value(forKey: "carDetail") as? NSDictionary
            /*if dataDict != nil {
                let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                print("\n\(Date()) Response In Json --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
            }*/
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dict1 = dataDict.value(forKey: "response") as? NSDictionary
                let arr = dict1?.allValues as NSArray?
                
                APIFunctions.dictSubscriptionDetailModel = arr?[0] as? NSDictionary
                
                let dictttt = arr?[0] as? NSDictionary
                
                if dictttt != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dictttt, options: [])
                    print("\n\(Date()) JSON DATA  Of Car Detail --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                
                /*var newData = [String:AnyObject]()
                newData = response.result.value! as! [String : AnyObject]
                let data = newData["response"]
                let aaaa = data?["carDetail"]
                if let data = newData["response"]!["carDetail"]!
                {
                    print("data: ", data)
                }*/
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK:- Additional Documents API
    
    func userAdditonalDocumentListAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getSubscriptionDoc(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dataDict = response.result.value! as! NSDictionary
                
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA  Of Additonal Doc --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func createUpdareSubscriptionAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK: - Report Lost API
    func reportLostItemAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    // MARK:- API For Document Upload
    
    func searchCarForUpgradeSubscription(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true
        {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                
                let dataDict = response.result.value! as! NSDictionary
                
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA  Of Search Car --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                //self.saveDataForRequest(response.result.value! as AnyObject)
            
                //let userDetails  = UserLoginResponse.init(fromDictionary:(JSON.object(forKey: kResponseInfo) as! NSDictionary) as! [String : Any])
                //DefaultsValues.setCustomObjToUserDefaults(userDetails, forKey: kSearchCarList)
                /*let dict = response.result.value! as! NSDictionary
                if dict != nil
                {
                    let Json = dict.value(forKey: "response") as! NSDictionary
                    let arrCarList = Json.value(forKey: "Cars") as! NSArray
                    APIFunctions.arrGlobalCarList = arrCarList
                    let dictFilterList = Json.value(forKey: "Fitlers") as! NSDictionary
                    
                    DefaultsValues.setDictionaryValueToUserDefaults(dictFilterList, forKey: kFilterData)
                    APIFunctions.dictGlobalCarFilter = dictFilterList
                }*/
                
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func upgradeSubscriptionAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func subscriptionTerminationAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func subscriptionTerminationRequestAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func renewSubscriptionRequestAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func checkPriceForRenewSubscriptionAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func subscriptionCallbackRequestAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func updateUpcomingServiceSlots(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func createServiceRepairAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getScheduleServiceHistory(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                let dataDict = response.result.value! as! NSDictionary
                
                if dataDict != nil {
                    let data = try! JSONSerialization.data(withJSONObject: dataDict, options: [])
                    print("\n\(Date()) JSON DATA  Of Schedule History --> \(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))")
                }
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getCallbackReasonAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getLossItemAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getRequestForUpgradeSubscriptionAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func getKmAndTenureAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func uploadSingDocumentAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders? ,completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: dataDict, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    func fetchCityListAPI(_ url:String!, dataDict:[String:AnyObject]!, dataHeaders:HTTPHeaders?, completion: @escaping (AnyObject, NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        
        self.webServiceClient.hitWebServiceWithPost(String(format: "%@",url), postData: nil, header: dataHeaders) { (response) in
            //print("response: ", response)
            let JSON = response.result.value as AnyObject
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                self.saveDataForRequest(response.result.value! as AnyObject)
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    /*func loginApiRequest1(_ url:String!, completion: @escaping (NSError?) -> Void) {
        if self.networkReachability?.isReachable == false {
            completion(NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        self.webServiceClient.hitWebServiceWithGET(String(format: "%@%@",APIConstants.API_UserLogin,url)) { (response) in
            if let error = self.verifyResponse(response) {
                completion(error)
            } else {
                self.saveDataForRequest(response.result.value! as AnyObject)
                completion(nil)
            }
            self.isOngoingCall = false
        }
    }*/
    
    func getAccessTokenAPI(_ url:String!,dataDict:[String:AnyObject]!,completion: @escaping (AnyObject,NSError?) -> Void)
    {
        if self.networkReachability?.isReachable == false {
            completion([] as AnyObject,NSError(domain: Constants.kNetworkUnavailableKey, code: 440, userInfo: nil))
            return
        }
        if self.isOngoingCall == true {
            self.cancelLastRequest()
        }
        self.isOngoingCall = true
        //self.webServiceClient.hitWebServiceWithGET(String(format: "%@%@",self.baseURL,url)) { (response) in
        
        self.webServiceClient.hitWebServiceWithPost1(String(format: "%@",url), postData: dataDict, header: nil) { (response) in
              let JSON = response.result.value as AnyObject
            
            if let error = self.verifyResponse(response)
            {
                completion(JSON as AnyObject,error)
            }
            else
            {
                self.saveDataForRequest(response.result.value! as AnyObject)
                completion(JSON as AnyObject,nil)
            }
            self.isOngoingCall = false
        }
    }
    
    //MARK:- Convert JSON To String
    func jsonToString(json: AnyObject) -> String{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            print("jsonToString: ", convertedString) // <-- here is ur string
            return convertedString ?? ""
            
        } catch let myJSONError {
            print(myJSONError)
        }
        return ""
    }
    
    
    //MARK:- Cancel Last Request
    func cancelLastRequest() {
        self.webServiceClient.cancelRequest()
    }
    
    //MARK:- Save Data For Login Request
    func saveDataForRequest(_ JSON:AnyObject)
    {
        if let response = JSON.object(forKey: Constants.kResponseData) as? NSDictionary {
            //print("response: ", response)
            
            /*self.cabsModel.isCompanyAlreadyRegistered = (response.object(forKey: "is_cor_registered_company") as? Bool)?.hashValue
            print(self.cabsModel.isCompanyAlreadyRegistered)
            
            if let corpProfiles = response.object(forKey: "available_corp_profiles") as? Array<NSDictionary> {
                self.cabsModel.availableCorpProfiles = corpProfiles
            }*/
        }
    }
    
    func getDataForRequest() -> NSDictionary
    {
        let jsonData = NSDictionary()
        return jsonData
    }

}

extension String {
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
