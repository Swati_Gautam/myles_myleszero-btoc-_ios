//
//  Constants.swift
//  Myles Zero
//
//  Created by skpissay on 09/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    //MARK:- Custom Keys
    static let GOOGLE_SIGNIN_CLIENTID = "741244143055-vfs74leet5vf4apuc1fppjknhsq9p1bt.apps.googleusercontent.com"
    static let AWS_ACCESS_KEY = "AKIAJM3EUJGYK546JWCA"
    static let AWS_SECRET_KEY = "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"
    
    #if DEBUG
        static let AWS_S3_BUCKET_NAME = "uploadmylestest"
    #else
        static let AWS_S3_BUCKET_NAME = "uploadmyles"
    #endif
    
    static let FACEBOOK_AUTH_ID = ""
    
    //MARK:- Screen Height And Width
    
    static let WIDTH_OF_SCREEN = UIScreen.main.bounds.size.width
    static let HEIGHT_OF_SCREEN = UIScreen.main.bounds.size.height
    
    // MARK:- Storyboard Names -
    
    //static let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let subscribeStoryboard: UIStoryboard = UIStoryboard(name: "Subscribe", bundle: nil)
    static let sideMenuStoryboard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
    static let userProfileStoryboard: UIStoryboard = UIStoryboard(name: "UserProfile", bundle: nil)
    
    // MARK:- View Controller Names -
    
    static let VC_Login: String = "idLoginVC"
    static let VC_SearchCar: String = "idSearchCarVC"
    
    static let VC_ChangePassword: String = "idChangePasswordVC"
    static let VC_DocumentsEdit: String = "idDocumentsEditVC"
    
    static let VC_DocumentsDetail: String = "idDocumentsDetailVC"
    static let VC_SubmitDocuments: String = "idSubmitDocumentsVC"
    static let VC_SubscriptionRequest: String = "idSubscriptionRequestVC"
    
    static let VC_CarList: String = "idCarListVC"
    static let VC_SideMenu: String = "idSideMenuVC"
    static let VC_SearchTableVC: String = "idSearchCityVC"
    static let VC_SearchCityState: String = "idSearchCityStateVC"
    static let VC_ViewUploadedDocumentVC: String = "idViewUploadedDocumentVC"
    
    static let VC_FilterBy: String = "idFilterByVC"
    static let VC_CarSubscriptionDetail: String = "idCarSubscriptionDetailVC"
    
    static let VC_EnterContactNo: String = "idEnterContactNoVC"
    static let VC_Registration: String = "idRegistrationVC"
    static let VC_TermsAndConditions: String = "idTermsAndConditionsVC"
    static let VC_MylesZeroTermsVC: String = "idMylesZeroTermsVC"
        
    static let VC_EnterOTPForForgotPwd: String = "idEnterOTPForForgotPwdVC"
    static let VC_ForgotPassword: String = "idForgotPasswordVC"
    static let VC_InvoiceDetail: String = "idInvoiceDetailVC"
    
    static let VC_SubscriptionActive: String = "idSubscriptionActiveVC"
    //static let VC_UserProfile: String = "idUserProfileVC"
    static let VC_UserProfileMylesZero: String = "idUserProfileMylesZeroVC"
    static let VC_UserAccountDetails: String = "idUserAccountDetailsVC"
    static let VC_UserProfileAccountVC: String = "idUserProfileAccountVC"
    static let VC_CarOverview: String = "idCarOverviewVC"
    static let VC_UserProfileDocuments: String = "idUserProfileDocumentsVC"
    static let VC_CarTabVC: String = "idCarTabVC"
    static let VC_SubscriptionTabVC: String = "idSubscriptionTabVC"
    static let VC_ServiceTabVC: String = "idServiceTabVC"
    static let VC_SubscriptionStatus: String = "idSubscriptionStatusVC"
    static let VC_DocumentAndPaymentVC: String = "idDocumentAndPaymentVC"
    static let VC_SubscriptionList: String = "idSubscriptionListVC"
    static let VC_WebView: String = "idWebViewVC"
    static let VC_PaymentWebView: String = "idPaymentWebViewVC"
    static let VC_SignAgreementVC: String = "idSignAgreementVC"
    static let VC_AuthorizePayment: String = "idAuthorizePaymentVC"
    static let VC_AdditionalDocuments: String = "idAdditionalDocumentsVC"
    static let VC_Myles: String = "idMylesVC"
    static let VC_TrackCar: String = "idTrackCarVC"
    static let VC_Faq: String = "idFaqVC"

    //MARK:- Errors Constants
    class var kNetworkUnavailableKey:String {return "Network Unavailable"}
    class var kUnknownErrorKey:String {return "The data couldn't be read because it isn't in the correct format"}
    
    class var kMetaDataKeyConstant:String { return "meta_data"}
    class var kResponseData:String { return "response"}
    class var kServicesArrKeyConstant:String { return "services"}
    class var kVerifyOTPSegueConstant:String { return "VerifyOTP"}
    class var kNewUserSegueConstant:String { return "NewUserVC"}
    class var kPhoneNumCharSetConstant:String { return "0123456789"}
    class var kOKConstant:String { return "OK"}
    class var kYesConstant:String { return "YES"}
    class var kNoConstant:String { return "NO"}
    class var kPHNumMaxCount:Int { return 10 }
    
    class var kHTTPStatusConstant:String { return "http_status" }
    class var kHTTPStatusErrMsgConstant:String { return "error_message" }
    class var kHTTPInvalidMsgConstant:String { return "Invalid User Authentication" }
    
    
    
    
    // MARK: - RGB COLORS -
    
    //static let GREY_COLOR : UIColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
    
    static let GREY_COLOR : UIColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
    
    static let GREY_COLOR_1 : UIColor = UIColor(red: 193.0/255.0, green: 193.0/255.0, blue: 197.0/255.0, alpha: 1.0)
    
    static let LIGHT_GREY_COLOR : UIColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
    
    static let RED_COLOR : UIColor = UIColor(red: 207.0/255.0, green: 60.0/255.0, blue: 44.0/255.0, alpha: 1.0)
    
    static let RED_COLOR_1 : UIColor = UIColor(red: 102.0/255.0, green: 21.0/255.0, blue: 31.0/255.0, alpha: 1.0)
    
    //MARK: - HEXA COLOR CODE
    static let RED_HEX_COLOR = "CF3C2C"
    
    
    //MARK: - FONTS -
    
    static let Roboto_Medium_10 : UIFont  =  UIFont(name: "Roboto-Medium", size: 10.0)!
    static let Roboto_Medium_11 : UIFont  =  UIFont(name: "Roboto-Medium", size: 11.0)!
    
    static let Roboto_Thin_10 : UIFont  =  UIFont(name: "Roboto-Thin", size: 10.0)!
    static let Roboto_Thin_11 : UIFont  =  UIFont(name: "Roboto-Thin", size: 11.0)!
    
    static let Roboto_Regular_10 : UIFont  =  UIFont(name: "Roboto-Regular", size: 10.0)!
    static let Roboto_Regular_11 : UIFont  =  UIFont(name: "Roboto-Regular", size: 11.0)!
    
    static let Roboto_Bold_10 : UIFont  =  UIFont(name: "Roboto-Bold", size: 10.0)!
    static let Roboto_Bold_11 : UIFont  =  UIFont(name: "Roboto-Bold", size: 11.0)!
    
    static let Roboto_Light_10 : UIFont  =  UIFont(name: "Roboto-Light", size: 10.0)!
    static let Roboto_Light_11 : UIFont  =  UIFont(name: "Roboto-Light", size: 11.0)!
    
    
    
    
    
    //let kCurrentLocation : String  = "kUserCurrentLocation";

}



