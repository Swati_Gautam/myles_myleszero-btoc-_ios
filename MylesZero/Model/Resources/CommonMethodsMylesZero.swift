//
//  CommonMethodsMylesZero.swift
//  Myles
//
//  Created by Swati Gautam on 08/03/21.
//  Copyright © 2021 Myles. All rights reserved.
//

import UIKit
import CommonCrypto
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG
import JGProgressHUD

class CommonMethodsMylesZero: NSObject
{
    static let MAX : UInt32 = 9
    static let MIN : UInt32 = 1
    //static let TimeStamp = String(format: "%.0f", Date().timeIntervalSince1970 * 1000)
    
    class func showAlertController1(strTitle:String, strMsg:String, strAction1: String, strAction2: String, viewController:UIViewController)
    {
        let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
        
        //let addBtn = UIAlertAction(title: "ADD LOCATION", style:.default, handler: {(_ action: UIAlertAction) -> Void in
        
            let OKAction = UIAlertAction(title: strAction1, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            })
            let cancelAction = UIAlertAction(title: strAction2, style:.destructive, handler: {(_ action: UIAlertAction) -> Void in
            })
            
            alert.addAction(OKAction)
            alert.addAction(cancelAction)
            
            viewController.parent!.present(alert, animated: true, completion: nil)
        //}
    }
    
    class func showDateTimePicker(datePickerView:UIDatePicker, datePickerMode:UIDatePicker.Mode, viewController: UIViewController)
    {
        var dateString : String?
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "Please select the date", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        
        datePickerView.frame = CGRect(x: 0, y: 40, width: viewController.view.frame.size.width, height: 180)
        datePickerView.tag = 444
        //pickerFrame.delegate = self
        alert.view.addSubview(datePickerView)
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        //datePickerView.minimumDate = Date()
        datePickerView.isHidden = false
        //datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
        
        let selectAction = UIAlertAction(title: "Select", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //Perform Action
            let dateformatter1 = DateFormatter()
            dateformatter1.dateFormat = "dd/MM/yyyy"
            dateString = dateformatter1.string(from: datePickerView.date)
        })
        alert.addAction(selectAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant:350) //self.view.frame.height * 0.50 //0.80
        alert.view.addConstraint(height);
        //viewController.present(alert, animated: true, completion: nil)
        viewController.parent!.present(alert, animated: true, completion: nil)
    }
    
    class func showDateTimePicker1(datePickerView:UIDatePicker, datePickerMode:UIDatePicker.Mode, viewController: UIViewController) -> String
    {
        var dateString : String?
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "Please select the date", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        
        datePickerView.frame = CGRect(x: 0, y: 40, width: viewController.view.frame.size.width, height: 180)
        datePickerView.tag = 444
        //pickerFrame.delegate = self
        alert.view.addSubview(datePickerView)
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        //datePickerView.minimumDate = Date()
        datePickerView.isHidden = false
        //datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
        
        let selectAction = UIAlertAction(title: "Select", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //Perform Action
            let dateformatter1 = DateFormatter()
            dateformatter1.dateFormat = "dd/MM/yyyy"
            dateString = dateformatter1.string(from: datePickerView.date)
        })
        alert.addAction(selectAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant:350) //self.view.frame.height * 0.50 //0.80
        alert.view.addConstraint(height);
        //viewController.present(alert, animated: true, completion: nil)
        viewController.parent!.present(alert, animated: true, completion: nil)
        
        return (dateString ?? "")
    }
    
    class func showTimePicker(timePickerView:UIDatePicker, viewController: UIViewController)
    {
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "Please select the time", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        
        timePickerView.frame = CGRect(x: 0, y: 40, width: viewController.view.frame.size.width, height: 180)
        timePickerView.tag = 555
        alert.view.addSubview(timePickerView)
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        timePickerView.isHidden = false
        
        let selectAction = UIAlertAction(title: "Select", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //Perform Action
        })
        alert.addAction(selectAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant:350) //self.view.frame.height * 0.50 //0.80
        alert.view.addConstraint(height);
        //viewController.present(alert, animated: true, completion: nil)
        viewController.parent!.present(alert, animated: true, completion: nil)
    }

    
    class func showAlertController2(strTitle:String, strMsg:String, strAction1: String, viewController:UIViewController)
    {
        let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
        
        //let addBtn = UIAlertAction(title: "ADD LOCATION", style:.default, handler: {(_ action: UIAlertAction) -> Void in
        
        let OKAction = UIAlertAction(title: strAction1, style: .default, handler: {(_ action: UIAlertAction) -> Void in
        })
        
        alert.addAction(OKAction)
        
        viewController.parent!.present(alert, animated: true, completion: nil)
        //}
    }
    
    class func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?], viewController:UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        viewController.parent!.present(alert, animated: true, completion: nil)
    }
    
    class func setBorder(button: UIButton)
    {
        button.layer.cornerRadius = 4.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.black.cgColor
    }
    
    class func getCurrentDate() -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        return dateFormatter.string(from: Date())
        
    }
    
    class func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
    
    class func randomNumberForCard() -> Int
    {
        let random_number = Int(arc4random_uniform(MAX) + MIN)
        //print ("random = ", random_number);
        return random_number
    }
    
    class func randomNumber(lower: Double = 0, _ upper: Double = 100) -> Double {
        return (Double(arc4random()) / 0xFFFFFFFF) * (upper - lower) + lower
    }
    
    class func currentTimeInMiliseconds() -> Int {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
    
    class func generateSessionID() -> String? {
        let randomID = Int(arc4random() % 9000 + 1000)
        let TimeStamp = String(format: "%.0f", Date().timeIntervalSince1970 * 1000)
        let timestampWithRandomNumber = "\(TimeStamp)\(randomID)"
        let sessionID = "sess_\(timestampWithRandomNumber)_ios"
        print("sessionid = \(sessionID)")
        return sessionID
    }
    
    class func MD5(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
    class func showProgressHudDark(view:UIView) -> JGProgressHUD?
    {
        let hud = JGProgressHUD(style: .light)
        hud.indicatorView?.tintColor = UIColor.black
        hud.textLabel.text = "Loading"
        hud.show(in:view)
        return hud
    }
    
    class func showProgressHudLight(view:UIView) -> JGProgressHUD?
    {
        let hud = JGProgressHUD(style: .dark)
        hud.indicatorView?.tintColor = UIColor.white
        hud.textLabel.text = "Loading"
        hud.show(in:view)
        return hud
    }
    
    class func getHeaderValues(sessionId : String, customerId : String, hasValue : String) -> [String: String]?
    {
        //"cache-control": "no-cache",
        let dictHeaders : [String:String] = ["Content-Type":"application/json",
                                             "sessionID": sessionId,
                                             "customerID":customerId,
                                             "hashVal":hasValue]
        return dictHeaders
    }
    /*class func md5(string: String) -> String {
        var digest = [UInt8](_unsafeUninitializedCapacity: Int(CC_MD5_DIGEST_LENGTH), initializingWith: 0)
        if let data = string.data(usingEncoding: String.Encoding.utf8) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }*/

}
