//
//  GlobalModelKeys.swift
//  Myles Zero
//
//  Created by skpissay on 04/09/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import Foundation

//MARK:- Constant Variables

let kCustomerCoID: Int = 2205
let kCustomerID: String = "-99"

let kAWSLiveFolderName: String = "uploadmyles"


//MARK:- User Default Custom Object Keys

let kAccessToken: String = "accessToken"
let kSessionID: String = "sessionID"


let kCityList:String = "city_list"
let kCityListArray:String = "arr_city_list"
let kCarTypeList:String = "car_type_list"
let kTenureList:String = "arr_tenure_list"
let kKmList:String = "arr_km_list"
let kHttpHeaders:String = "http_headers"

let kStateList:String = "state_list"
let kStateListArray:String = "arr_state_list"
let kIsMylesZeroClicked:String = "is_myles_zero_clicked"


//MARK:- Saving Model Class Custom Object Keys

let kResponseInfo:String = "response"

let kIsLogout:String = "is_logout"

let kUserInfo:String = "user_info"
let kSearchCarList:String = "search_car"
let kGoogleSignInInfo:String = "google_signin"
let kTenure:String = "tenure_data"
let kFilterData:String = "filter_data"
let kAllFilterData:String = "all_filter_data"
let kUserProfileDetails:String = "user_profile_info"
let kAdhaarCardData:String = "adhaar_card"
let kDrivingLicenceData:String = "driving_licence"
let kPanCardData:String = "pan_card"
let kPassportData:String = "passport_data"
let kSubscriptionDetail:String = "subscription_detail"

let kAdhaarCardJsonStr:String = "adhaar_card_str"
let kDrivingLicenceJsonStr:String = "driving_licence_str"
let kPanCardJsonStr:String = "pan_card_str"
let kPassportJsonStr:String = "passport_str"
let kSearchParamKey:String = "search_param"
let kSubscriptionList:String = "subscription_list"
let kInvoiceList:String = "invoice_list"
let kSelectedInvoiceList:String = "selected_invoice_list"
let kDocuments:String = "invoice_list"

//MARK:- Placeholder Image String Key

let kCarPlaceholder:String = "car_placeholder"

var kDictHeaders = [String: String]()

let strDefaultAccessToken = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAccessToken)
let strDefaultSessionID = DefaultsValues.getStringValueFromUserDefaults_(forKey: kSessionID)

