//
//  UserProfileDocumentsVC.swift
//  Myles Zero
//
//  Created by skpissay on 13/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import SDWebImage
import AWSS3
import AWSCore
import Alamofire

class UserProfileDocumentsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnPassportRadio: UIButton!
    @IBOutlet var btnAdhaarRadio: UIButton!
    @IBOutlet var txtAdhaarNo: UITextField!
    
    @IBOutlet var btnDrivingDOB: UIButton!
    @IBOutlet var btnAdhaarDOB: UIButton!
    @IBOutlet var txtAdhaarName: UITextField!
    
    @IBOutlet var lblAdhaarImageName: UILabel!
    @IBOutlet var lblDrivingImageName: UILabel!
    @IBOutlet var lblPanCardImageName: UILabel!
    @IBOutlet var lblDrivingDOB: UILabel!
    @IBOutlet var lblDLGender: UILabel!
    @IBOutlet var txtDrivingLicenceNo: UITextField!
    @IBOutlet var lblAdhaarDOB: UILabel!
    
    @IBOutlet var txtPanCardNo: UITextField!
    
    @IBOutlet var btnUploadAdhaar: UIButton!
    @IBOutlet var btnUploadPanCard: UIButton!
    @IBOutlet var btnUploadDrivingLicence: UIButton!
    
    var isAdhaarSelected = false
    var strTypeOfDocument : String?
    var datePickerView = UIDatePicker()
    var imageURL = NSURL()
    var uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var uploadFileURL: NSURL?
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var viewAdhaarPassportContent: UIView!
    var strAdhaarBirthYear : String?
    
    var viewPassportView = PassportView()
    var dictAdhaarInfo : [String:AnyObject]?
    var dictDrivingLicenceInfo : [String:AnyObject]?
    var dictPanCardInfo : [String:AnyObject]?
    var dictPassportInfo : [String:AnyObject]?
    var uploadButtonTag:Int = 0
    var dateOfBirthTag:Int = 0
    var strAmazonBucketPath : String?
    
    var strAdhaarFile : String?
    var strDLFile : String?
    var strPanFile : String?
    var strAmazonBucketFilePath : String?
    var strCoricID : String?
    var strDocType : String?
    var arrStateList = NSArray()
    var strStateCode : String?
    var strDLExpiryDate : String?
    var strDocumentUrl : String?
    var strPassport : String?
    var strAdhaarDOB : String?
    
    var strAdhaarUrl : String?
    var strDLUrl : String?
    var strPancardUrl : String?
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    
    @IBOutlet weak var btnViewAdhaarFile: UIButton!
    @IBOutlet weak var btnViewDLFile: UIButton!
    @IBOutlet weak var btnViewPanFile: UIButton!
    
    override func viewDidLoad()
    {
        strTypeOfDocument = "Adhaar"
        strAdhaarUrl = ""
        strDLUrl = ""
        strPancardUrl = ""
        strStateCode = ""
        strAdhaarFile = ""
        strDLFile = ""
        strPanFile = ""
        strPassport = ""
        strAdhaarDOB = ""
        strAmazonBucketFilePath = ""
        
        CommonMethodsMylesZero.setBorder(button: self.btnUploadAdhaar)
        CommonMethodsMylesZero.setBorder(button: self.btnUploadDrivingLicence)
        CommonMethodsMylesZero.setBorder(button: self.btnUploadPanCard)
        //self.callApiToGetEmployeeDetail()
        self.setDocumentDetails()
        imagePicker.delegate = self
        super.viewDidLoad()
       
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    func setDocumentDetails()
    {
        //self.lblDrivingDOB.text = "Expiry Date"
        //self.lblDrivingDOB.textColor = UIColor.lightGray
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
        if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse != nil
        {
            self.btnViewAdhaarFile.isHidden = false
            self.btnViewDLFile.isHidden = false
            self.btnViewPanFile.isHidden = false
            
            self.txtAdhaarNo.text = UserProfileInfo?.aadharno
            self.txtAdhaarName.text = UserProfileInfo?.aadharname
            //self.lblAdhaarCardDOB.text = UserProfileInfo.aadhardob
            //self.lblAdhaarDOB.text = UserProfileInfo.aadhardob
            
            if UserProfileInfo?.aadhardob != ""
            {
                if UserProfileInfo?.aadhardob == "1/1/1900 12:00:00 AM"
                {
                    self.lblAdhaarDOB.text = "Date Of Birth"
                    self.lblAdhaarDOB.textColor = Constants.GREY_COLOR_1
                    self.strAdhaarDOB = ""
                }
                else
                {
                    let arr11 = UserProfileInfo?.aadhardob?.components(separatedBy: " ")
                    let arr22 = arr11?[0].components(separatedBy: "/")
                    self.strAdhaarBirthYear = arr22?[2]//objUserProfileInfo.aadhardob!//"1978"
                    self.lblAdhaarDOB.textColor = UIColor.black
                    self.lblAdhaarDOB.text = (arr11?[0] ?? "") as String//"04/05/1978"
                    self.strAdhaarDOB = self.lblAdhaarDOB.text
                }
            }
            else
            {
                self.lblAdhaarDOB.text = "Date Of Birth"
                self.strAdhaarDOB = ""
                self.lblAdhaarDOB.textColor = Constants.GREY_COLOR_1
            }
            
            if UserProfileInfo?.aadharfile != ""
            {
                let arr22 = UserProfileInfo?.aadharfile.components(separatedBy: "/")
                self.lblAdhaarImageName.text = (arr22?.last ?? "") as String //UserProfileInfo.aadharfile
                self.strAdhaarUrl = UserProfileInfo?.aadharfile
                self.strAdhaarFile = self.lblAdhaarImageName.text
            }
            else
            {
                self.strAdhaarUrl = ""
                self.strAdhaarFile = ""
            }
            if UserProfileInfo?.passportfile != ""
            {
                let arr22 = UserProfileInfo?.passportfile.components(separatedBy: "/")
                //self.lblpa.text = (arr22.last ?? "") as String //UserProfileInfo.aadharfile
                self.strPassport = (arr22?.last ?? "") as String //UserProfileInfo.aadharfile
                
            }
            else
            {
                self.strPassport = ""
            }
            if UserProfileInfo?.dlfile != ""
            {
                let arr22 = UserProfileInfo?.dlfile.components(separatedBy: "/")
                self.lblDrivingImageName.text = (arr22?.last ?? "") as String //UserProfileInfo.aadharfile
                self.strDLUrl = UserProfileInfo?.dlfile
                self.strDLFile = self.lblDrivingImageName.text //arr22.last
            }
            else
            {
                self.strDLUrl = ""
                self.strDLFile = ""
            }
            if UserProfileInfo?.panfile != ""
            {
                let arr22 = UserProfileInfo?.panfile.components(separatedBy: "/")
                self.lblPanCardImageName.text = (arr22?.last ?? "") as String //UserProfileInfo.aadharfile
                self.strPancardUrl = UserProfileInfo?.panfile
                self.strPanFile = self.lblPanCardImageName.text//arr22.last
            }
            else
            {
                self.strPancardUrl = ""
                self.strPanFile = ""
            }
            
            //self.imgViewAdhaarCard.sd_setImage(with: URL(string: UserProfileInfo.aadharfile), placeholderImage: UIImage(named: "car_placeholder"))
           
            self.txtDrivingLicenceNo.text = UserProfileInfo?.dlNo
            
            self.lblDrivingDOB.textColor = UIColor.black
            /*if UserProfileInfo?.dlexpirydate! !=  ""
            {
                let strDLDate : String = UserProfileInfo?.dlexpirydate!
                let arr1 = strDLDate.components(separatedBy: " ")
                self.lblDrivingDOB.text = String(format: "%@", arr1[0])//objUserProfileInfo.deliverydate!
                //"12-05-1986"
                self.strDLExpiryDate = self.lblDrivingDOB.text
            }
            else
            {
                self.lblDrivingDOB.text = "Expiry Date"
                self.lblDrivingDOB.textColor = Constants.GREY_COLOR_1
                self.strDLExpiryDate = ""
            }*/
            //self.lblDLGender.text = "Male"
            self.lblDLGender.text = UserProfileInfo?.passportGender
            self.lblDLGender.textColor = UIColor.black
            
            //self.imgViewDrivingLicence.sd_setImage(with: URL(string: UserProfileInfo.dlfile), placeholderImage: UIImage(named: "car_placeholder"))
            
            self.txtPanCardNo.text = UserProfileInfo?.panNo
            //self.imgViewPanCard.sd_setImage(with: URL(string: UserProfileInfo.panfile), placeholderImage: UIImage(named: "car_placeholder"))
            
            /*self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
            let arr1 = arrStateList
            for i in 0..<arr1.count
            {
                let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
                let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
                if (strStateName.lowercased()).contains(UserProfileInfo.state.lowercased())
                {
                    self.strStateCode = UserProfileInfo.state//String(strStateCode)
                }
                else
                {
                    self.strStateCode = "07"
                }
            }*/
        }
        else
        {
            
        }
    }
    
    //MARK: UIImage Picker Controller Delegate Methods
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView?.contentMode = .scaleAspectFit
            imageView?.image = pickedImage
            self.uploadFileOnAWS(withImage: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func uploadFileOnAWS(withImage image: UIImage)
    {
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let access = Constants.AWS_ACCESS_KEY
        let secret = Constants.AWS_SECRET_KEY
        let credentials = AWSStaticCredentialsProvider(accessKey: access, secretKey: secret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast1, credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //let s3BucketName = String(format: "%@/%@", Constants.AWS_S3_BUCKET_NAME,strClientID)//userDetails.clientID!
        let s3BucketName = String(format: "%@", Constants.AWS_S3_BUCKET_NAME)
        let compressedImage = image.resizedImage(newSize: CGSize(width: 80, height: 80))
        let data: Data = compressedImage.pngData()!
        
        let miliSeconds: Int = CommonMethodsMylesZero.currentTimeInMiliseconds()
        let strSeconds : String = String(miliSeconds)//String(format: "%d",miliSeconds)
        let keyName = String(format: "%@/%@%@.%@",strClientID,strClientID,strSeconds,data.format)
        let remoteName = String(format: "%@%@.%@",strClientID,strSeconds,data.format) //userDetails.clientID!
        
        //let remoteName = String(format: "%@%@",userDetails.clientID!,generateRandomStringWithLength(length: 12)+"."+data.format)
        print("REMOTE NAME : ", remoteName)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                DispatchQueue.main.async{
                    print("Progress: \(Float(progress.fractionCompleted))")
                }
            })
        }
        expression.setValue("public-read-write", forRequestHeader: "x-amz-acl")   //4
        expression.setValue("public-read-write", forRequestParameter: "x-amz-acl")
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                if(error != nil)
                {
                    print("Failure uploading file Error: %@", error?.localizedDescription)
                }
                else
                {
                    print("Success uploading file")
                    
                    CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Document Uploaded Successfully", strAction1: "OK", viewController: self)
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data, bucket: s3BucketName, key: remoteName, contentType: "image/"+data.format, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
            if let error = task.error {
                print("Error : \(error.localizedDescription)")
            }
            
            if task.result != nil
            {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(s3BucketName).appendingPathComponent(keyName) //remoteName
                if let absoluteString = publicURL?.absoluteString
                {
                    print("Image URL : ",absoluteString)
                    self.strAmazonBucketPath = s3BucketName
                    
                    if self.uploadButtonTag == 1120
                    {
                        self.lblAdhaarImageName.text = remoteName
                        self.lblAdhaarImageName.textColor = UIColor.black
                        self.strAdhaarFile = remoteName
                    }
                    else if self.uploadButtonTag == 1121
                    {
                        self.lblDrivingImageName.text = remoteName
                        self.lblDrivingImageName.textColor = UIColor.black
                        self.strDLFile = remoteName
                    }
                    else if self.uploadButtonTag == 1122
                    {
                       self.lblPanCardImageName.text = remoteName
                       self.lblPanCardImageName.textColor = UIColor.black
                       self.strPanFile = remoteName
                    }
                }
            }
            return nil
        }
    }
    
    //MARK: - Date For Adhaar Card
    
    @objc func dateSelected()
    {
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "MM/dd/yyyy"//"dd/MM/yyyy"
        let dateString1 = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(dateString1)")
        
        if self.dateOfBirthTag == 1220
        {
            self.lblAdhaarDOB.text = dateString1
            self.lblAdhaarDOB.textColor = UIColor.black
            self.btnAdhaarDOB.isSelected = false
            let arr1 = dateString1.components(separatedBy: "/")
            self.strAdhaarBirthYear = arr1[2]
            self.strAdhaarDOB = self.lblAdhaarDOB.text
        }
        else if self.dateOfBirthTag == 1221
        {
            let dateformatter2 = DateFormatter()
            dateformatter2.dateFormat = "MM/dd/yyyy" //"dd/MM/yyyy" //"dd-MM-yyyy"
            let dateString2 = dateformatter2.string(from: self.datePickerView.date)
            print("Date Selected For DL \(dateString2)")
            self.lblDrivingDOB.text = dateString2
            self.strDLExpiryDate = dateString2
            self.lblDrivingDOB.textColor = UIColor.black
        }
        self.showActionSheetForGender()
    }
    
    func showDateTimePicker()
    {
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "Please select the date", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        
        self.datePickerView.frame = CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 180)
        self.datePickerView.tag = 444
        //pickerFrame.delegate = self
        alert.view.addSubview(self.datePickerView)
        self.datePickerView.datePickerMode = UIDatePicker.Mode.date
        self.datePickerView.isHidden = false
        
        
        let selectAction = UIAlertAction(title: "Select", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //Perform Action
            //self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
            let dateformatter1 = DateFormatter()
            dateformatter1.dateFormat = "MM/dd/yyyy"//"dd/MM/yyyy"
            let dateString1 = dateformatter1.string(from: self.datePickerView.date)
            print("Date Selected For Adhaar Card \(dateString1)")
            
            if self.dateOfBirthTag == 1220
            {
                self.lblAdhaarDOB.text = dateString1
                self.lblAdhaarDOB.textColor = UIColor.black
                self.btnAdhaarDOB.isSelected = false
                let arr1 = dateString1.components(separatedBy: "/")
                self.strAdhaarBirthYear = arr1[2]
                self.strAdhaarDOB = self.lblAdhaarDOB.text
            }
            else if self.dateOfBirthTag == 1221
            {
                let dateformatter2 = DateFormatter()
                dateformatter2.dateFormat = "MM/dd/yyyy"//"dd/MM/yyyy" //"dd-MM-yyyy"
                let dateString2 = dateformatter2.string(from: self.datePickerView.date)
                print("Date Selected For DL \(dateString2)")
                self.lblDrivingDOB.text = dateString2
                self.strDLExpiryDate = dateString2
                self.lblDrivingDOB.textColor = UIColor.black
            }
        })
        alert.addAction(selectAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            //self.lblDrivingDOB.text = "Expiry Date"
            self.lblDrivingDOB.textColor = Constants.GREY_COLOR_1
            self.strDLExpiryDate = ""
        })
        alert.addAction(cancelAction)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant:350) //self.view.frame.height * 0.50 //0.80
        alert.view.addConstraint(height);
        self.present(alert, animated: true, completion: nil)
        //self.parent!.present(alert, animated: true, completion: nil)
    }

    //MARK:- Action Sheet With Picker View
    
    func showActionSheetForGender()
    {
        let alertController = UIAlertController(title: "", message: "Select car condition", preferredStyle: .actionSheet)
        
        let maleAction = UIAlertAction(title: "Male", style: .default, handler: { (action) -> Void in
            self.lblDLGender.text! = "Male"
            self.lblDLGender.textColor = UIColor.black
        })
        
        let  femaleAction = UIAlertAction(title: "Female", style: .default, handler: { (action) -> Void in
            self.lblDLGender.text! = "Female"
            self.lblDLGender.textColor = UIColor.black
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        alertController.addAction(maleAction)
        alertController.addAction(femaleAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            //self.present(imagePicker, animated: true, completion: nil)
            self.present(imagePicker, animated: true, completion: {
                self.imagePicker.delegate = self
            })
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        //self.present(imagePicker, animated: true, completion: nil)
        self.present(imagePicker, animated: true, completion: {
            self.imagePicker.delegate = self
        })
    }
    
    func openImagePickerAlert()
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - All Documents Verification API
    
    func checkValidityOfPassport()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let intNumber = CommonMethodsMylesZero.generateRandomDigits(9)
        let strTransID =  String(format: "Passport%d", intNumber)
        var dictParam:[String:AnyObject]!
        //
        dictParam = ["transID" : "123456",
                     "docType" : 3,
                     "docNumber" : "J8369854",
                     "surname" : "RAMADUGULA",
                     "firstName" : "SITA MAHA LAKSHMI",
                     "gender" : "F",
                     "countryCode" : "IND",
                     "dateOfBirth" : "23-09-1959",
                     "passportType" : "P",
                     "dateOfExpiry" : "10-10-2021",
                     "mrz1" : "P<INDRAMADUGULA<<SITA<<MAHA<<LAKSHMI<<<<<<<<<<",
                     "mrz2" : "J8369854<4IND5909234F2110101<<<<<<<<<<<<<<<8"] as [String : AnyObject] //2699122_73682_A
        
        self.dictPassportInfo = dictParam
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        APIFunctions.sharedInstance.checkValidityOfAdhaarCard(APIConstants.API_passport_validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    //self.checkValidityOfDrivingLicence()
                    //CommonFunctions.showAlertController2(strTitle: "", strMsg: response.value(forKey: "msg") as! String, strAction1: "OK", viewController: self)
                }
            }
        })
    }
    
    func checkValidityOfAdhaarCard()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        //let intNumber = CommonFunctions.generateRandomDigits(9)
        //let intAdhaarNo = Int(self.txtAdhaarCardNumber.text ?? "0")
        let strDoc = self.txtAdhaarNo.text ?? "0"
        let strTransID =  String(format: "iOS%@adhaarcard", strDoc)
        //let strTransID =  String(format: "Adhaar%d", intNumber)
        var dictParam:[String:AnyObject]!
        
        dictParam = [ "transID": strTransID as AnyObject, //Adhar2364777piiip
            "docType": 55 as AnyObject, //"docNumber": self.txtAdhaarCardNumber.text!,
            "name": self.txtAdhaarName.text! as AnyObject,
            "yob" : self.strAdhaarBirthYear ?? "0000", //"1981",
            "aadhaar_url" : "https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2700124/2700124_38704_A.pdf"] as [String : AnyObject] //2699122_73682_A //kAWSLiveFolderName
        
        self.dictAdhaarInfo = dictParam
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.checkValidityOfAdhaarCard(APIConstants.API_adhaarCard_validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.checkValidityOfDrivingLicence()
                    //CommonFunctions.showAlertController2(strTitle: "", strMsg: response.value(forKey: "msg") as! String, strAction1: "OK", viewController: self)
                }
            }
        })
    }
    
    func checkValidityOfDrivingLicence()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let intNumber = CommonMethodsMylesZero.generateRandomDigits(9)
        let strTransID =  String(format: "dl%d", intNumber)
        var dictParam:[String:AnyObject]!
        dictParam = ["transID" : strTransID as AnyObject, //"dl2334555uu",
            "docType" : 4 as AnyObject,
            "docNumber" : self.txtDrivingLicenceNo.text! as AnyObject, //"BYSPS8666666M",
            "dob":self.self.strDLExpiryDate,//self.lblDrivingDOB.text! as AnyObject,//"25-12-1981",
            "dl_photo":"0"] as [String : AnyObject]
        self.dictDrivingLicenceInfo = dictParam
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.checkValidityOfDrivingLicence(APIConstants.API_drivingLicence_Validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.checkValidityOfPanCard()
                    //CommonFunctions.showAlertController2(strTitle: "", strMsg: response.value(forKey: "msg") as! String, strAction1: "OK", viewController: self)
                }
            }
        })
    }
    
    func checkValidityOfPanCard()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let intNumber = CommonMethodsMylesZero.generateRandomDigits(9)
        let strTransID =  String(format: "pan%d", intNumber)
        var dictParam:[String:AnyObject]!
        dictParam = ["transID" : strTransID, //"pan2334555uu"
            "docType" : 2,
            "docNumber" : self.txtPanCardNo.text!] as [String : AnyObject]
        self.dictPanCardInfo = dictParam
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.checkValidityOfPanCard(APIConstants.API_pancard_validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    /*//let objPanCard  = PanCardDetailRootClass.init(fromDictionary:(response as! NSDictionary) as! [String : Any])
                    //DefaultsValues.setCustomObjToUserDefaults(objPanCard, forKey: kPanCardData)
                    let objDocumentsDetailVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_DocumentsDetail) as! DocumentsDetailVC
                    objDocumentsDetailVC.strAdhaarFile = self.lblAdhaarImageName.text ?? ""
                    objDocumentsDetailVC.strDLFile = self.lblDLImageName.text ?? ""
                    objDocumentsDetailVC.strPanFile = self.lblPanImageName.text ?? ""
                    objDocumentsDetailVC.strAmazonBucketFilePath = self.strAmazonBucketPath ?? ""
                    objDocumentsDetailVC.strDocType = self.strTypeOfDocument
                    
                    self.navigationController?.pushViewController(objDocumentsDetailVC, animated: true)*/
                    
                    self.submitDocumentDetailWebService()
                }
            }
        })
    }
    
    //MARK: - Submit Document WebService
    
    func submitDocumentDetailWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let strAdhaarJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAdhaarCardJsonStr)
        //let strPassportJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPassportJsonStr)
        let strDLJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kDrivingLicenceJsonStr)
        let strPanCardJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPanCardJsonStr)
        
        self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
        let arr1 = arrStateList
        for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
            if (strStateCode.lowercased()).contains(UserProfileInfo.state.lowercased())
            {
                self.strStateCode = String(strStateCode)
            }
            if (strStateName.lowercased()).contains(UserProfileInfo.state.lowercased())
            {
                self.strStateCode = String(strStateCode)
            }
        }
        
        self.strAmazonBucketFilePath = String(format: "https://s3.ap-southeast-1.amazonaws.com/%@/%@/",Constants.AWS_S3_BUCKET_NAME, strClientID)
        
        /*if UserProfileInfo.aadharfile != ""
        {
            let str = UserProfileInfo.aadharfile as String
            let arrValue = str.components(separatedBy: "/")
            self.strAmazonBucketFilePath = String(format: "https://%@/%@/%@/", arrValue[2], arrValue[3], arrValue[4])
        }*/
        
        var dictParam:[String:AnyObject]!
        
        dictParam = ["clientid":strClientID as AnyObject, //userDetails.clientID!
            "aadharfile": strAdhaarFile as AnyObject,
            "aadharno":self.txtAdhaarNo.text! as AnyObject,
            "aadharname":self.txtAdhaarName.text! as AnyObject,
            "aadhardob":self.strAdhaarDOB as AnyObject, //strAdhaarDOB as AnyObject,
            "Passportfile":strPassport as AnyObject,
            "Passportno":"",
            "Passportsurname":"",
            "Passportfirstname":"",
            "PassportGender":"Male",
            "Passportcountryco":"",
            "Passporttype":"",
            "passportexpirydate":"07/12/2023",
            "PassportMRZ1":"",
            "PassportMRZ2":"",
            "PassportDOb":"06/12/1999",
            "dlfile":strDLFile as AnyObject,
            "dlno":self.txtDrivingLicenceNo.text! as AnyObject,
            "panfile":strPanFile as AnyObject,
            "panno":self.txtPanCardNo.text! as AnyObject,
            "doctype":"Adhaar", //self.strDocType as AnyObject,
            "filepath":self.strAmazonBucketFilePath ?? "",
            "Delieveryaddress":"",
            "PermanantAddress":"", //"dlexpirydate": self.strDLExpiryDate,
            "deliverydate":"", //self.lblDrivingDOB.text! as AnyObject,
            "aadharverify":"0",
            "Passportverify":"0",
            "Panverify":"0",
            "dlverify":"0",
            "jsonaadharverifydata":strAdhaarJson ?? "",
            "jsonpassportverifydata":"",
            "jsonPanverifydata":strPanCardJson ?? "",
            "jsondlverifydata":strDLJson ?? "",
            "address1permanent":UserProfileInfo.address1permanent as AnyObject,
            "address2permanent":UserProfileInfo.address2permanent as AnyObject,
            "state":self.strStateCode as AnyObject, //UserProfileInfo.state
            "city":UserProfileInfo.city as AnyObject,
            "pincode":UserProfileInfo.pincode as AnyObject,
            "address1delivery":UserProfileInfo.address1delivery as AnyObject,
            "address2delivery":UserProfileInfo.address2delivery as AnyObject,
            "statedelivery":UserProfileInfo.statedelivery as AnyObject,
            "citydelivery":UserProfileInfo.citydelivery as AnyObject,
            "pincodedelivery":UserProfileInfo.pincodedelivery as AnyObject,
            "sameaddress":UserProfileInfo.isAddressSame as AnyObject] as [String : AnyObject]
        
        print("document Dictionary: ", dictParam)
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.uploadDocumentAuthBridgeExperian(APIConstants.API_UploadDocumentAuthBridgeExperian, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("response: %@", response)
                    let responseKey = response.value(forKey:"response") as? AnyObject
                    if responseKey is NSNull //((response.value(forKey:"response") as? NSNull) == nil)
                    {
                        let status = response.value(forKey:"status") as? Int
                        if status == 1
                        {
                            let strMessage = response.value(forKey:"message") as! String
                            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                            //self.callApiToGetEmployeeDetail() //Swati Commented This
                        }
                        else if status == 0
                        {
                            //let strMessage = response.value(forKey:"message") as! String
                            //CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                        }
                    }
                }
            }
        })
    }
    
    func callApiToGetEmployeeDetail()
    {
        //let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
        
        let strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String
        
        var dictLoginParam:[String:AnyObject]!
        dictLoginParam = ["clientid":strClientID] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.getEmployeeProfileDetail(APIConstants.API_GetEmployeeDetail, dataDict: dictLoginParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion: { (response, error) in
            
            DispatchQueue.main.async {
                //hud!.dismiss()
                if error != nil
                {
                    
                }
                else
                {
                    let dict = response as! NSDictionary
                    let strStatus = dict.value(forKey: "status") as! Int
                    if strStatus == 1
                    {
                        let arr = response.value(forKey: kResponseInfo) as! NSArray
                        let dict = (arr[0] as AnyObject) as! NSDictionary
                        let userProfileDetail  = UserProfileInfoResponse.init(fromDictionary:(dict) as! [String : Any])
                        DefaultsValues.setCustomObjToUserDefaults(userProfileDetail, forKey: kUserProfileDetails)
                        self.setDocumentDetails()
                    }
                }
            }
        })
    }
    
    //MARK: - Button Actions
    
    @IBAction func btnAadhaarViewFile_Click(_ sender: Any)
    {
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = self.strAdhaarUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnDLViewFile_Click(_ sender: Any)
    {
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = self.strDLUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
    }
    
    @IBAction func btnPanCardViewFile_Click(_ sender: Any)
    {
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = self.strPancardUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
    }
   
    @IBAction func btnGender_Click(_ sender: UIButton)
    {
        self.showActionSheetForGender()
    }
    @IBAction func btnAdhaarDOB_Click(_ sender: UIButton)
    {
        self.dateOfBirthTag = sender.tag
        self.showDateTimePicker()
    }
    
    @IBAction func btnDrivingDOB_Click(_ sender: UIButton)
    {
        self.dateOfBirthTag = sender.tag
        self.showDateTimePicker()
    }
    
    @IBAction func btnSelectAdhaar_Click(_ sender: UIButton)
    {
        isAdhaarSelected = true
        strTypeOfDocument = "Adhaar"
        self.lblAdhaarImageName.isHidden = false
        btnAdhaarRadio.setImage(UIImage(named:"select_radio"), for: UIControl.State())
        btnPassportRadio.setImage(UIImage(named:"unselect_radio"), for: UIControl.State())
        if self.viewPassportView.superview != nil
        {
           self.viewPassportView.removeFromSuperview()
        }
        else
        {
            guard Bundle.main.path(forResource: "PassportView", ofType: "nib") != nil else {
                return
            }
        }
    }
    
    @IBAction func btnSelectPassport_Click(_ sender: UIButton)
    {
        isAdhaarSelected = false
        strTypeOfDocument = "Passport"
        self.lblAdhaarImageName.isHidden = true
        btnPassportRadio.setImage(UIImage(named:"select_radio"), for: UIControl.State())
        btnAdhaarRadio.setImage(UIImage(named:"unselect_radio"), for: UIControl.State())
        
        self.viewPassportView = Bundle.main.loadNibNamed("PassportView", owner: nil, options: nil)![0] as! PassportView
        self.viewAdhaarPassportContent.addSubview(self.viewPassportView)
        self.viewPassportView.frame = CGRect(x: 0, y: 0, width: self.viewAdhaarPassportContent.frame.size.width, height: self.viewAdhaarPassportContent.frame.size.height)
    }
    
    @IBAction func btnUploadAdhaar_Click(_ sender: UIButton)
    {
        self.uploadButtonTag = sender.tag
        self.openImagePickerAlert()
    }
    
    @IBAction func btnUploadDL_Click(_ sender: UIButton)
    {
        self.uploadButtonTag = sender.tag
        self.openImagePickerAlert()
    }
    
    @IBAction func btnUploadPanCard_Click(_ sender: UIButton)
    {
        self.uploadButtonTag = sender.tag
        self.openImagePickerAlert()
    }
    
    @IBAction func btnUpdateDocument_Click(_ sender: Any)
    {
        if self.txtAdhaarNo.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the aadhaar number", strAction1: "OK", viewController: self)
        }
        else if self.txtAdhaarName.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the aadhaar name", strAction1: "OK", viewController: self)
        }
        else if self.txtDrivingLicenceNo.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter driving licence number", strAction1: "OK", viewController: self)
        }
        else if self.txtPanCardNo.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the pan card number", strAction1: "OK", viewController: self)
        }
        else if self.lblAdhaarImageName.text == "" ||  self.lblAdhaarImageName.text == "Choose jpeg and png files"
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please upload the adhaar", strAction1: "OK", viewController: self)
        }
        else if self.lblDrivingImageName.text == "" ||  self.lblAdhaarImageName.text == "Choose image (JPEG, PNG Files Only)"
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select state", strAction1: "OK", viewController: self)
        }
        else if self.lblPanCardImageName.text == "" ||  self.lblAdhaarImageName.text == "Choose image (JPEG, PNG Files Only)"
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select city", strAction1: "OK", viewController: self)
        }
        else
        {
            self.checkValidityOfAdhaarCard()
        }
    }
}

