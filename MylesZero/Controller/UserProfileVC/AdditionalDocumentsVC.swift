//
//  AdditionalDocumentsVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 21/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3
import AWSCore

class AdditionalDocumentsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AdditionalDocumentCellDelegate
{
    @IBOutlet weak var tblViewAdditionalDoc: UITableView!
    //@IBOutlet weak var btnSubmit: UIButton!
    var datePickerView = UIDatePicker()
    var imageURL = NSURL()
    var uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var uploadFileURL: NSURL?
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var imageView: UIImageView?
    var strAmazonBucketPath : String?
    var arrData = NSMutableArray()
    var arrData1 = NSMutableArray()
    var arrBankStatement = NSMutableArray()
    var arrCheque = NSMutableArray()
    var arrFinanceReport = NSMutableArray()
    var arrGST = NSMutableArray()
    var arrSignSI = NSMutableArray()
    var arrTAN = NSMutableArray()
    var indexSection : Int?
    var indexRow : Int?
    var insertIndexSection : Int?
    var insertIndexRow : Int?
        
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.arrData1.add(self.arrData1.count + 1)
        self.arrData.add(self.arrData.count + 1)
        print("self.arrData?.count: ", self.arrData.count)
        self.arrBankStatement.add("/Choose File")
        self.arrFinanceReport.add("/Choose File")
        self.arrCheque.add("/Choose File")
        self.arrCheque.add("/Choose File")
        self.arrCheque.add("/Choose File")
        self.arrCheque.add("/Choose File")
        self.arrCheque.add("/Choose File")
        self.arrCheque.add("/Choose File")
        self.arrSignSI.add("/Choose File")
        self.arrGST.add("/Choose File")
        self.arrTAN.add("/Choose File")
        
        indexSection = 0
        indexRow = 0
        
        insertIndexSection = 0
        insertIndexRow = 0
        
        self.GetSubscriptionDocsData()
        
        self.tblViewAdditionalDoc.register(UINib.init(nibName: "AdditionalDocumentCell", bundle: nil), forCellReuseIdentifier: "idAdditionalDocumentCell")
        self.tblViewAdditionalDoc.dataSource = self
        self.tblViewAdditionalDoc.delegate = self
        self.tblViewAdditionalDoc.isHidden = false
        self.tblViewAdditionalDoc.backgroundColor = Constants.LIGHT_GREY_COLOR
        
        imagePicker.delegate = self
        
        //let nib = UINib(nibName: "idAdditionalDocumentCell", bundle: nil)
        //self.tblViewAdditionalDoc.register(nib, forCellReuseIdentifier: "idAdditionalDocumentCell")
        
        //        self.tblViewAdditionalDoc.register(UINib(nibName: "TaskCollectionViewCell", bundle: Bundle(for: AdditionalDocumentCell.self)), forCellWithReuseIdentifier: "idAdditionalDocumentCell")
        
        //self.tblViewAdditionalDoc.isHidden = true
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Additional Document API
    
    func GetSubscriptionDocsData()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let dictDocumentParam = ["ClientCoIndivID": strClientID as AnyObject] as [String : AnyObject] //userDetails.clientID!
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.getSubscriptionDoc(APIConstants.API_GetSubscriptionDocsData, dataDict: dictDocumentParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    //print("response doc: ", response)
                    let dict = response as? NSDictionary
                    if let dict1 = dict!.value(forKey: "response") as? NSDictionary
                    {
                        self.arrBankStatement = NSMutableArray()
                        self.arrCheque = NSMutableArray()
                        self.arrFinanceReport = NSMutableArray()
                        self.arrGST = NSMutableArray()
                        self.arrSignSI = NSMutableArray()
                        self.arrTAN = NSMutableArray()
                        
                        if dict1 != nil
                        {
                            let arr1 = dict1.value(forKey: "BankStatement") as! NSArray //(arr[0] as AnyObject).value(forKey: "BankStatement") as! NSMutableArray
                            self.arrBankStatement = NSMutableArray(array: arr1)
                            
                            let arr2 = dict1.value(forKey: "Check") as! NSArray
                            self.arrCheque = NSMutableArray(array: arr2)
                            
                            let arr3 = dict1.value(forKey: "FinanceReport") as! NSArray
                            self.arrFinanceReport = NSMutableArray(array: arr3)
                            
                            let arr4 = dict1.value(forKey: "GSTN") as! NSArray
                            self.arrGST = NSMutableArray(array: arr4)
                            
                            let arr5 = dict1.value(forKey: "SignSI") as! NSArray
                            self.arrSignSI = NSMutableArray(array: arr5)
                            
                            let arr6 = dict1.value(forKey: "TAN") as! NSArray
                            self.arrTAN = NSMutableArray(array: arr6)
                            
                            self.tblViewAdditionalDoc.reloadData()
                        }
                    }
                    /*let objCarListVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_CarList) as! CarListVC
                    self.navigationController?.pushViewController(objCarListVC, animated: true)*/
                }
            }
        })
    }
    
    func callUserAdditionalDocumentsAPI()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        //let strClientID = String(format: "%@", userDetails.clientID)
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let dict = ["ClientCoIndivId":strClientID,
                    "FinanceReport":self.arrFinanceReport,
                    "TAN":self.arrTAN,
                    "GSTN":self.arrGST,
                    "Check":self.arrCheque,
                    "BankStatement":self.arrBankStatement,
                    "SignSI":self.arrSignSI] as [String:AnyObject]
        
        /*let dict = ["ClientCoIndivId":"2698070",
        "FinanceReport":["https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_58135_AdditionalDoc.pdf"],
        "TAN":["https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_93089_AdditionalDoc.pdf"],
        "GSTN":["https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_36993_AdditionalDoc.png"],
        "Check":["https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_26548_AdditionalDoc.png","https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_92173_AdditionalDoc.png","https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_88573_AdditionalDoc.png","https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_83076_AdditionalDoc.png","https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_58183_AdditionalDoc.png","https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_63663_AdditionalDoc.png"],
        "BankStatement":["https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_85276_AdditionalDoc.png"],
        "SignSI":["https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2698070/2698070_76269_AdditionalDoc.png"]] as [String:AnyObject]*/
        
        let dictDocumentParam = ["Doc": dict]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.userAdditonalDocumentListAPI(APIConstants.API_UserDocumentList, dataDict: dictDocumentParam as [String : AnyObject], dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("Saved Doc: ", response)
                    let status = response.value(forKey: "status") as? Int
                    if status == 1
                    {
                        CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Document Uploaded Successfully", strAction1: "OK", viewController: self)
                        self.tblViewAdditionalDoc.reloadData()
                    }
                    
                    /*let objCarListVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_CarList) as! CarListVC
                    self.navigationController?.pushViewController(objCarListVC, animated: true)*/
                }
            }
        })
    }
    
    //MARK: - UITableView Delegate Methods
    
    //(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return self.arrFinanceReport.count
        }
        else if section == 1
        {
            return self.arrTAN.count
        }
        else if section == 2
        {
            return self.arrGST.count
        }
        else if section == 3
        {
            return self.arrCheque.count
        }
        else if section == 4
        {
            return self.arrBankStatement.count
        }
        else if section == 5
        {
            return self.arrSignSI.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idAdditionalDocumentCell"
        let cell:AdditionalDocumentCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! AdditionalDocumentCell?)!
        
        cell.backgroundColor = Constants.LIGHT_GREY_COLOR
        
        cell.selectionStyle = .none
        
        self.indexSection = indexPath.section
        self.indexRow = indexPath.row
        
        if indexPath.section == 0
        {
            //cell.lblTitle.text = "Last 3 years financial reports & ITR (Form 16)"
            if self.arrFinanceReport.count >= 1
            {
                //let i = self.arrFinanceReport.count - 1
                
                if indexPath.row == 0 //i == indexPath.row
                {
                    cell.btnAddAnotherFile.isHidden = false
                }
                else
                {
                    cell.btnAddAnotherFile.isHidden = true
                }
            }
            //cell.btnAddAnotherFile.tag = indexPath.section
            cell.btnClose.isHidden = true
            let str = self.arrFinanceReport[indexPath.row] as? String
            if (str?.contains("/Choose File"))!
            {
                cell.btnViewFile.isHidden = true
            }
            else
            {
                cell.btnViewFile.isHidden = false
            }
            let arrValue = str?.components(separatedBy: "/")
            cell.lblDocumentName.text = String(format: "%@", arrValue?.last ?? "")
            //cell.lblDocumentName.text = self.arrFinanceReport[indexPath.row] as? String
            //cell.btnAddAnotherFile.addTarget(self, action:#selector(self.btnAddFile_Click(sender:)), for: .touchUpInside)
            //cell.btnUploadDoc.addTarget(self, action:#selector(self.btnUploadDoc_Click(sender:)), for: .touchUpInside)
        }
        else if indexPath.section == 1
        {
            //cell.lblTitle.text = "TAN Number"
            let str = self.arrTAN[indexPath.row] as? String
            if (str?.contains("/Choose File"))!
            {
                cell.btnViewFile.isHidden = true
            }
            else
            {
                cell.btnViewFile.isHidden = false
            }
            let arrValue = str?.components(separatedBy: "/")
            cell.lblDocumentName.text = String(format: "%@", arrValue?.last ?? "")
            //cell.lblDocumentName.text = self.arrTAN[indexPath.row] as? String
            cell.btnClose.isHidden = true
            cell.btnAddAnotherFile.isHidden = true
        }
        else if indexPath.section == 2
        {
            //cell.lblTitle.text = "GSTN Number"
            let str = self.arrGST[indexPath.row] as? String
            if (str?.contains("/Choose File"))!
            {
                cell.btnViewFile.isHidden = true
            }
            else
            {
                cell.btnViewFile.isHidden = false
            }
            let arrValue = str?.components(separatedBy: "/")
            cell.lblDocumentName.text = String(format: "%@", arrValue?.last ?? "")
            //cell.lblDocumentName.text = self.arrGST[indexPath.row] as? String
            cell.btnClose.isHidden = true
            cell.btnAddAnotherFile.isHidden = true
        }
        else if indexPath.section == 3
        {
            //cell.lblTitle.text = "Cheques"
            let str = self.arrCheque[indexPath.row] as? String
            if (str?.contains("/Choose File"))!
            {
                cell.btnViewFile.isHidden = true
            }
            else
            {
                cell.btnViewFile.isHidden = false
            }
            let arrValue = str?.components(separatedBy: "/")
            cell.lblDocumentName.text = String(format: "%@", arrValue?.last ?? "")
            //cell.lblDocumentName.text = self.arrCheque[indexPath.row] as? String
            cell.btnClose.isHidden = true
            cell.btnAddAnotherFile.isHidden = true
        }
        else if indexPath.section == 4
        {
            //cell.lblTitle.text = "6 Month Bank Statement"
            let str = self.arrBankStatement[indexPath.row] as? String
            if (str?.contains("/Choose File"))!
            {
                cell.btnViewFile.isHidden = true
            }
            else
            {
                cell.btnViewFile.isHidden = false
            }
            let arrValue = str?.components(separatedBy: "/")
            cell.lblDocumentName.text = String(format: "%@", arrValue?.last ?? "")
            //cell.lblDocumentName.text = self.arrBankStatement[indexPath.row] as? String
            cell.btnClose.isHidden = true
            if self.arrBankStatement.count >= 1
            {
                //let i = self.arrBankStatement.count - 1
                
                if indexPath.row == 0 //i == indexPath.row
                {
                    cell.btnAddAnotherFile.isHidden = false
                }
                else
                {
                    cell.btnAddAnotherFile.isHidden = true
                }
            }
            //cell.btnAddAnotherFile.tag = indexPath.section
            //cell.btnAddAnotherFile.addTarget(self, action:#selector(self.btnAddFile_Click(sender:)), for: .touchUpInside)
        }
        else if indexPath.section == 5
        {
            //cell.lblTitle.text = "Duly Signed Standing Instruction"
            let str = self.arrSignSI[indexPath.row] as? String
            if (str?.contains("/Choose File"))!
            {
                cell.btnViewFile.isHidden = true
            }
            else
            {
                cell.btnViewFile.isHidden = false
            }
            let arrValue = str?.components(separatedBy: "/")
            cell.lblDocumentName.text = String(format: "%@", arrValue?.last ?? "")
            //cell.lblDocumentName.text = self.arrSignSI[indexPath.row] as? String
            cell.btnClose.isHidden = true
            cell.btnAddAnotherFile.isHidden = true
        }
        
        cell.cellAdditionalDocDelegate = self
        
        return cell
    }
    
    /*func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String?
    {
        switch(section) {
          case 0:return "Last 3 years financial reports & ITR (Form 16)"
          case 1:return "TAN Number"
          case 2:return "GSTN Number"
          case 3:return "Cheques"
          case 4:return "6 Month Bank Statement"
          case 5:return "Duly Signed Standing Instruction"
            
          default :return ""
        }
    }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView: UIView = UIView.init(frame: CGRect.init(x: 20, y: 20, width: self.tblViewAdditionalDoc.frame.size.width - 16, height: 70))
        headerView.backgroundColor = UIColor.clear //Constants.LIGHT_GREY_COLOR
        
        let view: UIView = UIView.init(frame: CGRect.init(x: 16, y: 5, width: self.tblViewAdditionalDoc.frame.size.width - 31, height: 55))
        view.backgroundColor = UIColor.white

        let labelView: UILabel = UILabel.init(frame: CGRect.init(x: 10, y: 15, width: 290, height: 24))
                
        if section == 0
        {
            labelView.text = "Last 3 years financial reports & ITR (Form 16)"
        }
        else if section == 1
        {
            labelView.text = "TAN Number"
        }
        else if section == 2
        {
            labelView.text = "GSTN Number"
        }
        else if section == 3
        {
            labelView.text = "Cheques"
        }
        else if section == 4
        {
           labelView.text = "6 Month Bank Statement"
        }
        else if section == 5
        {
           labelView.text = "Duly Signed Standing Instruction"
        }
        labelView.textColor = Constants.RED_COLOR
        labelView.font = UIFont(name:"Roboto-Medium", size: 14.0)

        headerView.addSubview(view)
        view.addSubview(labelView)
        //self.tableView.tableHeaderView = headerView
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let footerView = UIView()
        //footerView.backgroundColor = colorLiteral(red: 0.9686274529, green:0.78039217, blue: 0.3450980484, alpha: 1)
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:
            70)
        footerView.backgroundColor = UIColor.white
        let button = UIButton()
        button.frame = CGRect(x: 16, y: 10, width:footerView.frame.size.width - 32, height: 45)
        button.setTitle("SUBMIT", for: .normal)
        button.addTarget(self, action:#selector(self.btnSubmitDoc_Click(sender:)), for: .touchUpInside)
        button.setTitleColor( UIColor.white, for: .normal)
        button.titleLabel?.font =  UIFont(name: "Roboto-Medium", size: 14.0)
        button.backgroundColor = Constants.RED_COLOR
        button.layer.cornerRadius = 4.0
        footerView.addSubview(button)
        return footerView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if section == 5
        {
            return 100
        }
        else
        {
           return 0
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    /*@objc func btnUploadDoc_Click(sender:UIButton!)
    {
        self.openImagePickerAlert()
    }*/
    
    //MARK: - ImagePicker Option
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openImagePickerAlert()
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - UIImagePicker Delegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView?.contentMode = .scaleAspectFit
            imageView?.image = pickedImage
            self.uploadFileOnAWS(withImage: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    /*func uploadImage(withImage image: UIImage){
        
        //defining bucket and upload file name
        let S3BucketName: String = "bucketName"
        let S3UploadKeyName: String = "public/testImage.jpg"
        
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        let access = Constants.AWS_ACCESS_KEY
        let secret = Constants.AWS_SECRET_KEY
        let credentials = AWSStaticCredentialsProvider(accessKey: access, secretKey: secret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast1, credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let s3BucketName = String(format: "%@/%@", Constants.AWS_S3_BUCKET_NAME,strClientID)
        let compressedImage = image.resizedImage(newSize: CGSize(width: 80, height: 80))
        let data: Data = compressedImage.pngData()!
        
        let miliSeconds: Int = CommonMethodsMylesZero.currentTimeInMiliseconds()
        let strSeconds : String = String(format: "%d",miliSeconds)
        let remoteName = String(format: "%@%@.%@",strClientID,strSeconds,data.format)
        
        //let remoteName = String(format: "%@%@",userDetails.clientID!,generateRandomStringWithLength(length: 12)+"."+data.format)
        print("REMOTE NAME : ", remoteName)
        
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.uploadProgress = {(task: AWSS3TransferUtilityTask, bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) in
            dispatch_async(dispatch_get_main_queue(), {
                let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
                print("Progress is: \(progress)")
            })
        }
        
        self.uploadCompletionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if ((error) != nil){
                    print("Failed with error")
                    print("Error: \(error!)");
                }
                else{
                    print("Sucess")
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadFile(imageURL, bucket: S3BucketName, key: S3UploadKeyName, contentType: "image/jpeg", expression: expression, completionHandler: uploadCompletionHandler).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                print("Error: \(error.localizedDescription)")
            }
            if let exception = task.exception {
                print("Exception: \(exception.description)")
            }
            if let _ = task.result {
                print("Upload Starting!")
            }
            
            return nil;
        }
    }*/
    
    //MARK: - Upload On AWS
    
    func uploadFileOnAWS(withImage image: UIImage)
    {
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        let access = Constants.AWS_ACCESS_KEY
        let secret = Constants.AWS_SECRET_KEY
        let credentials = AWSStaticCredentialsProvider(accessKey: access, secretKey: secret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast1, credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //let s3BucketName = String(format: "%@/%@", Constants.AWS_S3_BUCKET_NAME,strClientID)
        let s3BucketName = String(format: "%@", Constants.AWS_S3_BUCKET_NAME)
        let compressedImage = image.resizedImage(newSize: CGSize(width: 80, height: 80))
        let data: Data = compressedImage.pngData()!
        
        let miliSeconds: Int = CommonMethodsMylesZero.currentTimeInMiliseconds()
        let strSeconds : String = String(miliSeconds) //String(format: "%d",miliSeconds)
        let remoteName = String(format: "%@%@.%@",strClientID,strSeconds,data.format)
        let keyName = String(format: "%@/%@%@.%@",strClientID,strClientID,strSeconds,data.format)
        
        //let remoteName = String(format: "%@%@",userDetails.clientID!,generateRandomStringWithLength(length: 12)+"."+data.format)
        print("KEY NAME : ", keyName)
        print("REMOTE NAME : ", remoteName)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                DispatchQueue.main.async{
                    print("Progress: \(Float(progress.fractionCompleted))")
                }
            })
        }
        expression.setValue("public-read-write", forRequestHeader: "x-amz-acl")   //4
        expression.setValue("public-read-write", forRequestParameter: "x-amz-acl")
        
        //uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        uploadCompletionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                if(error != nil)
                {
                    print("Failure uploading file Error: ", error?.localizedDescription)
                }
                else
                {
                    print("Success uploading file1234567890")
                    
                    //CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Document Uploaded Successfully", strAction1: "OK", viewController: self)
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data, bucket: s3BucketName, key: keyName, contentType: "image/"+data.format, expression: expression, completionHandler: uploadCompletionHandler).continueWith { (task) -> Any? in
            if let error = task.error {
                print("Error : \(error.localizedDescription)")
            }
            /*if let exception = task.exception {
                print("Error : \(exception.localizedDescription)")
            }*/
            
            if let _ = task.result //if task.result != nil
            {
                DispatchQueue.main.async {

                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(s3BucketName).appendingPathComponent(keyName) //remoteName
                    if let absoluteString = publicURL?.absoluteString
                    {
                        print("Image URL : ",absoluteString)
                        let indexPath = IndexPath(row: self.indexRow!, section: self.indexSection!) //self.indexSection!
                        let cell = self.tblViewAdditionalDoc.cellForRow(at: indexPath) as? AdditionalDocumentCell
                        //let cellReuseIdentifier = "idAdditionalDocumentCell"
                        //cell:AdditionalDocumentCell = (tblViewAdditionalDoc.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! AdditionalDocumentCell?)!
                        if self.indexSection == 0
                        {
                            if self.arrFinanceReport.contains("/Choose File") {
                                //self.arrFinanceReport.remove("/Choose File")
                                self.arrFinanceReport.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            self.arrFinanceReport.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)

                            //self.arrFinanceReport.removeObject(at: self.insertIndexRow!)
                            //self.arrFinanceReport.add(absoluteString)
                            //cell?.lblDocumentName.text = self.arrFinanceReport[indexPath.row] as? String
                            self.tblViewAdditionalDoc.reloadData()
                        }
                        else if self.indexSection == 1
                        {
                            //self.arrTAN.add(absoluteString)
                            if self.arrTAN.contains("/Choose File") {
                                self.arrTAN.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                                //self.arrTAN.remove("/Choose File")
                            }
                            else
                            {
                                self.arrTAN.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            //cell?.lblDocumentName.text = self.arrTAN[indexPath.row] as? String
                            self.tblViewAdditionalDoc.reloadData()
                        }
                        else if self.indexSection == 2
                        {
                            //self.arrGST.add(absoluteString)
                            if self.arrGST.contains("/Choose File") {
                                //self.arrGST.remove("/Choose File")
                                self.arrGST.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            else
                            {
                               self.arrGST.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            
                            //cell?.lblDocumentName.text = self.arrGST[indexPath.row] as? String
                            self.tblViewAdditionalDoc.reloadData()
                        }
                        else if self.indexSection == 3
                        {
                            //self.arrCheque.add(absoluteString)
                            if self.arrCheque.contains("/Choose File") {
                                //self.arrCheque.remove("/Choose File")
                                self.arrCheque.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            else
                            {
                                self.arrCheque.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            //cell?.lblDocumentName.text = self.arrCheque[indexPath.row] as? String
                            self.tblViewAdditionalDoc.reloadData()
                        }
                        else if self.indexSection == 4
                        {
                            //self.arrBankStatement.add(absoluteString)
                            if self.arrBankStatement.contains("/Choose File") {
                                //self.arrBankStatement.remove("/Choose File")
                                self.arrBankStatement.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                                
                            }
                            else
                            {
                                self.arrBankStatement.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            //cell?.lblDocumentName.text = self.arrBankStatement[indexPath.row] as? String
                            self.tblViewAdditionalDoc.reloadData()
                        }
                        else if self.indexSection == 5
                        {
                            //self.arrSignSI.add(absoluteString)
                            if self.arrSignSI.contains("/Choose File") {
                                self.arrSignSI.remove("/Choose File")
                            }
                            else
                            {
                                self.arrSignSI.replaceObject(at: self.insertIndexRow ?? 0, with: absoluteString)
                            }
                            //cell?.lblDocumentName.text = self.arrSignSI[indexPath.row] as? String
                            self.tblViewAdditionalDoc.reloadData()
                        }
                        self.strAmazonBucketPath = s3BucketName
                    }
                }

            }
            return nil
        }
    }
    
    //MARK: - Custom Delegate Method
    
    func didUploadImage_Click(_ cell: AdditionalDocumentCell)
    {
        guard let indexPath = self.tblViewAdditionalDoc.indexPath(for: cell) else {
            // Note, this shouldn't happen - how did the user tap on a button that wasn't on screen?
            return
        }
        self.indexSection = indexPath.section
        self.indexRow = indexPath.row
        self.insertIndexSection = indexPath.section
        self.insertIndexRow = indexPath.row
        
        //  Do whatever you need to do with the indexPath
        print("Button tapped on section \(indexPath.section)")
        print("Button tapped on row \(indexPath.row)")
        self.openImagePickerAlert()
    }
    
    func didViewFile_Click(_ cell: AdditionalDocumentCell)
    {
        guard let indexPath = self.tblViewAdditionalDoc.indexPath(for: cell) else {
            // Note, this shouldn't happen - how did the user tap on a button that wasn't on screen?
            return
        }
        self.indexSection = indexPath.section
        self.indexRow = indexPath.row
        
        //  Do whatever you need to do with the indexPath
        print("Button tapped on section \(indexPath.section)")
        print("Button tapped on row \(indexPath.row)")
        var strUrl : String?
        
        if indexPath.section == 0
        {
            strUrl = self.arrFinanceReport[indexPath.row] as? String
        }
        else if indexPath.section == 1
        {
            strUrl = self.arrTAN[indexPath.row] as? String
        }
        else if indexPath.section == 2
        {
            strUrl = self.arrGST[indexPath.row] as? String
        }
        else if indexPath.section == 3
        {
            strUrl = self.arrCheque[indexPath.row] as? String
        }
        else if indexPath.section == 4
        {
            strUrl = self.arrBankStatement[indexPath.row] as? String
        }
        else if indexPath.section == 5
        {
            strUrl = self.arrSignSI[indexPath.row] as? String
        }
        
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = strUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
    }
    
    func didAddAnotherFile_Click(_ cell: AdditionalDocumentCell)
    {
        guard let indexPath = self.tblViewAdditionalDoc.indexPath(for: cell) else {
            // Note, this shouldn't happen - how did the user tap on a button that wasn't on screen?
            return
        }
        self.indexSection = indexPath.section
        self.indexRow = indexPath.row
        
        //  Do whatever you need to do with the indexPath
        print("Button tapped on section \(indexPath.section)")
        print("Button tapped on row \(indexPath.row)")
        
        if indexPath.section == 0
        {
            self.arrFinanceReport.add("/Choose File")
            print("arrFinanceReport: ", self.arrFinanceReport)
            tblViewAdditionalDoc.beginUpdates()
            tblViewAdditionalDoc.insertRows(at: [IndexPath(row: self.arrFinanceReport.count-1, section: indexPath.section)], with: .automatic)
            tblViewAdditionalDoc.endUpdates()
        }
        else if indexPath.section == 4
        {
            self.arrBankStatement.add("/Choose File")
            print("arrBankStatement: ", self.arrBankStatement)
            tblViewAdditionalDoc.beginUpdates()
            tblViewAdditionalDoc.insertRows(at: [IndexPath(row: self.arrBankStatement.count-1, section: indexPath.section)], with: .automatic)
            tblViewAdditionalDoc.endUpdates()
        }
        
        //self.arrData.add(self.arrData.count + 1)
        
        //tblViewAdditionalDoc.reloadData()
    }
    
    //MARK: - Button Action
    
    @IBAction func btnBack_Click(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnAddFile_Click(sender:UIButton!)
    {
        
    }
    
    @objc func btnSubmitDoc_Click(sender: UIButton!)
    {
        print("doc call")
        self.callUserAdditionalDocumentsAPI()
    }
    
    /*@IBAction func btnSubmit_Click(_ sender: Any)
    {
        print("Submit button tapped")
        self.callUserAdditionalDocumentsAPI()
    }*/
}
