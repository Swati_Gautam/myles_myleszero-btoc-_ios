//
//  UserProfileAccountVC.swift
//  Myles Zero
//
//  Created by Chetan Rajauria on 28/08/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import iOSDropDown

class UserProfileAccountVC: UIViewController, CityNameDelegate, SearchDataDelegate
{
    @IBOutlet var contentView: UIView!
  
    @IBOutlet var permanentStateCityData: [DropDown]!
    @IBOutlet var deliveryStateCityData: [DropDown]!

    @IBOutlet var txtDeliveryCityDropdown: DropDown!
   
    @IBOutlet var lblPermanentCity: UILabel!
    @IBOutlet var lblPermanentState: UILabel!
    @IBOutlet var txtPermanentAdd3: UITextField!
    @IBOutlet var txtPermanentAdd2: UITextField!
    @IBOutlet var txtPermanentAdd1: UITextField!
    
    @IBOutlet var lblDeliveryCity: UILabel!
    @IBOutlet var lblDeliveryState: UILabel!
    @IBOutlet var txtDeliveryAdd3: UITextField!
    @IBOutlet var txtDeliveryAdd2: UITextField!
    @IBOutlet var txtDeliveryAdd1: UITextField!
    
    @IBOutlet var btnDeliveryCheckbox: UIButton!
    
    @IBOutlet var deliveryAddHeightConstraint: NSLayoutConstraint!
    @IBOutlet var viewDelivery: UIView!
    @IBOutlet var viewDeliveryAddress: UIView!
    var strSameAddress : String?
    var cityTag:Int = 0
    var stateTag:Int = 0
    
    var arrCityList = NSArray()
    var arrStateList = NSArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //self.callApiToGetEmployeeDetail()
        self.showUserProfileDetail()
    }
    override func viewDidAppear(_ animated: Bool)
    {
        //self.callApiToGetEmployeeDetail()
        super.viewDidAppear(true)
    }
    
    func showUserProfileDetail()
    {
        if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse != nil
        {
            let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
            self.txtPermanentAdd1.text = UserProfileInfo.address1permanent
            self.txtPermanentAdd2.text = UserProfileInfo.address2permanent
            self.txtPermanentAdd3.text = UserProfileInfo.pincode
            
            self.txtDeliveryAdd1.text = UserProfileInfo.address1delivery
            self.txtDeliveryAdd2.text = UserProfileInfo.address2delivery
            self.txtDeliveryAdd3.text = UserProfileInfo.pincodedelivery
            
            self.deliveryStateCityData[0].text = UserProfileInfo.statedelivery
            self.deliveryStateCityData[1].text = UserProfileInfo.citydelivery
            self.permanentStateCityData[0].text = UserProfileInfo.state
            self.permanentStateCityData[1].text = UserProfileInfo.city
            
            arrCityList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray)!
            self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
            
            self.didGetPermanentData(objUserProfileInfo: UserProfileInfo)
            self.didGetDeliveryData(objUserProfileInfo: UserProfileInfo)
            
            if UserProfileInfo.isAddressSame == "0" //"1"
            {
                btnDeliveryCheckbox.setImage(UIImage(named: "unselected_checkbox"), for: UIControl.State.normal)
                self.viewDelivery.isHidden = false
                self.deliveryAddHeightConstraint.constant = 350
                self.strSameAddress = "1" //"0"
            }
            else if UserProfileInfo.isAddressSame == "1" //"0"
            {
                btnDeliveryCheckbox.setImage(UIImage(named: "selected_checkbox"), for: UIControl.State.normal)
                self.viewDelivery.isHidden = true
                self.deliveryAddHeightConstraint.constant = 100
                self.strSameAddress = "0" //"1"
            }
        }        
    }
    
    func didGetDeliveryData(objUserProfileInfo: UserProfileInfoResponse)
    {
        let arr1 = arrStateList
        for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
            if strStateCode.contains(objUserProfileInfo.statedelivery)
            {
                self.deliveryStateCityData[0].text = strStateName.uppercased()
            }
            if strStateCode.contains(objUserProfileInfo.state)
            {
                self.permanentStateCityData[0].text = strStateName.uppercased()
            }
            self.deliveryStateCityData[0].optionArray.append(strStateName)
        }
        
        self.permanentStateCityData[0].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.deliveryStateCityData[0].text = selectedText
            self.deliveryStateCityData[0].textColor = UIColor.black
        }
        
        for i in 0..<self.arrCityList.count
        {
            var objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[i] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = (DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse)!
            self.deliveryStateCityData[1].optionArray.append(objCityList.cityName)
        }
        self.deliveryStateCityData[1].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.deliveryStateCityData[1].text = selectedText
            self.deliveryStateCityData[1].textColor = UIColor.black
        }
    }
    
    func didGetPermanentData(objUserProfileInfo: UserProfileInfoResponse)
    {
        let arr1 = arrStateList
        for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            self.permanentStateCityData[0].optionArray.append(strStateName)
        }
        
        self.permanentStateCityData[0].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.permanentStateCityData[0].text = selectedText
            self.permanentStateCityData[0].textColor = UIColor.black
        }
        
        for i in 0..<self.arrCityList.count
        {
            var objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[i] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = (DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse)!
            self.permanentStateCityData[1].optionArray.append(objCityList.cityName)
        }
        self.permanentStateCityData[1].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.permanentStateCityData[1].text = selectedText
            self.permanentStateCityData[1].textColor = UIColor.black
        }
    }
    
    //MARK: - Drop Down Delegate Methods
    
    func didGetCityName(strCityName: String, strCityID: String)
    {
        print("strCityName: ", strCityName)
        print("strCityName: ", strCityID)
        
        if cityTag == 1051
        {
            self.lblPermanentCity.text = strCityName
            self.lblPermanentCity.textColor = UIColor.black
        }
        else if cityTag == 1053
        {
           self.lblDeliveryCity.text = strCityName
           self.lblDeliveryCity.textColor = UIColor.black
        }
        
//        self.lblCity.text! = strCityName
//        cityID = Int(strCityID)!
//        self.lblCity.textColor = UIColor.black
    }
    
    func didGetDataName(strName: String, strID: String)
    {
        print("strCityName: ", strName)
        //print("strCityName: ", strCityID)
        
        if stateTag == 1050
        {
            self.lblPermanentState.text = strName
            self.lblPermanentState.textColor = UIColor.black
        }
        else if stateTag == 1052
        {
            self.lblDeliveryState.text = strName
            self.lblDeliveryState.textColor = UIColor.black
        }
    }
    
    //MARK: - Submit Document WebService
    
    func submitDocumentDetailWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
        /*let str = UserProfileInfo.aadharfile as String
        let arrValue = str.components(separatedBy: "/")
        let strFilePath = String(format: "https://%@/%@/%@/", arrValue[2], arrValue[3], arrValue[4])
        var strAdhaar : String = ""
        var strPan : String = ""
        var strDL : String = ""
        var strPassport : String = ""
        
        if UserProfileInfo.aadharfile != ""
        {
            let arr1 = UserProfileInfo.aadharfile.components(separatedBy: "/")
            strAdhaar = String(format: "%@", arr1[5])
        }        
        if UserProfileInfo.panfile != ""
        {
            let arr2 = UserProfileInfo.panfile.components(separatedBy: "/")
            strPan = String(format: "%@", arr2[5])
        }
        if UserProfileInfo.dlfile != ""
        {
            let arr3 = UserProfileInfo.dlfile.components(separatedBy: "/")
            strDL = String(format: "%@", arr3[5])
        }
        if UserProfileInfo.passportfile != ""
        {
            let arr4 = UserProfileInfo.passportfile.components(separatedBy: "/")
            strPassport = String(format: "%@", arr4[5])
        }*/
                
        var dictParam:[String:AnyObject]!
        //(self.viewReportLossItem.txtIncidentTime.text ?? "")
        
        dictParam = ["clientid": strClientID as AnyObject, //(userDetails.clientID ?? "")
                     "aadharfile":(UserProfileInfo?.aadharfile ?? "") as AnyObject as AnyObject,
                     "aadharno":(UserProfileInfo?.aadharno ?? "") as AnyObject,
                     "aadharname":(UserProfileInfo?.aadharname ?? "") as AnyObject,
                     "aadhardob":(UserProfileInfo?.aadhardob ?? "") as AnyObject,
                     "Passportfile":(UserProfileInfo?.passportfile ?? "") as AnyObject as AnyObject,
                     "Passportno":"" as AnyObject,
                     "Passportsurname":"" as AnyObject,
                     "Passportfirstname":"" as AnyObject,
                     "PassportGender":"" as AnyObject, //Male
                     "Passportcountryco":"" as AnyObject,
                     "Passporttype":"" as AnyObject,
                     "passportexpirydate":"" as AnyObject, //07/12/2020
                     "PassportMRZ1":"" as AnyObject,
                     "PassportMRZ2":"" as AnyObject,
                     "PassportDOb":"" as AnyObject, //06/12/1999
                     "dlfile":(UserProfileInfo?.dlfile ?? "") as AnyObject as AnyObject,
                     "dlno":(UserProfileInfo?.dlNo ?? "") as AnyObject,
                     "panfile":(UserProfileInfo?.panfile ?? "") as AnyObject as AnyObject,
                     "panno":(UserProfileInfo?.panNo ?? "") as AnyObject,
                     "doctype":"aadhaar" as AnyObject,
                     "filepath":"",//strFilePath as AnyObject,
                     "Delieveryaddress":"" as AnyObject,
                     "PermanantAddress":"" as AnyObject,
                     "deliverydate":(UserProfileInfo?.deliverydate ?? "") as AnyObject as AnyObject,
                     "aadharverify":0 as AnyObject,
                     "Passportverify":0 as AnyObject,
                     "Panverify":0 as AnyObject,
                     "dlverify":0 as AnyObject,
                     "jsonaadharverifydata":"" as AnyObject,
                     "jsonpassportverifydata":"" as AnyObject,
                     "jsonPanverifydata":"" as AnyObject,
                     "jsondlverifydata":"" as AnyObject,
                     "address1permanent":(self.txtPermanentAdd1.text ?? "") as AnyObject,
                     "address2permanent":(self.txtPermanentAdd2.text ?? "") as AnyObject,
                     "state":(self.permanentStateCityData[0].text ?? "") as AnyObject,
                     "city":(self.permanentStateCityData[1].text ?? "") as AnyObject,
                     "pincode":(self.txtPermanentAdd3.text ?? "") as AnyObject,
                     "address1delivery":(self.txtDeliveryAdd1.text ?? "") as AnyObject,
                     "address2delivery":(self.txtDeliveryAdd2.text ?? "") as AnyObject,
                     "statedelivery":(self.deliveryStateCityData[0].text ?? "") as AnyObject,
                     "citydelivery":(self.deliveryStateCityData[1].text ?? "") as AnyObject,
                     "pincodedelivery":(self.txtDeliveryAdd3.text ?? "") as AnyObject,
                     "sameaddress":self.strSameAddress as AnyObject] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.uploadDocumentAuthBridgeExperian(APIConstants.API_UploadDocumentAuthBridgeExperian, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("response: %@", response)
                    let responseKey = response.value(forKey:"response") as? AnyObject
                    if responseKey is NSNull //((response.value(forKey:"response") as? NSNull) == nil)
                    {
                        
                        
                    }
                    if responseKey is NSNull //((response.value(forKey:"response") as? NSNull) == nil)
                    {
                        let status = response.value(forKey:"status") as? Int
                        if status == 1
                        {
                            let strMessage = response.value(forKey:"message") as! String
                            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                            self.callApiToGetEmployeeDetail()
                        }
                        
                        else if status == 0
                        {
                            //let strMessage = response.value(forKey:"message") as! String
                            //CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                        }
                    }

                }
            }
        })
    }
    
    
    func callApiToGetEmployeeDetail()
    {
        //let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
        
        let strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String
        
        var dictLoginParam:[String:AnyObject]!
        dictLoginParam = ["clientid":strClientID] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.getEmployeeProfileDetail(APIConstants.API_GetEmployeeDetail, dataDict: dictLoginParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion: { (response, error) in
            
            DispatchQueue.main.async {
                //hud!.dismiss()
                if error != nil
                {
                    
                }
                else
                {
                    let dict = response as! NSDictionary
                    let strStatus = dict.value(forKey: "status") as! Int
                    if strStatus == 1
                    {
                        let arr = response.value(forKey: kResponseInfo) as! NSArray
                        let dict = (arr[0] as AnyObject) as! NSDictionary
                        let userProfileDetail  = UserProfileInfoResponse.init(fromDictionary:(dict) as! [String : Any])
                        DefaultsValues.setCustomObjToUserDefaults(userProfileDetail, forKey: kUserProfileDetails)
                        self.showUserProfileDetail()
                    }
                }
            }
        })
    }

    //MARK: - Button Actions
    @IBAction func btnPermanentState_Click(_ sender: UIButton)
    {
        self.stateTag = sender.tag
        let objSearchCityStateVC = Constants.userProfileStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCityState) as! SearchCityStateVC
        objSearchCityStateVC.selectDataDelegate = self
        self.present(objSearchCityStateVC, animated: true, completion: nil)
    }
    
    @IBAction func btnPermanentCity_Click(_ sender: UIButton)
    {
        self.cityTag = sender.tag
        let objSearchCityVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchTableVC) as! SearchCityVC
        objSearchCityVC.selectCityNameDelegate = self
        objSearchCityVC.modalPresentationStyle = .fullScreen
        self.present(objSearchCityVC, animated: true, completion: nil)
    }
    
    @IBAction func btnDeliveryState_Click(_ sender: UIButton)
    {
        self.stateTag = sender.tag
        let objSearchCityStateVC = Constants.userProfileStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCityState) as! SearchCityStateVC
        objSearchCityStateVC.selectDataDelegate = self
        objSearchCityStateVC.modalPresentationStyle = .fullScreen
        self.present(objSearchCityStateVC, animated: true, completion: nil)
    }
    
    @IBAction func btnDeliveryCity_Click(_ sender: UIButton)
    {
        self.cityTag = sender.tag
        let objSearchCityVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchTableVC) as! SearchCityVC
        objSearchCityVC.selectCityNameDelegate = self
        objSearchCityVC.modalPresentationStyle = .fullScreen
        self.present(objSearchCityVC, animated: true, completion: nil)
    }
    
    @IBAction func btnSameAddress_Click(_ sender: UIButton)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "unselected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = false
            self.strSameAddress = "0"
            self.viewDelivery.isHidden = false
            self.deliveryAddHeightConstraint.constant = 350
        }
        else
        {
            sender.setImage(UIImage(named: "selected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = true
            self.strSameAddress = "1"
            self.viewDelivery.isHidden = true
            self.deliveryAddHeightConstraint.constant = 100
        }
    }
    
    @IBAction func btnUpdateAddress_Click(_ sender: Any)
    {
        if self.txtPermanentAdd1.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter permanent address", strAction1: "OK", viewController: self)
        }
        else if self.txtPermanentAdd2.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the address", strAction1: "OK", viewController: self)
        }
        else if self.txtPermanentAdd3.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter pincode", strAction1: "OK", viewController: self)
        }
        else if self.permanentStateCityData[0].text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the state", strAction1: "OK", viewController: self)
        }
        else if self.permanentStateCityData[1].text == ""         {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the city", strAction1: "OK", viewController: self)
        }
        else
        {
            self.submitDocumentDetailWebService()
        }
    }
}
