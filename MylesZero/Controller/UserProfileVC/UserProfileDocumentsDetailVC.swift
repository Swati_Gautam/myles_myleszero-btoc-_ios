//
//  UserProfileDocumentsDetailVC.swift
//  Myles Zero
//
//  Created by Chetan Rajauria on 01/09/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class UserProfileDocumentsDetailVC: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var viewAdhaarCard: UIView!
    @IBOutlet weak var viewDrivingLicence: UIView!
    @IBOutlet weak var viewPanCard: UIView!
    
    @IBOutlet weak var btnEditAdhaarCard: UIButton!
    @IBOutlet weak var imgViewAdhaarCard: UIImageView!
    @IBOutlet weak var lblAdhaarCardNo: UILabel!
    @IBOutlet weak var lblAdhaarCardName: UILabel!
    @IBOutlet weak var lblAdhaarCardDOB: UILabel!
    
    @IBOutlet weak var imgViewDrivingLicence: UIImageView!
    @IBOutlet weak var lblDrivingLicenceNo: UILabel!
    @IBOutlet weak var lblDrivingLicenceGender: UILabel!
    //@IBOutlet weak var lblDrivingLicenceName: UILabel!
     @IBOutlet weak var btnEditDrivingLicence: UIButton!
    
    @IBOutlet weak var imgViewPanCard: UIImageView!
    @IBOutlet weak var lblPanCardNo: UILabel!
    //@IBOutlet weak var lblPanCardName: UILabel!
    //@IBOutlet weak var lblPanCardDOB: UILabel!
    @IBOutlet weak var btnEditPanCard: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse != nil
        {
            let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
            
            self.lblAdhaarCardNo.text = UserProfileInfo?.aadharno
            self.lblAdhaarCardName.text = UserProfileInfo?.aadharname
            self.lblAdhaarCardDOB.text = UserProfileInfo?.aadhardob
            
            CommonMethodsMylesZero.setBorder(button: self.btnEditAdhaarCard)
            CommonMethodsMylesZero.setBorder(button: self.btnEditDrivingLicence)
            CommonMethodsMylesZero.setBorder(button: self.btnEditPanCard)
            
            self.imgViewAdhaarCard.sd_setImage(with: URL(string: UserProfileInfo!.aadharfile), placeholderImage: UIImage(named: "car_placeholder"))
            
            self.lblDrivingLicenceNo.text = UserProfileInfo?.dlNo
            //self.lblDrivingLicenceGender.text = UserProfileInfo.pincodedelivery
            self.imgViewDrivingLicence.sd_setImage(with: URL(string: UserProfileInfo!.dlfile), placeholderImage: UIImage(named: "car_placeholder"))
            
            self.lblPanCardNo.text = UserProfileInfo?.panNo
            self.imgViewPanCard.sd_setImage(with: URL(string: UserProfileInfo!.panfile), placeholderImage: UIImage(named: "car_placeholder"))
            
        }
        
    }
    
    @IBAction func btnUpdate_Click(_ sender: Any)
    {
        
    }
}
