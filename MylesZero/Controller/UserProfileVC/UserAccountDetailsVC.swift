//
//  UserAccountDetailsVC.swift
//  Myles Zero
//
//  Created by skpissay on 13/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class UserAccountDetailsVC: UIViewController {

    @IBOutlet weak var lblDeliveryPincode: UILabel!
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    @IBOutlet weak var lblDeliveryState: UILabel!
    @IBOutlet weak var lblDeliveryCity: UILabel!
    
    @IBOutlet weak var lblPermanentPincode: UILabel!
    @IBOutlet weak var lblPermanentAddress: UILabel!
    @IBOutlet weak var lblPermanentState: UILabel!
    @IBOutlet weak var lblPermanentCity: UILabel!
    
    @IBOutlet weak var btnEditPermanentAddress: UIButton!
    @IBOutlet weak var btnEditDeliveryAddress: UIButton!
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var viewEnterDeliveryAddress: UIView!
    @IBOutlet weak var viewEnterPermanentAddress: UIView!
    @IBOutlet weak var viewDeliveryAddress: UIView!
    @IBOutlet weak var viewPermanentAddress: UIView!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //self.showUserProfileDetail()
        
        CommonMethodsMylesZero.setBorder(button: self.btnEditPermanentAddress)
        CommonMethodsMylesZero.setBorder(button: self.btnEditDeliveryAddress)

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Profile Detail Function
    
    func showUserProfileDetail()
    {
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
        self.lblPermanentAddress.text = UserProfileInfo.address1permanent
        self.lblPermanentPincode.text = UserProfileInfo.pincode
        self.lblPermanentCity.text = UserProfileInfo.city
        self.lblPermanentState.text = UserProfileInfo.city.uppercased() //UserProfileInfo.state
        
        self.lblDeliveryAddress.text = UserProfileInfo.address1delivery
        self.lblDeliveryPincode.text = UserProfileInfo.pincodedelivery
        self.lblDeliveryCity.text = UserProfileInfo.citydelivery
        self.lblDeliveryState.text = UserProfileInfo.citydelivery.uppercased()//UserProfileInfo.statedelivery
    }

    @IBAction func btnUpdate_Click(_ sender: Any) {
    }
    
    @IBAction func btnEditPermanentAdd_Click(_ sender: Any) {
    }
    
    @IBAction func btnEditDeliveryAdd_Click(_ sender: Any) {
    }
}
