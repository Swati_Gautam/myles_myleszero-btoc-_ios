//
//  SearchCityStateVC.swift
//  Myles Zero
//
//  Created by Chetan Rajauria on 01/09/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class SearchCityStateVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {

    @IBOutlet var lblSearchTitle: UILabel!
    @IBOutlet var tblSearchList: UITableView!
    
    weak var selectDataDelegate: SearchDataDelegate?
    
    var resultSearchController = UISearchController()
    var arrCityList = NSArray()
    var arrFilteredCity = NSArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //arrCityList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray)!
        arrCityList = ["New Delhi","Uttar Pradesh","Madhya Pradesh","Chandigarh","Haryana","Maharashtra","Punjab","Rajasthan","Gujarat","Kerala"]
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            tblSearchList.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        // Reload the table
        tblSearchList.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:-  UISearchResultsUpdating delegate function
    
    func updateSearchResults(for searchController: UISearchController)
    {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        
        let searchPredicate: NSPredicate
        searchPredicate = NSPredicate(format: "CityName CONTAINS[C] %@", searchString)
        
        self.arrFilteredCity = (self.arrCityList as NSArray).filtered(using: searchPredicate) as NSArray
        
        print ("array = \(self.arrFilteredCity)")
        
        // Reload the tableview.
        self.tblSearchList.reloadData()
    }
    
    // MARK:- Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the number of rows
        if  (resultSearchController.isActive) {
            return arrFilteredCity.count
        } else {
            return arrCityList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idSearchCityStateCell"
        let cell:SearchCityStateTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SearchCityStateTableViewCell?)!
        //let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCityCell", for: indexPath)
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        var objCityList : CityListResponse?
        
        if (resultSearchController.isActive) {
            //cell.lblCityName.text! = filteredTableData[indexPath.row]
            //objCityList = CityListResponse.init(fromDictionary: (arrFilteredCity[indexPath.row] as! NSDictionary) as! [String : Any])
            //DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            //objCityList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListResponse
            cell.lblName.text! = arrFilteredCity[indexPath.row] as! String
            return cell
        }
        else {
            //cell.lblCityName.text! = arrData[indexPath.row]
            //objCityList = CityListResponse.init(fromDictionary: (arrCityList[indexPath.row] as! NSDictionary) as! [String : Any])
            //DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            //objCityList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListResponse
            cell.lblName.text! = arrCityList[indexPath.row] as! String
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var strCityName : String = ""
        //var objCityList : CityListResponse?
        
        if (resultSearchController.isActive)
        {
            strCityName = arrFilteredCity[indexPath.row] as! String
            //objCityList = CityListResponse.init(fromDictionary: (arrFilteredCity[indexPath.row] as! NSDictionary) as! [String : Any])
        }
        else
        {
            strCityName = arrCityList[indexPath.row] as! String
            //objCityList = CityListResponse.init(fromDictionary: (arrCityList[indexPath.row] as! NSDictionary) as! [String : Any])
            
        }
        //DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
        //objCityList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListResponse
        //strCityName = objCityList!.cityName!
        selectDataDelegate?.didGetDataName(strName: strCityName, strID: "")
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }

    //MARK: - Button Actions
    
    @IBAction func btnCancel_Click(_ sender: Any)
    {
        DispatchQueue.main.async {            
            self.dismiss(animated: true, completion: nil)
        }
    }
}

protocol SearchDataDelegate : class
{
    func didGetDataName(strName: String, strID: String)
}

