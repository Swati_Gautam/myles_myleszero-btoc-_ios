//
//  UserProfileMylesZeroVC.swift
//  Myles
//
//  Created by Swati Gautam on 08/03/21.
//  Copyright © 2021 Myles. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class UserProfileMylesZeroVC: UIViewController
{
    @IBOutlet weak var btnAdditionalDocuments: UIButton!
    @IBOutlet weak var viewAdditionalDocuments: UIView!
    @IBOutlet weak var btnUserProfile: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var btnAccountDetails: UIButton!
    @IBOutlet weak var lblUserContactNo: UILabel!
    @IBOutlet weak var lblUserEmailID: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnDocuments: UIButton!
    @IBOutlet weak var viewDocuments: UIView!
    @IBOutlet weak var viewAccountDetails: UIView!
    @IBOutlet weak var btnSideMenu: UIButton!
    var strClientID : String?
    
    @IBOutlet weak var viewContainerView: UIView!
    let customSideMenuManager = SideMenuManager()
    
    
    //MARK: - Child View Controllers
    /*private lazy var UserAccountVC: UserAccountDetailsVC = {
        
        let controllerAccount:UserAccountDetailsVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_UserAccountDetails) as! UserAccountDetailsVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerAccount)
        
        return controllerAccount
    }()*/
    
    private lazy var UserAccountVC: UserProfileAccountVC = {
        
        let controllerAccount:UserProfileAccountVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_UserProfileAccountVC) as! UserProfileAccountVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerAccount)
        
        return controllerAccount
    }()
    
    private lazy var UserDocumentVC: UserProfileDocumentsVC = {
        
        let controllerDocuments:UserProfileDocumentsVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_UserProfileDocuments) as! UserProfileDocumentsVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerDocuments)
        
        return controllerDocuments
    }()
    
    private lazy var UserAdditionalDocuments: AdditionalDocumentsVC = {
        
        let controllerAccount:AdditionalDocumentsVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_AdditionalDocuments) as! AdditionalDocumentsVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerAccount)
        
        return controllerAccount
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.viewAccountDetails.isHidden = false
        self.viewDocuments.isHidden = true
        self.viewAdditionalDocuments.isHidden = true
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        //self.lblUserName.text = String(format: "%@ %@", userDetails.fname, userDetails.lname)
        //self.lblUserEmailID.text = String(format: "%@", userDetails.emailId)
        //self.lblUserContactNo.text = String(format: "%@", userDetails.phone)
        //self.strClientID = String(format: "%@", userDetails.clientID)
        
        //self.lblUserName.text = String(format: "%@ %@", UserDefaults.standard.value(forKey: KUSER_FNAME) as! CVarArg,  UserDefaults.standard.value(forKey: KUSER_LNAME) as! CVarArg)
        self.lblUserName.text = UserDefaults.standard.value(forKey: KUSER_NAME) as? String
        self.lblUserEmailID.text = UserDefaults.standard.value(forKey: KUSER_EMAIL) as? String //String(format: "%@", UserDefaults.standard.value(forKey: KUSER_EMAIL) as! CVarArg)
        self.lblUserContactNo.text = UserDefaults.standard.value(forKey: KUSER_PHONENUMBER) as? String //String(format: "%@", UserDefaults.standard.value(forKey: KUSER_PHONENUMBER) as! CVarArg)
        self.strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String //String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        self.btnAdditionalDocuments.setTitle(String(format: "%@\n%@","ADDITIONAL","DOCUMENTS"), for: UIControl.State.normal)
        self.btnAccountDetails.setTitle(String(format: "%@\n%@","ACCOUNT","DETAILS"), for: UIControl.State.normal)
        
        callApiToGetEmployeeDetail()
        //Do any additional setup after loading the view.
    }
    
    func callApiToGetEmployeeDetail()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
        
        var dictLoginParam:[String:AnyObject]!
        dictLoginParam = ["clientid": self.strClientID] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
    APIFunctions.sharedInstance.getEmployeeProfileDetail(APIConstants.API_GetEmployeeDetail, dataDict: dictLoginParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion: { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                if error != nil
                {
                                        
                }
                else
                {
                    let dict = response as! NSDictionary
                    let strStatus = dict.value(forKey: "status") as! Int
                    if strStatus == 1
                    {
                        let arr = response.value(forKey: kResponseInfo) as! NSArray
                        let dict = (arr[0] as AnyObject) as! NSDictionary
                        let userProfileDetail  = UserProfileInfoResponse.init(fromDictionary:(dict) as! [String : Any])
                        DefaultsValues.setCustomObjToUserDefaults(userProfileDetail, forKey: kUserProfileDetails)
                        //self.add(asChildViewController: self.UserAccountVC)
                    }
                    else
                    {
                       /*let strMsg = dict.value(forKey: "message") as! String
                        CommonMethodsMylesZero.showAlertController1(strTitle: "Error", strMsg: strMsg, strAction1: "OK", strAction2: "Cancel", viewController: self)*/
                    }
                    self.add(asChildViewController: self.UserAccountVC)
                }
            }
        })
    }
    
    //MARK: - Helper Methods
    
    func add(asChildViewController viewController: UIViewController)
    {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        self.viewContainerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.viewContainerView.bounds
        //viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    func remove(asChildViewController viewController: UIViewController)
    {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    // MARK: - Side Menu Function
    
    private func setupSideMenu()
    {
        // Define the menus
        
        guard let objSideMenuVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SideMenu) as? SideMenuVC else { return }
        
        let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objSideMenuVC)
        //let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: SideMenuVC)
        customSideMenuManager.menuLeftNavigationController = leftMenuNavigationController
        
        customSideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        //customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        customSideMenuManager.menuWidth = 300.0
        customSideMenuManager.menuFadeStatusBar = false
        
        //leftMenuNavigationController.statusBarEndAlpha = 0
        
        present(leftMenuNavigationController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let sideMenuNavigationController = segue.destination as? UISideMenuNavigationController
        {
            sideMenuNavigationController.sideMenuManager = customSideMenuManager
        }
    }
    
    //MARK: - Button Action

    @IBAction func btnSideMenu_Click(_ sender: Any)
    {
        self.setupSideMenu()
    }
    @IBAction func btnUserProfile_Click(_ sender: Any)
    {
        
    }
    @IBAction func btnAdditionalDocuments_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.viewAdditionalDocuments.isHidden = false
            self.viewAccountDetails.isHidden = true
            self.viewDocuments.isHidden = true
            remove(asChildViewController: UserDocumentVC)
            remove(asChildViewController: UserAccountVC)
            add(asChildViewController: UserAdditionalDocuments)
        }
        else
        {
            //self.viewAccountDetails.isHidden = true
            //self.viewDocuments.isHidden = false
            //remove(asChildViewController: UserAccountVC)
            //add(asChildViewController: UserDocumentVC)
        }
    }
    
    @IBAction func btnAccountDetails_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.viewAccountDetails.isHidden = false
            self.viewDocuments.isHidden = true
            self.viewAdditionalDocuments.isHidden = true
            remove(asChildViewController: UserDocumentVC)
            remove(asChildViewController: UserAdditionalDocuments)
            add(asChildViewController: UserAccountVC)
        }
        else
        {
//            self.viewAccountDetails.isHidden = true
//            self.viewDocuments.isHidden = false
//            remove(asChildViewController: UserAccountVC)
//            add(asChildViewController: UserDocumentVC)
        }
    }
    
    @IBAction func btnDocuments_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.viewDocuments.isHidden = false
            self.viewAccountDetails.isHidden = true
            self.viewAdditionalDocuments.isHidden = true
            remove(asChildViewController: UserAccountVC)
            remove(asChildViewController: UserAdditionalDocuments)
            add(asChildViewController: UserDocumentVC)
        }
        else
        {
//            self.viewAccountDetails.isHidden = false
//            self.viewDocuments.isHidden = true
//            remove(asChildViewController: UserDocumentVC)
//            add(asChildViewController: UserAccountVC)
        }
    }
    
    @IBAction func btnPermanentCity_Click(_ sender: Any) {
    }
}
