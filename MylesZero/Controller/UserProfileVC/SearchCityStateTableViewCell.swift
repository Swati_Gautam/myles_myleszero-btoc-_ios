//
//  SearchCityStateTableViewCell.swift
//  Myles Zero
//
//  Created by Chetan Rajauria on 01/09/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class SearchCityStateTableViewCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
