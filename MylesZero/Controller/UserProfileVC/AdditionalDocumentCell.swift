//
//  AdditionalDocumentCell.swift
//  Myles Zero
//
//  Created by Swati Gautam on 21/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class AdditionalDocumentCell: UITableViewCell
{
    weak var cellAdditionalDocDelegate: AdditionalDocumentCellDelegate?
    
    @IBOutlet weak var btnAddAnotherFile: UIButton!
    @IBOutlet weak var btnUploadDoc: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var btnViewFile: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        CommonMethodsMylesZero.setBorder(button: self.btnUploadDoc)
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func didUploadImage_Click(_ sender: UIButton)
    {
        cellAdditionalDocDelegate?.didUploadImage_Click(self)
    }
    
    @IBAction func btnViewFile_Click(_ sender: Any)
    {
        cellAdditionalDocDelegate?.didViewFile_Click(self)
    }
    
    @IBAction func didAddAnotherFile_Click(_ sender: UIButton)
    {
    cellAdditionalDocDelegate?.didAddAnotherFile_Click(self)
    }
}

protocol AdditionalDocumentCellDelegate : class {
    func didUploadImage_Click(_ cell: AdditionalDocumentCell)
    func didAddAnotherFile_Click(_ cell: AdditionalDocumentCell)
    func didViewFile_Click(_ cell: AdditionalDocumentCell)
    //func didPressMenuButton(_tag: Int )
}
