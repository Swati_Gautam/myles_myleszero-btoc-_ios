//
//  CarImagesContainerVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 14/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class CarImagesContainerVC: UIViewController {

    //@IBOutlet weak var imgCarList: UIImageView!
    
    let imgCarList: UIImageView = {
        let v = UIImageView()
        //v.translatesAutoresizingMaskIntoConstraints = false
        //v.backgroundColor = .white
        //v.textAlignment = .center
        return v
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(imgCarList)
        self.imgCarList.frame = CGRect(x: 10, y: 10, width: self.view.frame.size.width - 100, height: 140)
        //self.imgCarList.contentMode = .center
        //self.imgCarList.backgroundColor = UIColor.blue
        
        /*NSLayoutConstraint.activate([
            imgCarList.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imgCarList.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imgCarList.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            ])*/
    }
}
