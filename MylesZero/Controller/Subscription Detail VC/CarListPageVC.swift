//
//  CarListPageVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 14/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class CarListPageVC: UIPageViewController {
    var carImages = [String]()
    var vc = CarImagesContainerVC()
    
    let colors: [UIColor] = [
        .red,
        .green,
        .blue,
        .cyan,
        .yellow,
        .orange
    ]

    var pages: [UIViewController] = [UIViewController]()

    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        dataSource = self
        delegate = nil

        // instantiate "pages"
        if carImages.count > 0
        {
            for i in 0..<carImages.count
            {
                vc.imgCarList.sd_setImage(with: URL(string: carImages[i]), placeholderImage: UIImage(named: kCarPlaceholder))
                //vc.theLabel.text = "Page: \(i)"
                //vc.view.backgroundColor = colors[i]
                pages.append(vc)
            }
            setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
        }
        else
        {
            vc.imgCarList.image = UIImage(named: kCarPlaceholder)
        }
    }
}

// typical Page View Controller Data Source
extension CarListPageVC: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        //guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }

        let previousIndex = viewControllerIndex - 1

        guard previousIndex >= 0 else { return pages.last }

        guard pages.count > previousIndex else { return nil }

        return pages[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        //guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }

        let nextIndex = viewControllerIndex + 1

        guard nextIndex < pages.count else { return pages.first }

        guard pages.count > nextIndex else { return nil }

        return pages[nextIndex]
    }
}

// typical Page View Controller Delegate
extension CarListPageVC: UIPageViewControllerDelegate {

    // if you do NOT want the built-in PageControl (the "dots"), comment-out these funcs

    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {

        guard let firstVC = pageViewController.viewControllers?.first else {
            return 0
        }
        guard let firstVCIndex = pages.firstIndex(of: firstVC) else {
            return 0
        }
        return firstVCIndex
    }
}

