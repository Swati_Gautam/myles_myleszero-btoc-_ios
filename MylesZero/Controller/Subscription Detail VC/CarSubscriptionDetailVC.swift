//
//  CarSubscriptionDetailVC.swift
//  Myles Zero
//
//  Created by skpissay on 23/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import Alamofire

class CarSubscriptionDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var containerView: UIView!
    var tblCarDetail: UITableView!
    var arrSpecification = NSMutableArray()
    var arrFeatures = NSMutableArray()
    var objSearchCarDetail : SearchCarList?
    var objCarDetail : SubscriptionDetailCarDetail?
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var isUpgradeSelected : Bool = false
    var strDateForUpgradation : String?
    var dictUpgradeData : NSDictionary?
    @IBOutlet weak var contentView: UIView!
    
    var strButtonTitle = String()
    
    var isClicked : Bool = false
    
    //weak var menuDelegate: menuViewDelegate?

    @IBOutlet weak var scrollerView: UIScrollView!
    @IBOutlet weak var viewCar: UIView!
    
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblCarModelName: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var lblFuelType: UILabel!
    @IBOutlet weak var lblTransmissionType: UILabel!
    @IBOutlet weak var lblSeatingCapacity: UILabel!
    @IBOutlet weak var imgCarPics: UIImageView!
    @IBOutlet weak var lblCarColor: UILabel!
    
    var dictSelectedCarInfo : [String:AnyObject]?
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    //MARK: - Child View Controllers
    private lazy var VC_CarOverview: CarOverviewVC = {
        
        let controllerOverView:CarOverviewVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_CarOverview) as! CarOverviewVC
        controllerOverView.objCarDetail = self.objCarDetail
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerOverView)
        
        return controllerOverView
    }()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //self.contentView.backgroundColor = UIColor.red
        
        self.viewCar.layer.borderColor = UIColor.lightGray.cgColor
        self.viewCar.layer.borderWidth = 1.0
        self.viewCar.layer.masksToBounds = true
       
        //self.lblTotalAmount.text = String(format: "\u{20B9}  %@", (self.objSearchCarDetail?.price ?? 00) as String)
        //self.setOverView()
        //self.setSubscriptionData()
        self.carDetailWebservice()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    //MARK: - Helper Methods
    
    func add(asChildViewController viewController: UIViewController)
    {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        self.containerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.containerView.bounds
        //viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    func remove(asChildViewController viewController: UIViewController)
    {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    //MARK: - Set Car OverView
    
    func setOverView()
    {
        if (tblCarDetail != nil)
        {
            self.tblCarDetail.isHidden = true
            self.tblCarDetail.removeFromSuperview()
        }
        
        let controller:CarOverviewVC = self.storyboard!.instantiateViewController(withIdentifier: "idCarOverviewVC") as! CarOverviewVC
        //controller.ANYPROPERTY=THEVALUE // If you want to pass value
        controller.objCarDetail = self.objCarDetail
        controller.view.frame = self.containerView.bounds
        controller.willMove(toParent: self)
        self.containerView.addSubview(controller.view)
        self.addChild(controller)
        //controller.didMove(toParent: self)
        
        // To remove container view
        /*self.willMoveToParentViewController(nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()*/
    }
    
    func carDetailWebservice()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        var searchCarParam:[String:AnyObject]!
        
        searchCarParam = ["Cityid" : String(format: "%d", dictSelectedCarInfo?["Cityid"] as! Int),
                          "Duration": String(format: "%d", dictSelectedCarInfo?["Duration"] as! Int),
                          "FromDate": String(format: "%@", dictSelectedCarInfo?["FromDate"] as! String),
                          "ToDate": String(format: "%@", dictSelectedCarInfo?["ToDate"] as! String),
                          "ClientCoId": kCustomerCoID as AnyObject,
                          "km": String(format: "%d", dictSelectedCarInfo?["km"] as! Int),
                          "CarVariantID": String(format: "%@", self.objSearchCarDetail!.carVariantID),
                          "CarModelID": self.objSearchCarDetail!.carModelID,
                          "CarID": self.objSearchCarDetail!.carID] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        print("Dict Headers Details: ", dictHeaders)
        APIFunctions.sharedInstance.CarSubcriptionCompleteDetailRequest(APIConstants.API_SearchSubscriptionByCarID, dataDict: searchCarParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    //print("All Data: ", APIFunctions.dictSubscriptionDetailModel)
                    var dict = NSDictionary()
                    if APIFunctions.dictSubscriptionDetailModel != nil
                    {
                        dict = APIFunctions.dictSubscriptionDetailModel!
                        let objCarDetail = SubscriptionDetailCarDetail.init(fromDictionary: dict as! [String : Any]) as SubscriptionDetailCarDetail
                        self.objCarDetail = objCarDetail
                        let arr = APIFunctions.dictSubscriptionDetailModel?.value(forKey: "CarFeature") as? NSArray
                        self.setSubscriptionData()
                        
                        if arr != nil
                        {
                            let array = APIFunctions.dictSubscriptionDetailModel!.value(forKey: "CarFeature") as! NSArray
                            
                            for i in 0..<array.count
                            {
                                let dictFeatures = array[i] as? NSDictionary
                                let strType = dictFeatures!.value(forKey: "Type") as! NSString
                                let dictSpecs = array[i] as? NSDictionary
                                
                                if strType == "Specification"
                                {
                                    self.arrSpecification.add(dictSpecs as Any)
                                }
                                else if strType == "Features"
                                {
                                    self.arrFeatures.add(dictSpecs as Any)
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    func setSubscriptionData()
    {
        //objSearchCarDetail
        self.lblCarModelName.text! = self.objSearchCarDetail!.carModelName!
        print("self.objSearchCarDetail!.carModelName!: ", self.objSearchCarDetail!.carModelName!)
        self.lblCarType.text! = self.objCarDetail!.carType!
        /*if self.objCarDetail!.carType!.contains("Old")
        {
            self.lblCarType.text! = "Used"
        }*/
        
        self.lblFuelType.text! = self.objCarDetail!.fuelTypeName!
        self.lblTransmissionType.text! = self.objCarDetail!.transmissiontype!
        self.lblCarColor.text! = self.objCarDetail!.carColorName!
        self.imgCarPics.sd_setImage(with: URL(string: (self.objCarDetail!.carImageWeb!)), placeholderImage: UIImage(named: kCarPlaceholder))
        
        self.add(asChildViewController: self.VC_CarOverview)
    }
    
    func setSpecificationData()
    {
        /*self.lblCarModelName.text! = self.objSearchCarDetail!.carModelName!
        self.lblCarType.text! = self.objSearchCarDetail!.carType!
        //self.lblTotalAmount.text! = String(format: "₹ %d", Int(self.objSearchCarDetail!.price))
        //        let strAmount = dictCarList.value(forKey: "Price") as! NSNumber
        //        let finalAmount = strAmount.doubleValue
        //        self.lblTotalAmount.text! = String(format: "₹ %.2f",finalAmount)
        
        self.lblFuelType.text! = self.objSearchCarDetail!.fuelTypeName!
        self.lblTransmissionType.text! = self.objSearchCarDetail!.transmissiontype!
        self.lblCarColor.text! = self.objSearchCarDetail!.carColorName!
        self.imgCarPics.sd_setImage(with: URL(string: (self.objSearchCarDetail!.carImageWeb!)), placeholderImage: UIImage(named: kCarPlaceholder))*/
    }
    
    func setFeaturesData()
    {
        self.lblCarModelName.text! = self.objCarDetail!.carModelName!
        self.lblCarType.text! = self.objCarDetail!.carType!
        self.lblFuelType.text! = self.objCarDetail!.fuelTypeName!
        self.lblTransmissionType.text! = self.objCarDetail!.transmissiontype!
        self.lblCarColor.text! = self.objCarDetail!.carColorName!
        self.imgCarPics.sd_setImage(with: URL(string: (self.objCarDetail!.carImageWeb!)), placeholderImage: UIImage(named: kCarPlaceholder))
    }
    
    func setUpTableView( strTitle :String)
    {
        var cellIdentifier = String()
        //isClicked = isClick
        
        if (tblCarDetail != nil)
        {
            self.tblCarDetail.isHidden = true
            self.tblCarDetail.removeFromSuperview()
        }
        self.tblCarDetail = UITableView(frame: CGRect(x: 15, y: self.containerView.frame.origin.y, width:self.view.frame.size.width - 30, height: self.view.frame.size.height - 150))
        
        tblCarDetail.delegate = self
        tblCarDetail.dataSource = self
        tblCarDetail.backgroundColor = UIColor.white
        tblCarDetail.layer.cornerRadius = 5
        tblCarDetail.layer.masksToBounds = true
        
        if strTitle == "SPECS"
        {
            if self.arrSpecification.count > 0
            {
                cellIdentifier = "CarSpecificationCell"
                //arrSpecification = ["Engine Capacity (CC)", "Length (in mm)", "Width (in mm)",  "Peak Torque (in NM)", "Height (in mm)", "Kerb Weight (in Kgs)", "Fuel Capacity(in Ltrs)", "Boot Capacity(in Ltrs)"]
                
                self.scrollerView.isHidden = true
                self.view.backgroundColor = Constants.GREY_COLOR
                self.lblNoDataFound.isHidden = true
                self.tblCarDetail.isHidden = false
                
                tblCarDetail.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier) //CarFeaturesCell
                
                //tblCarDetail.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
                self.view.addSubview(tblCarDetail)
                
                self.tblCarDetail.estimatedRowHeight = 85.0
                self.tblCarDetail.rowHeight = UITableView.automaticDimension
                self.tblCarDetail.separatorStyle = .singleLine
                
                self.tblCarDetail.tableFooterView = UIView()
            }
            else
            {
                self.lblNoDataFound.isHidden = false
                self.tblCarDetail.isHidden = true
                if VC_CarOverview.view != nil
                {
                    remove(asChildViewController: VC_CarOverview)
                }
            }
        }
        else if strTitle == "FEATURES"
        {
            if self.arrSpecification.count > 0
            {
                cellIdentifier = "CarFeaturesCell"
                //arrFeatures = ["Home", "Profile", "Add New Group",  "Terms of Use"]
                self.scrollerView.isHidden = true
                self.view.backgroundColor = Constants.GREY_COLOR
                self.lblNoDataFound.isHidden = true
                self.tblCarDetail.isHidden = false
                
                tblCarDetail.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier) //CarFeaturesCell
                
                //tblCarDetail.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
                self.view.addSubview(tblCarDetail)
                
                self.tblCarDetail.estimatedRowHeight = 85.0
                self.tblCarDetail.rowHeight = UITableView.automaticDimension
                self.tblCarDetail.separatorStyle = .singleLine
                
                self.tblCarDetail.tableFooterView = UIView()
            }
            else
            {
                self.lblNoDataFound.isHidden = false
                self.tblCarDetail.isHidden = true
                if VC_CarOverview.view != nil
                {
                    remove(asChildViewController: VC_CarOverview)
                }
            }            
        }
        else if strTitle == "OVERVIEW"
        {
            //self.tblCarDetail.isHidden = true
            self.scrollerView.isHidden = false
            if (tblCarDetail != nil)
            {
                self.tblCarDetail.isHidden = true
                self.tblCarDetail.removeFromSuperview()
            }
            
            return
        }
        
        //print("tblCarDetail :",tblCarDetail.frame)
    }
    
    // MARK: - Button Action
    
    @IBAction func btnOverview_Click(_ sender: UIButton)
    {
        //self.setOverView()
        self.lblNoDataFound.isHidden = true
        if (tblCarDetail != nil)
        {
            self.tblCarDetail.isHidden = true
            self.tblCarDetail.removeFromSuperview()
        }
        self.strButtonTitle = sender.currentTitle!
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            add(asChildViewController: VC_CarOverview)
        }
        else
        {
            remove(asChildViewController: VC_CarOverview)
        }
        
        //self.setUpTableView(strTitle: self.strButtonTitle)
    
        //self.scrollerView.isHidden = false
        //self.tblCarDetail.isHidden = true        
        
        /*if self.strButtonTitle == "OVERVIEW"
        {
            if (tblCarDetail != nil)
            {
                tblCarDetail.removeFromSuperview()
            }
            self.tblCarDetail.isHidden = true
            self.scrollerView.isHidden = false
            //isClicked = false
        }*/
    }
    
    @IBAction func btnBack_Click(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSpecs_Click(_ sender: UIButton)
    {
        //isClicked = true
        self.strButtonTitle = sender.currentTitle!
        self.setUpTableView(strTitle: self.strButtonTitle)
    }
    
    @IBAction func btnFeatures_Click(_ sender: UIButton)
    {
        //isClicked = true
        self.strButtonTitle = sender.currentTitle!
        self.setUpTableView(strTitle: self.strButtonTitle)
    }
    
    @IBAction func btnCarSubscribe_Click(_ sender: UIButton)
    {
//        let objDocumentsEditVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: "idDocumentsEditVC") as! DocumentsEditVC
//        self.navigationController?.pushViewController(objDocumentsEditVC, animated: true)
        
        let objSubmitDocumentsVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubmitDocuments) as! SubmitDocumentsVC
        if self.isUpgradeSelected == true
        {
            objSubmitDocumentsVC.objSubscriptionDetail = self.objSubscriptionDetail
        }
        objSubmitDocumentsVC.dictUpgradeData = self.dictUpgradeData
        objSubmitDocumentsVC.strDateForUpgradation = self.strDateForUpgradation
        self.navigationController?.pushViewController(objSubmitDocumentsVC, animated: true)
    }
    
    // MARK: - Table View Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if strButtonTitle == "OVERVIEW"
        {
            return 0
        }
        else
        {
            //return UITableViewAutomaticDimension
            return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if strButtonTitle == "SPECS"
        {
            return arrSpecification.count
        }
        else if strButtonTitle == "FEATURES"
        {
            return arrFeatures.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if strButtonTitle == "SPECS"
        {
            let cell:CarSpecificationTableViewCell = self.tblCarDetail.dequeueReusableCell(withIdentifier: "CarSpecificationCell") as! CarSpecificationTableViewCell
            cell.selectionStyle = .none
            //cell.lblMenuTitle.text = self.arrMenuTitle[indexPath.row] as? String
            
            let dictSpecification = self.arrSpecification[indexPath.row] as! NSDictionary
            
            let objSpecification = SubscriptionDetailCarFeature.init(fromDictionary: dictSpecification as! [String : Any]) as SubscriptionDetailCarFeature
            cell.lblSpecsTitle.text = objSpecification.featureName
            cell.lblDetail.text = objSpecification.value
            
            return cell
        }
        else if strButtonTitle == "FEATURES"
        {
            let cell:CarFeatureTableViewCell = self.tblCarDetail.dequeueReusableCell(withIdentifier: "CarFeaturesCell") as! CarFeatureTableViewCell
            cell.selectionStyle = .none
            //cell.lblFeatureTitle.text = self.arrFeatures[indexPath.row] as? String
            let dictSpecification = self.arrFeatures[indexPath.row] as! NSDictionary
            
            let objSpecification = SubscriptionDetailCarFeature.init(fromDictionary: dictSpecification as! [String : Any]) as SubscriptionDetailCarFeature
            cell.lblFeatureTitle.text = objSpecification.featureName
            cell.lblFeatureDetail.text = objSpecification.value
            
            return cell
        }
        else
        {
            let cell:CarFeatureTableViewCell = self.tblCarDetail.dequeueReusableCell(withIdentifier: "CarFeaturesCell") as! CarFeatureTableViewCell
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //menuDelegate?.navigateToController(index: indexPath.row)
    }
}
