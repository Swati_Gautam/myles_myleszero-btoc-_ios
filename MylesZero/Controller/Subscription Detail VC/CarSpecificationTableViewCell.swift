//
//  CarSpecificationTableViewCell.swift
//  Myles Zero
//
//  Created by skpissay on 26/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class CarSpecificationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblSpecsTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
