//
//  CarOverviewVC.swift
//  Myles Zero
//
//  Created by skpissay on 11/06/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class CarOverviewVC: UIViewController {
    
    @IBOutlet weak var contentView: UIView!    
    @IBOutlet weak var scrollerView: UIScrollView!
    
    @IBOutlet weak var lblTaxAmount: UILabel!
    @IBOutlet weak var viewCar: UIView!
    
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblFuelType: UILabel!
    @IBOutlet weak var lblTransmissionType: UILabel!
    @IBOutlet weak var lblSeatingCapacity: UILabel!
    @IBOutlet weak var imgCarPics: UIImageView!
    @IBOutlet weak var lblCarColor: UILabel!
    var objCarDetail : SubscriptionDetailCarDetail?
    @IBOutlet weak var lblExtraKMCharge: UILabel!
    @IBOutlet weak var imgViewSelectedCarColor: UIImageView!
    
    @IBOutlet weak var pageContainerView: UIView!
    /*let myContainerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .gray
        return v
    }()*/
    
    var thePageVC: CarListPageVC!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.viewCar.layer.borderColor = UIColor.lightGray.cgColor
        self.viewCar.layer.borderWidth = 1.0
        self.viewCar.layer.masksToBounds = true
        self.setSubscriptionData()
        print("Object Search Detail: ", self.objCarDetail?.carImages)
        // Do any additional setup after loading the view.
        
        //view.addSubview(pageContainerView)

        // constrain it - here I am setting it to
        //  40-pts top, leading and trailing
        //  and 200-pts height
        /*NSLayoutConstraint.activate([
            myContainerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 40.0),
            myContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 40.0),
            myContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -40.0),
            myContainerView.heightAnchor.constraint(equalToConstant: 200.0),
            ])*/

        // instantiate MyPageViewController and add it as a Child View Controller
        thePageVC = CarListPageVC()
        thePageVC.carImages = self.objCarDetail?.carImages as! [String]
        addChild(thePageVC)

        // we need to re-size the page view controller's view to fit our container view
        thePageVC.view.translatesAutoresizingMaskIntoConstraints = false

        // add the page VC's view to our container view
        pageContainerView.addSubview(thePageVC.view)

        // constrain it to all 4 sides
        /*NSLayoutConstraint.activate([
            thePageVC.view.topAnchor.constraint(equalTo: myContainerView.topAnchor, constant: 0.0),
            thePageVC.view.bottomAnchor.constraint(equalTo: myContainerView.bottomAnchor, constant: 0.0),
            thePageVC.view.leadingAnchor.constraint(equalTo: myContainerView.leadingAnchor, constant: 0.0),
            thePageVC.view.trailingAnchor.constraint(equalTo: myContainerView.trailingAnchor, constant: 0.0),
            ])*/

        thePageVC.didMove(toParent: self)
    }
    
    func setSubscriptionData()
    {
        self.lblCarType.text! = self.objCarDetail!.carType!
        self.lblFuelType.text! = self.objCarDetail!.fuelTypeName!
        self.lblTransmissionType.text! = self.objCarDetail!.transmissiontype!
        self.lblCarColor.text! = self.objCarDetail!.carColorName!
        
        if objCarDetail!.carColorName == "Red" || objCarDetail!.carColorName == "red" || objCarDetail!.carColorName == "RED" || objCarDetail!.carColorName == "Super Red"
        {
            self.lblCarColor.textColor = Constants.RED_COLOR //UIColor.init(hex: Constants.RED_HEX_COLOR)
            self.imgViewSelectedCarColor.setImageColor(color: Constants.RED_COLOR)
        }
        else
        {
            self.lblCarColor.textColor = UIColor.lightGray
            self.imgViewSelectedCarColor.setImageColor(color: UIColor.lightGray)
        }
        let price : Double = (self.objCarDetail?.price ?? 0.0)
        let priceInt : Int = Int(price)
        
        self.lblTotalAmount.text = String(format: "\u{20B9} %@", String(priceInt.withCommas()))
        
        
        //let strTotalAmount = NSAttributedString(string: String(format: "\u{20B9} %@ + %@ Taxes", String(priceInt.withCommas())))
        
        // Initialize with a string and inline attribute(s)
        //let strTaxAmount = NSAttributedString(string: String(format: "+ %@ Taxes", String(taxAmount.withCommas())))
        
        // Initialize with a string and separately declared attribute(s)
        /*let myTotalAmountAttributes = [ NSAttributedString.Key.foregroundColor: Constants.RED_COLOR, NSAttributedString.Key.font: UIFont(name:"Roboto-Medium", size: 22.0)]
        let myTaxAmountAttributes = [ NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name:"Roboto-Medium", size: 14.0)]
        let attr1 = NSAttributedString(string:  String(format: "\u{20B9} %@ + %@ Taxes", String(priceInt.withCommas())), attributes: myTotalAmountAttributes)
        let attr2 = NSAttributedString(string: String(format: "+ %@ Taxes", String(taxAmount.withCommas())), attributes: myTaxAmountAttributes)*/
        
        let taxAmount : Int = (self.objCarDetail?.taxAmount ?? 00)
        self.lblTaxAmount.text = String(format: "+ %@ Taxes", String(taxAmount.withCommas()))
        //self.lblTotalAmount.text! = String(format: "%d", self.objCarDetail!.depositAmt!)
        self.lblExtraKMCharge.text! = String(format: "%@/KM", self.objCarDetail!.extraKMRate!)
        self.imgCarPics.sd_setImage(with: URL(string: (self.objCarDetail!.carImageWeb!)), placeholderImage: UIImage(named: kCarPlaceholder))
    }
    
    @IBAction func btnCarSubscribe_Click(_ sender: UIButton)
    {
        let objSubmitDocumentsVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubmitDocuments) as! SubmitDocumentsVC
        self.navigationController?.pushViewController(objSubmitDocumentsVC, animated: true)
    }
}
