//
//  SortView.swift
//  Myles Zero
//
//  Created by skpissay on 23/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class SortView: UIView {
    
    @IBOutlet weak var btnSortLowPrice: UIButton!
    
    //@IBOutlet var btnSortLowPrice: UIButton!
    @IBOutlet var btnSortHighPrice: UIButton!    
    @IBOutlet var btnSortOldYear: UIButton!
    @IBOutlet var btnSortNewYear: UIButton!
    
    @IBOutlet weak var contentView: UIView!
    
    /*class func instanceFromNib() -> UIView {
        //let theHeight = view.frame.size.height
        //viewSort = SortView(frame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: 300)) //self.view.frame.origin.x
        return UINib(nibName: "SortView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }*/
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //loadViewFromNib ()
        //customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //loadViewFromNib ()
        //customInit()
    }
    
    func loadViewFromNib() {
        //let bundle = Bundle(for: type(of: self))
        //let nib = UINib(nibName: "SortView", bundle: bundle)
        let view = Bundle.main.loadNibNamed("SortView", owner: nil, options: nil)![0] as! UIView
        //let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
        //view.btnSortLowPrice.addTarget(self, action:#selector(self.btnSortLowPrice_Click(sender:)), for: .touchUpInside)
    }
    
    private func customInit()
    {
        Bundle.main.loadNibNamed("SortView", owner: self, options: nil)

        self.addSubview(self.contentView)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
//    @objc func btnSortLowPrice_Click(sender:UIButton!)
//    {
//    }
}
