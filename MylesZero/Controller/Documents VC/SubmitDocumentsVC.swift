//
//  SubmitDocumentsVC.swift
//  Myles Zero
//
//  Created by skpissay on 26/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3
import AWSCore
import iOSDropDown

class SubmitDocumentsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CityNameDelegate, SearchDataDelegate
{
    @IBOutlet weak var lblPanImageName: UILabel!
    @IBOutlet weak var lblDLImageName: UILabel!
    @IBOutlet weak var lblAdhaarImageName: UILabel!
    @IBOutlet weak var btnDLBirthDate: UIButton!
    @IBOutlet weak var btnAdhaarDOB: UIButton!
    @IBOutlet weak var lblDateOfBirthOfDL: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var txtDrivingLicenceNo: UITextField!
    @IBOutlet weak var txtPanCardNo: UITextField!
    @IBOutlet weak var txtNameOnAdhaarCard: UITextField!
    @IBOutlet weak var lblSelectDateForAdhaar: UILabel!
    @IBOutlet weak var txtAdhaarCardNumber: UITextField!
    var isAdhaarSelected = false
    var strTypeOfDocument : String?
    @IBOutlet weak var btnUploadAdhaarPassport: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnUploadDriving: UIButton!
    @IBOutlet weak var btnUploadPanCard: UIButton!
    @IBOutlet weak var btnPassport: UIButton!
    @IBOutlet weak var btnAdhaarCard: UIButton!
    @IBOutlet weak var btnVerifyDocuments: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    var datePickerView = UIDatePicker()
    var imageURL = NSURL()
    var uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var uploadFileURL: NSURL?
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var viewAdhaarPassportContent: UIView!
    var strAdhaarBirthYear : String?
    
    var viewPassportView = PassportView()
    var dictAdhaarInfo : [String:AnyObject]?
    var dictDrivingLicenceInfo : [String:AnyObject]?
    var dictPanCardInfo : [String:AnyObject]?
    var dictPassportInfo : [String:AnyObject]?
    var uploadButtonTag:Int = 0
    var dateOfBirthTag:Int = 0
    //var strAmazonBucketPath : String?
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var isUpgradeSelected : Bool = false
    var strDateForUpgradation : String?
    var dictUpgradeData : NSDictionary?
    
    @IBOutlet var permanentStateCityData: [DropDown]!
    @IBOutlet var deliveryStateCityData: [DropDown]!
    
    @IBOutlet var txtDeliveryCityDropdown: DropDown!
    
    @IBOutlet var lblPermanentCity: UILabel!
    @IBOutlet var lblPermanentState: UILabel!
    @IBOutlet var txtPermanentAdd3: UITextField!
    @IBOutlet var txtPermanentAdd2: UITextField!
    @IBOutlet var txtPermanentAdd1: UITextField!
    
    @IBOutlet var lblDeliveryCity: UILabel!
    @IBOutlet var lblDeliveryState: UILabel!
    @IBOutlet var txtDeliveryAdd3: UITextField!
    @IBOutlet var txtDeliveryAdd2: UITextField!
    @IBOutlet var txtDeliveryAdd1: UITextField!
    
    @IBOutlet var btnDeliveryCheckbox: UIButton!
    
    @IBOutlet var deliveryAddHeightConstraint: NSLayoutConstraint!
    @IBOutlet var viewDelivery: UIView!
    @IBOutlet var viewDeliveryAddress: UIView!
    
    var strSameAddress : String?
    var cityTag:Int = 0
    var stateTag:Int = 0
    
    var arrCityList = NSArray()
    var arrStateList = NSArray()
    var strAdhaarDOB : String?
    var strPassport : String?
    
    var strDeliveryAdd1 : String?
    var strDeliveryAdd2 : String?
    var strDeliveryAdd3 : String?
    var strDeliveryState : String?
    var strDeliveryCity : String?
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    
    @IBOutlet weak var imgAdhaarIcon: UIImageView!
    
    @IBOutlet weak var btnViewAdhaarFile: UIButton!
    @IBOutlet weak var btnViewDLFile: UIButton!
    @IBOutlet weak var btnViewPancardFile: UIButton!
    
    var strAdhaarUrl : String?
    var strDLUrl : String?
    var strPancardUrl : String?
    
    /*var strAdhaarFile : String?
    var strDLFile : String?
    var strPanFile : String?*/
    
    var strAmazonBucketFilePath : String?
    var strDocType : String?
    var strAdhaarFileName : String?
    var strDLFileName : String?
    var strPanFileName : String?
    var strIsSameAddress : String?
    //var strCoricID : String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        strTypeOfDocument = "Adhaar"
        strAdhaarUrl = ""
        strDLUrl = ""
        strPancardUrl = ""
       
        //scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1152)
        
        CommonMethodsMylesZero.setBorder(button: self.btnUploadAdhaarPassport)
        CommonMethodsMylesZero.setBorder(button: self.btnUploadDriving)
        CommonMethodsMylesZero.setBorder(button: self.btnUploadPanCard)
        
        self.scrollView.canCancelContentTouches = true
        self.scrollView.delaysContentTouches = true
        
        self.lblAdhaarImageName.isHidden = false
        self.imgAdhaarIcon.isHidden = true
        //self.showUserProfileDetail()
        
        arrCityList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray)!
        self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
        let arr1 = arrStateList
        for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            //let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
            //if strStateCode.contains(objUserProfileInfo.statedelivery)
            self.deliveryStateCityData[0].optionArray.append(strStateName)
            //self.permanentStateCityData[0].optionArray.append(strStateName)
        }
        for i in 0..<self.arrCityList.count
        {
            var objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[i] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = (DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse)!
            self.deliveryStateCityData[1].optionArray.append(objCityList.cityName)
        }
        self.permanentStateCityData[0].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.deliveryStateCityData[0].text = selectedText
            self.deliveryStateCityData[0].textColor = UIColor.black
        }
        
        for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            self.permanentStateCityData[0].optionArray.append(strStateName)
        }
              
        for i in 0..<self.arrCityList.count
        {
            var objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[i] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = (DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse)!
            self.permanentStateCityData[1].optionArray.append(objCityList.cityName)
        }
        self.deliveryStateCityData[1].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.deliveryStateCityData[1].text = selectedText
            self.deliveryStateCityData[1].textColor = UIColor.black
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.callApiToGetEmployeeDetail()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        imagePicker.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - UITextField Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK: - UIImage Picker Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView?.contentMode = .scaleAspectFit
            imageView?.image = pickedImage
            self.uploadFileOnAWS(withImage: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func showUserProfileDetail()
    {
        if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse != nil
        {
            let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
            self.txtPermanentAdd1.text = UserProfileInfo?.address1permanent
            self.txtPermanentAdd2.text = UserProfileInfo?.address2permanent
            self.txtPermanentAdd3.text = UserProfileInfo?.pincode
            
            self.txtDeliveryAdd1.text = UserProfileInfo?.address1delivery
            self.txtDeliveryAdd2.text = UserProfileInfo?.address2delivery
            self.txtDeliveryAdd3.text = UserProfileInfo?.pincodedelivery
            
            self.deliveryStateCityData[0].text = UserProfileInfo?.statedelivery
            self.deliveryStateCityData[1].text = UserProfileInfo?.citydelivery
            self.permanentStateCityData[0].text = UserProfileInfo?.state
            self.permanentStateCityData[1].text = UserProfileInfo?.city
            
            arrCityList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray)!
            self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
            
            //self.didGetPermanentData(objUserProfileInfo: UserProfileInfo!)
            //self.didGetDeliveryData(objUserProfileInfo: UserProfileInfo!)
            self.didGetPermanentData(strState: (UserProfileInfo?.state)!, strCity: (UserProfileInfo?.city)!)
            self.didGetDeliveryData(strDeliveryState: (UserProfileInfo?.statedelivery)!, strDeliveryCity: (UserProfileInfo?.citydelivery)!)
            
            if UserProfileInfo?.isAddressSame == "0" //"1"
            {
                btnDeliveryCheckbox.setImage(UIImage(named: "unselected_checkbox"), for: UIControl.State.normal)
                self.viewDelivery.isHidden = false
                self.deliveryAddHeightConstraint.constant = 350
                self.strSameAddress = "1" //"0"
                self.strDeliveryAdd1 = self.txtPermanentAdd1.text
                self.strDeliveryAdd2 = self.txtPermanentAdd2.text
                self.strDeliveryAdd3 = self.txtPermanentAdd3.text
                
                //self.strDeliveryState = (UserProfileInfo?.state)!
                /*let arr1 = arrStateList
                for i in 0..<arr1.count
                {
                    let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
                    let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
                    if (strStateCode.lowercased()).contains(UserProfileInfo!.state.lowercased())
                    {
                        self.strDeliveryState = String(strStateCode)
                    }
                    if (strStateName.lowercased()).contains(UserProfileInfo!.state.lowercased())
                    {
                        self.strDeliveryState = String(strStateCode)
                    }
                }*/
                
                self.strDeliveryState = self.lblPermanentState.text
                self.strDeliveryCity = self.lblPermanentCity.text //(UserProfileInfo?.city)!
            }
            else if UserProfileInfo?.isAddressSame == "1" //"0"
            {
                btnDeliveryCheckbox.setImage(UIImage(named: "selected_checkbox"), for: UIControl.State.normal)
                self.viewDelivery.isHidden = true
                self.deliveryAddHeightConstraint.constant = 100
                self.strSameAddress = "0" //"1"
                self.strDeliveryAdd1 = self.txtDeliveryAdd1.text
                self.strDeliveryAdd2 = self.txtDeliveryAdd2.text
                self.strDeliveryAdd3 = self.txtDeliveryAdd3.text
                self.strDeliveryState = self.lblDeliveryState.text
                self.strDeliveryCity = self.lblDeliveryCity.text //(UserProfileInfo?.citydelivery)!
                
                /*let arr1 = arrStateList
                for i in 0..<arr1.count
                {
                    let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
                    let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
                    if (strStateCode.lowercased()).contains(UserProfileInfo!.state.lowercased())
                    {
                        self.strDeliveryState = String(strStateCode)
                    }
                    if (strStateCode.lowercased()).contains(UserProfileInfo!.state.lowercased()) //(strStateName.lowercased()).contains(UserProfileInfo?.statedelivery.lowercased())
                    {
                        self.strDeliveryState = String(strStateCode)
                    }
                }*/
                //self.strDeliveryState = (UserProfileInfo?.statedelivery)! //(self.deliveryStateCityData[0].text ?? "") as String
                 //(self.deliveryStateCityData[1].text ?? "") as String
            }
        }
        else
        {
            /*arrCityList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray)!
            self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
            
            self.didGetPermanentData(strDeliveryState: "", strState: "")
            self.didGetDeliveryData(strDeliveryState: "", strState: "")*/
        }
    }
    
    //func didGetDeliveryData(objUserProfileInfo: UserProfileInfoResponse)
    func didGetDeliveryData(strDeliveryState: String, strDeliveryCity: String)
    {
        let arr1 = arrStateList
         for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
            //if strStateCode.contains(objUserProfileInfo.statedelivery)
            if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse != nil
            {
                if strStateCode.contains(strDeliveryState)
                {
                    self.deliveryStateCityData[0].text = strStateName.uppercased()
                }
                //if strStateCode.contains(objUserProfileInfo.state)
                if strStateCode.contains(strDeliveryCity)
                {
                    self.permanentStateCityData[0].text = strStateName.uppercased()
                }
            }
            //self.deliveryStateCityData[0].optionArray.append(strStateName)
        }
        
        self.permanentStateCityData[0].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.deliveryStateCityData[0].text = selectedText
            self.deliveryStateCityData[0].textColor = UIColor.black
        }
        
        for i in 0..<self.arrCityList.count
        {
            var objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[i] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = (DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse)!
            //self.deliveryStateCityData[1].optionArray.append(objCityList.cityName)
        }
        self.deliveryStateCityData[1].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.deliveryStateCityData[1].text = selectedText
            self.deliveryStateCityData[1].textColor = UIColor.black
        }
    }
    
    //func didGetPermanentData(objUserProfileInfo: UserProfileInfoResponse)
    func didGetPermanentData(strState: String, strCity: String)
    {
        let arr1 = arrStateList
           for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            self.permanentStateCityData[0].optionArray.append(strStateName)
        }
        
        self.permanentStateCityData[0].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.permanentStateCityData[0].text = selectedText
            self.permanentStateCityData[0].textColor = UIColor.black
        }
        
        for i in 0..<self.arrCityList.count
        {
            var objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[i] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = (DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse)!
            self.permanentStateCityData[1].optionArray.append(objCityList.cityName)
        }
        self.permanentStateCityData[1].didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.permanentStateCityData[1].text = selectedText
            self.permanentStateCityData[1].textColor = UIColor.black
        }
    }
    
    //MARK: - Drop Down Delegate Methods
    
    func didGetCityName(strCityName: String, strCityID: String)
    {
        print("strCityName: ", strCityName)
        print("strCityName: ", strCityID)
        
        if cityTag == 1072
        {
            self.lblPermanentCity.text = strCityName
            self.lblPermanentCity.textColor = UIColor.black
        }
        else if cityTag == 1074
        {
            self.lblDeliveryCity.text = strCityName
            self.lblDeliveryCity.textColor = UIColor.black
        }
        
        //        self.lblCity.text! = strCityName
        //        cityID = Int(strCityID)!
        //        self.lblCity.textColor = UIColor.black
    }
    
    func didGetDataName(strName: String, strID: String)
    {
        print("strCityName: ", strName)
        //print("strCityName: ", strCityID)
        
        if stateTag == 1071
        {
            self.lblPermanentState.text = strName
            self.lblPermanentState.textColor = UIColor.black
        }
        else if stateTag == 1073
        {
            self.lblDeliveryState.text = strName
            self.lblDeliveryState.textColor = UIColor.black
        }
    }
    
    //MARK: - Upload File On AWS
    
    func uploadFileOnAWS(withImage image: UIImage)
    {
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        let access = Constants.AWS_ACCESS_KEY
        let secret = Constants.AWS_SECRET_KEY
        let credentials = AWSStaticCredentialsProvider(accessKey: access, secretKey: secret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast1, credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //let s3BucketName = String(format: "%@/%@", Constants.AWS_S3_BUCKET_NAME,strClientID)
        let s3BucketName = String(format: "%@", Constants.AWS_S3_BUCKET_NAME)
        let compressedImage = image.resizedImage(newSize: CGSize(width: 80, height: 80))
        let data: Data = compressedImage.pngData()!
        
        let miliSeconds: Int = CommonMethodsMylesZero.currentTimeInMiliseconds()
        let strSeconds : String = String(miliSeconds)//String(format: "%d",miliSeconds)
        let keyName = String(format: "%@/%@%@.%@",strClientID,strClientID,strSeconds,data.format)
        let remoteName = String(format: "%@%@.%@",strClientID,strSeconds,data.format)
        
        //let remoteName = String(format: "%@%@",userDetails.clientID!,generateRandomStringWithLength(length: 12)+"."+data.format)
        print("REMOTE NAME : ", remoteName)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                DispatchQueue.main.async{
                    print("Progress: \(Float(progress.fractionCompleted))")
                }
            })
        }
        expression.setValue("public-read-write", forRequestHeader: "x-amz-acl")   //4
        expression.setValue("public-read-write", forRequestParameter: "x-amz-acl")
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                if(error != nil)
                {
                    print("Failure uploading file Error: %@", error?.localizedDescription)
                }
                else
                {
                    print("Success uploading file")
                    
                    CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Document Uploaded Successfully", strAction1: "OK", viewController: self)
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data, bucket: s3BucketName, key: remoteName, contentType: "image/"+data.format, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
            if let error = task.error {
                print("Error : \(error.localizedDescription)")
            }
            
            if task.result != nil
            {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(s3BucketName).appendingPathComponent(keyName) //remoteName
                if let absoluteString = publicURL?.absoluteString
                {
                    print("Image URL : ",absoluteString)
                    let str = "https://s3.ap-southeast-1.amazonaws.com/uploadmyles/"
                    
                    self.strAmazonBucketFilePath = String(format: "https://s3.ap-southeast-1.amazonaws.com/uploadmyles/%@/", strClientID)
                    
                    if self.uploadButtonTag == 303
                    {
                        self.lblAdhaarImageName.text = remoteName
                        self.lblAdhaarImageName.textColor = UIColor.black
                        self.strAdhaarFileName = remoteName
                    }
                    else if self.uploadButtonTag == 404
                    {
                        self.lblDLImageName.text = remoteName
                        self.lblDLImageName.textColor = UIColor.black
                        self.strDLFileName = remoteName
                    }
                    else if self.uploadButtonTag == 505
                    {
                       self.lblPanImageName.text = remoteName
                       self.lblPanImageName.textColor = UIColor.black
                       self.strPanFileName = remoteName
                    }
                }
            }
            return nil
        }
        /*AWSS3TransferUtility.default().uploadFile(imageURL as URL, bucket: s3BucketName, key: remoteName, contentType: "image/"+data.format, expression: expression, completionHandler: self.completionHandler).continueWith(block: { (task:AWSTask) -> AnyObject? in
            if(task.error != nil){
                print("Error uploading file: \(String(describing: task.error?.localizedDescription))")
            }
            if(task.result != nil){
                print("Starting upload...")
            }
            return nil
        })*/
    }
    
    func generateRandomStringWithLength(length: Int) -> String
    {
        let randomString: NSMutableString = NSMutableString(capacity: length)
        let letters: NSMutableString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var i: Int = 0
        
        while i < length {
            let randomIndex: Int = Int(arc4random_uniform(UInt32(letters.length)))
            randomString.append("\(Character( UnicodeScalar( letters.character(at: randomIndex))!))")
            i += 1
        }
        return String(randomString)
    }
    
    func uploadFile (with resource: String,type: String){   //1
        
       //resource = "https://s3-ap-southeast-1.amazonaws.com/"
        
        //let key = "\(resource).\(Constants.AWS_ACCESS_KEY)"
        //let ext = "jpg"
        let key : String = String(format: "%@%@", "https://s3-ap-southeast-1.amazonaws.com/", resource)
        let imageURL = Bundle.main.url(forResource: key, withExtension: type)
        print("imageURL:\(imageURL)")
        //let resource = Bundle.main.path(forResource: resource, ofType: type)!
        //let Url = URL(fileURLWithPath: resource)
        
        let expression  = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task: AWSS3TransferUtilityTask,progress: Progress) -> Void in
            print("Progress: \(Float(progress.fractionCompleted))")
            if progress.isFinished{           //3
                print("Upload Finished...")
                //do any task here.
                /*DispatchQueue.main.async{
                    print("Progress: \(Float(progress.fractionCompleted))")
                }*/
            }
        }
        
        expression.setValue("public-read-write", forRequestHeader: "x-amz-acl")   //4
        expression.setValue("public-read-write", forRequestParameter: "x-amz-acl")
        
        completionHandler = { (task:AWSS3TransferUtilityUploadTask, error:NSError?) -> Void in
            if(error != nil){
                print("Failure uploading file")
                
            }else{
                print("Success uploading file")
            }
            } as? AWSS3TransferUtilityUploadCompletionHandlerBlock
        
        
        //5
        AWSS3TransferUtility.default().uploadFile(imageURL!, bucket: Constants.AWS_S3_BUCKET_NAME, key: String(key), contentType: resource, expression: expression, completionHandler: self.completionHandler).continueWith(block: { (task:AWSTask) -> AnyObject? in
            if(task.error != nil){
                print("Error uploading file: \(String(describing: task.error?.localizedDescription))")
            }
            if(task.result != nil){
                print("Starting upload...")
            }
            return nil
        })
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Date For Adhaar Card
    
    @objc func dateSelected()
    {
        /*let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "ccc, d MMM yyy"
        
        let dateString1 = dateformatter1.string(from: datePicker.date)
        print("Date Selected \(dateString1)")
        labelDate.text = dateString1
        
        let dateformatter2 = DateFormatter()
        dateformatter2.dateFormat = "dd-MM-yyyy"
        let dateString2 = dateformatter2.string(from: datePicker.date)
        print("Date Selected \(dateString2)")*/
        
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "MM/dd/yyyy"//"dd/MM/yyyy"
        let dateString1 = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(dateString1)")
        
        if self.dateOfBirthTag == 1222
        {
            self.lblSelectDateForAdhaar.text = dateString1
            self.lblSelectDateForAdhaar.textColor = UIColor.black
            self.btnAdhaarDOB.isSelected = false
            let arr1 = dateString1.components(separatedBy: "/")
            self.strAdhaarBirthYear = arr1[2]
            self.strAdhaarDOB = dateString1
            
        }
        else if self.dateOfBirthTag == 1223
        {
            let dateformatter2 = DateFormatter()
            dateformatter2.dateFormat = "MM/dd/yyyy"//"dd-MM-yyyy"
            let dateString2 = dateformatter2.string(from: self.datePickerView.date)
            print("Date Selected For DL \(dateString2)")
            //self.lblDateOfBirthOfDL.text = dateString2
            //self.lblDateOfBirthOfDL.textColor = UIColor.black
        }
        self.showActionSheetForGender()
        
        /*let dateformatter4 = DateFormatter()
        dateformatter4.dateFormat = "dd MMMM yyyy hh:mm a"
        let dateString4 = dateformatter4.string(from: datePicker.date)
        print("Date Selected \(dateString4)")*/
    }
    
    func showActionSheetForDateTime()
    {
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "Please select the date", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        
        self.datePickerView.frame = CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 180)
        self.datePickerView.tag = 444
        self.datePickerView.maximumDate = Date()
        //pickerFrame.delegate = self
        alert.view.addSubview(self.datePickerView)
        self.datePickerView.datePickerMode = UIDatePicker.Mode.date
        self.datePickerView.isHidden = false
        self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
        
        let selectAction = UIAlertAction(title: "Select", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //Perform Action
        })
        alert.addAction(selectAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.50) //0.80
        alert.view.addConstraint(height);
        self.present(alert, animated: true, completion: nil)
        //self.parent!.present(alert, animated: true, completion: nil)
    }

    //MARK:- Action Sheet With Picker View
    
    func showActionSheetForGender()
    {
        let alertController = UIAlertController(title: "", message: "Select any one", preferredStyle: .actionSheet)
        
        let maleAction = UIAlertAction(title: "Male", style: .default, handler: { (action) -> Void in
            self.lblGender.text! = "Male"
            self.lblGender.textColor = UIColor.black
        })
        
        let  femaleAction = UIAlertAction(title: "Female", style: .default, handler: { (action) -> Void in
            self.lblGender.text! = "Female"
            self.lblGender.textColor = UIColor.black
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        alertController.addAction(maleAction)
        alertController.addAction(femaleAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - Get Employee Detail API
    
    func callApiToGetEmployeeDetail()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
        
        var dictLoginParam:[String:AnyObject]!
        //let objUserInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        dictLoginParam = ["clientid": strClientID] as [String : AnyObject] //objUserInfo.clientID!
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        APIFunctions.sharedInstance.getEmployeeProfileDetail(APIConstants.API_GetEmployeeDetail, dataDict: dictLoginParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion: { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                if error != nil
                {
                    
                }
                else
                {
                    let dict = response as! NSDictionary
                    let strStatus = dict.value(forKey: "status") as! Int
                    if strStatus == 1
                    {
                        let arr = response.value(forKey: kResponseInfo) as! NSArray
                        let dict = (arr[0] as AnyObject) as! NSDictionary
                        let userProfileDetail  = UserProfileInfoResponse.init(fromDictionary:(dict) as! [String : Any])
                        DefaultsValues.setCustomObjToUserDefaults(userProfileDetail, forKey: kUserProfileDetails)
                        
                        let objUserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
                        
                        self.setDocumentsDetails(objUserProfile: objUserProfileInfo)
                        //self.showUserProfileDetail()
                    }
                    else
                    {
                        /*let strMsg = dict.value(forKey: "message") as! String
                        if strMsg != "No Record !"
                        {
                            
                        }*/
                        //CommonMethodsMylesZero.showAlertController1(strTitle: "Error", strMsg: strMsg, strAction1: "OK", strAction2: "Cancel", viewController: self)
                    }
                }
            }
        })
    }
    
    func setDocumentsDetails(objUserProfile: UserProfileInfoResponse)
    {
        self.lblSelectDateForAdhaar.text = "Date Of Birth"
        self.lblSelectDateForAdhaar.textColor = Constants.GREY_COLOR_1
             
        self.txtAdhaarCardNumber.text = objUserProfile.aadharno //"999971658847"
        self.txtNameOnAdhaarCard.text = objUserProfile.aadharname
        //"Kumar Agarwal"
        /*if objUserProfile.aadharname != ""
        {
            self.txtNameOnAdhaarCard.text = objUserProfile.aadharname
        }
        else
        {
            self.txtNameOnAdhaarCard.text = UserDefaults.standard.value(forKey: KUSER_NAME) as? String
        }*/
        
        if objUserProfile.aadhardob != ""
        {
            let arr11 = objUserProfile.aadhardob?.components(separatedBy: " ")
            let arr22 = arr11?[0].components(separatedBy: "/")
            //self.strAdhaarBirthYear = arr1[1]
            
            self.strAdhaarBirthYear = arr22?[2]//objUserProfileInfo.aadhardob!//"1978"
            self.lblSelectDateForAdhaar.textColor = UIColor.black
            self.lblSelectDateForAdhaar.text = (arr11?[0] ?? "04/05/1978") as String//"04/05/1978"
            self.strAdhaarDOB = arr11?[0]
        }
        else
        {
            self.strAdhaarDOB = ""
            self.lblSelectDateForAdhaar.text = "Date Of Birth"
            self.lblSelectDateForAdhaar.textColor = Constants.GREY_COLOR_1
        }
        //self.lblAdhaarImageName.text = objUserProfileInfo.aadharfile!
        
        self.txtDrivingLicenceNo.text = objUserProfile.dlNo!//"MH-1220050000188"
        
        if objUserProfile.aadharfile != ""
        {
            let arr22 = objUserProfile.aadharfile.components(separatedBy: "/")
            self.lblAdhaarImageName.text = (arr22.last ?? "") as String //UserProfileInfo.aadharfile
            self.strAdhaarFileName = self.lblAdhaarImageName.text
        }
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)

        self.strAmazonBucketFilePath = String(format: "https://s3.ap-southeast-1.amazonaws.com/%@/%@/",Constants.AWS_S3_BUCKET_NAME, strClientID)
        if objUserProfile.dlfile != ""
        {
            let arr22 = objUserProfile.dlfile.components(separatedBy: "/")
            self.lblDLImageName.text = (arr22.last ?? "") as String //UserProfileInfo.aadharfile
            self.strDLFileName = self.lblDLImageName.text
        }
        if objUserProfile.panfile != ""
        {
            let arr22 = objUserProfile.panfile.components(separatedBy: "/")
            self.lblPanImageName.text = (arr22.last ?? "") as String //UserProfileInfo.aadharfile
            self.strPanFileName = self.lblPanImageName.text
        }
        
        //self.lblDLImageName.text = objUserProfileInfo.dlfile!
        
        self.lblGender.textColor = UIColor.black
        self.lblGender.text = objUserProfile.passportGender!//"Female"
        
        self.txtPanCardNo.text = objUserProfile.panNo!//"BYSPS8197M"
        //self.lblPanImageName.text = objUserProfileInfo.panfile!
        
        self.lblAdhaarImageName.textColor = UIColor.black
        self.lblDLImageName.textColor = UIColor.black
        self.lblPanImageName.textColor = UIColor.black
        
        self.showUserProfileDetail()
    }
    
    //MARK: - All Documents Verification API
    
    func checkValidityOfPassport()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let intNumber = CommonMethodsMylesZero.generateRandomDigits(9)
        let strTransID =  String(format: "Passport%d", intNumber)
        var dictParam:[String:AnyObject]!
        //
        dictParam = ["transID" : "123456",
                     "docType" : 3,
                     "docNumber" : "J8369854",
                     "surname" : "RAMADUGULA",
                     "firstName" : "SITA MAHA LAKSHMI",
                     "gender" : "F",
                     "countryCode" : "IND",
                     "dateOfBirth" : "23-09-1959",
                     "passportType" : "P",
                     "dateOfExpiry" : "10-10-2021",
                     "mrz1" : "P<INDRAMADUGULA<<SITA<<MAHA<<LAKSHMI<<<<<<<<<<",
                     "mrz2" : "J8369854<4IND5909234F2110101<<<<<<<<<<<<<<<8"] as [String : AnyObject] //2699122_73682_A
        
        self.dictPassportInfo = dictParam

        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        APIFunctions.sharedInstance.checkValidityOfAdhaarCard(APIConstants.API_passport_validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.checkValidityOfDrivingLicence()
                    //CommonFunctions.showAlertController2(strTitle: "", strMsg: response.value(forKey: "msg") as! String, strAction1: "OK", viewController: self)
                }
            }
        })
    }
    
    func checkValidityOfAdhaarCard()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        //let intNumber = CommonFunctions.generateRandomDigits(9)
        //let intAdhaarNo = Int(self.txtAdhaarCardNumber.text ?? "0")
        let strDoc = self.txtAdhaarCardNumber.text ?? "0"
        let strTransID =  String(format: "iOS%@adhaarcard", strDoc)
        //let strTransID =  String(format: "Adhaar%d", intNumber)
        var dictParam:[String:AnyObject]!

        dictParam = [ "transID": strTransID, //Adhar2364777piiip
                      "docType": 55, //"docNumber": self.txtAdhaarCardNumber.text!,
                      "name": self.txtNameOnAdhaarCard.text!,
                      "yob" : self.strAdhaarBirthYear ?? "0000", //"1981",
                      "aadhaar_url" : "https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/2700124/2700124_38704_A.pdf"] as [String : AnyObject] //2699122_73682_A  //kAWSLiveFolderName
        
        self.dictAdhaarInfo = dictParam
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.checkValidityOfAdhaarCard(APIConstants.API_adhaarCard_validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.checkValidityOfDrivingLicence()
                    //CommonFunctions.showAlertController2(strTitle: "", strMsg: response.value(forKey: "msg") as! String, strAction1: "OK", viewController: self)
                }
            }
        })
    }
    
    func checkValidityOfDrivingLicence()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let intNumber = CommonMethodsMylesZero.generateRandomDigits(9)
        let strTransID =  String(format: "dl%d", intNumber)
        var dictParam:[String:AnyObject]!
        dictParam = ["transID" : strTransID, //"dl2334555uu",
                     "docType" : 4,
                     "docNumber" : self.txtDrivingLicenceNo.text!, //"BYSPS8666666M",
                     "dob":"",//self.lblDateOfBirthOfDL.text!,//"25-12-1981",
                     "dl_photo":"0"] as [String : AnyObject]
        self.dictDrivingLicenceInfo = dictParam
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.checkValidityOfDrivingLicence(APIConstants.API_drivingLicence_Validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.checkValidityOfPanCard()
                    //CommonFunctions.showAlertController2(strTitle: "", strMsg: response.value(forKey: "msg") as! String, strAction1: "OK", viewController: self)
                }
            }
        })
    }
    
    func checkValidityOfPanCard()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let intNumber = CommonMethodsMylesZero.generateRandomDigits(9)
        let strTransID =  String(format: "pan%d", intNumber)
        var dictParam:[String:AnyObject]!
        dictParam = ["transID" : strTransID, //"pan2334555uu"
                     "docType" : 2,
                     "docNumber" : self.txtPanCardNo.text!] as [String : AnyObject]
        self.dictPanCardInfo = dictParam
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.checkValidityOfPanCard(APIConstants.API_pancard_validity, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.submitDocumentDetailWebService()
                    //let objPanCard  = PanCardDetailRootClass.init(fromDictionary:(response as! NSDictionary) as! [String : Any])
                    //DefaultsValues.setCustomObjToUserDefaults(objPanCard, forKey: kPanCardData)
                    
                }
            }
        })
    }
    
    func submitDocumentDetailWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let strAdhaarJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAdhaarCardJsonStr)
        //let strPassportJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPassportJsonStr)
        let strDLJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kDrivingLicenceJsonStr)
        let strPanCardJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPanCardJsonStr)
        
        /*let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
        if UserProfileInfo?.aadharfile != ""
        {
            let str = UserProfileInfo?.aadharfile as String
            let arrValue = str.components(separatedBy: "/")
            self.strAmazonBucketFilePath = String(format: "https://%@/%@/%@/", arrValue[2], arrValue[3], arrValue[4])
        }
        var strPassport : String = ""
        var strAdhaarDOB : String = ""
        
        if UserProfileInfo?.aadharfile != ""
        {
            let arr1 = UserProfileInfo?.aadharfile.components(separatedBy: "/")
            strAdhaarFileName = String(format: "%@", arr1.last!) //String(format: "%@", arr1[5])
        }
        if UserProfileInfo?.panfile != ""
        {
            let arr2 = UserProfileInfo?.panfile.components(separatedBy: "/")
            strPanFileName = String(format: "%@", arr2.last!) //String(format: "%@", arr2[5])
        }
        if UserProfileInfo?.dlfile != ""
        {
            let arr3 = UserProfileInfo?.dlfile.components(separatedBy: "/")
            strDLFileName = String(format: "%@", arr3.last!) //String(format: "%@", arr3[5])
        }
        if UserProfileInfo?.passportfile != ""
        {
            let arr4 = UserProfileInfo?.passportfile.components(separatedBy: "/")
            strPassport = String(format: "%@", arr4.last!) //String(format: "%@", arr4[5])
        }
        if UserProfileInfo?.aadhardob != ""
        {
            let arr11 = UserProfileInfo.aadhardob?.components(separatedBy: " ")
            strAdhaarDOB = (arr11?[0])! as String
        }*/
        //let strDState = (self.deliveryStateCityData[0].text ?? "") as String
        //let strDCity = (self.deliveryStateCityData[1].text ?? "") as String
        let strPState = (self.permanentStateCityData[0].text ?? "") as String
        let strPCity = (self.permanentStateCityData[1].text ?? "") as String
            
        var dictParam:[String:AnyObject]!
        
        dictParam = ["clientid":strClientID as AnyObject, //userDetails.clientID!
                     "aadharfile": strAdhaarFileName as AnyObject,
                     "aadharno":self.txtAdhaarCardNumber.text! as AnyObject,
                     "aadharname":self.txtNameOnAdhaarCard.text! as AnyObject,
                     "aadhardob":strAdhaarDOB as AnyObject,
                     "Passportfile":(strPassport ?? "") as AnyObject,
                     "Passportno":"",
                     "Passportsurname":"",
                     "Passportfirstname":"",
                     "PassportGender":"Male",
                     "Passportcountryco":"",
                     "Passporttype":"",
                     "passportexpirydate":"07/12/2020",
                     "PassportMRZ1":"",
                     "PassportMRZ2":"",
                     "PassportDOb":"06/12/1999",
                     "dlfile":strDLFileName as AnyObject,
                     "dlno":self.txtDrivingLicenceNo.text! as AnyObject,
                     "panfile":strPanFileName as AnyObject,
                     "panno":self.txtPanCardNo.text! as AnyObject,
                     "doctype":self.strTypeOfDocument as AnyObject,
                     "filepath":self.strAmazonBucketFilePath ?? "",
                     "Delieveryaddress":"",
                     "PermanantAddress":"",
                     "deliverydate":"",//self.lblDrivingLicenceDOB.text! as AnyObject,
                     "aadharverify":"0",
                     "Passportverify":"0",
                     "Panverify":"0",
                     "dlverify":"0",
                     "jsonaadharverifydata":strAdhaarJson ?? "",
                     "jsonpassportverifydata":"",
                     "jsonPanverifydata":strPanCardJson ?? "",
                     "jsondlverifydata":strDLJson ?? "",
                     "address1permanent":self.txtPermanentAdd1.text as AnyObject,
                     "address2permanent":self.txtPermanentAdd2.text as AnyObject,
                     "state":strPState, //self.lblPermanentState.text as AnyObject, //UserProfileInfo.state
                     "city":strPCity, //self.lblPermanentCity.text as AnyObject,
                     "pincode":self.txtPermanentAdd3.text as AnyObject,
                     "address1delivery":self.strDeliveryAdd1 as AnyObject, //self.txtDeliveryAdd1.text as AnyObject,
                     "address2delivery":self.strDeliveryAdd2 as AnyObject,
                     "statedelivery":self.strDeliveryState as AnyObject,
                     "citydelivery":self.strDeliveryCity as AnyObject,
                     "pincodedelivery":self.strDeliveryAdd3 as AnyObject,
                     "sameaddress":self.strSameAddress as AnyObject] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.uploadDocumentAuthBridgeExperian(APIConstants.API_UploadDocumentAuthBridgeExperian, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("response: %@", response)
                    let responseKey = response.value(forKey:"response") as? AnyObject
                    if responseKey is NSNull //((response.value(forKey:"response") as? NSNull) == nil)
                    {
                        let status = response.value(forKey:"status") as? Int
                        if status == 1
                        {
                            let objDocumentsDetailVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_DocumentsDetail) as! DocumentsDetailVC
                            objDocumentsDetailVC.strAdhaarFile = self.lblAdhaarImageName.text ?? ""
                            objDocumentsDetailVC.strDLFile = self.lblDLImageName.text ?? ""
                            objDocumentsDetailVC.strPanFile = self.lblPanImageName.text ?? ""
                            objDocumentsDetailVC.strAmazonBucketFilePath = self.strAmazonBucketFilePath ?? ""
                            objDocumentsDetailVC.strDocType = self.strTypeOfDocument
                            
                            self.navigationController?.pushViewController(objDocumentsDetailVC, animated: true)
                             //self.viewTransparent.removeFromSuperview()
                                                   //self.viewCreditScore.removeFromSuperview()
                                                   /*let strMessage = response.value(forKey:"message") as! String
                                                   CommonFunctions.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)*/ //Swati Commented uncommente after demo and delete below line
                                                   //self.createNewEmployeeSubscription()
                        }
                        else if status == 0
                        {
                            //let strMessage = response.value(forKey:"message") as! String
                            //CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                        }
                    }
                    else
                    {
                        /*if self.isUpgradeSelected == true
                        {
                            self.createUpgradeSubscription()
                        }
                        else
                        {
                            self.createNewEmployeeSubscription()
                        }*/
                    }
                }
            }
        })
    }
    
    // MARK: - Button Action
    
    @IBAction func btnSelectDateForDL_Click(_ sender: UIButton)
    {
        self.dateOfBirthTag = sender.tag
        self.showActionSheetForDateTime()
        /*sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.btnDLBirthDate.isSelected = true
        }
        else
        {
            self.btnAdhaarDOB.isSelected = false
        }
        self.showActionSheetForDateTime()*/
    }
    
    @IBAction func btnSelectGender_Click(_ sender: Any)
    {
        self.showActionSheetForGender()
    }
    
    @IBAction func btnSelectDate_Click(_ sender: UIButton)
    {
        self.dateOfBirthTag = sender.tag
        self.showActionSheetForDateTime()
        /*sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.btnAdhaarDOB.isSelected = true
        }
        else
        {
            self.btnDLBirthDate.isSelected = false
        }
        self.showActionSheetForDateTime()*/
    }
    
    @IBAction func btnBack_Click(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
     @IBAction func btnAdhaarCard_Click(_ sender: UIButton)
     {
        isAdhaarSelected = true
        strTypeOfDocument = "Adhaar"
        self.lblAdhaarImageName.isHidden = false
        self.imgAdhaarIcon.isHidden = true
        btnAdhaarCard.setImage(UIImage(named:"select_radio"), for: UIControl.State())
        btnPassport.setImage(UIImage(named:"unselect_radio"), for: UIControl.State())
        if self.viewPassportView.superview != nil
        {
           self.viewPassportView.removeFromSuperview()
        }
        else
        {
            guard Bundle.main.path(forResource: "PassportView", ofType: "nib") != nil else {
                return
            }
        }
       
     }
    
    @IBAction func btnPassport_Click(_ sender: UIButton)
    {
        isAdhaarSelected = false
        strTypeOfDocument = "Passport"
        btnPassport.setImage(UIImage(named:"select_radio"), for: UIControl.State())
        btnAdhaarCard.setImage(UIImage(named:"unselect_radio"), for: UIControl.State())
        self.lblAdhaarImageName.isHidden = true
        self.imgAdhaarIcon.isHidden = true
        self.viewPassportView = Bundle.main.loadNibNamed("PassportView", owner: nil, options: nil)![0] as! PassportView
        self.viewAdhaarPassportContent.addSubview(self.viewPassportView)
        self.viewPassportView.frame = CGRect(x: 0, y: 0, width: self.viewAdhaarPassportContent.frame.size.width, height: self.viewAdhaarPassportContent.frame.size.height)
        
        /*self.viewPassportView.layer.cornerRadius = 4.0
        self.viewPassportView.btnClosePassword.layer.cornerRadius = self.viewEnterPassword.btnClosePassword.frame.size.height / 2
        self.viewPassportView.btnClosePassword.layer.borderWidth = 1.0
        self.viewPassportView.btnContinue.addTarget(self, action:#selector(self.btnContinueToRegister_Click(sender:)), for: .touchUpInside)*/
    }
    
    @IBAction func btnAdhaarPassport_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        self.strTypeOfDocument = sender.currentTitle
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openImagePickerAlert()
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        /*switch UIDevice.current.userInterfaceIdiom {
         case .pad:
         alert.popoverPresentationController?.sourceView = sender
         alert.popoverPresentationController?.sourceRect = sender.bounds
         alert.popoverPresentationController?.permittedArrowDirections = .up
         default:
         break
         }*/
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Button Actions
    
    //MARK: - Button Actions
    
    
    @IBAction func btnViewAdhaarFile_Click(_ sender: Any)
    {
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = self.strAdhaarUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
    }
    
    @IBAction func btnViewDLFile_Click(_ sender: Any)
    {
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = self.strDLUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
    }
    
    @IBAction func btnViewPancardFile_Click(_ sender: Any)
    {
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = self.strPancardUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
    }
    
    @IBAction func btnPermanentState_Click(_ sender: UIButton)
    {
        self.stateTag = sender.tag
        let objSearchCityStateVC = Constants.userProfileStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCityState) as! SearchCityStateVC
        objSearchCityStateVC.selectDataDelegate = self
        self.present(objSearchCityStateVC, animated: true, completion: nil)
    }
    
    @IBAction func btnPermanentCity_Click(_ sender: UIButton)
    {
        self.cityTag = sender.tag
        let objSearchCityVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchTableVC) as! SearchCityVC
        objSearchCityVC.selectCityNameDelegate = self
        objSearchCityVC.modalPresentationStyle = .fullScreen
        self.present(objSearchCityVC, animated: true, completion: nil)
    }
    
    @IBAction func btnDeliveryState_Click(_ sender: UIButton)
    {
        self.stateTag = sender.tag
        let objSearchCityStateVC = Constants.userProfileStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCityState) as! SearchCityStateVC
        objSearchCityStateVC.selectDataDelegate = self
        objSearchCityStateVC.modalPresentationStyle = .fullScreen
        self.present(objSearchCityStateVC, animated: true, completion: nil)
    }
    
    @IBAction func btnDeliveryCity_Click(_ sender: UIButton)
    {
        self.cityTag = sender.tag
        let objSearchCityVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchTableVC) as! SearchCityVC
        objSearchCityVC.selectCityNameDelegate = self
        objSearchCityVC.modalPresentationStyle = .fullScreen
        self.present(objSearchCityVC, animated: true, completion: nil)
    }
    
    @IBAction func btnSameAddress_Click(_ sender: UIButton)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "unselected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = false
            self.strSameAddress = "0"
            self.viewDelivery.isHidden = false
            self.deliveryAddHeightConstraint.constant = 350
            self.strDeliveryAdd1 = self.txtDeliveryAdd1.text
            self.strDeliveryAdd2 = self.txtDeliveryAdd2.text
            self.strDeliveryAdd3 = self.txtDeliveryAdd3.text
            self.strDeliveryState = (self.deliveryStateCityData[0].text ?? "") as String
            self.strDeliveryCity = (self.deliveryStateCityData[1].text ?? "") as String
        }
        else
        {
            sender.setImage(UIImage(named: "selected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = true
            self.strSameAddress = "1"
            self.viewDelivery.isHidden = true
            self.deliveryAddHeightConstraint.constant = 100
            self.strDeliveryAdd1 = self.txtPermanentAdd1.text
            self.strDeliveryAdd2 = self.txtPermanentAdd2.text
            self.strDeliveryAdd3 = self.txtPermanentAdd3.text
            self.strDeliveryState = (self.permanentStateCityData[0].text ?? "") as String
            self.strDeliveryCity = (self.permanentStateCityData[1].text ?? "") as String
        }
    }
    
    @IBAction func btnUploadAdhaar_Click(_ sender: UIButton)
    {
        self.uploadButtonTag = sender.tag
        self.openImagePickerAlert()
    }
    
    @IBAction func btnUploadDrivingLicence_Click(_ sender: UIButton)
    {
        self.uploadButtonTag = sender.tag
        self.openImagePickerAlert()
    }
    
    @IBAction func btnUploadPanCard_Click(_ sender: UIButton)
    {
        self.uploadButtonTag = sender.tag
        self.openImagePickerAlert()
    }
    
     @IBAction func btnVerifyDocuments_Click(_ sender: Any)
     {
        if self.txtAdhaarCardNumber.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the aadhaar number", strAction1: "OK", viewController: self)
        }
        else if self.txtNameOnAdhaarCard.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the aadhaar name", strAction1: "OK", viewController: self)
        }
        else if self.txtDrivingLicenceNo.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter driving licence number", strAction1: "OK", viewController: self)
        }
        else if self.txtPanCardNo.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the pan card number", strAction1: "OK", viewController: self)
        }
        else if self.txtPermanentAdd1.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the permanent address", strAction1: "OK", viewController: self)
        }
        else if self.lblPermanentState.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select state", strAction1: "OK", viewController: self)
        }
        else if self.lblPermanentCity.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select city", strAction1: "OK", viewController: self)
        }
        else
        {
            self.checkValidityOfAdhaarCard()
        }
        /*if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) != nil
        {
            let objDocumentsDetailVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_DocumentsDetail) as! DocumentsDetailVC
            objDocumentsDetailVC.strAdhaarFile = self.lblAdhaarImageName.text ?? ""
            objDocumentsDetailVC.strDLFile = self.lblDLImageName.text ?? ""
            objDocumentsDetailVC.strPanFile = self.lblPanImageName.text ?? ""
            objDocumentsDetailVC.strAmazonBucketFilePath = self.strAmazonBucketPath ?? ""
            if self.isUpgradeSelected == true
            {
                objDocumentsDetailVC.objSubscriptionDetail = self.objSubscriptionDetail
            }
            self.navigationController?.pushViewController(objDocumentsDetailVC, animated: true)
        }
        else
        {
            self.checkValidityOfAdhaarCard()
        }*/
                
        /*print("Verify Documents Clicked")
        let objDocumentsEditVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_DocumentsEdit) as! DocumentsEditVC
        self.navigationController?.pushViewController(objDocumentsEditVC, animated: true)*/
     }
    
    /*func uploadImageOnAWS()
     {
     if imageView?.image == nil {
     // Do something to wake up user :)
     } else {
     let transferUtility = AWSS3TransferUtility.default()
     let expression = AWSS3TransferUtilityUploadExpression()
     //expression.progressBlock = progressBlock
     
     transferUtility.uploadData(<#T##data: Data##Data#>, bucket: <#T##String#>, key: <#T##String#>, contentType: <#T##String#>, expression: <#T##AWSS3TransferUtilityUploadExpression?#>, completionHandler: <#T##AWSS3TransferUtilityUploadCompletionHandlerBlock?##AWSS3TransferUtilityUploadCompletionHandlerBlock?##(AWSS3TransferUtilityUploadTask, Error?) -> Void#>)
     transferUtility.uploadData(
     data,
     bucket: S3BucketName,
     key: S3UploadKeyName,
     contentType: "image/png",
     expression: expression,
     completionHandler: completionHandler).continueWith { (task) -> AnyObject! in
     if let error = task.error {
     print("Error: \(error.localizedDescription)")
     self.statusLabel.text = "Failed"
     }
     
     if let _ = task.result {
     self.statusLabel.text = "Generating Upload File"
     print("Upload Starting!")
     // Do something with uploadTask.
     }
     
     return nil;
     }
     }
     }*/
}

extension Data {
    
    var format: String {
        let array = [UInt8](self)
        let ext: String
        switch (array[0]) {
        case 0xFF:
            ext = "jpg"
        case 0x89:
            ext = "png"
        case 0x47:
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = "unknown"
        }
        return ext
    }
    
}

extension UIImage {
    
    func resizedImage(newSize: CGSize) -> UIImage {
        guard self.size != newSize else { return self }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}
