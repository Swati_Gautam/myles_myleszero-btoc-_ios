//
//  DocumentsDetailVC.swift
//  Myles Zero
//
//  Created by skpissay on 26/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import Alamofire

class DocumentsDetailVC: UIViewController
{
    @IBOutlet weak var btnTandC: UIButton!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    @IBOutlet weak var btnGetCreditScore: UIButton!
    @IBOutlet weak var lblAdhaarCardNo: UILabel!
    @IBOutlet weak var lblAdhaarCardName: UILabel!
    @IBOutlet weak var lblAdhaarCardDOB: UILabel!
    @IBOutlet weak var imgViewAdhaarCard: UIImageView!
    
    @IBOutlet weak var lblDLGender: UILabel!
    @IBOutlet weak var lblDrivingLicenceNo: UILabel!
    //@IBOutlet weak var lblDrivingLicenceDOB: UILabel!
    @IBOutlet weak var imgViewDL: UIImageView!
    
    @IBOutlet weak var lblPanCardNo: UILabel!
    @IBOutlet weak var imgViewPanCard: UIImageView!
    
    @IBOutlet weak var btnAdhaarCardVerify: UIButton!
    @IBOutlet weak var btnDLVerify: UIButton!
    @IBOutlet weak var btnPanCardVerify: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    var dictAdhaarInfo : [String:AnyObject]?
    var dictDrivingLicenceInfo : [String:AnyObject]?
    var dictPanCardInfo : [String:AnyObject]?
    var dictPassportInfo : [String:AnyObject]?
    var strAdhaarDOB : String?
    
    var viewCreditScore = CreditScoreView()
    var viewTransparent : UIView!
    
    var strAdhaarFile : String?
    var strDLFile : String?
    var strPanFile : String?
    var strAmazonBucketFilePath : String?
    var strCoricID : String?
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var isUpgradeSelected : Bool = false
    var strDateForUpgradation : String?
    var dictUpgradeData : NSDictionary?
    var strDocType : String?
    var arrStateList = NSArray()
    var strStateCode : String?
    var strCreditScore : String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.scrollView.canCancelContentTouches = true
        self.scrollView.delaysContentTouches = true
        self.callApiToGetEmployeeDetail()
        
        /*if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse != nil
        {
            let objUserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
            
            self.lblAdhaarCardNo.text! = objUserProfileInfo.aadharno! //self.dictAdhaarInfo?["docNumber"] as! String
            if objUserProfileInfo.aadhardob! != ""
            {
                let arrAdhaarDate = objUserProfileInfo.aadhardob!.components(separatedBy: " ")
                let strAdhaarDate = arrAdhaarDate[0] as String
                self.lblAdhaarCardDOB.text! = strAdhaarDate //objUserProfileInfo.aadhardob!//self.strAdhaarDOB ?? "04/05/1978"
            }
            else
            {
                
            }
            
            self.lblAdhaarCardName.text! = objUserProfileInfo.aadharname! //self.dictAdhaarInfo?["name"] as! String
            //
            /*if objUserProfileInfo.dlexpirydate! != ""
            {
                let arrDlDate = objUserProfileInfo.dlexpirydate!.components(separatedBy: " ")
                let strDlDate = arrDlDate[0] as String
                self.lblDrivingLicenceDOB.text! = strDlDate //objUserProfileInfo.deliverydate! //self.dictDrivingLicenceInfo?["docNumber"] as! String
                 //self.dictDrivingLicenceInfo?["dob"] as! String
            }
            else
            {
                
            }*/
            self.lblDrivingLicenceNo.text! = objUserProfileInfo.dlNo!
            self.lblDLGender.text! = objUserProfileInfo.passportGender!
           
            self.lblPanCardNo.text! = objUserProfileInfo.panNo! //self.dictPanCardInfo?["docNumber"] as! String
            self.btnGetCreditScore.isUserInteractionEnabled = false
            self.btnGetCreditScore.isEnabled = false
        }*/
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Get Employee Detail API
    
    func callApiToGetEmployeeDetail()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
        
        var dictLoginParam:[String:AnyObject]!
        //let objUserInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        dictLoginParam = ["clientid": strClientID] as [String : AnyObject] //objUserInfo.clientID!
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        APIFunctions.sharedInstance.getEmployeeProfileDetail(APIConstants.API_GetEmployeeDetail, dataDict: dictLoginParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion: { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                if error != nil
                {
                    
                }
                else
                {
                    let dict = response as! NSDictionary
                    let strStatus = dict.value(forKey: "status") as! Int
                    if strStatus == 1
                    {
                        let arr = response.value(forKey: kResponseInfo) as! NSArray
                        let dict = (arr[0] as AnyObject) as! NSDictionary
                        let userProfileDetail  = UserProfileInfoResponse.init(fromDictionary:(dict) as! [String : Any])
                        DefaultsValues.setCustomObjToUserDefaults(userProfileDetail, forKey: kUserProfileDetails)
                        
                        let objUserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
                        
                        self.setDocumentsDetails(objUserProfile: objUserProfileInfo)
                        //self.showUserProfileDetail()
                    }
                    else
                    {
                        /*let strMsg = dict.value(forKey: "message") as! String
                        if strMsg != "No Record !"
                        {
                            
                        }*/
                        //CommonMethodsMylesZero.showAlertController1(strTitle: "Error", strMsg: strMsg, strAction1: "OK", strAction2: "Cancel", viewController: self)
                    }
                }
            }
        })
    }
    
    func setDocumentsDetails(objUserProfile: UserProfileInfoResponse)
    {
        //self.lblSelectDateForAdhaar.text = "Date Of Birth"
        //self.lblSelectDateForAdhaar.textColor = Constants.GREY_COLOR_1
             
        self.lblAdhaarCardNo.text = objUserProfile.aadharno //"999971658847"
        self.lblAdhaarCardName.text = objUserProfile.aadharname //"Kumar Agarwal"
        
        if objUserProfile.aadhardob != ""
        {
            let arr11 = objUserProfile.aadhardob?.components(separatedBy: " ")
            let arr22 = arr11?[0].components(separatedBy: "/")
            //self.strAdhaarBirthYear = arr1[1]
            
            //self.strAdhaarBirthYear = arr22?[2]//objUserProfileInfo.aadhardob!//"1978"
            self.lblAdhaarCardDOB.textColor = UIColor.black
            self.lblAdhaarCardDOB.text = (arr11?[0] ?? "04/05/1978") as String//"04/05/1978"
            self.strAdhaarDOB = arr11?[0]
        }
        else
        {
            self.strAdhaarDOB = ""
            self.lblAdhaarCardDOB.text = "Date Of Birth"
            self.lblAdhaarCardDOB.textColor = Constants.GREY_COLOR_1
        }
        //self.lblAdhaarImageName.text = objUserProfileInfo.aadharfile!
        
        self.lblDrivingLicenceNo.text = objUserProfile.dlNo!//"MH-1220050000188"
        
        self.lblDLGender.textColor = UIColor.black
        self.lblDLGender.text = objUserProfile.passportGender!//"Female"
        
        self.lblPanCardNo.text = objUserProfile.panNo!//"BYSPS8197M"
        
        self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
        let arr1 = arrStateList
        for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
            if (strStateName.lowercased()).contains(objUserProfile.state.lowercased())
            {
                self.strStateCode = String(strStateCode)
                //self.deliveryStateCityData[0].text = strStateName.uppercased()
            }
            if (strStateCode.lowercased()).contains(objUserProfile.state.lowercased())
            {
                self.strStateCode = String(strStateCode)
                //self.deliveryStateCityData[0].text = strStateName.uppercased()
            }
        }
        
        self.imgViewAdhaarCard.sd_setImage(with: URL(string: (objUserProfile.aadharfile!)), placeholderImage: UIImage(named: kCarPlaceholder))
        self.imgViewDL.sd_setImage(with: URL(string: (objUserProfile.dlfile!)), placeholderImage: UIImage(named: kCarPlaceholder))
        self.imgViewPanCard.sd_setImage(with: URL(string: (objUserProfile.panfile!)), placeholderImage: UIImage(named: kCarPlaceholder))
    }
    
    func getCreditScore()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        var dictParam:[String:AnyObject]!
        
        let objUserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
        
        //let objUserInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strUserName = UserDefaults.standard.value(forKey: KUSER_NAME) as? String
        var strFName : String = ""
        var strLName : String = ""
        var strNewCode : String = ""
        var strDOB : String = ""
        
        //MM/dd/yyyy
        
        if strUserName != ""
        {
            let arrValue = strUserName?.components(separatedBy: " ")
            strFName = (arrValue![0]) as String
            strLName = (arrValue?.last ?? "") as String
        }
        self.arrStateList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kStateListArray)!
        let arr1 = arrStateList
        for i in 0..<arr1.count
        {
            let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
            let strStateCode = (arr1[i] as AnyObject).value(forKey: "StateCode") as! String
            if (strStateName.lowercased()).contains(objUserProfileInfo.state.lowercased())
            {
                strNewCode = (arr1[i] as AnyObject).value(forKey: "StateName") as! String//objUserProfileInfo.state//String(strStateCode)
            }
            if (strStateCode.lowercased()).contains(objUserProfileInfo.state.lowercased())
            {
                strNewCode = (arr1[i] as AnyObject).value(forKey: "StateName") as! String//objUserProfileInfo.state
            }
        }
        if objUserProfileInfo.aadhardob != nil
        {
            let arr11 = objUserProfileInfo.aadhardob?.components(separatedBy: " ")
            let dob = (arr11?[0])! as String
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "mm/dd/yyyy"
            let convertedDate = dateFormatter.date(from: dob)
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            let date = dateFormatter.string(from: convertedDate!)
            print("date Credit: ", date)
            //dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
            //let strdate1 = dateFormatter.string(from: date as Date)
            //strDOB = strdate1
            //strDOB
        }
        
        dictParam = ["fname": strFName as AnyObject, //objUserInfo.fname,
         "sername":strLName as AnyObject, //objUserInfo.lname,
         "dob":"", //objUserInfo.dOB
         "gender":objUserProfileInfo.passportGender, //"1"
         "phone":objUserProfileInfo.phone1,
         "email":objUserProfileInfo.emailID, //"swati.gautam@carzonrent.com",
         "address":objUserProfileInfo.permanantAddress,
         "city":objUserProfileInfo.city,
         "state":strNewCode,//objUserProfileInfo.state,
         "pincode":objUserProfileInfo.pincode,
         "panno":self.lblPanCardNo.text!] as [String : AnyObject]
        
        /*dictParam = ["fname":"MUKESH",
                     "sername":"SHARMA",
                     "dob":"20041980",
                     "gender":"1",
                     "phone":"9833881234",
                     "email":"",
                     "address":"Devyani",
                     "city":"Mumbai",
                     "state":"Maharashtra",
                     "pincode":"422011",
                     "panno":"BUQPT2553F"] as [String : AnyObject]*/
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.getCreditScoreAPI(APIConstants.API_creditscore, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    let strMessage  = response.value(forKey: "msg") as? String
                    //let strStatus  = response.value(forKey: "status") as? String
                    let strScore  = response.value(forKey: "score") as? String
                    self.strCreditScore = strScore
                    self.loadCreditScoreView(strMsg: (strMessage ?? ""), strScore: (strScore ?? ""))
                    
                    /*let objDocumentsDetailVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_DocumentsDetail) as! DocumentsDetailVC
                    self.navigationController?.pushViewController(objDocumentsDetailVC, animated: true)*/
                }
            }
        })
    }
    
    func loadCreditScoreView(strMsg : String, strScore : String)
    {
        self.viewTransparent = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.viewTransparent.backgroundColor = UIColor.black
        self.viewTransparent.alpha = 0.6

        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        self.viewTransparent.addGestureRecognizer(tap)
        self.view.addSubview(self.viewTransparent)
        
        self.viewCreditScore = Bundle.main.loadNibNamed("CreditScoreView", owner: nil, options: nil)![0] as! CreditScoreView
        self.view.addSubview(self.viewCreditScore)
        //self.viewCreditScore.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 50, height: 235)
        //self.viewCreditScore.center = self.view.center
        
        self.viewCreditScore.center = CGPoint(x: self.view.frame.size.width  / 2,
                                      y: self.view.frame.size.height / 2)
        self.viewCreditScore.layer.cornerRadius = 4.0
        self.viewCreditScore.lblCreditScoreMsg.text = strMsg//"Congratulations! Your Experian credit score is" //strMsg
        if strScore == ""
        {
            self.viewCreditScore.lblCreditScoreNo.text = strScore//"744"
        }
        else
        {
            self.viewCreditScore.lblCreditScoreNo.text = strScore
        }
        
        self.viewCreditScore.btnSubmitRequest.addTarget(self, action:#selector(self.btnSubmitRequest_Click(sender:)), for: .touchUpInside)
        
        
        /*self.viewCreditScore = Bundle.main.loadNibNamed("CreditScoreView", owner: nil, options: nil)![0] as! CreditScoreView
        self.view.addSubview(self.viewCreditScore)
        self.viewCreditScore.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 32, height: 235)
        self.viewCreditScore.center = self.view.center*/
    }
    
    //MARK: - Button Actions
    @IBAction func btnBack_Click(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCheckbox_Click(_ sender: UIButton)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "unselected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = false
            self.btnGetCreditScore.isUserInteractionEnabled = false
            self.btnGetCreditScore.isEnabled = false
        }
        else
        {
            sender.setImage(UIImage(named: "selected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = true;
            self.btnGetCreditScore.isUserInteractionEnabled = true
            self.btnGetCreditScore.isEnabled = true
        }
    }
    
    @IBAction func btnTAndC_Click(_ sender: Any)
    {
        let objMylesZeroTermsVC = Constants.userProfileStoryboard.instantiateViewController(withIdentifier: Constants.VC_MylesZeroTermsVC) as! MylesZeroTermsVC
        self.navigationController?.pushViewController(objMylesZeroTermsVC, animated: true)
        
    }
    
    @IBAction func btnCreditScore_Click(_ sender: Any)
    {
        self.getCreditScore()
        
    }
    
    @objc func btnSubmitRequest_Click(sender:UIButton!)
    {
        self.reCheckCarAvailability()
        //if self.strCreditScore != ""
        //{
            //self.reCheckCarAvailability()
        //}
        //let objSubscriptionRequestVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionRequest) as! SubscriptionRequestVC
        //self.navigationController?.pushViewController(objSubscriptionRequestVC, animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        self.viewTransparent.removeFromSuperview()
        self.viewCreditScore.removeFromSuperview()
    }
    //MARK: - Upgrade Subscription
    
    func createUpgradeSubscription()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        //let strAdhaarJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAdhaarCardJsonStr)
        //let strPassportJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPassportJsonStr)
        //let strDLJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kDrivingLicenceJsonStr)
        //let strPanCardJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPanCardJsonStr)
        
        var dictParam:[String:AnyObject]!
        
        let objCarList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as! SearchCarList
        let strPreClosure = self.dictUpgradeData?.value(forKey: "TotalPreCloserCharges") as! String 
        //let strPreClosure = (self.dictUpgradeData?.value(forKey: "TotalPreCloserCharges")) as String
        //objSubmitDocumentsVC.dictUpgradeData = self.dictUpgradeData
        //objSubmitDocumentsVC.strDateForUpgradation = self.strDateForUpgradation
        
       // Employee_CreateSubscription_Upgrade
        dictParam = ["clientid" : strClientID as AnyObject, //(userDetails.clientID ?? "")
                     "amt" : (objCarList.priceStr ?? "") as AnyObject,
                     "cityid" : objCarList.cityID as AnyObject,
                     "variantid" : (objCarList.carVariantID ?? "") as AnyObject,
                     "aadharno" : (UserProfileInfo.aadharno ?? "") as AnyObject,
                     "aadharfile" : (UserProfileInfo.aadharfile ?? "") as AnyObject,
                     "dlno" : (UserProfileInfo.dlNo ?? "") as AnyObject,
                     "dlfile" : (UserProfileInfo.dlfile ?? "") as AnyObject,
                     "panno" : (UserProfileInfo.panNo ?? "") as AnyObject,
                     "panfile" : (UserProfileInfo.panfile ?? "") as AnyObject,
                     "filepath" : (self.strAmazonBucketFilePath ?? "") as AnyObject,
                     "pkgid" : (objCarList.pkgID ?? 0) as AnyObject,
                     "flag" : "" as AnyObject,
                     "Tenure" : (objCarList.carVariantID ?? "") as AnyObject,
                     "DLExpiryDate" : (UserProfileInfo.aadharno ?? "") as AnyObject,
                     "PassportExpiryDate" : (UserProfileInfo.aadharno ?? "") as AnyObject,
                     "CarColor" : (objSubscriptionDetail?.carColor ?? "") as AnyObject,
                     "CoricId" : (objSubscriptionDetail?.coricId ?? "") as AnyObject,
                     "doctype" : (self.strDocType ?? "") as AnyObject,
                     "address" : (UserProfileInfo.address ?? "") as AnyObject,
                     "deliverydate" : (UserProfileInfo.deliverydate ?? "") as AnyObject,
                     "OldSubId" : (objSubscriptionDetail?.subscriptionBookingid ?? "") as AnyObject,
                     "NewPkgID" : self.dictUpgradeData?.value(forKey: "NewPkgID") as? String?,
                     "TotalPreClosureAmt" : strPreClosure,
                     "PreferredTerminationDate":(self.strDateForUpgradation ?? "") as AnyObject,
                     "Remark":"" as AnyObject] as [String : AnyObject]
                
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.upgradeSubscriptionAPI(APIConstants.API_CreateUpgradeSubscription, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("response: %@", response)
                    let responseKey = response.value(forKey:"response") as? AnyObject
                    if responseKey is NSNull //((response.value(forKey:"response") as? NSNull) == nil)
                    {
                        self.viewTransparent.removeFromSuperview()
                        self.viewCreditScore.removeFromSuperview()
                        let strMessage = response.value(forKey:"message") as! String
                        CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                    }
                    else
                    {
                        //self.createNewEmployeeSubscription()
                    }
                }
            }
        })
    }
    
    func submitDocumentDetailWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let strAdhaarJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAdhaarCardJsonStr)
        //let strPassportJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPassportJsonStr)
        let strDLJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kDrivingLicenceJsonStr)
        let strPanCardJson = DefaultsValues.getStringValueFromUserDefaults_(forKey: kPanCardJsonStr)
        
        if UserProfileInfo.aadharfile != ""
        {
            let str = UserProfileInfo.aadharfile as String
            let arrValue = str.components(separatedBy: "/")
            self.strAmazonBucketFilePath = String(format: "https://%@/%@/%@/", arrValue[2], arrValue[3], arrValue[4])
        }
        var strPassport : String = ""
        var strAdhaarDOB : String = ""
        
        if UserProfileInfo.aadharfile != ""
        {
            let arr1 = UserProfileInfo.aadharfile.components(separatedBy: "/")
            strAdhaarFile = String(format: "%@", arr1.last!) //String(format: "%@", arr1[5])
        }
        if UserProfileInfo.panfile != ""
        {
            let arr2 = UserProfileInfo.panfile.components(separatedBy: "/")
            strPanFile = String(format: "%@", arr2.last!) //String(format: "%@", arr2[5])
        }
        if UserProfileInfo.dlfile != ""
        {
            let arr3 = UserProfileInfo.dlfile.components(separatedBy: "/")
            strDLFile = String(format: "%@", arr3.last!) //String(format: "%@", arr3[5])
        }
        if UserProfileInfo.passportfile != ""
        {
            let arr4 = UserProfileInfo.passportfile.components(separatedBy: "/")
            strPassport = String(format: "%@", arr4.last!) //String(format: "%@", arr4[5])
        }
        if UserProfileInfo.aadhardob != ""
        {
            let arr11 = UserProfileInfo.aadhardob?.components(separatedBy: " ")
            strAdhaarDOB = (arr11?[0])! as String
        }
            
        var dictParam:[String:AnyObject]!
        
        dictParam = ["clientid":strClientID as AnyObject, //userDetails.clientID!
                     "aadharfile": strAdhaarFile as AnyObject,
                     "aadharno":self.lblAdhaarCardNo.text! as AnyObject,
                     "aadharname":self.lblAdhaarCardName.text! as AnyObject,
                     "aadhardob":strAdhaarDOB as AnyObject,
                     "Passportfile":strPassport,
                     "Passportno":"",
                     "Passportsurname":"",
                     "Passportfirstname":"",
                     "PassportGender":"Male",
                     "Passportcountryco":"",
                     "Passporttype":"",
                     "passportexpirydate":"07/12/2020",
                     "PassportMRZ1":"",
                     "PassportMRZ2":"",
                     "PassportDOb":"06/12/1999",
                     "dlfile":strDLFile as AnyObject,
                     "dlno":self.lblDrivingLicenceNo.text! as AnyObject,
                     "panfile":strPanFile as AnyObject,
                     "panno":self.lblPanCardNo.text! as AnyObject,
                     "doctype":self.strDocType as AnyObject,
                     "filepath":self.strAmazonBucketFilePath ?? "",
                     "Delieveryaddress":"",
                     "PermanantAddress":"",
                     "deliverydate":"",//self.lblDrivingLicenceDOB.text! as AnyObject,
                     "aadharverify":"0",
                     "Passportverify":"0",
                     "Panverify":"0",
                     "dlverify":"0",
                     "jsonaadharverifydata":strAdhaarJson ?? "",
                     "jsonpassportverifydata":"",
                     "jsonPanverifydata":strPanCardJson ?? "",
                     "jsondlverifydata":strDLJson ?? "",
                     "address1permanent":UserProfileInfo.address1permanent as AnyObject,
                     "address2permanent":UserProfileInfo.address2permanent as AnyObject,
                     "state":self.strStateCode as AnyObject, //UserProfileInfo.state
                     "city":UserProfileInfo.city as AnyObject,
                     "pincode":UserProfileInfo.pincode as AnyObject,
                     "address1delivery":UserProfileInfo.address1delivery as AnyObject,
                     "address2delivery":UserProfileInfo.address2delivery as AnyObject,
                     "statedelivery":UserProfileInfo.statedelivery as AnyObject,
                     "citydelivery":UserProfileInfo.citydelivery as AnyObject,
                     "pincodedelivery":UserProfileInfo.pincodedelivery as AnyObject,
                     "sameaddress":UserProfileInfo.isAddressSame as AnyObject] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.uploadDocumentAuthBridgeExperian(APIConstants.API_UploadDocumentAuthBridgeExperian, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("response: %@", response)
                    let responseKey = response.value(forKey:"response") as? AnyObject
                    if responseKey is NSNull //((response.value(forKey:"response") as? NSNull) == nil)
                    {
                        let status = response.value(forKey:"status") as? Int
                        if status == 1
                        {
                             self.viewTransparent.removeFromSuperview()
                                                   self.viewCreditScore.removeFromSuperview()
                                                   /*let strMessage = response.value(forKey:"message") as! String
                                                   CommonFunctions.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)*/ //Swati Commented uncommente after demo and delete below line
                                                   self.createNewEmployeeSubscription()
                        }
                        else if status == 0
                        {
                            //let strMessage = response.value(forKey:"message") as! String
                            //CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                        }
                    }
                    else
                    {
                        if self.isUpgradeSelected == true
                        {
                            self.createUpgradeSubscription()
                        }
                        else
                        {
                            self.createNewEmployeeSubscription()
                        }
                    }
                }
            }
        })
    }
    
    //MARK: - Recheck Car Availability
    
    func reCheckCarAvailability()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        
        var dictParam:[String:AnyObject]!
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let objCarList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
        let strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String
        //let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let miliSeconds: Int = CommonMethodsMylesZero.currentTimeInMiliseconds()
        let strSeconds : String = String(format: "%d",miliSeconds)
        let strRandom : String = String(format: "%@", CommonMethodsMylesZero.generateRandomDigits(4))
        self.strCoricID = String(format: "%@%@%@","LTR",strRandom,strSeconds)
        //objCarList?.maxTenureMonths
        
        let tenure : Int = objCarList?.maxTenureMonths ?? 0
        let strFromDate = CommonMethodsMylesZero.getCurrentDate()
        let nextMonth = Calendar.current.date(byAdding: .month, value: tenure, to: Date())
        //let nextMonth = Calendar.current.date(byAdding: .month, value: 6, to: Date())
        //print("nextMonth: ", nextMonth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let strNextMonth = formatter.string(from: nextMonth!)
        
        /*dictParam = ["CityId":"2",
                     "ClientId":"2699120",
                     "fromDate":"2020-07-17",
                     "DateIn":"2021-01-16",
                     "PickupTime":1000,
                     "DropOffTime":2359,
                     "PkgId":"40271",
                     "CarModelId":"118",
                     "CoricId":"LTR8101594905105",
                     "CarVariantId":"2056",
                     "CarColour":"Red",
                     "ClientCoId":"3920",
                     "CarID":"8177"] as [String : AnyObject]*/
        
        
        dictParam = ["CityId":objCarList!.cityID! as AnyObject,
                     "ClientId":strClientID as AnyObject, //userDetails4.clientID!
                     "fromDate":strFromDate ?? "",
                     "DateIn":strNextMonth,
                     "PickupTime":1000,
                     "DropOffTime":2359,
                     "PkgId":objCarList!.pkgID! as AnyObject,
                     "CarModelId":objCarList!.carModelID! as AnyObject,
                     "CoricId":self.strCoricID ?? "",
                     "CarVariantId":objCarList!.carVariantID! as AnyObject,
                     "CarColour":objCarList!.carColorName! as AnyObject,
                     "ClientCoId":"2205", //userDetails.clientcoID!
                     "CarID":objCarList!.carID! as AnyObject] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.reCheckCarAvailability(APIConstants.API_RecheckCarAvailabilityLTR, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("response: ", response)
                    let isAvailable = response.value(forKey: "Available") as? Int
                    if isAvailable == 1
                    {
                        self.submitDocumentDetailWebService()
                    }
                    else
                    {
                        print("nil")
                        CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Server Error", strAction1: "OK", viewController: self)
                        //Sorry! car is not available. Please try for other.
                    }
                }
            }
        })
    }
    
    //MARK: - Create New Subscription
    
    func createNewEmployeeSubscription()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        var dictParam : [String:AnyObject]!
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let objCarList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
         let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        //let strFromDate = CommonFunctions.getCurrentDate()
        let nextMonth = Calendar.current.date(byAdding: .month, value: 6, to: Date())
        //print("nextMonth: ", nextMonth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let strNextMonth = formatter.string(from: nextMonth!)
        //let strCityID : String = String(objCarList?.cityID)
        let strTenure = DefaultsValues.getStringValueFromUserDefaults_(forKey:kTenure)
        
        dictParam = ["clientid":strClientID as AnyObject, //userDetails.clientID!
                     "amt":String(objCarList!.depositAmt!) as AnyObject,
                     "cityid":String(objCarList!.cityID!) as AnyObject,
                     "variantid":objCarList!.carVariantID! as AnyObject,
                     "aadharno":self.lblAdhaarCardNo.text! as AnyObject,
                     "aadharfile":self.strAdhaarFile as AnyObject,
                     "dlno":self.lblDrivingLicenceNo.text! as AnyObject,
                     "dlfile":self.strDLFile as AnyObject,
                     "panno":self.lblPanCardNo.text! as AnyObject,
                     "panfile":self.strPanFile as AnyObject,
                     "filepath":self.strAmazonBucketFilePath as AnyObject,
                     "pkgid":String(objCarList!.pkgID!) as AnyObject,
                     "flag":"1",
                     "Tenure":strTenure as AnyObject, //"DLExpiryDate":"",
                     "PassportExpiryDate":"7/6/1988",
                     "CarColor":objCarList!.carColorName! as AnyObject,
                     "CoricId":self.strCoricID as AnyObject,
                     "doctype":self.strDocType as AnyObject,
                     "address":"",
                     "deliverydate":"07/17/2020"] as [String:AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.CreateNewSubscriptionForEmployee(APIConstants.API_CreateNewSubscription, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    print("response: ", response)
                    let dict = response as! NSDictionary
                    let objSubscriptionRequestVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionRequest) as! SubscriptionRequestVC
                    objSubscriptionRequestVC.dictSubscriptionRequest = dict
                    objSubscriptionRequestVC.strFromPayment = false
                    self.navigationController?.pushViewController(objSubscriptionRequestVC, animated: true)
                }
            }
        })
    }
}
