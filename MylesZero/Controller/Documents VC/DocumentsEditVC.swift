//
//  DocumentsEditVC.swift
//  Myles Zero
//
//  Created by skpissay on 26/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import Alamofire

class DocumentsEditVC: UIViewController {
    
    var isAdhaarSelected = false
    var strTypeOfDocument : String?

    @IBOutlet weak var lblAdhaarDOB: UILabel!
    @IBOutlet weak var lblAdhaarImageName: UILabel!
    @IBOutlet weak var lblAdhaarCardNo: UILabel!
    @IBOutlet weak var lblAdhaarCardName: UILabel!
    
    @IBOutlet weak var lblDrivingImageName: UILabel!
    @IBOutlet weak var lblDrivingDOB: UILabel!
    @IBOutlet weak var lblDrivingNo: UILabel!
    
    @IBOutlet weak var lblPanImageName: UILabel!
    @IBOutlet weak var lblPanCardNo: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var btnPanCardChange: UIButton!
    @IBOutlet weak var btnLicenceChange: UIButton!
    @IBOutlet weak var btnAdhaarChange: UIButton!
    
    @IBOutlet weak var btnAdhaarCardRadio: UIButton!
    @IBOutlet weak var btnPassportRadio: UIButton!
    
    var dictAdhaarInfo : [String:AnyObject]?
    var dictDrivingLicenceInfo : [String:AnyObject]?
    var dictPanCardInfo : [String:AnyObject]?
    var dictPassportInfo : [String:AnyObject]?
    var strAdhaarDOB : String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        strTypeOfDocument = "Adhaar"
        
        CommonMethodsMylesZero.setBorder(button: self.btnAdhaarChange)
        CommonMethodsMylesZero.setBorder(button: self.btnLicenceChange)
        CommonMethodsMylesZero.setBorder(button: self.btnPanCardChange)
        
        btnAdhaarCardRadio.isSelected = true
        btnAdhaarCardRadio.setBackgroundImage(UIImage(named: "select_radio"), for: .normal)        
        btnPassportRadio.setBackgroundImage(UIImage(named: "unselect_radio"), for: .normal)
        self.scrollView.delaysContentTouches = true
        
        self.lblAdhaarCardNo.text! = self.dictAdhaarInfo?["docNumber"] as! String
        self.lblAdhaarDOB.text! = self.strAdhaarDOB ?? "04/05/1978"
        self.lblAdhaarCardName.text! = self.dictAdhaarInfo?["name"] as! String
        
        self.lblDrivingDOB.text! = self.dictDrivingLicenceInfo?["dob"] as! String
        self.lblDrivingNo.text! = self.dictDrivingLicenceInfo?["docNumber"] as! String
        
        self.lblPanCardNo.text! = self.dictPanCardInfo?["docNumber"] as! String
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Button Actions
    
    @IBAction func btnBack_Click(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGetCreditScore_Click(_ sender: Any)
    {

    }
    
    @IBAction func btnAdhaarRadio_Click(_ sender: Any)
    {
        isAdhaarSelected = true
        strTypeOfDocument = "Adhaar"
        btnAdhaarCardRadio.setImage(UIImage(named:"select_radio"), for: UIControl.State())
        btnPassportRadio.setImage(UIImage(named:"unselect_radio"), for: UIControl.State())
        
        /*if self.viewPassportView.superview != nil
        {
            self.viewPassportView.removeFromSuperview()
        }
        else
        {
            guard Bundle.main.path(forResource: "PassportView", ofType: "nib") != nil else {
                return
            }
        }*/
    }
    
    @IBAction func btnPassportRadio_Click(_ sender: Any)
    {
        isAdhaarSelected = false
        strTypeOfDocument = "Passport"
        btnPassportRadio.setImage(UIImage(named:"select_radio"), for: UIControl.State())
        btnAdhaarCardRadio.setImage(UIImage(named:"unselect_radio"), for: UIControl.State())
        
        /*self.viewPassportView = Bundle.main.loadNibNamed("PassportView", owner: nil, options: nil)![0] as! PassportView
        self.viewAdhaarPassportContent.addSubview(self.viewPassportView)
        self.viewPassportView.frame = CGRect(x: 0, y: 0, width: self.viewAdhaarPassportContent.frame.size.width, height: self.viewAdhaarPassportContent.frame.size.height)*/
    }
    
    @IBAction func btnUpdate_Click(_ sender: Any) {
    }
    @IBAction func btnPanCardChange_Click(_ sender: Any) {
        
    }
    
    @IBAction func btnLicenceChange_Click(_ sender: Any) {
        
    }
    
    @IBAction func btnAdhaarChange_Click(_ sender: Any) {
        
    }
}
