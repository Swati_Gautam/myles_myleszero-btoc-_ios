//
//  CreditScoreView.swift
//  Myles Zero
//
//  Created by skpissay on 15/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class CreditScoreView: UIView {

    @IBOutlet weak var lblCreditScoreMsg: UILabel!
    @IBOutlet weak var lblCreditScoreNo: UILabel!
    @IBOutlet weak var btnSubmitRequest: UIButton!
}
