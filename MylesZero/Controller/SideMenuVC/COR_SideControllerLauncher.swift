//
//  COR_SideControllerLauncher.swift
//  COR
//
//  Created by Nitesh Kant on 12/10/15.
//  Copyright © 2015 Nitesh Kant. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

class COR_SideControllerLauncher:NSObject,UIAlertViewDelegate
{
    //var  genericPopup:COR_GenericPopupLauncher!
    var  navigationController:UINavigationController!
    //var activityIndcicator:COR_ActivityIndicator!
    //var cabsModel = Cabs_Model.sharedInstance
    
    func launchSideController(_ navController:UINavigationController,index:Int ,name:String)
    {
        
        //Present new view controller
        
        self.navigationController = navController
        
        //let mainStoryboard: UIStoryboard = UIStoryboard(name: COR_Constants.kStoryBoardIdentifierStr,bundle: nil)
        var destViewController:UIViewController!

        if(name == "Log Out")
        {
           //self.showAlertviewController("Logout", message: "Are you sure you want to logout?", aNavController: navController, mainStoryboard: COR_Constants.mainStoryboard)
        
            return
        }
        
        
        switch (name)
        {
            
        case "Search":
            
            //self.cabsModel.isFromSideMenu = true
            
            let (exists, viewController) = self.isDestViewControllerExists(SearchCarVC.self)
            if exists
            {
                self.navigationController.popToViewController(viewController, animated: true)
                
            }
            else
            {
//                destViewController = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCar) as! SearchCarVC
//                self.navigationController?.pushViewController(destViewController, animated: true)
                
                guard let objSearchCarVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCar) as? SearchCarVC else { return }
                
                let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objSearchCarVC)
                SideMenuManager.default.menuRightNavigationController = leftMenuNavigationController
                SideMenuManager.default.menuWidth = 300.0
                SideMenuManager.default.menuFadeStatusBar = false
            }
            break
            
        case "Dashboard":
            
            
            /*let (exists, viewController) = self.isDestViewControllerExists(COR_MyRidesController.self)
            if exists
            {
                self.navigationController.popToViewController(viewController, animated: true)
            }
            else
            {            
                 destViewController = COR_Constants.mainStoryboard.instantiateViewController(withIdentifier: COR_Constants.kStoryBoardMRIdentifierStr) as? COR_MyRidesController
                self.navigationController.pushViewController(destViewController, animated: true)
            }*/
            break
            
        case "Invoice":
            let (exists, viewController) = self.isDestViewControllerExists(SearchCarVC.self)
            if exists
            {
                self.navigationController.popToViewController(viewController, animated: true)
                
            }
            else
            {
                //                destViewController = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCar) as! SearchCarVC
                //                self.navigationController?.pushViewController(destViewController, animated: true)
                
                guard let objInvoiceDetailVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_InvoiceDetail) as? InvoiceDetailVC else { return }
                
                let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objInvoiceDetailVC)
                SideMenuManager.default.menuRightNavigationController = leftMenuNavigationController
                SideMenuManager.default.menuWidth = 300.0
                SideMenuManager.default.menuFadeStatusBar = false
            }

            break
            
        case "FAQ's":
            
            let (exists, viewController) = self.isDestViewControllerExists(FaqVC.self)
            if exists
            {
                self.navigationController.popToViewController(viewController, animated: true)
            }
            else
            {
                guard let objFaqVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_Faq) as? FaqVC else { return }
                
                let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objFaqVC)
                SideMenuManager.default.menuRightNavigationController = leftMenuNavigationController
                SideMenuManager.default.menuWidth = 300.0
                SideMenuManager.default.menuFadeStatusBar = false
            }
            break
        
        case "Profile":
            
            /*self.cabsModel.isFromSideMenu = true
            let (exists, viewController) = self.isDestViewControllerExists(PaymentOptionsVC.self)
            if exists
            {
                self.navigationController.popToViewController(viewController, animated: true)
            }
            else
            {
                destViewController = COR_Constants.homeStoryboard.instantiateViewController(withIdentifier: COR_Constants.VC_EliteRewardsVC) as? EliteRewardVC
                self.navigationController.pushViewController(destViewController, animated: true)
            }*/
            break
            
        default:
            
            /*let (exists, viewController) = self.isDestViewControllerExists(COR_SupportViewController.self)
            
            if exists
            {
                self.navigationController.popToViewController(viewController, animated: true)
            }
            else
            {
                destViewController = COR_Constants.mainStoryboard.instantiateViewController(withIdentifier: COR_Constants.kStoryBoardSuppIdentifierStr) as! COR_SupportViewController
                self.navigationController.pushViewController(destViewController, animated: true)
            }*/
            break
        }
    }
    
    func isDestViewControllerExists(_ destVwController: AnyClass) -> (exists:Bool, vwController:UIViewController)
    {
        for controller in self.navigationController.viewControllers
        {
            if(controller.isKind(of: destVwController))
            {
                return (true, controller)
            }
        }
        return (false, UIViewController())
    }
    
    func removeTheControllers(_ navController:UINavigationController,stroryBoard:UIStoryboard)
    {
       // COR_LogOut().logOut()
    }
    
    // MARK: - Common Method
    
    func showAlertviewController(_ title:String, message:String,aNavController:UINavigationController,mainStoryboard:UIStoryboard)
    {

        /*genericPopup=COR_GenericPopupLauncher()
        genericPopup.genericDelegate=self
        genericPopup.initializePopupWithFrame(CGRect(x: 0, y: 0, width: 293, height: 184), onView:aNavController.view)
        genericPopup.setUITitles(COR_Constants.kNoConstant, aRightBtnTitle: COR_Constants.kYesConstant, popupTitle: title, msgContent: message)*/
    }

    func removeAllKeysFromUserDefault()
    {
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        //UserDefaults.standard.synchronize()
        //print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
    
    /*func onLeftBtnClick(_ aLeftBtn: UIButton)
    {
        if((genericPopup) != nil)
        {
            genericPopup.dismissViews()
        }
    }
    func onRightBtnClick(_ aRightBtn: UIButton)
    {
        if((genericPopup) != nil)
        {
            DispatchQueue.main.async{
                self.genericPopup.dismissViews()
            }
        }
        
        COR_LogOut().logOut()
    }
    
    func showActivityIndicator()
    {
        DispatchQueue.main.async{
            
            self.activityIndcicator=COR_ActivityIndicator()
            self.activityIndcicator.showProgressActivity(COR_Constants.kActivityMsgConstant, indicator: true,aResultantView:UIApplication.shared.keyWindow!)
        }
    }
    
    func removeActivityIndicator()
    {
        DispatchQueue.main.async{
            
            if((self.activityIndcicator) != nil)
            {
                self.activityIndcicator.messageFrame.removeFromSuperview()
                self.activityIndcicator=nil
            }
            
        }
    }*/
}
