//
//  SideMenuVC.swift
//  Myles Zero
//
//  Created by skpissay on 30/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var viewPwdAndLogOut: UIView!
    @IBOutlet weak var tblMenuList: UITableView!
    var drawerDelegate:CustomSideMenuDelegate!
    
    //var viewTableFooter : UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    var arrMenuContent = NSArray()
    var arrSubscriptionList = NSArray()
    
    var arrMenuImage: [UIImage] = [
        UIImage(named: "search")!,
        UIImage(named: "subscription")!,
        UIImage(named: "invoice")!,
        UIImage(named: "FAQ's")!,
        UIImage(named: "profile")!,
        UIImage(named: "myles_sidemenu")!
    ]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) != nil
        if UserDefaults.standard.bool(forKey: IS_LOGIN) == true
        {
            //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
            //self.lblUserName.text = String(format: "%@ %@", userDetails.fname, userDetails.lname)
            let name = UserDefaults.standard.object(forKey: KUSER_NAME) as? String
            let newStr = "(null) (null)"
            if (name == newStr) {
                self.lblUserName.text = "" //UserDefaults.standard.object(forKey: KUSER_NAME) as? String
            }
            else
            {
                self.lblUserName.text = UserDefaults.standard.object(forKey: KUSER_NAME) as? String
            }
            if (name == newStr) {
                self.lblUserName.text = "" //UserDefaults.standard.object(forKey: KUSER_NAME) as? String
            }
            else
            {
                self.lblUserName.text = String(format: "%@ %@", UserDefaults.standard.object(forKey: KUSER_FNAME) as! CVarArg, UserDefaults.standard.object(forKey: KUSER_LNAME) as! CVarArg)//UserDefaults.standard.object(forKey: KUSER_NAME) as? String
            }
            //self.arrMenuContent = ["Search", "My Dashboard", "Invoice", "FAQ's", "Profile", "Short-term rentals"]
            self.arrMenuContent = ["SEARCH", "MY DASHBOARD", "INVOICE", "FAQ's", "PROFILE", "SHORT-TERM RENTALS"]
            self.tblMenuList.separatorStyle = .none
            self.viewPwdAndLogOut.isHidden = false
        }
        else
        {
            //self.arrMenuContent = ["Search","FAQ's", "Short-term rentals"]
            self.arrMenuContent = ["SEARCH","FAQ's", "SHORT-TERM RENTALS"]
            self.arrMenuImage = [UIImage(named: "search")!, UIImage(named: "FAQ's")!, UIImage(named: "myles_sidemenu")!]
            self.tblMenuList.separatorStyle = .none
            self.viewPwdAndLogOut.isHidden = true
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //9899371345
    }
    
    // MARK: - Button Action
    
    @IBAction func btnChangePassword_Click(_ sender: Any)
    {
        /*let objChangePasswordVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "idChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(objChangePasswordVC, animated: true)*/
        
        
        /*guard let objChangePasswordVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: Constants.VC_ChangePassword) as? ChangePasswordVC else { return }
        
        /*let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objChangePasswordVC)
        SideMenuManager.default.menuRightNavigationController = leftMenuNavigationController
        SideMenuManager.default.menuWidth = 300.0
        SideMenuManager.default.menuFadeStatusBar = false
        
        present(leftMenuNavigationController, animated: true, completion: nil)
        
        guard let objInvoiceDetailVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_InvoiceDetail) as? InvoiceDetailVC else { return }*/
         
        self.navigationController?.pushViewController(objChangePasswordVC, animated: true)*/
    }
    
    @IBAction func btnLogout_Click(_ sender: Any)
    {
        CommonMethodsMylesZero.popupAlert(title: "Are you sure you want to logout?", message: "", actionTitles: ["Ok","Cancel",], actions:[{action1 in
            self.clearDataAfterLogout()
            
            DefaultsValues.setBooleanValueToUserDefaults(true, forKey: kIsLogout)
        },{action2 in
             
        }, nil], viewController: self)
        
        //CommonFunctions.showAlertController1(strTitle: "Are you sure you want to logout?", strMsg: "", strAction1: "OK", strAction2: "Cancel", viewController: self)
    }
    
    func sessionExpire()
    {
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "MYLES ZERO", message: "Session Expired", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //CommonFunctions.logoutcommonFunction()
                self.clearDataAfterLogout()
                //self.navigationController?.popToRootViewController(animated: false)
                guard let objSearchCarVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier:Constants.VC_SearchCar) as? SearchCarVC else { return }
                self.navigationController?.pushViewController(objSearchCarVC, animated: false)  
            })
            alertController.addAction(ok)
            self.present(alertController, animated: true) { }
        }
    }
    
    func clearDataAfterLogout()
    {
        let appDomain = Bundle.main.bundleIdentifier!
        let arrCityData = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray)! as NSArray
        
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) != nil
        {
            //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
            //self.lblUserName.text = String(format: "%@ %@", userDetails.fname, userDetails.lname)
            
            //self.lblUserName.text = String(format: "%@ %@", UserDefaults.standard.value(forKey: KUSER_FNAME) as! CVarArg,  UserDefaults.standard.value(forKey: KUSER_LNAME) as! CVarArg)
            
            self.lblUserName.text = UserDefaults.standard.value(forKey: KUSER_NAME) as? String
            
            self.arrMenuContent = ["SEARCH", "MY DASHBOARD", "INVOICE", "FAQ's", "PROFILE"]
            self.tblMenuList.separatorStyle = .none
            self.viewPwdAndLogOut.isHidden = false
        }
        else
        {
            self.lblUserName.text = "Guest !"
            self.arrMenuContent = ["SEARCH","FAQ's", "SHORT-TERM RENTALS"]
            self.arrMenuImage = [UIImage(named: "search")!, UIImage(named: "FAQ's")!,UIImage(named: "myles_sidemenu")!]
            self.tblMenuList.separatorStyle = .none
            self.viewPwdAndLogOut.isHidden = true
            UserDefaults.standard.set(false, forKey: IS_LOGIN)
            DefaultsValues.setArrayValueFromUserDefaults((arrCityData), forKey: kCityListArray)
            /*CommonFunctions.getAccessToken(completion: {
                                        
            })*/
            self.fetchAccessToken()
        }        
        self.tblMenuList.reloadData()
    }
    
    //MARK: - Fetch Access Token API
    
    func fetchAccessToken()
    {
        var sessionID = String()
        sessionID = CommonMethodsMylesZero.generateSessionID()!
        //let CustomerID = "-99"
        
        let md5Data = CommonMethodsMylesZero.MD5(string:"IOS@@MYLESCAR")
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        
        //[DEFAULTS boolForKey:IS_LOGIN] ? [DEFAULTS objectForKey:KUSER_ID]:@"-99"
        
        let dictAccessToken = ["userID" : "IOS",
                               "password" : md5Hex,
                               "IPRestriction" : false,
                               "sessionID" : sessionID,
                               "customerID" : UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"] as [String : AnyObject] //CustomerID
        
        APIFunctions.sharedInstance.getAccessTokenAPI(APIConstants.API_GetAccessToken, dataDict: dictAccessToken) { (response,error) in
            DispatchQueue.main.async {
                let JSON = response as AnyObject
                //fetchAccessToken
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "accessToken") as? String, forKey: kAccessToken)
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "sessionID") as? String, forKey: kSessionID)
                //let strAccessToken = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAccessToken)!
                //let strSessionID = DefaultsValues.getStringValueFromUserDefaults_(forKey: kSessionID)!
                
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "accessToken") as? String, forKey: K_AccessToken)
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "sessionID") as? String, forKey: K_SessionID)
                
                let strAccessToken = DefaultsValues.getStringValueFromUserDefaults_(forKey: K_AccessToken)!
                let strSessionID = DefaultsValues.getStringValueFromUserDefaults_(forKey: K_SessionID)!
                let customerID = UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"
                
                let dictHeaders = CommonMethodsMylesZero.getHeaderValues(sessionId: strSessionID , customerId: customerID as! String, hasValue: strAccessToken )! as [String:String]
                
                //let dictHeaders = CommonMethodsMylesZero.getHeaderValues(sessionId: strSessionID, customerId: kCustomerID, hasValue: strAccessToken)! as [String:String]
                kDictHeaders = dictHeaders
                DefaultsValues.setDictionaryValueToUserDefaults(kDictHeaders as NSDictionary, forKey: kHttpHeaders)
                //self.fetchCityListWebService()
            }
        }
    }
    
    /*func fetchAccessToken()
    {
        var sessionID = String()
        sessionID = CommonMethodsMylesZero.generateSessionID()!
        let CustomerID = "-99"
        
        let md5Data = CommonMethodsMylesZero.MD5(string:"IOS@@MYLESCAR")
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        
        let dictAccessToken = ["userID" : "IOS",
                               "password" : md5Hex,
                               "IPRestriction" : false,
                               "sessionID" : sessionID,
                               "customerID" : CustomerID] as [String : AnyObject]
        
        APIFunctions.sharedInstance.getAccessTokenAPI(APIConstants.API_GetAccessToken, dataDict: dictAccessToken) { (response,error) in
            DispatchQueue.main.async {
                let JSON = response as AnyObject
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "accessToken") as? String, forKey: kAccessToken)
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "sessionID") as? String, forKey: kSessionID)
                let strAccessToken = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAccessToken)!
                let strSessionID = DefaultsValues.getStringValueFromUserDefaults_(forKey: kSessionID)!
                let dictHeaders = CommonMethodsMylesZero.getHeaderValues(sessionId: strSessionID, customerId: kCustomerID, hasValue: strAccessToken)! as [String:String]
                kDictHeaders = dictHeaders
                DefaultsValues.setDictionaryValueToUserDefaults(kDictHeaders as NSDictionary, forKey: kHttpHeaders)
            }
            //self.fetchCityListWebService()
        }
    }*/
    
    // MARK: - TableView Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrMenuContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idSideMenuCell"
        let cell:SideMenuTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SideMenuTableViewCell?)!
        cell.selectionStyle = .none
        cell.imgMenu.image = self.arrMenuImage[indexPath.row]
        cell.lblMenuContent.text = self.arrMenuContent[indexPath.row] as? String
        
        //let strName = (self.arrFilter[indexPath.row] as AnyObject).object(forKey:"Number") as! String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let strSideMenuName = self.arrMenuContent[indexPath.row] as? String
        switch (strSideMenuName)
        {
            
        case "SEARCH":
                
            guard let objSearchCarVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier:Constants.VC_SearchCar) as? SearchCarVC else { return }                
            self.navigationController?.pushViewController(objSearchCarVC, animated: false)
            break
            
        case "MY DASHBOARD":
            
            /*guard let objSubscriptionListVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionList) as? SubscriptionListVC else { return }
            self.navigationController?.pushViewController(objSubscriptionListVC, animated: true)*/
            
            self.fetchSubscriptionList()
            
            /*let (exists, viewController) = self.isDestViewControllerExists(COR_MyRidesController.self)
             if exists
             {
             self.navigationController.popToViewController(viewController, animated: true)
             }
             else
             {
             destViewController = COR_Constants.mainStoryboard.instantiateViewController(withIdentifier: COR_Constants.kStoryBoardMRIdentifierStr) as? COR_MyRidesController
             self.navigationController.pushViewController(destViewController, animated: true)
             }*/
            break
            
        case "PROFILE":
            
            guard let objUserProfileVC = Constants.userProfileStoryboard.instantiateViewController(withIdentifier: Constants.VC_UserProfileMylesZero) as? UserProfileMylesZeroVC else { return }
            self.navigationController?.pushViewController(objUserProfileVC, animated: true)
            
            /*let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objInvoiceDetailVC)
             SideMenuManager.default.menuRightNavigationController = leftMenuNavigationController
             SideMenuManager.default.menuWidth = 300.0
             SideMenuManager.default.menuFadeStatusBar = false*/
            
            break
            
        case "INVOICE":
                
            guard let objInvoiceDetailVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_InvoiceDetail) as? InvoiceDetailVC else { return }
            self.navigationController?.pushViewController(objInvoiceDetailVC, animated: true)
                
            /*let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objInvoiceDetailVC)
            SideMenuManager.default.menuRightNavigationController = leftMenuNavigationController
            SideMenuManager.default.menuWidth = 300.0
            SideMenuManager.default.menuFadeStatusBar = false*/
            
            break
            
        case "FAQ's":
            
        guard let objFaqVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_Faq) as? FaqVC else { return }
        self.navigationController?.pushViewController(objFaqVC, animated: true)
        
        break
            
        case "SHORT-TERM RENTALS": //"Self Driven Car":
            
            let mainTabStoryboard: UIStoryboard = UIStoryboard(name: "MainTabStoryboard", bundle: nil)
            //guard let viewController = mainTabStoryboard.instantiateViewController(withIdentifier: "MainTabController") else { return }
            let viewController = mainTabStoryboard.instantiateViewController(withIdentifier: "MainTabController")
            AppDelegate.getSharedInstance()?.window.rootViewController = viewController
            
            break

        default:
            
            /*let (exists, viewController) = self.isDestViewControllerExists(COR_SupportViewController.self)
             
             if exists
             {
             self.navigationController.popToViewController(viewController, animated: true)
             }
             else
             {
             destViewController = COR_Constants.mainStoryboard.instantiateViewController(withIdentifier: COR_Constants.kStoryBoardSuppIdentifierStr) as! COR_SupportViewController
             self.navigationController.pushViewController(destViewController, animated: true)
             }*/
            break
        }
        //tableView.reloadData()
        //self.drawerDelegate.launchController(indexPath.row, name: self.arrMenuContent[indexPath.row] as! String)
    }
    
    //MARK: - Fetch Subscription
    
    func fetchSubscriptionList()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        var dictParam:[String:AnyObject]!
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        //let strClientID = String(format: "%@", userDetails.clientID)
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        //UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strUserId, forKey: KUSER_ID)
        
        dictParam = ["ClientCoIndivID" : strClientID, //"pan2334555uu"
                     "SubscriptionId" : ""] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.fetchSubscriptionList(APIConstants.API_FetchSubscriptionDashboard, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.arrSubscriptionList = response.value(forKey: "response") as! NSArray
                    
                    guard let objSubscriptionActiveVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionActive) as? SubscriptionActiveVC else { return }
                    if self.arrSubscriptionList.count > 0
                    {
                        objSubscriptionActiveVC.arrSubscriptionList = self.arrSubscriptionList
                        /*for i in 0..<self.arrSubscriptionList.count
                        {
                            let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[i]) as! [String : Any])
                            DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
                            
                            let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
                            
                            if objSubscriptionDetail?.carSubscriptionStatus == "Subscription Active"
                            {
                                objSubscriptionActiveVC.arrSubscriptionList = self.arrSubscriptionList
                            }
                        }*/
                    }
                    else
                    {
                        
                    }
                    self.navigationController?.pushViewController(objSubscriptionActiveVC, animated: true)
                }
            }
        })
    }
    
    
    /*func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        viewTableFooter = TableFooterView.loadFromNibNamed(nibNamed: "TableFooterView")
        //viewTableFooter = TableFooterView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 70))
        return viewTableFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 110
    }*/

}

protocol CustomSideMenuDelegate {
    func launchController(_ onIndex:Int,name: String)
}
