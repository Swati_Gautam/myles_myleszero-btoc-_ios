//
//  TableFooterView.swift
//  Myles Zero
//
//  Created by skpissay on 30/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class TableFooterView: UIView {

    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    /*
     
     // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
