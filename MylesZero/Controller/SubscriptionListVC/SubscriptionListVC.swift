//
//  SubscriptionListVC.swift
//  Myles Zero
//
//  Created by skpissay on 17/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import Alamofire

class SubscriptionListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var arrSubscriptionList = NSArray()
    @IBOutlet weak var tblViewSubscription: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.fetchSubscriptionList()
    }
    
    func toggleSection(_ section: Int) {
        print("section: ", section)
    }
    
    func fetchSubscriptionList()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        var dictParam:[String:AnyObject]!
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        //let strClientID = String(format: "%@", userDetails.clientID)
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        dictParam = ["ClientCoIndivID" : strClientID, //"pan2334555uu"
                     "SubscriptionId" : ""] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.fetchSubscriptionList(APIConstants.API_FetchSubscriptionDashboard, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    self.arrSubscriptionList = response.value(forKey: "response") as! NSArray                    
                    self.tblViewSubscription.isHidden = false
                    self.tblViewSubscription.reloadData()
                }
            }
        })
    }
    
    //MARK: - UITableView Delegate Methods
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrSubscriptionList.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        /*let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SubscriptionHeaderView") as? SubscriptionHeaderView ?? SubscriptionHeaderView(reuseIdentifier: "SubscriptionHeaderView")*/
        tableView.register(UINib(nibName: "SubscriptionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "SubscriptionHeaderView")
        
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SubscriptionHeaderView") as! SubscriptionHeaderView
        let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[section]) as! [String : Any])
        //let dictSubscriptionDetail = self.arrSubscriptionList[indexPath.row] as! NSDictionary
        DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
        
        let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
        
        headerCell.lblCarModelName.text = String(format: "%@ - %@", objSubscriptionDetail!.carModelName!, objSubscriptionDetail!.carVariantName!)
        headerCell.lblSubscriptionID.text = objSubscriptionDetail!.subscriptionId!
        //header.arrowLabel.text = ">"
        //header.setCollapsed(sections[section].collapsed)
        headerCell.section = section
        headerCell.delegate = self
        return headerCell
    }*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrSubscriptionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idSubscriptionList"
        let cell:SubscriptionListTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SubscriptionListTableViewCell?)!

        let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[indexPath.row]) as! [String : Any])
        //let dictSubscriptionDetail = self.arrSubscriptionList[indexPath.row] as! NSDictionary
        DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
        
        let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
        
        cell.lblCarName.text! = String(format: "%@ - %@", objSubscriptionDetail!.carModelName!, objSubscriptionDetail!.carVariantName!)
        cell.lblSubscriptionRequestID.text = objSubscriptionDetail!.subscriptionId!
        cell.lblSubscriptionStatus.text = objSubscriptionDetail!.carSubscriptionStatus!
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[indexPath.row]) as! [String : Any])
        //let dictSubscriptionDetail = self.arrSubscriptionList[indexPath.row] as! NSDictionary
        DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
        
        let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass

        let strSubscriptionStatus = objSubscriptionDetail!.carSubscriptionStatus! as String
        
        if strSubscriptionStatus == "Subscription Active"
        {
            let objSubscriptionActiveVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionActive) as! SubscriptionActiveVC
            //objSubscriptionActiveVC.objSubscriptionDetail = objSubscriptionDetail
            self.navigationController?.pushViewController(objSubscriptionActiveVC, animated: true)
        }
    }
    
    //MARK: - Button Actios
    
    @IBAction func btnBack_Click(_ sender: Any) {
    }
}

/*extension SubscriptionListVC: CollapsibleTableSectionDelegate {
    
    func numberOfSections(_ tableView: UITableView) -> Int {
        return self.arrSubscriptionList.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CustomCell ??
            CustomCell(style: .default, reuseIdentifier: "Cell")
        
        let item: Item = sections[(indexPath as NSIndexPath).section].items[(indexPath as NSIndexPath).row]
        
        cell.nameLabel.text = item.name
        cell.detailLabel.text = item.detail
        
        return cell
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return sections[section].name
    }
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return true
    }
    
    func shouldCollapseOthers(_ tableView: UITableView) -> Bool {
        return true
    }
}*/
