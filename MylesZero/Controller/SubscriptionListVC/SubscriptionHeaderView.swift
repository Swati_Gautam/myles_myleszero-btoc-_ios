//
//  SubscriptionHeaderView.swift
//  Myles Zero
//
//  Created by skpissay on 19/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

protocol SubscriptionHeaderViewDelegate {
    func toggleSection(_ section: Int)
}

class SubscriptionHeaderView: UITableViewHeaderFooterView
{
    var delegate: SubscriptionHeaderViewDelegate?
    var section: Int = 0
    @IBOutlet weak var btnExpandCollapse: UIButton!
    @IBOutlet weak var lblSubscriptionID: UILabel!
    @IBOutlet weak var lblCarModelName: UILabel!

    override public init(reuseIdentifier: String?)
    {
        super.init(reuseIdentifier: reuseIdentifier)
        
        // Content View
        contentView.backgroundColor = UIColor(hex: 0x2E3944)
        
        let marginGuide = contentView.layoutMarginsGuide
        
        // Arrow label
        /*contentView.addSubview(btnExpandCollapse)
        //btnExpandCollapse.textColor = UIColor.white
        btnExpandCollapse.translatesAutoresizingMaskIntoConstraints = false
        btnExpandCollapse.widthAnchor.constraint(equalToConstant: 12).isActive = true
        btnExpandCollapse.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        btnExpandCollapse.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        btnExpandCollapse.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        
        // Title label
        contentView.addSubview(btnExpandCollapse)
        //btnExpandCollapse.textColor = UIColor.white
        btnExpandCollapse.translatesAutoresizingMaskIntoConstraints = false
        btnExpandCollapse.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        btnExpandCollapse.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        btnExpandCollapse.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        btnExpandCollapse.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true*/
        
        //
        // Call tapHeader when tapping on this header
        //
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(_:))))
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    // Trigger toggle section when tapping on the header
    //
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? SubscriptionHeaderView else {
            return
        }
        
        _ = delegate?.toggleSection(cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        //
        // Animate the arrow rotation (see Extensions.swf)
        //
        btnExpandCollapse.rotate(collapsed ? 0.0 : .pi / 2)
    }

}


