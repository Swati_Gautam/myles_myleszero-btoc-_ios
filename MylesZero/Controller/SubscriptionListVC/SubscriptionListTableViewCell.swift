//
//  SubscriptionListTableViewCell.swift
//  Myles Zero
//
//  Created by skpissay on 17/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class SubscriptionListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSubscriptionStatus: UILabel!
    @IBOutlet weak var btnSubscription: UIButton!
    @IBOutlet weak var lblSubscriptionRequestID: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
