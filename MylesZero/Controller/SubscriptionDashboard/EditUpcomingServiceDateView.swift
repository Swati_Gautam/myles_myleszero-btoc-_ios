//
//  EditUpcomingServiceDateView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 27/01/21.
//  Copyright © 2021 skpissay. All rights reserved.
//

import UIKit

class EditUpcomingServiceDateView: UIView
{
    @IBOutlet weak var txtUpcomingDate: UITextField!
    @IBOutlet weak var btnEditDate: UIButton!
    @IBOutlet weak var btnSubmitRequest_Click: UIButton!
}
