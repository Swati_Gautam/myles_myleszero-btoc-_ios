//
//  UpdateDocumentsView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 16/01/21.
//  Copyright © 2021 skpissay. All rights reserved.
//

import UIKit
import iOSDropDown

class UpdateDocumentsView: UIView {

    @IBOutlet weak var txtDocumentType: DropDown!
    @IBOutlet weak var txtDocumentNo: DropDown!
    @IBOutlet weak var txtExpiryDate: UITextField!
    @IBOutlet weak var lblDocumentTitle: UILabel!
    @IBOutlet weak var txtReasonForUpdate: UITextField!
    @IBOutlet weak var lblUploadDocName: UILabel!
    @IBOutlet weak var viewDocType: UIView!
    @IBOutlet weak var docTypeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewExpiryDate: UIView!
    @IBOutlet weak var èxpiryDateHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnChangeImage: UIButton!
    @IBOutlet weak var btnUpdateDocument: UIButton!
    @IBOutlet weak var btnExpiryDate: UIButton!
}
