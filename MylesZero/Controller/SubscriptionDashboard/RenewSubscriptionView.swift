//
//  RenewSubscriptionView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 29/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import iOSDropDown

class RenewSubscriptionView: UIView {

    @IBOutlet weak var lblPriceDetail: UILabel!
    @IBOutlet weak var btnUpdateAndRenew: UIButton!
    @IBOutlet weak var txtKmLimit: DropDown!
    @IBOutlet weak var txtTenure: DropDown!

    @IBOutlet weak var btnKYCCheckbox: UIButton!
}
