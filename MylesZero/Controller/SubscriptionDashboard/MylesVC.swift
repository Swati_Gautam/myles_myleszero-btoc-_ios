//
//  MylesVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 28/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import WebKit

class MylesVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var progressBar: UIProgressView!
    var strMylesUrl = String()

    var isFirst = true
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.getMylesCarUrl()
        // Do any additional setup after loading the view.
    }
    
    func getMylesCarUrl()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
        strMylesUrl = "https://www.mylescars.com"
        let url = URL(string: strMylesUrl)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        hud!.dismiss()
    }
    
    //MARK :- Progress Bar Methods
    func setProgress()
    {
        self.progressBar.progress = 0.9
    }
        
    func completeProgress() {
        self.progressBar.progress = 1.0
    }
    
    
    //MARK :- WebView Delegate Methods
    func webViewDidStartLoad(_ webView: WKWebView)
    {
           self.setProgress()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("WebView content loaded.")
    }
       
    func webViewDidFinishLoad(_ webView: WKWebView)
    {
        self.completeProgress()
    }
       
    func webView(_ webView: WKWebView, didFailLoadWithError error: NSError?)
    {
        self.progressBar.progress = 0.0
        self.progressBar.isHidden = true
    }

}
