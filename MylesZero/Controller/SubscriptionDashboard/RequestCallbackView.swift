//
//  RequestCallbackView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 24/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import iOSDropDown

class RequestCallbackView: UIView {

    @IBOutlet weak var txtReasonForCallback: DropDown!
    @IBOutlet weak var txtTimeSlot: DropDown!
    @IBOutlet weak var txtPreferredDate: UITextField!
    @IBOutlet weak var txtContactNumber: DropDown!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var btnRequestCallback: UIButton!
}
