//
//  EndSubscriptionView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 21/01/21.
//  Copyright © 2021 skpissay. All rights reserved.
//

import UIKit
import iOSDropDown

class EndSubscriptionView: UIView {

    @IBOutlet weak var txtTerminationDate: DropDown!
    @IBOutlet weak var txtReasonForTermination: DropDown!
    @IBOutlet weak var btnTerminationDate: UIButton!
    @IBOutlet weak var btnEndSubscription: UIButton!
    @IBOutlet weak var viewPreclosure: UIView!
    @IBOutlet weak var preClosureHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblExistingRental: UILabel!
    
    @IBOutlet weak var lblTotalPreClosurePeriod: UILabel!
    @IBOutlet weak var lblTotalPreClosureAmount: UILabel!
    @IBOutlet weak var lblDifferenceAmount: UILabel!
    @IBOutlet weak var lblRevisedRental: UILabel!
}
