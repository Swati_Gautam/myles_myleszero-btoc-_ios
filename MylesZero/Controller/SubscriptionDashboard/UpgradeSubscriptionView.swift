//
//  UpgradeSubscriptionView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 29/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import iOSDropDown

class UpgradeSubscriptionView: UIView
{
    @IBOutlet weak var btnUpgradeDate: UIButton!
    @IBOutlet weak var lblSubscriptionTitle: UILabel!
    @IBOutlet weak var txtUpgradeDate: DropDown!
    
    @IBOutlet weak var btnKmUsageCheckbox: UIButton!
    @IBOutlet weak var txtKmUsage: DropDown!
    @IBOutlet weak var btnTenureCheckbox: UIButton!
    @IBOutlet weak var txtTenure: DropDown!
    @IBOutlet weak var btnCarModelCheckbox: UIButton!
    @IBOutlet weak var btnSubmitRequest: UIButton!
    
    @IBOutlet weak var lblKMValidation: UILabel!
    @IBOutlet weak var lblTenureValidation: UILabel!
    @IBOutlet weak var lblUpgradeAmount: UILabel!
}
