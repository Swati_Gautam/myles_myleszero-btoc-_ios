//
//  ReportLossView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 16/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import iOSDropDown

class ReportLossView: UIView {

    @IBOutlet weak var txtLossItem: DropDown!
    @IBOutlet weak var txtIncidentDate: UITextField!
    @IBOutlet weak var txtIncidentTime: DropDown!
   
    @IBOutlet weak var txtViewIncidentDetail: UITextView!
    @IBOutlet weak var btnUploadFIR: UIButton!
    
    @IBOutlet weak var txtFIRUrl: UITextField!
    @IBOutlet weak var btnIncidentDate: UIButton!
       @IBOutlet weak var btnIncidentTime: UIButton!
       @IBOutlet weak var btnSelectLoss: UIButton!
       @IBOutlet weak var btnReportLoss: UIButton!
    
    @IBOutlet weak var txtAccidentImage: UITextField!
    @IBOutlet weak var txtIncidentPlace: UITextField!
    
    @IBOutlet weak var viewAccident: UIView!
    
    @IBOutlet weak var accidentHeightConstraint: NSLayoutConstraint!
}
