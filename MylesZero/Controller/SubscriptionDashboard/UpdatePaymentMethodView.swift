//
//  UpdatePaymentMethodView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 18/01/21.
//  Copyright © 2021 skpissay. All rights reserved.
//

import UIKit

class UpdatePaymentMethodView: UIView
{
    
    @IBOutlet weak var txtCardNo: UITextField!
    
    @IBOutlet weak var txtCardName: UITextField!
    @IBOutlet weak var txtReasonForUpdate: UITextField!
    @IBOutlet weak var txtCVVNo: UITextField!
    @IBOutlet weak var txtExpiryDate: UITextField!
    
    @IBOutlet weak var btnUpdatePaymentCard: UIButton!
    @IBOutlet weak var btnExpiryDate: UIButton!
}
