//
//  ServiceHistoryView.swift
//  Myles Zero
//
//  Created by Swati Gautam on 11/01/21.
//  Copyright © 2021 skpissay. All rights reserved.
//

import UIKit
import iOSDropDown

class ServiceHistoryView: UIView {

    @IBOutlet weak var txtKMLimit: DropDown!
    @IBOutlet weak var lblServiceType: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblKMDriven: UILabel!
    
    @IBOutlet weak var btnRequestServiceRepair: UIButton!
    
    @IBOutlet weak var viewServiceData: UIView!
    
    @IBOutlet weak var txtPickupAddress: DropDown!
    @IBOutlet weak var txtTypesOfService: DropDown!
    @IBOutlet weak var txtPreferredTimeSlot: DropDown!
    @IBOutlet weak var txtPreferredDate: UITextField!
    @IBOutlet weak var txtContactNumber: DropDown!
    @IBOutlet weak var txtContactName: DropDown!
    @IBOutlet weak var btnSubmitRequest: UIButton!
    @IBOutlet weak var viewRequestSeviceRepair: UIView!
    @IBOutlet weak var btnServiceDate: UIButton!
    
    @IBOutlet weak var btnSubmitRequestForRepair: UIButton!
    @IBOutlet weak var viewOfServiceButton: UIView!
    
    @IBOutlet weak var serviceHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var RequestServiceTopConstraint: NSLayoutConstraint!
}
