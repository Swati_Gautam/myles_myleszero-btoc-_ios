//
//  WebViewVC.swift
//  Myles Zero
//
//  Created by skpissay on 13/08/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController, WKNavigationDelegate {
    
    //var webView: WKWebView!
    var strUrl = String()

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        
        //webview.load(URLRequest(url: Bundle.main.url(forResource: "index", withExtension:"html", subdirectory: "subdirectories")! as URL) as URLRequest)
        //self.view.addSubview(webView)
        self.callWebView()
    }
    
    /*override func loadView()
    {
        //webView = WKWebView()
        webView.navigationDelegate = self
        self.contentView = webView
        //webView.frame  = CGRect(x: 0, y: 72, width: self.view.frame.size.width, height: self.view.frame.size.height - 80)
        //let url = URL(string: "https://www.hackingwithswift.com")!
        
        //self.webView.scalesPageToFit = true
        self.webView.scrollView.isScrollEnabled = true
        let url = URL(string: strUrl)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }*/
    
    func callWebView()
    {
        let url = URL(string: strUrl)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    @IBAction func btnBack_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
