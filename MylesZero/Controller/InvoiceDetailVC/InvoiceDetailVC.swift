//
//  InvoiceDetailVC.swift
//  Myles Zero
//
//  Created by skpissay on 01/05/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class InvoiceDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{    
    @IBOutlet weak var tblViewInvoice: UITableView!
    let customSideMenuManager = SideMenuManager()
    var arrInvoiceList = NSArray()
    var index : Int = 0
    var isClickedFromSideMenu : Bool = false
    @IBOutlet var lblNoDataFound: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.getInvoiceList()
    }
    
    // MARK: - Side Menu Function
    
    private func setupSideMenu()
    {
        // Define the menus
        
        guard let objSideMenuVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SideMenu) as? SideMenuVC else { return }
        
        let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objSideMenuVC)
        //let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: SideMenuVC)
        customSideMenuManager.menuLeftNavigationController = leftMenuNavigationController
        
        customSideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        //customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        customSideMenuManager.menuWidth = 300.0
        customSideMenuManager.menuFadeStatusBar = false
        
        //leftMenuNavigationController.statusBarEndAlpha = 0
        
        present(leftMenuNavigationController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let sideMenuNavigationController = segue.destination as? UISideMenuNavigationController
        {
            sideMenuNavigationController.sideMenuManager = customSideMenuManager
        }
    }
    
    func getCurrentDate() -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        return dateFormatter.string(from: Date())
        
    }//8109973188
    
    //MARK: - Get Invoice List
    
    func getInvoiceList()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let strFromDate = CommonMethodsMylesZero.getCurrentDate() //"6/19/2020" 
        let nextMonth = Calendar.current.date(byAdding: .month, value: 6, to: Date())
        //print("nextMonth: ", nextMonth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let strNextMonth = formatter.string(from: nextMonth!)
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        let invoiceParam = ["loginid" : strClientID, //userDetails.clientID as String,
                            "logintype": "I", //int
                            "fromdate": strFromDate, //"09/10/19",
                            "todate": strNextMonth] as [String : AnyObject] //strNextMonth
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        APIFunctions.sharedInstance.GetInvoiceList(APIConstants.API_GetInvoiceList, dataDict: invoiceParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    if response != nil
                    {
                        self.arrInvoiceList = response.value(forKey: "response") as! NSArray
                        if self.arrInvoiceList.count > 0
                        {
                            self.tblViewInvoice.isHidden = false
                            self.lblNoDataFound.isHidden = true
                            self.tblViewInvoice.reloadData()
                        }
                        else
                        {
                            self.lblNoDataFound.isHidden = false
                            self.tblViewInvoice.isHidden = true
                        }
                    }
                }
            }
        })
    }
    
    //MARK: - UITableView Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrInvoiceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idInvoiceCell"
        let cell:InvoiceTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! InvoiceTableViewCell?)!
        
        let dictInvoice  = InvoiceDetailRootClass.init(fromDictionary:(self.arrInvoiceList[indexPath.row]) as! [String : Any])
        //let dictSubscriptionDetail = self.arrSubscriptionList[indexPath.row] as! NSDictionary
        DefaultsValues.setCustomObjToUserDefaults(dictInvoice, forKey: kInvoiceList)
        
        let objInvoice = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kInvoiceList) as? InvoiceDetailRootClass
        self.index = indexPath.row
        
        cell.lblCarName.text! = String(format: "%@ - %@", objInvoice!.carModel!, objInvoice!.variant!)
        //cell.lblInvoiceDate.text = objInvoice!.accountingDate!//objInvoice!.subscriptionStartDate!
        
        let startDate = objInvoice!.accountingDate!
        let arr1 = startDate.components(separatedBy: " ")
        let strFinalDate = arr1[0]
        cell.lblInvoiceDate.text = strFinalDate
        
        //cell.lblInvoiceNo.text! = String(format: "%@", objInvoice!.invoiceNumber!)
        cell.lblSubscriptioID.text = objInvoice!.subscriptionId!
        //cell.lblNextPaymentDate.text = objInvoice!.nextPaymentDate!
        //cell.lblTenureRemaining.text = objInvoice!.monthsRemaining!
        cell.lblInvoiceAmount.text = String(format: "₹ %@", objInvoice!.totalCost!)
        cell.lblStatus.text = objInvoice!.paidStatus!
        cell.lblInvoiceType.text = "Monthly Subscription Invoice"
        cell.btnViewInvoice.addTarget(self, action:#selector(self.btnViewInvoice_Click(sender:)), for: .touchUpInside)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[indexPath.row]) as! [String : Any])

        DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
        
        let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
        
        let strSubscriptionStatus = objSubscriptionDetail!.carSubscriptionStatus! as String
        
        if strSubscriptionStatus == "Subscription Active"
        {
            let objSubscriptionActiveVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionActive) as! SubscriptionActiveVC
            objSubscriptionActiveVC.objSubscriptionDetail = objSubscriptionDetail
            self.navigationController?.pushViewController(objSubscriptionActiveVC, animated: true)
        }*/
    }
    
    //MARK: - Button Actions
    
    
    @objc func btnViewInvoice_Click(sender:UIButton!)
    {
        let dictInvoice  = InvoiceDetailRootClass.init(fromDictionary:(self.arrInvoiceList[self.index]) as! [String : Any])
        DefaultsValues.setCustomObjToUserDefaults(dictInvoice, forKey: kSelectedInvoiceList)
        let objInvoice = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSelectedInvoiceList) as? InvoiceDetailRootClass
        
        let objWebViewVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_WebView) as! WebViewVC
        objWebViewVC.strUrl = objInvoice!.url!
        self.navigationController?.pushViewController(objWebViewVC, animated: true)
    }
    
    @IBAction func btnSideMenu_Click(_ sender: Any)
    {
        self.setupSideMenu()
    }
}
