//
//  InvoiceTableViewCell.swift
//  Myles Zero
//
//  Created by skpissay on 27/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblInvoiceDate: UILabel!
    @IBOutlet weak var lblInvoiceNo: UILabel!
    @IBOutlet weak var lblSubscriptioID: UILabel!
    @IBOutlet weak var lblNextPaymentDate: UILabel!
    @IBOutlet weak var lblTenureRemaining: UILabel!
    @IBOutlet weak var lblInvoiceAmount: UILabel!
    @IBOutlet weak var btnViewInvoice: UIButton!
    
    
    @IBOutlet weak var lblInvoiceType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnViewInvoice.layer.cornerRadius = 4.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
