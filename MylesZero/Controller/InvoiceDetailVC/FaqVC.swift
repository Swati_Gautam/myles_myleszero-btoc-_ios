//
//  FaqVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 07/01/21.
//  Copyright © 2021 skpissay. All rights reserved.
//

import UIKit
import WebKit
import SideMenu

class FaqVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    let customSideMenuManager = SideMenuManager()
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.callWebView()
    }
    
    // MARK: - Side Menu Function
    
    private func setupSideMenu()
    {
        // Define the menus
        
        guard let objSideMenuVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SideMenu) as? SideMenuVC else { return }
        
        let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objSideMenuVC)
        //let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: SideMenuVC)
        customSideMenuManager.menuLeftNavigationController = leftMenuNavigationController
        
        customSideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        //customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        customSideMenuManager.menuWidth = 300.0
        customSideMenuManager.menuFadeStatusBar = false
        
        //leftMenuNavigationController.statusBarEndAlpha = 0
        
        present(leftMenuNavigationController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let sideMenuNavigationController = segue.destination as? UISideMenuNavigationController
        {
            sideMenuNavigationController.sideMenuManager = customSideMenuManager
        }
    }
    
    func callWebView()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let url = URL(string: APIConstants.API_FAQ)//URL(string: "https://myleszero.mylescars.com/faq?q=app") //https://myleszero.mylescars.com/faq
        webView.load(URLRequest(url: url!))
        webView.allowsBackForwardNavigationGestures = true
        hud?.dismiss()
    }
    
    @IBAction func btnSideMenu_Click(_ sender: Any)
    {
        self.setupSideMenu()
    }
}
