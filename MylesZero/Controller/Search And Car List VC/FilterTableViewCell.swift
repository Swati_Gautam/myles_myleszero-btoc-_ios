//
//  FilterTableViewCell.swift
//  Myles Zero
//
//  Created by skpissay on 12/09/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    
    weak var cellFilterDelegate: FilterCarSubscriptionDelegate?

    @IBOutlet weak var imgFilterCheck: UIImageView!
    @IBOutlet weak var lblVariantNumber: UILabel!
    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var lblVariantName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnCheckBox_Click(_ sender: UIButton)
    {
        cellFilterDelegate?.didCheckboxClicked(sender.tag)
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "unselected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = false
        }
        else
        {
            sender.setImage(UIImage(named: "selected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

protocol FilterCarSubscriptionDelegate : class {
    func didFilterCarSubscribe(_ tag: Int)
    func didCheckboxClicked(_ index: Int)
    //func didPressMenuButton(_tag: Int )
}
