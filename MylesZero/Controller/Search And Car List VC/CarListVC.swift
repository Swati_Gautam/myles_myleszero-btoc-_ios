//
//  CarListVC.swift
//  Myles Zero
//
//  Created by skpissay on 09/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class CarListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, LoginProtocol, CarListSubscribeDelegate
{
    @IBOutlet weak var viewNoCarFound: UIView!
    var viewTransparent : UIView!
    var arrCarListInfo = NSArray()
    var filterArrCarListInfo = NSArray()

    var dictSelectedCarInfo : [String:AnyObject]?
    //var dictFilterListInfo : NSDictionary?
    //var arrFilterListInfo = NSArray()

    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var tblCarList: UITableView!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    var viewSort = SortView()
    var isUpgradeSelected : Bool = false
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var strDateForUpgradation : String?
    var dictUpgradeData : NSDictionary?

    var isFiltered : Bool =  false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        //print("self.dictFilterListInfo", self.dictFilterListInfo)
        self.viewNoCarFound.layer.borderColor = UIColor.lightGray.cgColor
        self.viewNoCarFound.layer.borderWidth = 1.0
        
        if self.arrCarListInfo.count > 0
        {
            self.tblCarList.isHidden = false
            self.tblCarList.delegate = self
            self.tblCarList.dataSource = self
            self.tblCarList.tableFooterView = UIView()
            
            self.viewNoCarFound.isHidden = true
            self.viewFilter.isHidden = false
        }
        else
        {
            self.tblCarList.isHidden = true
            self.viewFilter.isHidden = true
            self.viewNoCarFound.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.getStateList()
        if DefaultsValues.getBooleanValueFromUserDefaults_(forKey: kUserProfileDetails) == nil
        {
            let strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String
            
            if strClientID != nil
            {
                self.callApiToGetEmployeeDetail()
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func getStateList()
    {
        if let path = Bundle.main.path(forResource: "StateList", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let stateData = jsonResult["StateListData"] as? [Any] {
                    DefaultsValues.setCustomObjToUserDefaults(stateData, forKey: kStateListArray)
                    //print("State1234: ", DefaultsValues.getCustomObjFromUserDefaults_(forKey: kStateList))
                }
            } catch {
                // handle error
            }
        }
    }
    
    //MARK: - Get Employee Detail API
    
    func callApiToGetEmployeeDetail()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
        
        let strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String
        
        var dictLoginParam:[String:AnyObject]!
        dictLoginParam = ["clientid": strClientID] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.getEmployeeProfileDetail(APIConstants.API_GetEmployeeDetail, dataDict: dictLoginParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion: { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                if error != nil
                {
                    
                }
                else
                {
                    let dict = response as! NSDictionary
                    let strStatus = dict.value(forKey: "status") as! Int
                    if strStatus == 1
                    {
                        let arr = response.value(forKey: kResponseInfo) as! NSArray
                        let dict = (arr[0] as AnyObject) as! NSDictionary
                        let userProfileDetail  = UserProfileInfoResponse.init(fromDictionary:(dict) as! [String : Any])
                        DefaultsValues.setCustomObjToUserDefaults(userProfileDetail, forKey: kUserProfileDetails)
                    }
                }
            }
        })
    }
    
    func loadSortView()
    {
        let xHeight = CGFloat(300)
        let viewHeight = self.view.frame.size.height
        self.viewSort = Bundle.main.loadNibNamed("SortView", owner: nil, options: nil)![0] as! SortView
        self.viewSort.frame = CGRect(x: 0, y: viewHeight - xHeight, width: self.view.frame.size.width, height: xHeight)
        
        //viewSort = SortView(frame: CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: 300))
        
        let screenRect = UIScreen.main.bounds
        viewTransparent = UIView(frame: screenRect)
        //viewTransparent = UIView(frame: CGRect(x: 0, y: self.viewSort.frame.origin.y - 10, width: self.viewSort.frame.size.width, height: self.viewSort.frame.size.height + 30))
        viewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.70)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        
        self.viewSort.btnSortLowPrice.addTarget(self, action:#selector(self.btnSortLowPrice_Click(sender:)), for: .touchUpInside)
        
        self.viewSort.btnSortHighPrice.addTarget(self, action:#selector(self.btnSortHighPrice_Click(sender:)), for: .touchUpInside)
        
        self.viewSort.btnSortOldYear.addTarget(self, action:#selector(self.btnSortOldYear_Click(sender:)), for: .touchUpInside)
        
        self.viewSort.btnSortNewYear.addTarget(self, action:#selector(self.btnSortNewYear_Click(sender:)), for: .touchUpInside)
        
        viewTransparent.addGestureRecognizer(tap)
        self.view.addSubview(viewTransparent)
        self.view.addSubview(viewSort)
    }
    
    func hideSortView()
    {
        if self.viewTransparent.superview != nil
        {
            self.viewTransparent.removeFromSuperview()
        }
        if self.viewSort.superview != nil
        {
            self.viewSort.removeFromSuperview()
        }
    }
    
    //MARK: - Button Action
    
    @IBAction func btnChangeParam_Click(_ sender: Any)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBack_Click(_ sender: Any)
    {
        //self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSortBy_Click(_ sender: Any)
    {
        self.loadSortView()        
    }
    
    @objc func btnSortLowPrice_Click(sender:UIButton!)
    {
        arrCarListInfo = (arrCarListInfo as NSArray).sortedArray(using: [NSSortDescriptor(key: "Price", ascending: true)]) as! [[String:AnyObject]] as NSArray
        self.hideSortView()
        tblCarList.reloadData()
    }
    
    @objc func btnSortHighPrice_Click(sender:UIButton!)
    {
        arrCarListInfo = (arrCarListInfo as NSArray).sortedArray(using: [NSSortDescriptor(key: "Price", ascending: false)]) as! [[String:AnyObject]] as NSArray
        self.hideSortView()
        tblCarList.reloadData()

    }
    
    @objc func btnSortOldYear_Click(sender:UIButton!)
    {
        arrCarListInfo = (arrCarListInfo as NSArray).sortedArray(using: [NSSortDescriptor(key: "CarType", ascending: false)]) as! [[String:AnyObject]] as NSArray
        self.hideSortView()
        tblCarList.reloadData()
        
    }
    
    @objc func btnSortNewYear_Click(sender:UIButton!)
    {
        arrCarListInfo = (arrCarListInfo as NSArray).sortedArray(using: [NSSortDescriptor(key: "CarType", ascending: true)]) as! [[String:AnyObject]] as NSArray
        self.hideSortView()
        tblCarList.reloadData()
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        viewTransparent.removeFromSuperview()
        viewSort.removeFromSuperview()
        // handling code
    }
    
    func sortByPrice()
    {
        
    }
    // MARK: - Table View Delegate Methods
    
    @IBAction func btnFilterBy_Click(_ sender: Any)
    {
        let objFilterByVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_FilterBy) as! FilterByVC
        objFilterByVC.modalPresentationStyle = .fullScreen
        objFilterByVC.delegate = self
        //objFilterByVC.dictFilterListInfo = self.dictFilterListInfo
        self.present(objFilterByVC, animated: true, completion: nil)
    }
    
    //MARK: - UITableView Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return isFiltered ? self.filterArrCarListInfo.count:self.arrCarListInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idCarListCell"
        let cell:CarListTableVIewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CarListTableVIewCell?)!
        
        let dictSearchCarList  = SearchCarList.init(fromDictionary:(self.arrCarListInfo[indexPath.row]) as! [String : Any])
        //let dictCarList = self.arrCarListInfo[indexPath.row] as! NSDictionary
        DefaultsValues.setCustomObjToUserDefaults(dictSearchCarList, forKey: kSearchCarList)
        
        if isFiltered == true
        {
            let dictFilterData  = SearchCarList.init(fromDictionary:(self.filterArrCarListInfo[indexPath.row]) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(dictFilterData, forKey: "kFilterData")
        }
                
        let objCarList = isFiltered ? DefaultsValues.getCustomObjFromUserDefaults_(forKey: "kFilterData") as? SearchCarList : DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
        
        //let objCarList =  DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
        
        cell.lblCarName.text! = objCarList!.carModelName!
        let price : Int = (objCarList?.price)!
        let strAmount = price.withCommas()
        //let strAmount = dictCarList.value(forKey: "Price") as! NSNumber //Int(objCarList!.depositAmt!).withCommas()
        //let finalAmount = strAmount.doubleValue//Double(truncating: strAmount)
        //cell.lblTotalAmount.text! = String(format: "₹ %.2f",finalAmount)  //strAmount
        cell.lblTotalAmount.text! = String(format: "₹ %@", strAmount)
        cell.imgCarPic.sd_setImage(with: URL(string: (objCarList!.carImageWeb!)), placeholderImage: UIImage(named: kCarPlaceholder))
        
        //if objCarList!.carColorCode == nil || objCarList!.carColorCode == ""
        if objCarList!.carColorName == "Red" || objCarList!.carColorName == "red" || objCarList!.carColorName == "RED" || objCarList!.carColorName == "Super Red"
        {
            cell.lblCarColor.textColor = Constants.RED_COLOR //UIColor.init(hex: Constants.RED_HEX_COLOR)
            cell.imgCarColor.setImageColor(color: Constants.RED_COLOR)
        }
        else
        {
            cell.imgCarColor.setImageColor(color: UIColor.lightGray)
        }
        
        cell.lblCarType.text! = objCarList!.carType!
        /* if objCarList!.carType!.contains("Old")
        {
            cell.lblCarType.text! = "Used"
        }*/
        cell.lblDeliveryDays.text! = String(format: "%d days", Int(objCarList!.tentativeDeliveryDay)!)
        cell.lblCarColor.text! = objCarList!.carColorName!
        cell.lblSeater.text! = String(format: " %d seater", Int(objCarList!.seatingCapacity))
        cell.lblCarTransmissionType.text! = objCarList!.transmissiontype!
        cell.lblFuelType.text! = objCarList!.fuelTypeName!
        cell.lblVariantName.text! = objCarList!.carVariantName!
        
        cell.btnSubscribe.tag = indexPath.row
        
        cell.cellSubscribeDelegate = self
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    //MARK: - Delegate Method Of Button On TableViewCell
    
    func didCarSubscribeButton(_ index: Int)
    {
        let dictSearchCarList  = SearchCarList.init(fromDictionary:(self.arrCarListInfo[index]) as! [String : Any])
        DefaultsValues.setCustomObjToUserDefaults(dictSearchCarList, forKey: kSearchCarList)
        let objCarList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
        let strMylesZero : String = "true"
        DefaultsValues.setStringValueToUserDefaults(strMylesZero, forKey: kIsMylesZeroClicked)
        //if (!DefaultsValues.getBooleanValueFromUserDefaults_(forKey: IS_LOGIN))
        if (!UserDefaults.standard.bool(forKey: IS_LOGIN))
        {
            let storyboard: UIStoryboard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
            let editController = storyboard.instantiateViewController(withIdentifier: "LoginProcess") as! LoginProcess
            let navigationController: UINavigationController = UINavigationController.init(rootViewController: editController)
            editController.delegate = self
            editController.strOtherDestination = "0"
            editController.fromScreen = "From_Booking" //"From Booking"
            navigationController.viewControllers = [editController]
            //editController.modalPresentationStyle = .fullScreen
            navigationController.modalPresentationStyle = .fullScreen
            DispatchQueue.main.async {
                self.present(navigationController, animated: true, completion: nil)
            }            
        }
        else
        {
            let objCarSubscriptionDetailVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_CarSubscriptionDetail) as! CarSubscriptionDetailVC
            objCarSubscriptionDetailVC.objSearchCarDetail = objCarList
            objCarSubscriptionDetailVC.dictSelectedCarInfo = self.dictSelectedCarInfo
            if self.isUpgradeSelected == true
            {
                objCarSubscriptionDetailVC.objSubscriptionDetail = self.objSubscriptionDetail
            }
            self.navigationController?.pushViewController(objCarSubscriptionDetailVC, animated: true)
        }
        
        /*if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) != nil
        {
            let objCarSubscriptionDetailVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_CarSubscriptionDetail) as! CarSubscriptionDetailVC
            objCarSubscriptionDetailVC.objSearchCarDetail = objCarList
            objCarSubscriptionDetailVC.dictSelectedCarInfo = self.dictSelectedCarInfo
            if self.isUpgradeSelected == true
            {
                objCarSubscriptionDetailVC.objSubscriptionDetail = self.objSubscriptionDetail
            }
            self.navigationController?.pushViewController(objCarSubscriptionDetailVC, animated: true)
        }
        else
        {
            let objLoginVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: Constants.VC_Login) as! LoginVC
            self.navigationController?.pushViewController(objLoginVC, animated: true)
        }*/
    }
    
    //MARK: - Login Process Protocol Method
    
    func afterLoginChangeDestination(strDestination: String)
    {
        print("After Login Change Destination Method Called")
    }
}




extension CarListVC: FilterProtocol {
    
    func filterUsingVariant(keyName: String, value: String) {

        let predicate = NSPredicate(format: "SELF.%K contains[cd] %@", keyName, value)
        filterArrCarListInfo = arrCarListInfo.filtered(using: predicate) as NSArray
        
        if filterArrCarListInfo.count == 0 {
            isFiltered = false
        } else {
            isFiltered = true
        }
        
        tblCarList.reloadData()
    }    
}
