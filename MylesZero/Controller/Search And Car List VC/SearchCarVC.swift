//
//  SearchCarVC.swift
//  Myles Zero
//
//  Created by skpissay on 09/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit
import SideMenu
import JGProgressHUD
import Alamofire
import iOSDropDown

class SearchCarVC: UIViewController, CityNameDelegate
{
    @IBOutlet weak var txtCondition: DropDown!
    @IBOutlet weak var txtCity: DropDown!
    @IBOutlet weak var txtMonthlyKmUsage: DropDown!
    @IBOutlet weak var txtSubscriptionTenure: DropDown!
    //@IBOutlet weak var lblMonthlyUsage: UILabel!
    //@IBOutlet weak var lblCarCondition: UILabel!
    //@IBOutlet weak var lblSubscriptionTenure: UILabel!
    //@IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    let customSideMenuManager = SideMenuManager()
    var objSearchCarFilter : SearchCarFilter?
    var searchCarParam:[String:AnyObject]!
    var strFinalDuration : String?
    var strFinalKMs : String?
    var valueForMonth : Int?
    var arrCondition = NSArray()
    var arrCarType = NSArray()
    var arrKm = NSArray()
    
    //var arrCondition = NSArray()
    var arrTenure = NSArray()
    var arrMonthlyUsage = NSArray()
    //var defaults:UserDefaults?
    
    //let arrCondition : NSArray = ["--Select--","6 Months", "12 Months", "18 Months", "24 Months", "30 Months", "36 Months", "42 Months", "48 Months", "54 Months", "60 Months"]
    
    var cityID : Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print("Thanks URL: ", APIConstants.PAYMENT_MAIN_URL.lowercased()+"appthanks".lowercased())
        //print("Access Token: ", DefaultsValues.getStringValueFromUserDefaults_(forKey: K_AccessToken))
        //print("Access Token: ", DefaultsValues.getStringValueFromUserDefaults_(forKey: K_SessionID))
        //K_SessionID
        
        // Do any additional setup after loading the view.
        self.btnSearch.layer.cornerRadius = 4.0
        
        let tapCityLabel = UITapGestureRecognizer(target: self, action: #selector(SearchCarVC.tapCity))
        //self.lblCity.isUserInteractionEnabled = true
        //self.lblCity.addGestureRecognizer(tapCityLabel)
        self.txtCity.isUserInteractionEnabled = true
        self.txtCity.addGestureRecognizer(tapCityLabel)
        
        let tapSubscriptionTenureLabel = UITapGestureRecognizer(target: self, action: #selector(SearchCarVC.tapSubscription))
        self.txtSubscriptionTenure.isUserInteractionEnabled = true
        self.txtSubscriptionTenure.addGestureRecognizer(tapSubscriptionTenureLabel)
        //self.lblSubscriptionTenure.isUserInteractionEnabled = true
        //self.lblSubscriptionTenure.addGestureRecognizer(tapSubscriptionTenureLabel)
        
        let tapConditionLabel = UITapGestureRecognizer(target: self, action: #selector(SearchCarVC.tapCondition))
        self.txtCondition.isUserInteractionEnabled = true
        self.txtCondition.addGestureRecognizer(tapConditionLabel)
        //self.lblCarCondition.isUserInteractionEnabled = true
        //self.lblCarCondition.addGestureRecognizer(tapConditionLabel)
        
        let tapMonthlyLabel = UITapGestureRecognizer(target: self, action: #selector(SearchCarVC.tapMonthly))
        self.txtMonthlyKmUsage.isUserInteractionEnabled = true
        self.txtMonthlyKmUsage.addGestureRecognizer(tapMonthlyLabel)
        
        //self.lblMonthlyUsage.isUserInteractionEnabled = true
        //self.lblMonthlyUsage.addGestureRecognizer(tapMonthlyLabel)
        //self.fetchAccessToken()
        //self.fetchCityListWebService()
        
        
        self.strFinalKMs = "1000"
        self.txtMonthlyKmUsage.text = String(format: "%@ KMs", self.strFinalKMs!)
        self.txtMonthlyKmUsage.textColor = UIColor.black
        self.txtCondition.text = "All"
        self.txtCondition.textColor = UIColor.black
        self.valueForMonth = 60
        self.strFinalDuration = "60"
        self.txtSubscriptionTenure.text = String(format: "%@ Month", strFinalDuration!)
        self.txtSubscriptionTenure.textColor = UIColor.black
        self.navigationController?.navigationBar.isHidden = true        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.getStateList()
        self.fetchNewAccessToken()
        
        /*if UserDefaults.standard.bool(forKey: IS_LOGIN) == true
        {
            self.fetchNewAccessToken()
        }
        else if UserDefaults.standard.bool(forKey: IS_LOGIN) == false
        {
            self.fetchAccessToken()
        }*/
        
        //let isLogout = DefaultsValues.getBooleanValueFromUserDefaults_(forKey: kIsLogout)
        
        
        /*if isLogout == true
        {
            self.lblCity.text = "City"
            self.lblCity.textColor = Constants.LIGHT_GREY_COLOR
            self.lblMonthlyUsage.text = "Monthly KM Usage"
            self.lblMonthlyUsage.textColor = Constants.LIGHT_GREY_COLOR
            self.lblCarCondition.text = "Condition"
            self.lblCarCondition.textColor = Constants.LIGHT_GREY_COLOR
            self.lblSubscriptionTenure.text = "Subscription Tenure"
            self.lblSubscriptionTenure.textColor = Constants.LIGHT_GREY_COLOR
        }*/
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setSubscriptionTenure()
    {
        self.txtSubscriptionTenure.optionArray = [String]()
        for i in 0..<self.arrTenure.count
        {
            let strTenure = self.arrTenure[i]  as! String
            self.txtSubscriptionTenure.optionArray.append(strTenure)
        }
        self.txtSubscriptionTenure.arrowColor = UIColor.black
        self.txtSubscriptionTenure.selectedRowColor = Constants.RED_COLOR
        self.txtSubscriptionTenure.checkMarkEnabled = false
        self.txtSubscriptionTenure.didSelect{(selectedText , index ,id) in
           
        }
    }
    
    func setMonthlyUsage()
    {
        self.txtMonthlyKmUsage.optionArray = [String]()
        for i in 0..<self.arrMonthlyUsage.count
        {
            let strTenure = self.arrMonthlyUsage[i]  as! String
            self.txtMonthlyKmUsage.optionArray.append(strTenure)
        }
        self.txtMonthlyKmUsage.arrowColor = UIColor.black
        self.txtMonthlyKmUsage.selectedRowColor = Constants.RED_COLOR
        self.txtMonthlyKmUsage.checkMarkEnabled = false
        self.txtMonthlyKmUsage.didSelect{(selectedText , index ,id) in
        }
    }
    
    func setCarCondition()
    {
        self.txtCondition.optionArray = [String]()
        for i in 0..<self.arrCondition.count
        {
            let strTenure = self.arrCondition[i]  as! String
            self.txtCondition.optionArray.append(strTenure)
        }
        self.txtCondition.arrowColor = UIColor.black
        self.txtCondition.selectedRowColor = Constants.RED_COLOR
        self.txtCondition.checkMarkEnabled = false
        self.txtCondition.didSelect{(selectedText , index ,id) in
        }
    }
    
    /*func setCarCondition()
    {
        print("arrCondition: ", self.arrCondition)
        for i in 0..<self.arrCondition.count
        {
            let strTenure = self.arrCondition[i]  as! String
            self.txtCondition.optionArray.append(strTenure)
        }
        self.txtCondition.arrowColor = UIColor.black
        self.txtCondition.selectedRowColor = Constants.RED_COLOR
        self.txtCondition.checkMarkEnabled = false
        self.txtCondition.didSelect{(selectedText , index ,id) in
        }
    }*/
    
    //MARK: - Webservices
    
    func fetchCityListWebService()
    {
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        
         APIFunctions.sharedInstance.fetchCityListAPI(APIConstants.API_FetchCityList,dataDict: nil, dataHeaders: (dictHeaders as! HTTPHeaders),completion:{ (response, error) in
         
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    self.arrTenure = NSArray()
                    self.arrMonthlyUsage = NSArray()
                    self.arrCondition = NSArray()
                    
                    let arrCity = response.value(forKey: "City") as? NSArray
                    let arrCarType = response.value(forKey: "CarType") as! NSArray
                    let arrKm = response.value(forKey: "Km") as! NSArray
                    let arrTenure = response.value(forKey: "Tenure") as! NSArray
                    print("arrCity: ", arrCity)
                    self.arrTenure = arrTenure
                    self.arrMonthlyUsage = arrKm
                    self.arrCondition = arrCarType
                    
                    self.setSubscriptionTenure()
                    self.setCarCondition()
                    self.setMonthlyUsage()
                    
                    DefaultsValues.setArrayValueFromUserDefaults(arrCity, forKey: kCityListArray)
                    DefaultsValues.setArrayValueFromUserDefaults((arrCarType as NSArray), forKey: kCarTypeList)
                    DefaultsValues.setArrayValueFromUserDefaults((arrTenure as NSArray), forKey: kTenureList)
                    DefaultsValues.setArrayValueFromUserDefaults((arrKm as NSArray), forKey: kKmList)
                    
                    
                }
            }
         })
    }
    
    /*func fetchCityListWebService()
    {
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        
         APIFunctions.sharedInstance.fetchCityListAPI(APIConstants.API_FetchCityList,dataDict: nil, dataHeaders: (dictHeaders as! HTTPHeaders),completion:{ (response, error) in
         
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    //let JsonLogin = response as AnyObject
                    let arrResponse = response.value(forKey: "response") as! NSArray
                    print("arrResponse: ", arrResponse)
                    DefaultsValues.setArrayValueFromUserDefaults((arrResponse as NSArray), forKey: kCityListArray)
                }
            }
         })
    }*/
    
    func fetchAccessToken()
    {
        var sessionID = String()
        sessionID = CommonMethodsMylesZero.generateSessionID()!
        //let CustomerID = "-99"
        
        let md5Data = CommonMethodsMylesZero.MD5(string:"IOS@@MYLESCAR")
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        
        //[DEFAULTS boolForKey:IS_LOGIN] ? [DEFAULTS objectForKey:KUSER_ID]:@"-99"
        
        let dictAccessToken = ["userID" : "IOS",
                               "password" : md5Hex,
                               "IPRestriction" : false,
                               "sessionID" : sessionID,
                               "customerID" : UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"] as [String : AnyObject] //CustomerID
        
        APIFunctions.sharedInstance.getAccessTokenAPI(APIConstants.API_GetAccessToken, dataDict: dictAccessToken) { (response,error) in
            DispatchQueue.main.async {
                let JSON = response as AnyObject
                //fetchAccessToken
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "accessToken") as? String, forKey: kAccessToken)
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "sessionID") as? String, forKey: kSessionID)
                //let strAccessToken = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAccessToken)!
                //let strSessionID = DefaultsValues.getStringValueFromUserDefaults_(forKey: kSessionID)!
                
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "accessToken") as? String, forKey: K_AccessToken)
                DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "sessionID") as? String, forKey: K_SessionID)
                
                let strAccessToken = DefaultsValues.getStringValueFromUserDefaults_(forKey: K_AccessToken)!
                let strSessionID = DefaultsValues.getStringValueFromUserDefaults_(forKey: K_SessionID)!
                let customerID = UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"
                
                let dictHeaders = CommonMethodsMylesZero.getHeaderValues(sessionId: strSessionID , customerId: customerID as! String, hasValue: strAccessToken )! as [String:String]
                
                //let dictHeaders = CommonMethodsMylesZero.getHeaderValues(sessionId: strSessionID, customerId: kCustomerID, hasValue: strAccessToken)! as [String:String]
                kDictHeaders = dictHeaders
                DefaultsValues.setDictionaryValueToUserDefaults(kDictHeaders as NSDictionary, forKey: kHttpHeaders)
                self.fetchCityListWebService()
            }
        }
    }
    
    func fetchNewAccessToken()
    {
        //var sessionID = String()
        //sessionID = CommonMethodsMylesZero.generateSessionID()!
        //let CustomerID = "-99"
        
        //let md5Data = CommonMethodsMylesZero.MD5(string:"IOS@@MYLESCAR")
        //let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        
        /*let dictAccessToken = ["userID" : "IOS",
                               "password" : md5Hex,
                               "IPRestriction" : false,
                               "sessionID" : sessionID,
                               "customerID" : CustomerID] as [String : AnyObject]*/
        
        //APIFunctions.sharedInstance.getAccessTokenAPI(APIConstants.API_GetAccessToken, dataDict: dictAccessToken) { (response,error) in
            DispatchQueue.main.async {
                //let JSON = response as AnyObject
                //DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "accessToken") as? String, forKey: kAccessToken)
                //DefaultsValues.setStringValueToUserDefaults(JSON.value(forKey: "sessionID") as? String, forKey: kSessionID)
                //let strAccessToken = DefaultsValues.getStringValueFromUserDefaults_(forKey: kAccessToken)!
                //let strSessionID = DefaultsValues.getStringValueFromUserDefaults_(forKey: kSessionID)!
                
                let strAccessToken =  UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : ""//DefaultsValues.getStringValueFromUserDefaults_(forKey: K_AccessToken)!
                let strSessionID = UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "" //DefaultsValues.getStringValueFromUserDefaults_(forKey: K_SessionID)!
                let customerID = UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"
                
                /*let newDictHeaders = ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]*/
                
                let dictHeaders = CommonMethodsMylesZero.getHeaderValues(sessionId: strSessionID as! String, customerId: customerID as! String, hasValue: strAccessToken as! String)! as [String:String]
                kDictHeaders = dictHeaders
                DefaultsValues.setDictionaryValueToUserDefaults(kDictHeaders as NSDictionary, forKey: kHttpHeaders)
                self.fetchCityListWebService()
            }
        //}
    }
    
    func getStateList()
    {
        if let path = Bundle.main.path(forResource: "StateList", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let stateData = jsonResult["StateListData"] as? [Any] {
                    DefaultsValues.setCustomObjToUserDefaults(stateData, forKey: kStateListArray)
                    //print("State1234: ", DefaultsValues.getCustomObjFromUserDefaults_(forKey: kStateList))
                }
            } catch {
                // handle error
            }
        }
    }
    
//    func getLast6Month() -> Date? {
//        return Calendar.current.date(byAdding: .month, value: 6, to: Date)
//    }
    
    func searchCarWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let strFromDate = CommonMethodsMylesZero.getCurrentDate()
        
        //let nextMonth = Calendar.current.date(byAdding: .month, value: 6, to: Date())
        let nextMonth = Calendar.current.date(byAdding: .month, value: self.valueForMonth!, to: Date())
        //print("nextMonth: ", nextMonth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let strNextMonth = formatter.string(from: nextMonth!)
            
        searchCarParam = ["Cityid" : Int(cityID) as AnyObject,
                          "Duration": Int(strFinalDuration!)! as AnyObject, //int
                          "FromDate": strFromDate as AnyObject, //"09/10/19",
                          "ToDate": strNextMonth as AnyObject, //"09/10/20",
                          "ClientCoId": kCustomerCoID as AnyObject,//int
                          "km": Int(strFinalKMs!)! as AnyObject, //int
                          "CarType": self.txtCondition.text! as AnyObject] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.searchCarForSubscribeRequest(APIConstants.API_SearchCarForSubscribe, dataDict: searchCarParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    //print("Json Login: ", response)
                    let Json = response.value(forKey: "response") as? NSDictionary
                    if Json != nil
                    {
                        if let arrCarList = Json?.value(forKey: "Cars") as? NSArray, arrCarList.count > 0
                        {
                            //let arrCarList = Json.value(forKey: "Cars") as! NSArray
                            //print("cars1: ", arrCarList )
                            let dictFilterList = Json?.value(forKey: "Fitlers") as! NSDictionary
                            //kFilterData
                            self.objSearchCarFilter?.fuelname = dictFilterList.value(forKey: "fuelname") as? [String]
                            
                            DefaultsValues.setDictionaryValueToUserDefaults(dictFilterList, forKey: kFilterData)
                            DefaultsValues.setStringValueToUserDefaults(self.strFinalDuration, forKey: kTenure)
                            
                            let objCarListVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_CarList) as! CarListVC
                            objCarListVC.arrCarListInfo = arrCarList
                            objCarListVC.dictSelectedCarInfo = self.searchCarParam
                            //objCarListVC.dictFilterListInfo = dictFilterList
                            APIFunctions.dictGlobalCarFilter = dictFilterList
                            DefaultsValues.setDictionaryValueToUserDefaults(self.searchCarParam as NSDictionary?, forKey: kSearchParamKey)
                            /*let navigationController: UINavigationController = UINavigationController.init(rootViewController: objCarListVC)
                             navigationController.viewControllers = [objCarListVC]
                             navigationController.modalPresentationStyle = .fullScreen
                             objCarListVC.modalPresentationStyle = .fullScreen
                             navigationController.navigationBar.isHidden = true
                             self.present(navigationController, animated: true, completion: nil)*/
                            
                            //let objCarListVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_CarList) as! CarListVC
                            self.navigationController?.pushViewController(objCarListVC, animated: true)
                        }
                        else
                        {
                            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "No Car is available for this location. Please try for another.", strAction1: "OK", viewController: self)
                        }
                    }
                    else
                    {
                        //self.fetchNewAccessToken()
                        /*var appDelegate : AppDelegate?
                        AppDelegate.getSharedInstance()
                        appDelegate?.getAccessToken(completion: {
                            self.searchCarWebService()
                        })*/
                        
                        //AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                        //[appDelegate getAccessTokenWithCompletion:^{
                            //[homeSelf searchCar];
                            //}];
                    }
                    

                }
            }
        })
    }
    
    // MARK: - Button Action
    
    @IBAction func btnSelectCity_Click(_ sender: Any)
    {
        let objSearchCityVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchTableVC) as! SearchCityVC
        objSearchCityVC.selectCityNameDelegate = self
        objSearchCityVC.modalPresentationStyle = .fullScreen
        self.present(objSearchCityVC, animated: true, completion: nil)
        //self.navigationController?.pushViewController(objSearchCityTableViewController, animated: true)
    }
    
    /*@IBAction func btnSelectSubscriptionTenure_Click(_ sender: Any) {
        //self.showActionSheetForSubscriptionTenure()
    }
    
    @IBAction func btnSelectCarCondition_Click(_ sender: Any) {
        //self.showActionSheetForCarCondition()
    }
    
    @IBAction func btnSelectMonthlyUsage_Click(_ sender: Any)
    {
        //self.showActionSheetForMonthlyUsage()
    }*/
    
    @IBAction func btnSearchCar_Click(_ sender: Any)
    {
        if self.txtCity.text == "City"
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the city", strAction1: "OK", viewController: self)
        }
        else if self.txtSubscriptionTenure.text == "Subscription Tenure"
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the subscription tenure", strAction1: "OK", viewController: self)
        }
        else if self.txtCondition.text == "Condition"
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the car condition", strAction1: "OK", viewController: self)
        }
        else if self.txtMonthlyKmUsage.text == "Monthly KM Usage"
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the monthly usage", strAction1: "OK", viewController: self)
        }
        else
        {
            self.searchCarWebService()
        }
    }
    
    @IBAction func btnSideMenu_Click(_ sender: Any)
    {
        setupSideMenu()
    }
    
    // MARK: - Side Menu Function
    
    private func setupSideMenu()
    {
        // Define the menus
        
        guard let objSideMenuVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SideMenu) as? SideMenuVC else { return }
        
        let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objSideMenuVC)
        //let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: SideMenuVC)
        customSideMenuManager.menuLeftNavigationController = leftMenuNavigationController
        
        //SideMenuManager.default.menuLeftNavigationController = storyboard?.instantiateViewController(withIdentifier: "idLeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        //SideMenuManager.default.menuAddPanGestureToPresent(toView: navigationController!.navigationBar)
        //SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view)
        
        //let navigationController : UINavigationController = UINavigationController.init(rootViewController: leftMenuNavigationController)
        //customSideMenuManager.menuAddPanGestureToPresent(toView: navigationController.navigationBar)
        
        customSideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        //customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        customSideMenuManager.menuWidth = 300.0
        customSideMenuManager.menuFadeStatusBar = false
        
        //leftMenuNavigationController.statusBarEndAlpha = 0
        
        present(leftMenuNavigationController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let sideMenuNavigationController = segue.destination as? UISideMenuNavigationController
        {
            sideMenuNavigationController.sideMenuManager = customSideMenuManager
        }
    }
    
    @objc func tapCity(sender:UITapGestureRecognizer) {
        let objSearchCityVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchTableVC) as! SearchCityVC
        objSearchCityVC.selectCityNameDelegate = self
        self.present(objSearchCityVC, animated: true, completion: nil)
    }
    
    @objc func tapSubscription(sender:UITapGestureRecognizer) {
        self.setSubscriptionTenure()
        //self.showActionSheetForSubscriptionTenure()
    }
    
    @objc func tapCondition(sender:UITapGestureRecognizer) {
        self.setCarCondition()
        //self.showActionSheetForCarCondition()
    }
    
    @objc func tapMonthly(sender:UITapGestureRecognizer) {
        self.setMonthlyUsage()
        //self.showActionSheetForMonthlyUsage()
    }
    
    //MAK:- Delegate Method Call
    
     func didGetCityName(strCityName: String, strCityID: String)
     {
        print("strCityName: ", strCityName)
        print("strCityName: ", strCityID)
        self.txtCity.text! = strCityName
        //self.lblCity.text! = strCityName
        
        cityID = Int(strCityID)!
        //self.lblCity.textColor = UIColor.black
        self.txtCity.textColor = UIColor.black
    }
}

extension SearchCarVC: UISideMenuNavigationControllerDelegate
{    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
}
