//
//  SearchCityVC.swift
//  Myles Zero
//
//  Created by skpissay on 05/09/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class SearchCityVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    weak var selectCityNameDelegate: CityNameDelegate?

    @IBOutlet weak var tblSearchCity: UITableView!
    
    let arrData = ["One", "Two", "Three","Twenty-One"]
    //var filteredTableData = [String]()
    var resultSearchController = UISearchController()
    var arrCityList = NSArray()
    var arrFilteredCity = NSArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if (DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray) != nil)
        {
            self.arrCityList = DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray)!
        }
        else
        {
            return
        }
        
        /*if DefaultsValues.getArrayValueFromUserDefaults_(forKey:kCityListArray) != nil
        {
            
        }
        else
        {
            //CommonMethodsMylesZero.showAlertController2(strTitle: "Server Error", strMsg: "No response from server", strAction1: "OK", viewController: self)
        }*/
                
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            tblSearchCity.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        // Reload the table
        tblSearchCity.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnCancel_Click(_ sender: Any)
    {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Search Controller Method
    
    /*func updateSearchResults(for searchController: UISearchController)
    {
        //filteredTableData.removeAll(keepingCapacity: false)
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (arrCityList as NSArray).filtered(using: searchPredicate)
        arrFilteredCity = array as NSArray
//        let array = (arrData as NSArray).filtered(using: searchPredicate)
//        filteredTableData = array as! [String]
        
        self.tblSearchCity.reloadData()
    }*/
    
    // MARK:-  UISearchResultsUpdating delegate function
    
    func updateSearchResults(for searchController: UISearchController)
    {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        
        let searchPredicate: NSPredicate
        searchPredicate = NSPredicate(format: "CityName CONTAINS[C] %@", searchString)
        
        self.arrFilteredCity = (self.arrCityList as NSArray).filtered(using: searchPredicate) as NSArray
        
        print ("array = \(self.arrFilteredCity)")
        
        // Reload the tableview.
        self.tblSearchCity.reloadData()
    }
    
    // MARK:- Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the number of rows
        if  (resultSearchController.isActive) {
            return arrFilteredCity.count
        } else {
            return arrCityList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idSearchCityCell"
        let cell:SearchCityTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SearchCityTableViewCell?)!
        //let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCityCell", for: indexPath)
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        var objCityList : CityListMylesZeroResponse?
        
        if (resultSearchController.isActive) {
            //cell.lblCityName.text! = filteredTableData[indexPath.row]
            objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrFilteredCity[indexPath.row] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse
            cell.lblCityName.text! = objCityList!.cityName!
            return cell
        }
        else {
            //cell.lblCityName.text! = arrData[indexPath.row]
            objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[indexPath.row] as! NSDictionary) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
            objCityList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse
            cell.lblCityName.text! = objCityList!.cityName!
            return cell
        }        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var strCityName : String = ""
        var objCityList : CityListMylesZeroResponse?
        
        if (resultSearchController.isActive)
        {
            //strCityName = filteredTableData[indexPath.row]
            objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrFilteredCity[indexPath.row] as! NSDictionary) as! [String : Any])
        }
        else
        {
            objCityList = CityListMylesZeroResponse.init(fromDictionary: (arrCityList[indexPath.row] as! NSDictionary) as! [String : Any])
            
        }
        DefaultsValues.setCustomObjToUserDefaults(objCityList, forKey: kCityList)
        objCityList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse
        strCityName = objCityList!.cityName!
        selectCityNameDelegate?.didGetCityName(strCityName: strCityName, strCityID: objCityList!.cityId)
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

protocol CityNameDelegate : class {
    func didGetCityName(strCityName: String, strCityID: String)
}
