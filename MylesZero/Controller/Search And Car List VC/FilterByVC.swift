//
//  FilterByVC.swift
//  Myles Zero
//
//  Created by skpissay on 26/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

protocol FilterProtocol {
    func filterUsingVariant(keyName: String, value: String)
}

class FilterByVC: UIViewController, UITableViewDelegate, UITableViewDataSource, FilterCarSubscriptionDelegate {
    
    @IBOutlet weak var btnTransmissionType: UIButton!
    @IBOutlet weak var btnCarType: UIButton!
    @IBOutlet weak var btnFuelType: UIButton!
    @IBOutlet weak var btnCarCategory: UIButton!
    @IBOutlet weak var btnModel: UIButton!
    @IBOutlet weak var btnVariants: UIButton!
    
    var selectedCells:[Int] = []
    var strFilterName : String?
    
    var delegate: FilterProtocol?
    
    var objSearchCarFilter : SearchCarFilter?
    var objFilter : SearchCarFilter?
    //var dictFilterListInfo : NSDictionary?

    @IBOutlet weak var tblFilterView: UITableView!
    var arrVariants = NSArray()
    var arrModelList = NSArray()
    var arrFuelType = NSArray()
    var arrCarModel = NSArray()
    var arrSeatingCapacity = NSArray()
    var arrCarCompany = NSArray()
    var arrCarType = NSArray()
    var arrTransmissionType = NSArray()
    var arrCarCategory = NSArray()
    
    var strFilterValue = String()
    var strFilterKey = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //print("Filter Array: ", DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kFilterData)!)
        let dictFilterData = SearchCarFilter.init(fromDictionary: DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kFilterData)! as! [String:Any])
        DefaultsValues.setCustomObjToUserDefaults(dictFilterData, forKey: kAllFilterData)
        self.objFilter = (DefaultsValues.getCustomObjFromUserDefaults_(forKey: kAllFilterData) as! SearchCarFilter)
        
        //self.objFilter?.variant = APIFunctions.dictGlobalCarFilter!.value(forKey: "Variant") as? [SearchCarVariant]
        
        self.strFilterName = "Brand" //Variant
        self.setUpTableView(strTitle: self.strFilterName!)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    /*func createSliderForPrice()
    {
        let slider = MultiSlider()
        slider.minimumValue = 1    // default is 0.0
        slider.maximumValue = 5    // default is 1.0

        slider.value = [1, 4.5, 5]

        //slider.addTarget(self, action: #selector(sliderChanged(_:)), for: .valueChanged) // continuous changes
        //slider.addTarget(self, action: #selector(sliderDragEnded(_:)), for: . touchUpInside) // sent when drag ends
        
        slider.outerTrackColor = .lightGray
        
        slider.orientation = .horizontal // default is .vertical
        slider.isVertical = false // same effect, but accessible from Interface Builder
        slider.valueLabelPosition = .left // .notAnAttribute = don't show labels
        slider.isValueLabelRelative = true // show differences between thumbs instead of absolute values
        slider.valueLabelFormatter.positiveSuffix = " 𝞵s"
        //slider.valueLabelColor = .green
        //slider.valueLabelFont = someFont
        slider.snapStepSize = 0.5 // default is 0.0, i.e. don't snap
        slider.isHapticSnap = false
        
        // add images at the ends of the slider:
        slider.minimumImage = UIImage(named: "clown")
        slider.maximumImage = UIImage(named: "cloud")

        // change image for all thumbs:
        slider.thumbImage = UIImage(named: "balloon")

        // or let each thumb have a different image:
        slider.thumbViews[0].image = UIImage(named: "ball")
        slider.thumbViews[1].image = UIImage(named: "club")
        // allow thumbs to overlap:
        slider.keepsDistanceBetweenThumbs = false

        // make thumbs keep a greater distance from each other (default = half the thumb size):
        slider.distanceBetweenThumbs = 3.14
        
        slider.disabledThumbIndices = [1, 3]
    }*/
    
//    @objc func sliderChanged(slider: MultiSlider) {
//        print("thumb \(slider.draggedThumbIndex) moved")
//        print("now thumbs are at \(slider.value)") // e.g., [1.0, 4.5, 5.0]
//    }
//
//    @objc func sliderDragEnded(slider: MultiSlider) {
//        print("thumb \(slider.draggedThumbIndex) moved")
//        print("now thumbs are at \(slider.value)") // e.g., [1.0, 4.5, 5.0]
//    }
    func setFilterData()
    {
        DefaultsValues.setCustomObjToUserDefaults(self.objSearchCarFilter?.variant, forKey: "variant")
    }
    
   //MARK: - Filter Delegate Methods
    
    func didFilterCarSubscribe(_ tag: Int) {
        
    }
    
    func didCheckboxClicked(_ index: Int)
    {
        print("index: ", index)
        if self.strFilterName == "Brand"
        {
            if self.arrCarCompany.count > 0
            {
                /*let objVariants = self.arrVariants[indexPath.row] as! SearchCarVariant
                 strFilterValue = objVariants.value! as String*/
                strFilterValue = self.arrCarCompany[index] as! String
                strFilterKey = "CarCompany"
                //self.selectedCells.append(indexPath.row)
            }
        }
        else if self.strFilterName == "Model"
        {
            if self.arrCarCompany.count > 0
            {
                let dict = self.arrModelList[index] as? NSDictionary
                strFilterValue = (dict?.value(forKey: "Value") as? String)!
                //strFilterValue = self.arrModelList[index] as! String
                strFilterKey = "Model" //"FuelTypeName"
                //cell.lblVariantNumber.text! = objVariants.key!
                //self.selectedCells.append(indexPath.row)
            }
            
        }
        else if self.strFilterName == "FuelType"
        {
            if self.arrFuelType.count > 0
            {
                strFilterValue = self.arrFuelType[index] as! String
                strFilterKey = "fuelname" //"FuelTypeName"
                //cell.lblVariantNumber.text! = objVariants.key!
                //self.selectedCells.append(indexPath.row)
            }
        }
        else if self.strFilterName == "Car Type"
        {
            if self.arrCarType.count > 0
            {
                strFilterValue = self.arrCarType[index] as! String
                strFilterKey = "cartype"
                //self.selectedCells.append(indexPath.row)
            }
        }
        else if self.strFilterName == "Transmission"
        {
            if self.arrFuelType.count > 0
            {
                strFilterValue = self.arrFuelType[index] as! String
                strFilterKey = "Transmissiontype"
                //self.selectedCells.append(indexPath.row)
            }
        }
        else if self.strFilterName == "Car Category"
        {
            if self.arrFuelType.count > 0
            {
                strFilterValue = self.arrFuelType[index] as! String
                strFilterKey = "carcategory"
                //self.selectedCells.append(indexPath.row)
            }
        }
        self.tblFilterView.reloadData()
    }
    
    //MARK: - UITableView Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 51.0
        //return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.strFilterName == "Brand"
        {
            return arrCarCompany.count
        }
        else if self.strFilterName == "Model"
        {
            return arrModelList.count
        }
        else if self.strFilterName == "FuelType"
        {
            return arrFuelType.count
        }
        else if self.strFilterName == "Car Type"
        {
            return arrCarType.count
        }
        else if self.strFilterName == "Transmission"
        {
            return arrTransmissionType.count
        }
        else if self.strFilterName == "Car Category"
        {
            return arrCarCategory.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idFilterCell"
        let cell:FilterTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! FilterTableViewCell?)!
        cell.selectionStyle = .none
        
        if self.strFilterName == "Brand"
        {
            if self.arrCarCompany.count > 0
            {
            cell.lblVariantNumber.isHidden = true
            cell.lblVariantName.text! = self.arrCarCompany[indexPath.row] as! String
//            let objVariants = self.arrVariants[indexPath.row] as! SearchCarVariant
//            cell.lblVariantName.text! = objVariants.value!
//            cell.lblVariantNumber.text! = objVariants.key!
            }
        }
        else if self.strFilterName == "Model"
        {
            if self.arrModelList.count > 0
            {
            cell.lblVariantNumber.isHidden = true
            let dict = self.arrModelList[indexPath.row] as! NSDictionary
            cell.lblVariantName.text! = dict.value(forKey: "Value") as! String //self.arrModelList[indexPath.row] as! String
            //cell.lblVariantNumber.text! = objVariants.key!
            }
        }
        else if self.strFilterName == "FuelType"
        {
            if self.arrFuelType.count > 0
            {
            cell.lblVariantNumber.isHidden = true
            cell.lblVariantName.text! = self.arrFuelType[indexPath.row] as! String
            //cell.lblVariantNumber.text! = objVariants.key!
            }
        }
        else if self.strFilterName == "Car Type"
        {
            if self.arrCarType.count > 0
            {
            cell.lblVariantNumber.isHidden = true
            cell.lblVariantName.text! = self.arrCarType[indexPath.row] as! String
            let strCarType = self.arrCarType[indexPath.row] as! String
            }
            /*if strCarType.contains("Old")
            {
                cell.lblVariantName.text! = "Used"
            }*/
            //cell.lblVariantNumber.text! = objVariants.key!
        }
        else if self.strFilterName == "Transmission"
        {
            if self.arrTransmissionType.count > 0
            {
            cell.lblVariantNumber.isHidden = true
            cell.lblVariantName.text! = self.arrTransmissionType[indexPath.row] as! String
            //cell.lblVariantNumber.text! = objVariants.key!
            }
        }
        else if self.strFilterName == "Car Category"
        {
            if self.arrCarCategory.count > 0
            {
            //self.createSliderForPrice()
            cell.lblVariantNumber.isHidden = true
            cell.lblVariantName.text! = self.arrCarCategory[indexPath.row] as! String
            //cell.lblVariantNumber.text! = objVariants.key!
            }
        }
        
        //cell.imgFilterCheck.isHidden = true
        cell.cellFilterDelegate = self
        cell.btnCheckbox.tag = indexPath.row
        //cell.accessoryType = self.selectedCells.contains(indexPath.row) ? .checkmark : .none
        cell.tintColor = Constants.RED_COLOR
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       //let cell = self.tblFilterView.cellForRow(at: indexPath) as! FilterTableViewCell
       //cell.imgFilterCheck.isHidden = false
        
        if self.strFilterName == "Brand"
        {
            /*let objVariants = self.arrVariants[indexPath.row] as! SearchCarVariant
            strFilterValue = objVariants.value! as String*/
            if self.arrCarCompany.count > 0
            {
                strFilterValue = self.arrCarCompany[indexPath.row] as! String
                strFilterKey = "CarCompany"
            }
            
            //self.selectedCells.append(indexPath.row)
        }
        else if self.strFilterName == "Model"
        {
            /*let objVariants = self.arrVariants[indexPath.row] as! SearchCarVariant
             strFilterValue = objVariants.value! as String*/
            if self.arrModelList.count > 0
            {
                let dict = self.arrModelList[indexPath.row] as! NSDictionary
                strFilterValue = dict.value(forKey: "Value") as! String
                //strFilterValue = self.arrModelList[indexPath.row] as! String
                strFilterKey = "Model"
            }
            //self.selectedCells.append(indexPath.row)
        }
        else if self.strFilterName == "FuelType"
        {
            if self.arrFuelType.count > 0
            {
                strFilterValue = self.arrFuelType[indexPath.row] as! String
                strFilterKey = "FuelTypeName" //"fuelname" //"FuelTypeName"
                //cell.lblVariantNumber.text! = objVariants.key!
                //self.selectedCells.append(indexPath.row)
            }
        }
        else if self.strFilterName == "Car Type"
        {
            if self.arrCarType.count > 0
            {
                strFilterValue = self.arrCarType[indexPath.row] as! String
                strFilterKey = "cartype"
            //self.selectedCells.append(indexPath.row)
            }
        }
        else if self.strFilterName == "Transmission"
        {
            if self.arrTransmissionType.count > 0
            {
                strFilterValue = self.arrTransmissionType[indexPath.row] as! String
                strFilterKey = "Transmissiontype"
            //self.selectedCells.append(indexPath.row)
            }
        }
        else if self.strFilterName == "Car Category"
        {
            if self.arrCarCategory.count > 0
            {
                strFilterValue = self.arrCarCategory[indexPath.row] as! String
                strFilterKey = "carcategory"
            //self.selectedCells.append(indexPath.row)
            }
        }
        
        /*if self.selectedCells.contains(indexPath.row) {
            self.selectedCells.remove(at: self.selectedCells.firstIndex(of: indexPath.row)!)
            //self.selectedCells.removeAtIndex(self.selectedCells.indexOf(indexPath.row)!)
            strFilterValue = ""
            strFilterKey = ""
        }
        else
        {
            //let image = UIImage(named: "filter_red_tick")
            //cell.imgFilterCheck.image = image
            /*if self.strFilterName == "Variant"
            {
                let objVariants = self.arrVariants[indexPath.row] as! SearchCarVariant
                strFilterValue = objVariants.value! as String
                self.selectedCells.append(indexPath.row)
            }*/
        }*/
        
        tableView.reloadData()
    }
    
    func setUpTableView( strTitle :String)
        
    {
        self.objSearchCarFilter?.seatingCap = APIFunctions.dictGlobalCarFilter!.value(forKey: "SeatingCap") as? [String]
        
        /*if strTitle == "Variant"
        {
            self.btnVariants.backgroundColor = UIColor.white
            self.btnVariants.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
            self.arrVariants = self.objFilter!.variant! as NSArray
            //print("arrVariants: ", self.arrVariants)
            self.tblFilterView.reloadData()
        }*/
        if strTitle == "Brand"
        {
            self.btnVariants.backgroundColor = UIColor.white
            self.btnVariants.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
            if let arr = APIFunctions.dictGlobalCarFilter!.value(forKey: "CarCompany") as? [String] {
                self.arrCarCompany = arr as NSArray
            }
            self.tblFilterView.reloadData()
        }
        else if strTitle == "Model"
        {
            //let arr1 = APIFunctions.dictGlobalCarFilter!.value(forKey: "Model") as! NSArray
            if let arr = APIFunctions.dictGlobalCarFilter!.value(forKey: "Model") as? NSArray {
                self.arrModelList = arr as NSArray
            }
            self.tblFilterView.reloadData()
        }
        else if strTitle == "FuelType"
        {
            if let arr = APIFunctions.dictGlobalCarFilter!.value(forKey: "fuelname") as? [String] {
                self.arrFuelType = arr as NSArray
            }
            self.tblFilterView.reloadData()
        }
        /*else if strTitle == "CarCompany"
        {
            if let arr = APIFunctions.dictGlobalCarFilter!.value(forKey: "CarCompany") as? [String] {
                self.arrCarCompany = arr as NSArray
            }
            self.tblFilterView.reloadData()
        }*/
        else if strTitle == "Car Type"
        {
            if let arr = APIFunctions.dictGlobalCarFilter!.value(forKey: "cartype") as? [String] {
                self.arrCarType = arr as NSArray
            }
            self.tblFilterView.reloadData()
        }
        else if strTitle == "Transmission"
        {
            if let arr = APIFunctions.dictGlobalCarFilter!.value(forKey: "Transmissiontype") as? [String] {
                self.arrTransmissionType = arr as NSArray
            }
            self.tblFilterView.reloadData()
        }
        else if strTitle == "Car Category"
        {
            if let arr = APIFunctions.dictGlobalCarFilter!.value(forKey: "carcategory") as? [String] {
                self.arrCarCategory = arr as NSArray
            }
            self.tblFilterView.reloadData()
        }
    }
    
    @IBAction func btnBack_Click(_ sender: Any)
    {
        //self.navigationController?.popViewController(animated: true)
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnClose_Click(_ sender: Any)
    {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
   
    @IBAction func btnApply_Click(_ sender: Any)
    {
        self.delegate?.filterUsingVariant(keyName: self.strFilterKey, value: self.strFilterValue)
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSelectOptionForFilter_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        self.strFilterName = sender.currentTitle!
        switch (sender.tag)
        {
            case 101:
                do {
                    
                    self.btnVariants.backgroundColor = UIColor.white
                    self.btnVariants.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
                    
                    self.btnModel.backgroundColor = Constants.RED_COLOR
                    self.btnModel.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnFuelType.backgroundColor = Constants.RED_COLOR
                    self.btnFuelType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarType.backgroundColor = Constants.RED_COLOR
                    self.btnCarType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnTransmissionType.backgroundColor = Constants.RED_COLOR
                    self.btnTransmissionType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarCategory.backgroundColor = Constants.RED_COLOR
                    self.btnCarCategory.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnVariants.isSelected = false
                    
                    //self.tblFilterView.isHidden = false
                    //self.tblFilterView.reloadData()
            }
            break
            case 102:
                do {
                    self.btnModel.backgroundColor = UIColor.white
                    self.btnModel.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
                
                    self.btnModel.isSelected = true
                
                    self.btnVariants.backgroundColor = Constants.RED_COLOR
                    self.btnVariants.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnFuelType.backgroundColor = Constants.RED_COLOR
                    self.btnFuelType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                
                    self.btnCarType.backgroundColor = Constants.RED_COLOR
                    self.btnCarType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                
                    self.btnTransmissionType.backgroundColor = Constants.RED_COLOR
                    self.btnTransmissionType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                
                    self.btnCarCategory.backgroundColor = Constants.RED_COLOR
                    self.btnCarCategory.setTitleColor(UIColor.white, for: UIControl.State.normal)
            }
            break
            case 103:
                do {
                    self.btnFuelType.backgroundColor = UIColor.white
                    self.btnFuelType.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
                    
                    self.btnFuelType.isSelected = true
                    
                    self.btnVariants.backgroundColor = Constants.RED_COLOR
                    self.btnVariants.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnModel.backgroundColor = Constants.RED_COLOR
                    self.btnModel.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarType.backgroundColor = Constants.RED_COLOR
                    self.btnCarType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnTransmissionType.backgroundColor = Constants.RED_COLOR
                    self.btnTransmissionType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarCategory.backgroundColor = Constants.RED_COLOR
                    self.btnCarCategory.setTitleColor(UIColor.white, for: UIControl.State.normal)
            }
            break
            case 104:
                do {
                    self.btnCarType.backgroundColor = UIColor.white
                    self.btnCarType.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
                    
                    self.btnFuelType.backgroundColor = Constants.RED_COLOR
                    self.btnFuelType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnVariants.backgroundColor = Constants.RED_COLOR
                    self.btnVariants.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnTransmissionType.backgroundColor = Constants.RED_COLOR
                    self.btnTransmissionType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarCategory.backgroundColor = Constants.RED_COLOR
                    self.btnCarCategory.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    self.btnCarType.isSelected = true
                }
            break
            case 105:
                do {
                    self.btnTransmissionType.backgroundColor = UIColor.white
                    self.btnTransmissionType.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
                    
                    self.btnFuelType.backgroundColor = Constants.RED_COLOR
                    self.btnFuelType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarType.backgroundColor = Constants.RED_COLOR
                    self.btnCarType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnVariants.backgroundColor = Constants.RED_COLOR
                    self.btnVariants.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnModel.backgroundColor = Constants.RED_COLOR
                    self.btnModel.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarCategory.backgroundColor = Constants.RED_COLOR
                    self.btnCarCategory.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnTransmissionType.isSelected = true
                }
            break
            case 106:
                do {
                    self.btnCarCategory.backgroundColor = UIColor.white
                    self.btnCarCategory.setTitleColor(Constants.RED_COLOR, for: UIControl.State.normal)
                    
                    self.btnFuelType.backgroundColor = Constants.RED_COLOR
                    self.btnFuelType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarType.backgroundColor = Constants.RED_COLOR
                    self.btnCarType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnTransmissionType.backgroundColor = Constants.RED_COLOR
                    self.btnTransmissionType.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnVariants.backgroundColor = Constants.RED_COLOR
                    self.btnVariants.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnModel.backgroundColor = Constants.RED_COLOR
                    self.btnModel.setTitleColor(UIColor.white, for: UIControl.State.normal)
                    
                    self.btnCarCategory.isSelected = true
                }
            break
            default:
                print("")
        }
        self.setUpTableView(strTitle: self.strFilterName!)
    }
}
