//
//  SearchCityTableViewCell.swift
//  Myles Zero
//
//  Created by skpissay on 05/09/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class SearchCityTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCityName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
