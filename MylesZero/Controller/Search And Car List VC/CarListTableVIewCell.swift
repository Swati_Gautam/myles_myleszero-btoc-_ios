//
//  CarListTableVIewCell.swift
//  Myles Zero
//
//  Created by skpissay on 09/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import UIKit

class CarListTableVIewCell: UITableViewCell {
    
    //@IBOutlet weak var lblCarType: UILabel!
    weak var cellSubscribeDelegate: CarListSubscribeDelegate?
    
    @IBOutlet weak var lblVariantName: UILabel!
    @IBOutlet weak var lblCarTransmissionType: UILabel!
    @IBOutlet weak var lblCarColor: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var btnSubscribe: UIButton!

    @IBOutlet weak var lblDeliveryDays: UILabel!
    @IBOutlet weak var lblSeater: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var imgCarPic: UIImageView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var imgCarColor: UIImageView!
    @IBOutlet weak var lblFuelType: UILabel!
    @IBOutlet weak var viewForCarImage: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnSubscribe.layer.cornerRadius = 4.0
        self.viewForCarImage.layer.borderWidth = 1.0
        self.viewForCarImage.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func btnCarSubscribe_Click(_ sender: UIButton)
    {
        cellSubscribeDelegate?.didCarSubscribeButton(sender.tag)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

protocol CarListSubscribeDelegate : class {
    func didCarSubscribeButton(_ tag: Int)
    //func didPressMenuButton(_tag: Int )
}
