//
//  SubscriptionStatusVC.swift
//  Myles Zero
//
//  Created by Chetan Rajauria on 11/09/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import Alamofire

class SubscriptionStatusVC: UIViewController {
    
    @IBOutlet weak var lblTransmission: UILabel!
    @IBOutlet weak var lblCarKM: UILabel!
    @IBOutlet weak var lblCarColor: UILabel!
    @IBOutlet weak var lblFuelType: UILabel!
    @IBOutlet weak var lblCarYear: UILabel!
    @IBOutlet weak var lblCarModelType: UILabel!
    
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!

    @IBOutlet weak var btnAuthorizePayment: UIButton!
    @IBOutlet var imgDocumentUploaded: UIImageView!
    @IBOutlet var imgDocumentVerified: UIImageView!
    @IBOutlet var imgPaymentAuthorized: UIImageView!
    @IBOutlet var imgOngoingSubscription: UIImageView!
    
    @IBOutlet var viewDocumentUploaded: UIView!
    @IBOutlet var viewDocumentVerified: UIView!
    @IBOutlet var viewPaymentAuthorized: UIView!
    
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var strSubscriptionStatus : String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        //self.checkSubscriptionStatus()
        print("Status Page : ", self.objSubscriptionDetail?.subscriptionId)
        
        self.imgDocumentUploaded.layer.cornerRadius = self.imgDocumentUploaded.frame.size.width/2
        self.imgDocumentVerified.layer.cornerRadius = self.imgDocumentVerified.frame.size.width/2
        self.imgPaymentAuthorized.layer.cornerRadius = self.imgPaymentAuthorized.frame.size.width/2
        self.imgOngoingSubscription.layer.cornerRadius = self.imgOngoingSubscription.frame.size.width/2
        
        self.imgDocumentUploaded.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgDocumentUploaded.layer.borderWidth = 1
        self.imgDocumentVerified.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgDocumentVerified.layer.borderWidth = 1
        self.imgPaymentAuthorized.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgPaymentAuthorized.layer.borderWidth = 1
        self.imgOngoingSubscription.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgOngoingSubscription.layer.borderWidth = 1
        
        self.lblCarModelType.text = objSubscriptionDetail?.carModelName
        self.lblCarKM.text = objSubscriptionDetail?.kmRun
        self.lblCarColor.text = objSubscriptionDetail?.carColor
        self.lblCarYear.text = objSubscriptionDetail?.purchaseYear
        self.lblFuelType.text = objSubscriptionDetail?.fuelType
        self.lblTransmission.text = objSubscriptionDetail?.transmission
        
        self.lblPaymentStatus.text = objSubscriptionDetail?.carSubscriptionStatus
        let strSubscriptionStatus = objSubscriptionDetail?.carSubscriptionStatus.lowercased()
        self.setSubscriptionStatus(strStatus: strSubscriptionStatus ?? "")
        //self.setSubscriptionStatus(strStatus: objSubscriptionDetail?.carSubscriptionStatus ?? "")
        self.lblAmount.text = "\u{20B9}"+"\(objSubscriptionDetail?.indicatedMonthlyRental ?? "0000")"
        self.lblTaxAmount.text = String(format: "+ %@ taxes", (objSubscriptionDetail?.taxAmount)! as String)   
        //objSubscriptionDetail?.carSubscriptionStatus
        print("objSubscriptionDetail?.carSubscriptionStatus: ", objSubscriptionDetail?.carSubscriptionStatus!)
        
        /*if objSubscriptionDetail?.carSubscriptionStatus == "Pending Approval"
        {
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Document Verify"
        {
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Pending Agreement"
        {
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.imgPaymentAuthorized.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = false
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Payment Recieved"
        {
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.imgPaymentAuthorized.backgroundColor = Constants.RED_COLOR
        } */
    }
    
    func setSubscriptionStatus(strStatus: String)
    {
        if strStatus ==  "pending approval"{
            self.lblPaymentStatus.text = "Admin approval pending"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus ==  "pending doc verification"{
            self.lblPaymentStatus.text = "Ongoing Documents Verification"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus ==  "business associate confirmed"{
            self.lblPaymentStatus.text =  ""
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus ==  "pending payment"{
            self.lblPaymentStatus.text =  "Payment Authorization Pending"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = false
        }
        else if strStatus ==  "telematics installed"{
            self.lblPaymentStatus.text = "Pending Delivery"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus ==  "ready for delivery"{
            self.lblPaymentStatus.text = "Pending Delivery"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus ==  "cancelled"{
            self.lblPaymentStatus.text = "Subscription Cancelled"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus ==  "subscription termination requested"{
            self.lblPaymentStatus.text = "Termination Requested"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus ==  "pending security"{
            self.lblPaymentStatus.text = "Security Pending"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "takeover inspection complete"{
            self.lblPaymentStatus.text = ""
        }
        else if strStatus == "pending outstanding payment"{
            self.lblPaymentStatus.text = ""
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "subscription terminated"{
            self.lblPaymentStatus.text = "Subscription Terminated"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "admin rejected"{
            self.lblPaymentStatus.text = "Refused By Admin"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "pending si"{
            self.lblPaymentStatus.text = "Payment Completed"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.imgPaymentAuthorized.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "pending agreement"{
            self.lblPaymentStatus.text = "Payment Authorization Pending"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.imgPaymentAuthorized.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = false
        }
        else if strStatus == "cancel by user"{
            self.lblPaymentStatus.text = "Cancelled by User"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "document rejected"{
            self.lblPaymentStatus.text = "Document Rejected"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus.contains("document verify"){
            self.lblPaymentStatus.text = "Payment Authorization Pending"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = false
        }
        if strStatus == "order confirmed"{
            self.lblPaymentStatus.text = "Order Confirmed"
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "pending ecs"{
            self.lblPaymentStatus.text = "Payment Completed"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.imgPaymentAuthorized.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = true
        }
        else if strStatus == "payment recieved"{
            self.lblPaymentStatus.text = "Payment Completed"
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
            self.imgPaymentAuthorized.backgroundColor = Constants.RED_COLOR
            self.btnAuthorizePayment.isHidden = true
        }
    }
    
    func checkSubscriptionStatus()
    {
        self.imgDocumentUploaded.layer.cornerRadius = self.imgDocumentUploaded.frame.size.width/2
        self.imgDocumentVerified.layer.cornerRadius = self.imgDocumentVerified.frame.size.width/2
        self.imgPaymentAuthorized.layer.cornerRadius = self.imgPaymentAuthorized.frame.size.width/2
        self.imgOngoingSubscription.layer.cornerRadius = self.imgOngoingSubscription.frame.size.width/2
        
        self.imgDocumentUploaded.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgDocumentUploaded.layer.borderWidth = 1
        self.imgDocumentVerified.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgDocumentVerified.layer.borderWidth = 1
        self.imgPaymentAuthorized.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgPaymentAuthorized.layer.borderWidth = 1
        self.imgOngoingSubscription.layer.borderColor = Constants.RED_COLOR.cgColor
        self.imgOngoingSubscription.layer.borderWidth = 1
        
        if self.strSubscriptionStatus == "Pending Approval"
        {
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Document Verify"
        {
            self.imgDocumentVerified.backgroundColor = Constants.RED_COLOR
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Payment Recieved"
        {
            self.imgPaymentAuthorized.backgroundColor = Constants.RED_COLOR
        }
        /*if objSubscriptionDetail?.carSubscriptionStatus == "Subscription Active"
        {
            self.imgDocumentUploaded.backgroundColor = Constants.RED_COLOR
        }*/
    }
    
    @IBAction func btnAuthorizePayment_Click(_ sender: Any)
    {
        let objSignAgreement = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SignAgreementVC) as! SignAgreementVC
        var url : String = ""
        url = String(format: APIConstants.API_CarSubscriptionAgreement,objSubscriptionDetail!.clientID,(objSubscriptionDetail?.subscriptionBookingid!)! as String)

        objSignAgreement.strUrl = url
        DefaultsValues.setCustomObjToUserDefaults(objSubscriptionDetail, forKey: kSubscriptionDetail)
        self.navigationController?.pushViewController(objSignAgreement, animated: true)
    }
    
    func authorizePaymentAPI()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        var dictpaymentParam:[String:AnyObject]!
        dictpaymentParam = ["CarColour":"Red","CarModelFullName":"HECTORSharp Hybrid 1.5 Petrol","CarModelName":"HECTOR","CarVariantId":"2056","ModelId":"118","PkgId":"40091","VariantName":"Sharp Hybrid 1.5 Petrol","amount":"44067","carid":"8172","city":"Delhi","cityid":"2","client_id":"3922","coric":"LTR6201601008687","custid":"2702166","dropoffdate":"","email":"murlidhar.kanhaiya@mylescars.com","extrakmrate":"11.00","kmuses":"16500","maxamountlimit":"44067","mobile":"9560806470","monthly_amount":"33160.00","name":"kanhaiya murli","penalty_amount":"0","pickupdate":"10/13/2020 3:20:19 PM","source":"Mobile","subs_request_id":"DL/2021/000396","tenure":"60 months"] as [String:AnyObject]
        
        /*dictpaymentParam = ["CarColour" : (objSubscriptionDetail?.carColor ?? "") as String,
                            "CarModelFullName": (objSubscriptionDetail?.carModelName ?? "") as String,
                            "CarModelName": (objSubscriptionDetail?.carModelName ?? "") as String,
                            "CarVariantId": (objSubscriptionDetail?.carVariantName ?? "") as String,
                            "ModelId": (objSubscriptionDetail?.carModelName ?? "") as String,
                            "PkgId": objSubscriptionDetail?.carColor as String,
                            "VariantName": (objSubscriptionDetail?.carVariantName ?? "") as String,
                            "amount" : (objSubscriptionDetail?.taxAmount ?? "") as String,
                            "carid": objSubscriptionDetail?.carColor as String,
                            "city": objSubscriptionDetail?.carColor as String,
                            "cityid": objSubscriptionDetail?.carColor as String,
                            "client_id": kCustomerCoID,
                            "coric": objSubscriptionDetail?.carColor as String,
                            "custid": objSubscriptionDetail?.carColor as String,
                            "dropoffdate" : objSubscriptionDetail?.carColor as String,
                            "email": objSubscriptionDetail?.carColor as String
                            "extrakmrate": objSubscriptionDetail?.carColor as String,
                            "kmuses": objSubscriptionDetail?.carColor as String,
                            "maxamountlimit": kCustomerCoID,
                            "mobile": objSubscriptionDetail?.carColor as String
                            "monthly_amount": objSubscriptionDetail?.carColor as String,
                            "penalty_amount": objSubscriptionDetail?.carColor as String,
                            "pickupdate": objSubscriptionDetail?.carColor as String,
                            "source": "Mobile",
                            "subs_request_id": objSubscriptionDetail?.carColor as String
                            "tenure": objSubscriptionDetail?.carColor as String] as [String : AnyObject]*/
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.authorizePaymentUrl(APIConstants.API_Payment_Url, dataDict: dictpaymentParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
//                    let Json = response.value(forKey: "response") as! NSDictionary
//                    let arrCarList = Json.value(forKey: "Cars") as! NSArray
//                    let dictFilterList = Json.value(forKey: "Fitlers") as! NSDictionary
//                    self.objSearchCarFilter?.fuelname = dictFilterList.value(forKey: "fuelname") as? [String]
                    
                    //DefaultsValues.setDictionaryValueToUserDefaults(dictFilterList, forKey: kFilterData)
                }
            }
        })
    }
}
