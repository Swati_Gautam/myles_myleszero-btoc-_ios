//
//  ViewUploadedDocumentVC.swift
//  Myles
//
//  Created by skpissay on 13/04/21.
//  Copyright © 2021 Myles. All rights reserved.
//

import UIKit
import WebKit

class ViewUploadedDocumentVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var strDocumentUrl : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.strDocumentUrl != ""
        {
            let url = URL(string: self.strDocumentUrl!) //"https://developer.apple.com/documentation/webkit/wkwebview"
            let request = URLRequest(url: url!)
            webView.navigationDelegate = self as? WKNavigationDelegate
            webView.load(request)
        }
    }
    
    @IBAction func btnCancel_Click(_ sender: Any)
    {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
