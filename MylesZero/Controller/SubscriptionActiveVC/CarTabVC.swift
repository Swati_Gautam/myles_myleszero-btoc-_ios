//
//  CarTabVC.swift
//  Myles Zero
//
//  Created by skpissay on 16/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class CarTabVC: UIViewController {

    @IBOutlet weak var btnCarHealthScore: UIButton!
    @IBOutlet weak var btnOrderMylesCar: UIButton!
    @IBOutlet weak var btnTrackCar: UIButton!
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    
    @IBOutlet weak var lblTransmission: UILabel!
    @IBOutlet weak var lblCarKM: UILabel!
    @IBOutlet weak var lblCarColor: UILabel!
    @IBOutlet weak var lblFuelType: UILabel!
    @IBOutlet weak var lblCarYear: UILabel!
    @IBOutlet weak var lblCarModelType: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        CommonMethodsMylesZero.setBorder(button: self.btnTrackCar)
        CommonMethodsMylesZero.setBorder(button: btnCarHealthScore)
        CommonMethodsMylesZero.setBorder(button: btnOrderMylesCar)

        self.lblCarModelType.text = objSubscriptionDetail!.cartype!
        self.lblCarKM.text = objSubscriptionDetail!.kmRun!
        self.lblCarYear.text = objSubscriptionDetail!.purchaseYear!
        self.lblCarColor.text = objSubscriptionDetail!.carColor!
        self.lblFuelType.text = objSubscriptionDetail!.fuelType!
        self.lblTransmission.text = objSubscriptionDetail!.transmission!
    }
    
    @IBAction func btnTrackCar_Click(_ sender: Any)
    {
        let objTrackCarVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_TrackCar) as! TrackCarVC
        self.navigationController?.pushViewController(objTrackCarVC, animated: true)
    }
    
    @IBAction func btnMylesCar_Click(_ sender: Any)
    {
       let objMylesVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_Myles) as! MylesVC
        self.navigationController?.pushViewController(objMylesVC, animated: true)
    }
    
    @IBAction func btnCarHealthScore_Click(_ sender: Any)
    {
        
    }
}
