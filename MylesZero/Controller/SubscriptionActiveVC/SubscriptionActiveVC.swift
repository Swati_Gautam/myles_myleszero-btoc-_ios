//
//  SubscriptionActiveVC.swift
//  Myles Zero
//
//  Created by skpissay on 10/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import Alamofire
import SideMenu
import iOSDropDown

class SubscriptionActiveVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var txtDropdown: DropDown!
    @IBOutlet var lblNoDataFound: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var viewCar: UIView!
    @IBOutlet weak var viewSubscription: UIView!
    @IBOutlet weak var viewService: UIView!
    @IBOutlet weak var viewDocumentPayment: UIView!
    @IBOutlet weak var imgViewCar: UIImageView!
    @IBOutlet weak var viewDashboard: UIView!
    @IBOutlet weak var viewSubscriptionTabs: UIView!
    @IBOutlet weak var viewContainerView: UIView!
    
    @IBOutlet weak var lblSubscriptionID: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var btnCarTab: UIButton!
    @IBOutlet weak var btnSubscriptionTab: UIButton!
    @IBOutlet weak var btnServiceTab: UIButton!
    @IBOutlet weak var btnDocumentPaymentTab: UIButton!
    
    @IBOutlet weak var btnRequestCallback: UIButton!
    var arrSubscriptionList = NSArray()
    var arrSubscriptionIDList = NSMutableArray()
    
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    let customSideMenuManager = SideMenuManager()

    var arrCallbackReason : NSArray?
       
    var viewTransparent = UIView()
    var viewServiceCallback = RequestCallbackView()
    var strCallbackReasonId : String?
    var strTimeSlot : String?
    
    var datePickerView = UIDatePicker()
    
    //MARK: - Child View Controllers
    private lazy var VC_CarTab: CarTabVC = {
        
        let controllerCar:CarTabVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_CarTabVC) as! CarTabVC
        controllerCar.objSubscriptionDetail = self.objSubscriptionDetail
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerCar)
        
        return controllerCar
    }()
    
    private lazy var VC_Subscription: SubscriptionTabVC = {
        
        let controllerSubscription:SubscriptionTabVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_SubscriptionTabVC) as! SubscriptionTabVC
        controllerSubscription.objSubscriptionDetail = self.objSubscriptionDetail
        controllerSubscription.viewActive = self.view
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerSubscription)
        
        return controllerSubscription
    }()
    
    private lazy var VC_Service: ServiceTabVC = {
        
        let controllerService:ServiceTabVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_ServiceTabVC) as! ServiceTabVC
        controllerService.objSubscriptionDetail = self.objSubscriptionDetail
        controllerService.viewActive = self.view
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerService)
        
        return controllerService
    }()
    
    private lazy var VC_SubscriptionStatus: SubscriptionStatusVC = {
           
           let controllerStatus:SubscriptionStatusVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_SubscriptionStatus) as! SubscriptionStatusVC
        print("ObjS ", self.objSubscriptionDetail?.subscriptionId)
        controllerStatus.objSubscriptionDetail = self.objSubscriptionDetail
           
           // Add View Controller as Child View Controller
           self.add(asChildViewController: controllerStatus)
           
           return controllerStatus
       }()
    
    private lazy var VC_DocumentPayment: DocumentAndPaymentVC = {
        
        let controllerDocumentAndPayment:DocumentAndPaymentVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_DocumentAndPaymentVC) as! DocumentAndPaymentVC
        controllerDocumentAndPayment.objSubscriptionDetail = self.objSubscriptionDetail
        controllerDocumentAndPayment.viewActive = self.view
        //controllerDocumentAndPayment.reportDelegate = self
        // Add View Controller as Child View Controller
        self.add(asChildViewController: controllerDocumentAndPayment)
        
        return controllerDocumentAndPayment
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.arrSubscriptionList.count > 0
        {
            self.GetCallbackReasonWebService()
            self.btnRequestCallback.isHidden = false
        }
        else
        {
            self.btnRequestCallback.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.setupUI()
        self.callApiToGetEmployeeDetail()
    }
    
    /*func reportLossView() {
        print("ABCD")
        //self.loadReportALossItemView()
    }*/
    
    //MARK: - API For Employee Detail
    
    func callApiToGetEmployeeDetail()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.view)
                
        let strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String
        var dictLoginParam:[String:AnyObject]!
        dictLoginParam = ["clientid": strClientID] as [String : AnyObject]
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        APIFunctions.sharedInstance.getEmployeeProfileDetail(APIConstants.API_GetEmployeeDetail, dataDict: dictLoginParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion: { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                if error != nil
                {
                    
                }
                else
                {
                    let dict = response as! NSDictionary
                    let strStatus = dict.value(forKey: "status") as! Int
                    if strStatus == 1
                    {
                        let arr = response.value(forKey: kResponseInfo) as! NSArray
                        let dict = (arr[0] as AnyObject) as! NSDictionary
                        let userProfileDetail  = UserProfileInfoResponse.init(fromDictionary:(dict) as! [String : Any])
                        DefaultsValues.setCustomObjToUserDefaults(userProfileDetail, forKey: kUserProfileDetails)
                    }
                    else
                    {
                        //let strMsg = dict.value(forKey: "message") as! String
                        //CommonMethodsMylesZero.showAlertController1(strTitle: "Error", strMsg: strMsg, strAction1: "OK", strAction2: "Cancel", viewController: self)
                    }
                }
            }
        })
    }
    
    //MARK: - SetUp UI For Dashboard

    func setupUI()
    {
        if self.arrSubscriptionList.count == 0
        {
            self.lblNoDataFound.isHidden = false
            self.viewContainerView.isHidden = true
            self.viewSubscriptionTabs.isHidden = true
            self.viewDashboard.isHidden = true
            return
        }
        else
        {
            self.lblNoDataFound.isHidden = true
            self.viewContainerView.isHidden = false
            self.viewSubscriptionTabs.isHidden = false
            self.viewDashboard.isHidden = false
        }
        
        self.viewCar.isHidden = false
        self.viewSubscription.isHidden = true
        self.viewService.isHidden = true
        self.viewDocumentPayment.isHidden = true
        //self.viewSubscriptionTabs.isHidden = false
        // self.btnRequestCallback.isHidden = true
        
        for i in 0..<self.arrSubscriptionList.count
        {
            let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[i]) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
            
            let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
            txtDropdown.optionArray.append(objSubscriptionDetail!.subscriptionId!)
            
            self.arrSubscriptionIDList.add(objSubscriptionDetail!.subscriptionId!)
            //print("self.arrSubscriptionIDList: ", self.arrSubscriptionIDList)
            
            self.viewSubscriptionTabs.isHidden = true
            self.btnRequestCallback.isHidden = true
            self.txtDropdown.text = objSubscriptionDetail!.subscriptionId!
            self.lblCarName.text = String(format: "%@ - %@", objSubscriptionDetail!.carModelName!, objSubscriptionDetail!.carVariantName!)
            self.imgViewCar.sd_setImage(with: URL(string: (objSubscriptionDetail!.carImage!)), placeholderImage: UIImage(named: kCarPlaceholder))
            let viewSubscription = self.getSubscriptionStatusVC(object: objSubscriptionDetail!)
            self.remove(asChildViewController: viewSubscription)
            self.add(asChildViewController:viewSubscription)
            
            if objSubscriptionDetail?.carSubscriptionStatus == "Subscription Active"
            {
                self.txtDropdown.text = objSubscriptionDetail!.subscriptionId!
                self.viewSubscriptionTabs.isHidden = false
                self.btnRequestCallback.isHidden = false
                //self.lblSubscriptionID.text = objSubscriptionDetail!.subscriptionId!
                self.lblCarName.text = String(format: "%@ - %@", objSubscriptionDetail!.carModelName!, objSubscriptionDetail!.carVariantName!)
                self.imgViewCar.sd_setImage(with: URL(string: (objSubscriptionDetail!.carImage!)), placeholderImage: UIImage(named: kCarPlaceholder))
                self.objSubscriptionDetail = objSubscriptionDetail
                let viewCar = self.getCarTabVC(object: objSubscriptionDetail!)
                self.add(asChildViewController: viewCar)
                //add(asChildViewController: VC_CarTab)
                //break
            }
        }
        txtDropdown.arrowColor = UIColor.white
        txtDropdown.selectedRowColor = Constants.RED_COLOR
        txtDropdown.checkMarkEnabled = false
        txtDropdown.didSelect{(selectedText , index ,id) in
        print("Selected String: \(selectedText) \n index: \(index)")
            
        let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[index]) as! [String : Any])
        DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
        let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
        print("objSubscriptionDetail: ", objSubscriptionDetail!.subscriptionId!)
        self.objSubscriptionDetail = objSubscriptionDetail
        self.txtDropdown.text = objSubscriptionDetail!.subscriptionId!
        self.lblCarName.text = String(format: "%@ - %@", objSubscriptionDetail!.carModelName!, objSubscriptionDetail!.carVariantName!)
        self.imgViewCar.sd_setImage(with: URL(string: (objSubscriptionDetail!.carImage!)), placeholderImage: UIImage(named: kCarPlaceholder))
            
        if objSubscriptionDetail?.carSubscriptionStatus == "Subscription Active"
        {
            self.objSubscriptionDetail = objSubscriptionDetail
            self.viewSubscriptionTabs.isHidden = false
            self.btnRequestCallback.isHidden = false
                
            if self.VC_SubscriptionStatus.view != nil
            {
                self.remove(asChildViewController: self.VC_SubscriptionStatus)
            }
            let viewCar = self.getCarTabVC(object: objSubscriptionDetail!)
            self.add(asChildViewController: viewCar)
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Pending Approval"
        {
            //self.objSubscriptionDetail = objSubscriptionDetail
            self.viewSubscriptionTabs.isHidden = true
            self.btnRequestCallback.isHidden = true
            print("objSubscriptionDetail Pending Approval: ", objSubscriptionDetail!.subscriptionId!)
            let viewSubscription = self.getSubscriptionStatusVC(object: objSubscriptionDetail!)
            self.remove(asChildViewController: viewSubscription)
            self.add(asChildViewController:viewSubscription)
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Document Verify"
        {
            //self.objSubscriptionDetail = objSubscriptionDetail
            self.viewSubscriptionTabs.isHidden = true
            self.btnRequestCallback.isHidden = true
            print("objSubscriptionDetail Document Verify: ", objSubscriptionDetail!.subscriptionId!)
            //self.remove(asChildViewController: self.VC_SubscriptionStatus)
            //self.add(asChildViewController:self.VC_SubscriptionStatus)
            
            let viewSubscription = self.getSubscriptionStatusVC(object: objSubscriptionDetail!)
            self.remove(asChildViewController: viewSubscription)
            self.add(asChildViewController:viewSubscription)
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Payment Recieved"
        {
            //self.objSubscriptionDetail = objSubscriptionDetail
            self.viewSubscriptionTabs.isHidden = true
            print("objSubscriptionDetail Payment Recieved: ", objSubscriptionDetail!.subscriptionId!)
            //self.remove(asChildViewController: self.VC_SubscriptionStatus)
            //self.add(asChildViewController:self.VC_SubscriptionStatus)
            let viewSubscription = self.getSubscriptionStatusVC(object: objSubscriptionDetail!)
            self.remove(asChildViewController: viewSubscription)
            self.add(asChildViewController:viewSubscription)
        }
        if objSubscriptionDetail?.carSubscriptionStatus == "Pending Agreement"
        {
            //self.objSubscriptionDetail = objSubscriptionDetail
            self.viewSubscriptionTabs.isHidden = true
            self.btnRequestCallback.isHidden = true
            print("objSubscriptionDetail Pending Agreement: ", objSubscriptionDetail!.subscriptionId!)
            //self.remove(asChildViewController: self.VC_SubscriptionStatus)
            //self.add(asChildViewController:self.VC_SubscriptionStatus)
            let viewSubscription = self.getSubscriptionStatusVC(object: objSubscriptionDetail!)
            self.remove(asChildViewController: viewSubscription)
            self.add(asChildViewController:viewSubscription)
        }
            //pending doc verification
        self.txtDropdown.textColor = UIColor.white
        }
    }
    
    //MARK: - Get Container View
    
    func getSubscriptionStatusVC(object : SubscriptionStatusDetailRootClass) -> SubscriptionStatusVC
    {
         let controllerStatus:SubscriptionStatusVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_SubscriptionStatus) as! SubscriptionStatusVC
         controllerStatus.objSubscriptionDetail = object
                  
                  // Add View Controller as Child View Controller
         self.add(asChildViewController: controllerStatus)
                  
         return controllerStatus
    }
    
    func getCarTabVC(object : SubscriptionStatusDetailRootClass) -> CarTabVC
    {
         let controllerCar:CarTabVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_CarTabVC) as! CarTabVC
         controllerCar.objSubscriptionDetail = object
         
         // Add View Controller as Child View Controller
         self.add(asChildViewController: controllerCar)
         
         return controllerCar
    }
    
    func getDocumentPaymentTabVC(object : SubscriptionStatusDetailRootClass) -> DocumentAndPaymentVC
    {
         let controllerDocumentAndPayment:DocumentAndPaymentVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_DocumentAndPaymentVC) as! DocumentAndPaymentVC
         controllerDocumentAndPayment.objSubscriptionDetail = object
         controllerDocumentAndPayment.viewActive = self.view
         //controllerDocumentAndPayment.reportDelegate = self
         // Add View Controller as Child View Controller
         self.add(asChildViewController: controllerDocumentAndPayment)
         
         return controllerDocumentAndPayment
    }
    
    func getSubscriptionTabVC(object : SubscriptionStatusDetailRootClass) -> SubscriptionTabVC
    {
         let controllerSubscription:SubscriptionTabVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_SubscriptionTabVC) as! SubscriptionTabVC
         controllerSubscription.objSubscriptionDetail = object
         controllerSubscription.viewActive = self.view
         // Add View Controller as Child View Controller
         self.add(asChildViewController: controllerSubscription)
         
         return controllerSubscription
    }
    func gerServiceTabVC(object : SubscriptionStatusDetailRootClass) -> ServiceTabVC
    {
         let controllerService:ServiceTabVC = self.storyboard!.instantiateViewController(withIdentifier: Constants.VC_ServiceTabVC) as! ServiceTabVC
         controllerService.objSubscriptionDetail = object
         controllerService.viewActive = self.view
         // Add View Controller as Child View Controller
         self.add(asChildViewController: controllerService)
         
         return controllerService
    }
    
    //MARK: - Helper Methods
    
    func add(asChildViewController viewController: UIViewController)
    {
        addChild(viewController)
        self.viewContainerView.addSubview(viewController.view)
        viewController.view.frame = self.viewContainerView.bounds
        viewController.didMove(toParent: self)
    }
    
    func remove(asChildViewController viewController: UIViewController)
    {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    //MARK: - Create UITableView
    func setUpTableView()
    {
        let tableView: UITableView = UITableView()
        tableView.frame = CGRect(x: 10, y: 10, width: 100, height: 500)
        tableView.dataSource = self
        tableView.delegate = self

        self.view.addSubview(tableView)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "Cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        cell.textLabel!.text = "foo"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    // MARK: - Side Menu Function
    
    private func setupSideMenu()
    {
        // Define the menus
        
        guard let objSideMenuVC = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SideMenu) as? SideMenuVC else { return }
        
        let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: objSideMenuVC)
        //let leftMenuNavigationController = UISideMenuNavigationController(rootViewController: SideMenuVC)
        customSideMenuManager.menuLeftNavigationController = leftMenuNavigationController
        
        customSideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        //customSideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        customSideMenuManager.menuWidth = 300.0
        customSideMenuManager.menuFadeStatusBar = false
        
        //leftMenuNavigationController.statusBarEndAlpha = 0
        
        present(leftMenuNavigationController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let sideMenuNavigationController = segue.destination as? UISideMenuNavigationController
        {
            sideMenuNavigationController.sideMenuManager = customSideMenuManager
        }
    }
    
    //MARK: - Request Callback
    
    func GetCallbackReasonWebService()
    {
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.getCallbackReasonAPI(APIConstants.API_GetCallbackReason, dataDict: nil, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    //let JsonLogin = response as AnyObject
                    let arrResponse = response.value(forKey: "response") as! NSArray
                    print("arrResponse Callback Reason: ", arrResponse)
                    self.arrCallbackReason = arrResponse
                }
            }
         })
    }
    
    func RequestForSubscriptionCallbackWebservice()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders)
        let dictParam = ["subscriptionbookingId":self.objSubscriptionDetail?.subscriptionBookingid,
             "ReasonId":self.strCallbackReasonId,
             "Remark":"",
             "DateTimeSlot":self.strTimeSlot] as [String:AnyObject]
        APIFunctions.sharedInstance.subscriptionCallbackRequestAPI(APIConstants.API_SubsriptionCallBackRequest, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
                hud!.dismiss()
         
                if error != nil
                {
                }
                else
                {
                    let strMessage  = response.value(forKey: "Message") as? String
                    CommonMethodsMylesZero.popupAlert(title: strMessage, message: "", actionTitles: ["Ok"], actions:[{action1 in
                        self.hideCallbackView()
                    }, nil], viewController: self)
                }
            }
         })
    }
    //MARK: - Set Drop Down
    
    func setCallbackReasonDropDown()
    {
        for i in 0..<self.arrCallbackReason!.count
        {
            let dictCallbackReason = self.arrCallbackReason![i] as! NSDictionary
            let strCallbackReasonName = dictCallbackReason.value(forKey: "Reason") as! String
            self.viewServiceCallback.txtReasonForCallback.optionArray.append(strCallbackReasonName)
            //let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
        }
        viewServiceCallback.txtReasonForCallback.arrowColor = UIColor.black
        viewServiceCallback.txtReasonForCallback.selectedRowColor = Constants.RED_COLOR
        viewServiceCallback.txtReasonForCallback.checkMarkEnabled = false
        viewServiceCallback.txtReasonForCallback.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            let dictData = self.arrCallbackReason![index] as! NSDictionary
            let reasonId = dictData.value(forKey: "ReasonID") as! Int
            self.strCallbackReasonId = String(format: "%d", reasonId)
            self.viewServiceCallback.txtReasonForCallback.textColor = UIColor.black
        }
    }
    
    func setTimeSlotsDropDown()
    {
        let arrTimeSlots = ["8:00 AM - 10:00 AM","10:00 AM - 12:00 PM", "12:00 PM - 2:00 PM", "2:00 PM - 4:00 PM" ,"4:00 PM - 6:00 PM"]
         
        for i in 0..<arrTimeSlots.count
        {
            let strCallbackReasonName = arrTimeSlots[i]
            self.viewServiceCallback.txtTimeSlot.optionArray.append(strCallbackReasonName)
        }
        viewServiceCallback.txtTimeSlot.arrowColor = UIColor.black
        viewServiceCallback.txtTimeSlot.selectedRowColor = Constants.RED_COLOR
        viewServiceCallback.txtTimeSlot.checkMarkEnabled = false
        viewServiceCallback.txtTimeSlot.didSelect{(selectedText , index ,id) in
        print("Selected String: \(selectedText) \n index: \(index)")
            self.strTimeSlot = selectedText
            
        self.viewServiceCallback.txtTimeSlot.textColor = UIColor.black
        }
    }
    
    //MARK: - Load Request Callback View
    
    func loadServiceCallbackView()
    {
        viewTransparent = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.view.addSubview(viewTransparent)
        
        self.viewServiceCallback = Bundle.main.loadNibNamed("RequestCallbackView", owner: nil, options: nil)![0] as! RequestCallbackView
        self.view.addSubview(self.viewServiceCallback)
        self.view.bringSubviewToFront(self.viewServiceCallback)
        //self.viewServiceCallback.frame = CGRect(x: 10, y: self.viewActive.frame.size.height/2, width: self.viewActive.frame.size.width - 20, height: self.viewActive.frame.size.height/2)
        self.viewServiceCallback.frame.origin = self.view.bounds.origin
        self.viewServiceCallback.center = self.view.convert(self.view.center, from: self.viewServiceCallback)
        //self.viewServiceCallback.center = self.view.center
        self.viewServiceCallback.layer.cornerRadius = 4.0
        self.viewServiceCallback.btnRequestCallback.layer.cornerRadius = 4.0
        
        //self.viewReportLossItem.btnReportLoss.layer.borderWidth = 1.0
        self.viewServiceCallback.btnRequestCallback.addTarget(self, action:#selector(self.btnCallback_Click(sender:)), for: .touchUpInside)
        self.viewServiceCallback.btnDate.addTarget(self, action:#selector(self.btnCallbackDate_Click(sender:)), for: .touchUpInside)
        
        self.setCallbackReasonDropDown()
        self.setTimeSlotsDropDown()
    }
    
    func hideCallbackView()
    {
        viewTransparent.removeFromSuperview()
        if (self.viewServiceCallback.superview != nil)
        {
            self.viewServiceCallback.removeFromSuperview()
        }
    }
    
    @objc func handleReferralTap(_ sender: UITapGestureRecognizer)
    {
        viewTransparent.removeFromSuperview()
        if self.viewServiceCallback.superview != nil
        {
            self.viewServiceCallback.removeFromSuperview()
        }
    }
    
    @objc func dateSelected()
    {
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "dd/MM/yyyy"
        let dateString1 = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(dateString1)")
        
        self.viewServiceCallback.txtPreferredDate.text = dateString1
        self.viewServiceCallback.txtPreferredDate.textColor = UIColor.black
    }
    
    func getCurrentTime() -> String
    {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "HH:mm"
        let timeString = df.string(from: date)
        return timeString
    }
    
    func CheckTime() -> Bool
    {
        var timeExist:Bool
        let calendar = Calendar.current
        let startTimeComponent = DateComponents(calendar: calendar, hour:8)
        let endTimeComponent   = DateComponents(calendar: calendar, hour: 10)
        //let endTimeComponent   = DateComponents(calendar: calendar, hour: 17, minute: 00) //17
        
        let now = Date()
        let startOfToday = calendar.startOfDay(for: now)
        let startTime    = calendar.date(byAdding: startTimeComponent, to: startOfToday)!
        let endTime      = calendar.date(byAdding: endTimeComponent, to: startOfToday)!

        if startTime <= now && now <= endTime
        {
            print("between 8 AM and 10:30 AM")
            timeExist = true
        }
        else
        {
            print("not between 8 AM and 5:30 AM")
            timeExist = false
        }
        return timeExist
    }
    
    
    //MARK: - Button Actions
    @objc func btnCallback_Click(sender: UIButton!)
    {
        if self.viewServiceCallback.txtContactNumber.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter contact number", strAction1: "OK", viewController: self)
        }
        else if self.viewServiceCallback.txtPreferredDate.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the preferred date", strAction1: "OK", viewController: self)
        }
        else if self.viewServiceCallback.txtTimeSlot.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the time", strAction1: "OK", viewController: self)
        }
        else if self.viewServiceCallback.txtReasonForCallback.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter pickup address", strAction1: "OK", viewController: self)
        }
        else
        {
            self.RequestForSubscriptionCallbackWebservice()
        }
        
    }
    
    @objc func btnCallbackDate_Click(sender:UIButton!)
    {
        CommonMethodsMylesZero.showDateTimePicker(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        
        /*let strDate = CommonFunctions.showDateTimePicker1(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        viewServiceCallback.txtPreferredDate.text = strDate
        viewServiceCallback.txtPreferredDate.textColor = UIColor.black*/
        //var strTime = self.getCurrentTime()
        let time = CheckTime()
        if time == false
        {
            self.datePickerView.minimumDate = Calendar.current.date(byAdding: .day, value: +1, to: Date())
        }
        else
        {
            
        }        
        self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func btnSubscriptionID_Click(_ sender: Any)
    {
        
    }
    
    @IBAction func btnRequestCallback_Click(_ sender: Any)
    {
        if self.arrSubscriptionList.count > 0
        {
            self.loadServiceCallbackView()
        }        
    }
    
    @IBAction func btnMenu_Click(_ sender: Any)
    {
        self.setupSideMenu()
    }
    
    @IBAction func btnCar_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.viewCar.isHidden = false
            self.viewSubscription.isHidden = true
            self.viewService.isHidden = true
            self.viewDocumentPayment.isHidden = true
            
            remove(asChildViewController: VC_Subscription)
            remove(asChildViewController: VC_Service)
            remove(asChildViewController: VC_DocumentPayment)
            add(asChildViewController: VC_CarTab)
        }
        else
        {
            /*self.viewCar.isHidden = true
            self.viewSubscription.isHidden = false
            //self.viewService.isHidden = false
            //self.viewDocumentPayment.isHidden = false
            remove(asChildViewController: VC_CarTab)
            add(asChildViewController: VC_Subscription)*/
        }
    }
    
    @IBAction func btnSubscription_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.viewCar.isHidden = true
            self.viewSubscription.isHidden = false
            self.viewService.isHidden = true
            self.viewDocumentPayment.isHidden = true
            
            remove(asChildViewController: VC_CarTab)
            remove(asChildViewController: VC_Service)
            remove(asChildViewController: VC_DocumentPayment)
            add(asChildViewController: VC_Subscription)
        }
        else
        {
            /*self.viewCar.isHidden = false
            self.viewSubscription.isHidden = true
            //self.viewService.isHidden = true
            //self.viewDocumentPayment.isHidden = true
            remove(asChildViewController: VC_Subscription)
            add(asChildViewController: VC_CarTab)*/
        }
    }
    
    @IBAction func btnService_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.viewCar.isHidden = true
            self.viewSubscription.isHidden = true
            self.viewService.isHidden = false
            self.viewDocumentPayment.isHidden = true
            
            remove(asChildViewController: VC_CarTab)
            remove(asChildViewController: VC_Subscription)
            remove(asChildViewController: VC_DocumentPayment)
            add(asChildViewController: VC_Service)
        }
    }
    
    @IBAction func btnDocumentPayment_Click(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true
        {
            self.viewCar.isHidden = true
            self.viewSubscription.isHidden = true
            self.viewService.isHidden = true
            self.viewDocumentPayment.isHidden = false
            
            remove(asChildViewController: VC_CarTab)
            remove(asChildViewController: VC_Service)
            remove(asChildViewController: VC_Subscription)
            add(asChildViewController: VC_DocumentPayment)
        }
    }
}
