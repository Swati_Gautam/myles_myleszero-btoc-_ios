//
//  SubscriptionTabVC.swift
//  Myles Zero
//
//  Created by skpissay on 16/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import Alamofire

class SubscriptionTabVC: UIViewController
{
    @IBOutlet weak var lblMonthlyFee: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var btnEndSubscription: UIButton!
    @IBOutlet weak var btnUpgradeSubscription: UIButton!
    @IBOutlet weak var btnRenewSubscription: UIButton!
    
    @IBOutlet weak var btnRenewInfo: UIButton!
    var viewTransparent = UIView()
    var viewRenewSubscription = RenewSubscriptionView()
    var viewUpgradeSubscription = UpgradeSubscriptionView()
    var viewEndSubscription = EndSubscriptionView()
    var viewActive = UIView()
    var viewRenewInfo = UIView()
    var dictSubscription : NSDictionary?
    var dictTermination : NSDictionary?
    var dictTerminationRequest : NSDictionary?
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var isCheckedPice : Bool = false
    var arrTenureList = NSArray()
    var arrKmList = NSArray()
    
    var datePickerView = UIDatePicker()
    var isCarModelSelected : String?
    var valueForMonth : Int?
    var objSearchCarFilter : SearchCarFilter?
    var searchCarParam:[String:AnyObject]!
    var strDateForUpgradation : String?
    
    @IBOutlet weak var upgradeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var renewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        CommonMethodsMylesZero.setBorder(button: self.btnEndSubscription)
        CommonMethodsMylesZero.setBorder(button: btnUpgradeSubscription)
        CommonMethodsMylesZero.setBorder(button: btnRenewSubscription)
                
        self.lblDuration.text = String(format: "%@ Months", objSubscriptionDetail!.subscriptionTenureMonths!) 
        self.lblMonthlyFee.text = objSubscriptionDetail!.indicatedMonthlyRental!
        
        self.getKmAndTenureForUpgrade()
        
        //self.datePickerView.maximumDate = Calendar.current.date(byAdding: .day, value: 45, to: Date())
        //self.get45DaysForSubscription()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        let calendar = Calendar.current
        let endDate = Date()
        let startDate = objSubscriptionDetail!.subscriptionEndDate!
        let arr1 = startDate.components(separatedBy: " ")
        let strFinalDate = arr1[0]
        self.lblEndDate.text = strFinalDate //objSubscriptionDetail!.subscriptionEndDate!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let formatedStartDate = dateFormatter.date(from: strFinalDate)
        let date1 = calendar.startOfDay(for: endDate)
        let date2 = calendar.startOfDay(for: formatedStartDate!)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        print("components.day: %d ", components.day! as Int)
        let daysRemaining = components.day! as Int
        
        if daysRemaining <= 45
        {
            self.btnRenewSubscription.isHidden = false
            self.btnRenewInfo.isHidden = false
            self.btnUpgradeSubscription.isHidden = true
            self.btnEndSubscription.isHidden = true
            self.renewHeightConstraint.constant = 31.0
            self.upgradeHeightConstraint.constant = 0.0
        }
        else
        {
            self.btnUpgradeSubscription.isHidden = false
            self.btnEndSubscription.isHidden = false
            self.btnRenewSubscription.isHidden = true
            self.renewHeightConstraint.constant = 0.0
            self.upgradeHeightConstraint.constant = 31.0
            self.btnRenewInfo.isHidden = true
        }
    }
    
    //MARK: - Load UIView
    
    func loadRenewSubscription()
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        self.viewRenewSubscription = Bundle.main.loadNibNamed("RenewSubscriptionView", owner: nil, options: nil)![0] as! RenewSubscriptionView
        self.viewActive.addSubview(self.viewRenewSubscription)
        self.viewActive.bringSubviewToFront(self.viewRenewSubscription)
        //self.viewServiceCallback.frame = CGRect(x: 10, y: self.viewActive.frame.size.height/2, width: self.viewActive.frame.size.width - 20, height: self.viewActive.frame.size.height/2)
        self.viewRenewSubscription.frame.origin = self.viewActive.bounds.origin
        self.viewRenewSubscription.center = self.viewActive.convert(self.viewActive.center, from: self.viewRenewSubscription)
        //self.viewServiceCallback.center = self.view.center
        self.viewRenewSubscription.layer.cornerRadius = 4.0
        self.viewRenewSubscription.btnUpdateAndRenew.layer.cornerRadius = 4.0
        
        //self.viewReportLossItem.btnReportLoss.layer.borderWidth = 1.0
        self.viewRenewSubscription.btnUpdateAndRenew.addTarget(self, action:#selector(self.btnUpdateAndRenew_Click(sender:)), for: .touchUpInside)
        self.viewRenewSubscription.btnUpdateAndRenew.isSelected = false
        self.viewRenewSubscription.btnKYCCheckbox.addTarget(self, action:#selector(self.btnKYCCheckbox_Click(sender:)), for: .touchUpInside)
        
        self.setTenureDropDown(strViewName: "Renew")
        self.setKmLimitDropDown(strViewName: "Renew")
    }
    
    func loadEndSubscription()
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        self.viewEndSubscription = Bundle.main.loadNibNamed("EndSubscriptionView", owner: nil, options: nil)![0] as! EndSubscriptionView
        self.viewActive.addSubview(self.viewEndSubscription)
        self.viewActive.bringSubviewToFront(self.viewEndSubscription)
        self.viewEndSubscription.frame.origin = self.viewActive.bounds.origin
        self.viewEndSubscription.frame.size.height = 300
        self.viewEndSubscription.center = self.viewActive.convert(self.viewActive.center, from: self.viewEndSubscription)
        //self.viewServiceCallback.center = self.view.center
        self.viewEndSubscription.layer.cornerRadius = 4.0
        self.viewEndSubscription.btnEndSubscription.layer.cornerRadius = 4.0

        self.viewEndSubscription.btnEndSubscription.addTarget(self, action:#selector(self.btnEndSubscription_Click(sender:)), for: .touchUpInside)
        self.viewEndSubscription.btnTerminationDate.addTarget(self, action:#selector(self.btnTerminationDate_Click(sender:)), for: .touchUpInside)
        self.viewEndSubscription.preClosureHeightConstraint.constant = 0.0
        self.viewEndSubscription.viewPreclosure.isHidden = true
        
        //let strDate = self.getCurrentDate()
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        //print("nextMonth: ", nextMonth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let strNextOneMonth = formatter.string(from: nextMonth!)
        self.viewEndSubscription.txtTerminationDate.text = strNextOneMonth
        //self.setTenureDropDown(strViewName: "Renew")
    }
    
    func hideSubscriptionView()
    {
        viewTransparent.removeFromSuperview()
        if (self.viewRenewSubscription.superview != nil)
        {
            self.viewRenewSubscription.removeFromSuperview()
        }
        if (self.viewUpgradeSubscription.superview != nil)
        {
            self.viewUpgradeSubscription.removeFromSuperview()
        }
        if (self.viewEndSubscription.superview != nil)
        {
            self.viewEndSubscription.removeFromSuperview()
        }
    }
    
    @objc func handleReferralTap(_ sender: UITapGestureRecognizer)
    {
        viewTransparent.removeFromSuperview()
        if self.viewRenewSubscription.superview != nil
        {
            self.viewRenewSubscription.removeFromSuperview()
        }
        if self.viewUpgradeSubscription.superview != nil
        {
            self.viewUpgradeSubscription.removeFromSuperview()
        }
        if self.viewRenewInfo.superview != nil
        {
            self.viewRenewInfo.removeFromSuperview()
        }
        if (self.viewEndSubscription.superview != nil)
        {
            self.viewEndSubscription.removeFromSuperview()
        }
    }
    
    //MARK: - Load XIB
    func loadRenewInfoView(strMessage : String)
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        let viewRenew = UIView()
        viewRenew.frame = CGRect(x: 50, y: 50, width: self.viewActive.frame.size.width - 50, height: 200)
        self.viewActive.addSubview(viewRenew)
        self.viewActive.bringSubviewToFront(viewRenew)
        
        viewRenew.backgroundColor = UIColor.white
        viewRenew.layer.cornerRadius = 4.0
        viewRenew.frame.origin = self.viewActive.bounds.origin
        viewRenew.center = self.viewActive.convert(self.viewActive.center, from: viewRenew)
        let lblTitle = UILabel()
        lblTitle.frame = CGRect(x: viewRenew.frame.size.width/4, y: 30, width: viewRenew.frame.size.width - 30, height: 25)
        lblTitle.text = "RENEW SUBSCRIPTION"
        lblTitle.textColor = Constants.RED_COLOR
        lblTitle.font = UIFont(name:"Roboto-Medium", size: 15.0)
        viewRenew.addSubview(lblTitle)
        let lblSubTitle = UILabel()
        lblSubTitle.frame = CGRect(x: 40, y: 30, width: 250, height: 150)
        lblSubTitle.text = "Renew Subscription means lorem ipsum text lorem ipsum text line 2 lorem ipsum"
        lblSubTitle.textColor = UIColor.black
        lblSubTitle.font = UIFont(name:"Roboto-Medium", size: 14.0)
        lblSubTitle.numberOfLines = 3
        viewRenew.addSubview(lblSubTitle)
        self.viewRenewInfo = viewRenew
    }
    
    func loadEndAndUpgradeSubscription(strButtonTitle: String?)
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
           
        self.viewUpgradeSubscription = Bundle.main.loadNibNamed("UpgradeSubscriptionView", owner: nil, options: nil)![0] as! UpgradeSubscriptionView
        self.viewActive.addSubview(self.viewUpgradeSubscription)

        self.viewUpgradeSubscription.frame.origin = self.viewActive.bounds.origin
        self.viewUpgradeSubscription.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpgradeSubscription)

        self.viewUpgradeSubscription.layer.cornerRadius = 4.0
        self.viewUpgradeSubscription.btnSubmitRequest.layer.cornerRadius = 4.0
        self.viewUpgradeSubscription.txtUpgradeDate.text! = self.getCurrentDate()
        
        self.viewUpgradeSubscription.lblUpgradeAmount.isHidden = true
        
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        //print("nextMonth: ", nextMonth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let strNextOneMonth = formatter.string(from: nextMonth!)
        self.viewUpgradeSubscription.txtUpgradeDate.text = strNextOneMonth
        
        self.viewUpgradeSubscription.btnKmUsageCheckbox.addTarget(self, action:#selector(self.btnKmUsageCheckbox_Click(sender:)), for: .touchUpInside)
        self.viewUpgradeSubscription.btnKmUsageCheckbox.isSelected = false
        
        self.viewUpgradeSubscription.btnTenureCheckbox.addTarget(self, action:#selector(self.btnTenureCheckbox_Click(sender:)), for: .touchUpInside)
        self.viewUpgradeSubscription.btnTenureCheckbox.isSelected = false
        
        self.viewUpgradeSubscription.btnCarModelCheckbox.addTarget(self, action:#selector(self.btnCarModelCheckbox_Click(sender:)), for: .touchUpInside)
        self.viewUpgradeSubscription.btnCarModelCheckbox.isSelected = false
        self.isCarModelSelected = "0"
        
        if strButtonTitle == "UPGRADE SUBSCRIPTION"
        {
            self.viewUpgradeSubscription.lblSubscriptionTitle.isHidden = false
            self.viewUpgradeSubscription.lblSubscriptionTitle.text! = "UPGRADE SUBSCRIPTION"
        }
//        else if strButtonTitle == "END SUBSCRIPTION"
//        {
//            self.viewUpgradeSubscription.lblSubscriptionTitle.isHidden = false
//            self.viewUpgradeSubscription.lblSubscriptionTitle.text! = "END SUBSCRIPTION"
//        }
           
        self.viewUpgradeSubscription.btnSubmitRequest.addTarget(self, action:#selector(self.btnSubmitRequest_Click(sender:)), for: .touchUpInside)
        self.viewUpgradeSubscription.btnUpgradeDate.addTarget(self, action:#selector(self.btnUpgradeDate_Click(sender:)), for: .touchUpInside)
        
        self.setTenureDropDown(strViewName: "Upgrade")
        self.setKmLimitDropDown(strViewName: "Upgrade")
    }
    
    //MARK: - DropDown Methods
    
    func setTenureDropDown(strViewName: String)
    {
        //let arrTenureList = ["6 Months", "12 Months", "18 Months", "24 Months", "30 Months", "36 Months", "42 Months", "48 Months", "54 Months", "60 Months"]
        //let arrTenureList = ["8-10 hr","10-12 hr", "12-14 hr", "14-16 hr" ,"16-18 hr"]
        if self.arrTenureList.count > 0
        {
            for i in 0..<self.arrTenureList.count
            {
                let strTenure = arrTenureList[i]
                if strViewName == "Upgrade"
                {
                    self.viewUpgradeSubscription.txtTenure.optionArray.append(strTenure as! String)
                }
                else if strViewName == "Renew"
                {
                    self.viewRenewSubscription.txtTenure.optionArray.append(strTenure as! String)
                }
            }
            if strViewName == "Upgrade"
            {
                viewUpgradeSubscription.txtTenure.arrowColor = UIColor.black
                viewUpgradeSubscription.txtTenure.selectedRowColor = Constants.RED_COLOR
                viewUpgradeSubscription.txtTenure.checkMarkEnabled = false
                viewUpgradeSubscription.txtTenure.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                    self.viewUpgradeSubscription.txtTenure.text = selectedText
                    self.viewUpgradeSubscription.txtTenure.textColor = UIColor.black
                    let arrValue = selectedText.components(separatedBy: " ")
                    let strFinalDuration = arrValue[0]
                    self.valueForMonth = Int(strFinalDuration)
                    self.viewUpgradeSubscription.lblTenureValidation.text = ""
                }
            }
            else if strViewName == "Renew"
            {
                viewRenewSubscription.txtTenure.arrowColor = UIColor.black
                viewRenewSubscription.txtTenure.selectedRowColor = Constants.RED_COLOR
                viewRenewSubscription.txtTenure.checkMarkEnabled = false
                viewRenewSubscription.txtTenure.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                self.viewRenewSubscription.txtTenure.textColor = UIColor.black
                }
            }
        }
        else
        {
            if strViewName == "Upgrade"
            {
                //self.viewUpgradeSubscription.txtTenure.isEditing = false
                if self.arrTenureList.count == 0
                {
                    viewUpgradeSubscription.txtTenure.arrowSize = 0.0
                }
            }
            else if strViewName == "Renew"
            {
                viewRenewSubscription.txtTenure.arrowSize = 0.0
            }
        }
    }
    
    func get45DaysForSubscription()
    {
        let dateB = Date()
        let strDate = "2021-02-05"//"02/05/2021"

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd" //"mm/dd/yyyy" //"yyyy-MM-dd'T'HH:mm:ssZ"
        let dateA = dateFormatter.date(from:strDate)!

        let diffInDays = Calendar.current.dateComponents([.day], from: dateA, to: dateB).day
        print("diffInDays: ", diffInDays as Any)
        
        /*let previousDate = "02/05/2021"
        //let strCurrentDate = self.getCurrentDate()
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "mm/dd/yyyy"//"yyyy-MM-dd"
        let previousDateFormated : Date? = dateFormatter.date(from: previousDate)
        //let currentDateFormatted : Date? = dateFormatter.date(from: strCurrentDate)
        //let difference = currentDateFormatted!.timeIntervalSince(previousDateFormated!)
        let difference = previousDateFormated!.timeIntervalSince(currentDate)
        //let difference = currentDate.timeIntervalSince(previousDateFormated!)
        let differenceInDays = Int(difference/(60 * 60 * 24 ))
        print("differenceInDays: ", differenceInDays)*/
    }
    
    func setKmLimitDropDown(strViewName: String)
    {
        //let arrKmLimitList = ["1000 kms","1260 kms","1600 kms", "2000 kms", "3000 kms" ,"3920 kms"]
        if self.arrKmList.count > 0
        {
            for i in 0..<self.arrKmList.count
            {
                let strKmLimit = self.arrKmList[i]
                if strViewName == "Upgrade"
                {
                    self.viewUpgradeSubscription.txtKmUsage.optionArray.append(strKmLimit as! String)
                }
                else if strViewName == "Renew"
                {
                    self.viewRenewSubscription.txtKmLimit.optionArray.append(strKmLimit as! String)
                }
            }
            if strViewName == "Upgrade"
            {
                viewUpgradeSubscription.txtKmUsage.arrowColor = UIColor.black
                viewUpgradeSubscription.txtKmUsage.selectedRowColor = Constants.RED_COLOR
                viewUpgradeSubscription.txtKmUsage.checkMarkEnabled = false
                viewUpgradeSubscription.txtKmUsage.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                self.viewUpgradeSubscription.txtKmUsage.textColor = UIColor.black
                self.viewUpgradeSubscription.lblKMValidation.text = ""
                }
            }
            else if strViewName == "Renew"
            {
                viewRenewSubscription.txtKmLimit.arrowColor = UIColor.black
                viewRenewSubscription.txtKmLimit.selectedRowColor = Constants.RED_COLOR
                viewRenewSubscription.txtKmLimit.checkMarkEnabled = false
                viewRenewSubscription.txtKmLimit.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                self.viewRenewSubscription.txtKmLimit.textColor = UIColor.black
                }
            }
        }
        else
        {
           if strViewName == "Upgrade"
            {
                viewUpgradeSubscription.txtKmUsage.arrowSize = 0.0
            }
            else if strViewName == "Renew"
            {
                viewRenewSubscription.txtKmLimit.arrowSize = 0.0
            }
        }
    }
    
    func getCurrentDate() -> String
    {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yyyy"
        let dateString = df.string(from: date)
        return dateString
    }
    
    //MARK: - Methods Of Webservices
    
    func getKmAndTenureForUpgrade()
    {
        //let hud = CommonFunctions.showProgressHudLight(view: self.viewRenewSubscription)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        let dictParam = ["Subscriptionbookingid":self.objSubscriptionDetail?.subscriptionBookingid] as [String:AnyObject]
        APIFunctions.sharedInstance.getKmAndTenureAPI(APIConstants.API_GetKmAndTenureForUpgrade, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            //hud!.dismiss()
         
            DispatchQueue.main.async {
                         
                if error != nil
                {
                }
                else
                {
                    let dictData = response as? NSDictionary
                    print("dictData: ", dictData)
                    
                    if dictData?.value(forKey: "Km") is NSNull {
                        // do something with null JSON value here
                    }
                    else
                    {
                        self.arrKmList = dictData?.value(forKey: "Km") as! NSArray
                    }
                    
                    if dictData?.value(forKey: "Tenure") is NSNull {
                        // do something with null JSON value here
                    }
                    else
                    {
                        self.arrTenureList = dictData?.value(forKey: "Tenure") as! NSArray
                    }
                    //let arrKM = dictData?.value(forKey: "Km") as! NSArray
                    //let arrTenure = dictData?.value(forKey: "Tenure") as! NSArray
                }
            }
         })
    }
        
    func checkPriceForSubscriptionWebService(strTenure: String, strKms: String)
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.viewRenewSubscription)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders)
        //string subscriptionbookingId, string Kms, string Tenure
        let arrValue = strKms.components(separatedBy: " ")
        let strFinalKMs = arrValue[0]
        
        let arrTenure = strTenure.components(separatedBy: " ")
        let strFinalTenure = arrTenure[0]
        
        let dictParam = ["subscriptionbookingId":self.objSubscriptionDetail?.subscriptionBookingid,
             "Kms":strFinalKMs,
             "Tenure":strFinalTenure] as [String:AnyObject]
        APIFunctions.sharedInstance.checkPriceForRenewSubscriptionAPI(APIConstants.API_CheckPriceForRenewSubscription, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            hud!.dismiss()
         
            DispatchQueue.main.async {
                         
                if error != nil
                {
                }
                else
                {
                    let dictData = response as! NSDictionary
                    print("dictData: ", dictData)
                    self.dictSubscription = dictData
                    self.viewRenewSubscription.lblPriceDetail.text! = String(format: "%@ %@", self.dictSubscription?.value(forKey: "message") as! String, self.dictSubscription?.value(forKey: "Price") as! String)
                    self.isCheckedPice = true
                    /*{
                        PkgId = 35798;
                        Price = "73644.00";
                        message = "Your renew Subcription price is:";
                        status = 1;
                    }*/
                }
            }
         })
    }
    
    func requestForRenewSubscriptionWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.viewRenewSubscription)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders)
        //string subscriptionbookingId, string Kms, string Tenure
        let arrTenure = self.viewRenewSubscription.txtTenure.text!.components(separatedBy: " ")
        let strFinalTenure = arrTenure[0]
        let dictParam = ["subscriptionbookingId":self.objSubscriptionDetail?.subscriptionBookingid as AnyObject,
             "Tenure":strFinalTenure as AnyObject,
             "PkgId":self.dictSubscription?.value(forKey: "PkgId") as! String,
             "Price":self.dictSubscription?.value(forKey: "Price") as! String] as [String:AnyObject]
        APIFunctions.sharedInstance.renewSubscriptionRequestAPI(APIConstants.API_RenewSubscriptionRequest, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            hud!.dismiss()
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    let strMessage  = response.value(forKey: "Message") as? String
                    CommonMethodsMylesZero.popupAlert(title: strMessage, message: "", actionTitles: ["Ok"], actions:[{action1 in
                        self.hideSubscriptionView()
                        
                    }, nil], viewController: self)
                    /*{
                        Message = "Subscription Renew request is successfully submitted please note the Request id. Request id: 119733";
                        RequestId = 119733;
                        Status = 1;
                    }*/
                }
            }
         })
    }
    
    func getSubscriptionRequestForUpgrade()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.viewRenewSubscription)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        
        let dictParam = ["subscriptionbookingId":self.objSubscriptionDetail?.subscriptionBookingid as AnyObject,
            "PerferredDte":(self.viewUpgradeSubscription.txtUpgradeDate.text ?? "") as AnyObject,
            "IsKm":(self.viewUpgradeSubscription.txtKmUsage.text ?? "0") as AnyObject,
            "IsTenure":(self.viewUpgradeSubscription.txtTenure.text ?? "0") as AnyObject,
            "IsCar":(self.isCarModelSelected ?? "0") as AnyObject] as [String:AnyObject]
        APIFunctions.sharedInstance.getRequestForUpgradeSubscriptionAPI(APIConstants.API_SubsriptionUpgradeRequest, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            hud!.dismiss()
         
            DispatchQueue.main.async {
                         
                if error != nil
                {
                }
                else
                {
                    print("response: ", response)
                    self.subscriptionTerminationWebService(strDate: self.viewUpgradeSubscription.txtUpgradeDate.text ?? "", strUsedFor: "Upgrade")
                    self.strDateForUpgradation = self.viewUpgradeSubscription.txtUpgradeDate.text
                    //let dictData = response as? NSDictionary
                    //print("dictData: ", dictData)
                }
            }
         })
    }
    
    func subscriptionTerminationWebService(strDate: String, strUsedFor: String)
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.viewUpgradeSubscription)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        let dictParam = ["subscriptionbookingId":self.objSubscriptionDetail?.subscriptionBookingid as AnyObject,
             "PreferredTerminationDate":strDate] as [String:AnyObject]
        //self.viewEndSubscription.txtTerminationDate.text as AnyObject
        APIFunctions.sharedInstance.subscriptionTerminationAPI(APIConstants.API_SubscriptionTermination, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            hud!.dismiss()
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    let dictData = response as! NSDictionary
                    print("dictData: ", dictData)
                    self.dictTermination = dictData
                    if strUsedFor == "End"
                    {
                        self.viewEndSubscription.preClosureHeightConstraint.constant = 200.0
                        self.viewEndSubscription.viewPreclosure.isHidden = false
                        self.viewEndSubscription.frame.origin.y = 50
                        self.viewEndSubscription.frame.size.height = 500
                        //self.viewEndSubscription.center = self.viewActive.convert(self.viewActive.center, from: self.viewEndSubscription)
                        self.viewEndSubscription.lblRevisedRental.text = String(format: "\u{20B9} %d", self.dictTermination?.value(forKey: "RevisedRental") as! Int)
                        self.viewEndSubscription.lblExistingRental.text = String(format: "\u{20B9} %d", self.dictTermination?.value(forKey: "ExistingRental") as! Int)
                        self.viewEndSubscription.lblDifferenceAmount.text = String(format: " \u{20B9} %d", self.dictTermination?.value(forKey: "DifferenceAmount") as! Int)
                        self.viewEndSubscription.lblTotalPreClosureAmount.text = String(format: "\u{20B9} %d", self.dictTermination?.value(forKey: "TotalPreCloserCharges") as! Int)
                        self.viewEndSubscription.lblTotalPreClosurePeriod.text = String(format: "Total Pre-closure Charge (%d Months): ", self.dictTermination?.value(forKey: "Period") as! Int)
                    }
                    else if strUsedFor == "Upgrade"
                    {
                        self.viewUpgradeSubscription.lblUpgradeAmount.isHidden = false
                        self.viewUpgradeSubscription.lblUpgradeAmount.text = String(format: "Upgrade Amount for the subscription: \u{20B9} %d", self.dictTermination?.value(forKey: "TotalPreCloserCharges") as! Int)
                    }
                        
                    //self.subscriptionTerminationRequestWebService()
                    
                    /*{
                        DifferenceAmount = 68236;
                        ExistingRental = 68236;
                        NewPkgID = 43802;
                        Period = 2;
                        RevisedRental = 68236;
                        Status = 1;
                        TotalPreCloserCharges = 136472;
                    }*/
                    /*{
                        DifferenceAmount = 0;
                        ExistingRental = 0;
                        NewPkgID = 0;
                        Period = 0;
                        RevisedRental = 0;
                        Status = 0;
                        TotalPreCloserCharges = 0;
                    }*/
                }
            }
         })
    }
    
    func subscriptionTerminationRequestWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.viewUpgradeSubscription)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        let strDate = self.getCurrentDate()
        
        let dictParam = ["subscriptionbookingId":self.objSubscriptionDetail?.subscriptionBookingid as AnyObject,
             "PreferredTerminationDate":strDate as AnyObject,
             "newPkgId":self.dictTermination?.value(forKey: "NewPkgID") as! Int,
             "TotalPreClosureAmt":self.dictTermination?.value(forKey: "TotalPreCloserCharges") as! Int,
             "Remark":""] as [String:AnyObject]
        APIFunctions.sharedInstance.subscriptionTerminationRequestAPI(APIConstants.API_SubscriptionTerminationRequest, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            hud!.dismiss()
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    let strMessage  = response.value(forKey: "Message") as? String
                    CommonMethodsMylesZero.popupAlert(title: strMessage, message: "", actionTitles: ["Ok"], actions:[{action1 in
                        self.hideSubscriptionView()
                        
                    }, nil], viewController: self)
                }
            }
         })
    }
    
    func createUpdareSubscriptionWebservice()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.viewUpgradeSubscription)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        let strDate = self.getCurrentDate()
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as! UserProfileInfoResponse
        
        let dictParam = ["clientid" : strClientID as AnyObject, //userDetails.clientID
                     "amt" : "",
                     "cityid" : objSubscriptionDetail?.cityName as AnyObject,
                     "variantid" : objSubscriptionDetail?.carVariantName as AnyObject,
                     "aadharno" : objSubscriptionDetail?.adharNo as AnyObject,
                     "aadharfile" : UserProfileInfo.aadharfile as AnyObject,
                     "dlno" : objSubscriptionDetail?.dLNo as AnyObject,
                     "dlfile" : UserProfileInfo.dlfile as AnyObject,
                     "panno" : objSubscriptionDetail?.panNo as AnyObject,
                     "panfile" : UserProfileInfo.panfile as AnyObject,
                     "filepath" : "",
                     "pkgid" : "",
                     "flag" : "",
                     "Tenure" : objSubscriptionDetail?.subscriptionTenureMonths as AnyObject,
                     "DLExpiryDate" : "",
                     "PassportExpiryDate" : "",
                     "CarColor" : objSubscriptionDetail?.carColor as AnyObject,
                     "CoricId" : objSubscriptionDetail?.coricId as AnyObject,
                     "doctype" : "",
                     "address" : "",
                     "deliverydate" : "",
                     "OldSubId" : objSubscriptionDetail?.subscriptionId as AnyObject,
                     "NewPkgID" : self.dictTermination?.value(forKey: "NewPkgID") as! Int,
                     "TotalPreClosureAmt" : self.dictTermination?.value(forKey: "TotalPreCloserCharges") as! Int,
                     "PreferredTerminationDate" : strDate as AnyObject,
                     "Remark" : ""] as [String : AnyObject]

        APIFunctions.sharedInstance.createUpdareSubscriptionAPI(APIConstants.API_CreateUpgradeSubscription, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            hud!.dismiss()
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    let strMessage  = response.value(forKey: "Message") as? String
                    CommonMethodsMylesZero.popupAlert(title: strMessage, message: "", actionTitles: ["Ok"], actions:[{action1 in
                        self.hideSubscriptionView()
                        
                    }, nil], viewController: self)
                }
            }
         })
    }
    
    func searchCarWebService()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.viewActive)
        
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        //let strClientID = String(format: "%@", userDetails.clientcoID)
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        let objCityList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kCityList) as? CityListMylesZeroResponse
        self.valueForMonth = 6
        let strFromDate = CommonFunctions.getCurrentDate()
        let nextMonth = Calendar.current.date(byAdding: .month, value: self.valueForMonth!, to: Date())
        //print("nextMonth: ", nextMonth)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let strNextMonth = formatter.string(from: nextMonth!)
            
        searchCarParam = ["Cityid" : Int(objCityList!.cityId!)!,
                          "Duration": self.valueForMonth!,
                          "FromDate": strFromDate,
                          "ToDate": strNextMonth,
                          "ClientCoId": Int(strClientID) ?? kCustomerCoID,//kCustomerCoID,
                          "km": Int(self.viewUpgradeSubscription.txtKmUsage.text!)!,
                          "CarType": "All",
                          "OldSubscriptionId": self.objSubscriptionDetail!.subscriptionBookingid!] as [String : AnyObject]
        //(self.viewUpgradeSubscription.txtUpgradeDate.text ?? "") as AnyObject
        
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders as Any)
        APIFunctions.sharedInstance.searchCarForUpgradeSubscription(APIConstants.API_SearchSubscriptionForUpgrade, dataDict: searchCarParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
            
            DispatchQueue.main.async {
                hud!.dismiss()
                
                if error != nil
                {
                    
                }
                else
                {
                    //print("Json Login: ", response)
                    let Json = response.value(forKey: "response") as! NSDictionary
                    let arrCarList = Json.value(forKey: "Cars") as! NSArray
                    //print("cars1: ", arrCarList )
                    let dictFilterList = Json.value(forKey: "Fitlers") as! NSDictionary
                    //kFilterData
                    self.objSearchCarFilter?.fuelname = dictFilterList.value(forKey: "fuelname") as? [String]
                    
                    DefaultsValues.setDictionaryValueToUserDefaults(dictFilterList, forKey: kFilterData)
                    
                    let objCarListVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_CarList) as! CarListVC
                    objCarListVC.arrCarListInfo = arrCarList
                    objCarListVC.dictSelectedCarInfo = self.searchCarParam
                    objCarListVC.isUpgradeSelected = true
                    objCarListVC.objSubscriptionDetail = self.objSubscriptionDetail
                    objCarListVC.dictUpgradeData = self.dictTermination
                    objCarListVC.strDateForUpgradation = self.strDateForUpgradation
                    //self.strDateForUpgradation
                    APIFunctions.dictGlobalCarFilter = dictFilterList
                    //DefaultsValues.setDictionaryValueToUserDefaults(self.searchCarParam as NSDictionary?, forKey: kSearchParamKey)
                    self.navigationController?.pushViewController(objCarListVC, animated: true)
                }
            }
        })
    }
    
    //
    
    //MARK: - Button Action
    
    @objc func dateSelected()
    {
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "MM/dd/yyyy" //"dd/MM/yyyy"
        let dateString1 = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(dateString1)")
        
        self.viewUpgradeSubscription.txtUpgradeDate.text = dateString1
        self.viewUpgradeSubscription.txtUpgradeDate.textColor = UIColor.black
    }
    
    @objc func btnEndSubscription_Click(sender: UIButton!)
    {
        self.subscriptionTerminationWebService(strDate: self.viewEndSubscription.txtTerminationDate.text ?? "", strUsedFor: "End")
    }
    
    @objc func btnTerminationDate_Click(sender: UIButton!)
    {
        CommonMethodsMylesZero.showDateTimePicker(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        self.datePickerView.maximumDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
    }

    @objc func btnUpgradeDate_Click(sender: UIButton!)
    {
        CommonMethodsMylesZero.showDateTimePicker(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        self.datePickerView.maximumDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
    }
    
    @objc func btnKmUsageCheckbox_Click(sender: UIButton!)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "checkbox_unfill"), for: UIControl.State.normal)
            sender.isSelected = false
        }
        else
        {
            sender.setImage(UIImage(named: "checkbox_fill"), for: UIControl.State.normal)
            sender.isSelected = true
           //self.viewUpgradeSubscription.lblKMValidation.text = ""
        }
    }
    
    @objc func btnKYCCheckbox_Click(sender: UIButton!)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "checkbox_unfill"), for: UIControl.State.normal)
            sender.isSelected = false
        }
        else
        {
            sender.setImage(UIImage(named: "checkbox_fill"), for: UIControl.State.normal)
            sender.isSelected = true
        }
    }
    
    @objc func btnTenureCheckbox_Click(sender: UIButton!)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "checkbox_unfill"), for: UIControl.State.normal)
            sender.isSelected = false
        }
        else
        {
            sender.setImage(UIImage(named: "checkbox_fill"), for: UIControl.State.normal)
            sender.isSelected = true
            //self.viewUpgradeSubscription.lblTenureValidation.text = ""
        }
    }
    
    @objc func btnCarModelCheckbox_Click(sender: UIButton!)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "checkbox_unfill"), for: UIControl.State.normal)
            sender.isSelected = false
            self.isCarModelSelected = "0"
        }
        else
        {
            sender.setImage(UIImage(named: "checkbox_fill"), for: UIControl.State.normal)
            sender.isSelected = true;
            self.isCarModelSelected = "1"
        }
    }
    
    @objc func btnUpdateAndRenew_Click(sender: UIButton!)
    {
        if self.isCheckedPice == true
        {
            self.requestForRenewSubscriptionWebService()
        }
        else
        {
            self.checkPriceForSubscriptionWebService(strTenure: self.viewRenewSubscription.txtTenure.text!, strKms: self.viewRenewSubscription.txtKmLimit.text!)
        }
    }
    
    @objc func btnSubmitRequest_Click(sender: UIButton!)
    {
//        if self.viewUpgradeSubscription.lblSubscriptionTitle.text == "UPGRADE SUBSCRIPTION"
//        {
//
//        }
        
        if self.isCarModelSelected == "0"
        {
            if self.viewUpgradeSubscription.txtKmUsage.text == ""
            {
                self.viewUpgradeSubscription.lblKMValidation.text = "Please select the KM for upgrade"
            }
            else if self.arrTenureList.count > 0
            {
                if self.viewUpgradeSubscription.txtTenure.text == ""
                {
                    self.viewUpgradeSubscription.lblTenureValidation.text = "Please select the tenure for upgrade"
                }
            }
            else
            {
               self.viewUpgradeSubscription.lblKMValidation.text = ""
               self.viewUpgradeSubscription.lblTenureValidation.text = ""
                self.subscriptionTerminationWebService(strDate: self.viewUpgradeSubscription.txtUpgradeDate.text ?? "", strUsedFor: "Upgrade")
               self.getSubscriptionRequestForUpgrade()
            }
        }
        else if self.isCarModelSelected == "1"
        {
            if self.viewUpgradeSubscription.txtKmUsage.text == nil
            {
                self.viewUpgradeSubscription.lblKMValidation.text = "Please select the KM for upgrade"
            }
            else if self.viewUpgradeSubscription.txtTenure.text == nil
            {
                self.viewUpgradeSubscription.lblTenureValidation.text = "Please select the tenure for upgrade"
            }
            else
            {
                self.viewUpgradeSubscription.lblKMValidation.text = ""
                self.viewUpgradeSubscription.lblTenureValidation.text = ""
                self.searchCarWebService()
            }
        }
    }
    
    @IBAction func btnRenewInfo_Click(_ sender: Any)
    {
        self.loadRenewInfoView(strMessage: "")
    }
    
    @IBAction func btnRenew_Click(_ sender: Any)
    {
        self.loadRenewSubscription()        
    }
    
    @IBAction func btnUpdate_Click(_ sender: UIButton)
    {
        self.btnUpgradeSubscription.tag = sender.tag
        self.loadEndAndUpgradeSubscription(strButtonTitle: sender.currentTitle)
    }
    
    @IBAction func btnEnd_Click(_ sender: UIButton)
    {
        self.btnEndSubscription.tag = sender.tag
        self.loadEndSubscription()
    }
}
