//
//  ServiceTabVC.swift
//  Myles Zero
//
//  Created by skpissay on 16/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import Alamofire
import iOSDropDown
import AWSS3
import AWSCore
import WebKit

class ServiceTabVC: UIViewController
{
    @IBOutlet weak var txtSelectedDocument: DropDown!
    @IBOutlet weak var btnDownloadDocument: UIButton!
    @IBOutlet weak var btnDocumentType: UIButton!
    @IBOutlet weak var lblUpcomingService: UILabel!
    @IBOutlet weak var lblRoadsideHelpline: UILabel!
    @IBOutlet weak var btnEditService: UIButton!
    @IBOutlet weak var btnServiceHistory: UIButton!
 
    @IBOutlet weak var upcomingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnUpcomingRequestService: UIButton!
    //@IBOutlet weak var btnRequestRepair: UIButton!
    @IBOutlet weak var btnRequestCallback: UIButton!
    
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var arrDocumentsList : NSMutableArray?
    var arrServiceList : NSMutableArray?
    var arrCallbackReason : NSArray?
    
    var viewTransparent = UIView()
    var viewServiceCallback = RequestCallbackView()
    var viewUpcomingRequest = UpcomingRequestServiceVC()
    var viewEditUpcomingDate = EditUpcomingServiceDateView()
    var viewActive = UIView()
    var viewNoServiceHistory = UIView()
    var strCallbackReasonId : String?
    var strTimeSlot : String?
    var datePickerView = UIDatePicker()
    var historyStatus : Int = 0
    var arrHistoryResponse = NSArray()
    var arrDocuments = NSArray()
    
    var customView = UIView()
    
    @IBOutlet weak var btnEditUpcomingService: UIButton!
    @IBOutlet weak var viewUpcomingService: UIView!
    
    var strDocumentUrl : String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        CommonMethodsMylesZero.setBorder(button: self.btnServiceHistory)
        CommonMethodsMylesZero.setBorder(button: self.btnUpcomingRequestService)
        CommonMethodsMylesZero.setBorder(button: self.btnRequestCallback)
        //UpComingService = "1/1/1900 12:00:00 AM"
        self.GetScheduleServiceHistoryWebservice()

        //self.arrDocumentsList = ["Fitness", "Insurance", "RC", "Permit"]
        self.arrServiceList = ["Damage", "Service"]
        //self.showDocumentList()
        
        self.arrDocuments = (self.objSubscriptionDetail?.doc as NSArray?)!
        
        /*for i in 0..<self.arrDocuments.count
        {
            //let strStateName = (self.arrDocumentsList![i] as AnyObject) as! String
            //self.txtSelectedDocument.optionArray.append(strStateName)
            
            //let dict  = SubscriptionStatusDetailDoc.init(fromDictionary:((self.arrDocuments[i]) as? [String : Any])!)
            let objDoc = self.arrDocuments[i]  as! SubscriptionStatusDetailDoc
            let strDocType = objDoc.doctype as String
            //DefaultsValues.getCustomObjFromUserDefaults_(forKey: kDocuments) as? SubscriptionStatusDetailDoc
            //let dictSubscriptionDetail = self.arrSubscriptionList[indexPath.row] as! NSDictionary
            //DefaultsValues.setCustomObjToUserDefaults(dict, forKey: kDocuments)
            //let objDoc = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kDocuments) as? SubscriptionStatusDetailDoc
            self.arrDocumentsList?.add(strDocType)
            //self.arrDocumentsList?.add(objDoc as SubscriptionStatusDetailDoc)
        }*/
        self.showDocumentList()
        //self.lblDocumentType.text = objSubscriptionDetail!.cartype!
        //self.lblUpcomingService.text = objSubscriptionDetail!.upComingService!
    }
    
    //MARK: - Load UIView
    
    func hideServiceView()
    {
        viewTransparent.removeFromSuperview()
        if (self.viewUpcomingService.superview != nil)
        {
            self.viewUpcomingService.removeFromSuperview()
        }
        if (self.viewUpcomingRequest.superview != nil)
        {
            self.viewUpcomingRequest.removeFromSuperview()
        }
        if (self.viewNoServiceHistory.superview != nil)
        {
            self.viewNoServiceHistory.removeFromSuperview()
        }
        if (self.viewEditUpcomingDate.superview != nil)
        {
            self.viewEditUpcomingDate.removeFromSuperview()
        }
        if (self.viewUpcomingService.superview != nil)
        {
            self.viewUpcomingService.removeFromSuperview()
        }
    }
    
    @objc func handleReferralTap(_ sender: UITapGestureRecognizer)
    {
        viewTransparent.removeFromSuperview()
        if self.viewUpcomingRequest.superview != nil
        {
            self.viewUpcomingRequest.removeFromSuperview()
        }
        if self.viewNoServiceHistory.superview != nil
        {
            self.viewNoServiceHistory.removeFromSuperview()
        }
        if self.viewEditUpcomingDate.superview != nil
        {
            self.viewEditUpcomingDate.removeFromSuperview()
        }
    }
    
    func setServiceIDOrKmDrivenDropDown()
    {
        for i in 0..<self.arrHistoryResponse.count
        {
            let dictHistory = self.arrHistoryResponse[i] as! NSDictionary
            let strHistory = dictHistory.value(forKey: "ServiceScheduleID") as! String
            self.viewUpcomingRequest.txtKMLimit.optionArray.append(strHistory)
            //let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
        }
        self.viewUpcomingRequest.txtKMLimit.arrowColor = UIColor.black
        self.viewUpcomingRequest.txtKMLimit.selectedRowColor = Constants.RED_COLOR
        self.viewUpcomingRequest.txtKMLimit.checkMarkEnabled = false
        self.viewUpcomingRequest.txtKMLimit.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            let dictHistory  = ServiceScheduleHistoryRootClass.init(fromDictionary:(self.arrHistoryResponse[index]) as! [String : Any])
            DefaultsValues.setCustomObjToUserDefaults(dictHistory, forKey: "kServiceHistory")
            let objServiceDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: "kServiceHistory") as? ServiceScheduleHistoryRootClass
            self.viewUpcomingRequest.lblDateTime.text = objServiceDetail?.scheduledServiceDate
            self.viewUpcomingRequest.lblServiceType.text = objServiceDetail?.serviceType
            self.viewUpcomingRequest.lblKMDriven.text = objServiceDetail?.serviceKm
            self.viewUpcomingRequest.lblDescription.text = objServiceDetail?.serviceYN
            if objServiceDetail?.serviceKm == ""
            {
                self.viewUpcomingRequest.lblKMDriven.text = "0"
            }
            //let dictData = self.arrCallbackReason![index] as! NSDictionary
            //let reasonId = dictData.value(forKey: "ReasonID") as! Int
            //self.strCallbackReasonId = String(format: "%d", reasonId)
            self.viewUpcomingRequest.txtKMLimit.textColor = UIColor.black
        }
    }
    /*for i in 0..<self.arrSubscriptionList.count
    {
        let dictSubscription  = SubscriptionStatusDetailRootClass.init(fromDictionary:(self.arrSubscriptionList[i]) as! [String : Any])
        DefaultsValues.setCustomObjToUserDefaults(dictSubscription, forKey: kSubscriptionList)
        
        let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
        txtDropdown.optionArray.append(objSubscriptionDetail!.subscriptionId!)
        
        self.arrSubscriptionIDList.add(objSubscriptionDetail!.subscriptionId!)
        //print("self.arrSubscriptionIDList: ", self.arrSubscriptionIDList)*/
    
    func loadUpcomingEditDateView()
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        self.viewEditUpcomingDate = Bundle.main.loadNibNamed("UpcomingServiceView", owner: nil, options: nil)![0] as! EditUpcomingServiceDateView
        self.viewActive.addSubview(self.viewEditUpcomingDate)
        self.viewActive.bringSubviewToFront(self.viewEditUpcomingDate)
        //self.viewEditUpcomingDate.frame.size.height = 400
        self.viewEditUpcomingDate.frame.origin = self.viewActive.bounds.origin
        self.viewEditUpcomingDate.center = self.viewActive.convert(self.viewActive.center, from: self.viewEditUpcomingDate)
        self.viewEditUpcomingDate.layer.cornerRadius = 4.0
        /*let yourAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name:"Roboto-Medium", size: 14.0)!,
            .foregroundColor: Constants.RED_COLOR,
        .underlineStyle: NSUnderlineStyle.single.rawValue]

        let attributeString = NSMutableAttributedString(string: "Request Service / Repair",
                                                        attributes: yourAttributes)
        self.viewUpcomingRequest.btnRequestServiceRepair.setAttributedTitle(attributeString, for: .normal)
        
        self.viewUpcomingRequest.btnSubmitRequest.layer.cornerRadius = 4.0
        //self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
        
        self.viewUpcomingRequest.btnRequestServiceRepair.addTarget(self, action:#selector(self.btnRequestServiceRepair_Click(sender:)), for: .touchUpInside)
        self.viewUpcomingRequest.btnSubmitRequestForRepair.addTarget(self, action:#selector(self.btnSubmitRequestForRepair_Click(sender:)), for: .touchUpInside)
        self.viewUpcomingRequest.btnServiceDate.addTarget(self, action:#selector(self.btnServiceDate_Click(sender:)), for: .touchUpInside)
        
        self.viewUpcomingRequest.btnRequestServiceRepair.isSelected = true
        self.viewUpcomingRequest.btnRequestServiceRepair.backgroundColor = UIColor.clear*/
    }
    
    func loadServiceHistoryAndUpcomingRequestView(arrData : NSArray, strMessage : String, strViewName : String)
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        self.viewUpcomingRequest = Bundle.main.loadNibNamed("UpcomingRequestServiceVC", owner: nil, options: nil)![0] as! UpcomingRequestServiceVC
        self.viewActive.addSubview(self.viewUpcomingRequest)
        self.viewActive.bringSubviewToFront(self.viewUpcomingRequest)
        self.viewUpcomingRequest.frame.size.height = 400
        self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
        self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
        self.viewUpcomingRequest.layer.cornerRadius = 4.0
        self.viewUpcomingRequest.viewService.isHidden = false
        self.viewUpcomingRequest.serviceInfoHeightConstraint.constant = 320.0
        self.historyStatus = 1
        
        if strViewName == "Upcoming"
        {
            self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
            if self.historyStatus == 0
            {
                self.viewUpcomingRequest.viewService.isHidden = true
                self.viewUpcomingRequest.serviceInfoHeightConstraint.constant = 0.0
                self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = false
                
                self.viewUpcomingRequest.frame.size.height = self.viewActive.frame.size.height - 20
                self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
                self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
                self.viewUpcomingRequest.layer.cornerRadius = 4.0
                self.viewUpcomingRequest.serviceHeightConstraint.constant = 0.0
                self.viewUpcomingRequest.RequestServiceTopConstraint.constant = 20
                self.viewUpcomingRequest.viewServiceData.isHidden = true
                self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = false
            }
            else if self.historyStatus == 1
            {
                self.viewUpcomingRequest.frame.size.height = 400
                self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
                self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
                self.viewUpcomingRequest.layer.cornerRadius = 4.0
                self.viewUpcomingRequest.RequestServiceTopConstraint.constant = 150
                //self.viewServiceHistory.serviceHeightConstraint.constant = 0.0
                self.viewUpcomingRequest.viewServiceData.isHidden = false
                self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
            }
            
           
            
            self.viewUpcomingRequest.btnRequestServiceRepair.isHidden = false
            //self.viewUpcomingRequest.btnRequestServiceRepair.isSelected = true
            let yourAttributes: [NSAttributedString.Key: Any] = [
                .font: UIFont(name:"Roboto-Medium", size: 14.0)!,
                .foregroundColor: Constants.RED_COLOR,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
            //btnRequestServiceRepair
            let attributeString = NSMutableAttributedString(string: "Request Service / Repair",
                                                            attributes: yourAttributes)
            self.viewUpcomingRequest.btnRequestServiceRepair.setAttributedTitle(attributeString, for: .normal)
            
            self.viewUpcomingRequest.btnSubmitRequest.layer.cornerRadius = 4.0
            //self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
            
            self.viewUpcomingRequest.btnRequestServiceRepair.addTarget(self, action:#selector(self.btnRequestServiceRepair_Click(sender:)), for: .touchUpInside)
            self.viewUpcomingRequest.btnSubmitRequestForRepair.addTarget(self, action:#selector(self.btnSubmitRequestForRepair_Click(sender:)), for: .touchUpInside)
            self.viewUpcomingRequest.btnServiceDate.addTarget(self, action:#selector(self.btnServiceDate_Click(sender:)), for: .touchUpInside)
            
            self.viewUpcomingRequest.btnRequestServiceRepair.isSelected = true
            self.viewUpcomingRequest.btnRequestServiceRepair.backgroundColor = UIColor.clear
            //self.showServiceDetail()
            self.showTypeOfServiceList()
            /*
             self.viewUpcomingRequest.txtTypesOfService.didSelect{(selectedText, index, id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                
                //self.txtSelectedDocument.text = selectedText
                self.txtSelectedDocument.textColor = UIColor.black
            }*/
        }
        else if strViewName == "History"
        {
            self.viewUpcomingRequest.frame.size.height = 380
            self.viewUpcomingRequest.btnRequestServiceRepair.isHidden = true
            self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
        }
        
        if self.arrHistoryResponse.count > 0
        {
            for i in 0..<self.arrHistoryResponse.count
            {
                let dictHistory  = ServiceScheduleHistoryRootClass.init(fromDictionary:(self.arrHistoryResponse[i]) as! [String : Any])
                DefaultsValues.setCustomObjToUserDefaults(dictHistory, forKey: "kServiceHistory")
                           
                let objServiceDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: "kServiceHistory") as? ServiceScheduleHistoryRootClass
                self.viewUpcomingRequest.lblDateTime.text = objServiceDetail?.scheduledServiceDate
                self.viewUpcomingRequest.lblServiceType.text = objServiceDetail?.serviceType
                self.viewUpcomingRequest.lblKMDriven.text = objServiceDetail?.serviceKm
                self.viewUpcomingRequest.lblDescription.text = objServiceDetail?.serviceYN
                if objServiceDetail?.serviceKm == ""
                {
                    self.viewUpcomingRequest.lblKMDriven.text = "0"
                }

            }
             self.setServiceIDOrKmDrivenDropDown()
        }
       
    }
    
    func LoadNoHistoryView(strMessage : String)
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        let viewHistory = UIView()
        viewHistory.frame = CGRect(x: 50, y: 50, width: self.viewActive.frame.size.width - 50, height: 200)
        self.viewActive.addSubview(viewHistory)
        self.viewActive.bringSubviewToFront(viewHistory)
        self.viewNoServiceHistory = viewHistory
        
        viewHistory.backgroundColor = UIColor.white
        viewHistory.layer.cornerRadius = 4.0
        viewHistory.frame.origin = self.viewActive.bounds.origin
        viewHistory.center = self.viewActive.convert(self.viewActive.center, from: viewHistory)
        let lblTitle = UILabel()
        lblTitle.frame = CGRect(x: viewHistory.frame.size.width/3, y: 50, width: viewHistory.frame.size.width - 30, height: 25)
        lblTitle.text = "SERVICE HISTORY"
        lblTitle.textColor = Constants.RED_COLOR
        lblTitle.font = UIFont(name:"Roboto-Medium", size: 15.0)
        viewHistory.addSubview(lblTitle)
        let lblSubTitle = UILabel()
        lblSubTitle.frame = CGRect(x: viewHistory.frame.size.width/3, y: lblTitle.frame.origin.y + 25, width: viewHistory.frame.size.width - 30, height: 25)
        let strNoServiceHistory = strMessage
        lblSubTitle.text = strNoServiceHistory
        lblSubTitle.textColor = UIColor.black
        lblSubTitle.font = UIFont(name:"Roboto-Medium", size: 14.0)
        viewHistory.addSubview(lblSubTitle)
    }
   
    //MARK: - Methods Of Webservices
    
    //UpdateSlotsforUpcomingService
    
    func UpdateSlotsForUpcomingService()
    {
        /*for i in 0..<self.arrHistoryResponse.count
        {
            let dictHistory = self.arrHistoryResponse[i] as! NSDictionary
            let strHistory = dictHistory.value(forKey: "ServiceScheduleID") as! String
            
        }*/
        let dictHistory  = ServiceScheduleHistoryRootClass.init(fromDictionary:(self.arrHistoryResponse[0]) as! [String : Any])
        DefaultsValues.setCustomObjToUserDefaults(dictHistory, forKey: "kServiceHistory")
        let objServiceDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: "kServiceHistory") as? ServiceScheduleHistoryRootClass
          
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders)
       
        let dictParam = ["TrnsMid":objServiceDetail?.serviceScheduleID as
                             AnyObject,
                         "fromdateslot":objServiceDetail?.scheduledServiceDate as AnyObject,
                         "Todateslot":objServiceDetail?.scheduledServiceDate as AnyObject,
                         "timeslot":"",
                         "type": objServiceDetail?.serviceYN as AnyObject] as [String:AnyObject]
        APIFunctions.sharedInstance.updateUpcomingServiceSlots(APIConstants.API_UpdateSlotsForUpcomingService, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
                hud!.dismiss()
         
                if error != nil
                {
                }
                else
                {
                    let strMessage  = response.value(forKey: "message") as? String
                    CommonMethodsMylesZero.popupAlert(title: strMessage, message: "", actionTitles: ["Ok"], actions:[{action1 in
                        self.hideServiceView()
                        
                    }, nil], viewController: self)
                }
            }
         })
    }
    func RequestForServiceRepairWebservice()
    {
        let hud = CommonMethodsMylesZero.showProgressHudLight(view: self.view)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders)
        
        let dictParam = ["SubscriptionId":self.objSubscriptionDetail?.subscriptionId as
                             AnyObject,
                         "ScheduledFromDate":self.viewUpcomingRequest.txtPreferredDate.text as AnyObject,
                         "ScheduledToDate":self.viewUpcomingRequest.txtPreferredDate.text as AnyObject,
                         "PickUpAddress":self.viewUpcomingRequest.txtPickupAddress.text as AnyObject,
                         "Description":"",
                         "ContactPerson": self.viewUpcomingRequest.txtContactName.text as AnyObject,
                         "ContactNumber": self.viewUpcomingRequest.txtContactNumber.text as AnyObject] as [String:AnyObject]
        APIFunctions.sharedInstance.createServiceRepairAPI(APIConstants.API_CreateServiceSubscription, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
                hud!.dismiss()
         
                if error != nil
                {
                }
                else
                {
                    let strMessage  = response.value(forKey: "message") as? String
                    CommonMethodsMylesZero.popupAlert(title: strMessage, message: "", actionTitles: ["Ok"], actions:[{action1 in
                        self.hideServiceView()
                        
                    }, nil], viewController: self)
                }
            }
         })
    }
    
    func GetScheduleServiceHistoryWebservice()
    {
        //let hud = CommonFunctions.showProgressHudLight(view: self.view)
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        //print("dictHeaders SearchCarVC: ", dictHeaders)
        let dictParam = ["SubscriptionId":self.objSubscriptionDetail?.subscriptionId] as [String:AnyObject]
        APIFunctions.sharedInstance.getScheduleServiceHistory(APIConstants.API_GetScheduleServiceHistory, dataDict: dictParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
                
                //hud!.dismiss()
         
                if error != nil
                {
                }
                else
                {
                    //let JsonLogin = response as AnyObject
                    let arrResponse = response.value(forKey: "response") as! NSArray
                    print("arrResponse for service history: ", arrResponse)
                    self.arrHistoryResponse = arrResponse
                    //self.arrCallbackReason = arrResponse
                    let strMessage = response.value(forKey: "message") as! String
                    let status = response.value(forKey: "status") as Any
                    self.historyStatus = status as! Int
                    
                    if status as! Int == 0
                    {
                        //self.viewUpcomingService.isHidden = true
                        self.viewUpcomingService.isHidden = false
                        self.upcomingHeightConstraint.constant = 43.0
                                                
                        //self.LoadNoHistoryView(strMessage: strMessage)
                    }
                    else if status as! Int == 1
                    {
                       self.viewUpcomingService.isHidden = false
                        self.upcomingHeightConstraint.constant = 43.0
                       //self.loadServiceHistoryAndUpcomingRequestView(arrData: arrResponse, strMessage: response.value(forKey: "message") as! String, strViewName: "History")
                    }
                }
            }
         })
    }
    
    @objc func dateSelected()
    {
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "dd/MM/yyyy"
        let dateString1 = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(dateString1)")
        
        self.viewUpcomingRequest.txtPreferredDate.text = dateString1
        self.viewUpcomingRequest.txtPreferredDate.textColor = UIColor.black
    }
    
    //MARK: - Upload on AWS
    
    func downloadFileFromAWS()
    {
        //let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        let access = Constants.AWS_ACCESS_KEY
        let secret = Constants.AWS_SECRET_KEY
        let credentials = AWSStaticCredentialsProvider(accessKey: access, secretKey: secret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast1, credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //let s3BucketName = String(format: "%@/%@", Constants.AWS_S3_BUCKET_NAME,strClientID)
        let s3BucketName = String(format: "%@", Constants.AWS_S3_BUCKET_NAME)
        let transferUtility = AWSS3TransferUtility.default()
        
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                DispatchQueue.main.async{
                    print("Progress: \(Float(progress.fractionCompleted))")
                }
            })
        }
        //https://s3-ap-southeast-1.amazonaws.com/uploadmylestest/1559214293679.jpg
        print("self.strDocumentUrl: ", self.strDocumentUrl)
        var strData = String()
        var strData1 = String()
        
        if self.strDocumentUrl != ""
        {
            let arr1 = self.strDocumentUrl?.components(separatedBy: "/")
            strData = String(format: "%@", arr1!.last!)
            strData1 = String(format: "%@", arr1![2])
        }
        //"1559214293679.jpg"
        let url = URL(string: String(format: "https://%@/", strData1) ) //URL(string: "https://s3-ap-southeast-1.amazonaws.com/")
        transferUtility.download(to: url!, bucket: Constants.AWS_S3_BUCKET_NAME, key:String(format: "/%@", strData), expression: expression, completionHandler: {(task, url, data, error) in
            if error != nil
            {
                print("AWS Download Error")
                return
            }
            DispatchQueue.main.async(execute: {
                self.createCustomView(strURL: self.strDocumentUrl ?? "")
                print("Document has been downloaded")
            })
        })
    }
    
    func createCustomView(strURL: String)
    {
        let objViewUploadedDocumentVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_ViewUploadedDocumentVC) as! ViewUploadedDocumentVC
        objViewUploadedDocumentVC.modalPresentationStyle = .fullScreen
        objViewUploadedDocumentVC.strDocumentUrl = self.strDocumentUrl
        self.present(objViewUploadedDocumentVC, animated: true, completion: nil)
        
        /*customView.frame = CGRect.init(x: 0, y: 0, width: self.viewActive.frame.size.width - 40, height: self.viewActive.frame.size.height - 40)
        customView.backgroundColor = UIColor.black     //give color to the view
        customView.center = self.view.center
        self.viewActive.addSubview(customView)
        
        let url = URL(string: self.strDocumentUrl!) //"https://developer.apple.com/documentation/webkit/wkwebview"
        let request = URLRequest(url: url!)
        let webView = WKWebView(frame: customView.frame)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight] //It assigns Custom View height and width
        webView.navigationDelegate = self as? WKNavigationDelegate
        webView.load(request)
        customView.addSubview(webView)*/
    }
     
    //MARK: - Button Actions
    
    @IBAction func btnDocumentType_Click(_ sender: Any)
    {
        self.showDocumentList()
        //self.showDocumentTypeDetail()
    }
    
    @IBAction func btnServiceHistory_Click(_ sender: Any)
    {
        if self.historyStatus == 0
        {
            self.LoadNoHistoryView(strMessage: "No History Exists")
        }
        else if self.historyStatus == 1
        {
            self.loadServiceHistoryAndUpcomingRequestView(arrData: [], strMessage: "", strViewName: "History")
        }
        //self.GetScheduleServiceHistoryWebservice()
    }
    
    @IBAction func btnUpcomingRequestService_Click(_ sender: Any)
    {
        self.loadServiceHistoryAndUpcomingRequestView(arrData: [], strMessage: "", strViewName: "Upcoming")
    }
    
    @IBAction func btnRepairService_Click(_ sender: Any)
    {
        
    }

    @objc func btnServiceDate_Click(sender: UIButton!)
    {
        CommonMethodsMylesZero.showDateTimePicker(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        
        self.datePickerView.maximumDate = Calendar.current.date(byAdding: .month, value: +1, to: Date())
//        let daysToSubtract = 10
//        var dateComponent = DateComponents()
//        dateComponent.day = daysToSubtract
//        let currentDate = NSDate()
//        let earlierDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
//        self.datePickerView.minimumDate = startDay
        self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
    }
      
    @objc func btnSubmitRequestForRepair_Click(sender: UIButton!)
    {
        if self.viewUpcomingRequest.txtContactName.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter contact name", strAction1: "OK", viewController: self)
        }
        else if self.viewUpcomingRequest.txtContactNumber.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter contact number", strAction1: "OK", viewController: self)
        }
        else if self.viewUpcomingRequest.txtPreferredDate.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the preferred date", strAction1: "OK", viewController: self)
        }
        else if self.viewUpcomingRequest.txtTypesOfService.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select type of service", strAction1: "OK", viewController: self)
        }
        else if self.viewUpcomingRequest.txtPickupAddress.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter pickup address", strAction1: "OK", viewController: self)
        }
        else
        {
            self.RequestForServiceRepairWebservice()
        }
    }
    
    @IBAction func btnDownloadDocuments_Click(_ sender: Any)
    {
        if self.strDocumentUrl != ""
        {
            self.createCustomView(strURL: self.strDocumentUrl!)
        }        
        //self.downloadFileFromAWS()
    }
    
    @IBAction func btnEditUpcomingService_Click(_ sender: Any)
    {
        self.loadUpcomingEditDateView()
    }
    
    @objc func btnRequestServiceRepair_Click(sender: UIButton!)
    {
        if self.historyStatus == 1
        {
            if (sender.isSelected == true)
            {
                sender.isSelected = false
                self.viewUpcomingRequest.frame.size.height = self.viewActive.frame.size.height - 20
                self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
                self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
                self.viewUpcomingRequest.layer.cornerRadius = 4.0
                self.viewUpcomingRequest.serviceHeightConstraint.constant = 0.0
                self.viewUpcomingRequest.RequestServiceTopConstraint.constant = 20
                self.viewUpcomingRequest.viewServiceData.isHidden = true
                self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = false
            }
            else
            {
                sender.isSelected = true
                self.viewUpcomingRequest.frame.size.height = 400
                self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
                self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
                self.viewUpcomingRequest.layer.cornerRadius = 4.0
                self.viewUpcomingRequest.RequestServiceTopConstraint.constant = 150
                //self.viewServiceHistory.serviceHeightConstraint.constant = 0.0
                self.viewUpcomingRequest.viewServiceData.isHidden = false
                self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
            }
        }
        else
        {
            //sender.isSelected = false
            self.viewUpcomingRequest.frame.size.height = 400
            self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
            self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
            self.viewUpcomingRequest.layer.cornerRadius = 4.0
            //self.viewUpcomingRequest.RequestServiceTopConstraint.constant = 150
            //self.viewServiceHistory.serviceHeightConstraint.constant = 0.0
            self.viewUpcomingRequest.viewServiceData.isHidden = false
            self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
            
            /*if (sender.isSelected == true)
            {
                sender.isSelected = false
                self.viewUpcomingRequest.frame.size.height = 100
                self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
                self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
                self.viewUpcomingRequest.layer.cornerRadius = 4.0
                //self.viewUpcomingRequest.RequestServiceTopConstraint.constant = 150
                //self.viewServiceHistory.serviceHeightConstraint.constant = 0.0
                self.viewUpcomingRequest.viewServiceData.isHidden = false
                self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = true
            }
            else
            {
                sender.isSelected = true
                self.viewUpcomingRequest.frame.size.height = 300 //self.viewActive.frame.size.height - 20
                self.viewUpcomingRequest.frame.origin = self.viewActive.bounds.origin
                self.viewUpcomingRequest.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpcomingRequest)
                self.viewUpcomingRequest.layer.cornerRadius = 4.0
                //self.viewUpcomingRequest.serviceHeightConstraint.constant = 0.0
                //self.viewUpcomingRequest.RequestServiceTopConstraint.constant = 20
                self.viewUpcomingRequest.viewServiceData.isHidden = true
                self.viewUpcomingRequest.viewRequestSeviceRepair.isHidden = false
            }*/
        }
    }
    
    //MARK: - Show Drop Down For Document
    
    func showDocumentList()
    {
        for i in 0..<self.arrDocuments.count
        {
            let objDoc = self.arrDocuments[i] as! SubscriptionStatusDetailDoc
            let strDocName = objDoc.doctype as String //(self.arrDocumentsList![i] as AnyObject) as! String
            self.txtSelectedDocument.optionArray.append(strDocName)
        }
        
        self.txtSelectedDocument.didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            let objDoc = self.arrDocuments[index] as! SubscriptionStatusDetailDoc
            
            if selectedText == objDoc.doctype
            {
                self.strDocumentUrl = objDoc.url
            }
            
            self.txtSelectedDocument.text = selectedText
            self.txtSelectedDocument.textColor = UIColor.black
        }
    }
    
    func showTypeOfServiceList()
    {
        let arrServiceList : NSArray = ["Damage", "Service"]
        for i in 0..<arrServiceList.count
        {
            let strServiceName = arrServiceList[i] as! String
            //let strServiceName = (arrServiceListt![i] as AnyObject) as! String
            self.viewUpcomingRequest.txtTypesOfService.optionArray.append(strServiceName)
        }
        
        self.viewUpcomingRequest.txtTypesOfService.didSelect{(selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.viewUpcomingRequest.txtTypesOfService.text = selectedText
            self.viewUpcomingRequest.txtTypesOfService.textColor = UIColor.black
        }
    }
    //MARK:- Action Sheet With Picker View
    
    func showServiceDetail()
    {
        let alertController = UIAlertController(title: "", message: "Select Service Type", preferredStyle: .actionSheet)
        
        let damageAction = UIAlertAction(title: "Damage", style: .default, handler: { (action) -> Void in
            self.viewUpcomingRequest.txtTypesOfService.text = "Damage"
        })
        
        let serviceAction = UIAlertAction(title: "Service", style: .default, handler: { (action) -> Void in
            self.viewUpcomingRequest.txtTypesOfService.text = "Service"
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        alertController.addAction(damageAction)
        alertController.addAction(serviceAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showDocumentTypeDetail()
    {
        let alertController = UIAlertController(title: "", message: "Select Document Type", preferredStyle: .actionSheet)
        
        let fitnessAction = UIAlertAction(title: "Fitness", style: .default, handler: { (action) -> Void in
            self.btnDocumentType.setTitle("Fitness", for: UIControl.State.normal)
            self.btnDocumentType.setTitleColor(UIColor.black, for: UIControl.State.normal)
        })
        
        let insuranceAction = UIAlertAction(title: "Insurance", style: .default, handler: { (action) -> Void in
            self.btnDocumentType.setTitle("Insurance", for: UIControl.State.normal)
            self.btnDocumentType.setTitleColor(UIColor.black, for: UIControl.State.normal)
        })
        
        let rcAction = UIAlertAction(title: "RC", style: .default, handler: { (action) -> Void in
            self.btnDocumentType.setTitle("RC", for: UIControl.State.normal)
            self.btnDocumentType.setTitleColor(UIColor.black, for: UIControl.State.normal)
        })
        
        let permitAction = UIAlertAction(title: "Permit", style: .default, handler: { (action) -> Void in
            self.btnDocumentType.setTitle("Permit", for: UIControl.State.normal)
            self.btnDocumentType.setTitleColor(UIColor.black, for: UIControl.State.normal)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        alertController.addAction(fitnessAction)
        alertController.addAction(insuranceAction)
        alertController.addAction(rcAction)
        alertController.addAction(permitAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

}
