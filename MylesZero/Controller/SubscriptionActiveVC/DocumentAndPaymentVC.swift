//
//  DocumentAndPaymentVC.swift
//  Myles Zero
//
//  Created by skpissay on 16/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import Alamofire
import iOSDropDown
import AWSS3
import AWSCore

protocol ReportLossDelegate
{
    func reportLossView()
}

class DocumentAndPaymentVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{    
    var viewTransparent = UIView()
    var viewReportLossItem = ReportLossView()
    var viewUpdateDocuments = UpdateDocumentsView()
    var viewUpdatePaymentCard = UpdatePaymentMethodView()
    var viewActive = UIView()
    
    var datePickerView = UIDatePicker()
    var timePickerView = UIDatePicker()
    
    @IBOutlet weak var lblSavedPaymentCard: UILabel!
    @IBOutlet weak var lblAdhaarCard: UILabel!
    @IBOutlet weak var lblLicenceNumber: UILabel!
    @IBOutlet weak var lblPanCard: UILabel!
    @IBOutlet weak var lblAdditionalExpense: UILabel!
    @IBOutlet weak var btnLossReport: UIButton!
    @IBOutlet weak var btnBillingHistory: UIButton!
    @IBOutlet weak var imageView: UIImageView?
    
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?
    var arrLossItemList : NSArray?
    var reportDelegate : ReportLossDelegate?
    
    var imageURL = NSURL()
    var uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var uploadFileURL: NSURL?
    let imagePicker = UIImagePickerController()
    var strAmazonBucketPath : String?
    var strTimeSlot : String?
    var strButtonName : String?
    var strDocType : String?
    var strDocFile : String?
    var strFilePath : String?
    var strLossItemId : String?
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    
    @IBOutlet weak var btnEditAdhaarCard: UIButton!
    @IBOutlet weak var btnEditLicenceNo: UIButton!
    @IBOutlet weak var btnEditPanCard: UIButton!
    @IBOutlet weak var btnEditSavedCard: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        CommonMethodsMylesZero.setBorder(button: self.btnBillingHistory)
        CommonMethodsMylesZero.setBorder(button: self.btnLossReport)
        
        //self.lblSavedPaymentCard.text = objSubscriptionDetail!.cartype!
        self.lblAdhaarCard.text = objSubscriptionDetail!.adharNo!
        self.lblLicenceNumber.text = objSubscriptionDetail!.dLNo!
        self.lblPanCard.text = objSubscriptionDetail!.panNo!
        self.lblAdditionalExpense.text = objSubscriptionDetail!.additionalExp!
        imagePicker.delegate = self
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
        if UserProfileInfo != nil
        {
            let arrValue = UserProfileInfo?.aadharfile.components(separatedBy: "/")
            //let strFinalKMs = arrValue[0] as String
            if UserProfileInfo?.aadharfile != ""
            {
                self.strFilePath = String(format: "https://%@/%@/", arrValue?[2] ?? "", arrValue?[3] ?? "")
            }
            //self.strFilePath = String(format: "https://%@/%@/%@/", arrValue![2], arrValue![3], arrValue![4])
        }
        //print("Adhaar File: ", UserProfileInfo.aadharfile)
        //self.strFilePath = UserProfileInfo.aadharfile
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    //MARK: - Upload on AWS
    
    func uploadFileOnAWS(withImage image: UIImage)
    {
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        let access = Constants.AWS_ACCESS_KEY
        let secret = Constants.AWS_SECRET_KEY
        let credentials = AWSStaticCredentialsProvider(accessKey: access, secretKey: secret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast1, credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //let s3BucketName = String(format: "%@/%@", Constants.AWS_S3_BUCKET_NAME,strClientID)
        let s3BucketName = String(format: "%@", Constants.AWS_S3_BUCKET_NAME)
        let compressedImage = image.resizedImage(newSize: CGSize(width: 80, height: 80))
        let data: Data = compressedImage.pngData()!
        
        let miliSeconds: Int = CommonMethodsMylesZero.currentTimeInMiliseconds()
        let strSeconds : String = String(format: "%d",miliSeconds)
        let keyName = String(format: "%@/%@%@.%@",strClientID,strClientID,strSeconds,data.format)
        let remoteName = String(format: "%@%@.%@",strClientID,strSeconds,data.format)
        self.strFilePath = String(format: "%@", strClientID) //userDetails.clientID!
        
        //let remoteName = String(format: "%@%@",userDetails.clientID!,generateRandomStringWithLength(length: 12)+"."+data.format)
        print("REMOTE NAME : ", remoteName)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                DispatchQueue.main.async{
                    print("Progress: \(Float(progress.fractionCompleted))")
                }
            })
        }
        expression.setValue("public-read-write", forRequestHeader: "x-amz-acl")   //4
        expression.setValue("public-read-write", forRequestParameter: "x-amz-acl")
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                if(error != nil)
                {
                    print("Failure uploading file Error: %@", error?.localizedDescription)
                }
                else
                {
                    print("Success uploading file")
                    
                    CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Document Uploaded Successfully", strAction1: "OK", viewController: self)
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data, bucket: s3BucketName, key: remoteName, contentType: "image/"+data.format, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
            if let error = task.error {
                print("Error : \(error.localizedDescription)")
            }
            
            if task.result != nil
            {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(s3BucketName).appendingPathComponent(keyName) //remoteName
                if let absoluteString = publicURL?.absoluteString
                {
                    print("Image URL : ",absoluteString)
                    let arrValue = absoluteString.components(separatedBy: "/")
                    //let strFinalKMs = arrValue[0] as String
                    self.strFilePath = String(format: "https://%@/%@/%@/", arrValue[2], arrValue[3], arrValue[4])
                    self.strAmazonBucketPath = s3BucketName
                    
                    if self.strButtonName == "Upload Document"
                    {
                        self.strDocFile = remoteName
                        self.viewUpdateDocuments.lblUploadDocName.text = absoluteString //remoteName
                        self.viewUpdateDocuments.lblUploadDocName.textColor = UIColor.black
                    }
                    else if self.strButtonName == "Upload FIR"
                    {
                        self.strDocFile = remoteName
                        self.viewReportLossItem.txtFIRUrl.text = absoluteString //remoteName
                        self.viewReportLossItem.txtFIRUrl.textColor = UIColor.black
                    }
                }
            }
            return nil
        }
    }
    
    //MARK: - UIPickerView Delegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView?.contentMode = .scaleAspectFit
            imageView?.image = pickedImage
            /*let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
            let miliSeconds: Int = self.currentTimeInMiliseconds()
            let fileName : String = String(format: "%@%d", userDetails.clientID!,miliSeconds)*/
            self.uploadFileOnAWS(withImage: pickedImage)
            //self.uploadFile(with: fileName, type: "png")
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openImagePickerAlert()
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: - Get Loss Item List API
    func GetLossItemListWebservice()
    {
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.getLossItemAPI(APIConstants.API_GetLostItemList, dataDict: nil, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
         
                if error != nil
                {
                }
                else
                {
                    //let JsonLogin = response as AnyObject
                    let arrResponse = response.value(forKey: "response") as! NSArray
                    print("arrResponse: ", arrResponse)
                    self.arrLossItemList = arrResponse
                    self.loadReportALossItemView()
                    //let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionList) as? SubscriptionStatusDetailRootClass
                    //txtDropdown.optionArray.append(objSubscriptionDetail!.subscriptionId!)
                    //DefaultsValues.setArrayValueFromUserDefaults((arrResponse as NSArray), forKey: kCityListArray)
                }
            }
         })
    }
    
    func reportLostItemAPI()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.viewReportLossItem)
        //strLossItemId
        let dictReportLostItem = [
            "subscriptionbookingId" : objSubscriptionDetail!.subscriptionBookingid! as String,
            "LostitemId": (self.strLossItemId ?? "") as String,
            "incidentDate": (self.viewReportLossItem.txtIncidentDate.text ?? "") as String,
            "incidentTime": (self.viewReportLossItem.txtIncidentTime.text ?? "") as String,
            "Remark": self.viewReportLossItem.txtViewIncidentDetail.text as String,
            "FIRUrl": (self.viewReportLossItem.txtViewIncidentDetail.text ?? "") as String,
            "AccidentImage": "",
            "IncidentPlace": ""] as [String : AnyObject]

        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.reportLostItemAPI(APIConstants.API_ReportLostItem, dataDict: dictReportLostItem, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
                hud!.dismiss()
                if error != nil
                {
                }
                else
                {
                    //let JsonLogin = response as AnyObject
                    let strMessage = response.value(forKey:"Message") as! String
                    CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
                    //self.loadReportALossItemView()
                }
            }
         })
    }
    
    func UploadDocumentWebservice()
    {
        let hud = CommonMethodsMylesZero.showProgressHudDark(view: self.viewUpdateDocuments)
        //let userDetails = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserInfo) as! UserLoginResponse
        let strClientID = String(format: "%@", UserDefaults.standard.value(forKey: KUSER_ID) as! CVarArg)
        
        //self.strDocFile =
        //self.strDocFile
        /*File path should be like:  https://s3-ap-southeast-1.amazonaws.com/uploadmyles/2966432/
        DocFile:  2966432_63170_PC.jpg*/
        
        /*{"DocName":"565445167402","Docfile":"2702230_1617865781036.jpeg","Doctype":"ADHAAR","clientid":"2702230","filepath":"https://uploadmylestest.s3.amazonaws.com/", "Docdob":""}*/
        
        let dictUploadDocParam = ["clientid" : strClientID as String, //userDetails.clientID!
                                  "Doctype": (self.strDocType!) as String,
                                  "Docfile": (self.strDocFile ?? "") as String,
                                  "DocName": (self.viewUpdateDocuments.txtDocumentNo.text ?? "") as String,
                                  "Docdob": "",//(self.viewUpdateDocuments.txtExpiryDate.text ?? "") as String,
                                  "filepath": (self.strFilePath ?? "") as String] as [String : AnyObject] //(self.viewUpdateDocuments.lblUploadDocName.text ?? "") as String
        let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
        print("dictHeaders SearchCarVC: ", dictHeaders)
        APIFunctions.sharedInstance.uploadSingDocumentAPI(APIConstants.API_UploadSingleDocument, dataDict: dictUploadDocParam, dataHeaders: (dictHeaders as! HTTPHeaders), completion:  { (response, error) in
         
            DispatchQueue.main.async {
                hud!.dismiss()
                if error != nil
                {
                }
                else
                {
                    print("response: %@", response)
                    let responseKey = response.value(forKey:"response") as? AnyObject
                    if responseKey is NSNull //((response.value(forKey:"response") as? NSNull) == nil)
                    {
                        let strMessage = response.value(forKey: "message") as? String
                        let status = response.value(forKey: "status") as? Int
                        if status == 1
                        {
                            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage ?? "", strAction1: "OK", viewController: self)
                        }
                        //let strMessage = response.value(forKey:"message") as! String
                    }

                    //let arrResponse = response.value(forKey: "response") as! NSArray
                    //print("arrResponse: ", arrResponse)
                }
            }
         })
    }
    
    //MARK: - Create DropDown
    
    func lossItemDropDown()
    {
        for i in 0..<self.arrLossItemList!.count
        {
            let dictLossItem = self.arrLossItemList![i] as! NSDictionary
            let strLossItemName = dictLossItem.value(forKey: "Item") as! String
            self.viewReportLossItem.txtLossItem.optionArray.append(strLossItemName)
            //let strStateName = (arr1[i] as AnyObject).value(forKey: "StateName") as! String
        }
        viewReportLossItem.txtLossItem.arrowColor = UIColor.black
        viewReportLossItem.txtLossItem.selectedRowColor = Constants.RED_COLOR
        viewReportLossItem.txtLossItem.checkMarkEnabled = false
        viewReportLossItem.txtLossItem.didSelect{(selectedText , index ,id) in
        print("Selected String: \(selectedText) \n index: \(index)")
            let dictLossItem = self.arrLossItemList![index] as! NSDictionary
            self.strLossItemId = dictLossItem.value(forKey: "ItemID") as? String
            if selectedText == "Accident"
            {
                self.viewReportLossItem.viewAccident.isHidden = false
                self.viewReportLossItem.accidentHeightConstraint.constant = 45.0
            }
            else
            {
                self.viewReportLossItem.viewAccident.isHidden = true
                self.viewReportLossItem.accidentHeightConstraint.constant = 0.0
            }
            
        self.viewReportLossItem.txtLossItem.textColor = UIColor.black
        }
    }
    
    func setTimeSlotsDropDown()
    {
        let arrTimeSlots = ["8-10 hr","10-12 hr", "12-14 hr", "14-16 hr" ,"16-18 hr"]
        
        for i in 0..<arrTimeSlots.count
        {
            let strCallbackReasonName = arrTimeSlots[i]
            self.viewReportLossItem.txtIncidentTime.optionArray.append(strCallbackReasonName)
        }
        viewReportLossItem.txtIncidentTime.arrowColor = UIColor.black
        viewReportLossItem.txtIncidentTime.selectedRowColor = Constants.RED_COLOR
        viewReportLossItem.txtIncidentTime.checkMarkEnabled = false
        viewReportLossItem.txtIncidentTime.didSelect{(selectedText , index ,id) in
        print("Selected String: \(selectedText) \n index: \(index)")
            self.strTimeSlot = selectedText
            
        self.viewReportLossItem.txtIncidentTime.textColor = UIColor.black
        }
    }
    
    func setDocumentTypeDropDown(objUserProfile : UserProfileInfoResponse)
    {
        let arrDcoumentType = ["Adhaar","Passport"]
        
        for i in 0..<arrDcoumentType.count
        {
            let strDocumentType = arrDcoumentType[i]
            self.viewUpdateDocuments.txtDocumentType.optionArray.append(strDocumentType)
        }
        self.viewUpdateDocuments.txtDocumentType.arrowColor = UIColor.black
        self.viewUpdateDocuments.txtDocumentType.selectedRowColor = Constants.RED_COLOR
        self.viewUpdateDocuments.txtDocumentType.checkMarkEnabled = false
        self.viewUpdateDocuments.txtDocumentType.didSelect{(selectedText , index ,id) in
        print("Selected String: \(selectedText) \n index: \(index)")
            self.strTimeSlot = selectedText
            
            if selectedText == "Adhaar"
            {
                self.viewUpdateDocuments.lblDocumentTitle.text = "UPDATE ADHAAR"
                self.viewUpdateDocuments.txtDocumentNo.placeholder = "Enter Adhaar Name"
                self.viewUpdateDocuments.èxpiryDateHeightConstraint.constant = 0.0
                self.viewUpdateDocuments.viewExpiryDate.isHidden = false
                
                self.viewUpdateDocuments.txtDocumentNo.text = objUserProfile.aadharname
                let arrValue = objUserProfile.aadhardob.components(separatedBy: " ")
                let strFinalKMs = arrValue[0] as String
                self.viewUpdateDocuments.txtExpiryDate.text = strFinalKMs
                self.viewUpdateDocuments.lblUploadDocName.text = objUserProfile.aadharfile
            }
            else if selectedText == "Passport"
            {
                self.viewUpdateDocuments.lblDocumentTitle.text = "UPDATE PASSPORT"
                self.viewUpdateDocuments.txtDocumentNo.placeholder = "Enter Passport No"
                self.viewUpdateDocuments.èxpiryDateHeightConstraint.constant = 45.0
                self.viewUpdateDocuments.viewExpiryDate.isHidden = false
                
                self.viewUpdateDocuments.txtDocumentNo.text = objUserProfile.passportNo
                let arrValue = objUserProfile.passportexpirydate.components(separatedBy: " ")
                let strFinalKMs = arrValue[0] as String
                self.viewUpdateDocuments.txtExpiryDate.text = strFinalKMs
                self.viewUpdateDocuments.lblUploadDocName.text = objUserProfile.passportfile
            }
            
        self.viewUpdateDocuments.txtDocumentType.textColor = UIColor.black
        }
    }
    
    func loadReportALossItemView()
    {        
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        self.viewReportLossItem = Bundle.main.loadNibNamed("ReportLossView", owner: nil, options: nil)![0] as! ReportLossView
        self.viewActive.addSubview(self.viewReportLossItem)
        //self.viewReportLossItem.frame = CGRect(x: 50, y: 50, width: self.viewActive.frame.size.width - 20, height: self.viewActive.frame.size.height - 40)
        self.viewReportLossItem.frame.origin = self.viewActive.bounds.origin
        self.viewReportLossItem.center = self.viewActive.convert(self.viewActive.center, from: self.viewReportLossItem)
        //self.viewReportLossItem.center = self.view.center
        self.viewReportLossItem.layer.cornerRadius = 4.0
        self.viewReportLossItem.btnReportLoss.layer.cornerRadius = 4.0
        self.viewReportLossItem.txtViewIncidentDetail.layer.borderWidth = 1.0
        self.viewReportLossItem.txtViewIncidentDetail.layer.borderColor = UIColor.lightGray.cgColor
        
        //self.viewReportLossItem.btnReportLoss.layer.borderWidth = 1.0
        self.viewReportLossItem.btnIncidentDate.addTarget(self, action:#selector(self.btnIncidentDate_Click(sender:)), for: .touchUpInside)
        self.viewReportLossItem.btnIncidentTime.addTarget(self, action:#selector(self.btnIncidentTime_Click(sender:)), for: .touchUpInside)
        self.viewReportLossItem.btnUploadFIR.addTarget(self, action:#selector(self.btnUploadFIR_Click(sender:)), for: .touchUpInside)
        self.viewReportLossItem.btnReportLoss.addTarget(self, action:#selector(self.btnRequestForReportLoss_Click(sender:)), for: .touchUpInside)
        self.viewReportLossItem.viewAccident.isHidden = true
        self.viewReportLossItem.accidentHeightConstraint.constant = 0.0
        self.lossItemDropDown()
    }    
    
    func hideReportALossItemView()
    {
        viewTransparent.removeFromSuperview()
        if (self.viewReportLossItem.superview != nil)
        {
            self.viewReportLossItem.removeFromSuperview()
        }
    }
    
    func loadUpdatePaymentMethodView(strDocumentName: String)
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
          
        self.viewUpdatePaymentCard = Bundle.main.loadNibNamed("UpdatePaymentCardView", owner: nil, options: nil)![0] as! UpdatePaymentMethodView
        self.viewActive.addSubview(self.viewUpdatePaymentCard)
    
        self.viewUpdatePaymentCard.frame.origin = self.viewActive.bounds.origin
        self.viewUpdatePaymentCard.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpdatePaymentCard)
        self.viewUpdatePaymentCard.layer.cornerRadius = 4.0
        self.viewUpdatePaymentCard.btnUpdatePaymentCard.layer.cornerRadius = 4.0
                
        self.viewUpdatePaymentCard.btnUpdatePaymentCard.tag = 234
        self.viewUpdatePaymentCard.btnUpdatePaymentCard.addTarget(self, action:#selector(self.btnUpdateDocument_Click(sender:)), for: .touchUpInside)
        self.viewUpdatePaymentCard.btnExpiryDate.addTarget(self, action:#selector(self.btnExpiryDate_Click(sender:)), for: .touchUpInside)
    }
    
    func loadUpdateDocumentsView(strDocumentName: String)
    {
        viewTransparent = UIView(frame: CGRect(x: self.viewActive.frame.origin.x, y: self.viewActive.frame.origin.y, width: self.viewActive.frame.size.width, height: self.viewActive.frame.size.height))
        viewTransparent.backgroundColor = UIColor.black
        viewTransparent.alpha = 0.6
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleReferralTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        viewTransparent.addGestureRecognizer(tap)
        self.viewActive.addSubview(viewTransparent)
        
        self.viewUpdateDocuments = Bundle.main.loadNibNamed("UpdateDocumentsView", owner: nil, options: nil)![0] as! UpdateDocumentsView
        self.viewActive.addSubview(self.viewUpdateDocuments)
  
        self.viewUpdateDocuments.frame.origin = self.viewActive.bounds.origin
        self.viewUpdateDocuments.center = self.viewActive.convert(self.viewActive.center, from: self.viewUpdateDocuments)
        self.viewUpdateDocuments.layer.cornerRadius = 4.0
        self.viewUpdateDocuments.btnUpdateDocument.layer.cornerRadius = 4.0
        self.viewUpdateDocuments.btnChangeImage.layer.cornerRadius = 4.0
        
        self.viewUpdateDocuments.btnChangeImage.layer.borderWidth = 1.0
        self.viewUpdateDocuments.btnChangeImage.layer.borderColor = UIColor.black.cgColor
                
        self.viewUpdateDocuments.btnUpdateDocument.tag = 235
        self.viewUpdateDocuments.btnUpdateDocument.addTarget(self, action:#selector(self.btnUpdateDocument_Click(sender:)), for: .touchUpInside)
        self.viewUpdateDocuments.btnChangeImage.addTarget(self, action:#selector(self.btnChangeImage_Click(sender:)), for: .touchUpInside)
        self.viewUpdateDocuments.btnExpiryDate.addTarget(self, action:#selector(self.btnExpiryDate_Click(sender:)), for: .touchUpInside)
        
        if DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse != nil
        {
            let objUserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
            
            if strDocumentName == "Saved Card"
            {
                self.viewUpdateDocuments.lblDocumentTitle.text = "SAVED CARD"
                self.viewUpdateDocuments.txtDocumentNo.placeholder = "Enter Card No"
                self.viewUpdateDocuments.èxpiryDateHeightConstraint.constant = 45.0
                self.viewUpdateDocuments.viewExpiryDate.isHidden = false
            }
            else if strDocumentName == "Adhaar"
            {
                self.viewUpdateDocuments.docTypeHeightConstraint.constant = 45.0
                self.viewUpdateDocuments.viewDocType.isHidden = false
                self.viewUpdateDocuments.lblDocumentTitle.text = "UPDATE ADHAAR"
                self.viewUpdateDocuments.txtDocumentNo.placeholder = "Enter Adhaar Name"
                self.viewUpdateDocuments.èxpiryDateHeightConstraint.constant = 0.0
                self.viewUpdateDocuments.viewExpiryDate.isHidden = true
                self.viewUpdateDocuments.txtDocumentType.text = "Adhaar"
                
                self.viewUpdateDocuments.txtDocumentNo.text = objUserProfileInfo!.aadharname
                if objUserProfileInfo!.aadhardob != ""
                {
                    let arrValue = objUserProfileInfo!.aadhardob.components(separatedBy: " ")
                    let strFinalKMs = arrValue[0] as String
                    self.viewUpdateDocuments.txtExpiryDate.text = strFinalKMs
                }
                self.viewUpdateDocuments.lblUploadDocName.text = objUserProfileInfo!.aadharfile
                if objUserProfileInfo!.aadharfile != ""
                {
                    
                    let arrValue1 = objUserProfileInfo?.aadharfile.components(separatedBy: "/")
                    //let strFinalKMs = arrValue[0] as String
                    self.strFilePath = String(format: "https://%@/", arrValue1?[2] ?? "")
                    self.strDocFile = String(format: "%@", arrValue1?.last ?? "") //String(format: "%@", arrValue1?[4] ?? "")
                }
            }
                /*else if strDocumentName == "Passport"
                 {
                 self.viewUpdateDocuments.lblDocumentTitle.text = "UPDATE PASSPORT"
                 self.viewUpdateDocuments.txtDocumentNo.placeholder = "Enter Passport No"
                 self.viewUpdateDocuments.èxpiryDateHeightConstraint.constant = 45.0
                 self.viewUpdateDocuments.viewExpiryDate.isHidden = false
                 
                 self.viewUpdateDocuments.txtDocumentNo.text = objUserProfileInfo.passportNo
                 let arrValue = objUserProfileInfo.passportexpirydate.components(separatedBy: " ")
                 let strFinalKMs = arrValue[0] as String
                 self.viewUpdateDocuments.txtExpiryDate.text = strFinalKMs
                 self.viewUpdateDocuments.lblUploadDocName.text = objUserProfileInfo.passportfile
                 
                 }*/
            else if strDocumentName == "Licence"
            {
                self.viewUpdateDocuments.docTypeHeightConstraint.constant = 0.0
                self.viewUpdateDocuments.viewDocType.isHidden = true
                self.viewUpdateDocuments.lblDocumentTitle.text = "UPDATE LICENCE"
                self.viewUpdateDocuments.txtDocumentNo.placeholder = "Enter Licence No"
                self.viewUpdateDocuments.èxpiryDateHeightConstraint.constant = 45.0
                self.viewUpdateDocuments.viewExpiryDate.isHidden = false
                
                self.viewUpdateDocuments.txtDocumentNo.text = objUserProfileInfo!.dlNo
                if objUserProfileInfo!.dlexpirydate != ""
                {
                    let arrValue = objUserProfileInfo!.dlexpirydate.components(separatedBy: " ")
                    let strFinalKMs = arrValue[0] as String
                    self.viewUpdateDocuments.txtExpiryDate.text = strFinalKMs
                }
                
                self.viewUpdateDocuments.lblUploadDocName.text = objUserProfileInfo!.dlfile
                if objUserProfileInfo!.dlfile != ""
                {
                    let arrValue1 = objUserProfileInfo?.dlfile.components(separatedBy: "/")
                    self.strFilePath = String(format: "https://%@/", arrValue1?[2] ?? "")
                    self.strDocFile = String(format: "%@", arrValue1?.last ?? "") //String(format: "%@", arrValue1?[4] ?? "")
                }
            }
            else if strDocumentName == "Pan Card"
            {
                self.viewUpdateDocuments.docTypeHeightConstraint.constant = 0.0
                self.viewUpdateDocuments.viewDocType.isHidden = true
                self.viewUpdateDocuments.lblDocumentTitle.text = "UPDATE PAN CARD"
                self.viewUpdateDocuments.txtDocumentNo.placeholder = "Enter Pan Card No"
                self.viewUpdateDocuments.èxpiryDateHeightConstraint.constant = 0.0
                self.viewUpdateDocuments.viewExpiryDate.isHidden = true
                
                self.viewUpdateDocuments.txtDocumentNo.text = objUserProfileInfo!.panNo
                self.viewUpdateDocuments.lblUploadDocName.text = objUserProfileInfo!.panfile
                
                let arrValue1 = objUserProfileInfo?.panfile.components(separatedBy: "/")
                self.strFilePath = String(format: "https://%@/", arrValue1?[2] ?? "")
                self.strDocFile = String(format: "%@", arrValue1?.last ?? "") //String(format: "%@", arrValue1?[4] ?? "")
            }
            self.setDocumentTypeDropDown(objUserProfile: objUserProfileInfo!)
        }
    }
    
    @objc func handleReferralTap(_ sender: UITapGestureRecognizer)
    {
        viewTransparent.removeFromSuperview()
        if self.viewReportLossItem.superview != nil
        {
            self.viewReportLossItem.removeFromSuperview()
        }
        if self.viewUpdateDocuments.superview != nil
        {
            self.viewUpdateDocuments.removeFromSuperview()
        }
        if self.viewUpdatePaymentCard.superview != nil
        {
            self.viewUpdatePaymentCard.removeFromSuperview()
        }
    }
    
    /*func maskTheAnyCardDetails(strCardNo : String)
    {
        //var cardNumber = "3456123434561234";

        var firstDigits = strCardNo.Substring(0, 6);
        var lastDigits = strCardNo.Substring(cardNumber.Length - 4, 4);

        var requiredMask = new String('X', strCardNo.Length - firstDigits.Length - lastDigits.Length);

        var maskedString = string.Concat(firstDigits, requiredMask, lastDigits);
        var maskedCardNumberWithSpaces = Regex.Replace(maskedString, ".{4}", "$0 ");
    }*/
    //MARK: - Time Selected
    
    @objc func timeSelected()
    {
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "HH:mm"
        let timeString = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(timeString)")
           
        self.viewReportLossItem.txtIncidentTime.text = timeString
        self.viewReportLossItem.txtIncidentTime.textColor = UIColor.black
    }
    
    //MARK: - Date Selected
    
    @objc func dateSelected()
    {
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "dd/MM/yyyy"
        let dateString1 = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(dateString1)")
           
        self.viewReportLossItem.txtIncidentDate.text = dateString1
        self.viewReportLossItem.txtIncidentDate.textColor = UIColor.black
    }
       
    @objc func dateSelectedForDocuments()
    {
        let dateformatter1 = DateFormatter()
        dateformatter1.dateFormat = "dd/MM/yyyy"
        let dateString1 = dateformatter1.string(from: self.datePickerView.date)
        print("Date Selected For Adhaar Card \(dateString1)")
           
        self.viewUpdateDocuments.txtExpiryDate.text = dateString1
        self.viewUpdateDocuments.txtExpiryDate.textColor = UIColor.black
    }
    
    //MARK: - Button Actions
    
    @objc func btnRequestForReportLoss_Click(sender: UIButton!)
    {
        if self.viewReportLossItem.txtLossItem.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the loss item", strAction1: "OK", viewController: self)
        }
        else if self.viewReportLossItem.txtFIRUrl.text != ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please upload FIR Copy", strAction1: "OK", viewController: self)
        }
        else if self.viewReportLossItem.txtIncidentDate.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the date", strAction1: "OK", viewController: self)
        }
        else if self.viewReportLossItem.txtIncidentTime.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select the time", strAction1: "OK", viewController: self)
        }
        else if self.viewReportLossItem.txtIncidentPlace.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the incident place", strAction1: "OK", viewController: self)
        }
        else
        {
            self.reportLostItemAPI()
        }
    }
    
    @objc func btnExpiryDate_Click(sender: UIButton!)
    {
        CommonMethodsMylesZero.showDateTimePicker(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        self.datePickerView.addTarget(self, action: #selector(self.dateSelectedForDocuments), for: UIControl.Event.valueChanged)
    }
    
    @objc func btnUpdateDocument_Click(sender: UIButton!)
    {
        if self.viewUpdateDocuments.txtDocumentNo.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please enter the document number/name", strAction1: "OK", viewController: self)
        }
        else if self.viewUpdateDocuments.txtExpiryDate.text == ""
        {
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Please select expiry date", strAction1: "OK", viewController: self)
        }
        else
        {
            self.UploadDocumentWebservice()
        }
        
         //Swati Commented this api uncoomment after demo
        
        /*if sender.tag == 234
        {
            self.UploadDocumentWebservice()
        }
        if sender.tag == 234
        {
            self.UploadDocumentWebservice()
        }*/
    }
    
    @objc func btnChangeImage_Click(sender: UIButton!)
    {
        self.strButtonName = "Upload Document"
        self.openImagePickerAlert()
    }
    
    @objc func btnIncidentDate_Click(sender:UIButton!)
    {
        CommonMethodsMylesZero.showDateTimePicker(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        //self.datePickerView.maximumDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
        
        //CommonMethodsMylesZero.showDateTimePicker(datePickerView: self.datePickerView, datePickerMode: UIDatePicker.Mode.date, viewController: self)
        //self.datePickerView.addTarget(self, action: #selector(self.dateSelected), for: UIControl.Event.valueChanged)
    }
    
    @objc func btnUploadFIR_Click(sender:UIButton!)
    {
        self.strButtonName = "Upload FIR"
       self.openImagePickerAlert()
    }
    
    @objc func btnIncidentTime_Click(sender:UIButton!)
    {
        self.showTimePicker()
        
        //self.openTimePicker()
        
        //CommonMethodsMylesZero.showTimePicker(timePickerView: self.datePickerView, viewController: self)
        //self.datePickerView.addTarget(self, action: #selector(self.timeSelected), for: UIControl.Event.valueChanged)
    }
    
    func showTimePicker()
    {
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "Please select the time", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        
        timePickerView.frame = CGRect(x: 40, y: 40, width: self.viewReportLossItem.frame.size.width, height: 180)
        timePickerView.tag = 555
        alert.view.addSubview(timePickerView)
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        // For 24 Hrs
        timePickerView.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        
        //For 12 Hrs
       //timePickerView.locale = NSLocale(localeIdentifier: "en_US") as Locale
        timePickerView.isHidden = false
        
        let selectAction = UIAlertAction(title: "Select", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //Perform Action
            self.timeSelected()
        })
        alert.addAction(selectAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant:350) //self.view.frame.height * 0.50 //0.80
        alert.view.addConstraint(height);
        self.present(alert, animated: true, completion: nil)
        //self.viewActive.parent!.present(alert, animated: true, completion: nil)
    }
    
    func openTimePicker()
    {
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        timePickerView.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 150.0)
        timePickerView.backgroundColor = UIColor.white
        self.view.addSubview(timePickerView)
        timePickerView.addTarget(self, action: #selector(self.timeSelected), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func btnBillingHistory_Click(_ sender: Any)
    {
        let objInvoiceDetail = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_InvoiceDetail) as! InvoiceDetailVC
        objInvoiceDetail.isClickedFromSideMenu = true
        self.navigationController?.pushViewController(objInvoiceDetail, animated: true)
    }
    
    @IBAction func btnLossReport_Click(_ sender: Any)
    {
        //self.reportDelegate?.reportLossView()
        self.GetLossItemListWebservice()
    }
    
    @IBAction func btnEditSavedCard_Click(_ sender: UIButton)
    {
        self.strDocType = "Card"
        self.loadUpdatePaymentMethodView(strDocumentName: "Saved Card")
    }
    
    @IBAction func btnEditAdhaarCard_Click(_ sender: UIButton)
    {
        self.strDocType = "ADHAAR"
        self.loadUpdateDocumentsView(strDocumentName: "Adhaar")
        
    }
    
    @IBAction func btnEditLicenceNo_Click(_ sender: UIButton)
    {
        self.strDocType = "DL"
        self.loadUpdateDocumentsView(strDocumentName: "Licence")
    }
    
    @IBAction func btnPanCard_Click(_ sender: UIButton)
    {
        self.strDocType = "PAN"
        self.loadUpdateDocumentsView(strDocumentName: "Pan Card")
    }
}
