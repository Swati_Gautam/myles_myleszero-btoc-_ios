//
//  SubscriptionRequestVC.swift
//  Myles Zero
//
//  Created by skpissay on 15/07/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class SubscriptionRequestVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var lblCarModelName: UILabel!
    @IBOutlet weak var lblCarVariantName: UILabel!
    @IBOutlet weak var lblCarPrice: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblSeater: UILabel!
    @IBOutlet weak var lblCarColor: UILabel!
    @IBOutlet weak var lblCarTransmissionType: UILabel!
    
    @IBOutlet weak var lblFuelType: UILabel!
    @IBOutlet weak var lblDeliveryDays: UILabel!
    
    @IBOutlet weak var imgCarColor: UIImageView!
    @IBOutlet weak var btnCarColor: UIButton!
    @IBOutlet weak var imgCarPic: UIImageView!
    
    @IBOutlet weak var lblTaxAmount: UILabel!
    @IBOutlet weak var tblViewSubscriptionRequest: UITableView!
    @IBOutlet weak var lblSubscriptionID: UILabel!
    @IBOutlet weak var lblUsage: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblTenure: UILabel!
    var dictSubscriptionRequest : NSDictionary?
    var strFromPayment: Bool?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if strFromPayment == true
        {
            self.setSubscriptionAfterPayment()
        }
        else if strFromPayment == false
        {
            self.setSubscriptionDetail()
        }
    }
    func setSubscriptionAfterPayment()
    {
        let objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionDetail) as? SubscriptionStatusDetailRootClass
        //let objCarList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
        self.lblCarModelName.text = objSubscriptionDetail!.carModelName
        self.lblCarVariantName.text = objSubscriptionDetail!.carVariantName
        //let price : Int = Int(objSubscriptionDetail!.toBePaidAmt) //(objCarList?.price)!
        //let strAmount = price.withCommas()
        //self.lblCarPrice.text! = String(format: "₹ %@", strAmount)
        self.lblCarPrice.text = String(format: "₹ %@", objSubscriptionDetail!.toBePaidAmt)
        self.imgCarPic.sd_setImage(with: URL(string: (objSubscriptionDetail!.carImage)), placeholderImage: UIImage(named: kCarPlaceholder))
        
        /*var dict = NSDictionary()
        dict = APIFunctions.dictSubscriptionDetailModel!
        let objCarDetail = SubscriptionDetailCarDetail.init(fromDictionary: dict as! [String : Any]) as SubscriptionDetailCarDetail*/
        
        //let taxAmount : Int = (objCarDetail.taxAmount)!
        //self.lblTaxAmount.text = String(format: "+ %@ Taxes", String(taxAmount.withCommas()))
        
        self.lblTaxAmount.text = String(format: "+ %@ Taxes", objSubscriptionDetail!.taxAmount)
        
        //if objCarList!.carColorCode == nil || objCarList!.carColorCode == ""
        if objSubscriptionDetail!.carColor == "Red" || objSubscriptionDetail!.carColor == "red" || objSubscriptionDetail!.carColor == "RED" || objSubscriptionDetail!.carColor == "Super Red"
        {
            self.lblCarColor.textColor = Constants.RED_COLOR //UIColor.init(hex: Constants.RED_HEX_COLOR)
            self.imgCarColor.setImageColor(color: Constants.RED_COLOR)
        }
        else
        {
            self.imgCarColor.setImageColor(color: UIColor.lightGray)
        }
        self.lblCarColor.text = objSubscriptionDetail?.carColor
        self.lblSeater.text = objSubscriptionDetail?.seatingCapacity //String(format: " %d seater", Int(objSubscriptionDetail!.seatingCapacity))
        self.lblCarTransmissionType.text = objSubscriptionDetail?.transmission
        self.lblFuelType.text! = objSubscriptionDetail?.fuelType as! String
        self.lblSubscriptionID.text = objSubscriptionDetail?.subscriptionId
        self.lblCity.text = objSubscriptionDetail?.cityName
        self.lblTenure.text = objSubscriptionDetail?.subscriptionTenureMonths //String(format: " %d", Int(objSubscriptionDetail!.subscriptionTenureMonths))
        lblUsage.text = String(format: "%@", objSubscriptionDetail!.kmRun!)
        //self.lblSubscriptionID.text = objsub
        /*let status = self.dictSubscriptionRequest!.value(forKey: "status") as! Int
        if status == 1
        {
            self.lblSubscriptionID.text = self.dictSubscriptionRequest!.value(forKey: "SubscriptionId") as? String
            self.lblCity.text = objSubscriptionDetail?.cityName
            self.lblTenure.text = objSubscriptionDetail?.subscriptionTenureMonths //String(format: " %d", Int(objSubscriptionDetail!.subscriptionTenureMonths))
            lblUsage.text = String(format: "%@", objSubscriptionDetail!.kmRun!)
        }*/
    }
    
    func setSubscriptionDetail()
    {
        let objCarList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
        self.lblCarModelName.text! = objCarList!.carModelName!
        self.lblCarVariantName.text! = objCarList!.carVariantName!
        let price : Int = (objCarList?.price)!
        let strAmount = price.withCommas()
        self.lblCarPrice.text! = String(format: "₹ %@", strAmount)
        self.imgCarPic.sd_setImage(with: URL(string: (objCarList!.carImageWeb!)), placeholderImage: UIImage(named: kCarPlaceholder))
        
        var dict = NSDictionary()
        dict = APIFunctions.dictSubscriptionDetailModel!
        let objCarDetail = SubscriptionDetailCarDetail.init(fromDictionary: dict as! [String : Any]) as SubscriptionDetailCarDetail
        
        let taxAmount : Int = (objCarDetail.taxAmount)!
        self.lblTaxAmount.text = String(format: "+ %@ Taxes", String(taxAmount.withCommas()))
        
        //if objCarList!.carColorCode == nil || objCarList!.carColorCode == ""
        if objCarList!.carColorName == "Red" || objCarList!.carColorName == "red" || objCarList!.carColorName == "RED" || objCarList!.carColorName == "Super Red"
        {
            self.lblCarColor.textColor = Constants.RED_COLOR //UIColor.init(hex: Constants.RED_HEX_COLOR)
            self.imgCarColor.setImageColor(color: Constants.RED_COLOR)
        }
        else
        {
            self.imgCarColor.setImageColor(color: UIColor.lightGray)
        }
        self.lblCarColor.text! = objCarList!.carColorName!
        self.lblSeater.text! = String(format: " %d seater", Int(objCarList!.seatingCapacity))
        self.lblCarTransmissionType.text! = objCarList!.transmissiontype!
        self.lblFuelType.text! = objCarList!.fuelTypeName!
        let status = self.dictSubscriptionRequest!.value(forKey: "status") as! Int
        if status == 1
        {
            self.lblSubscriptionID.text = self.dictSubscriptionRequest!.value(forKey: "SubscriptionId") as? String
            self.lblCity.text = objCarList?.cityName
            self.lblTenure.text = String(format: " %d", Int(objCarList!.maxTenureMonths))
            lblUsage.text = String(format: "%@", objCarList!.kmRun!)
        }
    }
    
    //MARK: - UITableView Delegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ((tableView.dataSource?.tableView(tableView, cellForRowAt: indexPath))?.contentView.frame.size.height)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "idCarListCell"
        let cell:CarListTableVIewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CarListTableVIewCell?)!
        
        /*let dictSearchCarList  = SearchCarList.init(fromDictionary:(self.arrCarListInfo[indexPath.row]) as! [String : Any])
        
        cell.lblCarType.text! = objCarList!.carType!
        cell.lblDeliveryDays.text! = String(format: "%d days", Int(objCarList!.tentativeDeliveryDay)!)
        cell.lblCarColor.text! = objCarList!.carColorName!
        cell.lblSeater.text! = String(format: " %d seater", Int(objCarList!.seatingCapacity))
        cell.lblCarTransmissionType.text! = objCarList!.transmissiontype!
        cell.lblFuelType.text! = objCarList!.fuelTypeName!
        cell.btnSubscribe.tag = indexPath.row*/
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    //MARK: - Button Actions
    
    @IBAction func btnBack_Click(_ sender: Any)
    {
        if strFromPayment == true
        {
            DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
            }
        }
        else if strFromPayment == false
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func btnAuthorizePayment_Click(_ sender: Any) {
        
    }

    @IBAction func btnNext_Click(_ sender: Any)
    {
        /*let objSubscriptionActiveVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionActive) as! SubscriptionActiveVC
        self.navigationController?.pushViewController(objSubscriptionActiveVC, animated: true)*/
    }
}
