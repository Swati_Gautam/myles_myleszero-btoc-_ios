//
//  PaymentWebViewVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 07/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

class PaymentWebViewVC: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    //@IBOutlet weak var newWebView: UIWebView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var webView: WKWebView!
    //var webView: WKWebView!
    var strPaymentUrl = String()
    var strPaymentType = String()
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?

    override func viewDidLoad()
    {
        super.viewDidLoad()
               
        self.objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionDetail) as? SubscriptionStatusDetailRootClass
         print("12", self.objSubscriptionDetail)

        
        /*let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(webView)
        webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        webView.uiDelegate = self*/
        
        self.progressBar.progress = 0.0
        self.progressBar.isHidden = false

        webView.uiDelegate = self
        webView.navigationDelegate  = self
        self.callWebView()

    }
    
   override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionDetail) as? SubscriptionStatusDetailRootClass
        //let requestObj = URLRequest(url: URL(string: strUrl)!)
        //self.webView.load(requestObj)
    }
    
    func callWebView(){
        
        let url = URL(string: strPaymentUrl)!
        let UserProfileInfo = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kUserProfileDetails) as? UserProfileInfoResponse
        
        let objSubscription = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionDetail) as? SubscriptionStatusDetailRootClass
        
        let strClientID = UserDefaults.standard.value(forKey: KUSER_ID) as? String
        let strCarFullName = String(format: "%@ %@", objSubscription?.carModelName ?? "", objSubscription?.carVariantName ?? "")
        
        
        guard let indicatedPrice =  Double(objSubscription!.indicatedMonthlyRental) else { return  }
        guard let taxAmount =  Double(objSubscription!.taxAmount) else { return }
        let totalAmount = Double(indicatedPrice + taxAmount)
        let strTotalAmount : String = String(totalAmount)
        
        let dic:[String:Any] = ["CarColour":objSubscription?.carColor ?? "",
                                "CarModelFullName":strCarFullName, //"TATA TiagoTATA Tiago XM"
                                "CarModelName":objSubscription?.carModelName ?? "",
                                "CarVariantId":objSubscription?.carVariantId ?? "",
                                "ModelId":objSubscription?.carModelId ?? "",
                                "PkgId":objSubscription?.pkgId ?? "",
                                "VariantName":objSubscription?.carVariantName ?? "",
                                "amount": objSubscription?.toBePaidAmt ?? "0",//"31436.0", // To be paid amount
                                "carid":objSubscription?.carId ?? "",
                                "city":objSubscription?.cityName ?? "",
                                "cityid":objSubscription?.cityId ?? "",
                                "client_id":String(kCustomerCoID), //"2205",
                                "coric":objSubscription?.coricId ?? "",
                                "custid":strClientID ?? "",//"2702226",
                                "dropoffdate":"",
                                "email":UserProfileInfo?.emailID ?? "",
                                "extrakmrate":objSubscription?.extraKmRate ?? "0",
                                "kmuses":objSubscription?.kmRun ?? "0",
                                "maxamountlimit":objSubscription?.maxAmtLimit ?? "0",//"22902.5", //maxAmountLimit Param
                                "mobile":UserProfileInfo?.phone1 ?? "",
                                "monthly_amount":strTotalAmount,//"62870", // indicated rental price _taxAmount
                                "name":UserProfileInfo?.employeeName ?? "",
                                "paymentType":strPaymentType,
                                "penalty_amount":"0",
                                "pickupdate":objSubscription?.pickUpDate ?? "",//"4/6/2021 4:28:00 PM",
                                "source":"ios",
                                "subs_request_id":objSubscription?.subscriptionId ?? "",
                                "tenure":objSubscription?.subscriptionTenureMonths ?? ""]
        
        print("Payment Dict: ", dic)
        
        do
        {
            let json = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0))
            let data_string = NSString(data: json, encoding:String.Encoding.utf8.rawValue)
            
            //let url = NSURL (string: "http://stage.voicetree.info/for_sachin/index.php" )
            let url = URL(string: strPaymentUrl)!
            
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let post: String = (data_string! as String) //"data=" + (data_string! as String)
            let postData: NSData = post.data(using: String.Encoding.ascii, allowLossyConversion: true)! as NSData
            
            request.httpBody = postData as Data
            webView.load(request as URLRequest)
            //newWebView.loadRequest(request as URLRequest)
        }
        catch{}
    }

    func webViewDidStartLoad(_ webView: UIWebView)
    {
        // UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("Strat Loading")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        // UIApplication.shared.isNetworkActivityIndicatorVisible = false
        print("Finish Loading")
    }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool
    {
        //        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("Starttttt Loading")
        return true
    }
    
    
   
    
    //MARK: - WK WebView Methods
      
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!)
    {
        debugPrint("didCommit")
    }
    
    /*func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("didFinish")
    }*/
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        debugPrint("didFail")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        print("Navigation: url ", navigationAction.request.url)
        //if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"app_action/").lowercased())) != nil // get booking Id
        //print("Navigation Url1234: ", webView.url?.absoluteString)
        
        /*if (navigationAction.request.url?.absoluteString.lowercased()) == (APIConstants.PAYMENT_MAIN_URL+"appthanks".lowercased())
        {//http://staging.mylescars.com/pages/terms_of_use_app
            //https://www.mylescars.com/pages/terms_of_use_app
            if let lastPathComponent = navigationAction.request.url?.lastPathComponent {
                if (lastPathComponent == "appthanks"){
                    //UIApplication.shared.openURL(request.url!)
                    DispatchQueue.main.async {
                        
                        print("asdfghjkl")
                        
                        /*let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = navigationAction.request.url
                        offerController.titleStr = "Terms Of Use"
                        self.present(offerController, animated: true, completion: nil)*/
                        self.createNewEmployeeSubscription()
                    }
                }
            }
            decisionHandler(.cancel)
        }*/
        print("Getting url from webview: ", navigationAction.request.url?.absoluteString.lowercased())
        let strAppThanksUrl = String(format: "%@", APIConstants.PAYMENT_MAIN_URL.lowercased()+"appthanks".lowercased())
        print("strAppThanksUrl: ", strAppThanksUrl)
        //print("Thanks URL: ", APIConstants.PAYMENT_MAIN_URL+"appthanks".lowercased())
        
        if (navigationAction.request.url?.absoluteString.lowercased()) == strAppThanksUrl.lowercased()//(APIConstants.PAYMENT_MAIN_URL+"appthanks".lowercased()
        {
            //CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
            print("Swati")
            DispatchQueue.main.async {
                let objSubscriptionRequestVC = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_SubscriptionRequest) as! SubscriptionRequestVC
                objSubscriptionRequestVC.strFromPayment = true
                self.present(objSubscriptionRequestVC, animated: true, completion: nil)
                //objSubscriptionRequestVC.dictSubscriptionRequest = dict
                //self.navigationController?.pushViewController(objSubscriptionRequestVC, animated: true)
            }
            decisionHandler(.cancel)
            return
        }
        /*else
        {
            decisionHandler(.allow)
            //return
        }*/
        /*else if (navigationAction.request.url?.absoluteString.lowercased()) == (APIConstants.PAYMENT_MAIN_URL.lowercased()+"appError".lowercased()) //(APIConstants.PAYMENT_MAIN_URL+"appError".lowercased())
        {
            print("1234")
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Your payment process has been failed. Please try again !", strAction1: "OK", viewController: self)
            decisionHandler(.allow)
        }*/
        
        decisionHandler(.allow)
        //createNewEmployeeSubscription
    }

    /*func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        /*let url = webView.url?.absoluteString
        print("---Hitted URL--->\(url!)") // here you are getting URL
        //print("AppThanks URL: ", APIConstants.PAYMENT_MAIN_URL+"appthanks")
        
        if (webView.url?.absoluteString.lowercased()) == (APIConstants.PAYMENT_MAIN_URL+"appthanks".lowercased())
        {
            print("App Thanks Called")
            //CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: strMessage, strAction1: "OK", viewController: self)
            self.createNewEmployeeSubscription()
        }
        if (webView.url?.absoluteString.lowercased()) == (APIConstants.PAYMENT_MAIN_URL+"appError".lowercased())
        {
            print("App Error Called")
            CommonMethodsMylesZero.showAlertController2(strTitle: "", strMsg: "Your payment process has been failed. Please try again !", strAction1: "OK", viewController: self)
        }
        else
        {
            print("App Checked")
        }*/
        /*if self.webVw.request?.url?.absoluteString ==  self.cabsModel.paymentSuccessURL
        {
            self.cabsModel.paymentSuccess = true
            self.isNetworkAvailable = true
            ErrorViewController.sharedInstance.showErrorMessage("Your payment process has been successful.")
            RegisterEvents.sharedInstance.createEventForSuccessFulPayment()
            self.paymentProcessCompleted()
        }
        else if self.webVw.request?.url?.absoluteString == self.cabsModel.paymentFailureURL
        {
            self.isNetworkAvailable = true
            self.cabsModel.paymentSuccess = false
            ErrorViewController.sharedInstance.showErrorMessage("Your payment process has been failed. Please try again !")
            self.paymentProcessCompleted()
        }*/
    }*/
    
    //MARK :- WebView Delegate Methods
    /*func webViewDidStartLoad(_ webView: WKWebView)
    {
           self.setProgress()
    }
    
    func webViewDidFinishLoad(_ webView: WKWebView)
    {
        self.completeProgress()
        //self.progressBar.isHidden = true
        /*if self.webVw.request?.url?.absoluteString ==  self.cabsModel.paymentSuccessURL
        {
            self.cabsModel.paymentSuccess = true
            self.isNetworkAvailable = true
            ErrorViewController.sharedInstance.showErrorMessage("Your payment process has been successful.")
            RegisterEvents.sharedInstance.createEventForSuccessFulPayment()
            self.paymentProcessCompleted()
        }
        else if self.webVw.request?.url?.absoluteString == self.cabsModel.paymentFailureURL
        {
            self.isNetworkAvailable = true
            self.cabsModel.paymentSuccess = false
            ErrorViewController.sharedInstance.showErrorMessage("Your payment process has been failed. Please try again !")
            self.paymentProcessCompleted()
        }*/
    }*/
    
    //MARK :- Progress Bar Methods
    func setProgress()
    {
        //        self.progressBarValue += 0.1
        //        if progressBarValue < 0.9 {
        //            self.progressBar.progress =
        //        }
        //        self.progressBar.progress = self.time / 3
        self.progressBar.progress = 0.9
    }
    
    func completeProgress() {
        self.progressBar.progress = 1.0
    }
    
    //MARK: - Button Action
    
    @IBAction func btnBack_Click(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
