//
//  SignAgreementVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 07/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit
import WebKit

class SignAgreementVC: UIViewController, WKNavigationDelegate
{
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var btnCheckbox: UIButton!
    var strUrl = String()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnCarColor: UIButton!
    @IBOutlet weak var lblCarColor: UILabel!
    @IBOutlet weak var lblFuelType: UILabel!
    @IBOutlet weak var lblTransmissionType: UILabel!
    @IBOutlet weak var lblCarSeater: UILabel!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var lblCarPrice: UILabel!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblVariantName: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    
    @IBOutlet weak var lblMonthlySI: UILabel!
    @IBOutlet weak var lblTenure: UILabel!
    @IBOutlet weak var lblKmUsage: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblSubscriptionID: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the
        webView.navigationDelegate = self
         
        self.webView.scrollView.isScrollEnabled = true
        let url = URL(string: strUrl)!
        print("Sign Agreement Url: ", strUrl)
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        self.setUIData()
    }
    
    func setUIData()
    {
        self.btnCheckbox.isSelected = true
        let objSubscription = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionDetail) as? SubscriptionStatusDetailRootClass
        
        //let objCarList = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSearchCarList) as? SearchCarList
        if objSubscription?.carColor == "Red" || objSubscription?.carColor == "red" || objSubscription?.carColor == "RED" || objSubscription?.carColor == "Super Red"
        {
            self.lblCarColor.textColor = Constants.RED_COLOR //UIColor.init(hex: Constants.RED_HEX_COLOR)
            //cell.imgCarColor.setImageColor(color: Constants.RED_COLOR)
        }
        else
        {
            //cell.imgCarColor.setImageColor(color: UIColor.lightGray)
        }
        
        self.lblCarType.text! = objSubscription?.cartype ?? ""
        self.lblDeliveryDate.text! = String(format: "%@ days", objSubscription?.tentativeDeliveryDate ?? "")
        self.lblCarColor.text = objSubscription?.carColor ?? ""
        self.lblCarSeater.text! = String(format: " %@ seater", objSubscription?.seatingCapacity ?? "")
        self.lblTransmissionType.text = objSubscription?.transmission ?? ""
        self.lblFuelType.text = objSubscription?.fuelType ?? ""
        self.lblCarName.text = objSubscription?.carModelName ?? ""
        self.lblVariantName.text = objSubscription?.carVariantName ?? ""
        
        let price : String = objSubscription?.toBePaidAmt ?? ""
        let priceInt : Int = Int(price)!
        let strAmount = priceInt.withCommas()
        self.lblMonthlySI.text = String(format: "₹ %@", strAmount)
        self.imgCar.sd_setImage(with: URL(string: (objSubscription!.carImage)), placeholderImage: UIImage(named: kCarPlaceholder))
        
        self.lblTaxAmount.text = String(format: "+ %@ Taxes", objSubscription?.taxAmount ?? "")
        
        self.lblSubscriptionID.text = objSubscription?.subscriptionId ?? ""
        self.lblCityName.text = objSubscription?.cityName ?? ""
        self.lblTenure.text = String(format: "%@ months", objSubscription?.subscriptionTenureMonths ?? "")
        self.lblCarPrice.text = String(format: "₹ %@", objSubscription?.indicatedMonthlyRental ?? "")
        self.lblStartDate.text = objSubscription?.pickUpDate ?? ""
        self.lblKmUsage.text = String(format: "%@ KM/Month", objSubscription?.km ?? "")
        
        if objSubscription?.tentativeDeliveryDate == ""
        {
            let today = Date()
            let days = Calendar.current.date(byAdding: .day, value: +7, to: today)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let strNextMonth = formatter.string(from: days!)
            self.lblStartDate.text = strNextMonth
        }
        else
        {
            self.lblStartDate.text = objSubscription?.tentativeDeliveryDate
        }
        
        /*print("SubscriptionEndDate: ", objSubscription?.subscriptionEndDate)
        print("subscriptionRequestDate: ", objSubscription?.subscriptionRequestDate)
        print("requestDate: ", objSubscription?.requestDate)
        print("tentativeDeliveryDate: ", objSubscription?.tentativeDeliveryDate)
        print("subscriptionApprovedDate: ", objSubscription?.subscriptionApprovedDate)
        print("createDate: ", objSubscription?.createDate)
        print("pickUpDate: ", objSubscription?.pickUpDate)*/
    }
    
    //MARK: - Button Actions

    @IBAction func btnBack_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCheckbox_Click(_ sender: UIButton)
    {
        if (sender.isSelected == true)
        {
            sender.setImage(UIImage(named: "unselected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = false
        }
        else
        {
            sender.setImage(UIImage(named: "selected_checkbox"), for: UIControl.State.normal)
            sender.isSelected = true
        }
    }
    
    @IBAction func btnProceedPayment_Click(_ sender: Any)
    {
        let objSignAgreement = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_AuthorizePayment) as! AuthorizePaymentVC
        self.navigationController?.pushViewController(objSignAgreement, animated: true)
    }
}
