//
//  AuthorizePaymentVC.swift
//  Myles Zero
//
//  Created by Swati Gautam on 10/12/20.
//  Copyright © 2020 skpissay. All rights reserved.
//

import UIKit

class AuthorizePaymentVC: UIViewController {
    
    var objSubscriptionDetail : SubscriptionStatusDetailRootClass?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.objSubscriptionDetail = DefaultsValues.getCustomObjFromUserDefaults_(forKey: kSubscriptionDetail) as? SubscriptionStatusDetailRootClass

        // Do any additional setup after loading the view.
    }

    @IBAction func btnBack_Click(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreditCard_Click(_ sender: Any)
    {
        let objPaymentWebView = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_PaymentWebView) as! PaymentWebViewVC
        var url : String = ""
        url = String(format: APIConstants.API_Payment_Url)
        objPaymentWebView.strPaymentUrl = url
        objPaymentWebView.strPaymentType = "C"
        self.navigationController?.pushViewController(objPaymentWebView, animated: true)
    }
    
    @IBAction func btnNonCreditCard_Click(_ sender: Any)
    {
        let objPaymentWebView = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_PaymentWebView) as! PaymentWebViewVC
        var url : String = ""
        url = String(format: APIConstants.API_Payment_Url)
        objPaymentWebView.strPaymentUrl = url
        objPaymentWebView.strPaymentType = "D"
        self.navigationController?.pushViewController(objPaymentWebView, animated: true)
        
        /*let objPaymentWebView = Constants.subscribeStoryboard.instantiateViewController(withIdentifier: Constants.VC_PaymentWebView) as! PaymentWebViewVC
        var url : String = ""
        url = String(format: APIConstants.API_CarSubscriptionAgreement,objSubscriptionDetail!.clientID,(objSubscriptionDetail?.subscriptionBookingid!)! as String)

        objPaymentWebView.strPaymentUrl = url
        self.navigationController?.pushViewController(objPaymentWebView, animated: true)*/
        
    }
}
