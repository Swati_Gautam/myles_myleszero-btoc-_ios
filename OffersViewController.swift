//
//  OffersViewController.swift
//  Myles
//
//  Created by Itteam2 on 15/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD
import WebKit

class OffersViewController: UIViewController,LoginProtocol, WKNavigationDelegate {

    @IBOutlet weak var progressBar: UIProgressView!
    var webViewOffers: WKWebView!
    //@IBOutlet weak var offerWebview: UIWebView!
    var urlToNavigate : String!
    var myTimer:Timer!
    var isComplete = Bool()
    
    func showLoader () {
        /*SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")*/
        if(myTimer != nil){
            myTimer.invalidate()
        }
        progressBar.progress = 0.0
        isComplete = false
        progressBar.isHidden = false
        myTimer =  Timer.scheduledTimer(timeInterval: 0.01667,target: self,selector: #selector(OffersViewController.timerCallback),userInfo: nil,repeats: true)
    }
    @objc func timerCallback(){
        if isComplete {
            if progressBar.progress >= 1 {
                progressBar.isHidden = true
                myTimer.invalidate()
            }else{
                progressBar.progress += 0.1
            }
        }else{
            progressBar.isHidden = false
            progressBar.progress += 0.05
            if progressBar.progress >= 0.95 {
                progressBar.progress = 0.95
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(myTimer != nil){
            myTimer.invalidate()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.customizeLeftRightNavigationBar("Offers", rightTItle: nil, controller: self, present: true)
        webViewOffers = WKWebView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 20, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        if(CommonFunctions.reachabiltyCheck()){
            showLoader()
            
            // Do any additional setup after loading the view.
            //let urlStr : String!
            #if DEBUG
                //urlToNavigate = "https://www.mylescars.com/marketings/myles_offers_app"
                urlToNavigate = "https://www.mylescars.com/marketings/myles_offers_app"

            #else
                urlToNavigate = "https://www.mylescars.com/marketings/myles_offers_app"
            #endif
            let request = NSMutableURLRequest(url: URL(string: urlToNavigate)!,
                                              cachePolicy: .reloadIgnoringLocalCacheData,
                                              timeoutInterval: 30.0)
            
            webViewOffers.load(request as URLRequest)
            self.view.addSubview(webViewOffers)
            
            //self.offerWebview.loadRequest(request as URLRequest)
        } else {
            CommonFunctions.alertTitle("MYLES", withMessage:"No Network available. Please connect and try again.")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveTestNotification), name: NSNotification.Name(rawValue: "openLogin"), object: nil)

    }
    
    
       @objc func receiveTestNotification(notification: NSNotification) {
        var userInfo = notification.userInfo!
        let index = userInfo["Selectedindex"] as! String
        
                
        if (index == "3") {
            let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
            let editController = (storyBoard.instantiateViewController(withIdentifier: "LoginProcess") as! LoginProcess)
            
            let navController = UINavigationController(rootViewController: editController)
            if userInfo["otherdestination"] != nil
            {
                editController.delegate = self;
                editController.strOtherDestination = userInfo["otherdestination"] as! String!;
            }
            navController.viewControllers = [editController]
            self.present(navController, animated: true, completion: nil)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        DispatchQueue.main.async(execute: {
            self.isComplete = true
            
            if (webView.isLoading){
                return
            }
            //SVProgressHUD.dismiss()
        })
    }
    
    /*func webViewDidFinishLoad(_ webView: UIWebView)
    {
        //http://stackoverflow.com/questions/5995210/disabling-user-selection-in-uiwebview
        //Disable user selection
        
        isComplete = true

                if (webView.isLoading){
                    return
                }
            //SVProgressHUD.dismiss()
    }*/
    
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Protocol for login with other destination
    
    
func afterLoginChangeDestination(strDestination: String) {
        //[revealController pushFrontViewController:navController animated:YES];
        let revealController = self.revealViewController()
        let storyBoard = UIStoryboard(name: (CInt(strDestination) == 1) ? k_MyRideStoryBoard : k_profileStoryBoard, bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: (Int(strDestination) == 1) ? k_RidesNavigation : k_profileNavigation) as! UINavigationController
        (revealController?.rearViewController as! MenuViewController).selectedIndex = Int(strDestination)!
        if CInt(strDestination) == 2 {
            let homeController = storyBoard.instantiateViewController(withIdentifier: k_profileController)
            navController.viewControllers = [homeController]
        }
        else if CInt(strDestination) == 1 {
            let homeController = storyBoard.instantiateViewController(withIdentifier: "rideHistory")
            navController.viewControllers = [homeController]
        }
        revealController?.pushFrontViewController(navController, animated: true)
    }

}
