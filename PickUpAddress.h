//
//  PickUpAddress.h
//  Myles
//
//  Created by Itteam2 on 24/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickUpAddress : BaseViewController//<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblAddress;
@property (weak, nonatomic) IBOutlet UISearchBar *scrBtn;
@property (weak ,nonatomic) id delegate;
@end

@protocol PassSearchAddressWithGoogleApi
-(void)setAddressThroughApi :(NSDictionary*)dictData;
@end
