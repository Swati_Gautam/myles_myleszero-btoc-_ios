//
//  HomeCell
//  Myles
//
//  Created by Itteam2 on 28/12/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import Foundation
class HomeCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblTitleDetail : UILabel?
    @IBOutlet weak var imgIcon : UIImageView?

}

class HomeCellFooter : UIView
{
    @IBOutlet weak var btnConfirmAge : UIButton?
    
}
