


#define KASucessTilte @"Sucess"
#define KAErrorTitle @"Sorry"

// Login Screen Messages

#define KAEmptyLoginAllField @"Please enter mobile number and password."
#define KAEmptyMobileNo @"Please enter a valid 10 digit mobile number."
#define KAValidPassword @"Please enter password."
#define kAPasswordLenghtValidation @"Password Should be atleast 6 Charactor long."
#define KAValidMobile @"Please enter valid mobile number."
#define KAValidLoginCredencial @"Please enter valid mobile no and password."

#define kEmptypasswordFieldMesg     @"Please enter your password."
#define kEnterMobileNumberMEssage   @"Please enter your registered mobile number."
#define kphonelessthen10msg         @"Please enter a valid 10 digit mobile number."
//Registration screen Messages
#define KAEmptyRegistrationAllFiled @"Please enter all details."
#define KAEmptyEmailId @"Please enter email id."
#define KAValidEmailId @"Please enter valid email id."
#define KConfirmPasswordNotSame @"Password and confirm password is not same."
#define kvaildCode         @"Please enter valid code."

#define serviceTimeoutUnabletoreach     @"Unable to reach the server. Please try again."
#define KAInternet @"No Network available. Please connect and try again."
#define KATryLater @"Could not reach the server. Please try again."
#define KATimeOut @"Time Out"
#define KARegistrationMessage @"Your account already exists. Please try logging in."
#define kLocationServiceUnAvailable             @"Location service off"
#define kLocationServiceOnMessage               @"Unable to find your current location. Please turn on the location services from Settings and try again."//@"Please turn on the location services and try again."
#define kSetting                @"Location Setting"

#define kEnterValidDate             @"Please Select Valid Date."
#define kSelectDate                 @"Please select a pick-up date and then proceed."
#define KInvalidOTP                 @"Please enter valid OTP."
#define KResetPassword              @"Password has been sent to your mobile number. Please try logging in."
#define KWrongMobileNumber          @"Mobile number does not exist. Please check."
#define kHistoryNotFound            @"No History Available."
#define kCancleRideSuccess          @"Ride has been Cancelled Successfully."
#define kCancleRidefail             @"This booking can't be cancelled as the booking date has passed."
#define kRideNotAvailable           @"Ride Not Available Select Other Model."
#define kSelectDocument             @"Please select a document first."
#define kEnterDocumentNumber        @"Please enter the document number."
#define kEnterDocumentExpireDate    @"Please enter the document expiry date."
#define kEnterDocumentIssueCity     @"Please enter the document issuing city."
#define kDocumentUploadSucessFully  @"Document uploaded successfully."
#define kpreAuthpayment             @"Do You Want To Pay Security Amount?"
#define kpreauthreminder(s)          [NSString stringWithFormat:@"Don't forget, you will be charged a Security Deposit amount of Rs %@",s]
//#define kpreauthreminder(s)        [NSString stringWithFormat:@"You are required to pay Rs %@ as security deposit?",s]
#define kSecurityAmount             @"Please pay security amount."
#define kCancelBookingConfirmMsg    @"Are you sure you want to cancel the ride ?"
#define kselectdropoffdateMsg       @"Duration of ride must be atleast 1day for Daily package booking."

#define kgpsServiceNotavailable     @"We're sorry - this service not available."
#define KMinimumTimeSelectionMsg(s)    [NSString stringWithFormat:@"Minimum time before booking is %ld hours",s]


#define KSelectPaymentMode              @"Please select a mode of payment."
#define KServiceNotAvailble             @"Please use Search to find the nearest Myles centre in your city."
#define KMessage                        @"Message"
#define KAlert                          @"Alert"
#define KSorry                          @"Error"
#define kContinue                       @"Continue"
#define kCancel                         @"Cancel"
#define kNo                             @"No"
#define kYes                            @"Yes"
#define KDefault                        @"Saved Cards"
#define KCards                          @"Cards"
#define KNetbanking                     @"Netbanking"
#define KIMPS                           @"IMPS"
#define kCamera                         @"Camera"
#define kGallery                        @"Gallery"
#define kSuccess                        @"Success!"
#define Knocaravailable                 @"Currently no car is available at this center."
#define kLogoutmessage                  @"You have logged out successfully."
#define kDeleteDocument                 @"Are you sure you want to delete this document."
#define kUploadDocument                 @"Please upload your documents from Profile Section."
#define kcallingServiceUnavailble       @"Call facility is not available!!!"
#define KServiceUnavailable             @"No Network available. Please connect and try again."
#define KHardWareConnectivity           @"Hardware Connectivity"
#define KrideLock                       @"Lock Ride"
#define KrideUnlock                     @"Unlock Ride"

#define kdetailsSaveSuccessFully         @"Details Save Successfully"

#define kPleaseEnterValidCode           @"Invalid code. Please enter a valid coupon code to claim a discount."
#define kPleaseEnterCode           @"Please enter a coupon code."
#define kTermsAndConditions        @"Please accept the Myles agreement to continue."

//Payment


#define kpaymentSuccessfulmsg          @"payment Successful"
#define kpaymentcanceledmsg                    @"Transaction aborted by user."
#define kfailed                 @"Failed!"
#define kpaymentFailuremsg       @"payment failed"
#define kCancelled              @"Cancelled!"
#define kpreauthFail         @"Unable to charge Security fee, please try again."
#define kpreAuthSuccessMessage  @"Security deposit amount has been charged successfully. Enjoy you ride!"

#define kLocationFetches @"LocationFetched"

#define KAFBLoginInSettingMessage @"Sorry! Please go to the device settings and login with a Facebook account to allow sharing."

#define KATwitterLoginInSettingMessage @"Please go to the device settings and login with twitter account to tweet."
#define KAFacebookSharingCancel @"Facebook sharing has been canceled."
#define KATwitterSharingCancel @"Tweet has been canceled."


#define kAppUrl @"https://itunes.apple.com/us/app/myles/id1055237998?ls=1&mt=8"
#define APP_STORE_ID 1061852579

#define kFBId @"880567355384794"//@"880567355384794"

#define KCitySelect @"Please Select city."
#define KLocationSelect @"Please Select Location."
#define KPackageSelect @"Please Select package."
#define KStartDateSelect @"Please Select Start Date."
#define KEndDateSelect @"Please Select End Date."

//#endif
