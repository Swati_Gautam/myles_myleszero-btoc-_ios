//
//  Helper.swift
//  Myles
//
//  Created by iOS Team on 04/07/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation

@objc class Helper : NSObject{
    @objc static var formatter = DateFormatter()
    @objc static func getTimeInAMPM (timeString : String) -> String {
        //let timeArr = Array(timeString.characters)
        var newTimeString = timeString
        //newTimeString = newTimeString.insert(":", at: 2)
        newTimeString.insert(":", at: newTimeString.index(newTimeString.startIndex, offsetBy: 2)) // prints hel!lo
        return newTimeString
    }
    
    @objc static func getDayOfWeek(_ today:String) -> String? {
        formatter.dateFormat = "MM-dd-yyyy"
        guard let todayDate = formatter.date(from: today) else { return "" }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return ("\(calenderDay(rawValue:weekDay)!)")
    }
    
    @objc static func getDateWithMonth(dateString : String) -> String? {
        formatter.dateFormat = "MM-dd-yyyy"
        guard let todayDate = formatter.date(from: dateString) else { return "" }
        let myCalendar = Calendar(identifier: .gregorian)
        let month = myCalendar.component(.month, from: todayDate)
        let date = myCalendar.component(.day, from: todayDate)
        let year = myCalendar.component(.year,from:todayDate)
        let yearStr = String(year).suffix(2)
        return ("\(date) \(calendarMonth(rawValue:month)!) '\(yearStr)")
        
    }
    
    @objc static func getDateWithMonthWithoutyear(dateString : String) -> String? {
        formatter.dateFormat = "MM-dd-yyyy"
        guard let todayDate = formatter.date(from: dateString) else { return "" }
        let myCalendar = Calendar(identifier: .gregorian)
        let month = myCalendar.component(.month, from: todayDate)
        let date = myCalendar.component(.day, from: todayDate)
        return ("\(date) \(calendarMonth(rawValue:month)!)")
        
    }

    @objc static func getTimeInAMPMCarList (timeString : String) -> String {
        //let timeArr = Array(timeString.characters)
        var newTimeString = timeString
        //newTimeString = newTimeString.insert(":", at: 2)
        newTimeString.insert(":", at: newTimeString.index(newTimeString.startIndex, offsetBy: 2))
        
        if (Int(getYear(yearString: timeString))! >= 12) {
            return "\(newTimeString) PM"
        } else {
            return "\(newTimeString) AM"
        }
    }

    @objc static func getYear(yearString: String) -> String {
        print("Stringtogetyear = \(yearString.suffix(2))")
        print(String(yearString.truncated()))
        
//        print("getvalue = \(yearString.toLengthOf(length: 2))")
        //yearString.toLengthOf(length: 2)
        return String(yearString.truncated())
    }
    
   // static func roundOffStringToTwoDecimalPlaces

}

extension String {
    func truncated() -> Substring {
        return prefix(2)
    }
}

extension String {
    func intValue() -> Int? {
        if let intVal = Int(self) {
            return intVal
        }
        return nil
    }
}
extension UIViewController{
 
    func emptyAddedView(){
    let storyBoard = UIStoryboard(name: k_mainStoryBoard, bundle: nil)
    let emptyController: EmptyViewController? = (storyBoard.instantiateViewController(withIdentifier: "EmptyViewController") as? EmptyViewController)
    self.navigationController?.pushViewController(emptyController!, animated: true)
    
    }
}

extension UserDefaults{
    
    //MARK: Check Login
    func isFirstTimeCityFetch(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isFirstTime.rawValue)
        //synchronize()
    }
    
    func getFirstTimeCityFetch()-> Bool {
        return bool(forKey: UserDefaultsKeys.isFirstTime.rawValue)
    }
    
    //MARK: Save User Data
    func setLoadMore(value: Bool){
        set(value, forKey: UserDefaultsKeys.loadMore.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getLoadMore() -> Bool{
        return bool(forKey: UserDefaultsKeys.loadMore.rawValue)
    }
}
enum UserDefaultsKeys : String {
    case isFirstTime
    case loadMore
}
//extension String {
//
//    func toLengthOf(length:Int) -> String {
//        if length <= 0 {
//            return self
//        } else if let to = self.index(self.startIndex, offsetBy: length, limitedBy: self.endIndex) {
//            return self.substring(from: to)
//
//        } else {
//            return ""
//        }
//    }
//}


extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}
