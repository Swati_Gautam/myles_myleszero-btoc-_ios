//
//  CustomButton.swift
//  Myles
//
//  Created by Akanksha Singh on 02/04/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import Foundation
import Foundation
@IBDesignable class CustomButton: UIButton {

    @IBInspectable var haveBorder:Bool = false{
        didSet{
            if haveBorder {
                layer.borderColor = borderColor.cgColor
                layer.borderWidth = borderWidth
            }
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.lightGray {
        didSet{
            if haveBorder {
                if isSelected {
                    layer.borderColor = selectedBorderColor.cgColor
                }else {
                    layer.borderColor = borderColor.cgColor
                }
            }
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 1 {
        didSet{
            if haveBorder {
                layer.borderWidth = borderWidth
            }
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat = 1 {
        didSet{
            if roundedCorner {
                layer.cornerRadius = cornerRadius
            }
        }
    }
    
    @IBInspectable var roundedCorner:Bool = false{
        didSet{
            if roundedCorner {
                clipsToBounds = true
                layer.cornerRadius = cornerRadius
            }
        }
    }
    
    @IBInspectable var selectedBorderColor:UIColor = UIColor.lightGray {
        didSet{
            if haveBorder {
                if isSelected {
                    layer.borderColor = selectedBorderColor.cgColor
                }else {
                    layer.borderColor = borderColor.cgColor
                }
            }
        }
    }
    
    override var isSelected: Bool{
        didSet{
            if haveBorder {
                if isSelected {
                    layer.borderColor = selectedBorderColor.cgColor
                    backgroundColor = selectedBackgroundColor
                }else {
                    layer.borderColor = borderColor.cgColor
                    backgroundColor = unselectedBackgroundColor
                }
            }
        }
    }
    
    @IBInspectable var unselectedBackgroundColor:UIColor = UIColor.clear {
        didSet {
            if isSelected {
                backgroundColor = selectedBackgroundColor
            }else {
                backgroundColor = unselectedBackgroundColor
            }
        }
    }
    
    @IBInspectable var selectedBackgroundColor:UIColor = UIColor.clear {
        didSet {
            if isSelected {
                backgroundColor = selectedBackgroundColor
            }else {
                backgroundColor = unselectedBackgroundColor
            }
        }
    }
    
    @IBInspectable var leftImg: UIImage? = nil {
        didSet {
            /* reset title */
            setAttributedTitle()
        }
    }
    
    @IBInspectable var rightImg: UIImage? = nil {
        didSet {
            /* reset title */
            setAttributedTitle()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setAttributedTitle()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setAttributedTitle()
    }
    
    private func setAttributedTitle() {
        var attributedTitle = NSMutableAttributedString()
        
        /* Attaching first image */
        if let leftImg = leftImg {
            let leftAttachment = NSTextAttachment(data: nil, ofType: nil)
            leftAttachment.image = leftImg
            let attributedString = NSAttributedString(attachment: leftAttachment)
            let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
            
            if let title = self.currentTitle {
                mutableAttributedString.append(NSAttributedString(string: title))
            }
            attributedTitle = mutableAttributedString
        }
        
        /* Attaching second image */
        if let rightImg = rightImg {
            let leftAttachment = NSTextAttachment(data: nil, ofType: nil)
            leftAttachment.image = rightImg
            let attributedString = NSAttributedString(attachment: leftAttachment)
            let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
            attributedTitle.append(mutableAttributedString)
        }
        
        /* Finally, lets have that two-imaged button! */
        self.setAttributedTitle(attributedTitle, for: .normal)
    }
    
}
