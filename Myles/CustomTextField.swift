//
//  CustomTextField.swift
//  Myles
//
//  Created by Akanksha Singh on 22/01/18.
//  Copyright © 2018 Divya Rai. All rights reserved.
//

import Foundation
@IBDesignable class CustomTextField: UITextField {
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    @IBInspectable var lineColor: UIColor = UIColor.darkGray
    
    @IBInspectable var insetTailing: CGFloat = 0
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var extraWidth:CGFloat = 0.0
        var extraX:CGFloat = 0.0

        return CGRect(x: bounds.origin.x + 8 + extraX, y: bounds.origin.y , width: bounds.size.width - 16 - (extraWidth + insetTailing), height: bounds.size.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds);
    }
    
    
}
