

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MenuViewController.h"
#import "ActiveRidesResponse.h"
#import "FeedbackVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SorryAlertViewController.h"
#import "NoInternetConnViewController.h"
//#import "Harpy.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>
#import "AmenitiesDetails.h"
#import "NSString+MD5.h"
#import "iRate.h"
#import "Myles-Swift.h"

@import GooglePlaces;
@import GoogleMaps;
@import FirebaseMessaging;

#import <Google/SignIn.h>

////GTM header
#import "TAGManager.h"
#import "TAGContainer.h"
#import "TAGContainerOpener.h"
#import "TAGDataLayer.h"
#import "TAGLogger.h"
#import <Google/Analytics.h>

#import "Branch.h"
#import <IQKeyboardManagerSwift-Swift.h>
 #import <AWSS3/AWSS3.h>


//#define FLURRY_TOKEN @"B36MMBK8D9CJ93DCJ7VJ"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@import FirebaseAnalytics;

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif
#import "VersionServiceResponse.h"


#define FLURRY_TOKEN @"B36MMBK8D9CJ93DCJ7VJ"

static NSString *alertAssociationKey  = @"alertAssociationKey";
static NSString *actionAssociationKey  = @"actionAssociationKey";

static NSString *AWS_ACCESS_KEY1  = @"AKIA44PP3RCTARWGIE6S"; //@"AKIA44PP3RCTB3OGHC4Q"; //@"AKIAJM3EUJGYK546JWCA";
static NSString *AWS_SECRET_KEY1  = @"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";
//@"6CW7CkyEPqC+D5fJcYFVNRvcAAVt1xtqsyYqJ71w";

@interface AppDelegate () <SWRevealViewControllerDelegate, SorryAlertProtocol,UIAlertViewDelegate,TAGContainerOpenerNotifier> //GIDSignInDelegate
{
    //UIVisualEffectView *blurEffectView;
    CLLocationManager *locationManager;
    float currentlatitude ;
    float currentlongitude;
    //NSTimer *timer;
    NSString *preAuthAmount , *kBookingId , *kTrackId;
    NSMutableArray *timersMA;
    CommonApiClass *commonapi;
    NSInteger bookingType ;
    NSDictionary *kPaymentInfoDict;
    SWRevealViewController *revealController ;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    [[FIRAnalyticsConfiguration sharedInstance] setAnalyticsCollectionEnabled:YES];
    [[FIRConfiguration sharedInstance] setLoggerLevel:FIRLoggerLevelMin];
    //FirebaseConfiguration.sharedInstance().setLoggerLevel(.Error)
    [[FIRMessaging messaging] setRemoteMessageDelegate:self];
    [[IQKeyboardManager shared] enable];
    //IQKeyboardManager.shared.enable = true
    
    AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc]initWithAccessKey:AWS_ACCESS_KEY1 secretKey:AWS_SECRET_KEY1];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionAPSoutheast1 credentialsProvider:credentialsProvider];
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    @try
    {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:K_AccessToken] == nil)
        {
            [self getAccessTokenWithCompletion:^{ //Swati added this line on 9 June
                NSLog(@"Done lets check version API");
                        //Check if its first time
                if ([CommonFunctions reachabiltyCheck])
                {
                    /*__weak AppDelegate *weakSelf = self;
                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            [weakSelf getRoadsideSupportPhone];
                            [weakSelf onFetchAppVersions];
                            [weakSelf getServiceCenterPhone];
                   });*/
                }
            }];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"@exception = %@",exception);
    }
    
    if ([CommonFunctions reachabiltyCheck])
    {
        __weak AppDelegate *weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf getRoadsideSupportPhone];
            [weakSelf onFetchAppVersions];
            [weakSelf getServiceCenterPhone];
        });
    }
    
    /*if ([CommonFunctions reachabiltyCheck])
    {
        [self getAccessTokenWithCompletion:^{ //Swati added this line on 9 June
        NSLog(@"Done lets check version API");
        //Check if its first time
        __weak AppDelegate *weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [weakSelf getRoadsideSupportPhone];
                [weakSelf onFetchAppVersions];
                [weakSelf getServiceCenterPhone];
            });
        }];
    }*/
    
    
    /*Swati you have commented a method didTouchupInside method in UserProfileVC.m
     cell.btnDob.didTouchUpInside = ^(Button *sender)*/
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    [InviteReferrals setupWithBrandId:2926 encryptedKey:@"1B3596545F3AF427193DA8B36E523D4F"];
    
    NSLog(@"hhhh");

#ifdef DEBUG
    //[GMSServices provideAPIKey:@"AIzaSyBbJf7chx86Xw1XnMXkhsmT5jC7t_uJgTE"];
   // [GMSPlacesClient provideAPIKey:@"AIzaSyBbJf7chx86Xw1XnMXkhsmT5jC7t_uJgTE"];
    [GMSServices provideAPIKey:kPlacesAPIKey];
    [GMSPlacesClient provideAPIKey:kPlacesAPIKey];
#else
    [GMSServices provideAPIKey:kPlacesAPIKey];
    [GMSPlacesClient provideAPIKey:kPlacesAPIKey];
#endif
    
    [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};

    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
//    [GIDSignIn sharedInstance].delegate = self;
    
    /*if ([CommonFunctions reachabiltyCheck]) {
        __weak AppDelegate *weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf getRoadsideSupportPhone];
            [weakSelf onFetchAppVersions];
            [weakSelf getServiceCenterPhone];
        });
    }*/ //Swati Commented this on 9 June 2021
    
    UILocalNotification *localNotif =
    [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif)
    {
        [self application:application didReceiveLocalNotification:localNotif];
    }
    //This line commented by Swati It checks the Version Of App
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onVersionNotificationReceived:) name:@"versionInfoReceived" object:nil];
    [self enableUserLocation];
    
    // Sourabh - Firebase APNS code
    // Adding Push notification code
    // Register for remote notifications
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter]
             requestAuthorizationWithOptions:authOptions
             completionHandler:^(BOOL granted, NSError * _Nullable error) {
             }
             ];
            
            // For iOS 10 display notification (sent via APNS)
            [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
            // For iOS 10 data message (sent via FCM)
            [[FIRMessaging messaging] setRemoteMessageDelegate:self];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
#ifdef DEBUG
    
#else
    
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        if (!error && params) {
            NSLog(@"params: %@", params.description);
        }
    }];
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Splash Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    // Crashlytics integration
    if (FIRApp.defaultApp == nil){
    [FIRApp configure];
    }
    [Fabric.sharedSDK setDebug:YES];
    
    //Sourabh
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
#endif

    [[UITabBar appearance] setTintColor:UIColorFromRGBForNavigationBar(0xDF4731)];

    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

    if ([CommonFunctions reachabiltyCheck]) {
        //[GMSPlacesClient provideAPIKey:@"AIzaSyCkZGXtthXoOIomBu_NqK7JjPDjm-6K5Sk"];
        //@"AIzaSyBDPss1QKvR8HY-Of9qkAUjMRCmF1eLqbg"];
        
        // Start GTM Code
        
        self.tagManager = [TAGManager instance];
        
        //Optional: Change the LogLevel to Verbose to enable logging at VERBOSE and higher levels.
        [self.tagManager.logger setLogLevel:kTAGLoggerLogLevelVerbose];
        
        /*
         * Opens a container.
         *
         * @param containerId The ID of the container to load.
         * @param tagManager The TAGManager instance for getting the container.
         * @param openType The choice of how to open the container.
         * @param timeout The timeout period (default is 2.0 seconds).
         * @param notifier The notifier to inform on container load events.
         */
        
        [TAGContainerOpener openContainerWithId:@"GTM-PRXSS3"   // Update with your Container ID.
                                     tagManager:self.tagManager
                                       openType:kTAGOpenTypePreferFresh
                                        timeout:nil
                                       notifier:self];
        
        
        
        //End GTM code
    }

    /*NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GoogleService-Info" ofType:@"plist"];
    FIROptions *options = [[FIROptions alloc] initWithContentsOfFile:filePath];
    [FIRApp configureWithOptions:options];*/  //Swati Commented This
   

    NSString * str = [[NSUserDefaults standardUserDefaults] valueForKey:@"IsHelpSeen"];
    
    if ([str isEqualToString:@"Yes"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainTabStoryboard" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MainTabController"];
        [self.window setRootViewController:viewController];
        [self.window makeKeyAndVisible];
    }
    str = @"Yes";
    
    if ([str isEqualToString:@"Yes"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MainScreenVC *objMainScreenVC = [storyboard instantiateViewControllerWithIdentifier:k_MainScreenVC];
        [self.window setRootViewController:objMainScreenVC];
        [self.window makeKeyAndVisible];

    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
   // [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"FirstTimeCityFetch"];
  return true;

}

/**
 *  @brief config dynamic shortcutItems
 *  @discussion after first launch, users can see dynamic shortcutItems
 */
- (void)configDynamicShortcutItems {
    
    // config image shortcut items
    // if you want to use custom image in app bundles, use iconWithTemplateImageName method
    
    if (![DEFAULTS boolForKey:IS_LOGIN]) {
        NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
        //UIApplicationShortcutIcon *shortcutSignUP = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeContact];
        UIApplicationShortcutIcon *shortcutsignIn = [UIApplicationShortcutIcon iconWithTemplateImageName:@"sign_up"];
        
        UIApplicationShortcutItem *shortcutSignIn = [[UIApplicationShortcutItem alloc]
                                                   initWithType:[NSString stringWithFormat:@"%@.SignIN",bundleIdentifier]
                                                   localizedTitle:@"Sign In/Sign Up"
                                                   localizedSubtitle:nil
                                                   icon:shortcutsignIn
                                                   userInfo:nil];
        
//        UIApplicationShortcutItem *shortcutSignUp = [[UIApplicationShortcutItem alloc]
//                                                    initWithType:[NSString stringWithFormat:@"%@.SignUP",bundleIdentifier]
//                                                    localizedTitle:@"Sign Up"
//                                                    localizedSubtitle:@"Get going"
//                                                    icon:shortcutSignUP
//                                                    userInfo:nil];
        
        // add all items to an array
        NSArray *items = @[shortcutSignIn];
        
        // add the array to our app
        [UIApplication sharedApplication].shortcutItems = items;
    } else {
        NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
        UIApplicationShortcutIcon *shortcutBookaride = [UIApplicationShortcutIcon iconWithTemplateImageName:@"book_ride"];
        UIApplicationShortcutIcon *shortcutMyRides = [UIApplicationShortcutIcon iconWithTemplateImageName:@"my_ride"];
        UIApplicationShortcutIcon *shortcutMyProfile = [UIApplicationShortcutIcon iconWithTemplateImageName:@"profile"];
        UIApplicationShortcutIcon *shortcutfaq = [UIApplicationShortcutIcon iconWithTemplateImageName:@"faq-1"];
        
        
        UIApplicationShortcutItem *shortcutFAQ = [[UIApplicationShortcutItem alloc]
                                                  initWithType:[NSString stringWithFormat:@"%@.faq",bundleIdentifier]
                                                  localizedTitle:@"FAQ"
                                                  localizedSubtitle:nil//@"A Defenitive Guide"
                                                  icon:shortcutfaq
                                                  userInfo:nil];
        
        UIApplicationShortcutItem *shortcutRides = [[UIApplicationShortcutItem alloc]
                                                    initWithType:[NSString stringWithFormat:@"%@.MY RIDES",bundleIdentifier]
                                                    localizedTitle:@"My Rides"
                                                    localizedSubtitle:nil//@"You history with Myles"
                                                    icon:shortcutMyRides
                                                    userInfo:nil];
        
        UIApplicationShortcutItem *shortcutProfile = [[UIApplicationShortcutItem alloc]
                                                      initWithType:[NSString stringWithFormat:@"%@.MY PROFILE",bundleIdentifier]
                                                      localizedTitle:@"My Profile"
                                                      localizedSubtitle:nil//@"Your awesome profile"
                                                      icon:shortcutMyProfile
                                                      userInfo:nil];

        
        UIApplicationShortcutItem *shortcutBook = [[UIApplicationShortcutItem alloc]
                                                   initWithType:[NSString stringWithFormat:@"%@.Bookride",bundleIdentifier]
                                                   localizedTitle:@"Book a Ride"
                                                   localizedSubtitle:nil//@"Go with Myles"
                                                   icon:shortcutBookaride
                                                   userInfo:nil];
        

        
        
        // add all items to an array
        NSArray *items = @[shortcutBook, shortcutRides, shortcutProfile, shortcutFAQ];
        
        dispatch_async(dispatch_get_main_queue(), ^{

        // add the array to our app
        [UIApplication sharedApplication].shortcutItems = items;
            
        });
    }
    
}


/**
 *  @brief handle shortcut item depend on its type
 *
 *  @param shortcutItem shortcutItem  selected shortcut item with quick action.
 *
 *  @return return BOOL description
 */
- (BOOL)handleShortCutItem : (UIApplicationShortcutItem *)shortcutItem{
    
    NSString *bundleId = [NSBundle mainBundle].bundleIdentifier;
    
    NSString *shortcutBookRide = [NSString stringWithFormat:@"%@.Bookride", bundleId];
    NSString *shortcutMyRides = [NSString stringWithFormat:@"%@.MY RIDES", bundleId];
    NSString *shortcutMyProfile = [NSString stringWithFormat:@"%@.MY PROFILE", bundleId];
    NSString *shortcutFaq = [NSString stringWithFormat:@"%@.faq", bundleId];
    NSString *shortcutSignIn = [NSString stringWithFormat:@"%@.SignIN", bundleId];
    //NSString *shortcutSignUp = [NSString stringWithFormat:@"%@.SignUP", bundleId];


    __block BOOL handled = NO;
    
    if (![DEFAULTS boolForKey:IS_LOGIN]) {
        [self getAccessTokenWithCompletion:^{ //Swati Uncommented this line
            NSLog(@"Done lets check version API");
            //Check if its first time
            if ([shortcutItem.type isEqualToString:shortcutSignIn]) {
                handled = YES;
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
                LoginProcess *editController = (LoginProcess *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginProcess"];
                
                
                UINavigationController *navigationController =
                [[UINavigationController alloc] initWithRootViewController:editController];
                
                
                navigationController.viewControllers = @[editController];
                NSLog(@"controller = %@",self.window.rootViewController.navigationController.viewControllers.lastObject);
                
                [self.window.rootViewController presentViewController:navigationController animated:YES completion:nil];
            }
        }];
        
    } else {
        //if ([CommonFunctions reachabiltyCheck])
        //{
            if ([shortcutItem.type isEqualToString:shortcutBookRide]) {
                handled = YES;
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                revealController = [storyBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                UINavigationController *homeNavigationController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                [revealController setFrontViewController:homeNavigationController];
                MenuViewController *menuController =  [storyBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                menuController.selectedIndex = 0;
                [revealController setRearViewController:menuController];
                revealController.delegate = self;
                [self.window setRootViewController:revealController];
            } else if ([shortcutItem.type isEqualToString:shortcutMyRides]) {
                handled = YES;
                UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];

                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_MyRideStoryBoard bundle:nil];
                revealController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_RidesNavigation];
                [revealController setFrontViewController:navController];
                MenuViewController *menuController =  [mainStoryBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                menuController.selectedIndex = 1;
                [revealController setRearViewController:menuController];
                revealController.delegate = self;
                [self.window setRootViewController:revealController];
            } else if ([shortcutItem.type isEqualToString:shortcutMyProfile]) {
                handled = YES;
                UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_profileStoryBoard bundle:nil];
                revealController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_profileNavigation];
                [revealController setFrontViewController:navController];
                MenuViewController *menuController =  [mainStoryBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                menuController.selectedIndex = 2;
                [revealController setRearViewController:menuController];
                revealController.delegate = self;
                [self.window setRootViewController:revealController];

            } else if ([shortcutItem.type isEqualToString:shortcutFaq]) {
                handled = YES;
                UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"FAQ" bundle:nil];
                revealController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"FAQ"];
                [revealController setFrontViewController:navController];
                MenuViewController *menuController =  [mainStoryBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                menuController.selectedIndex = 8;
                [revealController setRearViewController:menuController];
                revealController.delegate = self;
                [self.window setRootViewController:revealController];
            }

       // }
//        else
//        {
//            //[self noNetworkConnectionAlert];
//            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//            NoInternetConnViewController *noInternet = [mainStoryboard instantiateViewControllerWithIdentifier:@"NoInternetConnViewController"];
//            //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            //[[[(UINavigationController *)[self.window rootViewController] viewControllers] lastObject] presentViewController:noInternet animated:NO completion:nil];
//            [self.window.rootViewController presentViewController:noInternet animated:NO completion:nil];
//            handled = YES;
//        }
    }
    
    return handled;
}

-(void)launchWithoutQuickAction {  // Swati Uncommented this method
    
    //UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"MainTabStoryboard" bundle:nil];
    
    /*[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];

            revealController = [storyBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
            UINavigationController *homeNavigationController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
            [revealController setFrontViewController:homeNavigationController];
            MenuViewController *menuController =  [storyBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
            
            if (![DEFAULTS boolForKey:IS_LOGIN])
                [menuController.btnLogout setTitle:[NSString stringWithFormat:@"SIGN IN%@SIGN UP", @"\u2215"] forState:UIControlStateNormal];
            
            [revealController setRearViewController:menuController];
            revealController.delegate = self;
            
            [self.window setRootViewController:revealController];*/
    
}


/*
 Called when the user activates your application by selecting a shortcut on the home screen, except when
 application(_:,willFinishLaunchingWithOptions:) or application(_:didFinishLaunchingWithOptions) returns `false`.
 You should handle the shortcut in those callbacks and return `false` if possible. In that case, this
 callback is used if your application is already launched in the background.
 */

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler{
    
    BOOL handledShortCutItem = [self handleShortCutItem:shortcutItem];
    
    completionHandler(handledShortCutItem);
}


+(void)initialize {
    [iRate sharedInstance].daysUntilPrompt = 5;
    
    [iRate sharedInstance].usesUntilPrompt = 10;
    
    [iRate sharedInstance].appStoreID = 1061852579;
    [iRate sharedInstance].promptForNewVersionIfUserRated = YES;
}

/*
 CODE TO READ GTM CONTAINER
 */

- (void)containerAvailable:(TAGContainer *)container {
        self.container = container;
        
        //Refresh container to get updated data withing 15 min
        [self.container refresh];
        if(self.container != NULL){
            NSString *bannerURL = [container stringForKey:@"BannerImageURL"];

            
            NSString *VersionUpdate = [container stringForKey:@"VersionUpdate"];
            NSString *RedirectURL = [container stringForKey:@"RedirectURL"];
            NSString *BannerButtonTxt = [container stringForKey:@"BannerButtonTxt"];
            NSString *redirectURLFlag = [container stringForKey:@"redirectURLFlag"];
           /*
              Convert object to the dict to pass to the desired page
             */
            NSDictionary *dict;
            
            dict = [NSDictionary dictionaryWithObjectsAndKeys:
                    bannerURL, @"BannerImageURL",
                    VersionUpdate, @"VersionUpdate",
                    RedirectURL, @"RedirectURL",
                    BannerButtonTxt, @"BannerButtonTxt",
                    redirectURLFlag,@"redirectURLFlag",
                    nil];
           // if ([VersionUpdate isEqualToString:@"true"] && (bannerURL.length > 0)) {
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"ContainerAvailable" object:nil userInfo:dict];
          //  }
            
        }
}

#pragma mark - FCM Access Token

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

/*- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FIRMessaging messaging].APNSToken = deviceToken;
}*/

#pragma mark - AccessToken
- (void)getAccessTokenWithCompletion:(void (^)(void))completion
{
    //https://mylesaccesstoken.carzonrent.com/Services/MylesTokenService.svc
    if ([CommonFunctions reachabiltyCheck]) {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache" };
        
        NSString *localSessionId = [DEFAULTS objectForKey:K_SessionID];
        localSessionId = [self generateSessionID];

        NSDictionary *parameters = @{ @"userID": @"IOS",
                                      @"password": [@"IOS@@MYLESCAR" MD5],
                                      @"IPRestriction": @(FALSE),
                                      @"sessionID": localSessionId,
                                      @"customerID": [DEFAULTS boolForKey:IS_LOGIN] ? [DEFAULTS objectForKey:KUSER_ID]:@"-99"
                                      
                                    };
        NSLog(@"parameters for accessToken sending = %@",parameters);
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:accessTokenURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:KTIMEOUT_INTERVAL];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
             if (error) {
                 NSLog(@"%@", error);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completion();
                     [self showAlert:KATryLater];
//                   [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                      UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                      NoInternetConnViewController *noInternet = [mainStoryboard instantiateViewControllerWithIdentifier:@"NoInternetConnViewController"];
                      noInternet.messageText = @"Could not reach server";
                      //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                       [_window.rootViewController presentViewController:noInternet animated:false completion:nil];
                                                                
                 });
            }
            else
            {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                            
                NSLog(@"json response from accesstoken = %@", jsonreponse);

                NSString *tokenString = [jsonreponse objectForKey:@"accessToken"];
                NSLog(@"string = %@",tokenString);
                                                        
                if (tokenString != nil)
                {
                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                   
                        NSString *tokenString = [jsonreponse objectForKey:@"accessToken"];
                        [DEFAULTS setObject:tokenString forKey:K_AccessToken];
                        [DEFAULTS setObject:localSessionId forKey:K_SessionID];
                        [DEFAULTS synchronize];
                                                                    
                         __weak AppDelegate *weakSelf = self;
                         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                 [weakSelf getRoadsideSupportPhone];
                                 [weakSelf onFetchAppVersions];
                                 [weakSelf getServiceCenterPhone];
                              });

                               if (completion) {
                                  completion();
                               }
                        });
                }
                else
                {
                       dispatch_async(dispatch_get_main_queue(), ^{
                       [DEFAULTS setObject:@"" forKey:K_AccessToken];
                       [DEFAULTS setObject:@"" forKey:K_SessionID];
                       [DEFAULTS synchronize];

                        if (completion) {
                           completion();
                        }
                     });
                    }
                   }
        }];
        [dataTask resume];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            NoInternetConnViewController *noInternet = [mainStoryboard instantiateViewControllerWithIdentifier:@"NoInternetConnViewController"];
            //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //[self.window setRootViewController:noInternet];
                //[_window.rootViewController.navigationController pushViewController:noInternet animated:NO];
            [_window.rootViewController presentViewController:noInternet animated:false completion:nil];
            //});
        });
    }

}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

-(NSString *)generateSessionID {
    int randomID = arc4random() % 9000 + 1000;
    NSString *timestampWithRandomNumber = [NSString stringWithFormat:@"%@%d",TimeStamp,randomID];
    NSString *sessionID = [NSString stringWithFormat:@"sess_%@_ios",timestampWithRandomNumber];
    NSLog(@"sessionid = %@",sessionID);
    return sessionID;
}

#pragma mark -
#pragma mark To get App's own version
//Divya for force update
- (NSString *)appNameAndVersionNumberDisplayString {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return majorVersion;
}


#pragma mark -
#pragma mark Compare/update app version
//Divya for force update
- (void)onVersionNotificationReceived:(NSNotification *)notification
{
    NSString *appVersion = nil;
    appVersion = [self appNameAndVersionNumberDisplayString];
    BOOL isVErsionDiff = [self isCurrentVersionIsDIfferent];
    NSString *updateCheckDay = [self getCurrentDay];
    
    //isVErsionDiff = YES;

    if (isVErsionDiff) {
        for (int i = 0; i < notificationMsgArr.count; i++)
        {
            NSMutableDictionary *dict = notificationMsgArr[i];
            AppVersionInfo *amenities = [dict objectForKey:@"VersionInfoData"];
            
           if ( ( ![amenities.applyOnWeek isEqual:[NSNull null]] ) && ( [amenities.applyOnWeek length] != 0 ) ) {
                NSArray *arrtemp =[amenities.applyOnWeek componentsSeparatedByString:@","];
               //amenities.appSupport = @"2";
               for (int j = 0; j<arrtemp.count; j++)
               {
                   NSString *tmpDay = arrtemp[j];
                   if ([updateCheckDay isEqualToString:tmpDay ] && [amenities.appSupport isEqualToString:@"2"]) {
                       UIAlertController *alertController = [UIAlertController
                                                             alertControllerWithTitle:@"Update Info"
                                                             message:amenities.AppVersionsdescription
                                                             preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction *cancelAction = [UIAlertAction
                                                      actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                                      style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action)
                                                      {
                                                          NSLog(@"Cancel action");
                                                      }];
                       UIAlertAction *okAction = [UIAlertAction
                                                  actionWithTitle:NSLocalizedString(@"Update", @"OK action")
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      NSString *iTunesLink = @"https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579";
                                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];                                       }];
                       
                       [alertController addAction:cancelAction];
                       [alertController addAction:okAction];
                       [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                       
                       /*UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                       alertWindow.rootViewController = [[UIViewController alloc] init];
                       alertWindow.windowLevel = UIWindowLevelAlert + 1;
                       [alertWindow makeKeyAndVisible];
                       [alertWindow.rootViewController presentViewController:alertController animated:YES completion:nil];*/
                       return;
                   }
                   else if ([updateCheckDay isEqualToString:tmpDay] && [amenities.appSupport isEqualToString:@"3"])
                   {
                       UIAlertController *alertController = [UIAlertController
                                                             alertControllerWithTitle:@"Update Info"
                                                             message:amenities.AppVersionsdescription
                                                             preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction *okAction = [UIAlertAction
                                                  actionWithTitle:NSLocalizedString(@"Update", @"OK action")
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      NSLog(@"OK action");
                                                      
                                                      NSString *iTunesLink = @"https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579";
                                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                                  }];
                       
                       [alertController addAction:okAction];
                       [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                       /*UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                       alertWindow.rootViewController = [[UIViewController alloc] init];
                       alertWindow.windowLevel = UIWindowLevelAlert + 1;
                       [alertWindow makeKeyAndVisible];
                       [alertWindow.rootViewController presentViewController:alertController animated:YES completion:nil];*/
                       return;
               }
            }
         }
      }
    }
    else {
        NSLog(@"No need to update version");
    }
}

#pragma mark -
#pragma mark Version- API Handling
//Divya for force update
- (void)onFetchAppVersions
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:@"2" forKey:@"platform"];
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kRestM1FetchAppVersions completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"]) {
                                                       if (data != nil)
                                                       {
                                                           NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                           
                                                           NSLog(@"Respose Jsononversioncheck : %@",jsonreponse);
                                                           NSData *dataRespose = [(NSMutableDictionary *)jsonreponse objectForKey:@"response"];
                                                           
                                                           NSMutableDictionary *recordResponseJson = [(NSMutableDictionary *)jsonreponse objectForKey:@"response"];
                                                           
                                                           NSLog(@"Respose Json : %@",recordResponseJson);
                                                           if (!error)
                                                           {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   NSInteger status = [[jsonreponse valueForKey:@"status"] integerValue];
                                                                   if (status == 1)
                                                                   {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           NSLog(@"Respose Json : %@",recordResponseJson);
                                                                           VersionServiceResponse *service = [[VersionServiceResponse alloc]initWithData:data error:nil];
                                                                           
                                                                           if(service.response.count > 0)
                                                                           {
                                                                               for (int i = 0 ; i < service.response.count; i++)
                                                                               {
                                                                                   NSLog(@"service.response.count =%@",service.response[i]);
                                                                                   NSMutableDictionary *dict = [NSMutableDictionary new];
                                                                                   [dict setObject:service.response[i] forKey:@"VersionInfoData"];
                                                                                   if ((notificationMsgArr != nil) && (notificationMsgArr.count > 0)) {
                                                                                       [notificationMsgArr  addObject:dict];
                                                                                   } else {
                                                                                       notificationMsgArr = [NSMutableArray array];
                                                                                       [notificationMsgArr addObject:dict];
                                                                                   }
                                                                               }
                                                                               [[NSNotificationCenter defaultCenter] postNotificationName:@"versionInfoReceived" object:nil];
                                                                           }
                                                                           
                                                                       });
                                                                   }
                                                               });
                                                           }
                                                           else
                                                           {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [self showAlert:KATryLater];
                                                                   //[CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                               });
                                                               
                                                           }
                                                       }
                                                   }
                                                }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAlert:KAInternet];
//            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        });
    }
}


//Divya for force update
-(BOOL) needsUpdate{
    
    NSString *versionInAppStore;
    NSString *localAppVersion;
    
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSLog(@"App ID : %@",appID);
    
    NSString *urlString = [NSString stringWithFormat:@"https://itunes.apple.com/lookup?bundleId=%@",@"1061852579"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response;
    NSError *error;
    NSData *data = [NSURLConnection  sendSynchronousRequest:request returningResponse: &response error: &error];
    NSError *e = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &e];
    localAppVersion = infoDictionary[@"CFBundleShortVersionString"];

    versionInAppStore = [[[jsonDict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"version"];
    //versionInAppStore = @"4.9";
    
    
    if ([versionInAppStore compare:localAppVersion options:NSNumericSearch] == NSOrderedDescending) {
        // currentVersion is lower than the version
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark To get App current Date
//Divya for force update
- (NSString *)getCurrentDay {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [[NSDate alloc] init];
    
    //get the weekday of the current date
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* components = [cal components:NSWeekdayCalendarUnit fromDate:now];
    
    NSInteger weekday = [components weekday]; // 1 = Sunday, 2 = Monday, etc.
    
    NSString *currentDay;
    switch (weekday) {
        case 1:
            currentDay =@"Sunday";
            break;
        case 2:
            currentDay =@"Monday";
            break;
        case 3:
            currentDay =@"Tuesday";
            break;
        case 4:
            currentDay =@"Wednesday";
            break;
        case 5:
            currentDay =@"Thursday";
            break;
        case 6:
            currentDay =@"Friday";
            break;
        default:
            currentDay =@"Saturday";
            break;
    }
    
    return currentDay;
}

#pragma mark -
#pragma mark Version- Lets you know if the current version is different

-(BOOL) isCurrentVersionIsDIfferent{

        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString* appID = infoDictionary[@"CFBundleIdentifier"];
        NSString *urlString = [NSString stringWithFormat:@"https://itunes.apple.com/lookup?bundleId=%@",appID];
        NSLog(@"URL String: %@",urlString);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

        NSURLResponse *response;
        NSError *error;
        NSData *data = [NSURLConnection  sendSynchronousRequest:request returningResponse: &response error: &error];
        NSError *e = nil;
    if (data != nil) {
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &e];
        NSLog(@"JSON Dict: %@",jsonDict);
        NSString *localAppVersion = infoDictionary[@"CFBundleShortVersionString"];
        NSLog(@"Local veersion: %@",localAppVersion);
        
        NSArray *arrJson = [jsonDict objectForKey:@"results"];
        
        if ([arrJson count] > 0) {
            currentStoreVersion = [[[jsonDict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"version"];
            //currentStoreVersion = @"4.9";
            
            if ([currentStoreVersion compare:localAppVersion options:NSNumericSearch] == NSOrderedDescending) {
//                 currentVersion is lower than the version
                
                NSLog(@"currentVersion is lower than the version");
                return YES;
            }else
                NSLog(@"currentVersion is higher than the version");

                return false;
    }
    }
    
        return NO;
}



-(void)cancelAllExpiredNotifications
{
    NSArray *notificationsAr = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (int i = 0; i < [notificationsAr count]; i++)
    {
        UILocalNotification *notification = [notificationsAr objectAtIndex:i];
        
        NSDate *startDate = [[notification userInfo] objectForKey:@"date"];
        NSDate *endDate = [[NSDate date] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]];
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        df.locale = twelveHourLocale;
        
        [df setDateFormat:@"yyyy-dd-MM hh:mm:ss a"];
        NSString *strDate = [df stringFromDate:startDate];
        [df setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
        
        startDate = [df dateFromString:strDate];
        //NSLog(@"booking date : %@",startDate);
        
        
        NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
        
        double minutes = timeDifference / 60;
        double hours = minutes / 60;
        double seconds = timeDifference;
        double days = minutes / 1440;
        
        //NSLog(@" days = %.0f,hours = %.2f, minutes = %.0f,seconds = %.0f", days, hours, minutes, seconds);
        
        if (seconds >= 1)
        {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
        else
        {
            //NSLog(@"Start Date is grater");
        }
    }
    notificationsAr = [[UIApplication sharedApplication] scheduledLocalNotifications];
    //NSLog(@"FINAL NOTIFICATIONS - %@",notificationsAr);
    
}

#pragma mark - Harpy Delegate Methods

- (void)harpyDidDetectNewVersionWithoutAlert:(NSString *)message{
    
    NSLog(@"App new version uploaded on apple store");
   dispatch_async(dispatch_get_main_queue(), ^{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.alertType    = 3;
    controller.versionString = message;
    
    

        [_window.rootViewController presentViewController:controller animated:YES completion:nil];
    });
}


#pragma mark - SOURABH Firebase notoifications
// [START receive_message]
// To receive notifications for iOS 9 and below.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    NSLog(@"userNotificationSOURABH123");

    // Print message ID.
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Print full message.
    NSLog(@"%@", userInfo);
    NSString *str = userInfo[@"gcm.message_id"];
    
    
    if (str != nil) {
        
        [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
        NSString *apsMessage;
        NSString *apsTitle = @"MYLES";
        NSDictionary *apsDict = [userInfo objectForKey:@"aps"];
        NSLog(@"apsdict = %@",apsDict);
        //[obj isKindOfClass:[NSDictionary class]]
        if ([[apsDict objectForKey:@"alert"] isKindOfClass:[NSDictionary class]])
        {
            apsMessage = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
            //[NSString stringWithFormat:@"%@\n%@",[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"],[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"]];
            if ([[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"] != nil) {
                apsTitle = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"];
            }
        }
        else
        {
            apsMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        }
        
        if ([apsDict objectForKey:@"category"] != nil) {
            NSLog(@"Sourabh testing for opening link");
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                CouponViewController *offerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"CouponViewController"];
              //  offerController.titleStr = [userInfo objectForKey:@"gcm.notification.mylesTitle"];
             //   offerController.urlToNavigate = [NSURL URLWithString:[apsDict objectForKey:@"category"]];
                [self.window.rootViewController presentViewController:offerController animated:YES completion: nil];
            });
        } else {
            //Normal case
            NSLog(@"apsmessage = %@",apsMessage);
            UIAlertController *apnsMessageAlertController = [UIAlertController
                                                             
                                                             alertControllerWithTitle:apsTitle
                                                             
                                                             message:apsMessage
                                                             
                                                             preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction *okAction = [UIAlertAction
                                       
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       
                                       style:UIAlertActionStyleDefault
                                       
                                       handler:^(UIAlertAction *action)
                                       
                                       {
                                           
                                           //NSLog(@"OK action");
                                           
                                           
                                       }];
            
            
            
            [apnsMessageAlertController addAction:okAction];
            
            [self.window.rootViewController presentViewController:apnsMessageAlertController animated:YES completion:nil];
        }
        
        

    } else {
        NSString *notificationType = [userInfo valueForKey:k_notificationType];
        
        if ([notificationType isEqualToString:k_feedback])
        {
            
            NSString *bookingId = [userInfo valueForKey:k_feedbackbookingid];
            UIStoryboard *stroyBoard = k_storyBoard(k_MyRideStoryBoard);
            FeedbackVC *feedBackVC = [stroyBoard instantiateViewControllerWithIdentifier:k_feedbackVc];
            if (bookingId.integerValue > 0)
            {
                feedBackVC.bookingID = bookingId;
            }
            else
            {
                feedBackVC.bookingID = @"";
            }
            [DEFAULTS setValue:bookingId forKey:kUserfeedbackBookingId];
            [CommonFunctions addFeedBackViewAsChildView :feedBackVC];
        }
        if ([notificationType isEqualToString:k_general])
        {
            UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            notificationAlert.tag  = -10;
            [notificationAlert show];
        }
        if ([notificationType isEqualToString:k_pickup])
        {
            UIStoryboard *stroyBoard = k_storyBoard(k_MyRideStoryBoard);
            NotificationVC *notificationVC = [stroyBoard instantiateViewControllerWithIdentifier:k_notificationVCIdentifier];
            [CommonFunctions addFeedBackViewAsChildView :notificationVC];
        }//bookingconfirmation
        else if ([notificationType isEqualToString:k_bookingconfirmation])
        {
            UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            notificationAlert.tag  = -10;
            [notificationAlert show];
        }

    }
    
    completionHandler(UIBackgroundFetchResultNewData);

}
// [END receive_message]


// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSLog(@"userNotificationSOURABH");
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Print full message.
    NSLog(@"%@", userInfo);
    NSString *str = userInfo[@"gcm.message_id"];
    
    if (str != nil) {
        NSString *apsMessage;
        NSDictionary *apsDict = [userInfo objectForKey:@"aps"];
        NSLog(@"apsdict = %@",apsDict);
        //[obj isKindOfClass:[NSDictionary class]]
        if ([[apsDict objectForKey:@"alert"] isKindOfClass:[NSDictionary class]])
        {
            apsMessage = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
            //[NSString stringWithFormat:@"%@\n%@",[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"],[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"]];
        }
        else
        {
            apsMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        }
        UIAlertController *apnsMessageAlertController = [UIAlertController
                                                         
                                                         alertControllerWithTitle:@"MYLES"
                                                         
                                                         message:apsMessage
                                                         
                                                         preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   
                                   style:UIAlertActionStyleDefault
                                   
                                   handler:^(UIAlertAction *action)
                                   
                                   {
                                       
                                   }];
        
        [apnsMessageAlertController addAction:okAction];
        
        [self.window.rootViewController presentViewController:apnsMessageAlertController animated:YES completion:nil];
        
        completionHandler(UIBackgroundFetchResultNewData);
       
    }
}

//-(void)userNotificationCenter:()

// Receive data message on iOS 10 devices.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", [remoteMessage appData]);
}
#endif

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    // Won't connect since there is no token
    NSLog(@"aaaaa: %@",FIRInstanceID.instanceID.token);
    if (![[FIRInstanceID instanceID] token]) {
        
        NSLog(@"aaaaa :%@",FIRInstanceID.instanceID.token);
        return;
    }
    
    //Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
    
//    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
//        if (error != nil) {
//            NSLog(@"Unable to connect to FCM. %@", error);
//        } else {
//            NSLog(@"Connected to FCM.");
//        }
//    }];
}
// [END connect_to_fcm]

#pragma mark -
#pragma mark Registering Remote Notifications

-(void)notificationRegistration
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getActiveBooking:) name:KACTIVE_BOOKING_NOTIFICATION object:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (NSString *)stringFromDeviceToken:(NSData *)deviceToken
{
    NSUInteger length = deviceToken.length;
    if (length == 0) {
        return nil;
    }
    const unsigned char *buffer = deviceToken.bytes;
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(length * 2)];
    for (int i = 0; i < length; ++i) {
        [hexString appendFormat:@"%02x", buffer[i]];
    }
    return [hexString copy];
}

- (NSString *)stringWithDeviceToken1:(NSData *)deviceToken {
    const char *data = [deviceToken bytes];
    NSMutableString *token = [NSMutableString string];

    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }

    return [token copy];
}

/**
 *  didRegisterForRemoteNotificationsWithDeviceToken
 *
 *  @param application  UIApplication Instance
 *  @param _deviceToken token
 */

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)_deviceToken {
    
//    NSLog(@"%@",[[UIApplication sharedApplication] currentUserNotificationSettings]);
//    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){ // Check it's iOS 8 and above
//        UIUserNotificationSettings *grantedSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
//        
//        if (grantedSettings.types == UIUserNotificationTypeNone) {
//            NSLog(@"No permiossion granted");
//        }
//        else if (grantedSettings.types & UIUserNotificationTypeSound & UIUserNotificationTypeAlert ){
//            NSLog(@"DONTCALLIT");
//            // Add observer for InstanceID token refresh callback.
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
//            NSLog(@"Sound and alert permissions ");
//        }
//        else if (grantedSettings.types  & UIUserNotificationTypeAlert){
//            NSLog(@"Alert Permission Granted");
//            NSLog(@"DONTCALLIT");
//            // Add observer for InstanceID token refresh callback.
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
//        }
//    }

    // Get a hex string from the device token with no spaces or < >
    NSLog(@"devicetoken String in AppDelegate= %@",_deviceToken);
    deviceToken = @"";
    
    //AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    //deviceToken = self.stringWithDeviceToken
    
    //deviceToken = [[NSString alloc] init];
    #ifdef DEBUG
        [[FIRInstanceID instanceID] setAPNSToken:_deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
    #else
        [[FIRInstanceID instanceID] setAPNSToken:_deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];

    #endif
    
    NSUInteger length = deviceToken.length;
    if (length == 0) {
            //return nil;
    }
    
    deviceToken = [[[[[_deviceToken description]
                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                    stringByReplacingOccurrencesOfString: @" " withString: @""]
                   stringByReplacingOccurrencesOfString: @"..." withString: @""];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    deviceToken = [self stringFromDeviceToken:_deviceToken];
    NSLog(@"New DEVICETOKEN = %@",deviceToken);
    //Sourabh - saving device token to nsuserdefaults
#if TARGET_IPHONE_SIMULATOR
#else
    @try {

        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"devicetoken"] == nil)
        {
            [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"devicetoken"];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"@exception = %@",exception);
    }

     NSLog(@"firebaseinstanceid **===** %@",FIRInstanceID.instanceID.token);

    
    if (FIRInstanceID.instanceID.token) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = FIRInstanceID.instanceID.token;

//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Token Details" message:[NSString stringWithFormat:@"%@",FIRInstanceID.instanceID.token] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
        
        NSLog(@"Copied...: %@",pasteboard.string);
    }
    else
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = @"No Text Copied.";
        NSLog(@"NOOOO - Copied...: %@",pasteboard.string);
    }
    
#endif
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

/**
 *  registerForRemoteNotification
 *
 *  @param _deviceToken token
 */
-(void) registerForRemoteNotification:(NSData *) _deviceToken
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
/**
 *  didFailToRegisterForRemoteNotificationsWithError
 *
 *  @param application UIApplication Instance
 *  @param error       error
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSLog(@"Failed to register with error: %@", error);
}

/**
 *  didReceiveRemoteNotification
 *
 *  @param application UIApplication Instance
 *  @param userInfo    userinfo
 */
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //NSLog(@"USERINFo  %@",userInfo);

    NSString *notificationType = [userInfo valueForKey:k_notificationType];
    
    if ([notificationType isEqualToString:k_feedback])
    {
        
        NSString *bookingId = [userInfo valueForKey:k_feedbackbookingid];
        UIStoryboard *stroyBoard = k_storyBoard(k_MyRideStoryBoard);
        FeedbackVC *feedBackVC = [stroyBoard instantiateViewControllerWithIdentifier:k_feedbackVc];
        if (bookingId.integerValue > 0)
        {
            feedBackVC.bookingID = bookingId;
        }
        else
        {
            feedBackVC.bookingID = @"";
        }
        [DEFAULTS setValue:bookingId forKey:kUserfeedbackBookingId];
        [CommonFunctions addFeedBackViewAsChildView :feedBackVC];
    }
    if ([notificationType isEqualToString:k_general])
    {
        UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        notificationAlert.tag  = -10;
        [notificationAlert show];
    }
    if ([notificationType isEqualToString:k_pickup])
    {
        UIStoryboard *stroyBoard = k_storyBoard(k_MyRideStoryBoard);
        NotificationVC *notificationVC = [stroyBoard instantiateViewControllerWithIdentifier:k_notificationVCIdentifier];
        [CommonFunctions addFeedBackViewAsChildView :notificationVC];
    }//bookingconfirmation
    else if ([notificationType isEqualToString:k_bookingconfirmation])
    {
        UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        notificationAlert.tag  = -10;
        [notificationAlert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}


#pragma mark -
#pragma mark - UILOCAL NOTIFICATIONS -

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive || state == UIApplicationStateInactive || state ==  UIApplicationStateBackground)
    {
        NSMutableDictionary *userInfo  = [[NSMutableDictionary dictionaryWithDictionary:notification.userInfo] mutableCopy];
        [[UIApplication sharedApplication] cancelLocalNotification:notification];
        
        NSInteger times = [[userInfo objectForKey:@"numberOfTimes"] integerValue];
        NSInteger twoHourInterval = 2*3600;
        
        if ( times <= 4)
        {
            [userInfo setObject:[NSNumber numberWithInteger:times+1] forKey:@"numberOfTimes"];
            
            NSDate *pickupDate = [userInfo objectForKey:@"date"] != nil ?  [userInfo objectForKey:@"date"]:@"";
            
            //NSLog(@"TIMES : %ld",(long)times);
            NSTimeInterval secondsBetween = [pickupDate timeIntervalSinceDate:[[NSDate date] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]]];
            
            float numberOfHours = secondsBetween / 3600;
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
            
            NSArray *oldNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
            
            if (numberOfHours > 2)
            {
                if (times < 3)
                {
                    notification.fireDate = [notification.fireDate dateByAddingTimeInterval:twoHourInterval];
                    //replace here
                }
                else
                    notification.fireDate = [pickupDate dateByAddingTimeInterval:-twoHourInterval];
                notification.userInfo = [NSDictionary dictionaryWithDictionary:userInfo];
                [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            }
            else
            {
                notification.fireDate = [[NSDate date] dateByAddingTimeInterval:600];
            }
            //Delay execution of my block for 5 seconds.
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self addAlert:[NSString stringWithFormat:@"%@",kpreauthreminder([userInfo objectForKey:@"preAuth"] != nil ? [userInfo objectForKey:@"preAuth"]: @"")] state:(numberOfHours <= 2)?NO:YES andBookingDetails:userInfo];
                
                
                [self addAlert:[NSString stringWithFormat:@"%@",kpreauthreminder([userInfo objectForKey:@"preAuth"])] state:(numberOfHours <= 2)?NO:YES andBookingDetails:userInfo];
            });
            oldNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
            //NSLog(@"ne wnotifications : %@",oldNotifications);
        }
    }
    // Request to reload table view data
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//    if ([DEFAULTS boolForKey:IS_LOGIN]) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (!UIAccessibilityIsReduceTransparencyEnabled()) {
//                self.window.backgroundColor = [UIColor clearColor];
//                UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//                UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//                blurEffectView.tag = 1234;
//                blurEffectView.alpha = 0;
//                blurEffectView.frame = self.window.bounds;
//                blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//                [self.window makeKeyAndVisible];
//                [self.window addSubview:blurEffectView];
//                [self.window bringSubviewToFront:blurEffectView];
//                // fade in the view
//                [UIView animateWithDuration:0.5 animations:^{
//                    blurEffectView.alpha = 1;
//                }];
//            } else {
//                self.window.backgroundColor = [UIColor blackColor];
//            }
//        });
//
//    }
    

    [DEFAULTS setBool:YES forKey:kLocationFetches];
    [DEFAULTS synchronize];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    self.currentlocation = nil;
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
    // [UIApplication sharedApplication ].keyWindow.hidden = YES;
    
    //This will make sure whatever data user might have copied, they will not be able to paste it outside of the app
    [UIPasteboard generalPasteboard].string = @"";

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableUserLocation) name:UIApplicationDidChangeStatusBarFrameNotification object:self];

    /*
     Perform check for new version of your app
     Useful if user returns to you app from background after being sent tot he App Store,
     but doesn't update their app before coming back to your app.
     
     ONLY USE THIS IF YOU ARE USING *HarpyAlertTypeForce*
     
     Also, performs version check on first launch.
     */
    //[[Harpy sharedInstance] checkVersion];
//    if (blurEffectView != nil) {
//        [blurEffectView removeFromSuperview];
//    }
    //[UIApplication sharedApplication ].keyWindow.hidden = NO;
    
    if ([CommonFunctions reachabiltyCheck]){

        if ([self.window.rootViewController.presentedViewController isKindOfClass:[NoInternetConnViewController class]]) {
            [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        }
        __weak AppDelegate *weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf getRoadsideSupportPhone];
            [weakSelf onFetchAppVersions];
            
        });
        [DEFAULTS setBool:YES forKey:kLocationFetches];
        [DEFAULTS synchronize];
        

    }else {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        if ([self.window.rootViewController.presentedViewController isKindOfClass:[LoginProcess class]]) {
            [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        }
        
        NoInternetConnViewController *noInternet = [mainStoryboard instantiateViewControllerWithIdentifier:@"NoInternetConnViewController"];
        //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //[[[(UINavigationController *)[self.window rootViewController] viewControllers] lastObject] presentViewController:noInternet animated:NO completion:nil];
        [self.window.rootViewController presentViewController:noInternet animated:NO completion:nil];
    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    if ([DEFAULTS boolForKey:IS_LOGIN]) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (!UIAccessibilityIsReduceTransparencyEnabled()) {
//                UIVisualEffectView *blurEffectView = [self.window viewWithTag:1234];
//                
//                // fade away colour view from main view
//                [UIView animateWithDuration:0.5 animations:^{
//                    blurEffectView.alpha = 0;
//                } completion:^(BOOL finished) {
//                    // remove when finished fading
//                    [blurEffectView removeFromSuperview];
//                }];
//                
//            } else {
//                self.window.backgroundColor = [UIColor clearColor];
//            }
//        });
//
//    }
    
    
    /*
     Also, performs version check on first launch.
     */
    //[[Harpy sharedInstance] checkVersionDaily];
    //[[Harpy sharedInstance] checkVersionWeekly];
    [DEFAULTS setBool:YES forKey:kLocationFetches];
    [FBSDKAppEvents activateApp];
    [DEFAULTS synchronize];
    //self.window.hidden = NO;


    [self connectToFcm];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [DEFAULTS removeObjectForKey:k_userappStatus];
}


#pragma mark- GetServiceCenterNumber
- (void)getServiceCenterPhone
{
    //sourabh
    [CommonApiClass getServiceCenterPhoneNumber:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
        NSString *statusStr = [headers objectForKey:@"Status"];
        if (statusStr != nil && [statusStr isEqualToString:@"1"]) {
            if (!error ) {
                if (data != nil) {
                NSMutableDictionary *jsonresponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                NSInteger status = [[jsonresponse valueForKey:@"status"] integerValue];
                if (status == 1)
                {
                    NSString *response = [jsonresponse valueForKey:k_contactno];
                    response = [response stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [DEFAULTS setValue:response forKey:k_mylesCenterNumber];
                    response = [jsonresponse valueForKey:k_bookinggap];
                    [DEFAULTS setValue:response forKey:k_bookinggap];
                    
                    
                }
                }
                
            }

        }  else if (statusStr != nil && [statusStr isEqualToString:@"0"]){
            NSLog(@"Access token invalid");
        }
    }];
}


#pragma mark- GetServiceCenterNumber
    - (void)getRoadsideSupportPhone
    {
        
        [CommonApiClass getRoadsideSupportCenterPhoneNumber:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            NSString *statusStr = [headers objectForKey:@"Status"];
            if (statusStr != nil && [statusStr isEqualToString:@"1"]) {
                if (!error ) {
                    NSMutableDictionary *jsonresponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    NSInteger status = [[jsonresponse valueForKey:@"status"] integerValue];
                    if (status == 1)
                    {
                        NSString *response = [jsonresponse valueForKey:k_contactno];
                        response = [response stringByReplacingOccurrencesOfString:@" " withString:@""];
                        [DEFAULTS setValue:response forKey:kRoadsideSupport];
                        response = [jsonresponse valueForKey:k_bookinggap];
                        [DEFAULTS setValue:response forKey:k_bookinggap];
                    }
                    
                    //sourabh
                    // After successful completion of API Call call Fetch Versions
                   // [self onFetchAppVersions];
                    
                    
                }
            } else if (statusStr != nil && [statusStr isEqualToString:@"0"]){
                ;
                
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [CommonFunctions logoutcommonFunction];
                });
            }
            
        }];
    }
    
    
#pragma mark- GetActive Booking
    
    - (void)getActiveBooking:(NSNotification *)type
    {
        NSLog(@"NOTIFICATION INFO : %@",(NSDictionary *)type.userInfo);
        
        bookingType = [[(NSDictionary *)type.userInfo objectForKey:@"type"] integerValue];
        
        if ([CommonFunctions reachabiltyCheck])
        {
            NSString *userid = [DEFAULTS valueForKey:KUSER_ID];
            NSMutableDictionary *body = [NSMutableDictionary new];
            [body setObject:userid forKey:@"userid"];
            NSURLSession *session = [CommonFunctions defaultSession];
            NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
            APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KgetActiveBookings completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                                   {
                                                       NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                       NSString *statusStr = [headers objectForKey:@"Status"];
                                                       if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                       {
                                                           if (data != nil)
                                                           {
                                                               NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                               
                                                               
                                                               NSLog(@"Respose Json : %@",jsonreponse);
                                                               if (!error)
                                                               {
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       NSInteger status = [[jsonreponse valueForKey:@"status"] integerValue];
                                                                       if (status == 1)
                                                                       {
                                                                           kActiveBookingData = jsonreponse.mutableCopy;
                                                                           
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               
                                                                               [self setActiveRideDetailForPreAuthCheck:kActiveBookingData];
                                                                               
                                                                           });
                                                                       }
                                                                   });
                                                               }
                                                               else
                                                               {
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [self showAlert:KATryLater];

                                                                       //[CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                                   });
                                                                   
                                                               }
                                                           }
                                                           
                                                       } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                           NSLog(@"AccessToken Invalid");
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [CommonFunctions logoutcommonFunction];
                                                           });
                                                           
                                                       }
                                                   }];
            [globalOperation addOperation:registrationOperation];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self showAlert:KAInternet];

//                [CommonFunctions alertTitle:nil withMessage:KAInternet];
            });
        }
        
    }

- (void)setActiveRideDetailForPreAuthCheck : (NSDictionary *)response
{
    
    NSArray *oldNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    NSLog(@"all notifications : %@",oldNotifications);
    
    
    ActiveRidesResponse *rideReaponse = [[ActiveRidesResponse alloc]initWithDictionary:response error:nil];
    
    for (int i = 0 ; i < rideReaponse.response.count ; i ++) {
        
        ActiveRideDetails *details = rideReaponse.response[i];
        
        if ([details.TripStatus isEqualToString:k_tripstatusNew] || [details.TripStatus isEqualToString:k_tripstatusOpen])
        {
            // Temp Work
            
            if (([details.PreAuthStatus integerValue] != 1) && ([details.PreAuthAmount integerValue] > 0))
            {
                BOOL state = NO;
                
                // If booking is cancelled
                if (bookingType == 1)
                {
                    
                    
                    
                    for (int i = 0; i  < oldNotifications.count; i++)
                    {
                        if ([details.BookingId integerValue] == [[[[oldNotifications objectAtIndex:i] userInfo] objectForKey:@"bookingId"] integerValue])
                        {
                            bookingType = 0;
                            [[UIApplication sharedApplication] cancelLocalNotification:[oldNotifications objectAtIndex:i]];
                            
                            [self cancelAllExpiredNotifications];
                            
                            return;
                        }
                    }
                    
                }
                
                for (int i = 0; i  < oldNotifications.count; i++)
                {
                    if ([details.BookingId integerValue] == [[[[oldNotifications objectAtIndex:i] userInfo] objectForKey:@"bookingId"] integerValue])
                    {
                        state = YES;
                        continue;
                    }
                }
                if (state)
                {
                    continue;
                }
                NSString *pickupdate = details.PickupDate;
                NSRange range = [pickupdate rangeOfString:@" "];
                pickupdate = [pickupdate substringToIndex:range.location];
                pickupdate = [pickupdate stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
                
                
                NSString *timeString = details.PickupTime;
                timeString = [[timeString substringWithRange:NSMakeRange(0, 2)] stringByAppendingFormat:@":%@",[timeString substringWithRange:NSMakeRange(timeString.length-2, 2)]];
                
                
                NSString *completeStartDate = [pickupdate stringByAppendingFormat:@" %@",timeString];
                
                
                NSDate *formattedDate = [CommonFunctions dateFromString:completeStartDate];
                
                formattedDate = [formattedDate dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]];
                
                
                //48*60*60
                NSDate *nextDate = [formattedDate dateByAddingTimeInterval:-48*60*60];
                
                // Booking time is more than 48 hours then scheduling Notification before 48 hours of Booking
                if ([[NSDate date] compare:nextDate] ==  NSOrderedAscending)
                {
                    NSDictionary *dict = [[NSDictionary alloc
                                           ] initWithObjects:@[[NSNumber numberWithInteger:1],details.BookingId,details.PreAuthAmount,formattedDate,details.ModelName,details.TrackId] forKeys:@[@"numberOfTimes",@"bookingId",@"preAuth",@"date",@"model",@"trackid"]];
                    
                    [self scheduleNotificationWithInfo:dict andTime:nextDate andPreAuthAmount:details.PreAuthAmount];
                    
                }
                
                // Booking time is within 48 hours then scheduling Notification after Half an hour from now
                else
                {
                    
                    NSDictionary *dict = [[NSDictionary alloc
                                           ] initWithObjects:@[[NSNumber numberWithInteger:1],details.BookingId,details.PreAuthAmount,formattedDate,details.ModelName,details.TrackId] forKeys:@[@"numberOfTimes",@"bookingId",@"preAuth",@"date",@"model",@"trackid"]];
                    
                    NSTimeInterval interval = 0.5*3600;
                    
                    NSDate *fireDate = [NSDate dateWithTimeIntervalSinceNow:interval];
                    [self scheduleNotificationWithInfo:dict andTime:fireDate andPreAuthAmount:details.PreAuthAmount];
                }
            }
        }
        
    }
    
    
    [self cancelAllExpiredNotifications];
    
    oldNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    //NSLog(@"all notifications : %@",oldNotifications);
    
}


#pragma mark -
#pragma mark - Schedule Local Notification For Security Payment -


-(void)scheduleNotificationWithInfo:(NSDictionary *)paymentInfo andTime : (NSDate *)fireDate andPreAuthAmount:(NSString *)amount
{
    /*
     UILocalNotification* localNotification = [[UILocalNotification alloc] init];
     localNotification.fireDate = fireDate;
     localNotification.alertBody = [NSString stringWithFormat:@"%@",kpreauthreminder(amount)];
     localNotification.userInfo = paymentInfo;
     localNotification.timeZone = [NSTimeZone defaultTimeZone];
     [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];*/
    
    //  if (![[NSUserDefaults standardUserDefaults] boolForKey:@"first_time"]) {
    
    // NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    // [df setBool:YES forKey:@"first_time"];
    // [df synchronize];
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:2];
    localNotification.alertBody = [NSString stringWithFormat:@"%@",kpreauthreminder(amount)];
    localNotification.userInfo = paymentInfo;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    //   }
}



#pragma mark- PreAuth
- (void)PreAuthApi : (NSString *)bookingid Track : (NSString *)trackid  Transaction : (NSString *)transactionid Amount : (NSString *)preauthamount
{
    
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:bookingid forKey:k_bookingid];
        [body setObject:trackid forKey:k_trackid];
        [body setObject:transactionid forKey:k_transactionid];
        [body setObject:preauthamount forKey:k_preauthamount];
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        [self showLoader];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KpreAuthCreation completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                   //NSLog(@"Respose Json : %@",jsonreponse);
                                                   if (!error)
                                                   {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                           [CommonFunctions alertTitle:KMessage withMessage:[jsonreponse valueForKey:k_ApiMessageKey]];
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:KACTIVE_BOOKING_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
                                                           
                                                       });
                                                   }
                                                   else
                                                   {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                           [CommonFunctions alertTitle:KSorry withMessage:[jsonreponse valueForKey:k_ApiMessageKey]];                                                     });
                                                       
                                                   }
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                       
                                                       
                                                   });
                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        });
    }
    
}

#pragma mark - SorryAlertProtocol


- (void)preAuthNetBanking{
    
    NSDictionary *dataDic = detailNotificationInfo;
    if (dataDic)
        [self payNimoController:KNetbanking TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
}

- (void)preAuthCardPayment{
    
    NSDictionary *dataDic = detailNotificationInfo;
    if (dataDic)
        [self payNimoController:KCards TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
}


#pragma mark - CUSTOM ALERTVIEW

- (void)addAlert : (NSString *)message state : (BOOL)state andBookingDetails:(NSDictionary *)detail
{
    //NSLog(@"KAlert :  %@",KAlert);
    //NSLog(@"kc :  %@",kContinue);
    
    detailNotificationInfo = detail;
    dispatch_async(dispatch_get_main_queue(), ^{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.alertType    = 7;
    controller.secureInfo  = detail;
    //    UINavigationController *navController = (UINavigationController *)_window.rootViewController;
    controller.navController = (UINavigationController *)revealController.frontViewController;
    // controller.delegate = self;
    

        
        if (![[_window.rootViewController presentedViewController] isKindOfClass:[SorryAlertViewController class]]) {
            [_window.rootViewController presentViewController:controller animated:YES completion:nil];
            
        }
    });
    
    //    NSLog(@"self.navigationController.viewControllers after -> %@",_window.rootViewController.navigationController.viewControllers);
    
    
}



#pragma mark - UIActionSheetDelegate

- (void)addactionSheet : (id)delegate andState:(BOOL)state andInfo:(NSDictionary *)dict
{
    UIViewController *topmostcontroller = [CommonFunctions returntopMostController];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:KSelectPaymentMode delegate:delegate cancelButtonTitle:kCancel destructiveButtonTitle:nil otherButtonTitles:KCards,KNetbanking, nil];
    objc_setAssociatedObject(sheet, (__bridge const void *)(actionAssociationKey), dict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [sheet showInView:topmostcontroller.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSArray *titleAr = @[KCards,KNetbanking,kCancel];
    NSString *title = titleAr[buttonIndex];
    if (buttonIndex == 2)
    {
        
        NSDictionary *dict = objc_getAssociatedObject(actionSheet,(__bridge const void *)(actionAssociationKey) );
        
        kPaymentInfoDict = dict;
        if (!kSecurityState)
        {
            
            [self addAlert:[NSString stringWithFormat:@"%@",kpreauthreminder(preAuthAmount)] state:[[dict objectForKey:@"state"] boolValue] andBookingDetails:dict];
            
        }
        return;
    }
    else
    {
        commonapi = [[CommonApiClass alloc]init];
        commonapi.delegate = self;
        
        // Fix amount for paynimo test sdk - Praauth
        
        //[commonapi payNimoController:title TranactionAmount:preAuthAmount Date:[CommonFunctions formatDatePayNimo:[NSDate date]] withType:YES andEndDate:@"" withGap:0 andTrackId:kTrackId];
        
        //[commonapi payNimoController:title TranactionAmount:@"1.01" Date:[CommonFunctions formatDatePayNimo:[NSDate date]] withType:YES andEndDate:@"" withGap:0 andTrackId:kTrackId];
        
//        [commonapi payNimoController:title TranactionAmount:preAuthAmount Date:[CommonFunctions formatDatePayNimo:[NSDate date]] withType:YES andEndDate:@"" withGap:0 andTrackId:kTrackId];
    }
}
-(void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    
    //        [self postActiveNotifications:@"1"];
}



#pragma mark- CommonApiClassDelegate
//- (void)PresentPaymentScreen:(id )paymentVC
//{
//    
//    [self.window bringSubviewToFront:[(PMPaymentOptionView *)paymentVC view]];
//    
//    [[self.window rootViewController] presentViewController:(PMPaymentOptionView *)paymentVC animated:YES completion:nil];
//}
//- (void)PaymentSuccessDelagateWithData:(NSMutableDictionary *)dict
//{
//    //NSLog(@"DICT  %@",dict);
//    
//    [self PreAuthApi:kBookingId Track:[dict objectForKey:@"trackid"] Transaction:[dict objectForKey:@"transactionid"] Amount:preAuthAmount];
//    
//}
//- (void)PaymentFailureDelagate
//{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        if (!kSecurityState)
//        {
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
//            SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
//            controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//            controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            controller.alertType    = 6;
//            [_window.rootViewController presentViewController:controller animated:YES completion:nil];
//        }
//    });
//}
//- (void)PaymentCancelDelagte
//{
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        if (!kSecurityState)
//        {
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
//            SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
//            controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//            controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            controller.alertType    = 6;
//            [_window.rootViewController presentViewController:controller animated:YES completion:nil];
//            
//        }
//        
//    });
//}

//7838135034
#pragma mark- getSharedInstance
//call to get App Delegate's shared instance
+(AppDelegate *) getSharedInstance
{
        AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
        return appDelegate;
}

#pragma -mark Current Location

-(void)enableUserLocation
{
    if ([CLLocationManager locationServicesEnabled]) {
        locationManager = [CLLocationManager new];
        locationManager.delegate = self;
        locationManager.distanceFilter = 1000;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];

    }
}


- (CLLocation*)getCurrentLocation
{
    
    return self.currentlocation;
}

#pragma mark CLLocation delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSLog(@"newLocation111 %@",newLocation);
    
    BOOL shouldPost = NO;
    if ((newLocation.coordinate.latitude == 0) && (newLocation.coordinate.longitude == 0))
    {
        shouldPost = YES;
    }
    
    if (!self.currentlocation)
    {
        self.currentlocation = newLocation;
        
        [self sendLatLongGetSublocationList:self.currentlocation.coordinate];
        
    }
    
    [locationManager stopUpdatingLocation];
    
}
+(BOOL)locationAuthorized
{
    return ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized);
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    BOOL locationAllowed = [AppDelegate locationAuthorized];
    if (locationAllowed)
    {
        //NSLog(@"-------: %@", @"ENABLED");
    }
    else
    {
        //NSLog(@"-------: %@", @"DISABLED");
    }
    
    
    if ([error code] == 1)
    {
        
        //CLLocation *location = [[CLLocation alloc]initWithLatitude:0.00 longitude:0.00];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            self.currentlocation = [[CLLocation alloc]initWithLatitude:0.00 longitude:0.00];
            [self.delegate locationDidDisable];
            
        });
    }
    
    
}

#pragma mark - Geo Coding

- (void)sendLatLongGetSublocationList : (CLLocationCoordinate2D )coordinate
{
    //Send Request using Api
    if (coordinate.latitude == 0.00 && coordinate.longitude == 0.00)
    {
        
    }
    else
    {
        //GEt response
        [self reverseGeocodeLocation:self.currentlocation];
    }
}
- (void) reverseGeocodeLocation : (CLLocation *)location
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           //                           //NSLog(@"%@ ", addressDictionary);
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               
                               
                               NSString *city = [addressDictionary valueForKey:k_Citykey];
                               
                               self.Useraddress = city;
                               
                               if (city != nil)
                               {
                                   
                                   if ([city rangeOfString:@"Delhi"].location != NSNotFound)
                                   {
                                       
                                       
                                       //NSLog(@"string contains delhi!");
                                       city = @"Delhi";
                                       self.Useraddress = city;
                                   }
                               }
                               
                               
                               if(city)
                               {
                                   
                                   [self.delegate locationDidChanged:self.Useraddress location:location];
                                   
                               }
                               else
                               {
                                   // crash is happening because delgate is set to self and no uialertview delgate method is written.
                                   self.currentlocation = nil;
                                   if(self.currentlocation == nil)
                                   {
                                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kSetting message:KServiceNotAvailble delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                       [alert show];
                                   }
                               }
                               
                               
                           });
                       }
                   }];
}



#pragma mark -  Custom Dialog -


-(void) preauthPaymentNetBanking : (NSNotification *) notification {
    
    NSDictionary *dataDic = detailNotificationInfo;
    if (dataDic)
        [self payNimoController:KNetbanking TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
}

-(void) preauthPaymentCard : (NSNotification *) notification {
    
    NSDictionary *dataDic = detailNotificationInfo;
    if (dataDic)
        [self payNimoController:KCards TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
}

- (void)payNimoController : (NSString *)title TranactionAmount : (NSString *)totalamount andTrackId:(NSString *)trackId
{
    CommonApiClass *commonApi = [[CommonApiClass alloc]init];
    commonApi.delegate = self;
    preAuthAmount = totalamount;
    kBookingId = [detailNotificationInfo objectForKey:@"bookingId"];
    //Payment amount 1.01
//    [commonApi payNimoController:title TranactionAmount:totalamount Date:[self formatDate:[NSDate date]] withType:YES andEndDate:@"" withGap:0 andTrackId:trackId];
    
    //[commonApi payNimoController:title TranactionAmount:totalamount Date:[self formatDate:[NSDate date]] withType:preAuth andEndDate:[self formatDate:[[NSDate date] dateByAddingTimeInterval:kHoursBetweenDates*3600]] withGap:kHoursBetweenDates/24 andTrackId:trackId];
    
}

- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    //    [dateFormatter setDateFormat:@"MM'/'dd'/'yyyy"];
    [dateFormatter setDateFormat:@"dd'-'MM'-'yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}



//- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
//{
//    //NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
//    [alertView close];
//}



- (UIView *)createCustomViewWithDetails:(NSDictionary *)details
{
    
    //NSLog(@"details : %@",details);
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 180)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 290, 182)];
    [imageView setBackgroundColor:UIColorFromRedGreenBlue(227., 67., 48.)];
    [demoView addSubview:imageView];
    imageView.layer.cornerRadius = 7.0;
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(demoView.frame)-20, 25)];
    [title setFont:UIFONT_BOLD_RobotoFONT(18)];
    title.text = kSecurityAmount;
    title.textColor = [UIColor whiteColor];
    title.textAlignment = NSTextAlignmentCenter;
    [demoView addSubview:title];
    
    UIImageView *seperator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 40, 290, 1)];
    [seperator setBackgroundColor:[UIColor whiteColor]];
    [demoView addSubview:seperator];
    
    UILabel *bookingDetail = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 100, 20)];
    [bookingDetail setFont:UIFONT_REGULAR_RobotoFONT(15)];
    bookingDetail.text = [NSString stringWithFormat:@"Booking Id"];
    bookingDetail.textColor = [UIColor whiteColor];
    bookingDetail.textAlignment = NSTextAlignmentLeft;
    [demoView addSubview:bookingDetail];
    
    
    bookingDetail = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(demoView.frame)-140, 60, 120, 20)];
    [bookingDetail setFont:UIFONT_REGULAR_RobotoFONT(15)];
    
    bookingDetail.text = [NSString stringWithFormat:@"%@",[details objectForKey:@"bookingId"] != nil ? [details objectForKey:@"bookingId"]:@""];
    bookingDetail.textColor = [UIColor whiteColor];
    bookingDetail.textAlignment = NSTextAlignmentRight;
    [demoView addSubview:bookingDetail];
    
    
    UILabel *carName = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, CGRectGetWidth(demoView.frame)-20, 20)];
    [carName setFont:UIFONT_REGULAR_RobotoFONT(15)];
    carName.textAlignment = NSTextAlignmentLeft;
    carName.text = [NSString stringWithFormat:@"Car Model"];
    carName.textColor = [UIColor whiteColor];
    [demoView addSubview:carName];
    
    carName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(demoView.frame)-140, 90, 120, 20)];
    [carName setFont:UIFONT_REGULAR_RobotoFONT(15)];
    carName.textAlignment = NSTextAlignmentRight;
    carName.text = [NSString stringWithFormat:@"%@",[details objectForKey:@"model"] != nil ? [details objectForKey:@"model"]:@""];
    carName.textColor = [UIColor whiteColor];
    [demoView addSubview:carName];
    
    
    UILabel *bookingdate = [[UILabel alloc] initWithFrame:CGRectMake(10, 120, CGRectGetWidth(demoView.frame)-20, 20)];
    [bookingdate setFont:UIFONT_REGULAR_RobotoFONT(15)];
    bookingdate.textAlignment = NSTextAlignmentLeft;
    
    bookingdate.text = [NSString stringWithFormat:@"Pickup Date"];
    bookingdate.textColor = [UIColor whiteColor];
    [demoView addSubview:bookingdate];
    
    bookingdate = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(demoView.frame)-160, 120, 140, 20)];
    [bookingdate setFont:UIFONT_REGULAR_RobotoFONT(15)];
    bookingdate.textAlignment = NSTextAlignmentRight;
    bookingdate.text = [[NSString stringWithFormat:@"%@",[details objectForKey:@"date"] != nil ? [details objectForKey:@"model"]:@""] stringByReplacingOccurrencesOfString:@"+0000" withString:@""];
    bookingdate.textColor = [UIColor whiteColor];
    [demoView addSubview:bookingdate];
    
    UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 150, CGRectGetWidth(demoView.frame)-20, 20)];
    [messageLbl setFont:UIFONT_REGULAR_RobotoFONT(15)];
    messageLbl.textAlignment = NSTextAlignmentLeft;
    messageLbl.numberOfLines = 1;
    messageLbl.text = @"Security Amount";
    messageLbl.textColor = [UIColor whiteColor];
    [demoView addSubview:messageLbl];
    
    messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(demoView.frame)-140, 150, 120, 20)];
    [messageLbl setFont:UIFONT_REGULAR_RobotoFONT(15)];
    messageLbl.textAlignment = NSTextAlignmentRight;
    messageLbl.numberOfLines = 1;
    messageLbl.text = [NSString stringWithFormat:@"%@ /-",[details objectForKey:@"preAuth"] != nil ? [details objectForKey:@"preAuth"] : @"",nil];
    messageLbl.textColor = [UIColor whiteColor];
    [demoView addSubview:messageLbl];
    
    
    
    return demoView;
}
//(void (^)(void))completion
-(void)loginUserAPI
{
    NSMutableDictionary *body = [NSMutableDictionary new];
    
    [body setObject: [DEFAULTS objectForKey:KUSER_PHONENUMBER] forKey:@"phonenumber"];
    [body setObject:[DEFAULTS objectForKey:KUSER_PASSWORD] forKey:@"password"];
    
#if TARGET_IPHONE_SIMULATOR
    
    //Simulator
    deviceToken = @"0a826c47e82a3b2aeb14131357d470e1653b0757d4a985a8d452ad485a8951a7";
    [body setObject:deviceToken forKey:@"devicetoken"];
    
    
#else
    
    // Device
    deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    if (deviceToken.length > 0)
    {
        [body setObject:deviceToken forKey:@"devicetoken"];
    }
    else
    {
        [body setObject:@"" forKey:@"devicetoken"];
        
    }
    
    
#endif
    
    
    [body setObject:@"ios" forKey:@"devicetype"];
    

    NSLog(@"sourabh devicetoken = %@",deviceToken);
    NSLog(@"sourabh dictbody = %@",body);
    
    NSURLSession *session = [CommonFunctions defaultSession];
    [self showLoader];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSER_LOGIN completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                           {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD dismiss];
                                               });
                                               if (!error)
                                               {
                                                   NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                   
                                                   
                                                   // SUCCESSFUL LOGIN
                                                   if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 1)
                                                   {
                                                       
                                                       if (responseJson.count > 0)
                                                           [[NSUserDefaults standardUserDefaults] setObject:responseJson forKey:@"userDetail"];
                                                       
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                           
                                                           
                                                           
                                                           UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                                           revealController = [storyBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                                           
                                                           UINavigationController *homeNavigationController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                                                           
                                                           
                                                           [revealController setFrontViewController:homeNavigationController];
                                                           
                                                           
                                                           MenuViewController *menuController =  [storyBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                                                           [revealController setRearViewController:menuController];
                                                           revealController.delegate = self;
                                                           
                                                           [self.window setRootViewController:revealController];
                                                           
                                                       });
                                                   }
                                                   // LOGIN FAILED
                                                   else
                                                   {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                           [self logoutAPI];
                                                           [CommonFunctions alertTitle:nil withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                           
                                                       });
                                                       
                                                       
                                                       
                                                   }
                                                   
                                               }
                                               
                                               
                                           }];
    
    [globalOperation addOperation:registrationOperation];
    
    
    
}


- (void)logoutAPI
{
    [self showLoader];
    [CommonApiClass userLogoutApi:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        if (!error)
        {
            if (data != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    //NSLog(@"RES : %@",responseJson);
                    
                    NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                    if (status == 1)
                    {
                        [[UIApplication sharedApplication] cancelAllLocalNotifications];
                        
                        [DEFAULTS setBool:NO forKey:IS_LOGIN];
                        [DEFAULTS synchronize];
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
                        UINavigationController *registrationNavigation = [storyBoard instantiateViewControllerWithIdentifier:k_registrationNavigation];
                        self.window.rootViewController = registrationNavigation;
                    }
                });
            }
            else
            {
                

            }
            
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [CommonFunctions alertTitle:@"" withMessage:KAInternet];
            });
            
        }
        
        
    }];
    
    
}

//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary<NSString *, id> *)options {
//    return [self application:app openURL:url sourceApplication:nil annotation:@{}];
//}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    [InviteReferrals OpenUrlWithApplication: app Url:url];
    return YES;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSLog(@"SourceApplication = %@",sourceApplication);
    
    [[Branch getInstance] handleDeepLink:url];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:sourceApplication
                                                annotation:annotation];
    
    [[GIDSignIn sharedInstance] handleURL:url
                        sourceApplication:sourceApplication
                               annotation:annotation];
    
    // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
    return YES;

    
//    if ([[url absoluteString] containsString:@"fb1473523802673491"])
//    {
//        return [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                              openURL:url
//                                                    sourceApplication:sourceApplication
//                                                           annotation:annotation
//                ];
//
//    } else {
//        return
//
//    }
//    
    /*
    if ([sourceApplication isEqualToString:@"google"]) {//GIDSignIn
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];

    } else
    {
            }
    */
    

}

-(void)showAlert :(NSString *)strMessage
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Myles" message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [_window.rootViewController presentViewController:alertController animated:YES completion:nil];
    
}

// Respond to Universal Links
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    
    return handledByBranch;
}


-(void)noNetworkConnectionAlert {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        if ([self.window.rootViewController.presentedViewController isKindOfClass:[LoginProcess class]]) {
            [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        }
        
        NoInternetConnViewController *noInternet = [mainStoryboard instantiateViewControllerWithIdentifier:@"NoInternetConnViewController"];
        [self.window.rootViewController presentViewController:noInternet animated:NO completion:nil];
//        NSString *phNo = [[NSUserDefaults standardUserDefaults] objectForKey:k_mylesCenterNumber];
//        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phNo]];
//        
//        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
//            
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:KAInternet preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* call = [UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                NSString *phNo = [[NSUserDefaults standardUserDefaults] objectForKey:k_mylesCenterNumber];
//                NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phNo]];
//                [[UIApplication sharedApplication] openURL:phoneUrl];
//                
//                
//            }];
//            [alertController addAction:call];
//            
//            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
//                
//            }];
//            [alertController addAction:cancel];
//            
//            [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
//            
//            
//            
//        } else
//        {
//            UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//            [calert show];
//        }
    });
    
    

}


//    FIRDynamicLink *dynamicLink =
//    [[FIRDynamicLinks dynamicLinks] dynamicLinkFromCustomSchemeURL:url];
//
//    if (dynamicLink) {
//        // Handle the deep link. For example, show the deep-linked content or
//        // apply a promotional offer to the user's account.
//        // ...
//        return YES;
//    }
//    
//    return NO;
    


//- (BOOL)application:(UIApplication *)application
//continueUserActivity:(NSUserActivity *)userActivity
// restorationHandler:(void (^)(NSArray *))restorationHandler {
//    
//    BOOL handled = [[FIRDynamicLinks dynamicLinks]
//                    handleUniversalLink:userActivity.webpageURL
//                    completion:^(FIRDynamicLink * _Nullable dynamicLink,
//                                 NSError * _Nullable error) {
//                        // ...
//                        NSLog(@"ADDED useractivity.webpageurl = %@ and dynamic Link =%@",userActivity.webpageURL,dynamicLink);
//                    }];
//    
//    
//    return handled;
//}


//Google Sign IN

//// [START signin_handler]
//- (void)signIn:(GIDSignIn *)signIn
//didSignInForUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    // Perform any operations on signed in user here.
//    NSString *userId = user.userID;                  // For client-side use only!
//    NSString *idToken = user.authentication.idToken; // Safe to send to the server
//    NSString *fullName = user.profile.name;
//    NSString *givenName = user.profile.givenName;
//    NSString *familyName = user.profile.familyName;
//    NSString *email = user.profile.email;
//    // [START_EXCLUDE]
//    NSDictionary *statusText = @{@"statusText":
//                                     [NSString stringWithFormat:@"Signed in user: %@",
//                                      fullName]};
//    //[[NSNotificationCenter defaultCenter]
//     //postNotificationName:@"ToggleAuthUINotification"
//    // object:nil
//    // userInfo:statusText];
//    // [END_EXCLUDE]
//}
//// [END signin_handler]
//
//// This callback is triggered after the disconnect call that revokes data
//// access to the user's resources has completed.
//// [START disconnect_handler]
//- (void)signIn:(GIDSignIn *)signIn
//didDisconnectWithUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    // Perform any operations when the user disconnects from app here.
//    // [START_EXCLUDE]
//    NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"ToggleAuthUINotification"
//     object:nil
//     userInfo:statusText];
//    // [END_EXCLUDE]
//}
//// [END disconnect_handler]

//http://laurenz.io/2015/10/ios-9-uitableview-how-to-peek-and-pop-in-to-table-cells-with-3d-touch/
@end
