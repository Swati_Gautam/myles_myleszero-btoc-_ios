// 93
//1026910677415644

@protocol LocationDelegate <NSObject>
@optional
- (void) locationDidChanged : (NSString *)address location : (CLLocation *)location;
- (void) locationDidDisable;
@end

#import "CommonApiClass.h"
// Start - GTM Integration code - Divya
@class TAGManager;
@class TAGContainer;

//End - GTM Integration code



@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,CommonApiClassDelegate,UIActionSheetDelegate, UIAlertViewDelegate >
{
    BOOL kSecurityState;
    
    /*
     Data to load values in carlist view controller through alert
     */
    NSMutableArray* alertSharedArray;
    NSString *alertSublocationTitle;
    NSInteger alertSelectedCityId;
    NSInteger alertSublocationID;
    NSString *alertSubLocationAddress;
    
    NSDictionary *detailNotificationInfo;
    NSMutableArray *notificationMsgArr;
    NSString *currentStoreVersion;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CLLocation *currentlocation;
@property (strong, nonatomic) NSString *Useraddress;
@property (strong, nonatomic)id <LocationDelegate> delegate;

//propery of valiables
@property (nonatomic, retain) NSMutableArray* alertSharedArray;
@property (nonatomic, copy) NSString *alertSublocationTitle;
@property (nonatomic,assign) NSInteger alertSelectedCityId;
@property(nonatomic,assign) NSInteger alertSublocationID;
@property (nonatomic, copy) NSString *alertSubLocationAddress;
@property (nonatomic, copy) NSDictionary *detailNotificationInfo;

// Start - GTM Integration code - Divya
@property (nonatomic, strong) TAGManager *tagManager;
@property (nonatomic, strong) TAGContainer *container;
//End - GTM Integration code

@property(nonatomic, strong)  UIApplicationShortcutItem *shortcutItem;


- (CLLocation*)getCurrentLocation;
+(AppDelegate *) getSharedInstance;
- (void)getServiceCenterPhone;
- (void)getRoadsideSupportPhone;
- (void) reverseGeocodeLocation : (CLLocation *)location;
+(BOOL)locationAuthorized;
- (void)getAccessTokenWithCompletion:(void (^)(void))completion;
- (void)configDynamicShortcutItems;
-(void)noNetworkConnectionAlert;
@end

/*{
 "StateCode": "28",
 "StateName": "Andhra Pradesh"
 },*/


//    //pod 'Flurry-iOS-SDK/FlurrySDK', '~> 7.2.1'

