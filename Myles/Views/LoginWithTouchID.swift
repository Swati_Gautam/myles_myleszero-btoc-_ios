//
//  LoginWithTouch.swift
//  Myles
//
//  Created by Itteam2 on 09/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation
import LocalAuthentication


@objc protocol TouchLoginSucessOrError {
    func loginSucess(response:AnyObject)
}
@objc class LoginWithTouchID :NSObject
{
    @objc public var delegate : TouchLoginSucessOrError?

    @objc func checkDeviceSupportedTouchID() {
        let authenticationContext = LAContext()
        var error:NSError?
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            delegate?.loginSucess(response: "his device does not have a TouchID sensor." as AnyObject)
//            showAlertViewIfNoBiometricSensorHasBeenDetected()
            return
            
        }
        
        authenticationContext.evaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            localizedReason: "Only awesome people are allowed",
            reply: { [weak weakSelf = self] (success, error) -> Void in
                
                if( success ) {
                    
                    // Fingerprint recognized
                    // Go to view controller
                    self.delegate?.loginSucess(response: true as AnyObject)

                    
//                    self.navigateToAuthenticatedViewController()
                    
                }else {
                    
                    // Check if there is an error
                    if let error :NSError = error as NSError?  {
                        print(error.code)

                        var message = ""
                        switch error.code {
                            
                        case LAError.Code.appCancel.rawValue:
                            message = "Authentication was cancelled by application"
                            
                        case LAError.Code.authenticationFailed.rawValue:
                            message = "The user failed to provide valid credentials"
                            
                        case LAError.Code.invalidContext.rawValue:
                            message = "The context is invalid"
                            
                        case LAError.Code.passcodeNotSet.rawValue:
                            message = "Passcode is not set on the device"
                            
                        case LAError.Code.systemCancel.rawValue:
                            message = "Authentication was cancelled by the system"
                            
                        case LAError.Code.touchIDLockout.rawValue:
                            message = "Too many failed attempts."
                            
                        case LAError.Code.touchIDNotAvailable.rawValue:
                            message = "TouchID is not available on the device"
                            
                        case LAError.Code.userCancel.rawValue:
                            message = "The user did cancel"
                            
                        case LAError.Code.userFallback.rawValue:
                            message = "The user chose to use the fallback"
                            
                        default:
                            message = "Did not find error code on LAError object"
                            
                        }
                        self.delegate?.loginSucess(response: message as AnyObject)
print(message)
//                        self.showAlertViewAfterEvaluatingPolicyWithMessage(message)
                        
                    }
                    
                }
                
        })
    }
    
//    func showAlertWithTitle( _ title:String, message:String ) {
//        
//        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        
//        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//        alertVC.addAction(okAction)
//        
//        DispatchQueue.main.async { () -> Void in
//            
//            self.present(alertVC, animated: true, completion: nil)
//            
//        }
//        
//    }
//    func showAlertViewIfNoBiometricSensorHasBeenDetected(){
//        
//        showAlertWithTitle("Error", message: "This device does not have a TouchID sensor.")
//        
//    }
    
    /**
     This method will present an UIAlertViewController to inform the user that there was a problem with the TouchID sensor.
     
     - parameter error: the error message
     
     */
//    func showAlertViewAfterEvaluatingPolicyWithMessage( _ message:String ){
//        
//        showAlertWithTitle("Error", message: message)
//        
//    }
    /**
     This method will return an error message string for the provided error code.
     The method check the error code against all cases described in the `LAError` enum.
     If the error code can't be found, a default message is returned.
     
     - parameter errorCode: the error code
     - returns: the error message
     */
    @objc func errorMessageForLAErrorCode( _ errorCode:Int ) -> String{
        
        var message = ""
        
        switch errorCode {
            
        case LAError.Code.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.Code.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.Code.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.Code.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.Code.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.Code.touchIDLockout.rawValue:
            message = "Too many failed attempts."
            
        case LAError.Code.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
            
        case LAError.Code.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.Code.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = "Did not find error code on LAError object"
            
        }
        
        return message
        
    }
    
   
}
