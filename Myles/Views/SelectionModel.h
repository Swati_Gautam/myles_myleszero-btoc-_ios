//
//  SelectionModel.h
//  verticalTable
//
//  Created by iOS Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectionModel : NSObject
@property NSString *strSection;
@property NSString *strKey;
@property bool isOpen;

-(void)SetModelData :(NSString *)section :(NSString *)key :(BOOL)isOpen;
@end
