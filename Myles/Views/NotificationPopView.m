//
//  NotificationPopView.m
//  Myles
//
//  Created by Kavita Asija on 20/08/15.
//  Copyright (c) 2015 Sanjay Kumar  Yadav. All rights reserved.
//

#import "NotificationPopView.h"
#import "Myles-Swift.h"

@implementation NotificationPopView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
    }
    return self;
}
- (void)setaction
{
    [self.trackMyRideBtnAction addTarget:self action:@selector(trackMyRide:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancleBtnAction addTarget:self action:@selector(cancleBtn:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)trackMyRide :(UIButton*)sender
{
    UIStoryboard *storyboard = k_storyBoard(k_MyRideStoryBoard);
    TrackRide *trackVC = [storyboard instantiateViewControllerWithIdentifier:k_TrackMyRideViewController ];
    UIApplication *application = k_application;
    UINavigationController *navcontroller = (UINavigationController *)application.keyWindow.rootViewController;
    [navcontroller pushViewController:trackVC animated:trackVC];
}
- (void) cancleBtn : (UIButton *)sender
{
    [self removeFromSuperview];
}
@end
