//
//  SelectionModel.m
//  verticalTable
//
//  Created by iOS Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import "SelectionModel.h"

@implementation SelectionModel
-(void)SetModelData :(NSString *)section :(NSString *)key :(BOOL)isOpen
{
    self.strSection = section;
    self.strKey = key;
    self.isOpen = isOpen;
}

@end
