//
//  SortViewController.m
//  Myles
//
//  Created by Itteam2 on 15/09/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "SortViewController.h"
#import "SortCollectionCell.h"

@interface SortViewController ()
#define MylesColor [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:38.0/255.0 alpha:1]
#define MylesTextColor [UIColor colorWithRed:104.0/255.0 green:104.0/255.0 blue:104.0/255.0 alpha:1.0]
@end
@implementation SortViewController
{
    NSMutableArray *maImage,*maimageSelected,*maSubTitle;
}

- (void)viewDidLoad {
    
    //.estimatedItemSize = CGSize(width: 100, height: 100)

    
    maImage = [[NSMutableArray alloc]initWithObjects:@"price_low_to_high",@"price_high_to_low",@"name_a_z",@"name_z_a",@"seats_4_7",@"seats_7_4", nil];
    
    maimageSelected = [[NSMutableArray alloc]initWithObjects:@"price_low_to_high_onclick",@"price_high_to_low_onclick",@"name_a_z_onclick",@"name_z_a_onclick",@"seats_4_7_onclick",@"seats_7_4_onclick", nil];
    
    maSubTitle = [[NSMutableArray alloc]initWithObjects:@"Low to high",@"High to low",@"A-Z",@"Z-A",@"4-7",@"7-4", nil];
    
    _vwback.layer.cornerRadius = 5;
    
    NSString *myString = [NSString stringWithUTF8String:"\u2715"];
    [_btnCross setTitle:myString forState:UIControlStateNormal];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
    
    CGSize size = flowLayout.itemSize;
    
//    size.height = size.height * 2;
    size.width = (_collSort.frame.size.width / 2 )-5;
    return size;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    SortCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SortCollectionCell" forIndexPath:indexPath];
    
  //  cell.imgIcon.image = [UIImage imageNamed:maImage[indexPath.row]];
    cell.lbltitle.text = indexPath.row == 0  || indexPath.row == 1? @"Price" : indexPath.row == 2 || indexPath.row == 3  ? @"Car name":@"Seats";
    
    cell.lbllowhigh.text = maSubTitle[indexPath.row ];

    
    cell.imgIcon.image =_index != nil && [_index intValue] == indexPath.row ? [UIImage imageNamed:maimageSelected[indexPath.row]]  :[UIImage imageNamed:maImage[indexPath.row]];
    
    cell.lbltitle.textColor = _index != nil && [_index intValue] == indexPath.row ? MylesColor : MylesTextColor ;
     cell.lbllowhigh.textColor = _index != nil && [_index intValue] == indexPath.row ? MylesColor : MylesTextColor ;
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [self.delegate Sortdata:indexPath.row == 0  || indexPath.row == 1? @"IndicatedPrice" : indexPath.row == 2 || indexPath.row == 3  ? @"Model":@"SeatingCapacity" :indexPath.row % 2 == 0 ? true:false :[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)btnCross:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];

}

@end
