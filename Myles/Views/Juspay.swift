//
//  Juspay.swift
//  Myles
//
//  Created by Itteam2 on 26/09/16.
//  Copyright © 2016 Abhishek Singh. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import SVProgressHUD
import WebKit

@objc protocol BookingConfirm {
    @objc optional func bookingConfirmation(_ coricID : String,booking:String,strLat:String,strlng:String,insuranceTaken:Bool)
    @objc optional func securityPaid(_ isPaid : Bool)
    @objc optional func paymentFailure()
    @objc optional func ldwFailed()
}
@objc class Juspay :BaseViewController, WKNavigationDelegate, WKUIDelegate { //UIWebViewDelegate {
    
    //@IBOutlet weak var webPayMent: UIWebView!
    @objc var webPayMent: WKWebView!
    @objc var strJuspay = ""
    @objc var demoJson = [String:AnyObject]()
    @objc var jusPayReqData = JusPayReuestModel()
    @objc weak var delegate : BookingConfirm?
    @objc var bRentalorPreAurth = false
    @objc var strBookingId = ""
    fileprivate var lat = ""
    fileprivate var lng = ""
    @objc var  clLocationCoordinate = CLLocationCoordinate2D()
    @objc var isldwBooking = false
    @objc var myTimer:Timer!
    var isComplete = Bool()
    
  @objc  var previousDateTime:String = ""
  @objc  var currentDateTime:String = ""
  @objc  var currentMilliSec:TimeInterval = 0
  @objc var previousMilliSec:TimeInterval = 0
    
    
    @IBOutlet weak var progressBar: UIProgressView!
    //http://10.90.90.41/mergingbranchaws/
    //https://staging.mylescars.com
    //http://10.90.90.41/myleswithsecoff-live
    #if DEBUG
    //let strMainUrl = "http://10.90.90.41/mylescarslive/Bookings/"
//    old - staging.mylescars.com
    
    var strMainUrl = "https://qa2.mylescars.com/Bookings/"
    //"https://www.mylescars.com/Bookings/"
    //"http://10.90.90.41/mylesjuspayprojectfinal/Bookings/"
    #else
    var strMainUrl = "https://www.mylescars.com/Bookings/"
    #endif
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //https://www.mylescars.com/Bookings/Security Payment
    func showLoader () {
        /*SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")*/
        if(myTimer != nil){
            myTimer.invalidate()
        }
        progressBar.progress = 0.0
        isComplete = false
        progressBar.isHidden = false
        myTimer =  Timer.scheduledTimer(timeInterval: 0.01667,target: self,selector: #selector(Juspay.timerCallback),userInfo: nil,repeats: true)
    }
    @objc func timerCallback(){
        if isComplete {
            if progressBar.progress >= 1 {
                progressBar.isHidden = true
                myTimer.invalidate()
            }else{
                progressBar.progress += 0.1
            }
        }else{
            progressBar.isHidden = false
            progressBar.progress += 0.05
            if progressBar.progress >= 0.95 {
                progressBar.progress = 0.95
            }
        }
    }
    
    func askWhichTestServerToPoint()
    {
        let alertController = UIAlertController.init(title: "MYLES", message: "Please select Which Server to Point", preferredStyle: UIAlertController.Style.actionSheet)
        alertController.addAction(UIAlertAction.init(title: "QA1", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.strMainUrl = "https://qa1.mylescars.com/Bookings/"
            self.loadURLOnWebview()
        }))
        alertController.addAction(UIAlertAction.init(title: "QA2", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.strMainUrl = "https://qa2.mylescars.com/Bookings/"
            self.loadURLOnWebview()
        }))
        alertController.addAction(UIAlertAction.init(title: "staging", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.strMainUrl = "https://staging.mylescars.com/Bookings/"
            self.loadURLOnWebview()
        }))
        self.present(alertController, animated: true) {
        }
    }
    
    func loadURLOnWebview ()
    {
       showLoader()
        FIRAnalytics.logEvent(withName: "PayPageReq_NotRendered", parameters: nil)
        //self.customizeNavigationBar(!bRentalorPreAurth ? "Rental Payment" : "Security Payment", present: false)
        //self.navigationItem.setHidesBackButton(true, animated: false)
        // bRentalorPreAurth Check payment type/
        self.loadWebView(!bRentalorPreAurth ? jusPayReqData.convertToJson() as AnyObject: strBookingId as AnyObject)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(myTimer != nil){
            myTimer.invalidate()
        }
        FIRAnalytics.logEvent(withName: "Back_Paymentpg", parameters: nil)
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
         webPayMent = WKWebView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 20, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(webPayMent)
        self.webPayMent.navigationDelegate = self
        self.webPayMent.uiDelegate = self
        
        #if DEBUG
            askWhichTestServerToPoint()
        #else
            loadURLOnWebview()
        #endif
        self.tabBarController?.tabBar.isHidden = true
        self.title = (!bRentalorPreAurth ? "Rental Payment" : "Security Payment")

        
        
        // get lat lng through google api.
        let commonApi = CommonApiClass()
        commonApi.getLocationFromAddressString(jusPayReqData.strPickUpAdd+" "+jusPayReqData.strCity) {[weak weakSelf = self]  (coordinate)  in
            if ((weakSelf) != nil) {
                weakSelf!.lat = String(coordinate.latitude)
                weakSelf!.lng = String(coordinate.longitude)
                print(coordinate)
            }
                    }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.async {
          //  SVProgressHUD.dismiss()
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        
    }
    
    /**
     Post request data on juspay & load on uiwebview.
     - parameter Pass reqData in AnyObject type,TypeCast on condition base like payment type rental/preaurth.
     */
    
   fileprivate func loadWebView(_ reqData:AnyObject)  {
        
        //bRentalorPreAurth ? reqData as! String : reqData as! Data
        
        let strUrl : String
        //strUrl = !bRentalorPreAurth ? strMainUrl + "call_center_payment" : String(format: strMainUrl + "call_preauth?bookingid=%@",(reqData as! String).toBase64())
        strUrl = !bRentalorPreAurth ? strMainUrl + "app_payment" : String(format: strMainUrl + "call_preauth?bookingid=%@",(reqData as! String).toBase64())
        print(strUrl)
        let request = NSMutableURLRequest(url: URL(string: strUrl)!,
                                          cachePolicy: .reloadIgnoringLocalCacheData,
                                          timeoutInterval: 30.0)
         print(reqData)
         print(strUrl)
        // if payment type is Rental.
    
        if !bRentalorPreAurth {
            request.httpMethod = "POST"
            request.httpBody = reqData as? Data
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }

        let session = URLSession.shared
    //http://stackoverflow.com/questions/24320347/shall-we-always-use-unowned-self-inside-closure-in-swift
    
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: {[weak weakSelf = self] (data, response, error) -> Void in
        print("after submit")

        guard (error == nil) else
        {
            return
        }
        print("Payment Request:", request)
        print("url request: ", request.url?.absoluteString ?? "")
        if ((weakSelf) != nil)  {
            DispatchQueue.main.async {
                weakSelf!.webPayMent.load(request as URLRequest);
                FIRAnalytics.logEvent(withName: "PayPage_Rendered", parameters: nil)
                
                self.currentDateTime = CommonFunctions.getCurrentDate()
                self.currentMilliSec = CommonFunctions.getMilliSecond()
                let milliSec = CommonFunctions.finalMilliSecond(self.previousMilliSec, current: self.currentMilliSec)
                CommonFunctions.createStrig(forSavingData: "Ride_details_page", key: "rental_payment_screen", startTime: self.self.previousDateTime, endTime: self.currentDateTime, milliSec: milliSec)
         //       [CommonFunctions createStrigForSavingData:"Search_result_page" key:"Ride_details_page" startTime:previousDateTime endTime:(NSString *)currentDateTime milliSec:milliSec];
            }
        }
    })
    dataTask.resume()
    }
    
    func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        //http://stackoverflow.com/questions/5995210/disabling-user-selection-in-uiwebview
        isComplete = true
        if (webView.isLoading){
            //            //Disable user selection
            //            webPayMent.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitUserSelect='none'")
            //            //Disable callout
            //            webPayMent.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitTouchCallout='none'")
            
            return
        }
        
        // SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        
        // MARK: UIWebView Delegate method
        //   func webView(_ webView: UIWebView,
        //               request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
        // {
        print("urlStr = \(String(describing: navigationAction.request.url?.absoluteString))")
        
        print("url = \(String(describing: ((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"app_action/").lowercased()))))")
        if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"app_action/").lowercased())) != nil // get booking Id
        {
            self.navigationController?.popViewController(animated: false)
            let fullNameArr = (navigationAction.request.url?.absoluteString.characters.split{$0 == "/"}.map(String.init))! as [String]
            let bookingId = fullNameArr.last
            self.delegate?.bookingConfirmation!(jusPayReqData.strTrackID, booking: bookingId!, strLat: lat, strlng: lng,insuranceTaken: false)
            
            // ecommerceTracking
            let common = CommonFunctions()
            common.ecommerceTracking(!bRentalorPreAurth ? "Rental":"PerAuth", tranactionAmount: !bRentalorPreAurth ? jusPayReqData.strBasicAmount : jusPayReqData.strSecurityAmt, trackId: jusPayReqData.strTrackID, bookingId: bookingId, jusPayReqData.strModelName)
            
            // firebase analytic after rental payment completation.
            if !bRentalorPreAurth {
                FIRAnalytics.logEvent(withName: "booking_confirmed", parameters: nil)
            }
            
            
            //    print(bookingId)
            
        }
        else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"thanks").lowercased())) != nil // Thank you page
        {
            self.navigationController?.popViewController(animated: true)
            self.delegate?.securityPaid!(true)
        }
        else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"booking_fail").lowercased())) != nil// booking fail ||
        {
            FIRAnalytics.logEvent(withName: "Fail_PayPg_CarNA", parameters: nil)
            
            print("payment failure sourabh")
            self.navigationController?.popViewController(animated: true)
            self.delegate?.paymentFailure!()
            
        }
        else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"unavailability").lowercased())) != nil // booking unavailability
            
        {
            FIRAnalytics.logEvent(withName: "Fail_Ride2PayPg", parameters: nil)
            
            if(isldwBooking){
                
                moveToBackMessage(messageStr: "Sorry, you seem to have missed this LDW car, the car is not available for booking now. Please book the car with Security Deposit." , isPayment: false)
                
            }
            else{
                moveToBackMessage(messageStr: "Sorry, you seem to have missed this car, the car is not available for booking now. Please search again to book a car and get driving." , isPayment: false)
                
            }
            
        }
        else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"paid_but_unavailable").lowercased())) != nil // paid_but_unavailable
        {
            FIRAnalytics.logEvent(withName: "Fail_Paid_ButCarNA", parameters: nil)
            
            moveToBackMessage(messageStr: "We are trying hard to find a car for you. Your Payment has been successful, please wait for 10 minutes and check your email and My-Rides for confirmation of your booking, before retrying your booking!", isPayment: true)
            
        } else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("checkVisaCode").lowercased())) != nil {
            print("checkVisaCode done")
            DispatchQueue.main.async {
                self.showLoader()
            }
            
        }
        else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"thankyou_with_insurance").lowercased())) != nil //thankyou_with_insurance
        {
            //to work for now
            if let lastPathComponent = navigationAction.request.url?.lastPathComponent {
                
                self.delegate?.bookingConfirmation!(jusPayReqData.strTrackID, booking: lastPathComponent, strLat: lat, strlng: lng,insuranceTaken: true)
                
                // ecommerceTracking
                let common = CommonFunctions()
                common.ecommerceTracking("Rental", tranactionAmount: jusPayReqData.strBasicAmount, trackId: jusPayReqData.strTrackID, bookingId: lastPathComponent, jusPayReqData.strModelName)
                
                // firebase analytic after rental payment completation.
                if !bRentalorPreAurth {
                    FIRAnalytics.logEvent(withName: "booking_confirmed", parameters: nil)
                }
                
            }
            
            
        } else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"ldw").lowercased())) != nil
        {
            if let lastPathComponent = navigationAction.request.url?.lastPathComponent {
                if (lastPathComponent == "ldw" || lastPathComponent == "Myles Cover"){
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = navigationAction.request.url
                        offerController.titleStr = "LDW T&C"
                        self.present(offerController, animated: true, completion: nil)
                    }
                    
                    
                }
            }
            decisionHandler(.cancel) //return false
        } else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("http://staging.mylescars.com/pages/"+"terms_of_use_app").lowercased())) != nil
        {//http://staging.mylescars.com/pages/terms_of_use_app
            
            if let lastPathComponent = navigationAction.request.url?.lastPathComponent {
                if (lastPathComponent == "terms_of_use_app"){
                    //UIApplication.shared.openURL(request.url!)
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = navigationAction.request.url
                        offerController.titleStr = "Terms Of Use"
                        self.present(offerController, animated: true, completion: nil)
                    }
                    
                    
                }
            }
            decisionHandler(.cancel)
        } else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("https://www.mylescars.com/pages/"+"terms_of_use_app").lowercased())) != nil
        {//http://staging.mylescars.com/pages/terms_of_use_app
            //https://www.mylescars.com/pages/terms_of_use_app
            if let lastPathComponent = navigationAction.request.url?.lastPathComponent {
                if (lastPathComponent == "terms_of_use_app"){
                    //UIApplication.shared.openURL(request.url!)
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = navigationAction.request.url
                        offerController.titleStr = "Terms Of Use"
                        self.present(offerController, animated: true, completion: nil)
                    }
                }
            }
            decisionHandler(.cancel) //return false
        }
        else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("ttp://staging.mylescars.com/pages/terms_of_use_app").lowercased())) != nil
        {//http://staging.mylescars.com/pages/terms_of_use_app
            //https://www.mylescars.com/pages/terms_of_use_app
            if let lastPathComponent = navigationAction.request.url?.lastPathComponent {
                if (lastPathComponent == "terms_of_use_app"){
                    //UIApplication.shared.openURL(request.url!)
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = navigationAction.request.url
                        offerController.titleStr = "Terms Of Use"
                        self.present(offerController, animated: true, completion: nil)
                    }
                }
            }
            decisionHandler(.cancel)
        }
        else if(((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("https://www.mylescars.com/marketings/myles_offers_app?d=app").lowercased())) != nil{
            //            let storyboard = UIStoryboard(name: k_mainStoryBoard, bundle: nil)
            //            var controller = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            //
            //            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
            //            WebViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            //           controller.UrlString = "https://www.mylescars.com/marketings/myles_offers_app?d=app"
            //           self.navigationController?.pushViewController(controller, animated: true)
            decisionHandler(.allow)
            //            [[self navigationController] pushViewController:controller animated:YES];
        }
        
        
        decisionHandler(.allow)
    }
    
    func moveToBackMessage(messageStr:String , isPayment:Bool) {
        isComplete = true
        // SVProgressHUD.dismiss()
        let alertController = UIAlertController.init(title: "MYLES", message: messageStr, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.tabBarController?.tabBar.isHidden = false
            if(self.isldwBooking && !isPayment){
                self.delegate?.ldwFailed?()
                self.navigationController?.popViewController(animated: true)
            }
            else{
                self.navigationController?.popToRootViewController(animated: true)
                
            }
        }))
        self.present(alertController, animated: true) {
        }
        
    }
    
    /*func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType:  UIWebView.NavigationType) -> Bool {

    // MARK: UIWebView Delegate method
 //   func webView(_ webView: UIWebView,
  //               request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
   // {
        print("urlStr = \(String(describing: request.url?.absoluteString))")
        
        print("url = \(String(describing: ((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"app_action/").lowercased()))))")
        if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"app_action/").lowercased())) != nil // get booking Id
        {
            self.navigationController?.popViewController(animated: false)
            let fullNameArr = (request.url?.absoluteString.characters.split{$0 == "/"}.map(String.init))! as [String]
             let bookingId = fullNameArr.last
            self.delegate?.bookingConfirmation!(jusPayReqData.strTrackID, booking: bookingId!, strLat: lat, strlng: lng,insuranceTaken: false)
            
            // ecommerceTracking
            let common = CommonFunctions()
            common.ecommerceTracking(!bRentalorPreAurth ? "Rental":"PerAuth", tranactionAmount: !bRentalorPreAurth ? jusPayReqData.strBasicAmount : jusPayReqData.strSecurityAmt, trackId: jusPayReqData.strTrackID, bookingId: bookingId, jusPayReqData.strModelName)
            
            // firebase analytic after rental payment completation.
            if !bRentalorPreAurth {
                FIRAnalytics.logEvent(withName: "booking_confirmed", parameters: nil)
            }
            
         
        //    print(bookingId)
            
        }
        else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"thanks").lowercased())) != nil // Thank you page
        {
            self.navigationController?.popViewController(animated: true)
            self.delegate?.securityPaid!(true)
        }
        else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"booking_fail").lowercased())) != nil// booking fail ||
        {
            FIRAnalytics.logEvent(withName: "Fail_PayPg_CarNA", parameters: nil)

            print("payment failure sourabh")
            self.navigationController?.popViewController(animated: true)
            self.delegate?.paymentFailure!()
            
        }
        else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"unavailability").lowercased())) != nil // booking unavailability

        {
            FIRAnalytics.logEvent(withName: "Fail_Ride2PayPg", parameters: nil)

            if(isldwBooking){
                
                moveToBackMessage(messageStr: "Sorry, you seem to have missed this LDW car, the car is not available for booking now. Please book the car with Security Deposit." , isPayment: false)

            }
            else{
                moveToBackMessage(messageStr: "Sorry, you seem to have missed this car, the car is not available for booking now. Please search again to book a car and get driving." , isPayment: false)

            }
            
        }
        else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"paid_but_unavailable").lowercased())) != nil // paid_but_unavailable
        {
            FIRAnalytics.logEvent(withName: "Fail_Paid_ButCarNA", parameters: nil)

            moveToBackMessage(messageStr: "We are trying hard to find a car for you. Your Payment has been successful, please wait for 10 minutes and check your email and My-Rides for confirmation of your booking, before retrying your booking!", isPayment: true)
            
        } else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("checkVisaCode").lowercased())) != nil {
            print("checkVisaCode done")
            DispatchQueue.main.async {
                self.showLoader()
            }

        }
        else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"thankyou_with_insurance").lowercased())) != nil //thankyou_with_insurance
        {
        //to work for now
            if let lastPathComponent = request.url?.lastPathComponent {
                
                self.delegate?.bookingConfirmation!(jusPayReqData.strTrackID, booking: lastPathComponent, strLat: lat, strlng: lng,insuranceTaken: true)
                
                // ecommerceTracking
                let common = CommonFunctions()
                common.ecommerceTracking("Rental", tranactionAmount: jusPayReqData.strBasicAmount, trackId: jusPayReqData.strTrackID, bookingId: lastPathComponent, jusPayReqData.strModelName)
                
                // firebase analytic after rental payment completation.
                if !bRentalorPreAurth {
                    FIRAnalytics.logEvent(withName: "booking_confirmed", parameters: nil)
                }

            }
            

        } else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"ldw").lowercased())) != nil
        {
            if let lastPathComponent = request.url?.lastPathComponent {
                if (lastPathComponent == "ldw" || lastPathComponent == "Myles Cover"){
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = request.url
                        offerController.titleStr = "LDW T&C"
                       self.present(offerController, animated: true, completion: nil)
                    }
                    

                }
            }
            return false
        } else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("http://staging.mylescars.com/pages/"+"terms_of_use_app").lowercased())) != nil
        {//http://staging.mylescars.com/pages/terms_of_use_app
            
            if let lastPathComponent = request.url?.lastPathComponent {
                if (lastPathComponent == "terms_of_use_app"){
                    //UIApplication.shared.openURL(request.url!)
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = request.url
                        offerController.titleStr = "Terms Of Use"
                        self.present(offerController, animated: true, completion: nil)
                    }
                    
                    
                }
            }
            return false
        } else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("https://www.mylescars.com/pages/"+"terms_of_use_app").lowercased())) != nil
        {//http://staging.mylescars.com/pages/terms_of_use_app
            //https://www.mylescars.com/pages/terms_of_use_app
            if let lastPathComponent = request.url?.lastPathComponent {
                if (lastPathComponent == "terms_of_use_app"){
                    //UIApplication.shared.openURL(request.url!)
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = request.url
                        offerController.titleStr = "Terms Of Use"
                        self.present(offerController, animated: true, completion: nil)
                    }
                }
            }
            return false
        }
        else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("ttp://staging.mylescars.com/pages/terms_of_use_app").lowercased())) != nil
        {//http://staging.mylescars.com/pages/terms_of_use_app
            //https://www.mylescars.com/pages/terms_of_use_app
            if let lastPathComponent = request.url?.lastPathComponent {
                if (lastPathComponent == "terms_of_use_app"){
                    //UIApplication.shared.openURL(request.url!)
                    DispatchQueue.main.async {
                        
                        let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                        let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                        offerController.urlToNavigate = request.url
                        offerController.titleStr = "Terms Of Use"
                        self.present(offerController, animated: true, completion: nil)
                    }
                }
            }
            return false
        }
        else if(((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("https://www.mylescars.com/marketings/myles_offers_app?d=app").lowercased())) != nil{
//            let storyboard = UIStoryboard(name: k_mainStoryBoard, bundle: nil)
//            var controller = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
//            WebViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
 //           controller.UrlString = "https://www.mylescars.com/marketings/myles_offers_app?d=app"
 //           self.navigationController?.pushViewController(controller, animated: true)
            return true
//            [[self navigationController] pushViewController:controller animated:YES];
        }
        
        
        return true
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        //http://stackoverflow.com/questions/5995210/disabling-user-selection-in-uiwebview
        isComplete = true
        if (webView.isLoading){
//            //Disable user selection
//            webPayMent.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitUserSelect='none'")
//            //Disable callout
//            webPayMent.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitTouchCallout='none'")

            return
        }
        
      // SVProgressHUD.dismiss()


    }*/
    
    
}
/**
 Encode a String to Base64
 :returns:
 */
extension String {
    
    func toBase64()->String{
        
        let data = self.data(using: String.Encoding.utf8)
        
        return data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
    }
    
}


