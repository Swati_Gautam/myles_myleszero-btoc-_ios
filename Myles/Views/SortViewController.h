//
//  SortViewController.h
//  Myles
//
//  Created by Itteam2 on 15/09/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SortViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>


@property (weak, nonatomic) IBOutlet UICollectionView *collSort;
@property (weak, nonatomic) IBOutlet UIView *vwback;
@property id delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnCross;
@property NSString * index;
@end

@protocol SortProtocol

-(void)Sortdata :(NSString *)strSortTitle :(BOOL)bLowHigh :(NSString *)index;
@end
