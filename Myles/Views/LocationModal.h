//
//  LocationModal.h
//  verticalTable
//
//  Created by iOS Team on 24/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SubLocationModal : NSObject
@property (strong , nonatomic) NSMutableArray *maSubLocation;
@end


@interface LocationModal : NSObject
-(void)GetData :(NSDictionary *)dict;

@property NSString *strLocation;
@property NSString *strCityID;
@property NSString *strCityName;
@property NSString *strLatitude;
@property NSString *strSubLocationID;
@property NSString *strSublocationName;
@property NSString *strZoneId;
@property NSString *strZoneName;
//@property NSMutableArray *arrZonename;


@property (strong,nonatomic) SubLocationModal *subLocation;
@end
