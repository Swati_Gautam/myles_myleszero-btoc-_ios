//
//  LabelSubclass.m
//  verticalTable
//
//  Created by iOS Team on 24/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import "LabelSubclass.h"

@implementation LabelSubclass

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 15, 0, 5};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
