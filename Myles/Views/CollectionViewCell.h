//
//  CollectionViewCell.h
//  verticalTable
//
//  Created by IT Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak,nonatomic) IBOutlet UILabel *lbl;
@property (weak,nonatomic) IBOutlet UIView *vwBg ;
@property (weak, nonatomic) IBOutlet UIView *vwDot;
@property (weak, nonatomic) IBOutlet UILabel *lblNoSelected;

@end
