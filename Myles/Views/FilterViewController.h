
//
//  ViewController.h
//  verticalTable
//
//  Created by IT Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FilterViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate>


@property (weak ,nonatomic) IBOutlet UITableView *tblData;
@property (weak,nonatomic) IBOutlet UICollectionView *colVwData;

@property (weak,nonatomic) IBOutlet UIView *vwBack;

@property (strong, nonatomic) IBOutlet UISearchBar *scrOnTable;
@property (weak, nonatomic) IBOutlet UIButton *btnCross;
@property (weak, nonatomic) IBOutlet UIButton *btnApply;
@property (assign,nonatomic) BOOL isDoorStepDelievery;
@property (assign, nonatomic) BOOL isCityChangedApplied;
@property NSDictionary *filterDict;
@property NSDictionary *selectedfilterDict;
@property NSString * strCityId;
@property id delegate;
@end

@protocol FilterProtocol

-(void)ApplyFilter :(NSDictionary *)dict;

@end
