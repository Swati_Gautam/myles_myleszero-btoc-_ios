//
//  PriceRangeCell.m
//  verticalTable
//
//  Created by iOS Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import "PriceRangeCell.h"

@implementation PriceRangeCell
-(void)layoutSubviews
{
//    self.Slider.minValue = 0;
//    self.Slider.maxValue = 1000;
//    self.Slider.selectedMinimum = 100;
//    self.Slider.selectedMaximum =300;
    self.Slider.handleImage = [UIImage imageNamed:@"slider_handle"];
    self.Slider.selectedHandleDiameterMultiplier = 1;
    self.Slider.tintColorBetweenHandles = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:38.0/255.0 alpha:1];
    self.Slider.lineHeight = 10;
    
    /*
    NSNumberFormatter *customFormatter = [[NSNumberFormatter alloc] init];
    customFormatter.positiveSuffix = @"B";
    self.Slider.numberFormatterOverride = customFormatter;
     */

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
