//
//  FilterModel.h
//  Myles
//
//  Created by Itteam2 on 08/09/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterModel : NSObject

@property NSString *strSeatingCapacity;
@property NSString *strOriginalAmt;
@property NSString *strCarCatName;
@property NSString *strFuelType;
@end
