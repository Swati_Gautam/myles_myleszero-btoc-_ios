//
//  PriceRangeCell.h
//  verticalTable
//
//  Created by iOS Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"


@interface PriceRangeCell : UITableViewCell
@property (weak ,nonatomic) IBOutlet TTRangeSlider *Slider;
@property (weak ,nonatomic) IBOutlet UILabel *lblPriceRange;
@property (weak, nonatomic) IBOutlet UILabel *lblMIn;
@property (weak, nonatomic) IBOutlet UILabel *lblmax;


@end
