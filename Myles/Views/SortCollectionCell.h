//
//  SortCollectionCell.h
//  Myles
//
//  Created by Itteam2 on 17/09/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SortCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;
@property (weak, nonatomic) IBOutlet UILabel *lbllowhigh;

@end
