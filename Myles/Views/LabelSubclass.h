//
//  LabelSubclass.h
//  verticalTable
//
//  Created by iOS Team on 24/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelSubclass : UILabel
- (void)drawTextInRect:(CGRect)rect;
@end
