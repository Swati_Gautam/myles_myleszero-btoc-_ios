//
//  JusPayRequestModel.swift
//  Myles
//
//  Created by Itteam2 on 28/09/16.
//  Copyright © 2016 Abhishek Singh. All rights reserved.
//
import Foundation

class JusPayReuestModel : NSObject
{
    var strAddtionalService = ""{didSet{}} 
     var strAirportCharges = ""{didSet{}}
     var strBasicAmount = ""{didSet{}}
     var strCarCatName = ""{didSet{}}
     var strCarCompName = ""{didSet{}}
     var strDiscountAmount = ""{didSet{}}
     var strDiscountCode = ""{didSet{}}
     var strDropOffDate = ""{didSet{}}
     var strDropOffTime = ""{didSet{}}
     var strEmailID = ""{didSet{}}
     var strFname = ""{didSet{}}
     var strFreeduration = ""{didSet{}}
     var strIndicatedPkgID = ""{didSet{}}
     var strLname = ""{didSet{}}
     var strMobile = ""{didSet{}}
     var strModelName = ""{didSet{}}
     var strModelid = ""{didSet{}}
     var strOriginId = ""{didSet{}}
     var strOriginName = ""{didSet{}}
     var strOriginalAmount = ""{didSet{}}
     var strPackageType = ""{didSet{}}
     var strPayBackMember = ""{didSet{}}
     var strPaymentAmount = ""{didSet{}}
     var strPickUpAdd = ""{didSet{}}
     var strPickUpDate = ""{didSet{}}
     var strPickUpTime = ""{didSet{}}
     var strPickupdropoffAddress = ""{didSet{}}
     var strduration = ""{didSet{}}
     var strSecurityAmt = ""{didSet{}}
     var strServiceAmount = ""{didSet{}}
     var strSource = ""{didSet{}}
     var strSubLocationID = ""{didSet{}}
     var strSublocationAdd = ""{didSet{}}
     var strSublocationName = ""{didSet{}}
     var strTourType = ""{didSet{}}
     var strTrackID = ""{didSet{}}
     var strUserid = ""{didSet{}}
     var strVatRate = ""{didSet{}}
     var strWeekEndduration = ""{didSet{}}
     var strWeekdayduration = ""{didSet{}}
     var strCity = ""{didSet{}}
     var strApp_Version = ""{didSet{}}
     var insuranceAmt = ""
     var totalInsuranceAmt = ""
     var homepickdropamt = ""
    var homeLat = ""
    var homeLong = ""
    var CGSTRate = ""
    //var CGSTAmount = ""
    var SGSTRate = ""
    //var SGSTAmount = ""
    var IGSTRate = ""
    //var IGSTAmount = ""
    var UTGSTRate = ""
    //var UTGSTAmount = ""
    var speedGovernor = 0
    var totalDurationForDisplay = ""
    var kmIncluded = ""
    var extraKMRate = ""
    var transmissionType = ""
    var fuelType = ""
    var seatingCapacity = ""
    var luggage = ""
    var requesttype = "S"
    var insuranceSecurityAmt = ""
    
    var igstRate_Service = ""
    var cgstRate_Service = ""
    var sgstRate_Service = ""
    var utgstRate_Service = ""
    var cessRate = ""
    var taxAmt = ""
    var carVariant = ""
    var carVariantID = ""

    //KMIncluded,ExtraKMRate,TransmissionType,FuelType,SeatingCapacity and Luggage
    override init() {
        
    }
    
    @objc init(AddtionalService :String ,AirportCharges :String,BasicAmount :String, CarCatName:String,CarCompName :String,DiscountAmount:String,DiscountCode :String,DropOffDate:String,DropOffTime:String,EmailID:String,Fname:String,Freeduration:String,IndicatedPkgID:String,Lname:String,Mobile:String,ModelName:String,Modelid:String,OriginId:String,OriginName:String,OriginalAmount:String,PackageType:String,PayBackMember:String,PaymentAmount:String,PickUpAdd:String,PickUpDate:String,PickUpTime:String,PickupdropoffAddress:String,duration:String,SecurityAmt:String,ServiceAmount:String,Source:String,SubLocationID:String,SublocationAdd:String,SublocationName:String,TourType:String,TrackID:String,Userid:String,VatRate:String,WeekEndduration:String,Weekdayduration:String,City:String,App_Version:String,insuranceAmount : String,totalInsuranceAmount : String, homePickDropAmount: String,cgstRate: String,sgstRate: String ,igstRate: String,utgstRate: String,HomeLat: String,HomeLong: String,SpeedGovernor:Int,TotalDurationForDisplay:String,KMIncluded:String,ExtraKMRate:String,TransmissionType:String,FuelType:String,SeatingCapacity:String,Luggage:String,RequestType:String, InsuranceSecurityAmt:String, IGSTRate_Service:String, CGSTRate_Service:String, SGSTRate_Service:String, UTGSTRate_Service:String, CessRate:String, TaxAmount:String, CarVariant: String, CarVariantID: String)
    {
        insuranceSecurityAmt = InsuranceSecurityAmt

        strAddtionalService = AddtionalService
        strAirportCharges = AirportCharges
        strBasicAmount = BasicAmount
        strCarCatName = CarCatName
        //strCarCompName = CarCompName.components(separatedBy: " ")[0]
        
        strCarCompName = CarCompName.components(separatedBy: " ")[0]
        
        strCarCatName = CarCompName.components(separatedBy: "")[0]
        strDiscountAmount = DiscountAmount
        strDiscountCode = DiscountCode
        strDropOffDate = DropOffDate
        strDropOffTime = DropOffTime
        strEmailID = EmailID
        strFname = Fname
        strFreeduration = Freeduration
        strIndicatedPkgID = IndicatedPkgID
        strLname = Lname
        strMobile = Mobile
        strModelName = ModelName
        strModelid = Modelid
        strOriginId = OriginId
        strOriginName = OriginName
        strOriginalAmount = OriginalAmount
        strPackageType = PackageType
        strPayBackMember = PayBackMember
        strPaymentAmount = PaymentAmount
        strPickUpAdd = PickUpAdd
        strPickUpDate = PickUpDate
        strPickUpTime = PickUpTime
        strPickupdropoffAddress = PickupdropoffAddress
        strduration = duration
        strSecurityAmt = SecurityAmt
        strServiceAmount = ServiceAmount
        strSource = Source
        strSubLocationID = SubLocationID
        strSublocationAdd = SublocationAdd
        strSublocationName = SublocationName
        strTourType = TourType
        strTrackID = TrackID
        strUserid = Userid
        strVatRate = VatRate
        strWeekEndduration = WeekEndduration
        strWeekdayduration = Weekdayduration
        strCity = City
        strApp_Version = App_Version
        insuranceAmt = insuranceAmount
        totalInsuranceAmt = totalInsuranceAmount
        homepickdropamt = homePickDropAmount
        CGSTRate = cgstRate
//        CGSTAmount = cgstAmount
        SGSTRate = sgstRate
//        SGSTAmount = sgstAmount
        IGSTRate = igstRate
//        IGSTAmount = igstAmount
        UTGSTRate = utgstRate
//        UTGSTAmount = utgstAmount
        homeLat = HomeLat
        homeLong = HomeLong
        speedGovernor = SpeedGovernor
        totalDurationForDisplay = TotalDurationForDisplay
        kmIncluded = KMIncluded
        extraKMRate = ExtraKMRate
        transmissionType = TransmissionType
        fuelType = FuelType
        seatingCapacity = SeatingCapacity
        luggage = Luggage
        requesttype = RequestType
        
        igstRate_Service = IGSTRate_Service
        cgstRate_Service = CGSTRate_Service
        sgstRate_Service = SGSTRate_Service
        utgstRate_Service = UTGSTRate_Service
        cessRate = CessRate
        taxAmt = TaxAmount
        
        carVariant = CarVariant
        carVariantID = CarVariantID
    }
    
    func convertToJson() -> Data {
        
        var dict = [String:String]()
            dict["AddtionalService"] = self.strAddtionalService
         dict["AddtionalService"] = strAddtionalService
          dict["AirportCharges"] = strAirportCharges
          dict["BasicAmount"] = strBasicAmount
          dict["CarCatName"] = strCarCatName
          dict["CarCompName"] = strCarCompName
          dict["DiscountAmount"] = strDiscountAmount
          dict["DiscountCode"] = strDiscountCode
          dict["DropOffDate"] = strDropOffDate
          dict["DropOffTime"] = strDropOffTime
          dict["EmailID"] = strEmailID
          dict["Fname"] = strFname
          dict["Freeduration"] = strFreeduration
          dict["IndicatedPkgID"] = strIndicatedPkgID
          dict["Lname"] = strLname
          dict["Mobile"] = strMobile
          dict["ModelName"] = strModelName
          dict["Modelid"] = strModelid
          dict["OriginId"] = strOriginId
          dict["OriginName"] = strOriginName
          dict["OriginalAmount"] = strOriginalAmount
          dict["PackageType"] = strPackageType
          dict["PayBackMember"] = strPayBackMember
          dict["PaymentAmount"] = strPaymentAmount
          dict["PickUpAdd"] = strPickUpAdd
          dict["PickUpDate"] = strPickUpDate
          dict["PickUpTime"] = strPickUpTime
          dict["PickupdropoffAddress"] = strPickupdropoffAddress
          dict["duration"] = strduration
          dict["SecurityAmt"] = strSecurityAmt
          dict["ServiceAmount"] = strServiceAmount
          dict["Source"] = strSource
          dict["SubLocationID"] = strSubLocationID
          dict["SublocationAdd"] = strSublocationAdd
          dict["SublocationName"] = strSublocationName
          dict["TourType"] = strTourType
          dict["TrackID"] = strTrackID
          dict["Userid"] = strUserid
          dict["VatRate"] = strVatRate
          dict["WeekEndduration"] = strWeekEndduration
          dict["Weekdayduration"] = strWeekdayduration
          dict["app_version"] = strApp_Version
          dict["InsuranceAmt"] = insuranceAmt
          dict["TotalInsuranceAmt"] = totalInsuranceAmt
          dict["HomePickUpDropAmount"] = homepickdropamt
        dict["CGSTRate"] = CGSTRate
//        dict["CGSTAmount"] = CGSTAmount
        dict["SGSTRate"] = SGSTRate
//        dict["SGSTAmount"] = SGSTAmount
        dict["IGSTRate"] = IGSTRate
//        dict["IGSTAmount"] = IGSTAmount
        dict["UTGSTRate"] = UTGSTRate
//        dict["UTGSTAmount"] = UTGSTAmount
        dict["HomeLat"] = homeLat
        dict["HomeLong"] = homeLong
        dict["IsSpeedGovernor"] = String(speedGovernor)
        dict["TotalDurationForDisplay"] = totalDurationForDisplay
        dict["KMIncluded"] = kmIncluded
        dict["ExtraKMRate"] = extraKMRate
        dict["TransmissionType"] = transmissionType
        dict["FuelType"] = fuelType
        dict["SeatingCapacity"] = seatingCapacity
        dict["Luggage"] = luggage
        dict["requesttype"] = requesttype
        dict["InsuranceSecurityAmt"] = insuranceSecurityAmt
        
        dict["IGSTRate_Service"] = igstRate_Service
        dict["CGSTRate_Service"] = cgstRate_Service
        dict["SGSTRate_Service"] = sgstRate_Service
        dict["UTGSTRate_Service"] = utgstRate_Service
        dict["CessRate"] = cessRate
        dict["TaxAmount"] = taxAmt
        
        dict["CarVariant"] = carVariant
        dict["CarVariantID"] = carVariantID


        
        print(dict)
        
         let requestData =  try!  JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted
        )
        
        return requestData
    }
}
