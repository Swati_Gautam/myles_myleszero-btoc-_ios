//
//  SubCatagoriesCell.h
//  verticalTable
//
//  Created by iOS Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCatagoriesCell : UITableViewCell

@property (weak,nonatomic) IBOutlet UILabel *lblCatagories;
@property (weak,nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constantLead;

@end
