//
//  CarPropretyCell.h
//  verticalTable
//
//  Created by iOS Team on 24/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarPropretyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox1;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox2;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox3;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox4;

@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;

@property (weak, nonatomic) IBOutlet UILabel *lbl3;

@property (weak, nonatomic) IBOutlet UILabel *lbl4;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
