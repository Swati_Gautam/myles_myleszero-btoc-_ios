//
//  ViewController.m
//  verticalTable
//
//  Created by IT Team on 23/08/16.
//  Copyright © 2016 Carzonrent. All rights reserved.
//

#import "FilterViewController.h"
#import "CollectionViewCell.h"
#import "SubCatagoriesCell.h"
#import "SelectionModel.h"
#import "PriceRangeCell.h"
#import "LabelSubclass.h"
#import "carProperty.h"
#import "CarPropretyCell.h"
#import "LocationModal.h"
#import "APIOperation.h"
#import "Myles-Swift.h"

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])
#define MylesColor [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:38.0/255.0 alpha:1]
#define MylesTextColor [UIColor colorWithRed:104.0/255.0 green:104.0/255.0 blue:104.0/255.0 alpha:1.0]
#define MylesCheckBoxColor [UIColor colorWithRed:188.0/255.0 green:188.0/255.0 blue:188.0/255.0 alpha:1].CGColor

@interface FilterViewController ()
@end

@implementation FilterViewController
{
    NSArray *arrCollectionData , *arrCarCategory , *arrColIcon ,*arrColSelectIcon;
    BOOL bColorChange;
    int iCollSelection;// variable holds which collectionview cell is clicked
    NSMutableDictionary *dictCarProperty,*dictSelectCarproperty,*dictSelectPrice;
    NSMutableDictionary *dictSelectedFilterItem;
    NSMutableArray *maSelectedHeader,*demoLoc ,*maSearchData,*maSelectLoc,*maSelectCarCategory;
    UIView *vwSearch;
    LocationModal *locData;
    BOOL isSearch;
    NSString *strSearchtxt;
    NSMutableDictionary *dictSublocation;
    NSArray *arrSublockeys, *arrCProperty;
    
}

- (void)viewDidLoad {
    
     [super viewDidLoad];

    _vwBack.layer.cornerRadius =5;

    if (!_isDoorStepDelievery) {
        dictSublocation = [[NSMutableDictionary alloc] init];
        
        
        //if ([[SubLocationData sharedInstance] getSublocation].count > 0 && !_isCityChangedApplied) {
        if ([[SubLocationData sharedInstance] getSublocation].count > 0 ) {

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [self setDataOnFilterPage];
            });
            
        }
        else
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                
                [self setDataOnFilterPage];
                //removed V1_GetZoneandSublocationName API after discussion with pradeep as there was crash in tirupati location and this api was working earlier but now we get ZoneAndSublocation from V1_AvailableCarsForSearch api
            });
        }
    } else {
        [self setDataOnFilterPage];
    }
    
   
  
    
    //set icon on cross Btn
//
//
    
     arrCProperty = [[NSArray alloc]initWithObjects:@"Fuel",@"Transmission",@"Seats", nil];

    NSString *myString = [NSString stringWithUTF8String:"\u2715"];
    [_btnCross setTitle:myString forState:UIControlStateNormal];
  //  [_btnCross setTitleColor:MylesColor forState:UIControlStateNormal];
   
    if (!_isDoorStepDelievery) {
        arrCollectionData = [[NSArray alloc]initWithObjects:@"Location",@"Price",@"Car category",@"Car property", nil];
    } else {
        arrCollectionData = [[NSArray alloc]initWithObjects:@"Price",@"Car category",@"Car property", nil];
    }
    // Do any additional setup after loading the view, typically from a nib.
    
    self.colVwData.layer.borderColor = [UIColor colorWithRed:175.0/255.0 green:175.0/255.0 blue:175.0/255.0 alpha:1.0].CGColor;
    self.colVwData.layer.borderWidth = 1.0;
      dictCarProperty = [[NSMutableDictionary alloc]init];
    
    if (_filterDict != nil && _filterDict[@"catCategory"] != nil) {
        arrCarCategory = [[NSArray alloc]initWithArray:_filterDict[@"catCategory"]];
    }
    
    
    if (_filterDict != nil && _filterDict[@"fuel"] != nil)
    {
        [dictCarProperty setValue:_filterDict[@"fuel"] forKey:@"fuel"];

    }
     if (_filterDict != nil && _filterDict[@"transmission"] != nil)
    {
        [dictCarProperty setValue:_filterDict[@"transmission"] forKey:@"transmission"];


    }
     if (_filterDict != nil && _filterDict[@"seats"] != nil)
    {
        [dictCarProperty setValue:_filterDict[@"seats"] forKey:@"seats"];


    }
    if (!_isDoorStepDelievery) {
        arrColIcon = [[NSArray alloc]initWithObjects:@"location_value_filled",@"price_value_filled",@"car_category_value_filled",@"car_property_value_filled", nil];
        arrColSelectIcon = [[NSArray alloc]initWithObjects:@"location_selected",@"price_selected",@"car_category_selected",@"car_property_selected", nil];
    } else {
        arrColIcon = [[NSArray alloc]initWithObjects:@"price_value_filled",@"car_category_value_filled",@"car_property_value_filled", nil];
        arrColSelectIcon = [[NSArray alloc]initWithObjects:@"price_selected",@"car_category_selected",@"car_property_selected", nil];
    }
    
    
    _tblData.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    demoLoc = [[NSMutableArray alloc]init];
    
    _tblData.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _tblData.layer.borderWidth = 1;
    _colVwData.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _colVwData.layer.borderWidth = 1;
   
//       [_btnApply setTitle:@"Apply" forState:UIControlStateNormal];
//    [_btnApply setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 50)];
//
//    [_btnApply setImage:[UIImage imageNamed:@"apply_icn"] forState:UIControlStateNormal];

//    [_btnApply setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];

   
}

//-(void)viewDidAppear:(BOOL)animated {
//    self.tabBarController.tabBar.hidden = TRUE;
//}
//
//-(void)viewWillDisappear:(BOOL)animated {
//    self.tabBarController.tabBar.hidden = FALSE;
//}

-(void)setDataOnFilterPage
{
    if (!_isDoorStepDelievery) {
        for (NSDictionary *dictLoc in [[SubLocationData sharedInstance] subLocationDict]) {
            
            LocationModal *locDemoData = [[LocationModal alloc]init];
            [locDemoData GetData:dictLoc];
            NSLog(@"%@",locDemoData.strSublocationName);
            
            if (dictSublocation.count == 0 || dictSublocation[locDemoData.strZoneName] == nil) {
                [dictSublocation setObject:@[locDemoData] forKey:locDemoData.strZoneName];
            }
            else if (dictSublocation[locDemoData.strZoneName] != nil)
            {
                NSMutableArray *tempArr = [[NSMutableArray alloc]initWithArray:dictSublocation[locDemoData.strZoneName]];
                [tempArr addObject:locDemoData];
                [dictSublocation setObject:tempArr forKey:locDemoData.strZoneName];
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            arrSublockeys = [[NSArray alloc]initWithArray:[dictSublocation allKeys]];
            
            [self.colVwData reloadData];
            [self.tblData reloadData];
            [self filterDataSelected];
            
            
        });

    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.colVwData reloadData];
            [self.tblData reloadData];
            [self filterDataSelected];
            
            
        });
    }
}

-(void)filterDataSelected
{
    if (_selectedfilterDict[@"highprice"] != nil && _selectedfilterDict[@"lowprice"] != nil) {
        if (dictSelectPrice==nil) {
            dictSelectPrice=[[NSMutableDictionary alloc]init];
        }
        [dictSelectPrice setValue:_selectedfilterDict[@"lowprice"] forKey:@"lowprice"];
        [dictSelectPrice setValue:_selectedfilterDict[@"highprice"] forKey:@"highprice"];

    }
    
     if (_selectedfilterDict[@"CarCategory"] != nil)
    {
        if (maSelectCarCategory==nil) {
            maSelectCarCategory=[[NSMutableArray alloc]init];
        }
        [maSelectCarCategory addObjectsFromArray:_selectedfilterDict[@"CarCategory"]];
    }
    
    if (_selectedfilterDict[@"Fuel"] != nil) {
        
        if (dictSelectCarproperty==nil) {
            dictSelectCarproperty=[[NSMutableDictionary alloc]init];
        }
        [dictSelectCarproperty setValue:_selectedfilterDict[@"Fuel"] forKey:@"Fuel"];
    }
    
    if (_selectedfilterDict[@"Seats"] != nil) {
        
        if (dictSelectCarproperty==nil) {
            dictSelectCarproperty=[[NSMutableDictionary alloc]init];
        }
        [dictSelectCarproperty setValue:_selectedfilterDict[@"Seats"] forKey:@"Seats"];
    }
    
    if (_selectedfilterDict[@"Transmission"] != nil) {
        
        if (dictSelectCarproperty==nil) {
            dictSelectCarproperty=[[NSMutableDictionary alloc]init];
        }
        [dictSelectCarproperty setValue:_selectedfilterDict[@"Transmission"] forKey:@"Transmission"];
    }
    
    if (!_isDoorStepDelievery) {
        if (_selectedfilterDict[@"location"] != nil) {
            
            if (maSelectLoc==nil) {
                maSelectLoc=[[NSMutableArray alloc]init];
            }
            [maSelectLoc addObjectsFromArray:_selectedfilterDict[@"location"]];
        }
    }
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 @brief Show Selected Data on TableView
 @dev Abhishek Singh
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!_isDoorStepDelievery) {
        return iCollSelection == 0 ? !isSearch ? dictSublocation.count:1 : iCollSelection == 3 ? dictCarProperty.count : 1;
    } else {
        NSLog(@"numberOFSEctions = %lu",(unsigned long)(iCollSelection == 2 ? dictCarProperty.count : 1));
        return  iCollSelection == 2 ? dictCarProperty.count : 1;
    }
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!_isDoorStepDelievery) {
        if (iCollSelection == 0 && maSelectedHeader.count>0) {
            for (SelectionModel *seModel in maSelectedHeader)
            {
                if ([seModel.strSection integerValue] == section) {
                    //locData = demoLoc[section];
                    NSArray *arrSubLocation = [[NSArray alloc] init];
                    arrSubLocation = dictSublocation[[dictSublocation allKeys][section]];
                    
                    return !isSearch ? [arrSubLocation count] : maSearchData.count;
                }
            }
        }
        int k = iCollSelection == 0 ? !isSearch ? 0 : maSearchData.count: iCollSelection == 2 ? arrCarCategory.count:1;
        NSLog(@"k value = %d",k);
        return k;
    } else {
        int k =  iCollSelection == 1 ? arrCarCategory.count:1;
        NSLog(@"numberOfRowsInSection = %d",k);
        return k;
    }
    
//    i % 2 == 0 ? "a" : i % 3 == 0 ? "b" : i % 5 == 0 ? "c" : i % 7 == 0 ? "d" : "e";
//    string result = if i % 2 == 0 then "a"
//                    else if i % 3 == 0 then "b"
//                    else if i % 5 == 0 then "c"
//                    else if i % 7 == 0 then "d"
//                    else "e";
    
}

/*
 @brief all selection remove clear "maSelectLoc" || close open cell clear "maSelectedHeader"
 @dev Abhishek singh
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_isDoorStepDelievery) {
        switch (iCollSelection) {
            case 0:
            case 2:
                
            {
                static NSString *simpleTableIdentifier = @"SubCatagoriesCell";
                
                SubCatagoriesCell *cell = (SubCatagoriesCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubCatagoriesCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.lblCatagories.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
                
                if (iCollSelection==0) {
                    
                    
                    cell.constantLead.constant = 50;
                    [cell.btnCheckbox layoutIfNeeded];
                    
                    if (!isSearch) {
                        locData = dictSublocation[[dictSublocation allKeys][indexPath.section]][indexPath.row];
                        cell.lblCatagories.text =  locData.strSublocationName  ;
                    }
                    else
                    {
                        
                        locData = maSearchData[indexPath.row];
                        
                        NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString: locData.strSublocationName];
                        NSString *str = locData.strSublocationName;
                        NSRange range = [[str lowercaseString] rangeOfString:[strSearchtxt lowercaseString]];
                        
                        
                        [strText addAttribute:NSFontAttributeName
                                        value:[UIFont fontWithName:@"Helvetica-Bold" size:16]
                                        range:range];
                        
                        
                        
                        
                        
                        cell.lblCatagories.attributedText = strText;
                        
                        
                        
                    }
                    
                    
                    [cell.btnCheckbox addTarget:self action:@selector(btnSelectItem:) forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                    
                    
                    cell.constantLead.constant = 27;
                    [cell.btnCheckbox layoutIfNeeded];
                    
                    cell.lblCatagories.text = arrCarCategory[indexPath.row] ;
                    
                    [cell.btnCheckbox addTarget:self action:@selector(btnCPrbtnSelectCarCategoryoperty:) forControlEvents:UIControlEventTouchUpInside];
                    
                }
                
                if (![iCollSelection == 0 ? maSelectLoc : maSelectCarCategory containsObject:cell.lblCatagories.text]) {
                    cell.lblCatagories.textColor = MylesTextColor;
                    cell.btnCheckbox.layer.borderColor =MylesCheckBoxColor;
                }
                else{
                    cell.lblCatagories.textColor = MylesColor;
                    cell.btnCheckbox.layer.borderColor = MylesColor.CGColor;
                    
                    
                    NSString *myString = [NSString stringWithUTF8String:"\u2713"];
                    [cell.btnCheckbox setTitle:myString forState:UIControlStateNormal];
                    [cell.btnCheckbox setTitleColor:MylesColor forState:UIControlStateNormal];
                    
                    
                }
                
                cell.btnCheckbox.layer.borderWidth = 1.5
                ;
                
                cell.btnCheckbox.layer.cornerRadius =2.0;
                cell.btnCheckbox.backgroundColor = [UIColor clearColor];
                
                return cell;
                
            }
                break;
            case 1:
            {
                static NSString *simpleTableIdentifier = @"PriceRangeCell";
                
                PriceRangeCell *cell = (PriceRangeCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PriceRangeCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.lblPriceRange.text = [NSString stringWithFormat:@"₹ %@ - ₹ %@",dictSelectPrice.count==0? _filterDict[@"lowprice"]:dictSelectPrice[@"lowprice"],dictSelectPrice.count==0? _filterDict[@"highprice"]:dictSelectPrice[@"highprice"]];
                
                
                cell.Slider.minValue = [_filterDict[@"lowprice"] integerValue];
                cell.Slider.maxValue = [_filterDict[@"highprice"] integerValue];
                cell.Slider.selectedMinimum =  dictSelectPrice.count==0? [_filterDict[@"lowprice"] integerValue]:[dictSelectPrice[@"lowprice"] integerValue];
                cell.Slider.selectedMaximum = dictSelectPrice.count==0? [_filterDict[@"highprice"] integerValue]:[dictSelectPrice[@"highprice"] integerValue];
                cell.lblMIn.text = [NSString stringWithFormat:@"₹ %@",_filterDict[@"lowprice"]];
                cell.lblmax.text =[NSString stringWithFormat:@"₹ %@",_filterDict[@"highprice"]];
                cell.lblMIn.textColor = cell.lblmax.textColor = cell.lblPriceRange.textColor = MylesTextColor;
                return cell;
                
            }
                break;
                
            case 3:
            {
                static NSString *simpleTableIdentifier = @"CarPropertyCell";
                
                CarPropretyCell *cell = (CarPropretyCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CarPropertyCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.tag = indexPath.section;
                cell.lblTitle.text = [arrCProperty[indexPath.section] capitalizedString];
                
                NSString *myString = [NSString stringWithUTF8String:"\u2713"];
                NSArray *arrCarProperty = [[NSArray alloc] init];
                arrCarProperty = dictCarProperty[cell.lblTitle.text.lowercaseString];
                //for (int i = 0; i< [dictCarProperty[cell.lblTitle.text.lowercaseString] count] ; i++)
                for (int i = 0; i< [arrCarProperty count] ; i++)
                {
                    switch (i) {
                        case 0:{
                            //cell.lbl1.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSString *str = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSArray *properties = [str componentsSeparatedByString: @","];
                            if(properties.count >0){
                                cell.lbl1.text = properties[0];

                            }
                            else{
                                cell.lbl1.text = @"";

                            }

                            if (dictSelectCarproperty[cell.lblTitle.text] != nil)
                                cell.lbl1.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl1.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox1.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl1.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            ;
                            [cell.btnCheckBox1 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl1.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox1 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox1.layer.borderWidth = 1.5;
                            
                            [cell.btnCheckBox1 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            
                            
                            
                            break;
                        }
                        case 1:{
                            
                            //cell.lbl2.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSString *str = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSArray *properties = [str componentsSeparatedByString: @","];
                            if(properties.count >0){
                                cell.lbl2.text = properties[0];
                                
                            }
                            else{
                                cell.lbl2.text = @"";
                                
                            }
                            cell.lbl2.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl2.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox2.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl2.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            [cell.btnCheckBox2 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl2.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox2 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox2.layer.borderWidth = 1.5;
                            
                            [cell.btnCheckBox2 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            
                            break;
                        }
                        case 2:{
                           // cell.lbl3.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSString *str = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSArray *properties = [str componentsSeparatedByString: @","];
                            if(properties.count >0){
                                cell.lbl3.text = properties[0];
                                
                            }
                            else{
                                cell.lbl3.text = @"";
                                
                            }                            cell.lbl3.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl3.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox3.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl3.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            [cell.btnCheckBox3 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl3.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox3 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox3.layer.borderWidth = 1.5;
                            [cell.btnCheckBox3 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            
                            break;
                        }
                        case 3:{
                            //cell.lbl4.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSString *str = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            NSArray *properties = [str componentsSeparatedByString: @","];
                            if(properties.count >0){
                                cell.lbl4.text = properties[0];
                                
                            }
                            else{
                                cell.lbl4.text = @"";
                                
                            }                              cell.lbl4.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl4.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox4.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl4.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            [cell.btnCheckBox4 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl4.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox4 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox4.layer.borderWidth = 1.5;
                            [cell.btnCheckBox4 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            break;
                        }
                            
                        default:
                            break;
                    }
                }
                
                
                cell.btnCheckBox4.layer.cornerRadius = cell.btnCheckBox3.layer.cornerRadius = cell.btnCheckBox2.layer.cornerRadius = cell.btnCheckBox1.layer.cornerRadius = 2;
                
                
                
                
                
                ;
                
                
                return cell;
                
            }
            default:
                
                break;
                
                
        }
        return nil;
    }
    else {
        switch (iCollSelection) {
            case 1:
                
            {
                static NSString *simpleTableIdentifier = @"SubCatagoriesCell";
                
                SubCatagoriesCell *cell = (SubCatagoriesCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubCatagoriesCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.lblCatagories.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
                
                if (iCollSelection==0) {
                    
                    
                    cell.constantLead.constant = 50;
                    [cell.btnCheckbox layoutIfNeeded];
                    
                    if (!isSearch) {
                        locData = dictSublocation[[dictSublocation allKeys][indexPath.section]][indexPath.row];
                        cell.lblCatagories.text =  locData.strSublocationName  ;
                    }
                    else
                    {
                        
                        locData = maSearchData[indexPath.row];
                        
                        NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString: locData.strSublocationName];
                        NSString *str = locData.strSublocationName;
                        NSRange range = [[str lowercaseString] rangeOfString:[strSearchtxt lowercaseString]];
                        
                        
                        [strText addAttribute:NSFontAttributeName
                                        value:[UIFont fontWithName:@"Helvetica-Bold" size:16]
                                        range:range];
                        
                        
                        
                        
                        
                        cell.lblCatagories.attributedText = strText;
                        
                        
                        
                    }
                    
                    
                    [cell.btnCheckbox addTarget:self action:@selector(btnSelectItem:) forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                    
                    
                    cell.constantLead.constant = 27;
                    [cell.btnCheckbox layoutIfNeeded];
                    
                    cell.lblCatagories.text = arrCarCategory[indexPath.row] ;
                    
                    [cell.btnCheckbox addTarget:self action:@selector(btnCPrbtnSelectCarCategoryoperty:) forControlEvents:UIControlEventTouchUpInside];
                    
                }
                
                if (![iCollSelection == 0 ? maSelectLoc : maSelectCarCategory containsObject:cell.lblCatagories.text]) {
                    cell.lblCatagories.textColor = MylesTextColor;
                    cell.btnCheckbox.layer.borderColor =MylesCheckBoxColor;
                    
                    
                    
                }
                else{
                    cell.lblCatagories.textColor = MylesColor;
                    cell.btnCheckbox.layer.borderColor = MylesColor.CGColor;
                    
                    
                    NSString *myString = [NSString stringWithUTF8String:"\u2713"];
                    [cell.btnCheckbox setTitle:myString forState:UIControlStateNormal];
                    [cell.btnCheckbox setTitleColor:MylesColor forState:UIControlStateNormal];
                    
                    
                }
                
                cell.btnCheckbox.layer.borderWidth = 1.5
                ;
                
                cell.btnCheckbox.layer.cornerRadius =2.0;
                cell.btnCheckbox.backgroundColor = [UIColor clearColor];
                
                return cell;
                
            }
                break;
            case 0:
            {
                static NSString *simpleTableIdentifier = @"PriceRangeCell";
                
                PriceRangeCell *cell = (PriceRangeCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PriceRangeCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.lblPriceRange.text = [NSString stringWithFormat:@"₹ %@ - ₹ %@",dictSelectPrice.count==0? _filterDict[@"lowprice"]:dictSelectPrice[@"lowprice"],dictSelectPrice.count==0? _filterDict[@"highprice"]:dictSelectPrice[@"highprice"]];
                
                
                cell.Slider.minValue = [_filterDict[@"lowprice"] integerValue];
                cell.Slider.maxValue = [_filterDict[@"highprice"] integerValue];
                cell.Slider.selectedMinimum =  dictSelectPrice.count==0? [_filterDict[@"lowprice"] integerValue]:[dictSelectPrice[@"lowprice"] integerValue];
                cell.Slider.selectedMaximum = dictSelectPrice.count==0? [_filterDict[@"highprice"] integerValue]:[dictSelectPrice[@"highprice"] integerValue];
                cell.lblMIn.text = [NSString stringWithFormat:@"₹ %@",_filterDict[@"lowprice"]];
                cell.lblmax.text =[NSString stringWithFormat:@"₹ %@",_filterDict[@"highprice"]];
                cell.lblMIn.textColor = cell.lblmax.textColor = cell.lblPriceRange.textColor = MylesTextColor;
                return cell;
                
            }
                break;
                
            case 2:
            {
                static NSString *simpleTableIdentifier = @"CarPropertyCell";
                
                CarPropretyCell *cell = (CarPropretyCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                
                if (cell == nil)
                {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CarPropertyCell" owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.tag = indexPath.section;
                cell.lblTitle.text = [arrCProperty[indexPath.section] capitalizedString];
                
                NSString *myString = [NSString stringWithUTF8String:"\u2713"];
                NSArray *arrCarProperty = [[NSArray alloc] init];
                arrCarProperty = dictCarProperty[cell.lblTitle.text.lowercaseString];
                //for (int i = 0; i< [dictCarProperty[cell.lblTitle.text.lowercaseString] count] ; i++)
                for (int i = 0; i< [arrCarProperty count] ; i++)
                {
                    switch (i) {
                        case 0:
                            cell.lbl1.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            
                            if (dictSelectCarproperty[cell.lblTitle.text] != nil)
                                cell.lbl1.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl1.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox1.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl1.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            ;
                            [cell.btnCheckBox1 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl1.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox1 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox1.layer.borderWidth = 1.5;
                            
                            [cell.btnCheckBox1 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            
                            
                            
                            break;
                            
                        case 1:
                            
                            cell.lbl2.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            cell.lbl2.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl2.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox2.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl2.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            [cell.btnCheckBox2 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl2.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox2 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox2.layer.borderWidth = 1.5;
                            
                            [cell.btnCheckBox2 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            
                            break;
                        case 2:
                            cell.lbl3.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            cell.lbl3.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl3.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox3.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl3.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            [cell.btnCheckBox3 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl3.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox3 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox3.layer.borderWidth = 1.5;
                            [cell.btnCheckBox3 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            
                            break;
                            
                        case 3:
                            cell.lbl4.text = [dictCarProperty[cell.lblTitle.text.lowercaseString][i] capitalizedString];
                            cell.lbl4.textColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl4.text] ? MylesColor:MylesTextColor;
                            
                            cell.btnCheckBox4.layer.borderColor  =  [dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl4.text] ? MylesColor.CGColor:MylesCheckBoxColor;
                            [cell.btnCheckBox4 setTitle:[dictSelectCarproperty[cell.lblTitle.text] containsObject:cell.lbl4.text] ? myString:nil forState:UIControlStateNormal];
                            [cell.btnCheckBox4 setTitleColor:MylesColor forState:UIControlStateNormal];
                            cell.btnCheckBox4.layer.borderWidth = 1.5;
                            [cell.btnCheckBox4 addTarget:self action:@selector(btnCProperty:) forControlEvents:UIControlEventTouchUpInside];
                            break;
                            
                        default:
                            break;
                    }
                }
                
                
                cell.btnCheckBox4.layer.cornerRadius = cell.btnCheckBox3.layer.cornerRadius = cell.btnCheckBox2.layer.cornerRadius = cell.btnCheckBox1.layer.cornerRadius = 2;
                
                
                
                
                
                ;
                
                
                return cell;
                
            }
            default:
                
                break;
                
                
        }
        return nil;
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrCarProperty = [[NSArray alloc] init];
    arrCarProperty = dictCarProperty[[arrCProperty[indexPath.section] lowercaseString]];
    
    if (!_isDoorStepDelievery) {
        return iCollSelection == 1 ? 135 : iCollSelection == 3  ? [arrCarProperty count] > 2 ? 130 : 90 : iCollSelection == 2 ? 40 : 40;
    } else {
        return iCollSelection == 0 ? 135 : iCollSelection == 2  ? [arrCarProperty count] > 2 ? 130 : 90 : iCollSelection == 1 ? 40 : 40;
    }
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *myCell = [tableView cellForRowAtIndexPath:indexPath];
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!_isDoorStepDelievery) {
        return iCollSelection == 0 && !isSearch ? 40 : 0.01;
    } else {
        return 0.01;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *vwHeader = [[UIView alloc]initWithFrame:CGRectZero];

    if (!_isDoorStepDelievery) {
        
        if (iCollSelection == 0) {
            
            if (_scrOnTable==nil )
            {
                
                _scrOnTable = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tblData.frame), 44)];
                
                _scrOnTable.delegate = self;
                
            }
            if (_tblData.tableHeaderView == nil) {
                _tblData.tableHeaderView = _scrOnTable;
            }
            
            UIButton *btnHeader = [[UIButton alloc]initWithFrame:CGRectMake(-1, 0,self.tblData.frame.size.width+2, 41)];
            btnHeader.layer.borderWidth = 1.0;
            btnHeader.layer.borderColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0].CGColor;
            btnHeader.tag = section;
            [btnHeader setBackgroundColor:[UIColor whiteColor]];
            btnHeader.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
            [btnHeader setTitleColor:[UIColor colorWithRed:91.0/255.0 green:91.0/255.0 blue:91.0/255.0 alpha:1] forState:UIControlStateNormal];
            //        [btnHeader setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            btnHeader.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btnHeader addTarget:self action:@selector(headerBtn:) forControlEvents:UIControlEventTouchUpInside];
            if (iCollSelection == 0 && maSelectedHeader.count>0) {
                for (SelectionModel *seModel in maSelectedHeader)
                {
                    if ([seModel.strSection integerValue] == section) {
                        [btnHeader setImage:[UIImage imageNamed:@"less_icn"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [btnHeader setImage:[UIImage imageNamed:@"more_less_icn"] forState:UIControlStateNormal];
                        
                    }
                }
            }
            else
                
                [btnHeader setImage:[UIImage imageNamed:@"more_less_icn"] forState:UIControlStateNormal];
            
            
            btnHeader.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            
            [btnHeader setTitle:[dictSublocation allKeys][section] forState:UIControlStateNormal];
            btnHeader.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
            
            [vwHeader addSubview:btnHeader];
        } else {
            _tblData.tableHeaderView = nil;

        }
        NSLog(@"icollselection");
        return iCollSelection == 0 && !isSearch ? vwHeader :nil;


    } else {
        _tblData.tableHeaderView = nil;
        return iCollSelection == 0 && !isSearch ? vwHeader :nil;
//        return nil;
    }

    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_scrOnTable resignFirstResponder];
}

-(IBAction)btnCPrbtnSelectCarCategoryoperty:(id)sender
{
    SubCatagoriesCell *clickedCell = (SubCatagoriesCell *)[[sender superview] superview];
    
    if (maSelectCarCategory == nil)
    {
        maSelectCarCategory = [[NSMutableArray alloc]init];
    }
    
    if ([maSelectCarCategory containsObject:clickedCell.lblCatagories.text]) {
        clickedCell.lblCatagories.textColor = MylesTextColor;
        [maSelectCarCategory removeObject:clickedCell.lblCatagories.text];
        clickedCell.btnCheckbox.layer.borderColor = MylesCheckBoxColor;
        [clickedCell.btnCheckbox setTitle:nil forState:UIControlStateNormal];
        
        
        
    }
    else{
        [maSelectCarCategory addObject:clickedCell.lblCatagories.text];
        clickedCell.lblCatagories.textColor = MylesColor;
        clickedCell.btnCheckbox.layer.borderColor = MylesColor.CGColor;
        
        NSString *myString = [NSString stringWithUTF8String:"\u2713"];
        [clickedCell.btnCheckbox setTitle:myString forState:UIControlStateNormal];
        [clickedCell.btnCheckbox setTitleColor:MylesColor forState:UIControlStateNormal];
        
        
    }
    [self DotAppear];

    NSLog(@"sender %@",clickedCell.lblCatagories.text);
    
}

-(IBAction)btnCProperty:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    CarPropretyCell *clickedCell = (CarPropretyCell *)[[sender superview] superview];
    UILabel *label = [clickedCell viewWithTag:btn.tag+1];
    NSLog(@"sender %@",label.text);
    
    if (btn.titleLabel.text != nil) {
        
        btn.layer.borderColor = MylesCheckBoxColor;
        btn.titleLabel.text = nil;
        [btn setTitle:nil forState:UIControlStateNormal];
        label.textColor = MylesTextColor;
        
         if (dictSelectCarproperty[clickedCell.lblTitle.text] != nil)
        {
            NSMutableArray *tempArr = [[NSMutableArray alloc]initWithArray:dictSelectCarproperty[clickedCell.lblTitle.text]];
            NSArray *arrNewSelectedCarProperty = [[NSArray alloc] init];
            arrNewSelectedCarProperty = dictSelectCarproperty[clickedCell.lblTitle.text];
            if ([tempArr containsObject:label.text]) {
                [tempArr removeObject:label.text];
                
                [dictSelectCarproperty setValue:tempArr forKey:clickedCell.lblTitle.text];
            }
            //if ([dictSelectCarproperty[clickedCell.lblTitle.text] count] == 0)
            if ([arrNewSelectedCarProperty count] == 0)
                [dictSelectCarproperty removeObjectForKey:clickedCell.lblTitle.text];
            
            
        }
        
    }
    else{
        
        btn.layer.borderColor = MylesColor.CGColor;
        label.textColor = MylesColor;
        //  [btn setImage:[UIImage imageNamed:@"apply_icn_onclick"] forState:UIControlStateNormal];
        NSString *myString = [NSString stringWithUTF8String:"\u2713"];
        [btn setTitle:myString forState:UIControlStateNormal];
        [btn setTitleColor:MylesColor forState:UIControlStateNormal];
      
        if (dictSelectCarproperty == nil) {
            dictSelectCarproperty = [[NSMutableDictionary alloc]init];
            [dictSelectCarproperty setValue:@[label.text] forKey:clickedCell.lblTitle.text];
        }
        else if (dictSelectCarproperty[clickedCell.lblTitle.text] != nil)
        {
            NSMutableArray *tempArr = [[NSMutableArray alloc]initWithArray:dictSelectCarproperty[clickedCell.lblTitle.text]];
            if (![tempArr containsObject:label.text]) {
                [tempArr addObject:label.text];
                
                [dictSelectCarproperty setValue:tempArr forKey:clickedCell.lblTitle.text];
            }
            
            
        }
        else
        {
            [dictSelectCarproperty setValue:@[label.text] forKey:clickedCell.lblTitle.text];
        }
        
    }
    
   
    
    [self DotAppear];

}
-(IBAction)btnSelectItem:(id)sender
{
    SubCatagoriesCell *clickedCell = (SubCatagoriesCell *)[[sender superview] superview];
    clickedCell.lblCatagories.textColor = MylesColor;
    //NSIndexPath *clickedButtonPath = [self.tblData indexPathForCell:clickedCell];
    
    if (maSelectLoc == nil)
        maSelectLoc =[[NSMutableArray alloc]init];
    
    if ([maSelectLoc containsObject:clickedCell.lblCatagories.text]) {
        clickedCell.lblCatagories.textColor = MylesTextColor;
        clickedCell.btnCheckbox.layer.borderColor = MylesCheckBoxColor;
        [maSelectLoc removeObject:clickedCell.lblCatagories.text];
        [clickedCell.btnCheckbox setTitle:nil forState:UIControlStateNormal];
        
        
        
    }
    else{
        [maSelectLoc addObject:clickedCell.lblCatagories.text];
        clickedCell.lblCatagories.textColor = MylesColor;
        clickedCell.btnCheckbox.layer.borderColor = MylesColor.CGColor;
        
        NSString *myString = [NSString stringWithUTF8String:"\u2713"];
        [clickedCell.btnCheckbox setTitle:myString forState:UIControlStateNormal];
        [clickedCell.btnCheckbox setTitleColor:MylesColor forState:UIControlStateNormal];

        
    }
    
    
    [self DotAppear];
    
}
-(void)DotAppear
{
    CollectionViewCell *cell11 = (CollectionViewCell *)[_colVwData cellForItemAtIndexPath:[NSIndexPath indexPathForRow:iCollSelection inSection:0]];

     switch (iCollSelection) {
        case 0:
        {
            if (maSelectLoc.count>0)
            {
                cell11.vwDot.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1];
                cell11.vwDot.hidden = false;
                cell11.lblNoSelected.hidden = false;
                cell11.lblNoSelected.text = [NSString stringWithFormat:@"%lu",(unsigned long)maSelectLoc.count];
                
            }
            else{
                cell11.vwDot.hidden = true;
                cell11.lblNoSelected.text = nil;
                cell11.lblNoSelected.hidden = false;
                
            }
        }
        break;
        case 1:
        {
//            if (maSelectLoc.count>0)
//            {
            if (arrCollectionData.count == 4){
                cell11.vwDot.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1];
                cell11.vwDot.hidden = false;
                cell11.lblNoSelected.hidden = false;
                cell11.lblNoSelected.text = @"1";
            }
            else{
                cell11.vwDot.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1];
                cell11.vwDot.hidden = false;
                cell11.lblNoSelected.hidden = false;
                cell11.lblNoSelected.text = [NSString stringWithFormat:@"%lu",(unsigned long)maSelectCarCategory.count];
            }
        }
            break;
        case 2:
        {
            if (arrCollectionData.count == 4){
                if (maSelectCarCategory.count>0)
                {
                    cell11.vwDot.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1];
                    cell11.vwDot.hidden = false;
                    cell11.lblNoSelected.hidden = false;
                    cell11.lblNoSelected.text = [NSString stringWithFormat:@"%lu",(unsigned long)maSelectCarCategory.count];
                    
                }
                else{
                    cell11.vwDot.hidden = true;
                    cell11.lblNoSelected.text = nil;
                    cell11.lblNoSelected.hidden = false;
                    
                }
            }
            else{
          
            if (dictSelectCarproperty.count>0)
            {
                cell11.vwDot.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1];
                cell11.vwDot.hidden = false;
                cell11.lblNoSelected.hidden = false;
                int iCount= 0;
                for (NSArray *Count in [dictSelectCarproperty allValues])
                {
                    iCount += Count.count;
                }
                cell11.lblNoSelected.text = [NSString stringWithFormat:@"%d",iCount];
                
            }
            else{
                cell11.vwDot.hidden = true;
                cell11.lblNoSelected.text = nil;
                cell11.lblNoSelected.hidden = false;
                
            }
            }
           
        }
            break;
            
        case 3:
        {
            if (dictSelectCarproperty.count>0)
            {
                cell11.vwDot.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1];
                cell11.vwDot.hidden = false;
                cell11.lblNoSelected.hidden = false;
                int iCount= 0;
                for (NSArray *Count in [dictSelectCarproperty allValues])
                {
                    iCount += Count.count;
                }
                cell11.lblNoSelected.text = [NSString stringWithFormat:@"%d",iCount];
                
            }
            else{
                cell11.vwDot.hidden = true;
                cell11.lblNoSelected.text = nil;
                cell11.lblNoSelected.hidden = false;
                
            }
        }
            break;
            
            
        default:{
            cell11.vwDot.hidden = true;
            cell11.lblNoSelected.hidden = true;
        }
            break;
    }
}

-(IBAction)headerBtn:(id)sender
{
    if (maSelectedHeader==nil) {
        maSelectedHeader = [[NSMutableArray alloc]init];
    }
    
    UIButton *btn = (UIButton *)sender;
    
    
    SelectionModel *selModel = [[SelectionModel alloc]init];
    [selModel SetModelData:[NSString stringWithFormat:@"%ld",(long)btn.tag]  :nil :true];
    if (maSelectedHeader.count == 0)
    {
        [maSelectedHeader addObject:selModel];
        
    }
    else
    {
        bool isContain = false;
        id removeObj;
        for (SelectionModel *seModel in maSelectedHeader)
        {
            if ([seModel.strSection integerValue] == btn.tag) {
                isContain = true;
                removeObj = seModel;
                break;
            }
            
            
        }
        if (isContain) {
            [maSelectedHeader removeObject:removeObj];
        }
        else
            [maSelectedHeader addObject:selModel];
        
        
    }
 
    NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:btn.tag];
    [_tblData reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"collectionView numberOfItemsInSection = %lu", (unsigned long)arrCollectionData.count);
    return arrCollectionData.count;
   
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCollection" forIndexPath:indexPath];
    cell.vwBg.tag = indexPath.row;
    cell.tag = indexPath.row;
    cell.vwDot.layer.cornerRadius = cell.vwDot.frame.size.height /2;

    if (!bColorChange || iCollSelection == indexPath.row) {
        
        bColorChange = true;
        cell.vwBg.backgroundColor  = [UIColor whiteColor];
        cell.img.image =[UIImage imageNamed:arrColSelectIcon[indexPath.row]];
        cell.lbl.textColor = [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1];
        
    }
    else
    {
        cell.vwBg.backgroundColor  = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1.0];
        cell.img.image =[UIImage imageNamed:arrColIcon[indexPath.row]];
        cell.lbl.textColor = [UIColor colorWithRed:148.0/255.0 green:148.0/255.0 blue:148.0/255.0 alpha:1.0];
    }
     cell.vwDot.backgroundColor = iCollSelection == indexPath.row ? [UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1] :[UIColor colorWithRed:148.0/255.0 green:148.0/255.0 blue:148.0/255.0 alpha:1.0] ;
    
    if (!_isDoorStepDelievery) {
        switch (indexPath.row) {
            case 0:
            {
                if (maSelectLoc.count>0)
                {
                    
                    
                    cell.vwDot.hidden = false;
                    cell.lblNoSelected.hidden = false;
                    cell.lblNoSelected.text = [NSString stringWithFormat:@"%lu",(unsigned long)maSelectLoc.count];
                    
                }
                else{
                    cell.vwDot.hidden = true;
                    cell.lblNoSelected.text = nil;
                    cell.lblNoSelected.hidden = false;
                    
                }
            }
                
                break;
                
            case 1:
            {
                if (dictSelectPrice.count>0)
                {
                    cell.vwDot.hidden = false;
                    cell.lblNoSelected.hidden = false;
                    cell.lblNoSelected.text = @"1";
                    
                }
                else
                {
                    cell.vwDot.hidden = true;
                    cell.lblNoSelected.hidden = true;
                    cell.lblNoSelected.text =nil;
                    
                }
            }
                
                break;
                
            case 2:
            {
                if (arrCollectionData.count == 4){
                    if (maSelectCarCategory.count>0)
                    {
                        cell.vwDot.hidden = false;
                        cell.lblNoSelected.hidden = false;
                        cell.lblNoSelected.text = [NSString stringWithFormat:@"%lu",(unsigned long)maSelectCarCategory.count];
                        
                    }
                    else{
                        cell.vwDot.hidden = true;
                        cell.lblNoSelected.text = nil;
                        cell.lblNoSelected.hidden = false;
                        
                    }
                }
                else{
               
                
                if (dictSelectCarproperty.count>0)
                {
                    cell.vwDot.hidden = false;
                    cell.lblNoSelected.hidden = false;
                    int iCount= 0;
                    for (NSArray *Count in [dictSelectCarproperty allValues])
                    {
                        iCount += Count.count;
                    }
                    cell.lblNoSelected.text = [NSString stringWithFormat:@"%d",iCount];
                    
                }
                else{
                    cell.vwDot.hidden = true;
                    cell.lblNoSelected.text = nil;
                    cell.lblNoSelected.hidden = false;
                    
                }
                }
            }
                
                break;
                
            case 3:
            {
                if (dictSelectCarproperty.count>0)
                {
                    cell.vwDot.hidden = false;
                    cell.lblNoSelected.hidden = false;
                    int iCount= 0;
                    for (NSArray *Count in [dictSelectCarproperty allValues])
                    {
                        iCount += Count.count;
                    }
                    cell.lblNoSelected.text = [NSString stringWithFormat:@"%d",iCount];
                    
                }
                else{
                    cell.vwDot.hidden = true;
                    cell.lblNoSelected.text = nil;
                    cell.lblNoSelected.hidden = false;
                    
                }
            }
                break;
                
                
                
            default:{
                cell.vwDot.hidden = true;
                cell.lblNoSelected.hidden = true;
            }
                break;
        }
        
        cell.lbl.text = arrCollectionData[indexPath.row];
        return cell;
    } else {
        switch (indexPath.row) {
            case 0:
            {
                if (dictSelectPrice.count>0)
                {
                    cell.vwDot.hidden = false;
                    cell.lblNoSelected.hidden = false;
                    cell.lblNoSelected.text = @"1";
                    
                }
                else
                {
                    cell.vwDot.hidden = true;
                    cell.lblNoSelected.hidden = true;
                    cell.lblNoSelected.text =nil;
                    
                }
            }
                
                break;
                
            case 1:
            {
                if (maSelectCarCategory.count>0)
                {
                    cell.vwDot.hidden = false;
                    cell.lblNoSelected.hidden = false;
                    cell.lblNoSelected.text = [NSString stringWithFormat:@"%lu",(unsigned long)maSelectCarCategory.count];
                    
                }
                else{
                    cell.vwDot.hidden = true;
                    cell.lblNoSelected.text = nil;
                    cell.lblNoSelected.hidden = false;
                    
                }
            }
                
                break;
                
            case 2:
            {
                if (dictSelectCarproperty.count>0)
                {
                    cell.vwDot.hidden = false;
                    cell.lblNoSelected.hidden = false;
                    int iCount= 0;
                    for (NSArray *Count in [dictSelectCarproperty allValues])
                    {
                        iCount += Count.count;
                    }
                    cell.lblNoSelected.text = [NSString stringWithFormat:@"%d",iCount];
                    
                }
                else{
                    cell.vwDot.hidden = true;
                    cell.lblNoSelected.text = nil;
                    cell.lblNoSelected.hidden = false;
                    
                }
            }
                break;
                
                
                
            default:{
                cell.vwDot.hidden = true;
                cell.lblNoSelected.hidden = true;
            }
                break;
        }
        
        cell.lbl.text = arrCollectionData[indexPath.row];
        return cell;
    }
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (iCollSelection != (int)indexPath.row) {
        iCollSelection = (int)indexPath.row;
        [_scrOnTable resignFirstResponder];
        [_colVwData reloadData];
        [self.tblData reloadData];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length==0) {
        isSearch = false;
    }
    else
    {
        isSearch = true;
        strSearchtxt = searchText;
    }
    if (maSearchData == nil) {
        maSearchData = [[NSMutableArray alloc]init];
    }
    else
        [maSearchData removeAllObjects];
    
    
    for (NSDictionary *key in arrSublockeys) {
        

            NSPredicate *resultPredicate = [NSPredicate
                                            predicateWithFormat:@"SELF.strSublocationName contains[cd] %@",
                                            searchText];
 
                   [maSearchData addObjectsFromArray:[[dictSublocation[key]  filteredArrayUsingPredicate:resultPredicate] mutableCopy]];
    }
    [maSearchData sortUsingDescriptors:
     [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"strSublocationName"
                                                            ascending:YES
                                                             selector:@selector(caseInsensitiveCompare:)]]];
    
    NSLog(@"maSearchData %@",maSearchData);
    [_tblData reloadData];
    
    
}

#pragma mark TTRangeSliderViewDelegate
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    
    PriceRangeCell *clickedCell = (PriceRangeCell *)[[sender superview] superview];
    
    clickedCell.lblPriceRange.text = [NSString stringWithFormat:@"₹ %d - ₹ %d",(int)selectedMinimum,(int)selectedMaximum];
    if (dictSelectPrice==nil) {
        dictSelectPrice=[[NSMutableDictionary alloc]init];
    }
    [dictSelectPrice setValue:[NSString stringWithFormat:@"%d",(int)selectedMinimum] forKey:@"lowprice"];
    [dictSelectPrice setValue:[NSString stringWithFormat:@"%d",(int)selectedMaximum] forKey:@"highprice"];
    [self DotAppear];

}

-(IBAction)reset:(id)sender
{
    [dictSelectedFilterItem removeAllObjects];
    [maSelectLoc removeAllObjects];
    [dictSelectPrice removeAllObjects];
    [dictSelectCarproperty removeAllObjects];
    [maSelectCarCategory removeAllObjects];
    [self.delegate ApplyFilter:nil];

    [_tblData reloadData];
    [_colVwData reloadData];
    
}
- (IBAction)popFilter:(id)sender {
//     [self.delegate ApplyFilter:nil];
    [self dismissModalViewControllerAnimated:YES];

}
-(IBAction)btnApply:(id)sender
{
    NSMutableDictionary *mergeDict =[[NSMutableDictionary alloc]init];
    if (dictSelectPrice !=nil)
    [mergeDict addEntriesFromDictionary: dictSelectPrice];
    if (dictSelectCarproperty !=nil)
    [mergeDict addEntriesFromDictionary: dictSelectCarproperty];
    if (dictSelectCarproperty !=nil)
        [mergeDict addEntriesFromDictionary: dictSelectCarproperty];
    if (maSelectCarCategory.count>0)
        [mergeDict setObject:maSelectCarCategory forKey:@"CarCategory"];
    
    if (maSelectLoc.count>0)
        [mergeDict setObject:maSelectLoc forKey:@"location"];
    
    
    
    
        [self.delegate ApplyFilter:mergeDict];
        [self dismissModalViewControllerAnimated:YES];

    
}

@end
