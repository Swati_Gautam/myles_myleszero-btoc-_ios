//
//  NotificationPopView.h
//  Myles
//
//  Created by Kavita Asija on 20/08/15.
//  Copyright (c) 2015 Sanjay Kumar  Yadav. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NotificationPopView : UIView
@property (weak, nonatomic) IBOutlet UILabel *modelName;
@property (weak, nonatomic) IBOutlet UILabel *modelLocation;
@property (weak, nonatomic) IBOutlet UILabel *modelsubLocation;
@property (weak, nonatomic) IBOutlet UIImageView *modelImage;
@property (weak, nonatomic) IBOutlet UILabel *pickUptime;
@property (weak, nonatomic) IBOutlet UILabel *pickUpDate;
@property (weak, nonatomic) IBOutlet UIButton *trackMyRideBtnAction;
@property (weak, nonatomic) IBOutlet UIButton *cancleBtnAction;
- (void)setaction;
@end
