//
//  SubLocationData.swift
//  Myles
//
//  Created by Itteam2 on 18/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import UIKit

@objc class SubLocationData: NSObject {
    
    @objc static let sharedInstance = SubLocationData()
    
    @objc var subLocationDict = [[String:AnyObject]]()//Dictionary<String,AnyObject>()
    
    @objc func setSublocation(_ dictSublocation : [[String:AnyObject]])  {
        subLocationDict = dictSublocation
    }
    
    @objc func getSublocation() -> [[String:AnyObject]] {
        return subLocationDict 
    }

    @objc func deletSublocation() {
        
        subLocationDict.removeAll()
    }
}
