//
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import "MenuViewController.h"
#import "RegistrationViewController.h"
#import "MyRidesVC.h"
#import "RegistrationViewController.h"
#import "HomeViewController.h"
#import "ViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import "Myles-Swift.h"
#import "LoginViewController.h"

@import FirebaseAnalytics;

//static NSInteger indexValue = 0;

@implementation SWUITableViewCell
@end

@implementation MenuViewController
{
    NSArray *titleAr ;
    NSArray *imageAr;
    NSString *strOtherDestination;
    
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // configure the destination view controller:
    if ( [sender isKindOfClass:[UITableViewCell class]] )
    {
        UINavigationController *navController = segue.destinationViewController;
        RegistrationViewController* cvc = [navController childViewControllers].firstObject;
        if ( [cvc isKindOfClass:[RegistrationViewController class]] )
        {
            
            
            // [self ]
        }
    }
}

/*
 Event tracking: Event tracking allows you to track specific interactive elements within the screen. Things like:
 
 Button clicks
 Menu selections
 Mobile ad clicks
 Video plays
 Swipes or other gestures
 To track an event, you must set up tracking in your account and then attach a method call to the particular event you’d like to track.
 
 You can include four parameters within a method:
 
 Category – allows you to organize the events that you track into groups
 Action – they are used to describe the event
 Label – optional strings that further describe the elements you’re tracking
 Value – numerical variable used to collect customized values
 
 */

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // adding SHARING TEXT
    sharingText = @"Drive yourself with Myles, India's fastest growing self-drive brand. Book your ride now! https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579?mt=8";
    
    // Do any additional setup after loading the view.
    _indexValue = 0;
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"MenuView"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MenuView"
                                                          action:@"Click"
                                                           label:@"LogOut"
                                                           value:@1] build]];
    
//    bool isLogin = [DEFAULTS boolForKey:IS_LOGIN];
//    if (isLogin) {
        titleAr = @[k_HomeTitle , k_MyRideTitle ,k_ProfileTitle,@"OFFERS",k_AboutUsTitle,k_SubscribeCar,k_COnatctUsTitle,k_SharingTitle,k_RateTitle,@"FAQ"];
        imageAr = @[[k_HomeTitle lowercaseString] , k_MyRideTitle ,k_ProfileTitle,@"Offers",k_AboutUsTitle,k_SubscribeCar,k_COnatctUsTitle,k_SharingTitle,k_RateTitle,@"faq"];
//    }
//    else
//    {
//        titleAr = @[k_HomeTitle ,@"OFFERS",k_AboutUsTitle,k_COnatctUsTitle,k_SharingTitle,k_RateTitle,@"FAQ"];
//        imageAr = @[[k_HomeTitle lowercaseString] ,@"Offers",k_AboutUsTitle,k_COnatctUsTitle,k_SharingTitle,k_RateTitle,@"faq"];
//    }
   
    
    /*
    titleAr = @[k_HomeTitle , k_MyRideTitle ,k_ProfileTitle,k_WalletTitle,k_AboutUsTitle,k_COnatctUsTitle,k_SharingTitle,k_RateTitle];
    imageAr = @[[k_HomeTitle lowercaseString] , k_MyRideTitle ,k_ProfileTitle,[k_WalletTitle lowercaseString],k_AboutUsTitle,k_COnatctUsTitle,k_SharingTitle,k_RateTitle];
     */
    
    [self.menuTable reloadData];
    self.revealViewController.delegate = self;
    
   }
-(IBAction)LoginEvent:(id)sender
{
    
    [self.revealViewController revealToggleAnimated:YES];
    
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
    
    NSMutableDictionary *maDict =[[NSMutableDictionary alloc]init];
    [maDict setValue:[NSString stringWithFormat:@"%ld",(long)_selectedRow] forKey:@"Selectedindex"];
    if (strOtherDestination != nil)
    {
        [maDict setValue:strOtherDestination forKey:@"otherdestination"];

    }
   
    [[NSNotificationCenter defaultCenter] postNotificationName: @"openLogin" object:nil userInfo:maDict];
    
//    LoginViewController *editController = (LoginViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [.navigationController presentViewController:editController animated:YES completion:nil];
    
    

//    SWRevealViewController *revealController = self.revealViewController;
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
//    UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_registrationNavigation];
////    [(MenuViewController *)revealController.rearViewController setSelectedIndex:2];
////    [(MenuViewController *)revealController.rearViewController setSelectedRow:2];
//    LoginViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//    navController.viewControllers = @[homeController];
//    //NSLog(@"CONRTOL : %@",navController.viewControllers);
//    [revealController pushFrontViewController:navController animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    if (![DEFAULTS boolForKey:IS_LOGIN]) {
        [_btnLogout setTitle:[NSString stringWithFormat:@"SIGN IN%@SIGN UP", @"\u2215"] forState:UIControlStateNormal];
        [_btnLogout removeTarget:self action:@selector(logoutBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
        [_btnLogout addTarget:self action:@selector(LoginEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
       
            [_btnLogout setTitle:@"LOGOUT" forState:UIControlStateNormal];
            [_btnLogout removeTarget:self action:@selector(LoginEvent:) forControlEvents:UIControlEventTouchUpInside];
            [_btnLogout addTarget:self action:@selector(logoutBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
        

    }

    [super viewWillAppear:YES];
    if (self.selectedIndex != 0)
    {
        _indexValue = self.selectedIndex;
        [[self menuTable] reloadData];
        self.selectedIndex = 0;
    }
    else
    {
        [[self menuTable] reloadData];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    
    //NSLog(@"CLASS : %@",self.revealViewController.frontViewController);
    //    NSInteger count = [[(UINavigationController *)self.revealViewController.frontViewController viewControllers] count];
    //    if (count)
    //    {
    //        if ([[[(UINavigationController *)self.revealViewController.frontViewController viewControllers] objectAtIndex:0] isKindOfClass:[HomeViewController class]])
    //        {
    //            [[(HomeViewController *)self.revealViewController.frontViewController SearchReulttableView] setHidden:YES];
    //            [[(HomeViewController *)self.revealViewController.frontViewController tablesuperView] setHidden:YES];
    //        }
    //    }
    //
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [titleAr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = k_SWUITableViewCellIdentifier;
    
    SWUITableViewCell *cell = (SWUITableViewCell *)[tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
    
    if (cell)
    {
        [(UILabel *)[cell viewWithTag:1] setText:[titleAr objectAtIndex:indexPath.row]];
        
        [(UILabel *)[cell viewWithTag:1] setFont:UIFONT_REGULAR_Motor(12)];
        
        [(UIImageView *)[cell viewWithTag:2] setImage:[UIImage imageNamed:[imageAr objectAtIndex:indexPath.row]]];
        
        if (indexPath.row == _indexValue)
        {
            
        }
        [(UIImageView *)[cell viewWithTag:3] setHidden:!(indexPath.row == _indexValue)];
        
    }
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//
    
    //if (_selectedRow != indexPath.row){
    //    _selectedIndexPath = indexPath;
    //_selectedIndexPath = [tableView indexPathForSelectedRow];
    SWRevealViewController *revealController = self.revealViewController;
    
//    bool isLogin = [DEFAULTS boolForKey:IS_LOGIN];log
    //strOtherDestination = nil;

    if (indexPath.row == 1 || indexPath.row == 2)
    {
        if ([DEFAULTS boolForKey:IS_LOGIN])
        {
            _indexValue = indexPath.row;

        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:(indexPath.row == 1)?k_MyRideStoryBoard:k_profileStoryBoard bundle:nil];
        UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:(indexPath.row == 1)?k_RidesNavigation:k_profileNavigation];
        //[revealController pushFrontViewController:navController animated:YES];
        [revealController setFrontViewController:navController];
        [revealController revealToggleAnimated:YES];
        }
        else
        {
           // _selectedRow = indexPath.row;
            strOtherDestination = [NSString stringWithFormat:@"%ld",(long)indexPath.row];

            [self LoginEvent:self];
            strOtherDestination = nil;
        }
    }
    else
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
        UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
        if (indexPath.row == 0)
        {
            _selectedIndex = indexPath.row;

            _indexValue = indexPath.row;

            _selectedRow = indexPath.row;
            HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
            navController.viewControllers = @[homeController];
            [revealController setFrontViewController:navController];
            [revealController revealToggleAnimated:YES];
        
            //[revealController pushFrontViewController:navController animated:YES];
        }
        else if (indexPath.row == 3)
        {
            _selectedIndex = indexPath.row;
            
            _indexValue = indexPath.row;
            
            _selectedRow = indexPath.row;
            
           // _selectedRow = indexPath.row;
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"CouponViewController"];
            [revealController setFrontViewController:navController];
            [revealController revealToggleAnimated:YES];
        }
        else if ( indexPath.row == 5)
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            if ([DEFAULTS valueForKey:k_mylesCenterNumber])
            {
                NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            }
            else
            {
//                AppDelegate *delegate = [AppDelegate getSharedInstance];
//                [delegate getServiceCenterPhone];
                NSString *url = [NSString stringWithFormat:@"tel://08882222222"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

               // [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
            }
        }
        else if (indexPath.row == 4)
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            if ([DEFAULTS valueForKey:kRoadsideSupport])
            {
                NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:kRoadsideSupport]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            }
            else
            {
                NSString *url = [NSString stringWithFormat:@"tel://%@", @"18001035079"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

               // [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
            }
            
        }
        else if (indexPath.row == 6 )
        {
            
            //_selectedRow = indexPath.row;
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:kCancel destructiveButtonTitle:nil otherButtonTitles:@"Share on Facebook",@"Share on Twitter",@"Share on Whatsapp", nil];
            [sheet showInView:self.view];
        }
        else if (indexPath.row == 7 )
        {

           // _selectedRow = indexPath.row;
            [self rateApp];
            
        }
        else if (indexPath.row == 8 )
        {
            _indexValue = indexPath.row;
            _selectedIndex = indexPath.row;

            _selectedRow = indexPath.row;
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"FAQ" bundle:nil];
            UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"FAQ"];
            [revealController setFrontViewController:navController];
            [revealController revealToggleAnimated:YES];
            
        }
        else
        {
            
        }
        
        
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [FIRAnalytics logEventWithName:kFIREventShare parameters:nil];

    
    switch (buttonIndex)
    {
        case k_FB_SHARE:
        {
            [self shareOnFb];
        }
            break;
            
        case k_TWITTER_SHARE:
        {
            [self shareOnTwitter];
        }
            break;
            
        case k_WHATSAPP_SHARE:
        {
            [self shareOnWhatsApp];
        }
            break;
            
        default:
            break;
    }
}
-(void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    
}

#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}

- (void)applicationFinishedRestoringState {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO call whatever function you need to visually restore
}

-(void)revealController:(SWRevealViewController *)revealController animateToPosition:(FrontViewPosition)position
{
    
}

-(void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    
    [self.menuTable reloadData];
    
}

- (void)aboutUsBtnTouched
{
    [self showLoader];
    [CommonApiClass aboutUsApi:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        if (!error)
        {
            if (data != nil)
            {
                    dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    //NSLog(@"RES : %@",responseJson);
                    NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                    if (status == 1)
                    {
                        
                        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
                        
                        //[CommonFunctions callPhone:[responseJson objectForKey:@"contactno"]];
                    }
                    
                });

            } else {
                
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [CommonFunctions alertTitle:@"" withMessage:KAInternet];
            });
            
        }
    }];
    
    
}


//alertView.tag = 1;
//alertew.tag = 2;

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Index =%ld",buttonIndex);
    if (buttonIndex == 0)
    {
        NSLog(@"You have clicked Cancel");
    }
    else if(buttonIndex == 1)
    {
        NSLog(@"You have clicked ok");
        [self userLogoutClickAction];
    }
}


/*
 UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"OK Dailog"
 message:@"This is OK dialog"
 delegate:self
 cancelButtonTitle:@"Cancel"
 otherButtonTitles: nil];
 [alert addButtonWithTitle:@"GOO"];
 [alert show];
 */

- (IBAction)logoutBtnTouched:(id)sender
{
    
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"LogOut"
                                                     message:@"Are you sure you want to logout?"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles: nil];
    
    [alert addButtonWithTitle:@"OK"];
    [alert show];

}


-(void)userLogoutClickAction {
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"LogOut"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self showLoader];
    [CommonApiClass userLogoutApi:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
        NSString *statusStr = [headers objectForKey:@"Status"];
        if (statusStr != nil && [statusStr isEqualToString:@"1"])
        {
            
            if (!error)
            {
                if (data!= nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                        //NSLog(@"RES : %@",responseJson);
                        NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                        if (status == 1)
                        {
                            [DEFAULTS setObject:@"" forKey:K_SessionID];
                            [DEFAULTS setObject:@"" forKey:KUSER_PHONENUMBER];
                            [DEFAULTS setObject:@"" forKey:K_AccessToken];
                            [DEFAULTS setBool:false forKey:IS_LOGIN];
                            //[CommonFunctions resetDefaults];
                            [[UIApplication sharedApplication] cancelAllLocalNotifications];
                            [CommonFunctions logoutcommonFunction];
                            
                            if (_indexValue == 1 || _indexValue ==2) {
                                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                                HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
                                navController.viewControllers = @[homeController];
                                [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:0];
                                _selectedRow = 0;
                                [self.revealViewController pushFrontViewController:navController animated:true];
                                [self.revealViewController revealToggleAnimated:true];
                                _indexValue = 0;


                            }else
                            [self.revealViewController revealToggleAnimated:true];

                            [CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                            
                        }
                    });
                    
                } else {
                    
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [CommonFunctions alertTitle:@"" withMessage:KAInternet];
                });
                
            }
        }
        else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
            NSLog(@"AccessToken Invalid");
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired, Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [CommonFunctions logoutcommonFunction];
                    [self.revealViewController revealToggleAnimated:true];
                }];
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
            });
            
        }
        
        
    }];

}



/*
- (IBAction)logoutBtnTouched:(id)sender
{
 
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"LogOut"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
 
    [GMDCircleLoader setOnView:self.view withTitle:@"Please wait..." animated:YES];
    [CommonApiClass userLogoutApi:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                //NSLog(@"RES : %@",responseJson);
                [GMDCircleLoader hideFromView:self.view animated:YES];
                
                NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                if (status == 1)
                {
                    [[UIApplication sharedApplication] cancelAllLocalNotifications];
                    
                    [self logoutcommonFunction] ;
                }
                
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [CommonFunctions alertTitle:@"" withMessage:KAInternet];
            });
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [GMDCircleLoader hideFromView:self.view animated:YES];
        });
        
    }];
    
    
}
 */

#pragma mark -
#pragma Share Application On Social Sites

-(void)shareOnFb
{
    
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        
        self.accountStore = [[ACAccountStore alloc]init];
        
        ACAccountType *FBaccountType= [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        
        NSString *key = kFBId;
        NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,@[@"email"],ACFacebookPermissionsKey, nil];
        
        [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
         ^(BOOL granted, NSError *e) {
             if (granted)
             {
                 NSArray *accounts = [self.accountStore accountsWithAccountType:FBaccountType];
                 
                 //it will always be the last object with single sign on
                 self.Account = [accounts lastObject];
                 
                 //NSString *strName=[self.Account valueForKeyPath:@"properties.ACUIAccountSimpleDisplayName"];
                 NSString *strEmail =[self.Account valueForKeyPath:@"properties.ACUIDisplayUsername"];
                 if(!strEmail)
                 {
                     strEmail=@"'Restricted'";
                 }
                 
                 //NSLog(@"facebook account =%@",[self.Account valueForKeyPath:@"properties.uid"]);
                 //NSLog(@"facebook account =%@",[self.Account valueForKeyPath:@"properties.ACUIDisplayUsername"]);
                 //NSLog(@"facebook account =%@",[self.Account valueForKeyPath:@"properties.ACUIAccountSimpleDisplayName"]);
                 
                 // create Facebook controller
                 SLComposeViewController *socialController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                 
                 // add initial text
                 [socialController setInitialText:sharingText];
                 
                 // add an image
                 [socialController addImage:nil];
                 
                 // add a URL
                 [socialController addURL:nil];
                 
                 // present controller
                 
                 BOOL fbInstalled = [self schemeAvailable:@"fb://"];
                 
                 if (fbInstalled)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSArray* sharedObjects=@[sharingText];
                         UIActivityViewController * activityViewController=[[UIActivityViewController alloc]initWithActivityItems:sharedObjects applicationActivities:nil];
                         
                         activityViewController.popoverPresentationController.sourceView = self.view;
                         
                         
                         NSArray *excludeActivities = @[
                                                        
                                                        UIActivityTypePostToWeibo,
                                                        UIActivityTypeMessage,
                                                        UIActivityTypeMail,
                                                        UIActivityTypePrint,
                                                        UIActivityTypeCopyToPasteboard,
                                                        UIActivityTypeAssignToContact,
                                                        UIActivityTypeSaveToCameraRoll,
                                                        UIActivityTypeAddToReadingList,
                                                        UIActivityTypePostToFlickr,
                                                        UIActivityTypePostToVimeo,
                                                        UIActivityTypePostToTencentWeibo,
                                                        UIActivityTypeAirDrop,
                                                        ];
                         activityViewController.excludedActivityTypes = excludeActivities;
                         [self presentViewController:activityViewController animated:YES completion:nil];
                     });
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self presentViewController:socialController animated:YES completion:nil];

                     });

                     
                 }
             } else {
                 //Fail gracefully...
                // NSLog(@"error getting permission %@",e);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KSorry message:KAFBLoginInSettingMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                     [alert show];
                     
                     
                     
                 });
                 
                 
                 
             }
         }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KSorry message:KAFBLoginInSettingMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        });
        
    }
}

// Expects the URL of the scheme e.g. "fb://"
- (BOOL)schemeAvailable:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];
    return [application canOpenURL:URL];
}
- (void)shareOnTwitter
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        if ([CommonFunctions reachabiltyCheck])
        {
            SLComposeViewController *tweetSheetOBJ = [SLComposeViewController
                                                      composeViewControllerForServiceType:SLServiceTypeTwitter];
            //@"https://itunes.apple.com/us/app/myles/id1055237998?ls=1&mt=8"
            [tweetSheetOBJ setInitialText:sharingText];
            [tweetSheetOBJ setTitle:@"Myles"];
            [tweetSheetOBJ setCompletionHandler:^(SLComposeViewControllerResult result)
             {
                 
                 switch (result) {
                     case SLComposeViewControllerResultCancelled:
                         //NSLog(@"Post Canceled");
                         [CommonFunctions alertTitle:@"Alert" withMessage:KATwitterSharingCancel ];
                         
                         break;
                     case SLComposeViewControllerResultDone:
                         [CommonFunctions alertTitle:@"Success" withMessage:@"You have successfully posted on Twitter"];
                        break;
                         
                     default:
                         break;
                 }
             }];
            
            [self presentViewController:tweetSheetOBJ animated:YES completion:^{
                
            }];
            
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:KAInternet delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            });        }
    }
    else
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KSorry message:KATwitterLoginInSettingMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        });
    }
    
}

- (void)shareOnWhatsApp
{
    NSString * urlWhats = sharingText;
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    urlWhats = [urlWhats stringByReplacingOccurrencesOfString:@"." withString:@"%2E"];
    
    NSString * final = [NSString stringWithFormat:@"whatsapp://send?text=%@",urlWhats];
    NSURL * whatsappURL = [NSURL URLWithString:final];
    
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}

#pragma mark - ABPeoplePickerNavigationController Delegate

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    NSString* name = (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    //NSLog(@"Name %@", name);
    return NO;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    return NO;
}
// Called after a person has been selected by the user.
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person
{
    NSInteger recordId = (int)ABRecordGetRecordID(person);
    
    //NSLog(@"%ld",recordId);
    
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?abid=%ld&text=%@",recordId,sharingText];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

// Called after a property has been selected by the user.
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    
}

-(void)rateApp
{
    //https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579?mt=8
    //https://itunes.apple.com/us/app/myles/id1061852579?ls=1&mt=8
    
    
    /*
     Old URL code
     static NSString *const iOS7AppStoreURLFormat = @"https://itms-apps://itunes.apple.com/app/id%d";
     */
    static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%d";
    static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d";
    NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, APP_STORE_ID]];
    [[UIApplication sharedApplication] openURL:url];
}



@end
