//
//  MenuViewController.h
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>


typedef enum kSharetype
{
    k_FB_SHARE = 0,
    k_TWITTER_SHARE,
    k_WHATSAPP_SHARE,
    k_RATE_APP
}kSharetype;

@interface SWUITableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel *label;
@property (nonatomic) IBOutlet UIImageView *icon;
@end

@interface MenuViewController : BaseViewController <UITableViewDelegate , UITableViewDataSource , SWRevealViewControllerDelegate, UIActionSheetDelegate ,ABPeoplePickerNavigationControllerDelegate , UINavigationControllerDelegate>
{
    NSString  *afdsc;
    char value;
    
    

    kSharetype shareType;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (strong, nonatomic) ABPeoplePickerNavigationController *peoplePicker;

@property (strong,  nonatomic ) ACAccount *Account;
@property (strong, nonatomic) ACAccountType *AccountType;
@property (strong, nonatomic) ACAccountStore *accountStore;

@property (nonatomic , assign)  NSInteger selectedIndex;
@property (nonatomic , assign) NSInteger selectedRow;
@property (nonatomic , strong) IBOutlet UITableView *menuTable;
@property (nonatomic , assign) NSInteger indexValue;
- (IBAction)logoutBtnTouched:(id)sender;


@end
