//
//  ContactUs.h
//  Myles
//
//  Created by Myles   on 16/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ContactUsVC : BaseViewController <WKNavigationDelegate> // <UIWebViewDelegate>
{
    
}

@property (weak, nonatomic) IBOutlet WKWebView *wkWebViewContactUs;

@property (nonatomic , strong) IBOutlet UILabel *versionLbl;

@end
