//
//  FeedbackVC.m
//  Myles
//
//  Created by Myles   on 06/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import "FeedbackVC.h"
#import <Google/Analytics.h>


@interface FeedbackVC ()
{
    EDStarRating *starRating;
    EDStarRatingDisplayMode displayMode;
    NSString *ratingPoint;
    UIView *snapshot;
    BOOL firstTym;
    
}
@end

@implementation FeedbackVC

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Feedback"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    //lCurrentWidth = DEVICE_SIZE.width;
    //lCurrentHeight = DEVICE_SIZE.height;
    
    
    starRating = [[EDStarRating alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.starView.frame), CGRectGetHeight(self.starView.frame))];
    [self viewSetUp];
    ratingPoint = @"";
    firstTym = true;
    [self.parentViewController.view removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.starView addSubview:starRating];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewSetUp
{
    
    starRating.backgroundColor  = [UIColor clearColor];
    starRating.starImage = UIImageName(k_star) ;
    starRating.starHighlightedImage = UIImageName(k_starhighlighted);
    starRating.maxRating = 6.0;
    starRating.delegate = self;
    starRating.horizontalMargin = 5 ;
    starRating.editable=YES;
    starRating.rating= 0;
    starRating.displayMode=EDStarRatingDisplayHalf;
    [starRating  setNeedsDisplay];
    // This will use a return block to handle the rating changes
    // Setting the return block automatically nils the delegate
    starRating.returnBlock = ^(float rating)
    {
        ratingPoint =  [NSString stringWithFormat:@"%.1f",rating];
        //NSLog(@"Star Rating changed to %.1f" ,rating);
    };
    self.fedbackBtn.layer.cornerRadius = 2;
    self.fedbackBtn.layer.shadowOffset = CGSizeMake(1.3, 3);
    self.fedbackBtn.layer.shadowOpacity = 0.2;
    self.feedbackTxtFd.layer.cornerRadius = 2;
    self.feedbackTxtFd.delegate = self;
    
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [self animationView:self.superview];
    });
}
#pragma mark - Animation


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark- UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    textView.text = @"";
    [self animateViewMoving:true Value:210];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length <= 0)
    {
        textView.text = @"Your Comments here";
    }
    [self animateViewMoving:false Value:210];
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}
#pragma mark- Submit Button
- (IBAction)submitFeedbackAction:(UIButton *)sender
{
    NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
    if (ratingPoint.floatValue == 0.0)
    {
        [CommonFunctions alertTitle:KMessage withMessage:@"Please give rating to continue"];
        return;
    }
    
    [self sendUserFeedback:userid Rate:ratingPoint Remark:self.feedbackTxtFd.text BookingId:self.bookingID ];
    
}
#pragma mark -Cancel Feedback Api

- (IBAction)cancelFeedbackAction:(UIButton *)sender {
    
    [self.view removeFromSuperview];
}


#pragma mark -Send Feedback Api


- (void) sendUserFeedback : (NSString *)userid Rate : (NSString*)rating Remark : (NSString*)remark BookingId : (NSString *)bookingid
{
    [self.feedbackTxtFd resignFirstResponder];
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject: userid forKey:@"userid"];
        [body setObject: rating forKey:KRating];
        [body setObject: remark forKey:KRemarks];
        [body setObject: bookingid forKey:KBookingid];
        [body setObject:@"1" forKey:@"status"];
        [self showLoader];
        //NSLog(@"body : %@",body);
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KcustomerRating completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD dismiss];
                                                 });
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     if (!error)
                                                     {
                                                         NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                         NSLog(@"RES : %@",responseJson);
                                                         
                                                         dispatch_async(dispatch_get_main_queue(), ^{

                                                             NSInteger status = [[responseJson valueForKey:@"status"]  integerValue];
                                                             NSString *message = [responseJson valueForKey:@"message"];
                                                             if (status == 1 ) {
                                                                 [DEFAULTS removeObjectForKey:k_feedbackbookingid];
                                                                 [self.view removeFromSuperview];
                                                                 [self removeFromParentViewController];
                                                                 UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:KMessage message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                                 [alert show];
                                                                 
                                                             }
                                                             
                                                             
                                                         });
                                                     }
                                                     else
                                                     {
                                                         dispatch_async(dispatch_get_main_queue(), ^{

                                                             [CommonFunctions alertTitle:@"" withMessage:KAInternet];
                                                         });
                                                         
                                                     }
                                                     
                                                 } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired,  Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                         
                                                         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                             [CommonFunctions logoutcommonFunction];
                                                             //[self.revealViewController revealToggleAnimated:true];

                                                             
                                                         }];
                                                         [alertController addAction:ok];
                                                         
                                                         [self presentViewController:alertController animated:YES completion:nil];
                                                     });

                                                 } else
                                                 {
                                                     
                                                 }
                                                 
                                                 
                                             }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
    
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
@end
