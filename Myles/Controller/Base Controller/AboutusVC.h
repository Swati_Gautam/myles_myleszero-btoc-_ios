#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface AboutusVC : BaseViewController <WKNavigationDelegate> //<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet WKWebView *wkWebVieAboutUs;

@end
