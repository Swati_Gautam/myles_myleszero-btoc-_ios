//
//  ContactUs.m
//  Myles
//
//  Created by Myles   on 16/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "ContactUsVC.h"

@interface ContactUsVC ()
//@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ContactUsVC

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self customizeLeftRightNavigationBar:k_ContactUsVcTitle RightTItle:k_phonecall Controller:self Present:true];
    
    [self customizeLeftRightNavigationBar:k_ContactUsVcTitle RightTItle:k_phonecallRed Controller:self Present:true];
    
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    [self viewSetUp];
    
}
-(void)viewSetUp
{
  _wkWebViewContactUs.frame = CGRectMake(self.view.frame.origin.x + 25,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
  [self.view addSubview:_wkWebViewContactUs];
    
    [self.versionLbl setText:[NSString stringWithFormat:@"Version :%@",[[UIDevice currentDevice] systemVersion]]];
    
    self.wkWebViewContactUs.navigationDelegate = self;
    NSURL *url=[NSURL URLWithString:ConatctUsURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.wkWebViewContactUs loadRequest:request];
}
#pragma mark- Right Button
- (void)rightButton : (UIButton *)sender
{
    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}
#pragma mark- WebViewDelegate
/*- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showLoader];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}*/

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showLoader];
    });
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
