//
//  FeedbackVC.h
//  Myles
//
//  Created by Myles   on 06/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EDStarRating/EDStarRating.h>

@interface FeedbackVC : BaseViewController < EDStarRatingProtocol, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *starView;

@property (weak, nonatomic) IBOutlet UITextView *feedbackTxtFd;
- (IBAction)submitFeedbackAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *fedbackBtn;
@property (weak, nonatomic) IBOutlet UIView *superview;
@property (strong, nonatomic) NSString *bookingID;

@end
