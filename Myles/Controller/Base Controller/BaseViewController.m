
#import "BaseViewController.h"

@interface BaseViewController ()

//@property (nonatomic)  UIBarButtonItem* revealButtonItem;
{
    UIView *snapshot;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* --------------------------------------------------------------------------------
 LEFT BUTTON
 -------------------------------------------------------------------------------- */
- (void)customizeBackButton  : (BOOL)presentState
{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftBtnTouchedPop)];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.leftbackButton = [self barButton:CGRectMake(20, 25, 20, 15) Image:k_backBtnImg Present:presentState];
    [self.leftbackButton addTarget:self action:@selector(leftBtnTouchedPop) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.leftbackButton];
    [view addGestureRecognizer:gesture];
    [self.view addSubview:view];
    self.navigationController.navigationBarHidden = YES;
}
-(void)customizeNavigationBar : (NSString *)titleController Present : (BOOL)presentState
{
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 220, 20)];
    [title setFont:UIFONT_REGULAR_Motor(13)];
    [title setText:titleController];
    [title setTextAlignment:NSTextAlignmentLeft];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = title;
    UIButton *btn = [self barButton:CGRectMake(0, 2, 20, 15) Image:k_backBtnImg Present:presentState];
    [btn addTarget:self action:@selector(leftBtnTouchedPop) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem =  rightBtn;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBarHidden = false;
    
    
    
    
}
// Confrim View COntroller Screen Special Navigation Bar
-(void)customizeNavigationBarwithImage : (NSString *)titleController LeftImage : (NSString *)leftButton RightImage :(NSString *)rightButton Caller : (NSString *)callerButton  Controller :(UIViewController*)controller  Present : (BOOL)presentState
{
    
    UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 180, 20)];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftBtnTouchedPop)];
    if ([leftButton isEqualToString:k_backBtnImg])
    {
        self.leftbackButton = [self barButton:CGRectMake(0, 0, 20, 15) Image:k_backBtnImg Present:presentState];
        [self.leftbackButton addTarget:self action:@selector(leftBtnTouchedPop) forControlEvents:UIControlEventTouchUpInside];
        [leftView addSubview:self.leftbackButton];
        [leftView setUserInteractionEnabled:true];
        [leftView addGestureRecognizer:gesture];
    }
    else
    {
        UIImageView *imageView = [[UIImageView alloc]init ];//WithImage:UIImageName(leftButton)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.userInteractionEnabled = true;
        imageView.frame = CGRectMake(0, 0, 20, 18);
        [imageView addGestureRecognizer:gesture];
        if (([titleController isEqualToString:k_BookingTitleCancel]) || ([titleController isEqualToString:k_BookingTitleClose]))
        {
            imageView.image =   [UIImage imageWithCGImage:UIImageName(leftButton).CGImage
                                                    scale:UIImageName(leftButton).scale
                                              orientation:UIImageOrientationDownMirrored];
        }
        else
        {
            imageView.image = UIImageName(leftButton);
        }
        [leftView setUserInteractionEnabled:true];
        [leftView addGestureRecognizer:gesture];
        [leftView addSubview:imageView];
    }
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithCustomView:leftView];
    self.navigationItem.leftBarButtonItem =  nil;
    self.navigationItem.leftBarButtonItem =  rightBtn;
    //self.navigationController.navigationBar.barTintColor = UICOLOR_ORANGE;
    
    // Left Button
    UIView *RightView = [[UIView alloc]initWithFrame:CGRectMake(-20, 0, 100, 20)];
    
    //Right Button
    if (rightButton.length > 0)
    {
        UIButton * deleteButton = [self barButton:CGRectMake(0, 0, 50, 20)Image:rightButton Present:presentState];
        //        [deleteButton setImageEdgeInsets:UIEdgeInsetsMake(10, 0, 10, 0)];
        
        [deleteButton addTarget:controller action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
        [RightView addSubview:deleteButton];
    }
    UIButton *callButton  = [self barButton:CGRectMake(CGRectGetWidth(RightView.frame) - 40, 0, 50, 40)Image:callerButton Present:presentState];
    [callButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 20, 0)];
    
    [callButton addTarget:controller action:@selector(callerButton:) forControlEvents:UIControlEventTouchUpInside];
    [RightView addSubview:callButton];
    UIBarButtonItem *rightbarBtn = [[UIBarButtonItem alloc]initWithCustomView:RightView];
    self.navigationItem.rightBarButtonItem = rightbarBtn;
    
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(30, 5, 130, 20)];
    [title setFont:UIFONT_REGULAR_Motor(13)];
    [title setText:titleController];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setTextColor:[UIColor whiteColor]];
    [title sizeToFit];
    [leftView addSubview:title];
    self.navigationItem.titleView = nil;
    
}
-(void)customizeLeftRightNavigationBar : (NSString *) leftTitle RightTItle : (NSString *)rightTitle Controller :(UIViewController*)controller Present : (BOOL)menuicon
{
    @try {
        //Controller Title
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(30, 3, 90, 20)];
        if ([leftTitle isEqualToString:@"Pick Up / Drop Off Address"])
        {
            
            CGRect rect = title.frame;
            rect.origin.y = -5;
            title.frame = rect;
            [title setFont:UIFONT_BOLD_RobotoFONT(13)];
        }
        else [title setFont:UIFONT_REGULAR_Motor(13)];
        [title setText:leftTitle];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setTextColor:[UIColor whiteColor]];
        [title sizeToFit];
        // View gesture
        UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 20)];
        [leftView addSubview:title];
        if (menuicon)
        {
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self.revealViewController action:@selector(revealToggle:)];
            [leftView addGestureRecognizer:gesture];
        }
        else
        {
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftBtnTouchedPop)];
            [leftView addGestureRecognizer:gesture];
            
        }
        //left Menu Icon
        self.leftmenuButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 15)];
        [self.leftmenuButton setBackgroundImage: UIImageName(k_menuImg) forState:UIControlStateNormal];
        [self.leftmenuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [leftView addSubview:self.leftmenuButton];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
        // LeftBAck Button
        self.leftbackButton = [self barButton:CGRectMake(0, 0, 20, 15) Image:k_backBtnImg Present:menuicon];
        [self.leftbackButton addTarget:self action:@selector(leftBtnTouchedPop) forControlEvents:UIControlEventTouchUpInside];
        
        self.leftbackButton.hidden = true;
        [leftView addSubview:self.leftbackButton];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:leftView];
        self.navigationItem.leftBarButtonItem =  leftBtn;
        if (rightTitle != nil)
        {
            self.RightbarButton = [self barButton:CGRectMake(0, 20, 50, 20)Image:rightTitle Present:menuicon];
            [self.RightbarButton setTitle:rightTitle forState:UIControlStateNormal];
            [self.RightbarButton addTarget:controller action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *rightbarBtn = [[UIBarButtonItem alloc]initWithCustomView:self.RightbarButton];
            self.navigationItem.rightBarButtonItem = rightbarBtn;
        }
        
        self.navigationController.navigationBarHidden = false;
        self.navigationController.navigationBar.hidden = NO;
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];

    } @catch (NSException *exception) {
        NSLog(@"Exception = %@",exception);
    }
}

-(UIButton *)barButton : (CGRect)frame Image : (NSString*)imageName Present : (BOOL)presentState
{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    btn.contentMode=UIViewContentModeCenter;
    btn.backgroundColor = [UIColor clearColor];
    [btn setImage:UIImageName(imageName) forState:UIControlStateNormal];
    btn.titleLabel.font = UIFONT_REGULAR_Motor(13);
    btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    return btn;
}

/* --------------------------------------------------------------------------------
 LEFT BUTTON ACTION
 -------------------------------------------------------------------------------- */

-(void)leftBtnTouchedPresent
{
    [self dismissViewControllerAnimated:true completion:nil];
}
-(void)leftBtnTouchedPop
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)rightButton : (UIButton *)sender
{
    
}
-(void)callerButton : (UIButton *)sender
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark- animation


-(void) animateViewMoving :(BOOL)up Value: (double)moveValue
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.view.frame;
        if (up)
        {
            CGRect rect = self.view.frame;
            self.view.frame = CGRectMake(CGRectGetMinX(rect), CGRectGetMinY(rect) - moveValue, CGRectGetWidth(rect), CGRectGetHeight(rect));
        }
        else
        {
            self.view.frame = CGRectMake(CGRectGetMinX(rect), CGRectGetMinY(rect) + moveValue, CGRectGetWidth(rect), CGRectGetHeight(rect));
        }
    }];
    
}
//POPUp nanimation
- (void )animationView : (UIView *)superview
{
    if ([CommonFunctions getMajorSystemVersion] >= 8)
    {
        snapshot = [self.view snapshotViewAfterScreenUpdates:false];
        snapshot.frame = CGRectMake(CGRectGetMidX(self.view.frame), CGRectGetMidY(self.view.frame), 1, 1);
        [self.view addSubview:snapshot];
        superview.hidden = true;
        [UIView animateKeyframesWithDuration:1.0 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
            [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
                snapshot.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            }];
            
        } completion:^(BOOL finished) {
            [snapshot removeFromSuperview];
            superview.hidden = false;
            
        }];
        
        
    }
    else
    {
        
    }
}

- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"dd'-'MM'-'yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

- (void)logoutcommonFunction
{
    [DEFAULTS setObject:@"" forKey:K_SessionID];
    [DEFAULTS setObject:@"" forKey:KUSER_PHONENUMBER];
    [DEFAULTS setObject:@"" forKey:K_AccessToken];
    [DEFAULTS setBool:NO forKey:IS_LOGIN];
    
    [DEFAULTS synchronize];
    [CommonFunctions logoutcommonFunction];
    [self.revealViewController revealToggleAnimated:true];
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
//    UINavigationController *registrationNavigation = [storyBoard instantiateViewControllerWithIdentifier:k_registrationNavigation];
//    [AppDelegate getSharedInstance].window.rootViewController = registrationNavigation;
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
