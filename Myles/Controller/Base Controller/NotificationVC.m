//
//  NotificationVC.m
//  Myles
//
//  Created by Myles   on 21/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "NotificationVC.h"
#import "Myles-Swift.h"
#import "PickUpPackageDetail.h"

@interface NotificationVC ()

{
    UIView *snapshot;
}
@end

@implementation NotificationVC

{
    CLLocation *tracklocation;
    NSString *address;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    self.subView.backgroundColor = [UIColor whiteColor];
    self.subView.alpha=1.0;
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [self animationView];
    });
}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}


#pragma mark- Animation
- (void )animationView
{
    [self animationView:self.view];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)getBookingInfomation : (NSString *)bookingid
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:bookingid forKey:KBookingid];
        NSURLSession *session = [CommonFunctions defaultSession];
        [self showLoader];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KbookingHistory completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                     // time-consuming task
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [SVProgressHUD dismiss];
                                                     });
                                                 });
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     if (!error)
                                                     {
                                                         NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             NSString *status = [responseJson valueForKey:@"status"];
                                                             
                                                             if ([status integerValue] == 1)
                                                             {
                                                                 PickUpPackageDetail *pickupresponse = [[PickUpPackageDetail alloc]initWithDictionary:responseJson error:nil];
                                                                 tracklocation = [[CLLocation alloc]initWithLatitude:[[pickupresponse Lat] doubleValue] longitude:[[pickupresponse Lon] doubleValue]];
                                                                 address = (pickupresponse.sublocationAdd.length > 0) ? pickupresponse.sublocationAdd : pickupresponse.sublocationName;
                                                                 
                                                                 [self setValuesInComponents:pickupresponse.CarModelName LocationName:pickupresponse.sublocationName SubLocationName:pickupresponse.sublocationAdd ModelImage:pickupresponse.urlFullimage PickUpdate:pickupresponse.PickupDate PickUpTime:pickupresponse.PickupTime];
                                                                 
                                                             }
                                                             else
                                                             {
                                                                 [self removeFromParentViewController];
                                                             }

                                                         });
                                                     }
                                                     else
                                                     {
                                                         dispatch_async(dispatch_get_main_queue(), ^{

                                                             [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                         });
                                                         
                                                     }
                                                     

                                                 }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
//                                                     dispatch_async(dispatch_get_main_queue(), ^{
//                                                         [GMDCircleLoader hideFromView:self.view animated:YES];
//                                                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
//                                                         
//                                                         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                                                             [CommonFunctions logoutcommonFunction];
//                                                             
//                                                         }];
//                                                         [alertController addAction:ok];
//                                                         
//                                                         [self presentViewController:alertController animated:YES completion:nil];
//                                                     });


                                                 } else
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                     });
                                                 }
                                                 
                                                 
        }];
        [globalOperation addOperation:registrationOperation];
        
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
    
}

- (void)setValuesInComponents : (NSString *)modelName LocationName  : (NSString *)locationN SubLocationName : (NSString *)sublocationN  ModelImage : (NSString *)imageUrl PickUpdate : (NSString *)pickupdate PickUpTime : (NSString *)pickuptime
{
    self.modelName.text = modelName;
    self.modelLocation.text = locationN;
    self.modelsubLocation.text = sublocationN;
    NSDate *date = [CommonFunctions dateFromString:[NSString stringWithFormat:@"%@ %@",pickupdate, pickuptime]];
    NSDateFormatter *formet = [[NSDateFormatter alloc]init];
    formet.dateFormat = @"hh:mm a";
    self.pickUptime.text = [formet stringFromDate:date];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        /* Fetch the image from the server... */
        NSURL *url=[NSURL URLWithString:imageUrl];
        NSData *data1 = [NSData dataWithContentsOfURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data1)
            {
                self.modelImage.image = [[UIImage alloc] initWithData:data1];
            }
            else
            {
                self.modelImage.image = UIImageName(k_urlFullImage);
            }
        });
    });
}

- (void) addTarget
{
    [self.trackMyRideBtn addTarget:self action:@selector(trackMyRideBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)trackMyRideBtnAction:(UIButton *)sender
{
    
    /*
    UIStoryboard *storyboard = k_storyBoard(k_MyRideStoryBoard);
    // Send Track Lat long and Booking id
    TrackRide *trackVC = [storyboard instantiateViewControllerWithIdentifier:k_TrackMyRideViewController ];
    [trackVC setSourceAndDestinationLocation:tracklocation address:address Id:self.bookingID];
    [self.parentViewController.navigationController pushViewController:trackVC animated:true];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
     */
}

- (IBAction)cancleBtnAction:(UIButton *)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
@end
