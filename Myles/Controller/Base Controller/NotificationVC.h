//
//  NotificationVC.h
//  Myles
//
//  Created by Myles   on 21/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

@interface NotificationVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *superView;
@property (weak, nonatomic) IBOutlet UIView *subView;

@property (weak, nonatomic) IBOutlet UILabel *modelName;
@property (weak, nonatomic) IBOutlet UILabel *modelLocation;
@property (weak, nonatomic) IBOutlet UILabel *modelsubLocation;
@property (weak, nonatomic) IBOutlet UIImageView *modelImage;
@property (weak, nonatomic) IBOutlet UILabel *pickUptime;
@property (weak, nonatomic) IBOutlet UILabel *pickUpDate;

- (IBAction)trackMyRideBtnAction:(UIButton *)sender;
- (IBAction)cancleBtnAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *trackMyRideBtn;
- (void) addTarget ;
@property (strong, nonatomic) NSString *bookingID;
@end
