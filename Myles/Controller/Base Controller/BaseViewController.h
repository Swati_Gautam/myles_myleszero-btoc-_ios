
#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

-(void)customizeNavigationBar : (NSString *)titleController Present : (BOOL)presentState
;
-(void)customizeNavigationBarwithImage : (NSString *)titleController LeftImage : (NSString *)leftButton RightImage :(NSString *)rightButton Caller : (NSString *)callerButton  Controller :(UIViewController*)controller  Present : (BOOL)presentState;
- (void)customizeBackButton  : (BOOL)presentState;
-(void)customizeLeftRightNavigationBar : (NSString *) leftTitle RightTItle : (NSString *)rightTitle Controller :(UIViewController*)controller Present : (BOOL)menuicon
 ;
@property (strong, nonatomic) UIButton *leftbackButton;
@property (strong, nonatomic) UIButton *leftmenuButton;
@property (strong, nonatomic) UIButton *RightbarButton;

-(void)leftBtnTouchedPop;
//Move View animation
-(void) animateViewMoving :(BOOL)up Value: (double)moveValue;
//PopUp  animation
- (void )animationView : (UIView *)superview;
- (NSString *)formatDate:(NSDate *)date;
- (void)logoutcommonFunction;


@end
