//
//  NoInternetConnViewController.m
//  Myles
//
//  Created by IT Team on 17/02/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "NoInternetConnViewController.h"
#import "CommonFunctions.h"


@interface NoInternetConnViewController ()

@end

@implementation NoInternetConnViewController
NSTimer *timer;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.callButton.layer.masksToBounds = YES;
    self.callButton.layer.cornerRadius = 5.0;
    
    if (_messageText.length > 0)
    {
        self.noConnectionLabel.text = self.messageText;
    }
    else {
        self.noConnectionLabel.text = @"No Internet Connection";
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationEnteredForeground:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(appplicationIsActive:)
//                                                 name:UIApplicationDidBecomeActiveNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(applicationEnteredForeground:)
//                                                 name:UIApplicationWillEnterForegroundNotification
//                                               object:nil];
    _callButton.layer.borderColor =[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0].CGColor;
    _callButton.layer.borderWidth = 1.0;
    
    _callButton.layer.cornerRadius= 5.0;
    
//    2706
    
    [_callButton setBackgroundColor:[UIColor lightGrayColor]];
//    [_callButton addTarget:self action:@selector(changeButtonBackGroundColor:) forControlEvents:UIControlEventTouchDown];
//    
//    [_callButton addTarget:self action:@selector(changeButtonBackGroundColor:) forControlEvents:UIControlEventTouchUpOutside];
    
    [_callButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    


    
//    NSMutableAttributedString *titleText = [[NSMutableAttributedString alloc] initWithString:@"\u2706 Call to book your myles rides"];
//    
//    NSRange range = [titleText.string rangeOfString:@"Call to book your myles rides"];
//    NSRange range1 = [titleText.string rangeOfString:@"\u2706"];
//
//  //  This should be bold,\n and this should not.
//    // Set the font to bold from the beginning of the string to the ","
//    [titleText addAttributes:[NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:20] forKey:NSFontAttributeName] range:range1];
//    
//    // Normal font for the rest of the text
//    [titleText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14] forKey:NSFontAttributeName] range:range];
//    
//    // Set the attributed string as the buttons' title text
//    [self.callButton setAttributedTitle:titleText forState:UIControlStateNormal];
   //  [_callButton setTitle:[NSString stringWithFormat:@"%@ Call to book your myles rides", @"\u2706"] forState:UIControlStateNormal];

    
    
    
}
-(void)changeButtonBackGroundColor:(UIButton*) sender{
    if ([sender.backgroundColor isEqual:[UIColor whiteColor]]){
        [sender setBackgroundColor:[UIColor lightGrayColor]];
    }else{
        [sender setBackgroundColor:[UIColor whiteColor]];
    }}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)appplicationIsActive:(NSNotification *)notification {
//    NSLog(@"Application Did Become Active");
//    if ([CommonFunctions reachabiltyCheck])
//    {
//        NSLog(@"Network reachable");
//       // [self dismissViewControllerAnimated:NO completion:nil];
//    }
//    else
//    {
//        NSLog(@"Not Reachable");
//    }
//
//}


- (void)applicationEnteredForeground:(NSNotification *)notification {
    if ([CommonFunctions reachabiltyCheck])
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else{
        timer = [NSTimer scheduledTimerWithTimeInterval: 0.5
                                                 target: self
                                               selector: @selector(handleTimer:)
                                               userInfo: nil
                                                repeats: YES];
    }
    
    
}
-(void)handleTimer:(NSTimer*)timer{
    if ([CommonFunctions reachabiltyCheck])
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (IBAction)callAction:(UIButton *)sender {
    
    NSLog(@"customer care number = %@", [DEFAULTS valueForKey:k_mylesCenterNumber]);

    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        NSString *url = [NSString stringWithFormat:@"tel://08882222222"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

        //[CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
    
}

-(void)viewWillAppear:(BOOL)animated {
    
}
-(void) viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(timer != NULL){
        [timer invalidate ];

    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
