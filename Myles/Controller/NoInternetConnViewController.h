//
//  NoInternetConnViewController.h
//  Myles
//
//  Created by IT Team on 17/02/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoInternetConnViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *noConnectionLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (nonatomic , strong) NSString *messageText;
@end
