//
//  CouponCVCell.swift
//  Myles
//
//  Created by Akanksha Singh on 10/04/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import UIKit

class CouponCVCell: UICollectionViewCell {
    
    @IBOutlet weak var titleConstraint: NSLayoutConstraint!
    @IBOutlet weak var _promoCode: UILabel!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var validUpto: UILabel!
    @IBOutlet weak var arrayImage: UIImageView!
    
    @IBOutlet weak var termsConditionLbl: UILabel!
    @IBOutlet weak var ViewTC: UIView!
    @IBOutlet weak var _termsBtn: UIButton!
    
    @IBOutlet weak var ConstantHLbl: NSLayoutConstraint!
    @IBOutlet weak var _applyBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //title.sizeToFit()
        
    }
    
    
    
}
