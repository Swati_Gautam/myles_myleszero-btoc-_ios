//
//  CouponViewController.swift
//  Myles
//
//  Created by Akanksha Singh on 02/04/18.
//  Copyright © 2018 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD
import FirebaseAnalytics


@objc protocol SendCouponDataToBookingScreen {
    func selectedCity(cityM:String , updatedCityList:String)
}

postfix operator %
postfix func % (percentage: Int) -> Float {
    return (Float(percentage) / 100)
}

class CouponViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate{
    
    @IBOutlet weak var noOfferView: UIView!
    @IBOutlet weak var _collectionView: UICollectionView!
    @IBOutlet weak var _CouponTextField: CustomTextField!
    @objc var couponDict: NSMutableDictionary!
    @objc weak var delegate:SendCouponDataToBookingScreen?
    var dictionary = [String:Any]()
    
    @IBOutlet weak var _errorCoupon: UILabel!
    var ConditionHeight: Int!
    var currentCell = Int()
    let couponCode = ApiClass()
    var promoCouponArray = [[String:Any]]()
    var bankCouponArray = [[String:Any]]()
    var reloadMoreOffer: Bool!
    var reloadMoreBank:Bool!
    var promoShortArray = [[String:Any]]()
    var bankShortArray = [[String:Any]]()
    @objc var fromScreen = String()
    var totalCountPromo = Int()
    var currentCountPromo = Int()
    var tncHeight = CGFloat()
    var linesCount = String()
    
    @IBOutlet weak var couponTextView: UIView!
    @IBOutlet weak var ConstantCVTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _errorCoupon.isHidden = true
        reloadMoreOffer = false
        reloadMoreBank = false
        
        _collectionView.isHidden = false
        noOfferView.isHidden = true
        getCouponListing()
        if self.fromScreen == "RideDetails"{
            self.couponTextView.isHidden = false
        }
        else{
            self.couponTextView.isHidden = true
        }
        let nibCell = UINib(nibName: "CouponCVCell", bundle: nil)
        _collectionView.register(nibCell, forCellWithReuseIdentifier: "CouponCVCell")
        
        
        let headerCell = UINib(nibName: "CouponHeaderCell", bundle: nil)
        _collectionView.register(headerCell, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: "CouponHeaderCell")
        
        
        let footerCell = UINib(nibName: "CouponFooterView", bundle: nil)
        _collectionView.register(footerCell, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter , withReuseIdentifier: "CouponFooterView")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if fromScreen == "RideDetails"{
            ConstantCVTop.constant = 79
            
        }
        else{
            ConstantCVTop.constant = 0
        }
    }
    func showLoader(){
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            if fromScreen == "RideDetails"{
                return reloadMoreOffer ?  promoCouponArray.count :promoShortArray.count
            }
            return promoCouponArray.count
        }
        return reloadMoreBank ? bankCouponArray.count :bankShortArray.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CouponCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CouponCVCell", for: indexPath) as! CouponCVCell
        CommonClass.CellLayer(cell: cell)
        
        if fromScreen == "RideDetails"{
            cell._applyBtn.isHidden = false
            cell._applyBtn.isUserInteractionEnabled = true
            
        }
        else{
            cell._applyBtn.isHidden = true
            cell._applyBtn.isUserInteractionEnabled = false
        }
        
        if indexPath.section == 0{
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 2
            let attrString = self.promoCouponArray[indexPath.row]["promo_conditions"] as! NSMutableAttributedString
            attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
            cell.termsConditionLbl.attributedText = self.promoCouponArray[indexPath.row]["promo_conditions"] as? NSMutableAttributedString
            cell._promoCode.text = self.promoCouponArray[indexPath.row]["promotion_code"] as? String
            
            
            var titleText = String()
            titleText = self.promoCouponArray[indexPath.row]["title"] as! String
            
            
//        var titleText = String()
//
//            var array = NSArray()
//            array = ["Hello my name is Chetan Raja and tell me something","Hello my name is Chetan Rajauria and tell me Hello my name is Chetan Rajauria and tell me some","Hello my name is Chetan Rajauria and tell me some Hello my name is Chetan Rajauria and tell me some Hello my name is Chetan Rajauria and tell me some."]
//            titleText = array.object(at: indexPath.row) as! String
            
            
            if (titleText.count < 55)
            {
                print("counttt :- - ",titleText.count)
                cell.titleConstraint.constant = 30;
                linesCount = "one"
                
            }
                
            else if(titleText.count > 55 && titleText.count < 110)
            {
                print("counttt :- - ",titleText.count)
                cell.titleConstraint.constant = 45;
                linesCount = "two"
                
            }
            else
            {
                print("counttt :- - ",titleText.count)
                cell.titleConstraint.constant = 60;
                linesCount = "three"
            }
            
            
            //cell.title.text = self.promoCouponArray[indexPath.row]["title"] as? String
            
            
            cell.title.text = titleText
            
            // One Line -- 60 Char
            // Two Line -- 110 Char
            // Three Line -- 110 Char
            
            
            let date = self.promoCouponArray[indexPath.row]["expiry_date"] as? String
            print("date is ======\(date)")
            let datt =  convertDateFormater(date!)
            print("datt is ======\(datt)")
            cell.validUpto.text = "Valid till " + datt
        }
        else
        {
            
            cell.termsConditionLbl.attributedText = self.bankCouponArray[indexPath.row]["promo_conditions"] as? NSMutableAttributedString
            cell._promoCode.text = self.bankCouponArray[indexPath.row]["promotion_code"] as? String
            cell.title.text = self.bankCouponArray[indexPath.row]["title"] as? String
            let date = self.bankCouponArray[indexPath.row]["expiry_date"] as? String
            let datt =  convertDateFormater(date!)
            cell.validUpto.text = "Valid till " + datt
            
        }
        cell._promoCode.textAlignment = .justified
        cell.arrayImage.image = UIImage.init(named: "arrow-down")
        if (self.promoCouponArray[indexPath.row]["isOpen"] as! Bool == true){
            UIView.animate(withDuration: 0.25, animations: {
                cell.termsConditionLbl.isHidden = false
                cell.arrayImage.image = UIImage(named: "arrow-up")
            })
        }
        else{
            UIView.animate(withDuration: 0.25, animations: {
                cell.termsConditionLbl.isHidden = true
                cell.arrayImage.image = UIImage(named: "arrow-down")
            })
        }
        
        cell._termsBtn.addTarget(self, action:#selector(TermsConditionButtonClicked(_:)), for: .touchUpInside)
        cell._applyBtn.addTarget(self, action:#selector(applyCodeFromListing(_:)), for: .touchUpInside)
        
        return cell
        // Configure the cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if promoCouponArray.count > 0{
            return getCellSize(description: promoCouponArray[indexPath.row]["promo_conditions"] as! NSMutableAttributedString, index: indexPath, int: currentCell, open: promoCouponArray[indexPath.row]["isOpen"] as! Bool)
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind{
        case UICollectionView.elementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CouponHeaderCell", for: indexPath) as! CouponHeaderCell
            if indexPath.section == 0{
                header.titleLabel.text = "Applicable Offers"
            }
            else{
                header.titleLabel.text = "Bank Offers"
            }
            return header
        case UICollectionView.elementKindSectionFooter:
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "CouponFooterView", for: indexPath) as! CouponFooterView
            _collectionView.collectionViewLayout.invalidateLayout()
            footer._btnMore.addTarget(self, action:#selector(MoreButtoClicked(_:)), for: .touchUpInside)
            return  footer
            
        default:
            
            fatalError("Unexpected element kind")
        }
        // return reusableview
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            
            return CGSize(width: self.view.frame.width, height: 50)
        }
        else{
            if bankCouponArray.count > 0{
                return CGSize(width: self.view.frame.width, height: 50)
            }
            else{
                return CGSize.zero
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForFooterInSection section: Int) -> CGSize{
        if section == 0{
            if fromScreen == "RideDetails"{
                if reloadMoreOffer || totalCountPromo < 4{
                    return CGSize.zero
                }
                
                return CGSize(width: self.view.frame.width, height: 50)
            }
            return CGSize.zero
        }
        else{
            if bankCouponArray.count > 0{
                return CGSize(width: self.view.frame.width, height: 50)
            }
            else{
                return CGSize.zero
            }
        }
    }
    
    
    func getCellSize(description: NSMutableAttributedString , index:IndexPath , int:Int,open:Bool) -> CGSize{
        
        
        var titleTxt = String()
        titleTxt = promoCouponArray[index.row]["title"] as! String

        
//        var titleTxt = String()
//        var array = NSArray()
//        array = ["Hello my name is Chetan Raja and tell me something","Hello my name is Chetan Rajauria and tell me Hello my name is Chetan Rajauria and tell me some","Hello my name is Chetan Rajauria and tell me some Hello my name is Chetan Rajauria and tell me some Hello my name is Chetan Rajauria and tell me some."]
//        titleTxt = array.object(at: index.row) as! String
        
        
        
        if (promoCouponArray[index.row]["isOpen"] as! Bool == true){
            
            if (linesCount == "one")
            {
                return CGSize.init(width: self.view.frame.size.width - 30 , height: self.getCellHeight(string: description.string)+115) //130 -- chetan
                
            }
            else if (linesCount == "two")
            {
                return CGSize.init(width: self.view.frame.size.width - 30 , height: self.getCellHeight(string: description.string)+130) //130 -- chetan
            }
            else
            {
                return CGSize.init(width: self.view.frame.size.width - 30 , height: self.getCellHeight(string: description.string)+145) //130 -- chetan
            }
        }
        else{
            
            if (titleTxt.count < 55)
            {
                return CGSize.init(width: self.view.frame.size.width - 30 , height: 130) //130 -- chetan
            }
            else if (titleTxt.count > 55 && titleTxt.count < 110)
            {
                return CGSize.init(width: self.view.frame.size.width - 30 , height: 145) //130 -- chetan
            }
            else
            {
                return CGSize.init(width: self.view.frame.size.width - 30 , height: 170) //130 -- chetan
            }
        }
        
    }
    
    func getCouponListing(){
        if fromScreen == "SalesImage"{
            
            let deadlineTime = DispatchTime.now() + .seconds(1)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.showLoader()
                
            }
            
        }
        else{
            DispatchQueue.main.async {
                self.showLoader()
            }
        }
        var param = [String: Any]()
        if fromScreen == "RideDetails"{
            param[k_CarModelId] = couponDict[k_CarModelId]
            param[k_cityId] = couponDict[k_cityIDCap]
            param["ClientCoIndivId"] = couponDict["userId"]
            param[k_pickupDate] = couponDict[k_pickUpdate]
            param[k_TotalFare] = couponDict[k_totalFare]
            param[k_pkgIdCap] = couponDict[k_pkgId]
            param[k_dropOffDate] = couponDict[k_dropOffDate]
            
        }
        print(param)
        couponCode.getCouponListing(dict: param , strURL: kgetDiscount, completionHandler: {(promoCoupon) in
            if promoCoupon != nil{
                
                if promoCoupon?.count == 0{
                    DispatchQueue.main.async {
                        self._collectionView.isHidden = true
                        self.noOfferView.isHidden = false
                        SVProgressHUD.dismiss()
                    }
                    return
                }
                else{
                    DispatchQueue.main.async {
                        self._collectionView.isHidden = false
                        self.noOfferView.isHidden = true
                    }
                }
                //     FIRAnalytics.logEvent(withName: "App_offers_Shown", parameters: nil)
                for item in promoCoupon!{
                    
                    
                    var TermsCond =  [NSMutableAttributedString]()
                    if let promoCondition = item["promo_conditions"] as? [String]{
                        
                        for i in 0..<promoCondition.count{
                            let strr = self.addingBulletInText(str: promoCondition[i])
                            TermsCond.append(strr)
                        }
                        
                    }
                    let applicable_cities = CommonClass.attributedForCoupon(string: "Applicable Cities")
                    TermsCond.append(applicable_cities)
                    var applicableCity = NSMutableAttributedString()
                    if let cityListing = item["applicable_cities"] as? [String]
                    {
                        if cityListing.count == 1{
                            let strr = self.addingBulletInText(str: cityListing[0])
                            strr.append(NSMutableAttributedString(string: "\n"))
                            applicableCity = strr
                        }
                        else{
                            
                            for i in 0..<cityListing.count{
                                if i == 0{
                                    let strr = self.addingBulletInText(str: cityListing[0])
                                    applicableCity.append(strr)
                                }
                                else{
                                    //     let strr = self.addingBulletInText(str: cityListing[i])
                                    applicableCity.append(NSMutableAttributedString(string: ", "))
                                    applicableCity.append(NSMutableAttributedString(string: cityListing[i]))
                                }
                            }
                        }
                        TermsCond.append(applicableCity)
                    }
                    
                    let applicable_models = CommonClass.attributedForCoupon(string: "Applicable Car Models")
                    TermsCond.append(applicable_models)
                    
                    
                    var applicableModel = NSMutableAttributedString()
                    if let modelListing = item["applicable_models"] as? [String]
                    {
                        if modelListing.count == 1{
                            let strr = self.addingBulletInText(str: modelListing[0])
                            strr.append(NSMutableAttributedString(string: "\n"))
                            applicableModel = strr
                        }
                        else{
                            
                            for i in 0..<modelListing.count{
                                if i == 0{
                                    let strr = self.addingBulletInText(str: modelListing[0])
                                    applicableModel.append(strr)
                                }
                                else{
                                    applicableModel.append(NSMutableAttributedString(string: ", "))
                                    applicableModel.append(NSMutableAttributedString(string: modelListing[i]))
                                }
                            }
                        }
                        TermsCond.append(applicableModel)
                    }
                    
                    let promo_conditions = self.getTextFromArray(array: TermsCond)
                    
                    self.dictionary["expiry_date"] = item["expiry_date"] as? String
                    self.dictionary["is_bank_offer"] = item["is_bank_offer"] as? String
                    
                    if self.fromScreen == "RideDetails"{
                        let percentage = item["maximum_discount_percentage"] as! Int
                        let Fare = self.couponDict["totalFare"] as? String
                        let totalFare = roundf(Float(Fare!)! * percentage%)
                        let perDiscount = Int(totalFare)
                        self.dictionary["maximum_discount_percentage"] = perDiscount
                        self.dictionary["maximum_discount_amount"] = item["maximum_discount_amount"] as? Int
                    }
                    
                    self.dictionary["promo_conditions"] = promo_conditions
                    self.dictionary["promotion_code"] = item["promotion_code"] as? String
                    self.dictionary["title"] = item["title"] as? String
                    
                    self.dictionary["isOpen"] = false
                    print(self.dictionary)
                    if item["is_bank_offer"] as! Bool{
                        self.bankCouponArray.append(self.dictionary)
                    }
                    else{
                        
                        
                        self.promoCouponArray.append(self.dictionary)
                        self.totalCountPromo = self.promoCouponArray.count
                    }
                    
                    
                }
            }
                
            else{
                DispatchQueue.main.async {
                    self._collectionView.isHidden = true
                    self.noOfferView.isHidden = false
                }               
            }
            self.fireBaseEvents()
            // FIRAnalytics.logEvent(withName: "Bank_offers_Shown", parameters: nil)
            if self.fromScreen == "SalesImage"{
                
                let deadlineTime = DispatchTime.now() + .seconds(1)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    SVProgressHUD.dismiss()
                    
                }
                
            }
            else{
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            DispatchQueue.main.async {
                self.showlessOffers(currentIndex: 1)
                self._collectionView.reloadData()
                
            }
            
            
        })
        SVProgressHUD.dismiss()
    }
    
    func showlessOffers(currentIndex: Int){
        promoShortArray = [[:]]
        if promoCouponArray.count > 0{
            promoShortArray = Array(promoCouponArray.prefix(currentIndex*3))
        }
        if bankCouponArray.count > 0{
            
            bankShortArray = Array(bankCouponArray.prefix(5))
        }
    }
    
    func fireBaseEvents(){
        
        
        if fromScreen == "RideDetails"{
            if promoCouponArray.count == 0{
                FIRAnalytics.logEvent(withName: "App_offers_NotShown", parameters: nil)
            }
            if promoCouponArray.count > 0{
                FIRAnalytics.logEvent(withName: "App_offers_Shown", parameters: nil)
            }
            if bankCouponArray.count == 0{
                FIRAnalytics.logEvent(withName: "Bank_offers_NotShown", parameters: nil)
            }
            if bankCouponArray.count > 0{
                FIRAnalytics.logEvent(withName: "Bank_offers_Shown", parameters: nil)
            }
            
        }
        else if fromScreen == "MenuScreen"{
            if promoCouponArray.count == 0{
                //            FIRAnalytics.logEvent(withName: "App_offers_NotShown_nav", parameters: nil)
            }
            if promoCouponArray.count > 0{
                //           FIRAnalytics.logEvent(withName: "App_offers_Shown_nav", parameters: nil)
            }
            if bankCouponArray.count == 0{
                //          FIRAnalytics.logEvent(withName: "Bank_offers_NotShown_nav", parameters: nil)
            }
            if bankCouponArray.count > 0{
                //         FIRAnalytics.logEvent(withName: "Bank_offers_Shown_nav", parameters: nil)
            }
        }
        else if fromScreen == "SalesImage"{
            if promoCouponArray.count == 0{
                //         FIRAnalytics.logEvent(withName: "App_offers_NotShown_Mkt", parameters: nil)
            }
            if promoCouponArray.count > 0{
                //       FIRAnalytics.logEvent(withName: "App_offers_Shown_Mkt", parameters: nil)
            }
            if bankCouponArray.count == 0{
                //      FIRAnalytics.logEvent(withName: "Bank_offers_NotShown_Mkt", parameters: nil)
            }
            if bankCouponArray.count > 0{
                //      FIRAnalytics.logEvent(withName: "Bank_offers_Shown_Mkt", parameters: nil)
            }
        }
    }
    
    func addingBulletInText(str:String) -> NSMutableAttributedString{
        let stringValue = "●  "   //\u{2022}
        let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.red ]
        let myAttrString = NSAttributedString(string: stringValue, attributes: myAttribute)
        let combination = NSMutableAttributedString()
        let stringV = NSMutableAttributedString(string: str, attributes: nil)
        combination.append(myAttrString)
        combination.append(stringV)
        
        return combination
    }
    
    func getTextFromArray(array:[NSMutableAttributedString])-> NSMutableAttributedString{
        let string = NSMutableAttributedString()
        if array.count > 0{
            for i in 0..<array.count{
                if i == 0{
                    string.append(array[i])
                    string.append(NSMutableAttributedString(string: "\n"))
                }
                else{
                    
                    if array[i].string == "Applicable Cities"{
                        string.append(NSMutableAttributedString(string: "\n"))
                        string.append(array[i])
                        string.append(NSMutableAttributedString(string: "\n"))
                        //    string.append(NSMutableAttributedString(string: "\n"))
                        
                    }
                    else if array[i].string == "Applicable Car Models"{
                        //  string.append(NSMutableAttributedString(string: "\n"))
                        string.append(array[i])
                        string.append(NSMutableAttributedString(string: "\n"))
                        //    string.append(NSMutableAttributedString(string: "\n"))
                        
                    }
                    else{
                        
                        string.append(array[i])
                        string.append(NSMutableAttributedString(string: "\n"))
                    }
                    print(string)
                }
            }
            print(string)
        }
        return string
    }
    
    func getCellHeight(string:String) -> CGFloat{
        
        let labelHeight = CommonClass.getLabelHeight(string, _collectionView.frame.size.width - 50, 13.5)
        print(labelHeight)
        
        return labelHeight
    }
    
    
    @objc func TermsConditionButtonClicked( _ sender: UIButton){
        
        print("button is clicked")
        
        if let indexPath = _collectionView.indexPathForItem(at: _collectionView.convert(sender.center, from: sender.superview)) {
            print("indexPath is ========\(indexPath)")
            
            currentCell = indexPath.row
            let viewStatus = self.promoCouponArray[indexPath.row]["isOpen"] as! Bool
            self.promoCouponArray[indexPath.row]["isOpen"]  = !viewStatus
            
//            var titleText = String()
//            var array = NSArray()
//            array = ["Hello my name is Chetan Raja and tell me something","Hello my name is Chetan Rajauria and tell me Hello my name is Chetan Rajauria and tell me some","Hello my name is Chetan Rajauria and tell me some Hello my name is Chetan Rajauria and tell me some Hello my name is Chetan Rajauria and tell me some."]
//            titleText = array.object(at: indexPath.row) as! String
            
            var titleText = String()
            titleText = self.promoCouponArray[indexPath.row]["title"] as! String
            
            
            if (titleText.count < 55)
            {
                linesCount = "one"
            }
            else if (titleText.count > 55 && titleText.count < 110)
            {
                linesCount = "two"
            }
            else
            {
                linesCount = "three"
            }
            
            
            _collectionView.reloadData()
        }
        
    }
    
    
    @objc func applyCodeFromListing( _ sender: UIButton){
        
        DispatchQueue.main.async {
            self.showLoader()
        }
        if let indexPath = _collectionView.indexPathForItem(at: _collectionView.convert(sender.center, from: sender.superview)) {
            print("indexPath is ========\(indexPath)")
            couponDict["DiscountCode"] = promoCouponArray[indexPath.row]["promotion_code"] as? String
            print(couponDict)
            
            couponCode.ApplyCouponCode(dict: couponDict, strURL: kapplyDiscount, completionHandler: {(dictionary) in
                
                print(dictionary ?? "")
                
                let dic = dictionary as? [String: Any]
                
                if dic == nil{
                    SVProgressHUD.dismiss()
                    return
                }
                
                if dic!["status"] as! Int == 0{
                    DispatchQueue.main.async {
                        
                        let alert = UIAlertController(title: "Alert", message: dic!["Reason"] as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        FIRAnalytics.logEvent(withName: "Applied_SystemCoupon_fail", parameters: nil)
                    }
                    SVProgressHUD.dismiss()
                    return
                }
                
                FIRAnalytics.logEvent(withName: "Applied_SystemCoupon", parameters: nil)
                let  discountCode = dic!["DiscountCode"] as? String
                let perDiscount = dic!["PerDiscount"] as? String
                SVProgressHUD.dismiss()
                self.delegate?.selectedCity(cityM: discountCode!, updatedCityList:perDiscount! )
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }
        //        if let indexPath = _collectionView.indexPathForItem(at: _collectionView.convert(sender.center, from: sender.superview)) {
        //            print("indexPath is ========\(indexPath)")
        //
        //            let discountCode = promoCouponArray[indexPath.row]["promotion_code"] as? String
        //            let perAmount = promoCouponArray[indexPath.row]["maximum_discount_percentage"] as? Int
        //            if perAmount == 0{
        //                let disAmount = promoCouponArray[indexPath.row]["maximum_discount_amount"] as! Int
        //                let discountAmount = String(disAmount)
        //
        //                 FIRAnalytics.logEvent(withName: "Applied_SystemCoupon", parameters: nil)
        //                    self.delegate?.selectedCity(cityM: discountCode!, updatedCityList: discountAmount)
        //            }
        //            else{
        //                let percentageAmount = String(perAmount!)
        //
        //                 FIRAnalytics.logEvent(withName: "Applied_SystemCoupon", parameters: nil)
        //                self.delegate?.selectedCity(cityM: discountCode!, updatedCityList: percentageAmount)
        //            }
        //
        //            self.dismiss(animated: true) {
        //                }
        //        }
        
    }
    
    @objc func MoreButtoClicked( _ sender: UIButton){
        
        currentCountPromo += 1
        showlessOffers(currentIndex: currentCountPromo)
        
        
        if currentCountPromo * 3 < totalCountPromo{
            reloadMoreOffer = false
        }
        else{
            reloadMoreOffer = true
        }
        
        _collectionView.reloadData()
        
        //      }
        
    }
    
    @IBAction func ApplyBtn(_ sender: Any) {
        DispatchQueue.main.async {
            self.showLoader()
        }
        self._CouponTextField.resignFirstResponder()
        _errorCoupon.isHidden = true
        couponDict["DiscountCode"] = _CouponTextField.text
        print(couponDict)
        
        couponCode.ApplyCouponCode(dict: couponDict, strURL: kapplyDiscount, completionHandler: {(dictionary) in
            
            print(dictionary ?? "")
            
            let dic = dictionary as? [String: Any]
            
            if dic == nil{
                SVProgressHUD.dismiss()
                return
            }
            
            if dic!["status"] as! Int == 0{
                DispatchQueue.main.async {
                    self._errorCoupon.isHidden = false
                    self._errorCoupon.text = "Please enter a valid coupon code" //dic!["Reason"] as! String
                    
                    FIRAnalytics.logEvent(withName: "Applied_UserCoupon_fail", parameters: nil)
                }
                SVProgressHUD.dismiss()
                return
            }
            
            FIRAnalytics.logEvent(withName: "Applied_UserCoupon", parameters: nil)
            let  discountCode = dic!["DiscountCode"] as? String
            let perDiscount = dic!["PerDiscount"] as? String
            SVProgressHUD.dismiss()
            self.delegate?.selectedCity(cityM: discountCode!, updatedCityList:perDiscount! )
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
            
        })
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        _CouponTextField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _CouponTextField.resignFirstResponder()
        return true
    }
    
    @IBAction func backButton(_ sender: Any) {
        if fromScreen == "RideDetails"{
            FIRAnalytics.logEvent(withName: "Back_OfferPage", parameters: nil)
            
        }
        else if fromScreen == "MenuScreen"{
            FIRAnalytics.logEvent(withName: "Back_OfferPage_nav", parameters: nil)
        }
        else if fromScreen == "SalesImage"{
            FIRAnalytics.logEvent(withName: "Back_OfferPage_Mkt", parameters: nil)
        }
        
        _CouponTextField.resignFirstResponder()
        self.dismiss(animated: true) {
        }
    }
    func convertDateFormater(_ date: String) -> String
    {
        //4/30/2018 12:00:00 AM
        let outFormatter = DateFormatter()
        outFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        let date = outFormatter.date(from: date)!
        
        outFormatter.dateFormat = "dd MMM yyyy"
        let outStr = outFormatter.string(from: date)
        print(outStr) // -> outputs 16-Aug 05 : 08
        
        return outStr
        
    }
    
    
    
    
    
}




