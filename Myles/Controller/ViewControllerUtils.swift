//
//  ViewControllerUtils.swift
//  Myles
//
//  Created by iOS Team on 22/09/16.
//  Copyright © 2016 Sourabh. All rights reserved.
//

import Foundation
import UIKit

struct Platform {
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
        return TARGET_IPHONE_SIMULATOR != 0 // Use this line in Xcode 6
    }
    
}

class ViewControllerUtils {
    
    
    fileprivate var container = UIView()
    fileprivate var loadingView = UIView()
    fileprivate var activityIndicator = UIActivityIndicatorView()
    
    /*
     Show customized activity indicator,
     actually add activity indicator to passing view
     
     @param uiView - add activity indicator to this view
     */
    func showActivityIndicator(_ uiView: UIView) {
        self.container.frame = uiView.frame
        self.container.center = uiView.center
        self.container.backgroundColor = UIColorFromHex(0xffffff, alpha: 0.5)
        
        self.loadingView.frame = CGRect(x:0.0,y:0.0, width:80.0,height: 80.0)
        self.loadingView.center = uiView.center
        self.loadingView.backgroundColor = UIColorFromHex(0x444444, alpha: 0.7)
        self.loadingView.clipsToBounds = true
        self.loadingView.layer.cornerRadius = 10
        
        self.activityIndicator.frame = CGRect(x:0.0,y:0.0, width:40.0,height: 40.0)
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        self.activityIndicator.center = CGPoint(x:loadingView.frame.size.width / 2, y:loadingView.frame.size.height / 2);
        
        self.loadingView.addSubview(self.activityIndicator)
        self.container.addSubview(self.loadingView)
        uiView.addSubview(self.container)
        self.activityIndicator.startAnimating()
    }
    
    /*
     Hide activity indicator
     Actually remove activity indicator from its super view
     
     @param uiView - remove activity indicator from this view
     */
    func hideActivityIndicator(_ uiView: UIView) {
        self.activityIndicator.stopAnimating()
        self.container.removeFromSuperview()
    }
    
    /*
     Define UIColor from hex value
     
     @param rgbValue - hex color value
     @param alpha - transparency level
     */
    func UIColorFromHex(_ rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }

}
