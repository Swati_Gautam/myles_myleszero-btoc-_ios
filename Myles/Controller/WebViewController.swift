//
//  WebViewController.swift
//  Myles
//
//  Created by Akanksha Singh on 29/01/18.
//  Copyright © 2018 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate { //UIWebViewDelegate {

    @IBOutlet weak var progressBar: UIProgressView!
    @objc var UrlString:String!
    @objc var isTermsAndConditions:String!
    @objc var myTimer:Timer!
    @objc var isComplete = Bool()
    @objc var _webView: WKWebView!
    //@IBOutlet weak var _webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        _webView.navigationDelegate = self
        _webView = WKWebView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 20, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(_webView)
        showLoader()
        let url = NSURL (string: UrlString)
        let requestObj = NSURLRequest(url: url! as URL)
        _webView.load(requestObj as URLRequest);
        // Do any additional setup after loading the view.
        if(isTermsAndConditions == "true"){
            self.title = "Terms & Conditions"
        }
        else{
            self.title = "Key rental terms"
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(myTimer != nil){
            myTimer.invalidate()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!)
    {
        debugPrint("didCommit")
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        print("didFailLoadWithError \(String(describing: error.localizedDescription))")
    }
    
    
    func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        debugPrint("didFinish")
        isComplete = true
        if webView.isLoading {
            // still loading
            return
        }
        // SVProgressHUD.dismiss()
        
        print("finished")
    }
    
    /*func webViewDidFinishLoad(_ webView: UIWebView) {
        isComplete = true
        if webView.isLoading {
            // still loading
            return
        }
       // SVProgressHUD.dismiss()

        print("finished")
        // finish and do something here
    }
    
    @nonobjc func webView(webView: UIWebView, didFailLoadWithError error: Error?) {
        print("didFailLoadWithError \(String(describing: error?.localizedDescription))")
        // error happens
    }*/
    
    func showLoader () {
        /*SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")*/
        if(myTimer != nil){
            myTimer.invalidate()
        }
        progressBar.progress = 0.0
        isComplete = false
        progressBar.isHidden = false
        myTimer =  Timer.scheduledTimer(timeInterval: 0.01667,target: self,selector: #selector(WebViewController.timerCallback),userInfo: nil,repeats: true)
    }
    
    @objc func timerCallback(){
        if isComplete {
            if progressBar.progress >= 1 {
                progressBar.isHidden = true
                myTimer.invalidate()
            }else{
                progressBar.progress += 0.1
            }
        }else{
            progressBar.isHidden = false
            progressBar.progress += 0.05
            if progressBar.progress >= 0.95 {
                progressBar.progress = 0.95
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
