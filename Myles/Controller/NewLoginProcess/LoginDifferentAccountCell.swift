//
//  LoginDifferentAccountCell.swift
//  Myles
//
//  Created by Itteam2 on 20/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
//import GoogleSignIn

class LoginDifferentAccountCell: UITableViewCell {

    @IBOutlet weak var btnSignInWithApple: Button!
    
    @IBOutlet weak var btnSignInAppleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var googleSignInButton: Button!
    @IBOutlet weak var facebookSignInButton: Button!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnSubmit.layer.cornerRadius = 5
        facebookSignInButton.layer.cornerRadius = 5
        googleSignInButton.layer.cornerRadius = 5
        self.appleCustomLoginButton()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        googleSignInButton.didTouchUpInside = nil
    }
    
    func appleCustomLoginButton()
    {
            if #available(iOS 13.0, *)
            {
                //let customAppleLoginBtn = UIButton()
                btnSignInWithApple.layer.cornerRadius = 4.0
                btnSignInWithApple.layer.borderWidth = 1.0
                btnSignInWithApple.backgroundColor = UIColor.white
                btnSignInWithApple.layer.borderColor = UIColor.black.cgColor
                //btnSignInWithApple.setTitle("Sign in with Apple", for: .normal)
                btnSignInWithApple.setTitleColor(UIColor.black, for: .normal)
                //btnSignInWithApple.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
                btnSignInWithApple.setImage(UIImage(named: "apple"), for: .normal)
                btnSignInWithApple.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 12)
                //customAppleLoginBtn.addTarget(self, action: #selector(actionHandleAppleSignin), for: .touchUpInside)
                //self.addSubview(customAppleLoginBtn)
                // Setup Layout Constraints to be in the center of the screen
                /*customAppleLoginBtn.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([
                    customAppleLoginBtn.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                    customAppleLoginBtn.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                    customAppleLoginBtn.widthAnchor.constraint(equalToConstant: 200),
                    customAppleLoginBtn.heightAnchor.constraint(equalToConstant: 40)
                    ])*/
            }
        }
}
