//
//  LoginProcess.swift
//  Myles
//
//  Created by Itteam2 on 20/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import GoogleSignIn
import SVProgressHUD
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import FirebaseAnalytics
import AuthenticationServices

@objc protocol LoginProtocol
{
   @objc func afterLoginChangeDestination(strDestination: String)
    
}

let appleProvider = AppleSignInClientClass()
let GOOGLE_API_KEY = "AIzaSyA_FGVf0WztN-uJtTC89D0hFoezuN7vmLg"

class LoginProcess: UIViewController,UITableViewDelegate,UITableViewDataSource,AfterNumberVerify,UITextFieldDelegate,SignUpComplete,GIDSignInDelegate,GIDSignInUIDelegate
{
    
    @IBOutlet weak var btnSignInWithApple: Button!
    @objc var submitAction = UIAlertAction()
    @objc var txtOTP = UITextField()
    @objc var txtNewPassword = UITextField()
    @objc var fromScreen = String()
    @objc var strOtherDestination = ""
    @objc var phoneNumber = ""
    @objc var isSignin = Bool()
    @objc var bSignUp = Bool()
    @objc weak var delegate : LoginProtocol?
    
    @IBOutlet weak var tblLogin: UITableView!
    
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblLogin.backgroundView?.backgroundColor = UIColor.clear
        
        
     self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        let tabG = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tabG)
        
        
    }
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        
        for view in view.subviews {
            if view is UITextField {
                view.resignFirstResponder()
            }
        }
    }
    /**
     
     */
    func callSignupPage(isSignUp: Bool) {
        bSignUp = isSignUp
        isSignin = isSignUp
        tblLogin.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSignin ? bSignUp ? 2 : 3 : 2
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
        if (indexPath.row == 1 && !isSignin || indexPath.row == 1 && bSignUp) || (isSignin && indexPath.row == 2)
        {
            let identifier = "LoginDifferentAccountCell"
            var cell: LoginDifferentAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? LoginDifferentAccountCell
            if cell == nil {
                tableView.register(UINib(nibName: "LoginDifferentAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? LoginDifferentAccountCell
                cell.backgroundColor = UIColor.clear
            }
            if bSignUp {
                cell.btnSubmit.setTitle("Create Account", for: .normal)
            }
            else
            {
                cell.btnSubmit.setTitle(indexPath.row == 2 && isSignin ? "Sign In" : "Submit", for: .normal)
            }
                cell.googleSignInButton.didTouchUpInside  = {[weak weakSelf = self] (sender) in
                    self.AccountCreationSubmit()
                    weakSelf?.tblLogin.isUserInteractionEnabled = false

                    GIDSignIn.sharedInstance().delegate = weakSelf!
                    //GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
                    GIDSignIn.sharedInstance().uiDelegate = weakSelf!
                    //GIDSignIn.sharedInstance().clientID = "454846681290-380pn7a7tg8n3edp66onto1ih740rmma.apps.googleusercontent.com"

                    //GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
                    //GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/userinfo.email")
                    //GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
                    //GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/userinfo.profile")

                    GIDSignIn.sharedInstance().signIn()

                }

            
//                if indexPath.row == 2 && isSignin
//                {
////                cell.btnForgetPassword.addTarget(self, action: #selector(clickForgetPassword), for: .touchUpInside)
//                    cell.btnForgetPassword.addTarget(self, action: #selector(addButtonClicked), for: .touchUpInside)
//
//                }
                
                cell.facebookSignInButton.didTouchUpInside = { [weak weakSelf = self] (sender) in
                    self.AccountCreationSubmit()
                    print("facebookSignInButton Click")
                    weakSelf?.tblLogin.isUserInteractionEnabled = false
                    let loginManager = LoginManager() //LoginResult()
                    //let error = NSError()
                    
                    loginManager.logIn(permissions: [ .publicProfile,.email ,.userFriends,.custom("user_birthday")], viewController: weakSelf, completion: { loginResult in
                        switch loginResult {
                        case .failed(let error):
                            weakSelf?.tblLogin.isUserInteractionEnabled = true
                            loginManager.logOut()
                            print(error)
                            weakSelf?.AlertView(message: "Facebook unexpectedly stops")
                            break
                        case .cancelled:
                            print("Cancel")
                            weakSelf?.tblLogin.isUserInteractionEnabled = true

                            break
                        case .success:
                            print("Cansuccesscel")
                        //    print(grantedPermissions.debugDescription,declinedPermissions.)
                            weakSelf?.tblLogin.isUserInteractionEnabled = true
                            weakSelf?.fetchUserProfile()
                            break
                        }
                    })
                }
//            }
            if #available(iOS 13.0, *)
            {
               cell.btnSignInWithApple.isHidden = false
               cell.btnSignInAppleHeightConstraint.constant = 50.0
               cell.btnSignInWithApple.addTarget(self, action: #selector(btnSignInWithApple_Click), for: .touchUpInside)
            }
            else
            {
              cell.btnSignInWithApple.isHidden = true
              cell.btnSignInAppleHeightConstraint.constant = 0.0
                            //Fallback on earlier versions
            }
            cell.btnSubmit.addTarget(self, action: #selector(clickSubmit), for: .touchUpInside)
            return cell    
        }
        else
        {
        let identifier = "TextFieldForLoginRegistrationCell"
        var cell: TextFieldForLoginRegistrationCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldForLoginRegistrationCell
        if cell == nil {
            tableView.register(UINib(nibName: "TextFieldForLoginRegistrationCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldForLoginRegistrationCell
            cell.backgroundColor = UIColor.clear

        }
            cell.txtField.tag = indexPath.row
            cell.txtField.placeholder = indexPath.row == 1 && isSignin ? "Password" : "Mobile No."
            
            cell.txtField.isSecureTextEntry = indexPath.row == 1 && isSignin ? true:false
            
            if (((phoneNumber.characters.count) > 0) && (cell.txtField.tag == 0))
            {
                cell.txtField.text = phoneNumber
            }
            if isSignin && indexPath.row == 0
            {
                cell.isUserInteractionEnabled = false
            }
            else if indexPath.row == 1
            {
                cell.txtField.keyboardType = .alphabet
            }
            CommonFunctions.setLeftPaddingTo(cell.txtField, andPadding: 10)

        return cell
        }
     }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //do nothing
        print("DoNothing")
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let identifier = "TermsConditionCell"
        var cell: TermsConditionCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TermsConditionCell
        if cell == nil {
            tableView.register(UINib(nibName: "TermsConditionCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TermsConditionCell
            cell.backgroundColor = UIColor.clear

        }
        
//        let buttonTitleStr = NSMutableAttributedString(string:"Terms of Service", attributes:attrs)
//        attributedString.append(buttonTitleStr)
//        cell.btnTerms.setAttributedTitle(attributedString, for: .normal)
//        
//        let temString = NSMutableAttributedString(string: cell.btnTerms.titleLabel!.text!)
//        temString.addAttribute(NSFontAttributeName, value: Int(1), range: NSMakeRange(0, temString.length))
//        self.view!.layoutIfNeeded()
//        cell.btnTerms.titleLabel!.attributedText = temString

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 1 && !isSignin || indexPath.row == 1 && bSignUp) || (isSignin && indexPath.row == 2)
        {
            //return isSignin ? bSignUp ? 130 : 166 : 60 //Old Height without Sign In With Apple
            return isSignin ? bSignUp ? 180 : 230 : 60
            //return isSignin ? bSignUp ? 130 : 100 : 60
        }
        else
        {
            return 66
        }
    //    return 166
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    @IBAction func clickForgetPassword()
    {
        let apiClass = ApiClass()
        showLoader()
        apiClass.forgetPassword(forgetDict: ["mobileNo":self.phoneNumber],strUrl: KRestM1sendOTPonForgot) {[weak weakSelf = self]  (dict) in
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            guard dict != nil else
            {
                return
            }
            let response = signUpResponceModel(dict: dict!)
            
            if response.status == 1
                {
                    DispatchQueue.main.async {

                        let alertController = UIAlertController(title: "Myles", message: response.message!, preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                            self.addButtonClicked()
                        })
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }

                }
            else
            {
                

            }
            
        }
        
    }
 
    @IBAction func clickSubmit()
    {
        print("clickSubmitcalled")
        let indexpath = IndexPath(row: 0, section: 0)
        let currentCell0 = tblLogin.cellForRow(at: indexpath) as! TextFieldForLoginRegistrationCell
        //Assigning the phonenumber
        phoneNumber = currentCell0.txtField.text!
        if !isSignin
        {
            
            print(currentCell0.txtField.text ?? "empty")
            
            if CommonFunctions.getLength(currentCell0.txtField.text) == 10 && validate(phoneNumber: currentCell0.txtField.text!)
            {
                print(currentCell0.txtField.text ?? "empty number")
                
                showLoader()

                CommonApiClass.checkUserExistorNot(currentCell0.txtField.text, { [ weak weakSelf = self](dict,isValidToken) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }

                    guard isValidToken else
                    {
                        CommonFunctions.getAccessToken(completion: {
                            
                        }) // Swati In this way we have to use AccessToken Method in Swift
                        return
                    }
                    if dict != nil
                    {                        
                        print(dict?["status"] ?? "nil")
                        let strStatus : NSInteger = dict!["status"] as! NSInteger
                        switch strStatus
                        {
                            case 0: //NumberExist
                                weakSelf?.isSignin = true
                                DispatchQueue.main.async {
                                    UIView.transition(with: (weakSelf?.tblLogin)!,
                                                      duration: 0.45,
                                                      options: .transitionCrossDissolve,
                                                      animations:
                                        { () -> Void in
                                            DispatchQueue.main.async {
                                            weakSelf?.tblLogin.reloadData()
                                            }
                                    }, completion: nil)
                                }

                                //9899185034
                                
                            break
                            case  1: //Number not exist
                                DispatchQueue.main.async {
                                    self.view.endEditing(true)
                                    //FireBase event called
                                
                                
                                let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
                                let OtpController = storyBoard.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
                                OtpController.delegate = self
                                    if self.fromScreen == "From_Booking"{
                                        self.BookingMobileNoSubmit()
                                        OtpController.fromScreen = "From_Booking"
                                    }
                                OtpController.strMobileNumber = weakSelf!.phoneNumber
                                    OtpController.modalPresentationStyle = .currentContext
                                    

                            weakSelf?.navigationController!.present(OtpController, animated: true, completion: nil)
                                }

                            break
                            
                        case 2://Number Deactive By Admin
                            if dict?["message"] != nil
                            {
                                DispatchQueue.main.async {
                                    weakSelf?.AlertView(message: dict?["message"] as! String)

                                }
                            }

                            break
                            
                            default: break
                            
                        }
                    }
                    else
                    {
                        
                    }
                     })
            }
            else
            {
               AlertView(message: KAValidMobile)
            }
            
        }else if(isSignin && !bSignUp)
        {
            let indexpath = IndexPath(row: 1, section: 0)
            let currentCell = tblLogin.cellForRow(at: indexpath) as! TextFieldForLoginRegistrationCell
            
            if (currentCell.txtField.text?.isEmpty)!
            {
                AlertView(message: kEmptypasswordFieldMesg)
            }else
            {
                // login api call
                var dictLogin = [String:String]()
                dictLogin["password"] = currentCell.txtField.text?.md5()
                dictLogin["phonenumber"] = currentCell0.txtField.text
                
                #if TARGET_IPHONE_SIMULATOR
                    dictLogin["devicetoken"] = "0a826c47e82a3b2aeb14131357d470e1653b0757d4a985a8d452ad485a8951a7"
                    #else
                    dictLogin["devicetoken"] = UserDefaults.standard.object(forKey: "devicetoken") as? String ?? ""
                    #endif
                //dictLogin["devicetoken"] = "0a826c47e82a3b2aeb14131357d470e1653b0757d4a985a8d452ad485a8951a7"
                dictLogin["devicetype"] = "ios"
                
                showLoader()

                
                let apiObj = ApiClass()
                
                apiObj.loginApi(loginDict: dictLogin, completionHandler: { (dict) in
                    
                    print("Dictionary Login: %@",dict)
                    
                    SVProgressHUD.dismiss()
                    if dict != nil
                    {
                        let LoginModelObj = LoginModel(dict: dict!)
                        
                        if LoginModelObj.status == 1
                        {
                            FIRAnalytics.logEvent(withName: "login", parameters: nil)
                            UserDefaults.standard.set(true, forKey: IS_LOGIN)
                            DefaultsValues.setBooleanValueToUserDefaults(true, forKey: IS_LOGIN)
                            AppDelegate.getSharedInstance().configDynamicShortcutItems()
                            UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strEmail, forKey: KUSER_EMAIL)
                            UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strSubscribe, forKey: KUSER_SUBSCRIBE)

                            UserDefaults.standard.set((LoginModelObj.LoginResponseObj?.strLname.count)! > 0 ? (LoginModelObj.LoginResponseObj?.strFname)! + " " + (LoginModelObj.LoginResponseObj?.strLname)! : LoginModelObj.LoginResponseObj?.strFname , forKey: KUSER_NAME)
                            
                             UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strUserId, forKey: KUSER_ID)
                            let newDictHeaders = ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]
                            DefaultsValues.setCustomObjToUserDefaults(newDictHeaders, forKey: kHttpHeaders)
                            
                            DispatchQueue.main.async {
                                 UserDefaults.standard.set(currentCell0.txtField.text, forKey: KUSER_PHONENUMBER)
                            }
                            
                            if (LoginModelObj.LoginResponseObj?.strDob != nil && (LoginModelObj.LoginResponseObj?.strDob?.count)! > 0 && LoginModelObj.LoginResponseObj?.strDob != "1/1/1900 12:00:00 AM"){
                                UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strDob, forKey: "dob")
                            }

                            
                            DispatchQueue.main.async {

                            CommonFunctions.updateKeychain(forUsername: currentCell0.txtField.text, password: currentCell.txtField.text)
                            
                            CommonFunctions.searchKeyChain(forUserName: currentCell0.txtField.text, { (key) in
                                
                            })
                            }
                            DispatchQueue.main.async {

                                
                                self.navigationController?.dismiss(animated: true, completion: {
                                  if self.strOtherDestination.count > 0
                                  {
                                    self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                                  }
                                
                                    
                                })
 
                            }
                        }
                        else  {
                            DispatchQueue.main.async {

                            self.AlertView(message: LoginModelObj.strMessage)
                                
                            }
                        }

                    }
                    else
                    {
                        
                    }
                    
                })
            }
            

        }
        else if (isSignin && bSignUp)
        {
            self.AccountCreationSubmit()
            let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
            let RegistrationController = storyBoard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
            let navController = UINavigationController(rootViewController: RegistrationController)
            if self.fromScreen == "From_Booking"{
               
                RegistrationController.fromScreen = "From_Booking"
            }
            RegistrationController.strMobileNumber = currentCell0.txtField.text!
            RegistrationController.isSocialLogin = false
            RegistrationController.delegate = self
            navController.viewControllers = [RegistrationController]
            
            self.present(navController, animated: true, completion: nil)

        }
        
//        isVerified = !isVerified
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if  textField.tag == 0 {
            let length = CommonFunctions.getLength(textField.text)
            if length == 10 && !string.isEmpty
            {
                return false
            }
            else
            {
                return true
            }
        }
        else
        {
            return true
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        return textField.resignFirstResponder()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func skipEvent(_ sender: Any) {
        self.navigationController?.dismiss(animated: true
            , completion: nil)
    }
    
    func AlertView(message:String)  {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)

        }
    }
    /**
     check valid mobile number
     */
    
    private func validate(phoneNumber: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phoneNumber == filtered
    }
    
    /**
     signup complete
     */
    func signUpComplete(_ isSucess: Bool) {
//        bSignUp = false
//        tblLogin.reloadData()
        skipEvent(self)
        if self.strOtherDestination.characters.count > 0
        {
            self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
        }
        
        
    }
    
    func textFieldHandlerFullName(textField: UITextField!)
    {
        if (textField) != nil {
            textField.placeholder = "Enter your full name"
        }
    }
    func textFieldHandlerEmailID(textField: UITextField!)
    {
        if (textField) != nil {
            textField.placeholder = "Enter your apple email id"
        }
    }
    
    //MARK: - Apple Sign In
    @available(iOS 13.0, *)
        //@objc func btnSignInWithApple_Click(sender: ASAuthorizationAppleIDButton)
        //cellfo@objc func btnSignInWithApple_Click(sender: UIButton)
    @objc func btnSignInWithApple_Click(sender: ASAuthorizationAppleIDButton)
    {
        appleProvider.handleAppleIdRequest(block: { fullName, email, token, userID in
                        //receives data in login class
            //self.AccountCreationSubmit()
            appleProvider.getCredentialsState(userID: token ?? "")
            var appleDict = [String:Any]()
            let strName = ""
            let strEmail = ""
            
            appleDict["dob"] = "01/01/1987"
            appleDict["socialId"] = userID
            appleDict["fullName"] = strName //fullName
            appleDict["email"] = strEmail //email
            appleDict["mNumber"] = self.phoneNumber
            appleDict["type"] = "apple"
            
            //gDict["displayName"] = nil ?? nil
            //gDict["givenName"] = user.profile.givenName ?? nil
            //gDict["id"] = user.userID ?? nil
            print("appleDict: ", appleDict)
            //self.bSignUp = true
            print("Apple UserID: ", userID)
            //print("Social ID: ")
            
            self.signUporSignInWithSocial(socialM: SocialModel(appleDict: appleDict))
            
            
            /*let alert = UIAlertController(title: "", message: "Please enter the following details", preferredStyle:UIAlertController.Style.alert)

            alert.addTextField(configurationHandler: self.textFieldHandlerFullName)
            alert.addTextField(configurationHandler: self.textFieldHandlerEmailID)

            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                let txtFullName = alert.textFields![0] as UITextField
                print("txtFullName: \(txtFullName.text ?? "")")
                let txtEmailID = alert.textFields![1] as UITextField
                print("txtEmailID: \(txtEmailID.text ?? "")")
                
                
                if txtFullName.text != ""
                {
                    if ((txtFullName.text?.contains(" ")) != nil)
                    {
                        let arr1 = txtFullName.text?.components(separatedBy: " ")
                        let strFName = (arr1?[0] ?? "") as String
                        let strLName = (arr1?[1] ?? "") as String
                        appleDict["first_name"] = strFName
                        appleDict["last_name"] = strLName
                    }
                }
                
            }))
            self.present(alert, animated: true, completion:nil)*/
            
            /*@objc var dob : String?
                @objc var email : String?
                @objc var socialId : String?
                @objc var lName : String?
                @objc var fName : String?
                @objc var fullName : String?
                @objc var phoneNumber : String?
                @objc var type : String?*/
            
            /*let dict = ["fname":socialM.fName!,"lname":socialM.lName!,"mobileNo":socialM.phoneNumber!,"socialId":socialM.socialId!,"socialType":socialM.type!,"emailId":socialM.email!,"devicetype":"ios","dob":dob,"deviceid":UserDefaults.standard.value(forKey: "devicetoken") ?? ""]*/
            
            //self.signUporSignInWithSocial(socialM: SocialModel(dict: appleDict))
        })
    }
    
     func addButtonClicked(){
        
        let alertController = UIAlertController(title: "Enter New Password With OTP Sent On Your Mobile.", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Submit", style: .default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            secondTextField.isSecureTextEntry = true

            guard (firstTextField.text?.count)! > 0 && (secondTextField.text?.count)! > 0 else{
                return
            }
            self.showLoader()
            let apiClass = ApiClass()
            let dict = ["MobileNo":self.phoneNumber,"Password":secondTextField.text!.md5(),"Otp":firstTextField.text!,"devicetype":"ios","devicetoken":UserDefaults.standard.value(forKey: "devicetoken") as! String? ?? ""]
            
            print(dict)
            apiClass.forgetPassword(forgetDict: dict as! Dictionary<String, String>, strUrl: kRestM1ResetPassword, completionHandler: { [weak weakSelf = self] (dict) in
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                print("Forget Password",dict ?? "Forget Password  nil Response")
                
                if dict != nil
                {
                    let LoginModelObj = LoginModel(dict: dict!)
                    
                    if LoginModelObj.status == 1
                    {
                        UserDefaults.standard.set(true, forKey: IS_LOGIN)
                        AppDelegate.getSharedInstance().configDynamicShortcutItems()

                        
                        //UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strDob, forKey: "dob")

                        if (LoginModelObj.LoginResponseObj?.strDob != nil && (LoginModelObj.LoginResponseObj?.strDob?.characters.count)! > 0 && LoginModelObj.LoginResponseObj?.strDob != "1/1/1900 12:00:00 AM"){
                            UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strDob, forKey: "dob")
                        }
                        UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strEmail, forKey: KUSER_EMAIL)
                        
                        UserDefaults.standard.set((LoginModelObj.LoginResponseObj?.strLname.characters.count)! > 0 ? (LoginModelObj.LoginResponseObj?.strFname)! + " " + (LoginModelObj.LoginResponseObj?.strLname)! : LoginModelObj.LoginResponseObj?.strFname , forKey: KUSER_NAME)
                        
                        UserDefaults.standard.set(LoginModelObj.LoginResponseObj?.strUserId, forKey: KUSER_ID)
                        
                        UserDefaults.standard.set(self.phoneNumber, forKey: KUSER_PHONENUMBER)
                        
                        CommonFunctions.updateKeychain(forUsername: self.phoneNumber, password: secondTextField.text)
                        
                        CommonFunctions.searchKeyChain(forUserName: self.phoneNumber, { (key) in
                            
                        })
                        
                        DispatchQueue.main.async {
                            
                            self.navigationController?.dismiss(animated: true, completion: {
                                if self.strOtherDestination.characters.count > 0
                                {
                                    self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                                }
                                
                            })
                            
                        }
                    }
                    else  {
                        DispatchQueue.main.async {
                            
                            self.AlertView(message: LoginModelObj.strMessage)
                            
                        }
                    }
                    
                }
                
                })
            
            print("firstName \(firstTextField.text), secondName \(secondTextField.text)")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
            self.view.endEditing(true)
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter OTP"
            self.txtOTP = textField
            
              textField.addTarget(self, action: #selector(self.textChanged(_:)), for: .editingChanged)
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter New Password"
            textField.isSecureTextEntry = true;
            self.txtNewPassword = textField
            
              textField.addTarget(self, action: #selector(self.textChanged(_:)), for: .editingChanged)

        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.submitAction = saveAction
        self.submitAction.isEnabled = false
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func textChanged(_ sender:UITextField) {
        self.submitAction.isEnabled  = (self.txtOTP.text!.characters.count > 0 && self.txtNewPassword.text!.characters.count > 5)
    }
    
    //GOOGLE DELEGATE TO GET INFORMTION OF USER
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error != nil) {
            // Return the user saying sorry this time we are facing issue with Gmail Login/SignUp
            //So please use other services to get into the app.
            //return from this action
            AlertView(message: "Please signUp/login with other options, currently we are unable to fetch details.")
            self.tblLogin.isUserInteractionEnabled = true

            return
        } else {
            weak var weakSelf = self
            DispatchQueue.main.async {
                weakSelf?.tblLogin.isUserInteractionEnabled = true
                self.showLoader()

            }
        
            print("name = \(user.profile.name)")
            print("userID = \(user.userID)")
            
            var gDict = [String:Any]()
                gDict["displayName"] = nil ?? nil
                gDict["givenName"] = user.profile.givenName ?? nil
                gDict["familyName"] = user.profile.familyName ?? nil
                gDict["id"] = user.userID ?? nil
                gDict["email"] = user.profile.email ?? nil
                gDict["mNumber"] = self.phoneNumber 
                gDict["type"] = "gp"


            

//            phoneNumber = gDict["mNumber"] as? String ?? nil
//            socialId = gDict["id"] as? String ?? nil
//            dob = gDict["birthday"] as? String ?? nil
//            email = gDict["email"] as? String ?? nil
//            type = gDict["type"] as? String ?? nil

            
            // APi to find dob
            //"https://www.googleapis.com/oauth2/v1/userinfo?access_token="+user.authentication.accessToken;
            //https://www.googleapis.com/plus/v1/people/userId
            //https://www.googleapis.com/auth/plus.me
            //https://www.googleapis.com/oauth2/v1/userinfo?access_token=
            let urlProfile = "https://www.googleapis.com/plus/v1/people/\(user.userID!)?key="+GOOGLE_API_KEY
            
                //"https://www.googleapis.com/plus/v1/people/me?access_token=?access_token="+user.authentication.accessToken;
            if CommonFunctions.reachabiltyCheck()
            {
                let request = NSMutableURLRequest(url: NSURL(string: urlProfile)! as URL)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 30
                
                let task = session.dataTask(with: request as URLRequest) { [weak self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        CommonFunctions.alertTitle("Myles", withMessage: "Server not responding")
                        return
                    }
                    
                    print(data ?? "NO Data")
                    guard data != nil && (data?.count)! > 0 else {
                        print("no data found: \(error)")
                        return
                    }
                    guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,Any> else {
                        DispatchQueue.main.async {
                            self?.showAlert(message: "Server not responding")
                        }
                        return
                    }
                    
                    //let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,Any>
                    print("Json by Google Sign In:", json)
                 
//                    let result = json
                    
                    guard json.count > 0 else
                    {
                        return
                    }
                    
                    guard (json["birthday"] != nil) else
                    {
                        DispatchQueue.main.async {
                            self?.signUporSignInWithSocial(socialM: SocialModel(gDict: gDict))
                        }
                        return
                    }
                    
                    gDict["birthday"] = json["birthday"] as! String
                    DispatchQueue.main.async {
                        self?.signUporSignInWithSocial(socialM: SocialModel(gDict: gDict))
                    }
                    
                    
                }
                task.resume()
            }else
            {
                CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
                
            }


        }
    }
    
    func showAlert(message:String)  {
        let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler : nil)
        // let defaultAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }

    @IBAction func loginButtonClicked() {
        
    }
    /**
     get user details after facebook authentication
     */
    func fetchUserProfile()
    {
        showLoader()
        let params = ["fields": "email, name , first_name, gender, last_name, location,id,birthday"]
        
        // ["fields":"id, email, name, picture.width(480).height(480),last_name,"]
        
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters:params)
        
        graphRequest.start(completionHandler: {[weak weakSelf = self] (connection, result, error) -> Void in
            
            //Extra
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            if ((error) != nil)
            {
                weakSelf?.AlertView(message: "Facebook unexpectedly stops")
                print("Error took place: \(error)")
            }
            else
            {
                print("Print entire fetched result: \(result)")
                
                var result = result as! Dictionary<String, Any>
                
                guard result.count > 0 else
                {
                    return
                }
                result["mNumber"] = self.phoneNumber
                result["type"] = "fb"

                weakSelf?.signUporSignInWithSocial(socialM: SocialModel(dict: result))
              
        }
        })
    }
    
    func signUporSignInWithSocial(socialM: SocialModel)
    {
        let apiClass = ApiClass()
        print("Social ID: ", socialM.socialId)
        if self.bSignUp // sign up with social id
        {
            guard socialM.dob != nil && socialM.email != nil else
            {
                weak var weakSelf = self
                
                DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
                let RegistrationController = storyBoard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
                let navController = UINavigationController(rootViewController: RegistrationController)
                RegistrationController.isSocialLogin = true
                RegistrationController.delegate = self
                    if self.fromScreen == "From_Booking"{
                        
                        RegistrationController.fromScreen = "From_Booking"
                    }
                RegistrationController.socialM = socialM
                RegistrationController.strMobileNumber = weakSelf?.phoneNumber
                navController.viewControllers = [RegistrationController]
                
                self.present(navController, animated: true, completion: nil)
                }
                return
                
            }

            //Show indicator
            DispatchQueue.main.async {
                self.showLoader()
            }
            let dateFormatter = DateFormatter()
            if socialM.type == "fb"
            {
                dateFormatter.dateFormat = "MM/dd/yyyy"

            }
            else if socialM.type == "apple"
            {
                dateFormatter.dateFormat = "MM/dd/yyyy"

            }
            else
            {
                dateFormatter.dateFormat = "yyyy-MM-dd"

            }
            
            let date = dateFormatter.date(from: socialM.dob!)
            let dob = dateFormatter.string(from: date!)

            let dict = ["fname":socialM.fName!,"lname":socialM.lName!,"mobileNo":socialM.phoneNumber!,"socialId":socialM.socialId!,"socialType":socialM.type!,"emailId":socialM.email!,"devicetype":"ios","dob":dob,"deviceid":UserDefaults.standard.value(forKey: "devicetoken") ?? ""]
            
//
            print("Dict register Social: ", dict)
            apiClass.SocialSignInorUP(dict: dict as! Dictionary<String, String> , strUrl: "RestM1Registration_Social_new", completionHandler: { (dict) in
                print(dict ?? "dict nil" )
                let socialResponse = signUpResponceModel(dict: dict!)
                if socialResponse.status == 1
                {
                    //                            DispatchQueue.main.async {
                    //                            }
                    
                    let fullName : String
                    if socialM.fullName == nil{
                        if socialM.fName != nil && socialM.lName != nil
                        {
                            fullName = socialM.fName! + " " + socialM.lName!
                        }
                        else if socialM.fName != nil
                        {
                            fullName = socialM.fName!
                            
                        }
                        else
                        {
                            fullName = socialM.lName!
                            
                        }
                    }
                    else
                    {
                        fullName = socialM.fullName!
                        
                    }
                    
                    UserDefaults.standard.set(socialResponse.response?.userId, forKey: KUSER_ID)
                    UserDefaults.standard.set(socialM.fName, forKey: KUSER_FNAME)
                    UserDefaults.standard.set(socialM.lName, forKey: KUSER_LNAME)
                    UserDefaults.standard.set(fullName, forKey: KUSER_NAME)
                    UserDefaults.standard.set(socialM.phoneNumber, forKey: KUSER_PHONENUMBER)
                    UserDefaults.standard.set(socialM.email, forKey: KUSER_EMAIL)
                    
                    if socialM.type == "fb"
                    {
                        UserDefaults.standard.set(socialM.dob, forKey: "dob")
                        UserDefaults.standard.set(true, forKey: "socialLoginType")
                        
                    }else
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")

                        UserDefaults.standard.set(CommonFunctions.convertDate(socialM.dob), forKey: "dob")

                    }

                    UserDefaults.standard.set(true, forKey: IS_LOGIN)
                    
                    DispatchQueue.main.async {
                        AppDelegate.getSharedInstance().configDynamicShortcutItems()
                        SVProgressHUD.dismiss()
                        if self.strOtherDestination.count > 0
                        {
                            self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                        }
                        //   self.AlertView(message: socialResponse.message!)
                        
                        let alertController = UIAlertController(title: "Myles", message: socialResponse.message!, preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                            self.skipEvent(self)
                        })
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                else if socialResponse.status == 0 || socialResponse.status == 3 || socialResponse.status == 4
                {
//                    self.AlertView(message: socialResponse.message!)
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    let alertController = UIAlertController(title: "Myles", message: socialResponse.message!, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        self.skipEvent(self)
                        })
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
               
                print(dict ?? "Nodata")
            })
        }
        else // Login with social id
        {
            /*let dict = ["mobileNo":socialM.phoneNumber!,"socialId":socialM.socialId!,"type":socialM.type!,"deviceType":"ios","devicetoken":UserDefaults.standard.value(forKey: "devicetoken") ?? ""]
            DispatchQueue.main.async {
                self.showLoader()
            }
            apiClass.SocialSignInorUP(dict: dict as! Dictionary<String, String>, strUrl: "RestM1verifySocialmedia", completionHandler: { (dict) in*/
                let dict = ["mobileNo":socialM.phoneNumber!,"socialId":socialM.socialId ?? "12345678","type":socialM.type!,"emailId":socialM.email ?? "","deviceType":"ios","devicetoken":UserDefaults.standard.value(forKey: "devicetoken") ?? ""]
                DispatchQueue.main.async {
                    self.showLoader()
                }
                // apiClass.SocialSignInorUP(dict: dict as! Dictionary<String, String>, strUrl: "RestM1verifySocialmedia", completionHandler: { (dict) in
                apiClass.SocialSignInorUP(dict: dict as! Dictionary<String, String>, strUrl: "RestM2verifySocialmediaWithEmail", completionHandler: { (dict) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                print(dict ?? "nil dict")
                guard dict != nil else
                {
                    return
                }
                let socialResponse = signUpResponceModel(dict: dict!)
                if socialResponse.status == 1
                {
                //
                    let fullName : String
                    if socialM.fullName == nil{
                      if socialM.fName != nil && socialM.lName != nil
                       {
                        fullName = socialM.fName! + " " + socialM.lName!
                       }
                      else if socialM.fName != nil
                      {
                        fullName = socialM.fName!

                      }
                        else
                      {
                        fullName = socialM.lName!

                        }
                    }
                    else
                    {
                        fullName = socialM.fullName!

                    }
                    
                    
                    UserDefaults.standard.set(socialResponse.response?.dob, forKey: "dob")

                    UserDefaults.standard.set(socialResponse.response?.userId, forKey: KUSER_ID)
                    UserDefaults.standard.set(socialM.fName, forKey: KUSER_FNAME)
                    UserDefaults.standard.set(socialM.lName, forKey: KUSER_LNAME)
                    UserDefaults.standard.set(fullName, forKey: KUSER_NAME)
                    UserDefaults.standard.set(socialM.phoneNumber, forKey: KUSER_PHONENUMBER)
                    UserDefaults.standard.set(socialM.email, forKey: KUSER_EMAIL)
                    UserDefaults.standard.set(true, forKey: IS_LOGIN)
                    
                    if socialM.type == "fb"
                    {
                        UserDefaults.standard.set(true, forKey: "socialLoginType")

                    }
                    else if socialM.type == "gp"
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")

                    }
                    else if socialM.type == "apple"
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")
                        
                        UserDefaults.standard.set(socialResponse.response?.dob, forKey: "dob")

                        UserDefaults.standard.set(socialResponse.response?.userId, forKey: KUSER_ID)
                        UserDefaults.standard.set(socialResponse.response?.fName, forKey: KUSER_FNAME)
                        UserDefaults.standard.set(socialResponse.response?.lName, forKey: KUSER_LNAME)
                        socialM.fName = socialResponse.response?.fName
                        socialM.lName = socialResponse.response?.lName
                        let newFullName =  String(format: "%@ %@", socialM.fName as! CVarArg,  socialM.lName as! CVarArg)
                        UserDefaults.standard.set(newFullName, forKey: KUSER_NAME)
                        UserDefaults.standard.set(socialM.phoneNumber, forKey: KUSER_PHONENUMBER)
                        UserDefaults.standard.set(socialM.email, forKey: KUSER_EMAIL)
                        UserDefaults.standard.set(true, forKey: IS_LOGIN)
                    }
                    
                    DispatchQueue.main.async {
                        
                        AppDelegate.getSharedInstance().configDynamicShortcutItems()
                        self.skipEvent(self)

                        if self.strOtherDestination.count > 0
                        {
                            self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                        }
                        //self.delegate?.afterLoginChangeDestination(strDestination: "0")
                    }
                }
                else if(socialResponse.status == 2 && socialResponse.message == "success"){
                    //
                    let fullName : String
                    if socialM.fullName == nil{
                        if socialM.fName != nil && socialM.lName != nil
                        {
                            fullName = socialM.fName! + " " + socialM.lName!
                        }
                        else if socialM.fName != nil
                        {
                            fullName = socialM.fName!
                            
                        }
                        else
                        {
                            fullName = socialM.lName!
                            
                        }
                    }
                    else
                    {
                        fullName = socialM.fullName!
                        
                    }
                    
                    
                    UserDefaults.standard.set(socialResponse.response?.dob, forKey: "dob")
                    UserDefaults.standard.set(socialResponse.response?.userId, forKey: KUSER_ID)
                    UserDefaults.standard.set(socialM.fName, forKey: KUSER_FNAME)
                    UserDefaults.standard.set(socialM.lName, forKey: KUSER_LNAME)
                    UserDefaults.standard.set(fullName, forKey: KUSER_NAME)
                    UserDefaults.standard.set(socialM.phoneNumber, forKey: KUSER_PHONENUMBER)
                    UserDefaults.standard.set(socialM.email, forKey: KUSER_EMAIL)
                    UserDefaults.standard.set(true, forKey: IS_LOGIN)
                    
                    if socialM.type == "fb"
                    {
                        UserDefaults.standard.set(true, forKey: "socialLoginType")
                        
                    }
                    else if socialM.type == "gp"
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")
                    }
                    else if socialM.type == "apple"
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")
                    }
                    
                    
                    DispatchQueue.main.async {
                        
                        AppDelegate.getSharedInstance().configDynamicShortcutItems()
                        self.skipEvent(self)
                        
                        if self.strOtherDestination.characters.count > 0
                        {
                            self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                        }
                        //self.delegate?.afterLoginChangeDestination(strDestination: "0")
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        if socialM.type == "fb"
                        {
                            self.logoutWithSocialId(bSocialType: true)
                        }
                        else
                        {
                            self.logoutWithSocialId(bSocialType: false)

                        }
                        
                        let alertController = UIAlertController(title: "Myles", message: socialResponse.message ?? "Some thing went wrong please try after some time", preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        })
                        
                        
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            })
            
        }
    }
   /* func signUporSignInWithSocial(socialM: SocialModel)  {
        let apiClass = ApiClass()
        
        if self.bSignUp // sign up with social id
        {
            
            guard socialM.dob != nil && socialM.email != nil else
            {
                weak var weakSelf = self
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
                    let RegistrationController = storyBoard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
                    let navController = UINavigationController(rootViewController: RegistrationController)
                    RegistrationController.isSocialLogin = true
                    RegistrationController.delegate = self
                    RegistrationController.socialM = socialM
                    RegistrationController.strMobileNumber = weakSelf?.phoneNumber
                    navController.viewControllers = [RegistrationController]
                    
                    self.present(navController, animated: true, completion: nil)
                }
                return
                
            }
            
            //Show indicator
            DispatchQueue.main.async {
                self.showLoader()
            }
            let dateFormatter = DateFormatter()
            if socialM.type == "fb"
            {
                dateFormatter.dateFormat = "MM/dd/yyyy"
                
            }
            else
            {
                dateFormatter.dateFormat = "yyyy-MM-dd"
                
            }
            
            let date = dateFormatter.date(from: socialM.dob!)
            let dob = dateFormatter.string(from: date!)
            
            let dict = ["fname":socialM.fName!,"lname":socialM.lName!,"mobileNo":socialM.phoneNumber!,"socialId":socialM.socialId!,"socialType":socialM.type!,"emailId":socialM.email!,"devicetype":"ios","dob":dob,"deviceid":UserDefaults.standard.value(forKey: "devicetoken") ?? ""]
            
            //
            print(dict)
            apiClass.SocialSignInorUP(dict: dict as! Dictionary<String, String> , strUrl: "RestM1Registration_Social_new", completionHandler: { (dict) in
                print(dict ?? "dict nil" )
                let socialResponse = signUpResponceModel(dict: dict!)
                if socialResponse.status == 1
                {
                    //                            DispatchQueue.main.async {
                    //                            }
                    
                    let fullName : String
                    if socialM.fullName == nil{
                        if socialM.fName != nil && socialM.lName != nil
                        {
                            fullName = socialM.fName! + " " + socialM.lName!
                        }
                        else if socialM.fName != nil
                        {
                            fullName = socialM.fName!
                            
                        }
                        else
                        {
                            fullName = socialM.lName!
                            
                        }
                    }
                    else
                    {
                        fullName = socialM.fullName!
                        
                    }
                    
                    UserDefaults.standard.set(socialResponse.response?.userId, forKey: KUSER_ID)
                    UserDefaults.standard.set(socialM.fName, forKey: KUSER_FNAME)
                    UserDefaults.standard.set(socialM.lName, forKey: KUSER_LNAME)
                    UserDefaults.standard.set(fullName, forKey: KUSER_NAME)
                    UserDefaults.standard.set(socialM.phoneNumber, forKey: KUSER_PHONENUMBER)
                    UserDefaults.standard.set(socialM.email, forKey: KUSER_EMAIL)
                    
                    
                    
                    if socialM.type == "fb"
                    {
                        UserDefaults.standard.set(socialM.dob, forKey: "dob")
                        UserDefaults.standard.set(true, forKey: "socialLoginType")
                        
                    }else
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")
                        
                        UserDefaults.standard.set(CommonFunctions.convertDate(socialM.dob), forKey: "dob")
                        
                    }
                    
                    UserDefaults.standard.set(true, forKey: IS_LOGIN)
                    
                    DispatchQueue.main.async {
                        AppDelegate.getSharedInstance().configDynamicShortcutItems()
                        SVProgressHUD.dismiss()
                        if self.strOtherDestination.characters.count > 0
                        {
                            self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                        }
                        //   self.AlertView(message: socialResponse.message!)
                        
                        let alertController = UIAlertController(title: "Myles", message: socialResponse.message!, preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                            self.skipEvent(self)
                        })
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                else if socialResponse.status == 0 || socialResponse.status == 3 || socialResponse.status == 4
                {
                    //                    self.AlertView(message: socialResponse.message!)
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    let alertController = UIAlertController(title: "Myles", message: socialResponse.message!, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        self.skipEvent(self)
                    })
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                print(dict ?? "Nodata")
            })
        }
        else // Login with social id
        {
            //let dict = ["mobileNo":socialM.phoneNumber!,"socialId":socialM.socialId!,"type":socialM.type!,"deviceType":"ios","devicetoken":UserDefaults.standard.value(forKey: "devicetoken") ?? ""]
            let dict = ["mobileNo":socialM.phoneNumber!,"socialId":socialM.socialId!,"type":socialM.type!,"emailId":socialM.email!,"deviceType":"ios","devicetoken":UserDefaults.standard.value(forKey: "devicetoken") ?? ""]
            DispatchQueue.main.async {
                self.showLoader()
            }
            // apiClass.SocialSignInorUP(dict: dict as! Dictionary<String, String>, strUrl: "RestM1verifySocialmedia", completionHandler: { (dict) in
            apiClass.SocialSignInorUP(dict: dict as! Dictionary<String, String>, strUrl: "RestM2verifySocialmediaWithEmail", completionHandler: { (dict) in
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                print(dict ?? "nil dict")
                guard dict != nil else
                {
                    return
                }
                let socialResponse = signUpResponceModel(dict: dict!)
                if (socialResponse.status == 1 )
                {
                    //
                    let fullName : String
                    if socialM.fullName == nil{
                        if socialM.fName != nil && socialM.lName != nil
                        {
                            fullName = socialM.fName! + " " + socialM.lName!
                        }
                        else if socialM.fName != nil
                        {
                            fullName = socialM.fName!
                            
                        }
                        else
                        {
                            fullName = socialM.lName!
                            
                        }
                    }
                    else
                    {
                        fullName = socialM.fullName!
                        
                    }
                    
                    
                    UserDefaults.standard.set(socialResponse.response?.dob, forKey: "dob")
                    UserDefaults.standard.set(socialResponse.response?.userId, forKey: KUSER_ID)
                    UserDefaults.standard.set(socialM.fName, forKey: KUSER_FNAME)
                    UserDefaults.standard.set(socialM.lName, forKey: KUSER_LNAME)
                    UserDefaults.standard.set(fullName, forKey: KUSER_NAME)
                    UserDefaults.standard.set(socialM.phoneNumber, forKey: KUSER_PHONENUMBER)
                    UserDefaults.standard.set(socialM.email, forKey: KUSER_EMAIL)
                    UserDefaults.standard.set(true, forKey: IS_LOGIN)
                    
                    if socialM.type == "fb"
                    {
                        UserDefaults.standard.set(true, forKey: "socialLoginType")
                        
                    }else
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        AppDelegate.getSharedInstance().configDynamicShortcutItems()
                        self.skipEvent(self)
                        
                        if self.strOtherDestination.characters.count > 0
                        {
                            self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                        }
                        //self.delegate?.afterLoginChangeDestination(strDestination: "0")
                    }
                }
                else if(socialResponse.status == 2 && socialResponse.message == "success"){
                    //
                    let fullName : String
                    if socialM.fullName == nil{
                        if socialM.fName != nil && socialM.lName != nil
                        {
                            fullName = socialM.fName! + " " + socialM.lName!
                        }
                        else if socialM.fName != nil
                        {
                            fullName = socialM.fName!
                            
                        }
                        else
                        {
                            fullName = socialM.lName!
                            
                        }
                    }
                    else
                    {
                        fullName = socialM.fullName!
                        
                    }
                    
                    
                    UserDefaults.standard.set(socialResponse.response?.dob, forKey: "dob")
                    UserDefaults.standard.set(socialResponse.response?.userId, forKey: KUSER_ID)
                    UserDefaults.standard.set(socialM.fName, forKey: KUSER_FNAME)
                    UserDefaults.standard.set(socialM.lName, forKey: KUSER_LNAME)
                    UserDefaults.standard.set(fullName, forKey: KUSER_NAME)
                    UserDefaults.standard.set(socialM.phoneNumber, forKey: KUSER_PHONENUMBER)
                    UserDefaults.standard.set(socialM.email, forKey: KUSER_EMAIL)
                    UserDefaults.standard.set(true, forKey: IS_LOGIN)
                    
                    if socialM.type == "fb"
                    {
                        UserDefaults.standard.set(true, forKey: "socialLoginType")
                        
                    }else
                    {
                        UserDefaults.standard.set(false, forKey: "socialLoginType")
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        AppDelegate.getSharedInstance().configDynamicShortcutItems()
                        self.skipEvent(self)
                        
                        if self.strOtherDestination.characters.count > 0
                        {
                            self.delegate?.afterLoginChangeDestination(strDestination: self.strOtherDestination)
                        }
                        //self.delegate?.afterLoginChangeDestination(strDestination: "0")
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        if socialM.type == "fb"
                        {
                            self.logoutWithSocialId(bSocialType: true)
                        }
                        else
                        {
                            self.logoutWithSocialId(bSocialType: false)
                            
                        }
                        
                        let alertController = UIAlertController(title: "Myles", message: socialResponse.message ?? "Some thing went wrong please try after some time", preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        })
                        
                        
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            })
            
        }
    }*/
    
    
   @objc public func logoutWithSocialId(bSocialType : Bool )
    {
        if bSocialType
        {
            let lManager = LoginManager()
                lManager.logOut()
            
        }
        else
        {
            GIDSignIn.sharedInstance().signOut()
        }
    }
    
    

}
@IBDesignable
class FormTextField: UITextField {
    
    @IBInspectable var inset: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: inset)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
}
