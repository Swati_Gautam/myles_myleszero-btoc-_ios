//
//  TermsConditionCell.swift
//  Myles
//
//  Created by Itteam2 on 20/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

class TermsConditionCell: UITableViewCell {

    
    @IBOutlet weak var createAccountLabel: UILabel!
    
    @IBOutlet weak var btnTerms: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        createAccountLabel.shadowColor = UIColor.black
        createAccountLabel.shadowOffset = CGSize.zero
        createAccountLabel.layer.shadowRadius = 5.0
        createAccountLabel.layer.shadowOpacity = 1.0
        createAccountLabel.layer.masksToBounds = false
        createAccountLabel.layer.shouldRasterize = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
