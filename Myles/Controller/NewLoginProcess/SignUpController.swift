//
//  SignUpController.swift
//  Myles
//
//  Created by Itteam2 on 23/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

class SignUpController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
  
    @IBOutlet weak var tblSignUp: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "TextFieldForLoginRegistrationCell"
        var cell: TextFieldForLoginRegistrationCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldForLoginRegistrationCell
        if cell == nil {
            tableView.register(UINib(nibName: "TextFieldForLoginRegistrationCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldForLoginRegistrationCell
        }
        cell.txtField.tag = indexPath.row
        cell.txtField.keyboardType = .alphabet
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let identifier = "TermsConditionCell"
        var cell: TermsConditionCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TermsConditionCell
        if cell == nil {
            tableView.register(UINib(nibName: "TermsConditionCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TermsConditionCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 108
    }
    
//    func tx(<#parameters#>) -> <#return type#> {
//        <#function body#>
//    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
