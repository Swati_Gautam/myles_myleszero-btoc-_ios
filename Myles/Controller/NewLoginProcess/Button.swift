//
//  Button.swift
//  Myles
//
//  Created by iOS Team on 25/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation

@objc class Button: UIButton {
    
    typealias DidTapButton = (Button) -> ()
    
    @objc var didTouchUpInside: DidTapButton? {
        didSet {
            if didTouchUpInside != nil {
                addTarget(self, action: #selector(didTouchUpInside(_:)), for: .touchUpInside)
            } else {
                removeTarget(self, action: #selector(didTouchUpInside(_:)), for: .touchUpInside)
            }
        }
    }
    
    // MARK: - Actions
    
    @objc func didTouchUpInside(_ sender: UIButton) {
        if let handler = didTouchUpInside {
            handler(self)
        }
    }
}
