//
//  CarPackageCollectionViewCell.h
//  Myles
//
//  Created by Nowfal E Salam on 26/10/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarPackageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *kilometerLabel;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIView *subContentView;

@end
