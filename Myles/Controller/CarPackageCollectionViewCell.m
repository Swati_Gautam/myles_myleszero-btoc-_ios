//
//  CarPackageCollectionViewCell.m
//  Myles
//
//  Created by Nowfal E Salam on 26/10/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

#import "CarPackageCollectionViewCell.h"

@implementation CarPackageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _mainContentView.layer.cornerRadius = 3.0f;
    _subContentView.layer.cornerRadius = 3.0f;
}
- (void)setSelected:(BOOL)selected
{
    if(selected)
    {
        _priceLabel.textColor = [UIColor blackColor];
        _kilometerLabel.textColor = [UIColor darkGrayColor];
        UIColor *color = UICOLOR_ORANGE;
        _priceLabel.textColor = color;
        _kilometerLabel.textColor = color;
        //UIColorFromRedGreenBlue(251, 50, 36);
        _mainContentView.backgroundColor = color;
        _subContentView.alpha = 1.0;
    }
    else
    {
        _priceLabel.textColor = [UIColor lightGrayColor];
        _kilometerLabel.textColor = [UIColor lightGrayColor];
        _mainContentView.backgroundColor = UIColor.lightGrayColor;
        _subContentView.alpha = 1.0;

    }
}
@end
