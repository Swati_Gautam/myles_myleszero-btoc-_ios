//
//  DocumentUploadVC.m
//  Myles
//
//  Created by Myles   on 05/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "DocumentUploadVC.h"
#import <Google/Analytics.h>
#import "HomeViewController.h"
#import "MenuViewController.h"

#define ACCEPTABLE_CHARECTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\./-"

#define ACCEPTABLE_CHARECTERS_CITY @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

@interface DocumentUploadVC ()

@end

@implementation DocumentUploadVC

{
    NSString *expiryDate;
    BOOL docStatus;
    NSString *dobStr ;
}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   // self.navigationController.navigationBarHidden = true;
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Upload Documents"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [CommonFunctions setLeftPaddingToTextField:self.documentExpireTxtfd andPadding:5];
    [CommonFunctions setLeftPaddingToTextField:self.documentNumberTxtfd andPadding:5];
    [CommonFunctions setLeftPaddingToTextField:self.issueCityTxtfd andPadding:5];
    docStatus = false;
    
    [self InitialSetUp];
}



- (void)InitialSetUp
{
    /*
     [self.passportBtn setTag:10];
     [self.votarCardBtn setTag: 2];
     self.adharCradBtn.tag = 3;
     self.drivingBtn.tag = 1;
     */
    self.confirmBtn.layer.cornerRadius = 2;
    self.confirmBtn.layer.shadowOffset = CGSizeMake(1.3, 3);
    self.confirmBtn.layer.shadowOpacity = 0.2;
    
    //uploaded Document Image
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(uploadDocumentAction:)];
    self.uploaddocumentImg.userInteractionEnabled = true;
    [self.uploaddocumentImg addGestureRecognizer:tapgesture];
    //textfield Delegate
    self.documentExpireTxtfd.delegate = self;
    self.documentNumberTxtfd.delegate = self;
    self.nameTextField.delegate = self;
    self.issueCityTxtfd.delegate = self;
    self.dobTextField.delegate = self;
    self.documentExpireTxtfd.autocorrectionType = UITextAutocorrectionTypeNo;
    self.documentNumberTxtfd.autocorrectionType = UITextAutocorrectionTypeNo;
    self.issueCityTxtfd.autocorrectionType = UITextAutocorrectionTypeNo;
    self.dobTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [CommonFunctions setLeftPaddingToTextField:self.documentExpireTxtfd andPadding:15];
    
    // self.documentNumberTxtfd.attributedPlaceholder = EmailPlaceholderIs;
    [CommonFunctions setLeftPaddingToTextField:self.documentNumberTxtfd andPadding:15];
    
    [CommonFunctions setLeftPaddingToTextField:self.issueCityTxtfd andPadding:15];
    [CommonFunctions setLeftPaddingToTextField:self.dobTextField andPadding:15];
    [CommonFunctions setLeftPaddingToTextField:self.nameTextField andPadding:15];

     UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    //Date Picker
    datePicker = [[UIDatePicker alloc] init];
    
    [datePicker setMinimumDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    //Add Action
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    //Set Textfield Input mode
    if ([_documentType containsString: @"Aadhar"]){
        self.documentNumberTxtfd.placeholder = @"Aadhar Number";
        self.documentExpireTxtfd.placeholder = @"Aadhar Name";
        self.issueCityTxtfd.placeholder = @"Aadhar Issue City";
        self.dobTextField.placeholder = @"Aadhar DOB";
        self.nameTextField.placeholder = @"Aadhar Name";
        self.documentExpireTxtfd.keyboardType = UIKeyboardTypeDefault;
    }
     else if ([_documentType containsString: @"Passport"]){
         self.documentNumberTxtfd.placeholder = @"Passport Number";
         self.documentExpireTxtfd.placeholder = @"Passport Expiry Date";
         self.issueCityTxtfd.placeholder = @"Passport Issue City";
         self.dobTextField.placeholder = @"Passport DOB";
         self.nameTextField.placeholder = @"Passport Name";
         self.documentExpireTxtfd.inputView = datePicker;

    }
     else if ([_documentType containsString:@"DL"]){
         self.documentNumberTxtfd.placeholder = @"DL Number";
         self.documentExpireTxtfd.placeholder = @"DL Expiry Date";
         self.issueCityTxtfd.placeholder = @"DL Issue City";
         self.dobTextField.placeholder = @"DL DOB";
         self.nameTextField.placeholder = @"DL Name";
         self.documentExpireTxtfd.inputView = datePicker;

     }
    else{
    self.documentExpireTxtfd.inputView = datePicker;
    
    }
    
    self.documentExpireTxtfd.inputAccessoryView = toolbar;
    self.documentNumberTxtfd.inputAccessoryView = toolbar;
    self.nameTextField.inputAccessoryView = toolbar;
    self.issueCityTxtfd.inputAccessoryView = toolbar;
    //Date Picker
    datePicker2 = [[UIDatePicker alloc] init];
    
    datePicker2.datePickerMode = UIDatePickerModeDate;
    //Add Action
    [datePicker2 addTarget:self action:@selector(datePickerValueChangedDOB:) forControlEvents:UIControlEventValueChanged];
    
    //Adding Action on dobtext
    self.dobTextField.inputView = datePicker2;
    self.dobTextField.inputAccessoryView = toolbar;
    
    //Get Current date
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSString *currentdateString = [NSString stringWithFormat:@"%ld.%ld.%ld", (long)[components day], (long)[components month], (long)[components year]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *currentdate = [dateFormatter dateFromString:currentdateString];
    //Set Minimum Date
    //     self.documentExpireTxtfd.text = [dateFormatter stringFromDate:currentdate];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    expiryDate = [dateFormatter stringFromDate:currentdate];
    datePicker.minimumDate = currentdate;
    
    if ([self.documentStatus isEqualToString:@"4"]) {
        NSString *name = [DEFAULTS valueForKey: KUSER_NAME] != nil ? [DEFAULTS valueForKey: KUSER_NAME] : @"";
        dobStr = @"";
        self.nameTextField.text = name;
        [self.nameTextField setHidden:NO];
        [self.documentExpireTxtfd setHidden:YES];
        [self.issueCityTxtfd setHidden:YES];
        [self.dobTextField setHidden:YES];
    } else {
        
        [self.nameTextField setHidden:YES];
        [self.documentExpireTxtfd setHidden:NO];
        [self.issueCityTxtfd setHidden:NO];
        [self.dobTextField setHidden:NO];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismiss
{
    [self.documentExpireTxtfd resignFirstResponder];
    [self.dobTextField resignFirstResponder];
}
- (void) datePickerValueChangedDOB : (UIDatePicker *)sender
{
    
    
    NSDate *selectedDate = [sender date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *date = [dateFormat stringFromDate:selectedDate];
    self.dobTextField.text = date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dobStr = [dateFormat stringFromDate:selectedDate];
}

- (void) datePickerValueChanged : (UIDatePicker *)sender
{
    
    [sender setMinimumDate:[NSDate date]];
    
    NSDate *selectedDate = [sender date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *date = [dateFormat stringFromDate:selectedDate];
    self.documentExpireTxtfd.text = date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    expiryDate = [dateFormat stringFromDate:selectedDate];
}

/*
 #pragma mark - Navigation
  // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma -mark UITextFieldDelagate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:self.dobTextField]){

        NSDate *currentDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                           value:0
                                                                          toDate:[NSDate date]
                                                                         options:0];
        
        [datePicker2 setMaximumDate
         : currentDate];
    }
    
    [self animateViewMoving:true Value:210];
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.dobTextField]){
        NSDate *selectedDate = [datePicker2 date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        NSString *date = [dateFormat stringFromDate:selectedDate];
        self.dobTextField.text = date;
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        dobStr = [dateFormat stringFromDate:selectedDate];
    }

    if ([textField isEqual:self.documentExpireTxtfd])
    {
        if (![_documentType containsString:@"Aadhar"]){
        NSDate *selectedDate = [datePicker date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        NSString *date = [dateFormat stringFromDate:selectedDate];
        self.documentExpireTxtfd.text = date;
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        expiryDate = [dateFormat stringFromDate:selectedDate];
        }
    } else if ([textField isEqual:self.dobTextField]) {

    }

    [self animateViewMoving:false Value:210];

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == _documentNumberTxtfd ) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS_CITY] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
}



#pragma -mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage* image = info[UIImagePickerControllerOriginalImage];
    self.uploaddocumentImg.image = image;
    docStatus = true;
    [picker dismissViewControllerAnimated:true completion:nil];
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - Upload Document Button
- (IBAction)uploadDocumentAction:(id )sender
{
    
    [self resignresponsers];
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
   
    UIAlertController *alertController = [UIAlertController
                                          
                                          alertControllerWithTitle:@"Upload Document"
                                          
                                          message:nil
                                          
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    
    
    UIAlertAction *Camera = [UIAlertAction
                               
                               actionWithTitle:NSLocalizedString(@"Camera", @"Camera Action")
                               
                               style:UIAlertActionStyleDefault
                               
                               handler:^(UIAlertAction *action)
                               
                               {
                                   
                                   //NSLog(@"OK action");
                                   picker.sourceType = UIImagePickerControllerSourceTypeCamera;

                                   [self dismissViewControllerAnimated:YES completion:nil];
                                       [self presentViewController:picker animated:YES completion:nil];
                                   
                                   

                                   
                               }];
    
    UIAlertAction *PhotoLibary = [UIAlertAction
                               
                               actionWithTitle:NSLocalizedString(@"Photo Libary", @"Photo Libary Action")
                               
                               style:UIAlertActionStyleDefault
                               
                               handler:^(UIAlertAction *action)
                               
                               {
                                   
                                   //NSLog(@"OK action");
                                   picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   [self presentViewController:picker animated:YES completion:nil];
                                   
                               }];

    
    
    UIAlertAction *Cancel = [UIAlertAction
                                  
                                  actionWithTitle:NSLocalizedString(@"Cancel ", @"Cancel Action")
                                  
                                  style:UIAlertActionStyleCancel
                                  
                                  handler:^(UIAlertAction *action)
                                  
                                  {
                                      
                                
                                      
                                      [self dismissViewControllerAnimated:YES completion:nil];
                                  }];

    
    
    
    [alertController addAction:Camera];
    [alertController addAction:PhotoLibary];
    [alertController addAction:Cancel];

    
    [self presentViewController:alertController animated:YES completion:nil];

    
    
}
#pragma mark - Confirm Button
- (IBAction)confirmBtnAction:(UIButton *)sender
{
    if ([self textFieldValidation])
    {
        
       // self.uploaddocumentImg.image = [self resizeImage:self.uploaddocumentImg.image];
        NSData *pngData = UIImageJPEGRepresentation(self.uploaddocumentImg.image, .5);
        
        //NSData *pngData = UIImageJPEGRepresentation(self.uploaddocumentImg.image, 0.25);
        NSLog(@"pndgdata length %lu",(unsigned long)[pngData length]);
        
        //NSData *pngData = UIImageJPEGRepresentation(self.uploaddocumentImg.image, 0.5);
        long long milliseconds = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
        NSString *randomNumber = [NSString stringWithFormat:@"%lld",milliseconds];
        self.documentName = [NSString stringWithFormat:@"%@_%@_P.jpg",[DEFAULTS valueForKey:KUSER_ID],[randomNumber substringFromIndex:randomNumber.length - 5]];
        [self resignresponsers];
        
        NSString *str = [CommonFunctions encodeToBase64String:pngData];
        
        
        NSLog(@"Image Str str =%@",str);
        //issueCityTxtfd.text
       // [self postDocumentToserver:self.documentNumberTxtfd.text ExpiryDate:expiryDate City:@"Delhi" FileName:self.documentName Status:self.documentStatus image:str];
        [self postDocumentToserver:self.documentNumberTxtfd.text ExpiryDate:expiryDate City:self.issueCityTxtfd.text FileName:self.documentName Status:self.documentStatus image:str];
    }
    
}
-(void)alertView:(NSString *)strMessage withTitle:(NSString *)titleStr
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleStr message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
/*
 Code to reduce image size - By Divya
 */

-(UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

#pragma mark- Document Path
- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSString *documentsPath = [CommonFunctions documentsDirectory];
    return [documentsPath stringByAppendingPathComponent:name];
}

#pragma mark- Cancel Button
- (IBAction)cancleBtnAction:(UIButton *)sender
{
    [self resignresponsers];
    [self.navigationController popViewControllerAnimated:TRUE];
}


- (void)resignresponsers
{
    [self.documentNumberTxtfd resignFirstResponder];
    [self.documentExpireTxtfd resignFirstResponder];
}

#pragma mark- Text Validation
- (BOOL)textFieldValidation
{
    BOOL state  = true;
    
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];

    if (!docStatus)
    {
        [self alertView:@"Please upload Image First" withTitle:KSorry];
        state  = false;
    }
    if ([self.documentStatus isEqualToString:@"4"]) {
        
        if (self.documentNumberTxtfd.text.length == 0) {
            [self alertView:kEnterDocumentNumber withTitle:KSorry];
            state  = false;
        }
    }else {
            if ([[self.documentNumberTxtfd.text stringByTrimmingCharactersInSet: set] length] == 0) {
                [self alertView:kEnterDocumentNumber withTitle:KSorry];

//                [CommonFunctions alertTitle:KSorry withMessage:kEnterDocumentNumber];
                state  = false;
                
            }
            else if ([[self.self.documentExpireTxtfd.text stringByTrimmingCharactersInSet: set] length] == 0)
            {
                [self alertView:kEnterDocumentExpireDate withTitle:KSorry];

//                [CommonFunctions alertTitle:KSorry withMessage:kEnterDocumentExpireDate];
                state  = false;
                
            }
            else if ( [[self.issueCityTxtfd.text stringByTrimmingCharactersInSet: set] length] == 0)   {
                [self alertView:kEnterDocumentIssueCity withTitle:KSorry];

//                [CommonFunctions alertTitle:KSorry withMessage:kEnterDocumentIssueCity];
                state  = false;
            }
            else if ([[self.dobTextField.text stringByTrimmingCharactersInSet: set] length] == 0)
            {
                [self alertView:@"Please enter DOB" withTitle:KSorry];

//                [CommonFunctions alertTitle:KSorry withMessage:@"Please enter DOB"];
                state  = false;
            }
        }

    return state;
}

#pragma mark- Post Document


- (void)postDocumentToserver : (NSString *)idno  ExpiryDate : (NSString *)expirydate City : (NSString *)issueCity FileName : (NSString *)name Status : (NSString *)docstatus image : (NSString *)imagebase64
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
        [body setObject: userid forKey:@"userid"];
        [body setObject: idno forKey:@"idno"];
        [body setObject: name forKey:@"fileName"];
        [body setObject: docstatus forKey:@"status"];
        [body setObject: imagebase64 forKey:@"filecontent"];
        if ([docstatus isEqualToString:@"4"] ){
            [body setObject:@"" forKey:@"expirydate"];
        } else {
            [body setObject: expirydate != nil ? expirydate : @"" forKey:@"expirydate"];
        }
        [body setObject: issueCity forKey:@"issuecity"];
        if([dobStr length] > 0){
            [body setObject:dobStr forKey:@"DOBOnID"];
        } else {
            [body setObject:@"" forKey:@"DOBOnID"];
        }
        NSString *name = self.nameTextField.text;
        [body setObject:name.length > 0 ? name : @"" forKey:@"NameOnID"];
        
        
        NSLog(@"body = %@",body);
        //RestM1uploadDocument(string userid, string idno, string expirydate, string issuecity, string fileName, string status, string filecontent, string NameOnID, string DOBOnID)


//        NSString *urlStr = @"";
//        if ([self.documentStatus isEqualToString:@"4"]){
//            urlStr = kPanCardUpload;
//        } else {
//            urlStr = KuploadDocument;
//        }
       // NSLog(@"body : %@",body);
        
        [self showLoader];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KuploadDocument completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD dismiss];
                                                 });
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 
                                                 NSLog(@"response header =%@",response);
                                                 NSLog(@"statusStr header =%@",statusStr);


                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     if (!error)
                                                     {
                                                         NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             NSInteger status = [[responseJson valueForKey:@"status"]  integerValue];
                                                             if (status == 1 ) {
                                                                 NSArray *array = [self.navigationController viewControllers];
                                                                 NSInteger count = array.count;
                                                                 count = count - 2;
                                                                 UserDocumentUploadVC *userDocUpload = (UserDocumentUploadVC *)[array objectAtIndex:count];
                                                                 NSString *key;

                                                                 if ([self.documentStatus isEqualToString:@"4"]){
                                                                     key = [self getKeyForTrackUserDoc:@"4"];
                                                                 } else {
                                                                     key = [self getKeyForTrackUserDoc:docstatus];
                                                                 }
                                                                 [userDocUpload.docDic setObject:@"1" forKey:key];
                                                                 [self.navigationController popViewControllerAnimated:TRUE];
                                                                 [CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                             }
                                                             else
                                                             {
                                                                 [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                             }
                                                             
                                                         });
                                                     }
                                                     else
                                                     {
                                                         dispatch_async(dispatch_get_main_queue(), ^{

                                                             [CommonFunctions alertTitle:@"" withMessage:serviceTimeoutUnabletoreach];
                                                         });
                                                         
                                                     }
                                                 } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired,  Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                         
                                                         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                             [CommonFunctions logoutcommonFunction];
                                                             UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                                             UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                                                             HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
                                                             navController.viewControllers = @[homeController];
                                                             [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:0];
                                                             [(MenuViewController *)self.revealViewController.rearViewController setIndexValue:0];
                                                             [[(MenuViewController *)self.revealViewController.rearViewController menuTable] reloadData];
                                                             
                                                             [self.revealViewController pushFrontViewController:navController animated:true];
                                                             [self.revealViewController revealToggleAnimated:true];
                                                             
                                                         }];
                                                         [alertController addAction:ok];
                                                         
                                                         [self presentViewController:alertController animated:YES completion:nil];
                                                     });
                                                     
                                                 } else
                                                 {
                                                     
                                                 }
                                             }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}

#pragma mark - GetKey
- (NSString *)getKeyForTrackUserDoc : (NSString *)status
{
    NSString *key;
    if ([status isEqualToString:@"0"])
    {
        key = k_PASSPORT;
    }
    else
    {
        if ([status isEqualToString:@"1"])
        {
            key = k_DL;
        }
        else if ([status isEqualToString:@"4"])
        {
            key = k_PANCARD;
        }
        else
        {
            key = k_ADHAR;
        }
    }
    return key;
    
}


@end
