//
//  UserDocumentUploadVC.m
//  Myles
//
//  Created by Myles   on 12/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "UserDocumentUploadVC.h"
#import <Google/Analytics.h>


@interface UserDocumentUploadVC ()

@end

@implementation UserDocumentUploadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Upload Documents"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    self.view.frame = CGRectMake(0, 0, CGRectGetWidth([[UIScreen mainScreen] bounds]), CGRectGetHeight([[UIScreen mainScreen] bounds]));
    [self customizeBackButton:false];
   
    
    [self.passportBtn setTag:10];
    
    
    [self.votarCardBtn setTag: 2];
    [self.votarCardBtn setTag: 4];
    self.adharCradBtn.tag = 3;
   
    
    self.drivingBtn.tag = 1;
    [self.passportBtn setBackgroundImage:[UIImage imageNamed:k_unseletedDocumentBtnImg] forState:UIControlStateNormal];
    [self.votarCardBtn setBackgroundImage:[UIImage imageNamed:k_unseletedDocumentBtnImg] forState:UIControlStateNormal];
    [self.adharCradBtn setBackgroundImage:[UIImage imageNamed:k_unseletedDocumentBtnImg] forState:UIControlStateNormal];
    [self.drivingBtn setBackgroundImage:[UIImage imageNamed:k_unseletedDocumentBtnImg] forState:UIControlStateNormal];
    [self.drivingBtn addTarget:self action:@selector(attachedDocumentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self InitilseViewSetUp];
    
    for (NSString *str in self.docDic) {
        NSInteger value = [[self.docDic valueForKey:str] integerValue];
            //NSLog(@"Document Status : %ld",(long)value );
        if (value == 1)
        {
            if ([str isEqualToString:k_PANCARD])
                [self.votarCardBtn setBackgroundImage:[UIImage imageNamed:k_seletedDocumentBtnImg] forState:UIControlStateNormal];
            else
                if ([str isEqualToString:k_DL])
                    [self.drivingBtn setBackgroundImage:[UIImage imageNamed:k_seletedDocumentBtnImg] forState:UIControlStateNormal];
                else if ([str isEqualToString:k_PASSPORT])
                    [self.passportBtn setBackgroundImage:[UIImage imageNamed:k_seletedDocumentBtnImg] forState:UIControlStateNormal];
                else
                    [self.adharCradBtn setBackgroundImage:[UIImage imageNamed:k_seletedDocumentBtnImg] forState:UIControlStateNormal];
        }        
        
    }
}

- (void)InitilseViewSetUp
{
    self.submitBtn.layer.cornerRadius = 2;
    self.submitBtn.layer.shadowOffset = CGSizeMake(1.3, 3);
    self.submitBtn.layer.shadowOpacity = 0.2;
    NSString *documentPath = [CommonFunctions documentsDirectory];
    NSInteger tag = (self.documentStatus.length <= 0) ? -1 : [self.documentStatus integerValue] ;
    if (tag  == 0) {
        self.documentStatus = @"10";
        
        
    }
    UIButton *btnselected;
    if (tag > 0) {
        btnselected = (UIButton *)[self.buttonSuperview viewWithTag:tag];
        
    }
    if ([CommonFunctions documentExistwithName:@"Document1"])
    {
        
    }
    
    if ((documentPath.length > 0) && (btnselected != nil))
    {
        [btnselected setSelected:true];
    }
    else
    {
        [self.passportBtn setSelected:false];
        [self.votarCardBtn setSelected:false];
        [self.adharCradBtn setSelected:false];
        
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)submitBtnAction:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)attachedDocumentBtnAction:(UIButton *)sender
{
    if (sender.isSelected)
    {
        [self movetoUploaddocVC:sender.tag];
    }else if (self.documentStatus.length == 0)
    {
        [self movetoUploaddocVC:sender.tag];
    }
}
- (void) movetoUploaddocVC : (NSInteger )document
{
    
    
    if (document == 10)
    {
        document = 0;
    }
    
    
    if (document == 2){
        
        NSLog(@"Load pan card view controller ");
        
        
    }
    else {
    
   
    DocumentUploadVC *documentVC = [self.storyboard instantiateViewControllerWithIdentifier:k_DocumentUploadController ];
    documentVC.documentStatus = [NSString stringWithFormat:@"%ld",(long)document];
        if (document == 3){
            documentVC.documentType = @"Aadhar";
        }
        else if (document == 0){
            documentVC.documentType =@"Passport";
        }
        else if (document == 1){
            documentVC.documentType = @"DL";
        }
    [self.navigationController pushViewController:documentVC animated:true];
    }
}
@end
