//
//  UserProfileVC.m
//  Myles
//
//  Created by Myles   on 13/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "UserProfileVC.h"
#import "UserProfileCustomCell.h"
#import "UserInfo.h"
#import "SorryAlertViewController.h"
#import <Google/Analytics.h>
#import "NSString+MD5.h"

static NSString *associationKey = @"associationKey";
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678909)(."
#define ACCEPTABLE_CHARACTERS_EMAIL @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678909)(@."


@interface UserProfileVC ()
{
    UserInfoResponse *userDictionary;
    NSMutableDictionary *updatedInfo;
    NSString *userfullname;
    NSString *currentPassword,*newPassword,*confirmPassword;
    NSString *userpassword;
    UIView *overlappedView;
    UIView *otherOverLappedView;
    BOOL editmode;
    UIView *snapshot;
    NSMutableDictionary *documentStatusdic;
    NSString *deletedfilename ;
    UIButton *rightbarButton;
    UITextField *kCurrentTF;
    NSString *strDob ;
    BOOL boolVal;
}
//@property (nonatomic) UIBarButtonItem* revealButtonItem;
@end

@implementation UserProfileVC

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _editBtn.layer.cornerRadius = 4.0f;
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"My Profile"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    editmode = false;
    documentStatusdic = [[NSMutableDictionary alloc]initWithObjects:@[@0,@0,@0,@0] forKeys:@[k_PANCARD,k_DL,k_PASSPORT,k_ADHAR]];
    otherOverLappedView.backgroundColor = [UIColor whiteColor];
    updatedInfo = [[NSMutableDictionary alloc]init];
    [self.view addSubview:otherOverLappedView];
    
    // Menu Button
    //[self customizeLeftRightNavigationBar:@"My Profile" RightTItle:@""Controller:self Present:true];
    //set the title of edit button
    //[self.editBtn addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.editBtn setTitle:@"EDIT" forState:UIControlStateNormal];
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SavePassword:) name:@"PasswordChanged" object:nil];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (![DEFAULTS boolForKey:IS_LOGIN]) {
        self.tableView.hidden = true;

        UIAlertController *alertController = [UIAlertController
                                              
                                              alertControllerWithTitle:@"MYLES"
                                              
                                              message:@"Please login to check your profile"
                                              
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        
        UIAlertAction *loginAction = [UIAlertAction
                                   
                                   actionWithTitle:@"Login"
                                   style:UIAlertActionStyleDefault
                                   
                                   handler:^(UIAlertAction *action)
                                   
                                   {
                                       UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
                                       LoginProcess *editController = (LoginProcess *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginProcess"];
                                       
                                       UINavigationController *navigationController =
                                       [[UINavigationController alloc] initWithRootViewController:editController];
                                       //editController.delegate = self;
            
                                    
                                       editController.strOtherDestination = @"0"; //Swati Commented This 13 June 
                                       navigationController.viewControllers = @[editController];
                                       [self presentViewController:navigationController animated:YES completion:nil];
                                   }];
        [alertController addAction:loginAction];
        
        UIAlertAction *searchCarAction = [UIAlertAction
                                      
                                      actionWithTitle:@"Search Car"
                                      style:UIAlertActionStyleDefault
                                      
                                      handler:^(UIAlertAction *action)
                                      
                                      {
                                          self.tabBarController.selectedIndex = 0;
                                      }];
        [alertController addAction:searchCarAction];
        
        [self.tabBarController presentViewController:alertController animated:YES completion:nil];
        //[self presentViewController:alertController animated:YES completion:nil];
        
        
        
        
    } else{
        [super viewWillAppear:YES];
        self.navigationController.navigationBarHidden = false;
        //self.navigationController.navigationBar.translucent = NO;
        self.tableView.hidden = true;
        // ////NSLog(@"%@,%@,%@,%@",[DEFAULTS objectForKey:KUSER_ID],[DEFAULTS objectForKey:KUSER_PHONENUMBER],[DEFAULTS objectForKey:KUSER_EMAIL],[DEFAULTS objectForKey:KUSER_NAME]);
        [self fetchUserProfile];
        if (!editmode)
        {
            [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
        }

        
    }
}
- (void)fetchUserProfile
{
    if ([CommonFunctions reachabiltyCheck]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLoader];
        });
        NSMutableDictionary *body = [NSMutableDictionary new];
        NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
        [body setObject: userid forKey:@"userId"];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSERINFO_FATCH completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   NSLog(@"statusStr =%@",statusStr);
                                                   
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           NSLog(@"RES : %@",responseJson);
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                                                               if (status == 1)
                                                               {
                                                                   UserInfo *userInfo = [[UserInfo alloc]initWithDictionary:responseJson error:nil];
                                                                   
                                                                   userDictionary = (UserInfoResponse *) userInfo.response ;
                                                                   
                                                                   userDictionary.FName = [NSString stringWithFormat:@"%@", userDictionary.FName];
                                                                   
                                                                   userDictionary.LName = [NSString stringWithFormat:@"%@", userDictionary.LName];
                                                                   
                                                                   userDictionary.Dob = [NSString stringWithFormat:@"%@", userDictionary.Dob];
                                                                   
                                                                   if (userDictionary.Dob != nil && userDictionary.Dob.length>0 && ![userDictionary.Dob isEqualToString:@"1/1/1900 12:00:00 AM"]) {
                                                                       [DEFAULTS setValue:userDictionary.Dob forKey:@"dob"];
                                                                   }
                                                                   
                                                                   userDictionary.password = [CommonFunctions getKeychainPassword:[DEFAULTS valueForKey:KUSER_PHONENUMBER]];
                                                                   
                                                                   
                                                                   
                                                                   userpassword = [NSString stringWithFormat:@"%@", userDictionary.password];
                                                                   
                                                                   self.tableView.hidden = false;
                                                                   
                                                                   [self.tableView reloadData];
                                                               }
                                                               else
                                                               {
                                                                   [self AlertView:responseJson[k_ApiMessageKey]];
//                                                                   [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                               }
                                                               
                                                           });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{

                                                               [self AlertView:serviceTimeoutUnabletoreach];
                                                           });
                                                       }
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired, Please Login to continue" preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               //[self.revealViewController revealToggleAnimated:true];
                                                               UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                                               UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                                                               HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
                                                               navController.viewControllers = @[homeController];
                                                               [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:0];
                                                               [(MenuViewController *)self.revealViewController.rearViewController setIndexValue:0];
                                                               [[(MenuViewController *)self.revealViewController.rearViewController menuTable] reloadData];
                                                               
                                                               [self.revealViewController pushFrontViewController:navController animated:true];
                                                               [self.revealViewController revealToggleAnimated:true];

                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });
                                                   } else
                                                   {
                                                       
                                                   }
                                                   
                                            }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
       [self AlertView:KAInternet];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void )animationView
{
    /*if ([CommonFunctions getMajorSystemVersion] >= 7)
    {
        // Taking snapshot of screen on edit action
        snapshot = [self.tableView snapshotViewAfterScreenUpdates:false];
        if (editmode)
        {
            snapshot.frame = CGRectMake(CGRectGetMaxX(self.view.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.view.frame),  CGRectGetHeight(self.tableView.frame) );
        }
        else
        {
            snapshot.frame = CGRectMake(CGRectGetMinX(self.view.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.tableView.frame));
        }
        [self.view addSubview:snapshot];
        [UIView animateKeyframesWithDuration:1.0 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
            [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
                if (editmode)
                {
                    snapshot.frame = CGRectMake(0, CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(snapshot.frame));
                }
                else
                {
                    snapshot.frame = CGRectMake(CGRectGetMaxX(self.view.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(snapshot.frame));
                }
            }];
        } completion:^(BOOL finished) {
            [snapshot removeFromSuperview];
            
            if (editmode) {
                
                [self.tableView reloadData];
            }
            
        }];
    }*/
    if (editmode) {
        
        [self.tableView reloadData];
    }
}

#pragma -mark Right Button


//- (void)resignKeyboard{
//    
//    [self.tableView endEditing:YES];
//}

- (void)leftBtnTouchedPop
{
    if (editmode) {
        editmode = false;
        [self saveUserDetails:nil];
    }
    else
    {
        //after hiding navigational right EDIT button
        // [self rightButton : nil];
    }
}

- (void)rightButton :(UIButton *)sender
{
    
    
}


//bottom edit buton to edit form details


- (IBAction)editButtonAction:(UIButton *)sender{
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Edit My Profile"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if (!self.tableView.isHidden)
    {
        if (![sender.titleLabel.text isEqualToString:@"EDIT"]) {
            
            [self.tableView reloadData];
        }
        [sender setTitle:@"SAVE" forState:UIControlStateNormal];
        
        editmode = true;
        // Remove Target
        [sender addTarget:self action:@selector(saveUserDetails:) forControlEvents:UIControlEventTouchUpInside];
        
        self.leftmenuButton.hidden = true;
        self.leftbackButton.hidden = false;
        // Animation
        [self animationView];
        [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}




-(void)dataRefresh
{
    editmode = YES;
    NSIndexPath *index1 =  [NSIndexPath indexPathForItem:0 inSection:0];
    UserProfileCustomCell *cell = (UserProfileCustomCell*) [self.tableView cellForRowAtIndexPath:index1];
    //NSLog(@"%@,%@,%@,%@",[DEFAULTS objectForKey:KUSER_ID],[DEFAULTS objectForKey:KUSER_PHONENUMBER],[DEFAULTS objectForKey:KUSER_EMAIL],[DEFAULTS objectForKey:KUSER_NAME]);
    
    [updatedInfo setValue:[NSString stringWithFormat:@"%@",cell.userFirstNametxt.text] forKey:@"FName"];
    [updatedInfo setValue:[NSString stringWithFormat:@"%@",cell.userLastNametxt.text] forKey:@"LName"];
    [updatedInfo setValue:[NSString stringWithFormat:@"%@%@",cell.userFirstNametxt.text,cell.userLastNametxt.text] forKey:@"fullName"];
    
    index1 = [NSIndexPath indexPathForItem:1 inSection:0];
    cell = (UserProfileCustomCell*) [self.tableView cellForRowAtIndexPath:index1];
    [updatedInfo setValue:[NSString stringWithFormat:@"%@",cell.userEmailIdtxt.text] forKey:@"emailId"];
    
    
    index1 = [NSIndexPath indexPathForItem:2 inSection:0];
    cell = (UserProfileCustomCell*) [self.tableView cellForRowAtIndexPath:index1];
    [updatedInfo setValue:[NSString stringWithFormat:@"%@",cell.btnDob.titleLabel.text] forKey:@"dob"];
    
    index1 = [NSIndexPath indexPathForItem:3 inSection:0];
    cell = (UserProfileCustomCell*) [self.tableView cellForRowAtIndexPath:index1];
    [updatedInfo setValue:[NSString stringWithFormat:@"%@",cell.userHomeAddressTxtview.text] forKey:@"ResAdd"];
    
   
    
    
    index1 = [NSIndexPath indexPathForItem:4 inSection:0];
    cell = (UserProfileCustomCell*) [self.tableView cellForRowAtIndexPath:index1];
    [updatedInfo setValue:[NSString stringWithFormat:@"%@",cell.userPasswordtxt.text] forKey:@"password"];
    
    [updatedInfo setValue:userDictionary.phoneNumber forKey:@"phoneNumber"];
    [updatedInfo setValue:userDictionary.password forKey:@"password"];
    [updatedInfo setValue:userDictionary.deviceToken forKey:@"deviceToken"];
    userDictionary = [[UserInfoResponse alloc]initWithDictionary:updatedInfo error:nil];
    [self.tableView reloadData];
}
 

- (void )saveUserDetails : (UIButton *)sender
{
    //dispatch_async(dispatch_get_main_queue(), ^{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:YES];
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self dataRefresh];
        
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Edit My Profile"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        NSLog(@"name  length= %lu",[[updatedInfo valueForKey:@"FName"] length]);
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        [updatedInfo setObject:[[updatedInfo valueForKey:@"FName"] stringByTrimmingCharactersInSet:whitespace] forKey:@"FName"];
        [updatedInfo setObject:[[updatedInfo valueForKey:@"LName"] stringByTrimmingCharactersInSet:whitespace] forKey:@"LName"];
        [updatedInfo setObject:[[updatedInfo valueForKey:@"emailId"] stringByTrimmingCharactersInSet:whitespace] forKey:@"emailId"];
        NSLog(@"name  length= %lu",[[updatedInfo valueForKey:@"FName"] length]);
        //NSLog(@"kCurrentTF.text =%@",kCurrentTF.text);
        if([[updatedInfo valueForKey:@"FName"] isEqualToString:@""]|| ([updatedInfo valueForKey:@"FName"] == nil)||[[updatedInfo valueForKey:@"emailId"] isEqualToString:@""]|| ([updatedInfo valueForKey:@"emailId"] == nil) || [[updatedInfo valueForKey:@"LName"] isEqualToString:@""]|| ([updatedInfo valueForKey:@"LName"] == nil) || ([updatedInfo valueForKey:@"dob"] == nil) || [[updatedInfo valueForKey:@"dob"] isEqualToString:@""] || [[updatedInfo valueForKey:@"dob"] isEqualToString:@"(null)"])
        {
            if([[updatedInfo valueForKey:@"FName"] isEqualToString:@""]||([updatedInfo valueForKey:@"FName"] == nil))
            {
                [self AlertView:@"Please enter First Name"];
            }
            else if([[updatedInfo valueForKey:@"emailId"] isEqualToString:@""]|| ([updatedInfo valueForKey:@"emailId"] == nil)){
                [self AlertView:@"Please enter Email Id"];
            }
            else if([[updatedInfo valueForKey:@"LName"] isEqualToString:@""]||([updatedInfo valueForKey:@"LName"] == nil))
            {
                [self AlertView:@"Please enter Last Name"];
                
                //NSLog(@"-----------------------------------");
            }
            else if([[updatedInfo valueForKey:@"LName"] isEqualToString:@""]||([updatedInfo valueForKey:@"LName"] == nil))
            {
                [self AlertView:@"Please enter Last Name"];
                
                //NSLog(@"-----------------------------------");
            }
            else if ( ([updatedInfo valueForKey:@"dob"] == nil) || [[updatedInfo valueForKey:@"dob"] isEqualToString:@""] || [[updatedInfo valueForKey:@"dob"] isEqualToString:@"(null)"])
            {
                [self AlertView:@"Please enter DOB"];
                
            }
        }
        else {
            if (sender != nil) {
                [kCurrentTF resignFirstResponder];
                if ([CommonFunctions reachabiltyCheck]) {
                    NSMutableDictionary *body = [NSMutableDictionary new];
                    
                    if([[updatedInfo valueForKey:@"FName"] isEqualToString:@"(null)"])
                    {
                        [updatedInfo setValue:[NSString stringWithFormat:@"%@",[DEFAULTS valueForKey:KUSER_FNAME]] forKey:@"FName"];
                        [updatedInfo setValue:[NSString stringWithFormat:@"%@",[DEFAULTS valueForKey:KUSER_LNAME]] forKey:@"LName"];
                        [updatedInfo setValue:[NSString stringWithFormat:@"%@",[DEFAULTS valueForKey:KUSER_NAME]] forKey:@"fullName"];
                        [updatedInfo setValue:[NSString stringWithFormat:@"%@",[DEFAULTS valueForKey:KUSER_EMAIL]] forKey:@"emailId"];
                    }
                    else if ([[updatedInfo valueForKey:@"emailId"] isEqualToString:@"(null)"]) {
                        [updatedInfo setValue:[NSString stringWithFormat:@"%@",[DEFAULTS valueForKey:KUSER_EMAIL]] forKey:@"emailId"];
                    }
                    //set values to the body
                    NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
                    //                NSString *userPwd = [DEFAULTS valueForKey: KUSER_PASSWORD];
                    //                NSString *md5Password=[userPwd MD5];
                    
                    
                    [body setObject: [NSString stringWithFormat:@"%@",userid] forKey:@"userid"];
                    // [body setObject: [NSString stringWithFormat:@"%@",md5Password] forKey:@"password"];
                    //                [updatedInfo setValue:[NSString stringWithFormat:@"%@ %@",[updatedInfo valueForKey:@"FName"],[updatedInfo valueForKey:@"LName"]] forKey:@"fullName"];
                    //NSLog(@"Before fullName =%@",[updatedInfo valueForKey:@"fullName"]);
                    [body setObject: [NSString stringWithString:[updatedInfo valueForKey:@"FName"]] forKey:@"fname"];
                    [body setObject: [NSString stringWithString:[updatedInfo valueForKey:@"LName"]] forKey:@"lname"];
                    [body setObject: [updatedInfo valueForKey:@"emailId"] forKey:@"emailid"];
                    [body setObject: [updatedInfo valueForKey:@"ResAdd"] forKey:@"resadd"];
                    //body setObject: @"" forKey:@"peradd"];
                    //[updatedInfo valueForKey:@"ResAdd"] forKey:@"resadd"]
                    [body setObject: [updatedInfo valueForKey:@"ResAdd"] forKey:@"peradd"];
                    [body setObject: [updatedInfo valueForKey:@"dob"] forKey:@"dob"];
                    
                    
                    [DEFAULTS setValue:[updatedInfo valueForKey:@"FName"] forKey:KUSER_FNAME];
                    [DEFAULTS setValue:[updatedInfo valueForKey:@"LName"] forKey:KUSER_LNAME];
                    [DEFAULTS setValue:[NSString stringWithFormat:@"%@, %@",[updatedInfo valueForKey:@"FName"],[updatedInfo valueForKey:@"LName"]] forKey:KUSER_NAME];
                    [DEFAULTS setValue:[updatedInfo valueForKey:@"fullName"] forKey:KUSER_NAME];
                    [DEFAULTS setValue: [updatedInfo valueForKey:@"emailId"] forKey:KUSER_EMAIL];
                    [DEFAULTS setValue:[CommonFunctions convertDate:[updatedInfo valueForKey:@"dob"]]  forKey:@"dob"];
                    [DEFAULTS synchronize];
                    
                    NSURLSession *session = [CommonFunctions defaultSession];
                    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
                    [self showLoader];
                    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSERINFO_UPDATE completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                                           {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [SVProgressHUD dismiss];
                                                               });
                                                               NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                               NSString *statusStr = [headers objectForKey:@"Status"];
                                                               if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                               {
                                                                   if (!error)
                                                                   {
                                                                       
                                                                       //NSLog(@"strData : %@",strData);
                                                                       NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                                       //NSLog(@"RES : %@",responseJson);
                                                                       
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [self AlertView:[responseJson objectForKey:@"message"]];
                                                                           [self fetchUserProfile];
                                                                       });
                                                                   }
                                                               } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                                       
                                                                       UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                                           [CommonFunctions logoutcommonFunction];
                                                                           [self.revealViewController revealToggleAnimated:true];
                                                                           
                                                                       }];
                                                                       [alertController addAction:ok];
                                                                       
                                                                       [self presentViewController:alertController animated:YES completion:nil];
                                                                   });
                                                               } else
                                                               {
                                                                   
                                                               }
                                                               
                                                               
                                                           }];
                    [globalOperation addOperation:registrationOperation];
                    editmode = NO;
                }
                else
                {
                    editmode = NO;
                    [self AlertView:KAInternet];
                }
            }
            //[self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
            [self.editBtn setTitle:@"EDIT" forState:UIControlStateNormal];
            [self.editBtn removeTarget:self action:@selector(saveUserDetails:) forControlEvents:UIControlEventTouchUpInside];
            [self.editBtn addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            editmode = false;
            [self.editBtn setTitle:@"EDIT" forState:UIControlStateNormal];
            self.leftmenuButton.hidden = false;
            self.leftbackButton.hidden = true;
            [self.view resignFirstResponder];
        }

    });
}



#pragma -mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (userDictionary ) {
        return 6;
    }
    else
    {
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editmode == false) {
        CGFloat height = CGRectGetHeight(self.view.frame);
        if (indexPath.row == 0)
        {
            overlappedView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 25, height*0.12 + CGRectGetMinX(self.tableView.frame) + 60, 50, 20);
            [self.view bringSubviewToFront:overlappedView];
            
            return 80;
        }
        else if (indexPath.row == 1)
        {
            otherOverLappedView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 25, height*0.12 * 2 + CGRectGetMinX(self.tableView.frame) + 60, 50, 20);
            [self.view bringSubviewToFront:otherOverLappedView];
            return 80;
        }
        else if (indexPath.row == 2)
        {
            //[self.tableView beginUpdates];
            // [self.tableView endUpdates];
            return 80;
            
        }
        else if (indexPath.row == 3)
        {
            //[self.tableView beginUpdates];
            // [self.tableView endUpdates];
            return 80;
            
        }
        
        else if (indexPath.row == 4)
        {
            return 0;
        }
        else
        {
            return 300;
        }
        
    }
    else {
        
        CGFloat height = CGRectGetHeight(self.view.frame);
        if (indexPath.row == 0)
        {
            overlappedView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 25, height*0.12 + CGRectGetMinX(self.tableView.frame) + 60, 50, 20);
            [self.view bringSubviewToFront:overlappedView];
            
            return 80;
        }
        else if (indexPath.row == 1)
        {
            otherOverLappedView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 25, height*0.12 * 2 + CGRectGetMinX(self.tableView.frame) + 60, 50, 20);
            [self.view bringSubviewToFront:otherOverLappedView];
            return 80;
        }
        else if (indexPath.row == 2)
        {
            return 80;
        }
        
        else if (indexPath.row == 3)
        {
            return 80;
        }
        else if (indexPath.row == 4)
        {
            return 80;
        }
        else
        {
            return 300;
        }
        
        
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        static NSString *identifier = k_basicInfo;
        UserProfileCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if ( cell == nil )
        {
            cell = [[UserProfileCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.userFirstNametxt.delegate = self;
        cell.userLastNametxt.delegate = self;
        cell.userFirstNametxt.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.userLastNametxt.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.userFirstNametxt.autocapitalizationType =UITextAutocapitalizationTypeWords;
        cell.userLastNametxt.autocapitalizationType =UITextAutocapitalizationTypeWords;

        
        NSString *firstName = userDictionary.FName;
        NSString *lastName = userDictionary.LName;
        
        if (editmode)
        {
            [cell rowOneEditMode:editmode];
        }
        else
        {
           // [cell setUserBasicInfomation:firstName LastName:[lastName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] UserIntartion:editmode];
             [cell setUserBasicInfomation:firstName LastName:[lastName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] UserIntartion:editmode];
        }
        return cell;
        
    }
    else if (indexPath.row == 1)
    {
        static NSString *identifier = k_emailInfo;
        UserProfileCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if ( cell == nil )
        {
            cell = [[UserProfileCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.userEmailIdtxt.delegate = self;
        cell.userEmailIdtxt.autocorrectionType = UITextAutocorrectionTypeNo;
        if (editmode)
        {
            [cell rowTwoEditMode:editmode];
        }
        else
        {
            [cell setEmailIdLb:(!editmode)?userDictionary.emailId:cell.userEmailIdtxt.text Phone:userDictionary.phoneNumber UserIntartion:editmode ];
        }
        userDictionary.emailId = cell.userEmailIdtxt.text;
        return cell;
    }
    else if (indexPath.row == 2)
    {
        static NSString *identifier = @"UserDOB";
        UserProfileCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if ( cell == nil )
        {
            cell = [[UserProfileCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        
//        @property (nonatomic, copy) void (^ _Nullable didTouchUpInside)(Button * _Nonnull);
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"dd/MM/yyyy'T'HH:mm:ssZ"];
//        NSDate *date = [dateFormatter dateFromString:userDictionary.Dob];
//        NSString *dateString = @"01-02-2010";
        @try {
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            NSDate *dateFromString = [[NSDate alloc] init];
            // voila!
            dateFromString = [dateFormatter dateFromString:[userDictionary.Dob componentsSeparatedByString:@" "][0]];
            
            
            [cell.btnDob setTitle:strDob != nil ? strDob:[self IndianTimeConverter:dateFromString] forState:UIControlStateNormal];

        } @catch (NSException *exception) {
             [cell.btnDob setTitle:[self IndianTimeConverter:[NSDate date]] forState:UIControlStateNormal];
        }
        //[cell.btnDob addTarget:self action:@selector(didDob_Clicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnDob.didTouchUpInside = ^(Button *sender){
             //Select Date of Birth
             
              UIAlertController * alert=   [UIAlertController
                alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n"
                message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                            
                UIDatePicker *startTimePicker = [[UIDatePicker alloc] init];
                
                [startTimePicker setDatePickerMode:UIDatePickerModeDate];
                if (@available(iOS 14.0, *))
                {
                    [startTimePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels];
                    //UIDatePickerStyleInline
                }
                else
                {
                    // Fallback on earlier versions
                }
                startTimePicker.minuteInterval=30;
                startTimePicker.timeZone = [NSTimeZone localTimeZone];
                [startTimePicker setMaximumDate: [NSDate date]];
                [alert.view addSubview:startTimePicker];
                [alert addAction:({
                    
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        
                strDob = [self IndianTimeConverter:startTimePicker.date];
                        
                [cell.btnDob setTitle:[self IndianTimeConverter:startTimePicker.date] forState:UIControlStateNormal];
                        
                    }];
                    action;
                })];
                
                [alert addAction:({
                    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                        NSLog(@"cancel");
                    }];
                    action;
                })];
                [self presentViewController:alert animated:YES completion:nil];
        }; //Swati Commented This 13 June becoz of touchUpInside
        cell.userInteractionEnabled = editmode ? true:false;
        return cell;
    }
    else if (indexPath.row == 3)
    {
        static NSString *identifier = k_addressInfo;
        UserProfileCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if ( cell == nil )
        {
            cell = [[UserProfileCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelClicked:)],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)],
                               nil];
        
        for (int i = 0 ; i < numberToolbar.items.count; i++)
        {
            objc_setAssociatedObject([numberToolbar.items objectAtIndex:i], (__bridge const void *)(associationKey), cell, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            
            
        }
        [numberToolbar sizeToFit];
        cell.userHomeAddressTxtview.inputAccessoryView = numberToolbar;
        cell.userHomeAddressTxtview.scrollEnabled = YES;
        //cell.userHomeAddressTxtview.showsHorizontalScrollIndicator = YES;
        
        cell.userHomeAddressTxtview.delegate = self;
        cell.userHomeAddressTxtview.autocorrectionType = UITextAutocorrectionTypeNo;

        cell.userHomeAddressTxtview.selectable = YES;
        cell.userHomeAddressTxtview.editable =  editmode;
        
        //cell.userHomeAddressTxtview.text = (editmode)?cell.userHomeAddressTxtview.text:userDictionary.ResAdd;
        cell.userHomeAddressTxtview.text = (editmode)?cell.userHomeAddressTxtview.text:userDictionary.PerAdd;

        ////NSLog(@"t : %@",cell.userHomeAddressTxtview.text);
        if (editmode)
        {
            [cell rowThreeEditMode:editmode];
        }
        else
        {
            [cell setUserHomeAddress:userDictionary.ResAdd PlaceAddress:@"" UserIntartion:editmode];
        }
        return cell;
    }
    
    else if (indexPath.row == 4)
    {
        static NSString *identifier = @"UserPasswordInfo";
        UserProfileCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if ( cell == nil )
        {
            cell = [[UserProfileCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.userPasswordtxt.delegate = self;
        cell.userPasswordtxt.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.userPasswordtxt.secureTextEntry = YES;
        cell.userPasswordtxt.text = userpassword;
        cell.hidden = true;
        if (editmode)
        {
            //[cell rowFourEditMode:editmode];
            cell.changePasswordBtn.hidden = false;
            [cell.changePasswordBtn addTarget:self action:@selector(changePassworBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //NSLog(@"Row four shold be in edit mode");
            cell.hidden = false;
        }
        else
        {
            cell.changePasswordBtn.hidden = true;
            userDictionary.password = cell.userPasswordtxt.text;
        }
        return cell;
    }
    else
    {
        //NSLog(@"edit variable status = %d",editmode);
        static NSString *identifier = k_proofInfo;
        UserProfileCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if ( cell == nil )
        {
            cell = [[UserProfileCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.imgDeleteButton.tag = 1;
        cell.imgAddProfButton.tag = 2;
        
        if (editmode)
        {
            cell.ImgeditButton.hidden = NO;
            cell.imgDeleteButton.hidden = NO;
            cell.imgAddProfButton.hidden = NO;
            
        }else{
            
            cell.ImgeditButton.hidden = YES;
            cell.imgDeleteButton.hidden = YES;
            cell.imgAddProfButton.hidden = YES;
        }
        
        [cell.ImgeditButton addTarget:self action:@selector(editimgBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.imgDeleteButton addTarget:self action:@selector(deleteImgBtnACtion:) forControlEvents:UIControlEventTouchUpInside];
        [cell.imgAddProfButton addTarget:self action:@selector(deleteImgBtnACtion:) forControlEvents:UIControlEventTouchUpInside];
        
        //Address Url and Status
        NSString *addressUrl;
        NSString *addressStatus;
        NSString *addressDocName ;
        
        //Aadhar Card Status
        if (([userDictionary.AdharStatus isEqualToString:@"1"]) || ([[userDictionary.AdharUrl substringFromIndex:userDictionary.AdharUrl.length - 3] isEqualToString:@"jpg"] )) {
            addressUrl = userDictionary.AdharUrl;
            addressStatus = userDictionary.PassportStatus;
            addressDocName = userDictionary.AdharFile;
            [documentStatusdic setObject:@1 forKey:k_ADHAR];
        }else{
            
            [documentStatusdic setObject:@0 forKey:k_ADHAR];
        }
        
        //Passport Status
        if (([userDictionary.PassportStatus isEqualToString:@"1"]) || ([[userDictionary.PassportUrl substringFromIndex:userDictionary.PassportUrl.length - 3] isEqualToString:@"jpg"] )) {
            addressUrl = userDictionary.PassportUrl;
            addressStatus = userDictionary.PassportStatus;
            addressDocName = userDictionary.PassportFile;
            [documentStatusdic setObject:@1 forKey:k_PASSPORT];
        }else{
            [documentStatusdic setObject:@0 forKey:k_PASSPORT];
        }
        
        // Pan Card Status
        if (([userDictionary.PanStatus isEqualToString:@"1"]) || ([[userDictionary.PanUrl substringFromIndex:userDictionary.PanUrl.length - 3] isEqualToString:@"jpg"] )) {
            addressUrl = userDictionary.PanUrl;
            addressStatus = userDictionary.PanStatus;
            addressDocName = userDictionary.PanFile;
            [documentStatusdic setObject:@1 forKey:k_PANCARD];
        }else{
            [documentStatusdic setObject:@0 forKey:k_PANCARD];
        }
        
        if (addressUrl == nil) {
            
            addressUrl = userDictionary.PanUrl;
            addressStatus = userDictionary.PanStatus;
            addressDocName = userDictionary.PanFile;
        }
        
        if (([userDictionary.LiecenceStatus isEqualToString:@"1"]) || ([[userDictionary.DlUrl substringFromIndex:userDictionary.DlUrl.length - 3] isEqualToString:@"jpg"] ))
        {
            [documentStatusdic setObject:@1 forKey:k_DL];
        }else{
            [documentStatusdic setObject:@0 forKey:k_DL];
        }
        
        //NSLog(@"edit mode value =%d",editmode);
        
        [cell setuserProofDetails:userDictionary.DlUrl AddressProof:addressUrl LicenceStatus:userDictionary.LiecenceStatus AddressStatus:addressStatus UserIntartion:editmode LicenceDoc:userDictionary.LiecenceFile AddressDoc:addressDocName];
        rightbarButton.userInteractionEnabled = true;
        
        
        if (userDictionary.LiecenceFile.length == 0)
        {
            cell.drivingLicenceVerfiedStatus.hidden = true;
        }else{
            cell.drivingLicenceVerfiedStatus.hidden = NO;
            if ([userDictionary.LiecenceStatus isEqualToString:@"1"]) {
                cell.drivingLicenceVerfiedStatus.text = @"Approved";
                cell.drivingLicenceVerfiedStatus.textColor = [UIColor colorWithRed:22.0f/255.0f green:158.0f/255.0f blue:63.0f/255.0f alpha:1.0f];
            }
            else if ([userDictionary.LiecenceStatus isEqualToString:@"0"]) {
                cell.drivingLicenceVerfiedStatus.text = @"Rejected";
                cell.drivingLicenceVerfiedStatus.textColor = [UIColor colorWithRed:192.0f/255.0f green:17.0f/255.0f blue:30.0f/255.0f alpha:1.0f];
            }
            else {
                
                cell.drivingLicenceVerfiedStatus.text = @"Pending";
                cell.drivingLicenceVerfiedStatus.textColor = [UIColor colorWithRed:164.0f/255.0f green:164.0f/255.0f blue:164.0f/255.0f alpha:1.0f];
                
            }
        }
        if (userDictionary.PanFile.length == 0 && userDictionary.AdharFile.length == 0 && userDictionary.PassportFile.length == 0)
        {
            cell.addressProofVerfiedStatus.hidden = true;
            
        }else{
            //approved  rgb(22, 158, 63)
            if ([userDictionary.PassportStatus isEqualToString:@"1"] || [userDictionary.AdharStatus isEqualToString:@"1"]||[userDictionary.PanStatus isEqualToString:@"1"]) {
                cell.addressProofVerfiedStatus.text = @"Approved";
                //[UIColor colorWithRed:177.0f/255.0f green:137.0f/255.0f blue:4.0f/255.0f alpha:1.0f]
                cell.addressProofVerfiedStatus.textColor = [UIColor colorWithRed:22.0f/255.0f green:158.0f/255.0f blue:63.0f/255.0f alpha:1.0f];
            }
            //[sourceTypeString intValue] ==
            //164, 164, 164
            else if ([userDictionary.PassportStatus isEqualToString:@"2"] || [userDictionary.AdharStatus isEqualToString:@"2"]||[userDictionary.PanStatus isEqualToString:@"2"]) {
                cell.addressProofVerfiedStatus.text = @"Pending";
                cell.addressProofVerfiedStatus.textColor = [UIColor colorWithRed:164.0f/255.0f green:164.0f/255.0f blue:164.0f/255.0f alpha:1.0f];
            }
            
            else {
                cell.addressProofVerfiedStatus.text = @"Rejected";
                cell.addressProofVerfiedStatus.textColor = [UIColor colorWithRed:192.0f/255.0f green:17.0f/255.0f blue:30.0f/255.0f alpha:1.0f];
                
            }
            
            cell.addressProofVerfiedStatus.hidden = false;
        }
        
        return cell;
    }
}

//# MARK: Button Action

/*- (void)buttonClicked:(UIButton *)sender {
    UIAlertController * alert=   [UIAlertController
                                                 alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n"
                                                 message:nil
                                                 preferredStyle:UIAlertControllerStyleActionSheet];
                   
               
                   UIDatePicker *startTimePicker = [[UIDatePicker alloc] init];
                   
                   [startTimePicker setDatePickerMode:UIDatePickerModeDate];
                   startTimePicker.minuteInterval=30;
                   startTimePicker.timeZone = [NSTimeZone localTimeZone];
                   [startTimePicker setMaximumDate: [NSDate date]];
                   [alert.view addSubview:startTimePicker];
                   [alert addAction:({
                       
                       UIAlertAction *action = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                           
                           strDob = [self IndianTimeConverter:startTimePicker.date];
                           
                           [cell.btnDob setTitle:[self IndianTimeConverter:startTimePicker.date] forState:UIControlStateNormal];
                           
                       }];
                       action;
                   })];
                   
                   [alert addAction:({
                       UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                           NSLog(@"cancel");
                       }];
                       action;
                   })];
                   [self presentViewController:alert animated:YES completion:nil];
}*/

-(void)cancelClicked:(id)sender
{
    UserProfileCustomCell *cell = objc_getAssociatedObject((UIBarButtonItem *)sender, (__bridge const void *)(associationKey));
    cell.userHomeAddressTxtview.text = userDictionary.ResAdd;
    [cell.userHomeAddressTxtview resignFirstResponder];
}


-(void)doneClicked:(id)sender
{
    UserProfileCustomCell *cell = objc_getAssociatedObject((UIBarButtonItem *)sender, (__bridge const void *)(associationKey));
    
    userDictionary.ResAdd = cell.userHomeAddressTxtview.text;
    [cell.userHomeAddressTxtview resignFirstResponder];
    
}
#pragma mark- Edit Document Button
- (void)editimgBtnAction : (UIButton *)sender
{
    UserDocumentUploadVC *documentVC = [self.storyboard instantiateViewControllerWithIdentifier:k_UserDocumentUploadVC ];
    documentVC.docDic = documentStatusdic.mutableCopy;
    [self.navigationController pushViewController:documentVC animated:true];
}
#pragma mark- Delete Document Button
- (void)deleteImgBtnACtion : (UIButton *)sender
{
    NSIndexPath *index = [NSIndexPath indexPathForItem:5 inSection:0];
    UserProfileCustomCell *cell = (UserProfileCustomCell *)[self.tableView cellForRowAtIndexPath:index];
    
    //NSLog(@"pressed button = %d",sender.tag);
    if(sender.tag == 1){
        //NSLog(@"Button1 is clicked");
        //deletedfilename = cell.addressDocName;
        deletedfilename = cell.drivingLicenceDocName;
    }
    else if(sender.tag == 2){
        //NSLog(@"Button2 is clicked");
        //deletedfilename = cell.drivingLicenceDocName;
        deletedfilename = cell.addressDocName;
    }
    
    
    if (deletedfilename.length)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:kDeleteDocument delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1;
        [alert show];
        //[self deleteSelectedDocument];
    }
}


#pragma mark- UIAlertview Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 1)
        {
            
            //Delete Api
            if ([CommonFunctions reachabiltyCheck]) {
                
                if (deletedfilename.length > 0)
                {
                    NSMutableDictionary *body = [NSMutableDictionary new];
                    NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
                    [body setObject: userid forKey:@"userid"];
                    [body setObject: deletedfilename forKey:k_filename];
                    NSURLSession *session = [CommonFunctions defaultSession];
                    [self showLoader];
                    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
                    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KdeleteFile completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                                           {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [SVProgressHUD dismiss];
                                                               });
                                                               NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                               NSString *statusStr = [headers objectForKey:@"Status"];
                                                               if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                               {
                                                                   if (!error)
                                                                   {
                                                                       NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                                       //NSLog(@"RES : %@",responseJson);
                                                                       
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           [self AlertView:[responseJson valueForKey:@"message"]];
                                                                           [self fetchUserProfile];
                                                                           
                                                                       });
                                                                   }
                                                                   else
                                                                   {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{

                                                                          [self AlertView:KAInternet];
                                                                       });
                                                                       
                                                                   }
                                                                
                                                               } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                                       
                                                                       UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                                           [CommonFunctions logoutcommonFunction];
                                                                           [self.revealViewController revealToggleAnimated:true];

                                                                           
                                                                       }];
                                                                       [alertController addAction:ok];
                                                                       
                                                                       [self presentViewController:alertController animated:YES completion:nil];
                                                                   });

                                                               } else
                                                               {
                                                                   
                                                               }
                                                               
                                                               
                                                           }];
                    [globalOperation addOperation:registrationOperation];
                }
                
            }
            else
            {
                [self AlertView:KAInternet];
            }
            
        }
        
    }
}


#pragma mark- UITextFieldDelegate, UITextViewDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    kCurrentTF = textField;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.tableView reloadData];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self.tableView reloadData];
    [textField resignFirstResponder];
    
    return true;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return YES;
    }else if([[textView text] length] > 150){
        return textView.text.length + (text.length - range.length) <= 150;
        //return NO;
    }
    
    return YES;
}

/*
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= 140;
}
 */

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (IS_IPHONE_5)
    {
        [self.tableView setContentOffset:CGPointMake(0, 90) animated:YES];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    textView.scrollEnabled = YES;
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    //ACCEPTABLE_CHARACTERS_EMAIL
    NSIndexPath *index1 =  [NSIndexPath indexPathForItem:1 inSection:0];
    UserProfileCustomCell *cell = (UserProfileCustomCell*) [self.tableView cellForRowAtIndexPath:index1];

    if (textField == cell.userEmailIdtxt) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_EMAIL] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else {
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
    }
}


-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (IS_IPHONE_5)
    {
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    
    //detect if touch is on UITextView
    if ([touch.view isKindOfClass: UITextView.class]) {
        self.tableView.scrollEnabled = YES;
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Logout
- (IBAction)logoutButtonAction:(UIButton *)sender
{
    
}

-(void)changePassworBtnAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.alertType    = 2;
    controller.navController = self.navigationController;
    controller.userPassword = userpassword;
    controller.delegate = self;
    //controller.view.frame = self.view.frame;
    [self presentViewController:controller animated:YES completion:nil];
}



-(void)ValidateAllFields {
    currentPassword = userpassword;
    if (currentPassword.length > 0 & newPassword.length > 0 & confirmPassword.length > 0) {
        
    }
    
    
}

//-(void) SavePassword : (NSNotification *) notification{
//    
//    
//    
//}

//-(void) SavePassword : (NSNotification *) notification {
//    
//    
//    if ([CommonFunctions reachabiltyCheck]) {
//        
//        NSString *password = [notification.userInfo objectForKey:@"password"];
//        [updatedInfo setValue:[NSString stringWithFormat:@"%@",password] forKey:@"password"];
//        
//        NSMutableDictionary *body = [NSMutableDictionary new];
//        NSString *userId = [DEFAULTS valueForKey:KUSER_ID];
//        NSLog(@"User id =%@",userId);
//        [body setObject: userId forKey:@"userid"];
//        
//      //  NSString *oldPasswordStr = [CommonFunctions getKeychainPassword:userId];
//        //NSLog(@"oldPasswordStr = %@",oldPasswordStr);
//      //  NSString *oldMD5PasswordStr = [oldPasswordStr MD5];;
//      //  [body setObject: oldMD5PasswordStr forKey:@"OldPassword"];
//        NSString *newMD5PasswordStr = [password MD5];;
//        [body setObject: newMD5PasswordStr forKey:@"newpass"];
//        
//        NSLog(@"body =%@",body);
//        
//        //[body setObject: deletedfilename forKey:@"changePassword"];
//        NSURLSession *session = [CommonFunctions defaultSession];
//        [GMDCircleLoader setOnView:self.view withTitle:@"Please wait..." animated:YES];
//        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
//        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KCHANGE_PASSWORD completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                                               {
//                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
//                                                   NSString *statusStr = [headers objectForKey:@"Status"];
//                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
//                                                   {
//                                                       if (!error)
//                                                       {
//                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//                                                           NSLog(@"RES : %@",responseJson);
//                                                           
//                                                           
//                                                           
//                                                           
//                                                           //[CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
//                                                           NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
//                                                           
//                                                           if (status == 1) {
//                                                               
//                        [CommonFunctions updateKeychainForUsername:KUSER_PHONENUMBER Password:password];
//                [CommonFunctions searchKeyChainForUserName:KUSER_PHONENUMBER :^(NSString *password) {
//                                                                   
//                                                               }];
//                                                               
//                                                              // [self logoutButtonActionReset];
//                                                           }
//                                                           
//                                                           
//                                                           if ([[responseJson valueForKey:@"message"]
//                                                                isEqualToString:@"Dear Myler, your password has been changed."]) {
//                                                              // [self logoutButtonActionReset];
//                                                           }
//                                                           dispatch_async(dispatch_get_main_queue(), ^{
//                                                               [self AlertView:responseJson[@"message"]];
//                                                               
//                                                               [GMDCircleLoader hideFromView:self.view animated:YES];
//                                                           });
//                                                           
//                                                       }
//                                                       else
//                                                       {
//                                                           dispatch_async(dispatch_get_main_queue(), ^{
//                                                               [GMDCircleLoader hideFromView:self.view animated:YES];
//                                                               
//                                                               [self AlertView:KAInternet];
//                                                           });
//                                                       }
//                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
//                                                       
//                                                       dispatch_async(dispatch_get_main_queue(), ^{
//                                                           [GMDCircleLoader hideFromView:self.view animated:YES];
//                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
//                                                           
//                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                                                               [CommonFunctions logoutcommonFunction];
//
//                                                           }];
//                                                           [alertController addAction:ok];
//                                                           
//                                                           [self presentViewController:alertController animated:YES completion:nil];
//                                                       });
//
//                                                       NSLog(@"AccessToken Invalid");
//                                                   } else
//                                                   {
//                                                       dispatch_async(dispatch_get_main_queue(), ^{
//                                                           [GMDCircleLoader hideFromView:self.view animated:YES];
//                                                       });
//                                                   }
//                                                   
//                                                   
//                                               }];
//        [globalOperation addOperation:registrationOperation];
//    }
//    else
//    {
//        [self AlertView:KAInternet];
//    }
//}

- (void)logoutButtonActionReset
{
    boolVal = YES;
    [self showLoader];
    [CommonApiClass userLogoutApi:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
        NSString *statusStr = [headers objectForKey:@"Status"];
        if (statusStr != nil && [statusStr isEqualToString:@"1"])
        {
            if (!error)
            {
                if (data != nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                        NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                        if (status == 1) {
                            //
                            //[self AlertView:@"Password successfully changed! Please login again with your new password."];
                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Password updated sucessfully. Please login to continue." preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                [CommonFunctions logoutcommonFunction];
                                //[self.revealViewController revealToggleAnimated:true];
//                                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
//                                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
//                                HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
//                                navController.viewControllers = @[homeController];
//                                [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:0];
//                                [(MenuViewController *)self.revealViewController.rearViewController setIndexValue:0];
//                                [[(MenuViewController *)self.revealViewController.rearViewController menuTable] reloadData];
//
//                                [self.revealViewController pushFrontViewController:navController animated:true];
//                                [self.revealViewController revealToggleAnimated:true];
                                self.tabBarController.selectedIndex = 0;
                                
                            }];
                            [alertController addAction:ok];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                        }
                    });
                }
                else
                {
                   
                }
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self AlertView:serviceTimeoutUnabletoreach];
                });
            }
        }
        else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
            NSLog(@"AccessToken Invalid");
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate getAccessTokenWithCompletion:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self.navigationController popToRootViewControllerAnimated:NO];
                    }];
                    [alertController addAction:ok];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                });
            }];
            
        }

        
        
        
        
    }];
}

-(void)AlertView :(NSString *)strMessage
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Myles" message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];

}
/**
 @brief Convert nsdate to string
 @dev Abhishek singh
 */
-(NSString *)IndianTimeConverter : (NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
    dateFormatter1.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [dateFormatter1 stringFromDate:date];
    return strDate;
}

-(void)savePassWord : (NSString *)cnfrmPassword
{
    
    
    
    
    if ([CommonFunctions reachabiltyCheck]) {
        
        
        
        NSString *password = cnfrmPassword;
        
        [updatedInfo setValue:[NSString stringWithFormat:@"%@",password] forKey:@"password"];
        
        
        
        NSMutableDictionary *body = [NSMutableDictionary new];
        
        NSString *userPhone = [DEFAULTS valueForKey:KUSER_PHONENUMBER];
        
        NSLog(@"User phone =%@",userPhone);
        
        [body setObject: userPhone forKey:@"MobileNumber"];
        
        
        
        NSString *oldPasswordStr = [CommonFunctions getKeychainPassword:userPhone];
        
        NSLog(@"oldPasswordStr = %@",oldPasswordStr);
        
        NSString *oldMD5PasswordStr = [oldPasswordStr MD5];;
        
        [body setObject: oldMD5PasswordStr forKey:@"OldPassword"];
        
        NSString *newMD5PasswordStr = [password MD5];;
        
        [body setObject: newMD5PasswordStr forKey:@"NewPassword"];
        
        
        
        NSLog(@"body =%@",body);
        
        
        
        //[body setObject: deletedfilename forKey:@"changePassword"];
        
        NSURLSession *session = [CommonFunctions defaultSession];
        [self showLoader];
        
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KCHANGE_PASSWORD completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                       
                                                   {
                                                       
                                                       if (!error)
                                                           
                                                       {
                                                           
                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           
                                                           NSLog(@"RES : %@",responseJson);
                                                           
                                                           //[CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                           
                                                           NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                                                           
                                                           
                                                           
                                                           if (status == 1) {
                                                               
                                                               
                                                               
                                                               [CommonFunctions updateKeychainForUsername:userPhone Password:password];
                                                               
                                                               [CommonFunctions searchKeyChainForUserName:userPhone :^(NSString *password) {
                                                                   
                                                               }];
                                                               
                                                               
                                                               [self logoutButtonActionReset];
                                                               
                                                           }
                                                           
                                                           
                                                           
                                                           
                                                           
                                                           //                                                           if ([[responseJson valueForKey:@"message"]
                                                           //
                                                           //                                                                isEqualToString:@"Dear Myler, your password has been changed."]) {
                                                           //
                                                           //                                                               [self logoutButtonActionReset];
                                                           //
                                                           //                                                           }
                                                           
                                                           
                                                           
                                                           
                                                           
                                                       }
                                                       
                                                       else
                                                           
                                                       {
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               
                                                               
                                                               
                                                               [CommonFunctions alertTitle:@"" withMessage:KAInternet];
                                                               
                                                           });
                                                           
                                                       }
                                                       
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       
                                                       
                                                       
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                           
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               
                                                               [CommonFunctions logoutcommonFunction];
                                                               
                                                               
                                                               
                                                           }];
                                                           
                                                           [alertController addAction:ok];
                                                           
                                                           
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                           
                                                       });
                                                       
                                                       
                                                       
                                                       NSLog(@"AccessToken Invalid");
                                                       
                                                   } else
                                                       
                                                   {
                                                       
                                                       
                                                   }
                                                   
                                                   
                                                   
                                                   
                                                   
                                               }];
        
        [globalOperation addOperation:registrationOperation];
        
    }
    
    else
        
    {
        
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
        
    }
}
@end
