//
//  DocumentUploadVC.h
//  Myles
//
//  Created by Myles   on 05/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

@interface DocumentUploadVC : BaseViewController < UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate>
{
    UIDatePicker *datePicker;
    UIDatePicker *datePicker2;
}
//Property outlets
@property (weak, nonatomic) IBOutlet UIImageView *uploaddocumentImg;
@property (weak, nonatomic) IBOutlet UITextField *documentNumberTxtfd;
@property (weak, nonatomic) IBOutlet UITextField *documentExpireTxtfd;
@property (weak, nonatomic) IBOutlet UITextField *issueCityTxtfd;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property (weak, nonatomic) IBOutlet UITextField *dobTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

//buttpn Actions
- (IBAction)uploadDocumentAction:(id)sender;
- (IBAction)confirmBtnAction:(UIButton *)sender;
- (IBAction)cancleBtnAction:(UIButton *)sender;

@property (strong, nonatomic) NSString *documentType;
@property (strong, nonatomic) NSString *documentName;
@property (strong, nonatomic) NSString *documentStatus;

@end
