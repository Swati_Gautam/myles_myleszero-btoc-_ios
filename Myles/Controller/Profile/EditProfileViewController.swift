//
//  EditProfileViewController.swift
//  Myles
//
//  Created by Nowfal E Salam on 15/11/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD

class EditProfileViewController: UIViewController,UITableViewDataSource , UITableViewDelegate , UITextFieldDelegate , UITextViewDelegate ,  SorryAlertProtocol{
    var userProfile:UserInfoResponse!
    @IBOutlet weak var tableView: UITableView!
    var documentStatusdic = [String:Int]()
    var firstName = ""
    var lastName = ""
    var emailId = ""
    var dateOfBirth = ""
    var permenantAddress = ""
    var residenceAddress = ""
    var addressUrl = ""
    var addressStatus = ""
    var addressDocName = ""
    var strDob = ""
    var isFromProfile = false
    override func viewDidLoad() {
        super.viewDidLoad()
        getInitialValues()
        self.title = "Edit Profile"
        self.tableView.reloadData()
     // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        if(!isFromProfile){
            if(UserDefaults.standard.bool(forKey: IS_LOGIN)){
                fetchProfile()
            }
            else{
                navigationController?.popViewController(animated: true)
            }

        }
        else{
            isFromProfile = false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (userProfile != nil) {
            return 6;
        }
        else
        {
            return 0;
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 5)
        {
            return 300
        }
        else{
            return 80
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let identifier = k_basicInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            
            cell.setUserBasicInfomation(firstName: firstName, lastName: lastName, userIntraction: true)
            cell.userFirstNametxt.delegate = self
            cell.userLastNametxt.delegate = self
            cell.userFirstNametxt.tag = 11
            cell.userLastNametxt.tag = 12
            return cell;
        }
        else if(indexPath.row == 1){
            let identifier = k_emailInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            cell.userEmailIdtxt.delegate = self
            cell.userEmailIdtxt.tag = 13
            cell.setEmailIdLb(userEmailId: emailId, phoneNumber: userProfile.phoneNumber, userIntartion: true)
            return cell;
        }
        else if(indexPath.row == 2){
            let identifier = "UserDOB";
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            print(userProfile.dob)
            let dateFromString = dateFormatter.date(from: userProfile.dob.components(separatedBy: " ")[0])
            if (strDob != ""){
                cell.btnDob.setTitle(strDob, for: .normal)
            }
            else{
                if let dateFromServer = dateFromString{
                    cell.btnDob.setTitle(self.IndianTimeConverter(date: dateFromServer), for: .normal)
                }
                else{
                    cell.btnDob.setTitle(self.IndianTimeConverter(date: Date()), for: .normal)
                }
            }
            cell.btnDob.didTouchUpInside = {(_ sender: Button) -> Void in
                var alert = UIAlertController(title: """
    
    
    
    
    
    
    
    
    
    
    
    """, message: nil, preferredStyle: .actionSheet)
                let startTimePicker = UIDatePicker()
                startTimePicker.datePickerMode = .date
                /*if #available(iOS 14.0, *) {
                    startTimePicker.datePickerStyle = .wheels
                } else {
                    // Fallback on earlier versions
                }*/
                startTimePicker.minuteInterval = 30
                startTimePicker.timeZone = NSTimeZone.local
                startTimePicker.maximumDate = Date()
                let dateFormatter1 = DateFormatter()
               
                if (self.dateOfBirth != ""){
                    dateFormatter1.dateFormat = "yyyy-MM-dd"
                    let dateFromString1 = dateFormatter1.date(from: self.dateOfBirth)
                    startTimePicker.date = dateFromString1!
                    
                    
                }
                alert.view.addSubview(startTimePicker)
                
                let select = UIAlertAction(title: "Select", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                        self.strDob = self.IndianTimeConverter(date: startTimePicker.date)
                    cell.btnDob.setTitle(self.strDob, for: .normal)
                        self.dateOfBirth = self.strDob
                    
                    })
            
            
                
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
                        print("cancel")
                    })
                alert.addAction(select)
                alert.addAction(cancel)
                self.present(alert, animated: true)
            }
            return cell;
            
        }
        else if(indexPath.row == 3){
            let identifier = k_addressInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            cell.setUserHomeAddress(userHomeAddress: residenceAddress, placeAddress: "", userIntraction: true)
            cell.userHomeAddressTxtview.delegate = self
            return cell;
        }
        else if(indexPath.row == 4){
            let identifier = "UserPasswordInfo";
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
                //cell.userPasswordtxt.delegate = self;
                cell.userPasswordtxt.autocorrectionType = .no
                cell.userPasswordtxt.isSecureTextEntry = true
                cell.userPasswordtxt.text = userProfile.password
                cell.changePasswordBtn.isHidden = false
                cell.changePasswordBtn.addTarget(self, action: #selector(self.changePassworBtnAction), for: .touchUpInside)

            return cell;
        }
        else {
            let identifier = k_proofInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            cell.ImgeditButton.addTarget(self, action: #selector(self.editimgBtnAction), for: .touchUpInside)
            cell.imgDeleteButton.addTarget(self, action: #selector(self.deleteImgBtnACtion), for: .touchUpInside)
            cell.imgAddProfButton.addTarget(self, action: #selector(self.deleteImgBtnACtion), for: .touchUpInside)
            cell.imgDeleteButton.tag = 1
            cell.imgAddProfButton.tag = 2

           
            //Aadhar Card Status
            if (userProfile.adharStatus == "1") || (((userProfile.adharUrl as NSString?)?.substring(from: userProfile.adharUrl.count - 3)) == "jpg") {
                addressUrl = userProfile.adharUrl
                addressStatus = userProfile.passportStatus
                addressDocName = userProfile.adharFile
                documentStatusdic[k_ADHAR] = 1
            }
            else {
                documentStatusdic[k_ADHAR] = 0
            }
            // passport status
            if (userProfile.passportStatus == "1") || (((userProfile.passportUrl as NSString?)?.substring(from: userProfile.passportUrl.count - 3)) == "jpg") {
                addressUrl = userProfile.passportUrl
                addressStatus = userProfile.passportStatus
                addressDocName = userProfile.passportFile
                documentStatusdic[k_PASSPORT] = 1
            }
            else {
                documentStatusdic[k_PASSPORT] = 0
            }
            // Pan Card Status
            if (userProfile.panStatus == "1") || (((userProfile.panUrl as NSString?)?.substring(from: userProfile.panUrl.count - 3)) == "jpg") {
                addressUrl = userProfile.panUrl
                addressStatus = userProfile.panStatus
                addressDocName = userProfile.panFile
                documentStatusdic[k_PANCARD] = 1
            }
            else {
                documentStatusdic[k_PANCARD] = 0
            }
            if addressUrl == "" {
                addressUrl = userProfile.panUrl
                addressStatus = userProfile.panStatus
                addressDocName = userProfile.panFile
                
            }
            if (userProfile.liecenceStatus == "1") || (((userProfile.dlUrl as NSString?)?.substring(from: userProfile.dlUrl.count - 3)) == "jpg") {
                documentStatusdic[k_DL] = 1
            }
            else {
                documentStatusdic[k_DL] = 0
            }
            cell.setuserProofDetails(drivingLicenceUrl: userProfile.dlUrl, addressproofUrl: addressUrl, licenceStatus: userProfile.liecenceStatus.boolValue, addressStatus: addressStatus.boolValue, userIntraction: true, licenceDoc: userProfile.liecenceFile, addressDoc: addressDocName)
            
            if userProfile.liecenceFile.count == 0 {
                cell.drivingLicenceVerfiedStatus.isHidden = true
            }
            else {
                cell.drivingLicenceVerfiedStatus.isHidden = false
                if (userProfile.liecenceStatus == "1") {
                    cell.drivingLicenceVerfiedStatus.text = "Approved"
                    cell.drivingLicenceVerfiedStatus.textColor = UIColor(red: 22.0 / 255.0, green: 158.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
                }
                else if (userProfile.liecenceStatus == "0") {
                    cell.drivingLicenceVerfiedStatus.text = "Rejected"
                    cell.drivingLicenceVerfiedStatus.textColor = UIColor(red: 192.0 / 255.0, green: 17.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
                }
                else {
                    cell.drivingLicenceVerfiedStatus.text = "Pending"
                    cell.drivingLicenceVerfiedStatus.textColor = UIColor(red: 164.0 / 255.0, green: 164.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
                }
            }
            if userProfile.panFile.count == 0 && userProfile.adharFile.count == 0 && userProfile.passportFile.count == 0 {
                cell.addressProofVerfiedStatus.isHidden = true
            }
            else {
                cell.addressProofVerfiedStatus.isHidden = false
                
                //approved  rgb(22, 158, 63)
                if (userProfile.passportStatus == "1") || (userProfile.adharStatus == "1") || (userProfile.panStatus == "1") {
                    cell.addressProofVerfiedStatus.text = "Approved"
                    //[UIColor colorWithRed:177.0f/255.0f green:137.0f/255.0f blue:4.0f/255.0f alpha:1.0f]
                    cell.addressProofVerfiedStatus.textColor = UIColor(red: 22.0 / 255.0, green: 158.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
                }
                else if (userProfile.passportStatus == "2") || (userProfile.adharStatus == "2") || (userProfile.panStatus == "2") {
                    cell.addressProofVerfiedStatus.text = "Pending"
                    cell.addressProofVerfiedStatus.textColor = UIColor(red: 164.0 / 255.0, green: 164.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
                }
                else{
                    cell.addressProofVerfiedStatus.text = "Rejected"
                    cell.addressProofVerfiedStatus.textColor = UIColor(red: 192.0 / 255.0, green: 17.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
                }
                
            }
            return cell;
        }
        
    }
    
    // MARK: -Textfield Delegates
    private func textFieldShoudBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        setvalues(textField: textField)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate methodo
        setvalues(textField: textField)
        self.view.endEditing(true)
       // textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if (text == "\n") {
            self.view.endEditing(true)
            //textView.resignFirstResponder()
            return true
        }
        else if (textView.text.count ) > 150 {
            return (textView.text.count ) + ((text.count ) - range.length) <= 150
            //return NO;
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        //Handle the text changes here
        permenantAddress = textView.text
        residenceAddress = textView.text
    }

    // MARK: -IB Actions
    @objc func deleteImgBtnACtion(_ sender:UIButton){
       
        var deletedfilename = ""
        if(sender.tag == 1){
            deletedfilename = userProfile.liecenceFile
        }
        else if(sender.tag == 2){
            //NSLog(@"Button2 is clicked");
            //deletedfilename = cell.drivingLicenceDocName;
            deletedfilename = addressDocName;
        }
        let alertController = UIAlertController(title: "MYLES", message: kDeleteDocument, preferredStyle: UIAlertController.Style.alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            var dictLogin = [String:String]()
            let id = UserDefaults.standard.object(forKey: KUSER_ID) as! String
            dictLogin["userid"] = id
            dictLogin[k_filename] = deletedfilename
            let apiObj = ApiClass()
            self.showLoader()
            apiObj.deleteFile( deleteDict: dictLogin,completionHandler: { ( status) in
                self.dissmissLoader()
                if(status == "1"){
                    self.addressDocName = ""
                    self.fetchProfile()
                }
                else if (status == "0"){
                   self.sessionExpire()
                }
                
            })
            
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            (result : UIAlertAction) -> Void in
            
        }
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }

    @objc func editimgBtnAction(_ sender: UIButton){
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        let documentVC = storyboard.instantiateViewController(withIdentifier: k_UserDocumentUploadVC) as? UserDocumentUploadVC
        let foundationDictionary = NSMutableDictionary(dictionary: documentStatusdic)
        documentVC?.docDic =  foundationDictionary
        navigationController?.pushViewController(documentVC as? UIViewController ?? UIViewController(), animated: true)
    }
    
    @objc func changePassworBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "MainStoryboard", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "SorryAlertViewController") as? SorryAlertViewController
        controller?.modalPresentationStyle = .overCurrentContext
        controller?.modalTransitionStyle = .crossDissolve
        controller?.alertType = 2
        controller?.navController = navigationController
        controller?.userPassword = userProfile.password
        controller?.delegate = self
        //controller.view.frame = self.view.frame;
        present(controller as? UIViewController ?? UIViewController(), animated: true)
    }
   
    @IBAction func saveProfile(_ sender: UIButton) {
        self.view.endEditing(true)
        var dictLogin = [String:String]()
        var id = UserDefaults.standard.object(forKey: KUSER_ID) as! String
        dictLogin["userid"] = id
       
        if !(firstName.trimmingCharacters(in: .whitespaces).isEmpty) {
            dictLogin["fname"] = firstName
        }
        else{
            self.showAlert(message: "Please enter First Name")
            return
        }
        if !(lastName.trimmingCharacters(in: .whitespaces).isEmpty) {
            dictLogin["lname"] = lastName
        }
        else{
            self.showAlert(message: "Please enter Last Name")
            return
        }
       
        if(emailId.isEmail){
            dictLogin["emailid"] = emailId

        }
        else{
            self.showAlert(message: "Please enter a valid Email id")
            return

        }
        
        if !(dateOfBirth.trimmingCharacters(in: .whitespaces).isEmpty) {
            dictLogin["dob"] = dateOfBirth
        }
        else{
            self.showAlert(message: "Please enter Date of birth")
            return
        }
   
        dictLogin["resadd"] = residenceAddress
        dictLogin["peradd"] = permenantAddress
       
        let apiObj = ApiClass()
        showLoader()
        apiObj.updateProfile( updateDict: dictLogin,completionHandler: { ( status) in
            self.dissmissLoader()
            if(status == "1"){
                self.saveUserDefaults(dictLogin: dictLogin)
                DispatchQueue.main.async {

                self.navigationController?.popViewController(animated: true)
                }
            }
            else if (status == "0"){
                self.sessionExpire()
            }
            
        })
        
    }
    // MARK: -Common logics
    func saveUserDefaults(dictLogin:[String: String]){
        UserDefaults.standard.set(dictLogin["fname"], forKey: KUSER_FNAME)
        UserDefaults.standard.set(dictLogin["lname"], forKey: KUSER_LNAME)
        UserDefaults.standard.set("\(dictLogin["fname"]!) \(dictLogin["lname"]!)", forKey: KUSER_NAME)
        UserDefaults.standard.set(dictLogin["emailid"], forKey: KUSER_EMAIL)
        UserDefaults.standard.set(dictLogin["dob"], forKey: "dob")
        UserDefaults.standard.synchronize()
    }
    func savePassWord(_ cnfrmPassword: String) {
        var dictLogin = [String:String]()
        let phoneNumber = UserDefaults.standard.object(forKey: KUSER_PHONENUMBER) as! String
        dictLogin["MobileNumber"] = phoneNumber
        let oldPassword = CommonFunctions.getKeychainPassword(phoneNumber)
        dictLogin["OldPassword"] = oldPassword?.md5()
        dictLogin["NewPassword"] = cnfrmPassword.md5()
        let apiObj = ApiClass()
        self.showLoader()
        apiObj.changePassword( passwordDict: dictLogin,completionHandler: { ( status) in
            self.dissmissLoader()
            if(status == "1"){
                CommonFunctions.updateKeychain(forUsername: phoneNumber, password: cnfrmPassword)
                CommonFunctions.searchKeyChain(forUserName: phoneNumber, { (password) in
                    
                })
                self.logoutButtonActionReset()
            }
            else if (status == "0"){
                self.sessionExpire()
            }
            
        })
    }
    func fetchProfile(){
        var dictLogin = [String:String]()
        var id = UserDefaults.standard.object(forKey: KUSER_ID) as! String
        dictLogin["userId"] = id
        let apiObj = ApiClass()
        showLoader()
        apiObj.fetchProfile( loginDict: dictLogin,completionHandler: { (userProfile , status) in
            self.dissmissLoader()
            if(status == "1"){
                if let profile = userProfile {
                    self.userProfile = userProfile
                    self.setUpdatedValues()
                    if (self.userProfile.dob != nil && self.userProfile.dob.count > 0 && self.userProfile.dob != "1/1/1900 12:00:00 AM") {
                        UserDefaults.standard.set(self.userProfile?.dob, forKey: "dob")
                        
                    }
                    self.userProfile.password = CommonFunctions.getKeychainPassword(UserDefaults.standard.value(forKey: KUSER_PHONENUMBER) as! String)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }
                
            }
            else if(status == "0"){
                self.sessionExpire()
                
            }
        })
    }
    func setvalues(textField : UITextField){
        switch(textField.tag){
        case 11: firstName = textField.text!
        case 12: lastName = textField.text!
        case 13: emailId = textField.text!
        default: break
        }
        
    }
    func setUpdatedValues(){
         userProfile.fName = firstName
         userProfile.lName = lastName
         userProfile.emailId = emailId
        if(strDob != ""){
            userProfile.dob = strDob// dateOfBirth

        }
         userProfile.perAdd = permenantAddress
         userProfile.resAdd = residenceAddress
         addressUrl = ""
         addressStatus = ""
         addressDocName = ""
    }
    func getInitialValues(){
        firstName = userProfile.fName
        lastName = userProfile.lName
        emailId = userProfile.emailId
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let dateFromString = dateFormatter.date(from: userProfile.dob.components(separatedBy: " ")[0])
        
        dateOfBirth = self.IndianTimeConverter(date: dateFromString!)
        permenantAddress = userProfile.perAdd
        residenceAddress = userProfile.resAdd
    }
    func IndianTimeConverter(date:Date)-> String{
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        // dateFormatter1.timeZone = TimeZone.abbreviation(TimeZone.g)") // NSTimeZone(abbreviation: "GMT") as TimeZone
        dateFormatter1.timeZone = NSTimeZone.local
        let strDate: String = dateFormatter1.string(from: date)
        return strDate
    }
    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    func dissmissLoader(){
        SVProgressHUD.dismiss()
        
    }
    func showAlert(message: String){
        var alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
        var ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(ok)
        present(alertController,animated: true)
    }
    func logoutButtonActionReset(){
        CommonApiClass.userLogoutApi { (data, response, error) in
            var headers = (response as? HTTPURLResponse)?.allHeaderFields
            let statusStr = headers?["Status"] as? String
            if statusStr != nil && (statusStr == "1") {
                if(!(error != nil)){
                    if(data != nil){
                        let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                        if let jsn = json{
                            let status = jsn["status"] as! Int
                            if(status == 1){
                                DispatchQueue.main.async {
                                    
                                    let alertController = UIAlertController(title: "MYLES", message: "Password updated sucessfully. Please login to continue.", preferredStyle: .alert)
                                    let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                        CommonFunctions.logoutcommonFunction()
                                        self.navigationController?.popToRootViewController(animated: true)
                                        //self.tabBarController?.selectedIndex = 0;
                                    })
                                    alertController.addAction(ok)
                                    self.present(alertController, animated: true){}
                                }
                            }
                            else if(status == 0){
                                self.sessionExpire()
                            }
                        }

                        
                    }
                    
                }
                else{
                    self.showAlert(message: serviceTimeoutUnabletoreach)
                    
                }
            }
            else if (statusStr == "0"){
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.getAccessToken(completion: {
                    self.sessionExpire()
                })
            }
        }

    }
    func sessionExpire(){
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "MYLES", message: "Session Expired", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                CommonFunctions.logoutcommonFunction()
                self.navigationController?.popToRootViewController(animated: false)
            })
            alertController.addAction(ok)
            self.present(alertController, animated: true) { }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension String {
    
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    //validate PhoneNumber
    var isPhoneNumber: Bool {
        
        let charcter  = CharacterSet(charactersIn: "0123456789").inverted
        var filtered:String!
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        filtered = inputString.componentsJoined(by: "") as String!
        return  self == filtered
        
    }
}
