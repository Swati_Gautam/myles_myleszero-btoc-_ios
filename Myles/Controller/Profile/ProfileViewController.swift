//
//  ProfileViewController.swift
//  Myles
//
//  Created by Nowfal E Salam on 15/11/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
//import SVProgressHUD

class ProfileViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIButton!
    var documentStatusdic = [String:Int]()
    var userProfile:UserInfoResponse!
    @IBOutlet weak var btnDLViewFile: UIButton!
    @IBOutlet weak var btnAddressViewFile: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Profile"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        if(UserDefaults.standard.bool(forKey: IS_LOGIN)){
            fetchProfile()
        }
        else{
            userProfile = nil
            tableView.reloadData()
            presentLoginActionSheet()
        }
    }
    // MARK: - Api calls
    func fetchProfile(){
        var dictLogin = [String:String]()
        let id = UserDefaults.standard.object(forKey: KUSER_ID) as! String
            dictLogin["userId"] = id
        let apiObj = ApiClass()
        showLoader()
        apiObj.fetchProfile( loginDict: dictLogin,completionHandler: { (userProfile , status) in
            self.dissmissLoader()  //Swati Commented This
            if(status == "1"){
                if let profile = userProfile {
                    self.userProfile = profile
                    if (self.userProfile.dob != nil && self.userProfile.dob.count > 0 && self.userProfile.dob != "1/1/1900 12:00:00 AM") {
                        UserDefaults.standard.set(self.userProfile?.dob, forKey: "dob")

                    }
                    self.userProfile.password = CommonFunctions.getKeychainPassword(UserDefaults.standard.value(forKey: KUSER_PHONENUMBER) as! String)
                    DispatchQueue.main.async {
                        self.tableView.isHidden = false
                        self.editButton.isHidden = false
                        self.tableView.reloadData()
                    }
                
                }
                else{
                    DispatchQueue.main.async {
                        self.tableView.isHidden = true
                        self.editButton.isHidden = true
                    }

                }
            }
            else if(status == "0"){
                DispatchQueue.main.async {
                    self.tableView.isHidden = true
                    self.editButton.isHidden = true
                    CommonFunctions.logoutcommonFunction()
                    self.presentLoginActionSheet()
                }
                
            }
        })
    }
    // MARK: - IBActions
    
    
    @IBAction func btnViewDLFile_Click(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "idViewMylesDocumentVC") as! ViewMylesDocumentVC
        //vc.userProfile = self.userProfile
        //vc.isFromProfile = true
        vc.strDocumentUrl = self.userProfile.dlUrl
        self.navigationController?.present(vc, animated: true, completion: nil)
        //navigationController?.pushViewController(vc,animated: true)
        
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileViewController
            vc.userProfile = self.userProfile
            vc.isFromProfile = true
        navigationController?.pushViewController(vc,
                                                 animated: true)

    }
    // MARK: - Tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (userProfile != nil) {
            return 5;
        }
        else
        {
            return 0;
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 4)
        {
            return 300
        }
        else{
            return 80
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let identifier = k_basicInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            //print("FName: ", userProfile.fName)
            
            if (userProfile.fName != nil)
            {
                cell.setUserBasicInfomation(firstName: userProfile.fName, lastName: userProfile.lName, userIntraction: false)
            }
            else
            {
                cell.setUserBasicInfomation(firstName: "First Name", lastName: "Last Name", userIntraction: false)
            }
            
            
            return cell;
        }
        else if(indexPath.row == 1){
            let identifier = k_emailInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            cell.setEmailIdLb(userEmailId: userProfile.emailId, phoneNumber: userProfile.phoneNumber, userIntartion: false)
            return cell;
        }
        else if(indexPath.row == 2){
            let identifier = "UserDOB";
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            print(userProfile.dob)
            let dateFromString = dateFormatter.date(from: userProfile.dob.components(separatedBy: " ")[0])
            if let date = dateFromString{
                cell.btnDob.setTitle(self.IndianTimeConverter(date: date), for: .normal)
            }
            else{
                cell.btnDob.setTitle(self.IndianTimeConverter(date: Date()), for: .normal)
                
            }
            
            return cell;
            
        }
        else if(indexPath.row == 3){
            let identifier = k_addressInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            cell.setUserHomeAddress(userHomeAddress: userProfile.resAdd, placeAddress: "", userIntraction: false)
            return cell;
        }
        else {
            let identifier = k_proofInfo;
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProfileTableViewCell
            var addressUrl = ""
            var addressStatus = ""
            var addressDocName = ""
            //Aadhar Card Status
            if (userProfile.adharStatus == "1") || (((userProfile.adharUrl as NSString?)?.substring(from: userProfile.adharUrl.count - 3)) == "jpg") {
                addressUrl = userProfile.adharUrl
                addressStatus = userProfile.passportStatus
                addressDocName = userProfile.adharFile
                documentStatusdic[k_ADHAR] = 1
            }
            else {
                documentStatusdic[k_ADHAR] = 0
            }
            // passport status
            if (userProfile.passportStatus == "1") || (((userProfile.passportUrl as NSString?)?.substring(from: userProfile.passportUrl.count - 3)) == "jpg") {
                addressUrl = userProfile.passportUrl
                addressStatus = userProfile.passportStatus
                addressDocName = userProfile.passportFile
                documentStatusdic[k_PASSPORT] = 1
            }
            else {
                documentStatusdic[k_PASSPORT] = 0
            }
            // Pan Card Status
            if (userProfile.panStatus == "1") || (((userProfile.panUrl as NSString).substring(from: userProfile.panUrl.count - 3)) == "jpg") {
                addressUrl = userProfile.panUrl
                addressStatus = userProfile.panStatus
                addressDocName = userProfile.panFile
                documentStatusdic[k_PANCARD] = 1
            }
            else {
                documentStatusdic[k_PANCARD] = 0
            }
            if addressUrl == "" {
                addressUrl = userProfile.panUrl
                addressStatus = userProfile.panStatus
                addressDocName = userProfile.panFile
            }
            if (userProfile.liecenceStatus == "1") || (((userProfile.dlUrl as NSString).substring(from: userProfile.dlUrl.count - 3)) == "jpg") {
                documentStatusdic[k_DL] = 1
            }
            else {
                documentStatusdic[k_DL] = 0
            }
            cell.setuserProofDetails(drivingLicenceUrl: userProfile.dlUrl, addressproofUrl: addressUrl, licenceStatus: userProfile.liecenceStatus.boolValue, addressStatus: addressStatus.boolValue, userIntraction: false, licenceDoc: userProfile.liecenceFile, addressDoc: addressDocName)
            
            if userProfile.liecenceFile.count == 0 {
                cell.drivingLicenceVerfiedStatus.isHidden = true
            }
            else {
                cell.drivingLicenceVerfiedStatus.isHidden = false
                if (userProfile.liecenceStatus == "1") {
                    cell.drivingLicenceVerfiedStatus.text = "Approved"
                    cell.drivingLicenceVerfiedStatus.textColor = UIColor(red: 22.0 / 255.0, green: 158.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
                }
                else if (userProfile.liecenceStatus == "0") {
                    cell.drivingLicenceVerfiedStatus.text = "Rejected"
                    cell.drivingLicenceVerfiedStatus.textColor = UIColor(red: 192.0 / 255.0, green: 17.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
                }
                else {
                    cell.drivingLicenceVerfiedStatus.text = "Pending"
                    cell.drivingLicenceVerfiedStatus.textColor = UIColor(red: 164.0 / 255.0, green: 164.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
                }
            }
            if userProfile.panFile.count == 0 && userProfile.adharFile.count == 0 && userProfile.passportFile.count == 0 {
                cell.addressProofVerfiedStatus.isHidden = true
            }
            else {
                cell.addressProofVerfiedStatus.isHidden = false

                //approved  rgb(22, 158, 63)
                if (userProfile.passportStatus == "1") || (userProfile.adharStatus == "1") || (userProfile.panStatus == "1") {
                    cell.addressProofVerfiedStatus.text = "Approved"
                    //[UIColor colorWithRed:177.0f/255.0f green:137.0f/255.0f blue:4.0f/255.0f alpha:1.0f]
                    cell.addressProofVerfiedStatus.textColor = UIColor(red: 22.0 / 255.0, green: 158.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
                }
                else if (userProfile.passportStatus == "2") || (userProfile.adharStatus == "2") || (userProfile.panStatus == "2") {
                    cell.addressProofVerfiedStatus.text = "Pending"
                    cell.addressProofVerfiedStatus.textColor = UIColor(red: 164.0 / 255.0, green: 164.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
                }
                else{
                    cell.addressProofVerfiedStatus.text = "Rejected"
                    cell.addressProofVerfiedStatus.textColor = UIColor(red: 192.0 / 255.0, green: 17.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
                }
                
            }
            return cell;
        }
        
    }
    // MARK: - Common logics
    func IndianTimeConverter(date:Date)-> String{
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd"
       // dateFormatter1.timeZone = TimeZone.abbreviation(TimeZone.g)") // NSTimeZone(abbreviation: "GMT") as TimeZone
        dateFormatter1.timeZone = NSTimeZone.local
        let strDate: String = dateFormatter1.string(from: date)
        return strDate
    }
    func presentLoginActionSheet(){
        let alertController = UIAlertController(title: "MYLES", message: "Please login to check your profile", preferredStyle: UIAlertController.Style.actionSheet)
        let LoginAction = UIAlertAction(title: "Login", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            var storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
            var viewController  = storyBoard.instantiateViewController(withIdentifier: "LoginProcess") as! LoginProcess
            var navigationController = UINavigationController(rootViewController: viewController)
            viewController.strOtherDestination = "0"
            viewController.fromScreen = ""
            navigationController.viewControllers = [viewController]
            DispatchQueue.main.async {
                self.present(navigationController, animated: true, completion: nil)
            }            
        }
        let okAction = UIAlertAction(title: "Search Car", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            
            self.tabBarController?.selectedIndex = 0
            
        }
        alertController.addAction(LoginAction)
        alertController.addAction(okAction)
        self.tabBarController?.present(alertController, animated: true, completion: nil)
    }
    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    func dissmissLoader(){
        SVProgressHUD.dismiss()

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
