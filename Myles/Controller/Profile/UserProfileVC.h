//
//  UserProfileVC.h
//  Myles
//
//  Created by Myles   on 13/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

@interface UserProfileVC : BaseViewController < UITableViewDataSource, UITableViewDelegate,NSURLSessionDelegate,UITextFieldDelegate, UITextViewDelegate , UIAlertViewDelegate> {
    
     UIAlertController *alertPassword;
     //NSString *passwordFromServer;
}
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic , strong) NSString *passwordFromServer;
@property (nonatomic,retain) UIAlertView *alert;


- (IBAction)editButtonAction:(UIButton *)sender;
-(void)changePassworBtnAction:(id)sender;

@end
