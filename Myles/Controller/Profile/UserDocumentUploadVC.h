//
//  UserDocumentUploadVC.h
//  Myles
//
//  Created by Myles   on 12/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

@interface UserDocumentUploadVC : BaseViewController

- (IBAction)submitBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

- (IBAction)attachedDocumentBtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *buttonSuperview;

@property (weak, nonatomic) IBOutlet UIButton *votarCardBtn;
@property (weak, nonatomic) IBOutlet UIButton *passportBtn;
@property (weak, nonatomic) IBOutlet UIButton *adharCradBtn;
@property (weak, nonatomic) IBOutlet UIButton *drivingBtn;

@property (strong, nonatomic) NSString *documentStatus;
@property (strong, nonatomic) NSMutableDictionary *docDic;
@end
