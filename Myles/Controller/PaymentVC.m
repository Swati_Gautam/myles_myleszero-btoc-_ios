//
//  PaymentVC.m
//  Myles
//
//  Created by Nishant Tyagi on 23/09/15.
//  Copyright (c) 2015 Sanjay Kumar  Yadav. All rights reserved.
//

#import "PaymentVC.h"

@interface PaymentVC ()

@end

@implementation PaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
}
-(IBAction)pbtntouched:(id)sender
{
    
    Checkout *checkout = [[Checkout alloc] init];
    
    
    // setting Merchant fields values
    [checkout setMerchantIdentifier:@"T5163"];   //where T3239 is the merchant Code and will be provided by TPSL
    [checkout setMerchantTransactionIdentifier:@"TXN001"];  //where TXN001 is the Merchant transaction reference number
    [checkout setMerchantReferenceInformation:@"ORD001"];//where TXN001 is the Merchant Order number
    [checkout setTransactionAmount:@"1.0"]; //Transaction amount
    [checkout setMerchantTransactionType:@"Sale"];  //Transaction type,default leave it blank
    [checkout setMerchantTransactionSubType:@""];//Transaction type,default leave it blank
    [checkout setTransactionCurrencyCode:@"INR"]; //CURRENCY
    [checkout setTransactionDateTime:@"19-09-2015"]; //Transaction date
    // setting Consumer fields values
    [checkout setConsumerIdentifier:@"C0000001"]; //Consumer Identifier
    [checkout setConsumerEmailID:@"test@gmail.com"]; //Consumer email id
    [checkout setConsumerMobileNumber:@"7620656789"]; //Consumer mobile number
    //Add Cart items
    
    [checkout addCartItem:@"CARZONMOB" amount:@"1.0" SurchargeOrDiscount:@"0.0" SKU:@"MYLES2015-09-10051" Description:@"MYLES BOOKING" ProviderID:@"P00001" Reference:@"CARZONRENT LTD."];
    PMPaymentViewController *payViewNav = [[PMPaymentViewController alloc] initWithPublicKey:@"abcd" checkout:checkout paymentType:@"Default" success:^(id resObject) {
        //success block
        [checkout setResponseDictionary:resObject]; //first set dictonary data
        
        
        NSString *responseStr =   [NSString stringWithFormat:@"Amount: %@ \nDateTime: %@ \nStatusMessage: %@ \nErrorMessage: %@ \nStatusCode: %@ \nIdentifier: %@ \nRefundIdentifier: %@ \nInstrumentToken: %@ \nMerchantCode: %@ \nMerchantTransactionIdentifier: %@ \nMerchantTransactionRequestType: %@", [checkout getAmount], [checkout getDateTime], [checkout getStatusMessage], [checkout getErrorMessage],[checkout getStatusCode],[checkout getIdentifier],[checkout getRefundIdentifier], [checkout getInstrumentToken], [checkout getMerchantCode], [checkout getMerchantTransactionIdentifier], [checkout getMerchantTransactionRequestType]];
        
        NSLog(@"RESPONESE  : %@",responseStr);
        dispatch_async(dispatch_get_main_queue(), ^{
            [CommonFunctions alertTitle:@"Success!" withMessage:@"payment is successfull"];
            
            
            
            
        });
        
    } failure:^(NSDictionary *error) {
        //failure block
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [CommonFunctions alertTitle:@"Failed!" withMessage:@"payment failed"];
        });
        
        NSLog(@"error %@",error);
        
    } cancel:^(){
        //cancel block
        NSLog(@"Back button pressed!");
        [CommonFunctions alertTitle:@"Cancelled!" withMessage:@"payment cancelled"];
        
        
    }];
    
    [self presentViewController:payViewNav animated:YES completion:nil];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
