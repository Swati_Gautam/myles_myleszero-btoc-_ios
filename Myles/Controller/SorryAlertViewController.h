//
//  SorryAlertViewController.h
//  Myles
//
//  Created by Myles on 13/02/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "AppDelegate.h"

@protocol SorryAlertProtocol <NSObject>

@optional
- (void)preAuthNetBanking;
- (void)preAuthCardPayment;
- (void)netBanking;
- (void)cardBanking;
-(void)documentUpload;
-(void)savePassWord : (NSString *)cnfrmPassword;

@end


@interface SorryAlertViewController : UIViewController{
    
    NSArray *modelImageArray;
    NSString *sublocationTitle;
    NSInteger selectedCityId;
    NSInteger SublocationID;
    NSString *SubLocationAddress;
    AppDelegate  *theAppDelegate;
}
@property (weak, nonatomic) IBOutlet UIView *securityAlertContainerView;
@property (weak, nonatomic) IBOutlet UIButton *payLaterButton;
@property (weak, nonatomic) IBOutlet UIButton *payNowButton;
@property (weak, nonatomic) IBOutlet UILabel *pickupDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *securityAmtLabel;
@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingIDLabel;

@property (weak, nonatomic) IBOutlet UIView *securityPaymentAlertContainer;
@property (weak, nonatomic) IBOutlet UIView *paymentModeAlertContainerView;
@property (weak, nonatomic) IBOutlet UIButton *noNetworkCallButton;
@property (weak, nonatomic) IBOutlet UIView *noNetworkView;
@property (weak, nonatomic) IBOutlet UIButton *upgradeButton;
@property (weak, nonatomic) IBOutlet UIView *upgradeView;
@property (weak, nonatomic) IBOutlet UILabel *alertMessageLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *passwordResetScrollView;
@property (weak, nonatomic) IBOutlet UILabel *sorryMsgLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property (weak, nonatomic) IBOutlet UIButton *tryAnotherCarBtn;
@property (weak, nonatomic) IBOutlet UITextField *currentPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) UINavigationController *navController;
@property (assign, nonatomic) NSInteger alertType;
@property (strong, nonatomic) NSString *versionString;
@property (strong, nonatomic) NSString *userPassword;
@property (strong, nonatomic) NSDictionary *secureInfo;
@property (strong, nonatomic) id<SorryAlertProtocol> delegate;


- (void)dataFromHomeViewControllerFunction : (NSArray *)array NavigationTitle : (NSString *)title CityId : (NSInteger)cityid Sublocation : (NSInteger )sublocationID SubLocationAddress : (NSString *)sublocationAdd;


@end
