//
//  SorryAlertViewController.m
//  Myles
//
//  Created byDivya Rai on 13/02/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "SorryAlertViewController.h"
#import "CarListViewController.h"
#import "BookingDetailsVC.h"
#import "AppDelegate.h"
#import "Myles-Swift.h"

@interface SorryAlertViewController() <HomeViewControllerDelegate, UITextFieldDelegate> {
    
    UITextField *_selectedField;
}

@end

/*
 NSArray *modelImageArray;
 NSString *sublocationTitle;
 NSInteger selectedCityId;
 NSInteger SublocationID;
 NSString *SubLocationAddress;
 */


@implementation SorryAlertViewController


/*
 @property (weak, nonatomic) IBOutlet UIButton *payLaterButton;
 @property (weak, nonatomic) IBOutlet UIButton *payNowButton;
 @property (weak, nonatomic) IBOutlet UILabel *pickupDateLabel;
 @property (weak, nonatomic) IBOutlet UILabel *securityAmtLabel;
 @property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
 @property (weak, nonatomic) IBOutlet UILabel *bookingIDLabel;
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    modelImageArray = [[NSArray alloc]init];
    theAppDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    modelImageArray = theAppDelegate.alertSharedArray;
    sublocationTitle = theAppDelegate.alertSublocationTitle;
    selectedCityId = theAppDelegate.alertSelectedCityId;
    SublocationID = theAppDelegate.alertSublocationID;
    SubLocationAddress = theAppDelegate.alertSubLocationAddress;
    
    self.cancelButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.cancelButton.layer.borderWidth = 0.5;
    self.submitButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.submitButton.layer.borderWidth = 0.5;
    self.currentPasswordTextField.leftView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    self.currentPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.passTextField.leftView           = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    self.passTextField.leftViewMode = UITextFieldViewModeAlways;
    self.confirmPasswordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    self.confirmPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.currentPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.currentPasswordTextField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.passTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.passTextField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.confirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.confirmPasswordTextField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.currentPasswordTextField.layer.masksToBounds = YES;
    self.currentPasswordTextField.layer.cornerRadius = 3.0;
    self.passTextField.layer.masksToBounds = YES;
    self.passTextField.layer.cornerRadius = 3.0;
    self.confirmPasswordTextField.layer.masksToBounds = YES;
    self.confirmPasswordTextField.layer.cornerRadius = 3.0;
    self.upgradeButton.layer.masksToBounds = YES;
    self.upgradeButton.layer.cornerRadius = 3.0;
    self.noNetworkCallButton.layer.masksToBounds = YES;
    self.noNetworkCallButton.layer.cornerRadius = 3.0;
    self.sorryMsgLabel.text = [NSString stringWithFormat:@"Someone else had an eye on your car.\n Please book another car or call us at"];
    
    self.alertMessageLabel.text = [NSString stringWithFormat:@"%@", self.versionString];
    
    if (self.alertType == 1) {
        
        self.containerView.hidden = NO;
        
    }else if (self.alertType == 2){
        
        self.passwordResetScrollView.hidden = NO;
    }else if (self.alertType == 3){
        self.upgradeView.hidden = NO;
    }else if (self.alertType == 4){
        self.noNetworkView.hidden = NO;
    }else if (self.alertType == 5){
        self.paymentModeAlertContainerView.hidden = NO;
    }else if (self.alertType == 6){
        self.securityPaymentAlertContainer.hidden = NO;
    }else if (self.alertType == 7){
        self.securityAlertContainerView.hidden = NO;
    }
    
    NSDate *value = [self.secureInfo objectForKey:@"date"];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-YYYY"];
    NSString *dateString = [df stringFromDate:value];
    
    NSLog(@"self.secureInfo = %@",self.secureInfo);
    
    if (dateString == nil) {
       // dateString = [self.secureInfo objectForKey:@"date"];
        self.pickupDateLabel.text =  [self.secureInfo objectForKey:@"date"];
    }
    else {
        self.pickupDateLabel.text = dateString;
    }
    
    NSLog(@"dateString =%@",dateString);
    
    
    self.securityAmtLabel.text = [self.secureInfo objectForKey:@"preAuth"];
    self.carModelLabel.text = [self.secureInfo objectForKey:@"model"];
    self.bookingIDLabel.text = [self.secureInfo objectForKey:@"bookingId"];
    
    self.payLaterButton.layer.masksToBounds = YES;
    self.payNowButton.layer.masksToBounds = YES;
    self.payNowButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.payNowButton.layer.borderWidth = 0.5;
    self.payLaterButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.payLaterButton.layer.borderWidth = 0.5;
    self.paymentModeAlertContainerView.layer.masksToBounds = YES;
    self.paymentModeAlertContainerView.layer.cornerRadius = 5.0;
    
    NSMutableAttributedString *phoneNoString=[[NSMutableAttributedString alloc]initWithString:self.callButton.titleLabel.text];
    [phoneNoString addAttribute:NSUnderlineStyleAttributeName
                          value:[NSNumber numberWithInt:1]
                          range:(NSRange){0,[phoneNoString length]}];
    
    self.callButton.titleLabel.attributedText = phoneNoString;
    self.callButton.titleLabel.textColor = [UIColor colorWithWhite:1 alpha:.6];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillShowNotification object:nil];
    
}

- (void)keyboardWillAppear:(NSNotification *)notification{
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = _selectedField.frame.origin;
    
    CGFloat buttonHeight = _selectedField.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        
        [self.passwordResetScrollView setContentOffset:scrollPoint animated:YES];
    }
}




- (void)keyboardWillHide:(NSNotification *)notification{
    
    [self.passwordResetScrollView setContentOffset:CGPointZero animated:YES];
}


- (NSString*) getClassName:(id) obj
{
    const char* className = class_getName([obj class]);
    NSString *clsName = [@"" stringByAppendingFormat:@"%s",className];
    
    return clsName;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    [_selectedField resignFirstResponder];
    
    
    if (!CGRectContainsPoint(self.containerView.frame, location)){
    //[self dismissViewControllerAnimated:YES completion:nil];
    }

    UIViewController *tempController = nil;
    
    NSArray *controllers = self.navController.viewControllers;
    
    for (UIViewController *view in controllers) {
        
        if ([view isKindOfClass:[RideHistoryVC class]]||[view isKindOfClass:[BookingConfirmationVC class]]) {
            
            UIViewController *currentVCTest = self.navController.visibleViewController;
            NSLog(@"The Current Class : %@", [self getClassName:currentVCTest]);
            //currentVCTest.
            //()self.alertType
            if (self.alertType == 7) {

            }
            else {
                NSLog(@"No need to Pop the view");
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

/*
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    [_selectedField resignFirstResponder];
    
    
    if (!CGRectContainsPoint(self.containerView.frame, location)){
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
    
    UIViewController *tempController = nil;
    
    NSArray *controllers = self.navController.viewControllers;
    
    for (UIViewController *view in controllers) {
        
        if ([view isKindOfClass:[RideHistoryVC class]]||[view isKindOfClass:[BookingConfirmationVC class]]) {
            
            UIViewController *currentVCTest = self.navController.visibleViewController;
            NSLog(@"The Current Class : %@", [self getClassName:currentVCTest]);
            //currentVCTest.
            //()self.alertType
            if (self.alertType == 7) {
                
            }
            else {
                NSLog(@"No need to Pop the view");
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }
}
 */



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    _selectedField = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


- (IBAction)cancelAction:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (void)dismissAndPush:(UIViewController *)vc {
//    [self dismissViewControllerAnimated:NO completion:^{
//        NSMutableArray *mutableControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//        NSArray *controllers = [mutableControllers arrayByAddingObject:vc];
//        [self.navigationController setViewControllers:controllers animated:NO];
//    }];
//    
//}

- (IBAction)payNowAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([CommonFunctions reachabiltyCheck])
        {

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
        Juspay *jusPayVc = [storyboard instantiateViewControllerWithIdentifier:@"Juspay"];
        jusPayVc.bRentalorPreAurth = true;
        jusPayVc.strBookingId = [self.secureInfo objectForKey:@"bookingId"];
        jusPayVc.delegate = _navController.viewControllers.lastObject;
        [_navController pushViewController:jusPayVc animated:true];
        }
        else
        {
            [self showALert:KAInternet withTitle:KSorry];

//            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        }
    }];
}


- (IBAction)payLaterAction:(UIButton *)sender {
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isDocumentUpload"] == true)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isDocumentUpload"];
        
        [self.delegate documentUpload];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (IBAction)submitAction:(UIButton *)sender {
    
    [self validateBothPssword];

}

- (IBAction)upgradeNowAction:(UIButton *)sender {

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1061852579"]];
}

- (IBAction)mayBeLaterAction:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 
 else if ([passwordTxt.text isEqualToString:@""]||(passwordTxt.text.length < 6)||(passwordTxt.text.length >16)) {
 //passwordTxt
 //else if (passwordTxt.text isEqualToString:@""]||(![Validate isValidPassword:passwordTxt.text])) {
 if ([passwordTxt.text isEqualToString:@""]) {
 //[passwordTxt resignFirstResponder];
 [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter your password."];
 [passwordTxt becomeFirstResponder];
 
 }
 else {
 //[passwordTxt resignFirstResponder];
 [CommonFunctions alertTitle:@"Error" withMessage:@"Your password should contain 6 to 16 characters."];
 [passwordTxt becomeFirstResponder];
 
 //isValidPassword
 }
 }
 */
//void(^myCompletion)(BOOL);
//(void (^)(BOOL completed))completionBlock
//typedef void (^MyFunc)(BOOL finished);

-(void) validateBothPssword {
    
    NSString *password =   self.currentPasswordTextField.text;
    
    NSString *newPassword = self.passTextField.text;
    
    NSString *cnfrmPassword = self.confirmPasswordTextField.text;
    
    NSString *userPhone = [DEFAULTS valueForKey:KUSER_PHONENUMBER];
    
    self.userPassword = [CommonFunctions getKeychainPassword:userPhone];
    
    
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];

    
    
    if ([[self.currentPasswordTextField.text stringByTrimmingCharactersInSet: set] length] == 0||(self.currentPasswordTextField.text.length < 6)||[[self.passTextField.text stringByTrimmingCharactersInSet: set] length] == 0||(self.passTextField.text.length < 6)||(self.passTextField.text.length >16)||(self.confirmPasswordTextField.text.length < 6)||(self.confirmPasswordTextField.text.length >16) ||[[self.confirmPasswordTextField.text stringByTrimmingCharactersInSet: set] length] == 0) {
        
        
        
        if ([[self.currentPasswordTextField.text stringByTrimmingCharactersInSet: set] length] == 0) {
            [self showALert:@"Please enter your current password." withTitle:KSorry];

           // [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter your current password."];
        }
        
        
        
        else if ([[self.passTextField.text stringByTrimmingCharactersInSet: set] length] == 0) {
            [self showALert:@"Please enter your new password." withTitle:KSorry];

            //[CommonFunctions alertTitle:@"Error" withMessage:@"Please enter your new password."];
            
        }
        
        
        
        else if ([[self.confirmPasswordTextField.text stringByTrimmingCharactersInSet: set] length] == 0) {
            [self showALert:@"Please enter confirm password." withTitle:KSorry];

            //[CommonFunctions alertTitle:@"Error" withMessage:@"Please enter your confirm password."];
            
        }
        
        else {
            [self showALert:@"Your password should contain 6 to 16 characters." withTitle:KSorry];

            //[CommonFunctions alertTitle:@"Error" withMessage:@"Your password should contain 6 to 16 characters."];
            
        }
        
    }
    
    else {
        
        
        
        if (password.length> 0 &&newPassword.length>0 && cnfrmPassword.length > 0)
            
        {
            if ([password isEqualToString:newPassword]){
                
                [self showALert:@"New passwords and current password fields are same" withTitle:KSorry];
                
                //[CommonFunctions alertTitle:@"Alert" withMessage:@"New passwords and confirm password fields are different"];
                
            }
            else if (![newPassword isEqualToString:cnfrmPassword]){
            
            [self showALert:@"New passwords and confirm password fields are different" withTitle:KSorry];
            
            //[CommonFunctions alertTitle:@"Alert" withMessage:@"New passwords and confirm password fields are different"];
            
            }
            
           else if ([password isEqualToString:self.userPassword ]&&[newPassword isEqualToString:cnfrmPassword]&&(newPassword.length > 5))
                
            {
                
//                sdsd
                [self dismissViewControllerAnimated:TRUE completion:^{
                    [self.delegate savePassWord:cnfrmPassword];
                }];
                
               // [self dismissViewControllerAnimated:YES completion:nil];
                
              //  [[NSNotificationCenter defaultCenter] postNotificationName:@"PasswordChanged" object:nil userInfo:@{@"password" : cnfrmPassword}];
                
            }
            
            
            
            
            
            else {
                [self showALert:@"Please enter your correct old password." withTitle:KSorry];

                //[CommonFunctions alertTitle:@"Error" withMessage:@"Please enter your correct old password."];
                
            }
            
        }
        
    }
    

    
}


- (IBAction)netBankingAction:(UIButton *)sender {
    
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }

    [self dismissViewControllerAnimated:YES completion:^{

        [self.delegate netBanking];
    }];
}

- (IBAction)netBankingSecurityPaymentAction:(UIButton *)sender {
    
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    [self dismissViewControllerAnimated:YES completion:^{
       
        [self.delegate preAuthNetBanking];
    }];
}

- (IBAction)cardSecurityPaymentAction:(UIButton *)sender {
    
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
       
        [self.delegate preAuthCardPayment];
    }];
}



- (IBAction)cardAction:(UIButton *)sender {
    
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    [self dismissViewControllerAnimated:YES completion:^{
       
        [self.delegate cardBanking];
    }];
}
-(void)showALert :(NSString *)strMessage withTitle:(NSString *)titleStr
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Myles" message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (IBAction)callAction:(UIButton *)sender {
    
    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        [self showALert:kcallingServiceUnavailble withTitle:KSorry];
        //[CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}
/*
 modelImageArray = theAppDelegate.alertSharedArray;
 sublocationTitle = theAppDelegate.alertSublocationTitle;
 selectedCityId = theAppDelegate.alertSelectedCityId;
 SublocationID = theAppDelegate.alertSublocationID;
 SubLocationAddress = theAppDelegate.alertSubLocationAddress;
 */

- (IBAction)tryAnotherCarAction:(UIButton *)sender {
    
    /*
     UIStoryboard *storyboard = k_storyBoard(k_mainStoryBoard);
     CarListViewController *carlistController = [storyboard instantiateViewControllerWithIdentifier:k_CarListViewController];
     [carlistController initilise:modelImageArray NavigationTitle:sublocationTitle CityId:selectedCityId Sublocation:SublocationID SubLocationAddress:SubLocationAddress];
     [self.navigationController pushViewController:carlistController animated:true];
     */
    
    UIViewController *tempController = nil;
    
    NSArray *controllers = self.navController.viewControllers;
    
    for (UIViewController *view in controllers) {
        
        if ([view isKindOfClass:[CarListViewController class]]) {
            
            tempController = view;
            break;
        }
    }
    
    [self dismissViewControllerAnimated:true completion:^{
        
        if (tempController) {
            
            
            [self.navController popToViewController:tempController animated:YES];
            
        }else{
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
            
            NSMutableArray *controllers = [[NSMutableArray alloc] initWithArray:self.navController.viewControllers];
            
            for (int i = controllers.count - 1; i >= 0; i--) {
                
                UIViewController *temp = [controllers objectAtIndex:i];
                if ([temp isKindOfClass:[HomeViewController class]]) {
                    break;
                }else{
                    [controllers removeObject:temp];
                }
            }
            
            self.navController.viewControllers = controllers;
            CarListViewController *carlistController = [storyboard instantiateViewControllerWithIdentifier:@"CarListViewController"];
            //[self presentViewController:controller animated:YES completion:nil];
            [carlistController initilise:sublocationTitle CityId:selectedCityId Sublocation:SublocationID SubLocationAddress:SubLocationAddress];
            
            [self.navController pushViewController:carlistController animated:YES];
            //[self.navigationController pushViewController:controller animated:YES];
            //[self presentViewController:carlistController animated:YES completion:nil];
            
        }
    }];
    
    
    
    
}

/*
 
 Delegate functions to read all data from the home view form
 
 */

- (void)sublocationFromHomeViewController:(NSString *)data
{
    sublocationTitle = data;
    NSLog(@"sublocation title in alert view = %@",data);
    // _button.enabled = NO;
}


- (void)dataFromHomeViewControllerFunction : (NSArray *)array NavigationTitle : (NSString *)title CityId : (NSInteger)cityid Sublocation : (NSInteger )sublocationID SubLocationAddress : (NSString *)sublocationAdd  {
    
    modelImageArray = array;
    sublocationTitle = title;
    selectedCityId = cityid;
    SublocationID = sublocationID;
    SubLocationAddress = sublocationAdd;
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}

@end
