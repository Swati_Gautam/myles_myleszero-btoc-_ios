//
//  Data.swift
//  Myles
//
//  Created by iOS Team on 02/05/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation

@objc class LocationData : NSObject {
    @objc var toRefreshList : Bool = false
    @objc var location : CLLocation?
    @objc var nameOfLocation : String?
    @objc var doorStepDeliveryObj : DoorStepDeliveryAddress?
    
    override init() {
    }
    @objc init(location: CLLocation, nameOfLocation : String, toRefreshList : Bool, doorStepDeliveryAddressObj: DoorStepDeliveryAddress) {
        self.location = location
        self.nameOfLocation = nameOfLocation
        self.toRefreshList = toRefreshList
        self.doorStepDeliveryObj = doorStepDeliveryAddressObj
    }
}
