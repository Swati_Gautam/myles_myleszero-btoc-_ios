//
//  MapViewController.swift
//  Myles
//
//  Created by iOS Team on 17/04/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@objc protocol LocationDataDelegate
{
    func locationData(data: LocationData)
}

/// A view controller which displays a map which continually pans around the area specified as
/// a coordinate.
class MapViewController: UIViewController {
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var mapContainerView: UIView!
    
    @IBOutlet weak var showCarButton: UIButton!
    @IBOutlet weak var bottomMessageView: UIView!
    @objc var cityModelArray = [cityModel]()

    //var cityModelArray : NSArray!
    var AvailableCityArr: Array = [String]()
    @IBOutlet weak var selectedLocationLabel: UILabel!
    private let animationDuration = CFTimeInterval(30)
    private var isAnimating = false
    private var reduceMotionChanged: NotificationObserver<MapViewController>?
    private var lowPowerModeChanged: NotificationObserver<MapViewController>?
    @objc weak var locationDataDelegate : LocationDataDelegate?
    @objc var cityID : String!

    @IBOutlet weak var mapMarkerImageView: UIImageView!
    
    @IBOutlet weak var doorStepChargesLabel: UILabel!
    
    @IBOutlet weak var doorStepTimeLabel: UILabel!
    
    @IBOutlet weak var timeLabelTopConstraint: NSLayoutConstraint!
    var locationManager = CLLocationManager()
    @objc var selectedLocation: CLLocation?
    @objc var finalTimePassed : String = ""
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15
    var cityMatchedWithCurrentLocation = false
    var newLocation:CLLocation!
    var changedAdd = String()
    var isCityExists = false
    var currentCityModelObj : cityModel!
    var doorStepDeliveryAddressObj : DoorStepDeliveryAddress?
    @IBOutlet weak var textFieldView: UIView!

    /// The coordinate to animate the map around.
    var coordinate = CLLocationCoordinate2D(latitude:28.6798605097873, longitude: 77.2174432416314) { // Sydney
        didSet {
            //updateCoordinate()
        }
    }
    
    
    // The currently selected place.
    var selectedPlace: GMSPlace?
    
    // A default location to use when location permission is not granted.
    //let defaultLocation: CLLocation = selectedLocation
        //CLLocation(latitude:28.6798605097873, longitude: 77.2174432416314)


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
//        [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"SitePickUp SearchPage"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
        if (finalTimePassed.characters.count == 0) {
            doorStepTimeLabel.isHidden = true
            timeLabelTopConstraint.constant = 15
        } else {
            doorStepTimeLabel.isHidden = false
        }
        currentCityModelObj = cityModel(cityN:AppDelegate.getSharedInstance().useraddress,cityI:cityID)
        let homePickUpCharge = UserDefaults.standard.value(forKey: k_HomePickupCharge)!
        doorStepChargesLabel.text = "Doorstep Delivery (₹ \(homePickUpCharge))"
        doorStepTimeLabel.text = "Doorstep Delivery - Available from \(finalTimePassed)"
        showCarButton.isEnabled = false
        backButton.isEnabled = false
        showCarButton.layer.cornerRadius = 5
        let tap = UITapGestureRecognizer(target: self, action: #selector(MapViewController.tapFunction))
        selectedLocationLabel.isUserInteractionEnabled = true
        selectedLocationLabel.addGestureRecognizer(tap)
        if (selectedLocation == nil) {
            // Initialize the location manager.
            locationManager = CLLocationManager()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            //locationManager.requestAlwaysAuthorization()
            locationManager.distanceFilter = 50
            locationManager.startUpdatingLocation()
            locationManager.delegate = self
            // Create a map.
            
            let camera = GMSCameraPosition.camera(withLatitude: 28.6798605097873,
                                                  longitude:77.2174432416314 ,
                                                  zoom: zoomLevel)
            mapView = GMSMapView.map(withFrame: mapContainerView.bounds, camera: camera)
            mapView.settings.myLocationButton = true
            mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            mapView.isMyLocationEnabled = true
            // Add the map to the view, hide it until we've got a location update.
            mapContainerView.addSubview(mapView)
            mapView.delegate = self
            mapContainerView.bringSubviewToFront(self.textFieldView)
            applyHoverShadow(view: textFieldView)
            
            mapView.isHidden = true
        } else {
            updatePlaceName(location: selectedLocation!) //Swati Uncommented This
            //self.selectedLocationLabel.text = selectedLocation
            let camera = GMSCameraPosition.camera(withLatitude: (selectedLocation?.coordinate.latitude)!,
                                                  longitude: (selectedLocation?.coordinate.longitude)!,
                                                  zoom: zoomLevel)
            mapView = GMSMapView.map(withFrame: mapContainerView.bounds, camera: camera)
            mapView.settings.myLocationButton = true
            mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            mapView.isMyLocationEnabled = true
            // Add the map to the view, hide it until we've got a location update.
            mapContainerView.addSubview(mapView)
            mapView.delegate = self
            mapContainerView.bringSubviewToFront(self.textFieldView)
            applyHoverShadow(view: textFieldView)
            
            // Add a marker
            
            //            if self.mapMarker == nil {
            //                self.mapMarker = UIImageView()
            //                self.mapMarker.frame = CGRect(x:0,y:0,width :30,height : 30)
            //                self.mapMarker.image = #imageLiteral(resourceName: "MapMarker")
            //                //self.mapMarker.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,]
            //
            //                self.mapMarker.center = CGPoint(x: self.mapContainerView.bounds.midX,
            //                                                y: self.mapContainerView.bounds.midY);
            //                self.mapContainerView.addSubview(self.mapMarker)
            //            }
            
            mapView.animate(to: camera)
            //mapView.isHidden = true
        }
        
        
        
        mapContainerView.bringSubviewToFront(mapMarkerImageView)
        
        mapContainerView.bringSubviewToFront(self.backButton)
        if(cityModelArray.count < 13){
            callApiForCityList()
        }
       
    }
    func callApiForCityList(){
        if CommonFunctions.reachabiltyCheck(){

            
            let callApi = ApiClass()
            callApi.cityListApi(completionHandler: { (arrCityList, isValidToken, responseString) in
                print(responseString)
                if responseString == "Success"{
                    
                    if isValidToken{
                        for  dic in arrCityList!{
                            
                            if self.checkCityId(dic: dic["CityId"] as! String){
                                
                                self.cityModelArray.append(cityModel.init(cityN: dic["CityName"] as! String, cityI: dic["CityId"] as! String))
                            }
                        }
                        
                    }
                    
                }
                else{
                   
                }
                
            })
        }
    }
    func checkCityId(dic :String) -> Bool{
        if(self.cityModelArray.contains(where: { $0.cityId == dic })){
            return false
            
        }
        else{
            return true
            
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        guard let tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-64851985-4") else
        { return }
        tracker.set(kGAIScreenName, value: "DoorStep Search Page")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      //  isCityExists = false
       // cityMatchedWithCurrentLocation = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Implementation
    func updatePlaceName(location: CLLocation) {
        self.showCarButton.isEnabled = false

        //Assigning the selectedLocation to current
        self.selectedLocation = location
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(location.coordinate) { [unowned self] response, error in
            if let error1 = error {
                self.backButton.isEnabled = true
                print("error1.localizedDescription: ", error1.localizedDescription)
                return
            } else {
                print("response = \(String(describing: response))")
                if let address = response?.firstResult(){
                    print("address = \(address)")
                    let lines = address.lines!
                    print("lines = \(lines)")
                    print("locality = \(address.postalCode ?? "")")
                    var completeAddress = ""
                    if (lines.count > 0) {
                        completeAddress = lines[0]
                    }
                    DispatchQueue.main.async {
                        let appDelegate = AppDelegate.getSharedInstance()
                        guard let myString = appDelegate?.useraddress, !myString.isEmpty else {
                            print("String is nil or empty.")
                            return // or break, continue, throw
                        }
                        var str: String = (appDelegate?.useraddress)!
                        print("lines.endIndex: ", lines.endIndex)
                        let strToMatch = lines.joined(separator: "")
                        print("strToMatch is ===== aaaaa\(strToMatch)")
                        if strToMatch.characters.count > 0 {
                            print("printing data = \(lines.last1)")
                            
                            /*if (str == "Noida/Ghaziabad") {
                                let fullCityNameArr = str.components(separatedBy: "/")
                                if ((strToMatch.range(of: fullCityNameArr[0])) != nil) {
                                    str = fullCityNameArr[0]
                                } else if ((strToMatch.range(of: fullCityNameArr[1])) != nil){
                                    str = fullCityNameArr[1]
                                }
                            }*/ //Swati Commented This
                            
                            print("str value = \(str)")
                            
                            if (strToMatch.lowercased().range(of:str.lowercased()) != nil) {
                                // Matched
                                self.cityMatchedWithCurrentLocation = true
                                //self.isNoidaGhaziabad = true
                                
                            } else {
                                self.cityMatchedWithCurrentLocation = false
                                if (self.cityModelArray.count > 0) {
                                    for modelObj in self.cityModelArray {
                                        let cityModelObj = modelObj as! cityModel
                                        print("cityame = \(cityModelObj.cityname.lowercased())")
                                       // self.AvailableCityArr.append(cityModelObj.cityname.lowercased())
                                        let fullCityName = cityModelObj.cityname.lowercased()
                                        if (fullCityName == "delhi/ncr")
                                        {
                                            let fullCityNameArr = fullCityName.components(separatedBy: "/")
                                            if ((strToMatch.lowercased().range(of: fullCityNameArr[0]) != nil) ||  (strToMatch.lowercased().range(of: fullCityNameArr[1]) != nil)) {
                                                print("cityname = \(cityModelObj.cityname)")
                                                self.isCityExists = true
                                                self.currentCityModelObj.cityId = cityModelObj.cityId
                                                self.currentCityModelObj.cityname = cityModelObj.cityname

                                                break
                                            }
                                        } //Swati Added DELHI/NCR Condition
                                        if (fullCityName == "noida/ghaziabad")  //"noida/ghaziabad"
                                        {
                                            let fullCityNameArr = fullCityName.components(separatedBy: "/")
                                            if ((strToMatch.lowercased().range(of: fullCityNameArr[0]) != nil) ||  (strToMatch.lowercased().range(of: fullCityNameArr[1]) != nil)) {
                                                print("cityname = \(cityModelObj.cityname)")
                                                self.isCityExists = true
                                                self.currentCityModelObj.cityId = cityModelObj.cityId
                                                self.currentCityModelObj.cityname = cityModelObj.cityname
                                                
                                                break
                                            }
                                        }
                                        
                                        print("strToMatch===== \(strToMatch)")
                                        if (strToMatch.lowercased().range(of: cityModelObj.cityname.lowercased()) != nil) {
                                            // in city list but different with curent selected location
                                            print("cityname = \(cityModelObj.cityname)")
                                            self.isCityExists = true
                                            self.currentCityModelObj.cityId = cityModelObj.cityId
                                            self.currentCityModelObj.cityname = cityModelObj.cityname
                                            break
                                        }
                                        else {
                                            //not in city list
                                            self.isCityExists = false
                                            self.currentCityModelObj.cityId = "0"
                                            self.currentCityModelObj.cityname = ""
                                        }
                                        
                                      
                                      /* // for sentence in page {
                                        print("self.AvailableCityArr=====\(self.AvailableCityArr)")
                                            for name in self.AvailableCityArr {
                                                print("name=====\(name)")
                                                print("changeddddAaddddrrreeeeee====\(self.changedAdd)")
                                                if self.changedAdd.lowercased().contains(name.lowercased())  {
                                                    print("changedAddresssss ===true")
                                                    self.currentCityModelObj.cityId = cityModelObj.cityId
                                                    self.currentCityModelObj.cityname = cityModelObj.cityname
                                                     self.isCityExists = true
                                                    //do stuff regarding name
                                                    break
                                                }
                                            }
                                      //  }*/
                                        
                                        if (self.changedAdd.lowercased().range(of: cityModelObj.cityname.lowercased()) != nil) {
                                            print("changedAddresssss ===true")
                                            self.currentCityModelObj.cityId = cityModelObj.cityId
                                            self.currentCityModelObj.cityname = cityModelObj.cityname
                                            self.isCityExists = true
                                            //do stuff regarding name
                                            break
                                        }
//                                        let matchingNames = AvailableCityArr.filter { available in
//                                            changedAdd.contains { sentence in
//                                                sentence.contains(available)
//                                            }
//                                        }
                                       
                                    }
                                }
                            }
                        }
                        
                        self.backButton.isEnabled = true
                    }
                    
                    print("sourabhcominghere")
                    
                    self.doorStepDeliveryAddressObj = DoorStepDeliveryAddress(city: address.locality ?? "",locality : address.subLocality ?? "" ,houseFlatNumber: address.thoroughfare ?? "" ,street: address.thoroughfare ?? "",landmark:  address.administrativeArea ?? ""  , postalCode:  address.postalCode ?? "" ,completeAddress : completeAddress, cityID : self.cityID! )
                    
                    // 3
                    self.selectedLocationLabel.text = lines.joined(separator: " ")
                    self.showCarButton.isEnabled = true
                    // 4
//                    UIView.animate(withDuration: 0.25) {
//                        self.view.layoutIfNeeded()
//                    }
                }
            }
            
        }
    }

    
    func showAlertOnView(message: String, isCityAvailable:Bool , withCityModelObj:cityModel) {
        // Show alert
        print("coming city id = \(withCityModelObj.cityId)")
        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
        if (isCityAvailable) {
            alert.addAction(UIAlertAction(title: "Update City", style: .default) { action in
                // perhaps use action.title here
                //we have to update the city in the model
                self.doorStepDeliveryAddressObj?.cityID = withCityModelObj.cityId
                AppDelegate.getSharedInstance().useraddress = withCityModelObj.cityname
                let data = LocationData(location: self.selectedLocation!,nameOfLocation:self.selectedLocationLabel.text!,toRefreshList:true,doorStepDeliveryAddressObj: self.doorStepDeliveryAddressObj!)
                self.locationDataDelegate?.locationData(data: data)
                DispatchQueue.main.async {
                
                    self.dismiss(animated: true, completion: nil)
                }
            })
            alert.addAction(UIAlertAction(title: "Re Enter Location", style: .default) { action in
                // perhaps use action.title here
                //isCityAvailable = false
                //self.isCityExists = true
               if let location = self.newLocation {
                    self.updatePlaceName(location: location)
                } //Swati Uncommented this if condition

            })
        } else {
            alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
                // perhaps use action.title here
                
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
                // perhaps use action.title here
            })
        }
        
       
        self.present(alert, animated: true, completion: {
            //
        })
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if(!(CommonFunctions.connectedToNetwork())) {
            let locationObj = LocationData()
            locationObj.nameOfLocation = ""
//            locationObj = nil
            locationDataDelegate?.locationData(data: locationObj)
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            guard selectedLocation != nil else {
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
                return
            }
            let data = LocationData(location: selectedLocation!,nameOfLocation:self.selectedLocationLabel.text!,toRefreshList:false,doorStepDeliveryAddressObj: self.doorStepDeliveryAddressObj!)
            locationDataDelegate?.locationData(data: data)
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
        

    }
    
    @objc private func tapFunction(sender:UITapGestureRecognizer)
    {
        print("tap working")
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.country = "IN"
        
        // If we have to restrict city than uncomment below line
        
        filter.type = GMSPlacesAutocompleteTypeFilter.noFilter
        
        //filter.type = GMSPlacesAutocompleteTypeFilter.address
        //filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        autocompleteController.tintColor = UIColor.darkGray
        autocompleteController.navigationController?.navigationBar.tintColor = UIColor.darkGray
       UINavigationBar.appearance().tintColor = UIColor.darkGray
        UISearchBar.appearance().tintColor = UIColor.darkGray
        autocompleteController.navigationController?.navigationBar.barTintColor = UIColor.black;
        // Color of typed text in search bar.
        
        let searchBarTextAttributes: [AnyHashable: Any] = [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(UIFont.systemFontSize))]
        
        if let attributes = searchBarTextAttributes as? [NSAttributedString.Key : Any]{
            UITextField.appearance(whenContainedInInstancesOf: [GMSAutocompleteViewController.self]).defaultTextAttributes = attributes
        }
        present(autocompleteController, animated: true, completion: nil)
        
        /*resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self as GMSAutocompleteResultsViewControllerDelegate
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        
        let subView = UIView(frame: CGRect(x: 0, y: 65.0, width: 350.0, height: 45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
        
        definesPresentationContext = true*/
    }
    
    private func applyHoverShadow(view: UIView) {
        let size = view.bounds.size
        let width = size.width
        let height = size.height
        
        let ovalRect = CGRect(x: 5, y: height + 5, width: width - 10, height: 15)
        let path = UIBezierPath(roundedRect: ovalRect, cornerRadius: 10)
        
        let layer = view.layer
        layer.shadowPath = path.cgPath
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    @IBAction func showCarsTapped(_ sender: Any)
    {
        //self.cityID == self.currentCityModelObj?.cityId
        print("currentCityModelObj.cityname====\(currentCityModelObj.cityname)")
        let fullCityName = currentCityModelObj.cityname.lowercased()
        print("fullCityName ====\(fullCityName)")
        
        if changedAdd.lowercased().contains(AppDelegate.getSharedInstance().useraddress){
            
            let appDelegate = AppDelegate.getSharedInstance()
            
            self.showAlertOnView(message: "Your location for door-step delivery is in \(currentCityModelObj.cityname), while you have searched for Myles in \(String(describing: (appDelegate?.useraddress)!))",isCityAvailable: isCityExists,withCityModelObj: currentCityModelObj)
        }
            
//        else if (currentCityModelObj.cityname == "Delhi" || currentCityModelObj.cityname == "Noida" || currentCityModelObj.cityname == "Ghaziabad" || currentCityModelObj.cityname == "Gurgaon" || currentCityModelObj.cityname == "Gurugram" || currentCityModelObj.cityname == "Delhi/NCR" || currentCityModelObj.cityname == "Noida/Ghaziabad")
            
        else if (fullCityName == "delhi" || fullCityName == "noida" || fullCityName == "ghaziabad" || fullCityName == "gurgaon" || fullCityName == "gurugram" || fullCityName == "delhi/ncr" || fullCityName == "noida/ghaziabad")
        {
            let data = LocationData(location: selectedLocation!,nameOfLocation:self.selectedLocationLabel.text!,toRefreshList:true,doorStepDeliveryAddressObj: self.doorStepDeliveryAddressObj!)
            locationDataDelegate?.locationData(data: data)
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
       else if (cityMatchedWithCurrentLocation) {
            let data = LocationData(location: selectedLocation!,nameOfLocation:self.selectedLocationLabel.text!,toRefreshList:true,doorStepDeliveryAddressObj: self.doorStepDeliveryAddressObj!)
            locationDataDelegate?.locationData(data: data)
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
        else
        {
//            if (isNoidaGhaziabad && isCityExists) {
//                let data = LocationData(location: selectedLocation!,nameOfLocation:self.selectedLocationLabel.text!,toRefreshList:true,doorStepDeliveryAddressObj: self.doorStepDeliveryAddressObj!)
//                locationDataDelegate?.locationData(data: data)
//                
//                self.dismiss(animated: true, completion: nil)
//            } else {
            //Your location for door-step delivery is in 'City name'  while you have searched for Myles in 'City name
            
            //Location selected is not aligned with your search city
                if (isCityExists) {
                    let appDelegate = AppDelegate.getSharedInstance()

                    self.showAlertOnView(message: "Your location for door-step delivery is in \(currentCityModelObj.cityname), while you have searched for Myles in \(String(describing: (appDelegate?.useraddress)!))",isCityAvailable: isCityExists,withCityModelObj: currentCityModelObj)
                }
                else
                {
                    self.showAlertOnView(message: "Apologies for not being able to serve your location! You may pick your car from one of our site locations.", isCityAvailable: isCityExists,withCityModelObj: currentCityModelObj)
                }
           // }
            
        }
    }
    
}

extension MapViewController : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture) {
            self.showCarButton.isEnabled = false
            self.selectedLocationLabel.text = "Getting Location..."
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.showCarButton.isEnabled = true

        let mapLattitude : Double!
        let mapLongitude : Double!
       
        mapLattitude = position.target.latitude
        mapLongitude = position.target.longitude
        
        
        // update name of location
        newLocation = CLLocation.init(latitude: mapLattitude, longitude: mapLongitude)
        updatePlaceName(location: newLocation)
        
    }
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if let location = mapView.myLocation {
            updatePlaceName(location: location)
            return false
        } else {
            let alert = UIAlertController(title: kSetting, message: kLocationServiceOnMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
                // perhaps use action.title here
                
                UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
                
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
                // perhaps use action.title here
                //self.backButtonTapped(self)
                
            })
            
            
            
            self.present(alert, animated: true, completion: {
                //
            })

            return true
        }
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        //let changedLocation = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
        //updatePlaceName(location: changedLocation)
        
//        let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude,
//                                              longitude: marker.position.longitude,
//                                              zoom: self.zoomLevel)
//        
//        
//        
//        // Clear the map.
//        self.mapView.clear()
//        
//        // Add a marker to the map.
//            self.mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
//            let marker = GMSMarker(position: (marker.position))
//            marker.appearAnimation = kGMSMarkerAnimationPop
//            marker.groundAnchor = CGPoint(x:0.5,y:0.5)
//            //marker.tracksInfoWindowChanges = true
//            marker.title = self.selectedPlace?.name
//            marker.snippet = self.selectedPlace?.formattedAddress
//            marker.map = self.mapView
//            marker.isDraggable = true
//            self.mapView.animate(to: camera)
        
    }
}

extension MapViewController: GMSAutocompleteResultsViewControllerDelegate
{
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
}


extension MapViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print("Place Latitude: \(String(describing: place.coordinate.latitude))")
        print("Place Longitude: \(String(describing: place.coordinate.longitude))")
        var addressDetailsObj = AddressDetails()
        addressDetailsObj.houseFlatNumber = ""
        addressDetailsObj.completeAddress = place.formattedAddress
        // Get the address components.
        if let addressLines = place.addressComponents {
            print("place.addressComonents = \(String(describing: place.addressComponents))")
            
            // Populate all of the address fields we can find.
            for field in addressLines {
                switch field.type {
                case kGMSPlaceTypeStreetNumber:
                    print("street = \(field.name)")
                case kGMSPlaceTypeRoute:
                    print("Route = \(field.name)")
                    addressDetailsObj.street = field.name
                case kGMSPlaceTypeNeighborhood:
                    print("Neighberhood = \(field.name)")
                    addressDetailsObj.landmark = field.name
                case kGMSPlaceTypeLocality:
                    print("Locality = \(field.name)")
                    addressDetailsObj.locality = field.name
                case kGMSPlaceTypeAdministrativeAreaLevel1:
                    print("Area11 = \(field.name)")
                case kGMSPlaceTypeCountry:
                    print("countru = \(field.name)")
                case kGMSPlaceTypePostalCode:
                    print("postal code = \(field.name)")
                case kGMSPlaceTypePostalCodeSuffix:
                    print("suffix = \(field.name)")
                    addressDetailsObj.postalCode = field.name
                // Print the items we aren't using.
                case kGMSPlaceTypeSublocalityLevel1:
                    addressDetailsObj.houseFlatNumber = field.name
                case kGMSPlaceTypeSublocalityLevel2:
                    addressDetailsObj.houseFlatNumber = addressDetailsObj.houseFlatNumber! + field.name
                case kGMSPlaceTypeAdministrativeAreaLevel1:
                    addressDetailsObj.city = field.name
                case kGMSPlaceTypeAdministrativeAreaLevel2:
                    addressDetailsObj.city = field.name
                default:
                    print("Type: \(field.type), Name: \(field.name)")
                }
            }
        }
        
        
        dismiss(animated: true, completion: {
            

            print("formattedAddress = \(String(describing: place.formattedAddress))")
            //self.selectedLocationLabel.text = place.formattedAddress
            
            self.doorStepDeliveryAddressObj = DoorStepDeliveryAddress(city : addressDetailsObj.city ?? "",locality : addressDetailsObj.locality ?? "",houseFlatNumber: addressDetailsObj.houseFlatNumber ?? "",street : addressDetailsObj.street ?? "",landmark: addressDetailsObj.landmark ?? "",postalCode: addressDetailsObj.postalCode ?? "",completeAddress: addressDetailsObj.completeAddress ?? "", cityID: self.cityID!)
            if let add = place.formattedAddress{
                   self.changedAdd = add
                   self.currentCityModelObj.cityname = self.changedAdd //Swati Added This
            }       
            //self.coordinate = place.coordinate
            let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude,
                                                  longitude: place.coordinate.longitude,
                                                  zoom: self.zoomLevel)
            self.selectedPlace = place
            // Clear the map.
            //self.mapView.clear()
            
//            // Add a marker to the map.
//            if self.mapMarker == nil {
//                self.mapMarker = UIImageView()
//                self.mapMarker.frame = CGRect(x:0,y:0,width :30,height : 30)
//                self.mapMarker.image = #imageLiteral(resourceName: "MapMarker")
//                //self.mapMarker.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,]
//                
//                self.mapMarker.center = CGPoint(x: self.mapContainerView.bounds.midX,
//                                                y: self.mapContainerView.bounds.midY);
//                self.mapContainerView.addSubview(self.mapMarker)
//            }
//
//                self.mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
//                let marker = GMSMarker(position: (place.coordinate))
//                marker.appearAnimation = kGMSMarkerAnimationPop
//                marker.groundAnchor = CGPoint(x:0.5,y:0.5)
//                marker.tracksInfoWindowChanges = true
//                marker.title = self.selectedPlace?.name
//                marker.snippet = self.selectedPlace?.formattedAddress
//                marker.map = self.mapView
//                marker.isDraggable = true
                self.mapView.animate(to: camera)
                
           // }
        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error GMSPLACE: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //showCarButton.isEnabled = true
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("abcd")
        
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("efgh")
    }
}


// Delegates to handle events for the location manager.
extension MapViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last{
            print("Location in didUpdateLocations: \(location)")

            locationManager.stopUpdatingLocation()
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                  longitude: location.coordinate.longitude,
                                                  zoom: zoomLevel)
        
            updatePlaceName(location: location) //Swati Uncommented this line
//            GMSCameraUpdate
            if mapView.isHidden {
                mapView.isHidden = false
                mapView.camera = camera
            } else {
                // show pin
                //self.mapView.clear()
                let locationDate = location.timestamp
                let howRecent = locationDate.timeIntervalSinceNow
                print("howrecent value = \(howRecent)")
                if (abs(howRecent) < 15) {
                    let gmsCameraUpdate = GMSCameraUpdate.setTarget(location.coordinate, zoom: zoomLevel)
                    mapView.animate(with: gmsCameraUpdate)
                }
               
                //mapView.animate(to: camera)
            }

        }
       

    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
        let alert = UIAlertController(title: kSetting, message: kLocationServiceOnMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
                // perhaps use action.title here
                
                UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)

            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
                // perhaps use action.title here
                //self.backButtonTapped(self)
            })
        self.present(alert, animated: true, completion: {
            //
        })
    }
}


struct AddressDetails {
    var city: String?
    var locality : String?
    var houseFlatNumber : String?
    var street : String?
    var landmark : String?
    var postalCode : String?
    var completeAddress : String?
    
    init() {
        
    }
}


extension Array {
    func contains<T>(obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
}

extension Array {
    var last1: Element {
        return self[self.endIndex - 1]
    }
}
 
