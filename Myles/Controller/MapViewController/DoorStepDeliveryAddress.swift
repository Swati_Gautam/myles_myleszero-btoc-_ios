//
//  DoorStepDeliveryAddress.swift
//  Myles
//
//  Created by iOS Team on 05/05/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation

@objc class DoorStepDeliveryAddress : NSObject {
    @objc var city : String?
    @objc var locality : String?
    @objc var houseFlatNumber : String?
    @objc var street : String?
    @objc var landmark : String?
    @objc var postalCode : String?
    @objc var completeAddress : String?
    @objc var cityID : String?
    
    @objc init(city : String, locality : String, houseFlatNumber : String,street : String,landmark : String,postalCode : String,completeAddress : String,cityID : String) {
        self.city = city
        self.locality = locality
        self.houseFlatNumber = houseFlatNumber
        self.street = street
        self.landmark = landmark
        self.postalCode = postalCode
        self.completeAddress = completeAddress
        if (cityID.characters.count > 0)
        {
            self.cityID = cityID
        } else {
            self.cityID = ""
        }
    }
}
