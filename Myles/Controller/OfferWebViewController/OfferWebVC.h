//
//  OfferWebVC.h
//  Myles
//
//  Created by Chetan Rajauria on 18/02/19.
//  Copyright © 2019 Myles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>


@interface OfferWebVC : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet WKWebView *wkWebView;

@property (nonatomic, strong)NSString * webURLString;
@property (nonatomic, strong)NSDictionary * dict;

@end

