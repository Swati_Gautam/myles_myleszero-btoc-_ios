//
//  OfferWebVC.m
//  Myles
//
//  Created by Chetan Rajauria on 18/02/19.
//  Copyright © 2019 Myles. All rights reserved.
//

#import "OfferWebVC.h"

@import FirebaseAnalytics;


@interface OfferWebVC ()

@end

@implementation OfferWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _wkWebView.frame = CGRectMake(self.view.frame.origin.x + 25,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:_wkWebView];
    
    NSURL *url = [NSURL URLWithString:self.webURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    //_wkWebView = [[WKWebView alloc] initWithFrame:self.view.frame];
    [_wkWebView loadRequest:request];
    
    //[self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webURLString]]];
    //[self.webView setDelegate:(id)self];
    
    self.navigationController.navigationBar.topItem.title = [NSString stringWithFormat:@"%@",[self.dict valueForKey:@"title"]];

    
    [FIRAnalytics logEventWithName:@"Carousel_Web_Screen"
                        parameters:nil];

    
    [self showLoader];
    
    /*WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:theConfiguration];
    webView.navigationDelegate = self;
    NSURL *nsurl=[NSURL URLWithString:@"http://www.apple.com"];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webView loadRequest:nsrequest];
    [self.view addSubview:webView];*/
}

// Delegate methods

/*-(void)webViewDidStartLoad:(UIWebView *)webView {
    [self showLoader];

}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [SVProgressHUD dismiss];
}*/

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}


@end
