//
//  PageControlVC.swift
//  Myles
//
//  Created by Chetan Rajauria on 15/06/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import UIKit

class PageControlVC: UIViewController {

    @IBOutlet weak var imgPages: UIImageView!
    
    var strImgName: String?
    var pageIndex: NSInteger?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let image = UIImage(named: strImgName!)
        self.imgPages?.image = image;
        imgPages?.image = UIImage.init(named: strImgName! as String)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
