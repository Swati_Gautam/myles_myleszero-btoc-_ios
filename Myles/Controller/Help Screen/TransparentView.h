//
//  TransparentView.h
//  Myles
//
//  Created by Chetan Rajauria on 09/10/18.
//  Copyright © 2018 Myles. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransparentView : UIView

@property (nonatomic, assign) CGRect rectForClearing;
@property (nonatomic, strong) UIColor *overallColor;


@end

NS_ASSUME_NONNULL_END
