//
//  HelpVC.swift
//  Myles
//
//  Created by Chetan Rajauria on 15/06/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import UIKit

class HelpVC: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    @IBOutlet weak var outerView: UIView!
    
    
    @IBOutlet weak var skipBtn: UIButton!
    var pageViewController : UIPageViewController?
    var arrPageImages:NSArray = NSArray()
    var isComeFromMenu : Bool = false
    
    // Track the current index
    var currentIndex: Int?
    private var pendingIndex: Int?
    
    @IBOutlet var btnGetStarted: UIButton!
    @IBOutlet var btnBack: UIButton!
    
    var pageControl = UIPageControl()
    
    var revealController = SWRevealViewController()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//       let str = UserDefaults.standard.string(forKey: "IsHelpSeen")
//        if str == "Yes" {
//            DispatchQueue.main.async {
//                let storyboard = UIStoryboard(name: "MainTabStoryboard", bundle: nil)
//                let controller = storyboard.instantiateViewController(withIdentifier: "MainTabController")
//
//                self.present(controller, animated: false, completion: nil)
//
//            }
//        }

        // CarzOnRent
        
        arrPageImages = ["Image_one", "Image_two", "Image_three", "Image_four","Image_one", "Image_two", "Image_three", "Image_four"]
        print("arrImage Count: ", arrPageImages.count)
        
        
        pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "idPageViewController") as? UIPageViewController
        
        
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController!.dataSource = self
        pageViewController!.delegate = self;
        
        
        self.configurePageControl()
        
        let startingViewController: PageControlVC = viewControllerAtIndex(index: 0)!
        let viewControllers = [startingViewController]
        pageViewController!.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
        //pageViewController!.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height);
        
        addChild(pageViewController!)
        outerView.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
        
        outerView.addSubview(skipBtn)
        
        ///self .addButton()
    }
    
    func addButton() {
        let button = UIButton.init(type: UIButton.ButtonType.custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 50, height: 30)
        button.backgroundColor = UIColor.red
        outerView.addSubview(button)
    }
    
    
    func configurePageControl() {
        
        self.pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 50,width: UIScreen.main.bounds.width,height: 50))
        self.pageControl.currentPage = 0
        self.pageControl.numberOfPages = arrPageImages.count
       // self.pageControl.tintColor = UIColor.black
        self.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        self.view.addSubview(pageControl)
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        pendingIndex = (pendingViewControllers.first as! PageControlVC).pageIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentIndex = pendingIndex
            if let index = currentIndex {
                
                print(index, arrPageImages.count)
                self.pageControl.currentPage = index
                
                if index == arrPageImages.count - 1 {
                    
                    UserDefaults.standard.set("Yes", forKey: "IsHelpSeen")
                    
                    DispatchQueue.main.async {
                        let storyboard = UIStoryboard(name: "MainTabStoryboard", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "MainTabController")
                        
                        self.present(controller, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    /*
     private func setupPageControl() {
     let appearance = UIPageControl.appearance()
     appearance.pageIndicatorTintColor = UIColor.gray
     appearance.currentPageIndicatorTintColor = UIColor.white
     appearance.backgroundColor = UIColor.darkGray
     }
     
     func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
     {
        setupPageControl()
        return arrPageImages.count
     }
     
     func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
     {
     return 0
     }
     */
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! PageControlVC).pageIndex
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index = index! - 1
        
        return viewControllerAtIndex(index: index!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! PageControlVC).pageIndex
        
        if index == NSNotFound {
            return nil
        }
        
        index = index! + 1
        
        
        
        
        if (index == self.arrPageImages.count) {
            return nil
        }
        
        
        
        return viewControllerAtIndex(index: index!)
    }
    
    func viewControllerAtIndex(index: Int) -> PageControlVC?
    {
        if arrPageImages.count == 0 || index >= arrPageImages.count
        {
            return nil
        }
        
        // Create a new view controller and pass suitable data.
        let objPageControl = self.storyboard?.instantiateViewController(withIdentifier: "idPageControlVC") as! PageControlVC
        objPageControl.strImgName = arrPageImages[index] as? String
        objPageControl.pageIndex = index
        //currentIndex = index
        
        return objPageControl
    }
    
    @IBAction func skipAction(_ sender: Any) {
        
        UserDefaults.standard.set("Yes", forKey: "IsHelpSeen")
        
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "MainTabStoryboard", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MainTabController")

            self.present(controller, animated: true, completion: nil)

        }
        
    }
    
    
    
}
