//
//  AddressEditViewController.m
//  Myles
//
//  Created by IT Team on 13/03/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "AddressEditViewController.h"

@interface AddressEditViewController ()

@end

@implementation AddressEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    
    if (!CGRectContainsPoint(self.containerView.frame, location)){
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (IBAction)editAddress:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
    
        [self.delegate getEditOptionSelection:1];

    }];
}

- (IBAction)resetAddress:(id)sender {

    [self dismissViewControllerAnimated:YES completion:^{

        [self.delegate getEditOptionSelection:2];

    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
