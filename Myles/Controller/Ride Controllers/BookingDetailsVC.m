
#import "BookingDetailsVC.h"
#import "DetailCustomCell.h"
#import "CarDetailInfo.h"
#import "GSTPackageDetails.h"
#import "NSUserDefaults+Utility.h"
#import "AdditionalServiceInfo.h"
#import "CouponResponse.h"
#import "TermsAndConditionViewController.h"
#import "ActiveRidesResponse.h"
#import "MenuViewController.h"
#import "UserProfileVC.h"
#import "PickupAddressVC.h"
#import "SorryAlertViewController.h"
#import "AddressEditViewController.h"
#import <Google/Analytics.h>
#import<CoreLocation/CoreLocation.h>
#import "Myles-Swift.h"
#import "Haneke.h"
#import "PickUpAddressList.h"
#import "AddHomePickUpAddress.h"
#import "UserInfo.h"
#import "FRHyperLabel.h"
@import FirebaseMessaging;
@import FirebaseInstanceID;
#import "additionalServiceresponse.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import FirebaseAnalytics;
#define kTableHeaderHeight 40.0f
#define kTableRowHeight 25.0f
#define carBlockTime 600.0

#import "TransparentView.h"

static NSString *kAssociationKeyForConfirmation = @"kAssociationKeyForConfirmation";

static NSString *kDepositeAmt;
int depositAmt;


static NSString *const kActionString = @"read"; // the string to trigger action
static NSString *const kTermTag = @"I Accept Myles";

@interface BookingDetailsVC () <UIActionSheetDelegate, SorryAlertProtocol, AddressEditProtocol,CLLocationManagerDelegate,SendCouponDataToBookingScreen>
{
    EMAccordionTableViewController *emTV;
    EMAccordionTableParallaxHeaderView *emParallaxHeaderView;
    
    __weak IBOutlet UILabel *_lblNoSecurityDep;
    __weak IBOutlet UILabel *_lblLdw;
    __weak IBOutlet UITextView *_lblTermsAndCon;
    IBOutlet UIImageView *carImage;
    IBOutlet UILabel *seaterLabel , *carModelLabel , *priceLabel , *userLocationLabel;
    IBOutlet UILabel *addressLabel;
    
    IBOutlet UIView *pickdropInfoView;
    IBOutlet UILabel *picktimeLabel;
    IBOutlet UILabel *droptimeLabel;
    IBOutlet UILabel *pickdateLabel;
    IBOutlet UILabel *dropdateLable;
    
    __weak IBOutlet UIButton *selectPaymentOption;
    IBOutlet UIButton *bookBtn;
    NSString *ktotalBookingAmount , *kSubTotalAmount;
    NSString *kOriginalAmount;
    BOOL preAuth;
    BOOL fairDetailsOpened;
    BOOL isLdwAvailable;
    NSInteger hourlyType ;
    NSString *kBookingid , *kTrackId;
    NSString *offerClicked;
    NSMutableDictionary *kPaymentDict;
    NSInteger lastSelectedButton;
    //Location api to get user location
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
    NSString *strFinalTaxAmount;

    NSTimer *myTimer;
    UserInfoResponse *userDictionary;

    NSString *currentDateTime;
    NSTimeInterval currentMilliSec;
    
    //__weak IBOutlet FRHyperLabel *_lblMandatory;
    IBOutlet UIButton *offerBtn;
    BOOL isFirst;
    BOOL isSecurity;
    BOOL isConvenienceCharge;
    Helper * helper;
    
    UIView *newLaunchView;
    
    BOOL isTermsConditionSelected ;
    __weak IBOutlet UIButton *termsAndConditionButton;
    __weak IBOutlet UILabel *securityAmountInAlertLabel;
    __weak IBOutlet UIView *securityAlertView;
    __weak IBOutlet NSLayoutConstraint *securityAlertViewHeight;
    __weak IBOutlet NSLayoutConstraint *securityRefundAlertSuperViewHeight;
    __weak IBOutlet UIImageView *speedGovernorWarning;
    __weak IBOutlet UILabel *pickUpType;
    __weak IBOutlet UIView *ldwView;
    __weak IBOutlet UILabel *ldwAmount;
    __weak IBOutlet UIImageView *ldwCheckbox;
    __weak IBOutlet UIView *securityDepositeView;
    __weak IBOutlet UILabel *securityAmount;
    __weak IBOutlet UIImageView *securityCheckbox;
    //    NSDate *bookNowRetryInterval;
//    NSDate *paymentRetryInterval;
    //bool bRetry;

    __weak IBOutlet UIView *additionalInformationView;
    __weak IBOutlet UILabel *freeKmLabel;
    __weak IBOutlet UILabel *completeFuelLabel;
    __weak IBOutlet UILabel *additionalKmLabel;
    __weak IBOutlet UILabel *firstSeperatorLabel;
    __weak IBOutlet UILabel *speedGovernerLabel;
    __weak IBOutlet NSLayoutConstraint *heightOfAdditionalView;
    __weak IBOutlet UILabel *durationLabel;
    __weak IBOutlet UILabel *freeKmHeadingLabel;
    __weak IBOutlet NSLayoutConstraint *freeKmHeadingWidthLabel;
    
    __weak IBOutlet UIView *CouponView;
    __weak IBOutlet UILabel *CouponLabel;
    
    __weak IBOutlet UIButton *CouponButton;
    __weak IBOutlet UIImageView *CouponImage;
    
    __weak IBOutlet UIImageView *nextImage;
    __weak IBOutlet UIButton *removeOfferBtn;
    BOOL isCouponApplied;
    
    
    
    __weak IBOutlet NSLayoutConstraint *payViewConstraints;
    
    __weak IBOutlet NSLayoutConstraint *payViewHeight;
    int insuranceSecurityAmt;
    
    
    BOOL isMylesCover;
    BOOL isDone;
    
    PromotionalAlert *containerView;
    BOOL checkboxStatus;
    
    __weak IBOutlet UIView *securityBackView;
    
    __weak IBOutlet UIView *payView;
    
    int contentValue;
    
    NSAttributedString *benefitAtributedStr;
    
    __weak IBOutlet UILabel *plusLabel;
    
    __weak IBOutlet UIView *lineView;
    
    NSString * rideBookingAnalytics;
    NSString * subscriptionsAnalytics;

    
    
    __weak IBOutlet UILabel *reducedSecurityLbl;
    
    NSString * checkBoxButtonStatus;
    __weak IBOutlet UILabel *crossedLbl;
    
    BOOL isNextFeature;
}

@end

@implementation BookingDetailsVC
{
    NSMutableArray *fareDetailsMA;
    NSMutableArray *fareDetailsVAlueMA;
    NSMutableArray *amenitiesMA;
    NSMutableArray *amentiesVAlueMA;
    NSMutableArray *bookingDetailsMA;
    CarDetailInfo *carinfo;
    GSTPackageDetails *gstDetails;
    NSArray *sections;
    CGFloat origin;
    NSString *locationAddress;
    NSString *pickupType;
    NSString *tripDurationSelected;
    NSString *packageTypeSelected;
    NSString *SelectcityId;
    NSString *carImgUrl;
    float additionalCharges;
    float vateAdditionalCharges;
    NSMutableDictionary *detailResponse;
    NSInteger couponStatus;
    NSInteger couponAmount;
    NSString *couponCode;
    int totalAmount;
    BOOL isFromPayment;
}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}
- (NSString *)generateCoricID {
    NSString *str  =  [self getRandomPINString:3];
//    int randomID = arc3random() % 9000 + 100;
    NSTimeInterval secondsSinceUnixEpoch = [[NSDate date]timeIntervalSince1970];
    NSLog(@"secondsin epoch = %f",secondsSinceUnixEpoch);
    NSInteger time = secondsSinceUnixEpoch;
    NSString * trackId = [NSString stringWithFormat:@"CORIC%ld%@",(long)time ,str];
    NSLog(@"trackId =%@",trackId);
    return trackId;
}

-(NSString *)getRandomPINString:(NSInteger)length
{
    NSMutableString *returnString = [NSMutableString stringWithCapacity:length];

    NSString *numbers = @"0123456789";

    // First number cannot be 0
    [returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length]-1))+1]];
    
    for (int i = 1; i < length; i++)
    {
        [returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
    }
    
    return returnString;
}

//- (void)viewDidLayoutSubviews
//{
//    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, 800);
//
//}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [self callAPIForViewBenefits];
    NSLog(@"GST Object Detail: %@", self.bookingGSTPackageDetails);
    //NSLog(@"GST Defaults Detail: %@", [DEFAULTS objectForKey:k_gstDetails]);
    
    isCouponApplied = false;
    [self UpdateCouponStatus:isCouponApplied];
  //  [self emptyAddedView];
    [super viewDidLoad];
    offerClicked = @"No";
    isFromPayment = false;
    fairDetailsOpened = true;
    isSecurity = true;
    isLdwAvailable = false;
    isFirst = true;
    lastSelectedButton = 1;
    if (_doorStepDelieverySelected) {
        editButton.hidden = FALSE;
        
    }
    if ([[DEFAULTS objectForKey:KUSER_EMAIL] length] ==0 || [[DEFAULTS objectForKey:KUSER_LNAME] length] ==0 || [[DEFAULTS objectForKey:KUSER_FNAME] length] ==0 ) {
        
        [CommonApiClass getUserData];

    }
    [self registerForKeyboardNotifications];
    //bRetry =false;
//    bookNowRetryInterval = nil;
//    paymentRetryInterval = nil;
//    
//    [[NSUserDefaults standardUserDefaults] setObject:bookNowRetryInterval forKey:@"bookNowRetryInterval"];
    totalAmount = 0;
   
    
#ifdef DEBUG
#else
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Ride Details"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
#endif
    
    kTrackId = [self generateCoricID];

     
    
    [self getCurrentLocation];
    
    //[self customizeLeftRightNavigationBar:@"Ride Details" RightTItle:k_phonecallRed Controller:self Present:false];
    trackTermsBtnClicked = false;
    //self.leftbackButton.hidden = false;
    //self.leftmenuButton.hidden = true;
    [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    //self.navigationController.navigationBar.translucent = NO;
    
    //addressLabel.text = pickupAddress;
    //bookBtn
    //bookBtn.layer.masksToBounds = YES;
    //bookBtn.layer.cornerRadius = 3.0;
    
    couponStatus = 1;
    couponAmount = 0;
    couponCode = @"";
    fareDetailsMA = [[NSMutableArray alloc]init];
    fareDetailsVAlueMA = [[NSMutableArray alloc]init];
    amenitiesMA = [[NSMutableArray alloc]init];
    amentiesVAlueMA = [[NSMutableArray alloc]init];
    detailResponse = [[NSMutableDictionary alloc]init];
    //addressLabel = [[UILabel alloc]init];
    userLocationLabel = [[UILabel alloc]init];
    // Amount Label
    
   // totalAmountLbl.font = UIFONT_Medium_RobotoFONT(22);
    //totalAmountLbl.textColor =  UIColorFromRedGreenBlue(223., 70., 39.);
    //totalAmountLbl.numberOfLines = 1;
   // totalAmountLbl.textAlignment = NSTextAlignmentCenter;
   // totalAmountLbl.lineBreakMode = NSLineBreakByWordWrapping;
    //totalAmountLbl.backgroundColor = [UIColor clearColor];
    
    coupontxtFd.autocorrectionType = UITextAutocorrectionTypeNo;
    
    // Add Target
    [self.applyCouponBtn addTarget:self action:@selector(applyCouponAction) forControlEvents:UIControlEventTouchUpInside];
    [[self.applyCouponBtn layer] setCornerRadius:6];
    
    if ([carinfo.PkgType isEqualToString:k_hourly])
    {
        hourlyType = 1;
    }
    else
    {
        hourlyType = 0;
    }
    
    
    additionalCharges = 0.0f;
    preAuth = false;
    
    kOriginalAmount = nil;
//    scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    
   // [self setFairAndDetailsSectionContent];
    
    [self checkPaymentTypes: SelectcityId CarCatId : carinfo.CarCatID];
    [self.view layoutIfNeeded];
    
  
    NSArray *timearray = [CommonFunctions gettimeAndDateinDisplayFormet:[self.kBookingDurationMD  valueForKey:@"pDate"]FromTime:[self.kBookingDurationMD  valueForKey:@"pTime"] ToDate:[self.kBookingDurationMD  valueForKey:@"dDate"] ToTime:[self.kBookingDurationMD  valueForKey:@"dTime"] DateFormet:@"MM-dd-yyyy"];
    
  
   //return strDate;
    
    if (timearray)
    {
        pickdateLabel.text = timearray[0];
        dropdateLable.text = timearray[1];
        picktimeLabel.text = timearray[2];
        droptimeLabel.text = timearray[3];
    }
    kBookingid = [[NSString alloc] init];
    [DEFAULTS setObject:nil forKey:k_PickupAddress];
    [DEFAULTS setBool:NO forKey:k_PickupEnabled];
    [DEFAULTS synchronize];
    [self postUserDetailsBeforeBooking];
    [self setupAttributed];
    //[self setAttributeForMandatoryField];
    isTermsConditionSelected = false;
    
    
    
    [_checkboxButton setImage:[UIImage imageNamed:@"checkbox-selected"] forState:UIControlStateSelected];
    
    
    [securityBackView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [securityBackView.layer setBorderWidth:0.5];
    
    securityBackView.layer.shadowRadius  = 5.5f;
    securityBackView.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    securityBackView.layer.shadowOffset  = CGSizeMake(0,0);
    securityBackView.layer.shadowOpacity = 0.9f;
    securityBackView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, 0, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(securityBackView.bounds, shadowInsets)];
    securityBackView.layer.shadowPath    = shadowPath.CGPath;
    
    // code here
    
    
   // [self.view bringSubviewToFront:securityBackView];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, coupontxtFd.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, coupontxtFd.frame.origin.y-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}
- (void)checkPaymentTypes : (NSString *)cityId CarCatId : (NSString *)carCatid
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        
        [body setObject:cityId forKey:@"CityId"];
        [body setObject:@"2205" forKey:@"ClientId"];
        [body setObject: self.subLocationID forKey:@"SubLocationId"];
        [body setObject: carinfo.PkgType forKey:@"PkgType"];
        [body setObject: carinfo.ModelID forKey:@"CarModelId"];
        if(_speedGovernerIsActive){
            [body setObject: @"1" forKey:@"IsSpeedGovernor"];

        }
        else{
            [body setObject: @"0" forKey:@"IsSpeedGovernor"];

        }
        [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"dTime"]] forKey:@"DropOffTime"];
        [body setObject:[self.kBookingDurationMD objectForKey:@"pDate"] forKey:@"fromDate"];
        [body setObject:[self.kBookingDurationMD objectForKey:@"dDate"] forKey:@"DateIn"];
        [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"pTime"]] forKey:@"PickupTime"];
        [body setObject:carinfo.CarCatID forKey:k_CarCatId];
        [body setObject:carinfo.CarVariant forKey:@"CarVariant"]; //New parameter added by Swati
        [body setObject:carinfo.CarVariantID forKey:@"CarVariantID"];//New parameter added by Swati

        //[body setObject: @"2018-02-01" forKey:@"PickupTime"];

        //[body setObject: @"2018-02-02" forKey:@"DropOffTime"];

        
        
        NSLog(@"body for payment z%@", body);
        NSURLSession *session = [CommonFunctions defaultSession];
        [self showLoader];
        
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:@"GetAdditionalServicesWithPaymentOptions" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               
                                               {
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                           NSLog(@"payment response %@", dictionary);
                                                        NSDictionary *response =   [dictionary objectForKey:@"response"];
                                                           if(![[dictionary objectForKey:@"status"] boolValue ] ){
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [SVProgressHUD dismiss];
                                                               });
#ifdef DEBUG
                                                               currentDateTime = [CommonFunctions getCurrentDate];
                                                               currentMilliSec = [CommonFunctions getMilliSecond];
                                                               NSString *milliSec = [CommonFunctions finalMilliSecond:_previousMilliSec current:currentMilliSec];
                                                               [CommonFunctions createStrigForSavingData:@"Search_result_page" key:@"Ride_details_page" startTime:_previousDateTime endTime:(NSString *)currentDateTime milliSec:milliSec];
#endif
                                                              NSString *message = [dictionary objectForKey:@"message"];

                                                               if(message == nil){
                                                                   message = @"Sorry, you seem to have missed this car, the car is not available for booking now. Please search again to book a car and get driving.";
                                                               }
                                                               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Myles" message:message preferredStyle:UIAlertControllerStyleAlert];
                                                               UIAlertAction *alertaction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                   [self.navigationController popToRootViewControllerAnimated:NO];
                                                               }];
                                                               UIAlertAction *alertaction1 = [UIAlertAction actionWithTitle:@"RETRY" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                   //[self.navigationController popViewControllerAnimated:YES];
                                                                   [self checkPaymentTypes: SelectcityId CarCatId : carinfo.CarCatID];
                                                               }];
                                                               [alert addAction:alertaction];
                                                               [alert addAction:alertaction1];
                                                               [self presentViewController:alert animated:YES completion:nil];
                                                           }
                                                           else{
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [SVProgressHUD dismiss];
                                                               });
                                                               
                                                               NSDictionary *paymentOptions =   [response objectForKey:@"PaymentOptions"];
                                                               additionalServiceResponse *service = [[additionalServiceResponse alloc]initWithData:data error:nil];

                                                               if([[paymentOptions objectForKey:@"Available"] boolValue]){
                                                                   
                                                                   isLdwAvailable =  [[paymentOptions objectForKey:@"InsuranceAvailable"] boolValue];
                                                                   
                                                                   
                                                                   
                                                                   NSLog(@"LDW : %d",isLdwAvailable);
                                                                   NSLog(@"PaymentOptions: %@",paymentOptions);
                                                                   //[self setAmenitiesDetailsSectionContent: SelectcityId CarCatId : carinfo.CarCatID];
                                                                   [amenitiesMA removeAllObjects];
                                                                   if(service.response.AditionalServices.count > 0)
                                                                   {
                                                                       dispatch_async(dispatch_get_main_queue(), ^{

                                                                       for (int i = 0 ; i < service.response.AditionalServices.count; i++)
                                                                       {
                                                                           
                                                                           NSMutableDictionary *dict = [NSMutableDictionary new];
                                                                           
                                                                           [dict setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
                                                                           
                                                                           AmenitiesInfo *amenitiesInfo = service.response.AditionalServices[i];
                                                                           
                                                                           [dict setObject:service.response.AditionalServices[i] forKey:@"data"];

                                                                           //AmenitiesInfo *amenitiesInfo = additionalServices[i];

                                                                           
                                                                           if ([amenitiesInfo.ServiceID isEqualToString:@"3"]) {

                                                                               
                                                                           } else {
                                                                               [amenitiesMA  addObject:dict];
                                                                               
                                                                           }
                                                                               
                                                                       }
                                                                       });

                                                                   }
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [self setUpUIshowLDW:false];

                                                                       [self latestSectionOpened];
                                                                       [self setUpPaymentMod: true];
#ifdef DEBUG
                                                                       currentDateTime = [CommonFunctions getCurrentDate];
                                                                       currentMilliSec = [CommonFunctions getMilliSecond];
                                                                       NSString *milliSec = [CommonFunctions finalMilliSecond:_previousMilliSec current:currentMilliSec];
                                                                       [CommonFunctions createStrigForSavingData:@"Search_result_page" key:@"Ride_details_page" startTime:_previousDateTime endTime:(NSString *)currentDateTime milliSec:milliSec];
#endif

                                                                   });
                                                               }
                                                               else{
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [SVProgressHUD dismiss];
                                                                   });
                                                                   NSString *statusStr = [headers objectForKey:@"Status"];

                                                                   NSString *message = [dictionary objectForKey:@"message"];
                                                                   if(message == nil){
                                                                       message = @"Sorry, you seem to have missed this car, the car is not available for booking now. Please search again to book a car and get driving.";
                                                                   }
#ifdef DEBUG
                                                                   currentDateTime = [CommonFunctions getCurrentDate];
                                                                   currentMilliSec = [CommonFunctions getMilliSecond];
                                                                   NSString *milliSec = [CommonFunctions finalMilliSecond:_previousMilliSec current:currentMilliSec];
                                                                   [CommonFunctions createStrigForSavingData:@"Search_result_page" key:@"Ride_details_page" startTime:_previousDateTime endTime:(NSString *)currentDateTime milliSec:milliSec];
#endif
                                                                   UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Myles" message:message preferredStyle:UIAlertControllerStyleAlert];
                                                                   UIAlertAction *alertaction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                       [self.navigationController popToRootViewControllerAnimated:NO];
                                                                   }];
                                                                   [alert addAction:alertaction];
                                                                   [self presentViewController:alert animated:YES completion:nil];
                                                                   
                                                               }
                                                                   
                                                               
                                                               
                                                              
                                                           }
                                                           
                                                           
                                                           
                                                           
                                                           
                                                       }
                                                       
                                                       else
                                                           
                                                       {
#ifdef DEBUG
                                                           currentDateTime = [CommonFunctions getCurrentDate];
                                                           currentMilliSec = [CommonFunctions getMilliSecond];
                                                           NSString *milliSec = [CommonFunctions finalMilliSecond:_previousMilliSec current:currentMilliSec];
                                                           [CommonFunctions createStrigForSavingData:@"Search_result_page" key:@"Ride_details_page" startTime:_previousDateTime endTime:(NSString *)currentDateTime milliSec:milliSec];
#endif
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                           });
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                               
                                                           });
                                                           
                                                           
                                                           
                                                       }
                                                       
                                                       
                                                       
                                                       
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           [SVProgressHUD dismiss];
                                                       });
#ifdef DEBUG
                                                       currentDateTime = [CommonFunctions getCurrentDate];
                                                       currentMilliSec = [CommonFunctions getMilliSecond];
                                                       NSString *milliSec = [CommonFunctions finalMilliSecond:_previousMilliSec current:currentMilliSec];
                                                       [CommonFunctions createStrigForSavingData:@"Search_result_page" key:@"Ride_details_page" startTime:_previousDateTime endTime:(NSString *)currentDateTime milliSec:milliSec];
#endif
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired,  Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                                               //[self.revealViewController revealToggleAnimated:true];
                                                               
                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });
                                                       
                                                       
                                                   }
                                                   else
                                                   {
                                                       
                                                   }
                                                   
                                                   
                                               }];
        
        [globalOperation addOperation:registrationOperation];
        
    }
    
    else
        
    {
        
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
        
    }
    
}

/*- (void)setAmenitiesDetailsSectionContent : (NSString *)cityId CarCatId : (NSString *)carCatid

{
    
    [amenitiesMA removeAllObjects];
    
    if ([CommonFunctions reachabiltyCheck])
        
    {
        
        NSMutableDictionary *body = [NSMutableDictionary new];
        
        [body setObject:cityId forKey:k_cityId];
        
        [body setObject:carCatid forKey:k_CarCatId];
        
        
        
        //additional parameters
        
        [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"dTime"]] forKey:@"DropOffTime"];
        
        [body setObject:[self.kBookingDurationMD objectForKey:@"pDate"] forKey:@"PickUpDate"];
        
        [body setObject:[self.kBookingDurationMD objectForKey:@"dDate"] forKey:@"DropOffDate"];
        
        [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"pTime"]] forKey:@"PickUpTime"];
        
        
        
        
        
        NSURLSession *session = [CommonFunctions defaultSession];
        [self showLoader];
        
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kAdditionalServices completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                       
                                                   {
                                                       
                                                       if (!error)
                                                           
                                                       {
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               
                                                               
                                                               AdditionalServiceInfo *service = [[AdditionalServiceInfo alloc]initWithData:data error:nil];
                                                               
                                                               if(service.response.count > 0)
                                                                   
                                                               {
                                                                   NSLog(@"%@", service.response);

                                                                   for (int i = 0 ; i < service.response.count; i++)
                                                                       
                                                                   {
                                                                       
                                                                       NSMutableDictionary *dict = [NSMutableDictionary new];
                                                                       
                                                                       [dict setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
                                                                       
                                                                       [dict setObject:service.response[i] forKey:@"data"];
                                                                       AmenitiesInfo *amenitiesInfo = service.response[i];

                                                                       if ([carinfo.AirportCharges doubleValue] != 0.0){
                                                                           
                                                                           
                                                                          /* if ([amenitiesInfo.ServiceID isEqualToString:@"1"]){
                                                                               
                                                                               [amenitiesMA  addObject:dict];
                                                                               
                                                                           }*/
                                                                           // nowfal changes
                                                                       
                                                                          /* if ([amenitiesInfo.ServiceID isEqualToString:@"3"]) {
                                                                               
                                                                           } else {
                                                                               [amenitiesMA  addObject:dict];
                                                                               
                                                                           }
                                                                           
                                                                       } else {
                                                                           if ([amenitiesInfo.ServiceID isEqualToString:@"3"]) {
                                                                               
                                                                           } else {
                                                                               [amenitiesMA  addObject:dict];

                                                                           }
                                                                           
                                                                       }
                                                                       
                                                                   }
                                                                   
                                                                   //[tableView reloadData];
                                                                   
                                                               }
                                                               
                                                               else
                                                                   
                                                               {
                                                                   
                                                                   //amenitiesDetailtable.frame = CGRectMake(0, CGRectGetMinY(amenitiesDetailtable.frame), CGRectGetWidth(amenitiesDetailtable.frame), 0);
                                                                   
                                                               }
                                                               
                                                           });
                                                           
                                                       }
                                                       
                                                       else
                                                           
                                                       {
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                               
                                                           });
                                                           
                                                           
                                                           
                                                       }
                                                       
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                           
                                                           
                                                           
                                                           
                                                           [self setUpUI];
                                                           
                                                           [self latestSectionOpened];
                                                           [self setUpPaymentMod: true];
                                                       });
                                                       
                                                       
                                                       
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired,  Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                                               //[self.revealViewController revealToggleAnimated:true];

                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });

                                                       
                                                   }
                                                   else
                                                   {
                                                       
                                                   }
                         
                                                   
                                               }];
        
        [globalOperation addOperation:registrationOperation];
        
    }
    
    else
        
    {
        
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
        
    }
    
}*/


-(void) postUserDetailsBeforeBooking {
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [self getUserDetailsBeforeBookingBody];
        NSLog(@"body =%@",body);
       // [self showLoader];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kUserDetailsBeforeBooking completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                   //    [SVProgressHUD dismiss];
                                                   });
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                       NSLog(@"responseJson =%@",responseJson);
                                                       
                                                       if (!error)
                                                       {
                                                           //                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           //                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           //                                                           NSLog(@"responseJson =%@",responseJson);
                                                           //                                                       });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                           });
                                                       }
                                                       
                                                   } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               [self.navigationController popToRootViewControllerAnimated:TRUE];
                                                               //[self.revealViewController revealToggleAnimated:true];

                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });

                                                   } else
                                                   {
                                                       
                                                   }

                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}

- (NSMutableDictionary *)getUserDetailsBeforeBookingBody
{
    NSMutableDictionary *body = [NSMutableDictionary new];
    NSString *amenitiesStr = @"";
    for (int i = 0; i < amenitiesMA.count; i++)
    {
        NSMutableDictionary *dict = amenitiesMA[i];
        AmenitiesInfo *amenities = [dict objectForKey:@"data"];
        if([[dict objectForKey:@"selected"] boolValue])
        {
            if (amenitiesStr.length)
                amenitiesStr = [amenitiesStr stringByAppendingFormat:@",%@",amenities.ServiceID];
            else
                amenitiesStr = amenities.ServiceID;
        }
    }
    [body setObject:[NSString stringWithFormat:@"%0.f",carinfo.AirportCharges.floatValue] forKey:@"AirportCharges"];
    [body setObject:[NSString stringWithFormat:@"%ld",(long)carinfo.CarCatID.integerValue] forKey:k_CarCatId];
    
    [body setObject:SelectcityId forKey:k_cityId];
    [body setObject:[NSString stringWithFormat:@"%0.2f",carinfo.DepositeAmt.floatValue] forKey:@"DepositAmount"];
    [body setObject:[NSString stringWithFormat:@"%0.2f",carinfo.CessRate.floatValue] forKey:@"CessRate"];
    [body setObject:[NSString stringWithFormat:@"%0.2f",carinfo.CessAmount.floatValue] forKey:@"CessAmount"];
    
    //[body setObject:[NSString stringWithFormat:@"%@",carinfo.CarVariant] forKey:@"CarVariant"];
    //[body setObject:[NSString stringWithFormat:@"%@",carinfo.CarVariantID] forKey:@"CarVariantID"];
    
    // KMIncluded ---------missing
    [body setObject:@"0" forKey:@"KmIncluded"];
    
    //carinfo.ModelID
    [body setObject:carinfo.ModelID forKey:@"ModalId"];
    
    [body setObject:[NSString stringWithFormat:@"%.2f",carinfo.OriginalAmt.floatValue] forKey:@"OriginalAmount"];
    
    //carinfo.OriginalAmt
    [body setObject:[NSString stringWithFormat:@"%@",carinfo.PkgId] forKey:@"PkgId"];
    
    [body setObject:[NSString stringWithFormat:@"%.2f",carinfo.SubLocationCost.floatValue] forKey:@"SubLocationCost"];
    
    [body setObject:[NSString stringWithFormat:@"%0.1f",[carinfo.ToatlFare floatValue]] forKey:@"TotalFare"];
    
    //[body setObject:[NSString stringWithFormat:@"%@",carinfo.TotalDuration] forKey:@"TotalDuration"];
    [body setObject:[NSString stringWithFormat:@"%@",carinfo.TotalDuration] forKey:@"TotalDuration"];
    
    //carinfo.VatAmt   [carinfo.VatRate floatValue]
    [body setObject:[NSString stringWithFormat:@"%0.2f",[carinfo.VatAmt floatValue]] forKey:@"VatAmount"];
    [body setObject:[NSString stringWithFormat:@"%0.3f",[carinfo.VatRate floatValue]] forKey:@"VatRate"];
    
    [body setObject:[NSString stringWithFormat:@"%.3f",carinfo.WeekDayDuration.floatValue] forKey:@"WeekDayDuration"];
    [body setObject:[NSString stringWithFormat:@"%.3f",carinfo.WeekEndDuration.floatValue] forKey:@"WeekEndDuration"];
    
    [body setObject:[self.kBookingDurationMD objectForKey:@"pDate"] forKey:@"PickUpDate"];
    [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"pTime"]] forKey:@"PickUpTime"];
    
    
    [body setObject:[self.kBookingDurationMD objectForKey:@"dDate"] forKey:@"DropOffDate"];
    [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"dTime"]] forKey:@"DropOffTime"];
    
    [body setObject:kTrackId forKey:@"TrackId"];
    
    [body setObject:[NSString stringWithFormat:@"%@",[DEFAULTS objectForKey:KUSER_ID]] forKey:@"UserId"];
    
    [body setObject:[NSString stringWithFormat:@"%@",latitude] forKey:@"Latitude"];
    [body setObject:[NSString stringWithFormat:@"%@",longitude] forKey:@"Longitude"];
    
    [body setObject:@"iOS" forKey:@"Source"];
    [body setObject:@"0" forKey:@"ExtraKmRate"];
    
    [body setObject:[NSString stringWithFormat:@"%.3f",carinfo.FreeDuration.floatValue] forKey:@"FreeDuration"];
    return body;
}


- (void)keyboardWillAppear:(NSNotification *)notification{
    
    if (emTV.sectionsOpened.count > 0) {

    } else {
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= keyboardSize.height;
        if (!CGRectContainsPoint(aRect, coupontxtFd.superview.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, coupontxtFd.superview.frame.origin.y-keyboardSize.height - 50);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

- (void)keyboardDidDismiss
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)viewDidDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewWillAppear:(BOOL)animated
{
    if ([[UIScreen mainScreen] bounds].size.height > 568) {
        
    } else {
        [speedGovernerLabel setFont:[speedGovernerLabel.font fontWithSize: 9.7]];
    }
    
    if (_selectedPackage.TotalDurationForDisplay != nil) {
        durationLabel.text = _selectedPackage.TotalDurationForDisplay;
    }
    if (_selectedPackage.KMIncluded != nil) {
        if (_selectedPackage.KMIncluded.floatValue > 0) {
            // show
            freeKmLabel.text = [NSString stringWithFormat:@"%@ Kms",_selectedPackage.KMIncluded];
        } else {
            // hide free kms
            //freeKmHeadingLabel.hidden = TRUE;
            //freeKmHeadingWidthLabel.constant = -2;
            freeKmLabel.text = @"Unlimited";
        }
    }
    if ([_selectedPackage.PkgType isEqualToString:@"Hourly"]) {
        completeFuelLabel.text = @"Free";
    } else {
        completeFuelLabel.text = @"Full tank to full tank";
    }
    
    if (_selectedPackage.ExtraKMRate != nil) {
        if (_selectedPackage.ExtraKMRate.floatValue > 0) {
            additionalKmLabel.text = [NSString stringWithFormat:@"Additional ₹ %@/Km",_selectedPackage.ExtraKMRate];
        } else {
            additionalKmLabel.text = @"*No Additional KM Charge";
        }
    }
    //Additional ₹ 25/km
    //durationLabel.carde
    if (_speedGovernerIsActive) {
        heightOfAdditionalView.constant = 95;
        speedGovernerLabel.hidden = FALSE;
        firstSeperatorLabel.hidden = FALSE;
        speedGovernorWarning.hidden = FALSE;
    } else {
        heightOfAdditionalView.constant = 55;
        speedGovernerLabel.hidden = TRUE;
        firstSeperatorLabel.hidden = TRUE;
        speedGovernorWarning.hidden = TRUE ;

    }
    if ([DEFAULTS boolForKey:IS_LOGIN]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tabBarController.tabBar setHidden:FALSE];
        });
        
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Ride Details"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardDidShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss) name:UIKeyboardWillHideNotification object:nil];
        });
      
        
        
        if ([DEFAULTS objectForKey:k_PickupAddress] != nil)
        {
            NSMutableDictionary *Dicttemp = [(NSMutableDictionary *)[DEFAULTS objectForKey:k_PickupAddress] mutableCopy];
            NSString *pickupAddress = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",Dicttemp[@"Housenumber"],Dicttemp[@"Locality"],Dicttemp[@"Street"],Dicttemp[@"Landmark"],Dicttemp[@"City"]];
            addressLabel.text = pickupAddress;
            //NSString *amountStr = @"500";
            //[self addAdditionalCharges:[amountStr integerValue] TypeOfservice:@"3" : @""];
            
            //
            //
            
            //
            //        addressLabel.text = pickupAddress;
            //
            //        //Sourabh - Changes for homepick up selected
            //        //Here amount is fixed
            //
            //        for (int i = 0; i <= amenitiesMA.count; i++)
            //        {
            //            NSMutableDictionary *dict = amenitiesMA[i];
            //            NSString *serviceid = [[dict valueForKey:@"data"]valueForKey:@"ServiceID"];
            //            if ([serviceid isEqualToString:@"3"])
            //            {
            //                NSIndexPath *tableindex = [NSIndexPath indexPathForRow:i inSection:1];
            //                DetailCustomCell *cell = (DetailCustomCell *)[tableView cellForRowAtIndexPath:tableindex];
            //                AmenitiesInfo *items = dict[@"data"];
            //                userLocationLabel.hidden = false;
            //                if (![cell.discloseBtn isSelected])
            //                {
            //                    [dict setObject:[NSNumber numberWithBool:YES] forKey:@"selected"];
            //                    [self addAdditionalCharges:[items.Amount integerValue] TypeOfservice:items.ServiceID :items.Description];
            //                    [cell.discloseBtn setSelected:true];
            //                }
            //                if ([[[emTV sectionsOpened] objectAtIndex:1] integerValue] == 1)
            //                [tableView reloadRowsAtIndexPaths:@[tableindex] withRowAnimation:UITableViewRowAnimationFade];
            //                break;
            //            }
            //        }
        }

    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
   /* if(isFromPayment){
        isFromPayment = false;
        isSecurity = true;
        isLdwAvailable = false;
        [self setUpPaymentMod:false];
        [self checkPaymentTypes: SelectcityId CarCatId : carinfo.CarCatID];
        
        
    }*/
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark Terms And Conditions
- (void)openTermsAndConditions
{
    //http://www.mylescars.com/mobiles/terms?name=divya&email=abc.def@gmail.com&mobile=9012345678&city=Delhi
    //http://www.mylescars.com/mobiles/terms?name=" + mUserName +"&email=" + mUserEmail + "&mobile=" + mobileNo + "&city=" + mCityName
    [coupontxtFd resignFirstResponder];
    
    trackTermsBtnClicked = true;
    
    NSString *startDateStr = [self.kBookingDurationMD  valueForKey:@"pDate"];
    NSString *StartTimeStr = [self.kBookingDurationMD  valueForKey:@"pTime"];
    
    NSString *StartMinutes = [StartTimeStr substringFromIndex: [StartTimeStr length] - 2];
    NSString *StartHours = [StartTimeStr substringToIndex:2];
    
    NSString *StartDateAndTimeStr = [NSString stringWithFormat:@"%@ %@%@%@",startDateStr,StartHours,@":",StartMinutes];
    
    
    NSString *endDateStr = [self.kBookingDurationMD  valueForKey:@"dDate"];
    NSString *endTimeStr = [self.kBookingDurationMD  valueForKey:@"dTime"];
    NSString *EndMinutes = [endTimeStr substringFromIndex: [endTimeStr length] - 2];
    NSString *EndHours = [endTimeStr substringToIndex:2];
    
    NSString *endDateAndTimeStr = [NSString stringWithFormat:@"%@ %@%@%@",endDateStr,EndHours,@":",EndMinutes];
    NSInteger numbersOfHours = [self hoursBetweenDays:StartDateAndTimeStr second:endDateAndTimeStr];
    
    NSLog(@"[DEFAULTS objectForKey:KUSER_NAME] =%@",[DEFAULTS objectForKey:KUSER_NAME]);
    NSString *name = [[DEFAULTS objectForKey:KUSER_NAME] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSString *requestUrl  = [NSString stringWithFormat:@"https://www.mylescars.com/mobiles/terms?name=%@&email=%@&mobile=%@&city=%@&bookingHours=%ld",name,[DEFAULTS stringForKey:KUSER_EMAIL],[DEFAULTS stringForKey:KUSER_PHONENUMBER],[DEFAULTS stringForKey:K_LastLocation],(long)numbersOfHours];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    WebViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    controller.UrlString = requestUrl;
    controller.isTermsAndConditions = @"true";
    [[self navigationController] pushViewController:controller animated:YES];
  
    
}


-(NSInteger) hoursBetweenDays:(NSString *)date1 second:(NSString *)date2 {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss a"];
    [formatter setDateFormat:@"YYY-MM-dd HH:mm"];
    NSDate *startDate = [formatter dateFromString:date1];
    NSDate *endDate = [formatter dateFromString:date2];
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    return hoursBetweenDates;
}

#pragma mark- LDW or Security deposit
//mummy
- (IBAction)selectPaymentOption:(UIButton *)sender {
   
    if(sender.tag == 1){
        isSecurity = true;

       
        
    }else{
        isSecurity = false;
        
        
    }
    if(lastSelectedButton != sender.tag){
        lastSelectedButton = sender.tag;
        [self setUpPaymentMod: false];

    }
    
}
- (void)setupAttributed
{
    
    FRHyperLabel *label = self.label;
    label.numberOfLines = 2;
    
    //Step 1: Define a normal attributed string for non-link texts
    NSString *string = @"I Accept Myles Terms & Conditions and Confirm that I am 21 year old.";
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};//,NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    
    //Step 2: Define a selection handler block
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        [self openTermsAndConditions];

    };
    [label setLinksForSubstrings:@[@"Terms & Conditions"] withLinkHandler:handler];
    
    
   
}
- (IBAction)mandatoryDocumentClicked:(id)sender {
    //Veiwed_MandatoryDocs
    [FIRAnalytics logEventWithName:@"Veiwed_MandatoryDocs" parameters:nil];
    [self _btnMandatory];

}

-(void)setAttributeForMandatoryField{
   /* FRHyperLabel *mandatory = _lblMandatory;
    mandatory.numberOfLines = 2;
    
    //Step 1: Define a normal attributed string for non-link texts
    NSString *manString = @"Mandatory Documents & Customer eligibility";
    NSDictionary *attributesMan = @{NSForegroundColorAttributeName: [UIColor blackColor]};//,NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    mandatory.attributedText = [[NSAttributedString alloc]initWithString:manString attributes:attributesMan];
    
    //Step 2: Define a selection handler block
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        [self _btnMandatory];
        
    };
  
    [_lblMandatory setLinksForSubstrings:@[@"Mandatory Documents & Customer eligibility"] withLinkHandler:handler];
    _lblMandatory.textColor = [UIColor blackColor];*/
    
}
- (void)didTapReadTerm:(UITapGestureRecognizer *)sender
{
    UITextView *textView = (UITextView *)sender.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [sender locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    NSLog(@"location: %@", NSStringFromCGPoint(location));
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        NSDictionary *attributes = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        NSLog(@"%@, %@", attributes, NSStringFromRange(range));
          [self openTermsAndConditions];
        //Based on the attributes, do something
        ///if ([attributes objectForKey:...)] //make a network call, load a cat Pic, etc
        
    }
    
    
    
  
    // first: extract the sender view
//    UIView *senderView = sender.view;
//    if (![senderView isKindOfClass:[UILabel class]]) {
//        return;
//    }
//
//    // calculate layout manager touch location
//    UITextView *textView = (UITextView *)senderView;
//    NSLayoutManager *layoutManager = textView.layoutManager;
//    CGPoint location = [sender locationInView:textView];
//    location.x -= textView.textContainerInset.left;
//    location.y -= textView.textContainerInset.top;
//
//    // find the value
//    NSTextContainer *textContainer = textView.textContainer;
//    NSUInteger characterIndex = [layoutManager characterIndexForPoint:location inTextContainer:textContainer fractionOfDistanceBetweenInsertionPoints:NULL];
//    NSTextStorage *textStorage = textView.textStorage;
//    if (characterIndex < textStorage.length) {
//        NSRange range0;
//        id termsValue = [textStorage attribute:kTermTag atIndex:characterIndex effectiveRange:&range0];
//        if (termsValue) {
//            [self openTermsAndConditions];
//            // do something...
//        }
//    }
}

- (void)setUpPaymentMod: (BOOL*) isFromInitialLoad{
    if(isSecurity){ // security deposite
        ldwView.backgroundColor = [UIColor clearColor];
        securityDepositeView .backgroundColor = [UIColor groupTableViewBackgroundColor];
        ldwCheckbox.image = [UIImage imageNamed:k_greyRadio];
        
        securityCheckbox.image = [UIImage imageNamed:@"SelectedRadioButton"];
        
        if(fairDetailsOpened){
            tableViewHeight.constant = tableViewHeight.constant - 25.0;
            contentViewHieghtConstraint.constant -= 25.0;

            
            
        }
//        securityAlertView.hidden = FALSE;
//        securityAlertViewHeight.constant = 50.0;
//        securityRefundAlertSuperViewHeight.constant = 110.0;
        
        securityAlertView.hidden = TRUE;
        securityAlertViewHeight.constant = 0.0;
        securityRefundAlertSuperViewHeight.constant = 50.0;
        
        if(!isFromInitialLoad){
            contentViewHieghtConstraint.constant += 60.0;

        }
    }else{
        ldwView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        securityDepositeView .backgroundColor = [UIColor clearColor];
        ldwCheckbox.image = [UIImage imageNamed:@"SelectedRadioButton"];
        securityCheckbox.image = [UIImage imageNamed:@"blankBoxIcon"];
        if (!isLdwAvailable){
            ldwView.backgroundColor = [UIColor clearColor];
            securityDepositeView .backgroundColor = [UIColor groupTableViewBackgroundColor];
            ldwCheckbox.image = [UIImage imageNamed:k_greyRadio];
            
        }
        if(fairDetailsOpened){
            
           // if (isSecurity){
                tableViewHeight.constant = tableViewHeight.constant + 25.0;
                contentViewHieghtConstraint.constant += 25.0;
           // }

        }
        securityAlertView.hidden = TRUE;
        securityAlertViewHeight.constant = 0.0;
        securityRefundAlertSuperViewHeight.constant = 50.0;
        contentViewHieghtConstraint.constant -= 60.0;


    }
    [self.view layoutIfNeeded];
    [self setFairAndDetailsSectionContent:0.0];
    [self UpdateButtonPrizeLabel:ktotalBookingAmount];
    dispatch_async(dispatch_get_main_queue(), ^{
//        [tableView reloadData];
        
        [self tableViewReloadData];
    });
    
}

#pragma mark- CheckUnCheckButton
- (void)checkUncheckBtnAction : (UIButton *)sender
{
    if ([sender isSelected]) {
        sender.selected = false;
    }
    else
    {
        sender.selected = true;
    }
}

#pragma mark - Edit address for DoorStepDelievery

-(void)doorStepDelieveryEditOption {
    NSLog(@"address Tapped");
    if([carinfo.AirportCharges integerValue] > 0) {
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
        AddHomePickUpAddress *homePickupAddress = (AddHomePickUpAddress *)[storyboard instantiateViewControllerWithIdentifier:@"pickUpAddressIdentifier"];
        homePickupAddress.locationDataObj = _locationDataObj;
        //homePickupAddress.delegate = self;
        //homePickupAddress.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        //homePickupAddress.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        //[self presentViewController:homePickupAddress animated:YES completion:nil];
        [self.navigationController pushViewController:homePickupAddress animated:YES];
    }
    
}

#pragma mark - Edit Adddress

- (void)addactionSheetonAddressEdit
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Edit",@"Reset" ,nil];
    sheet.tag = 1000;
    [sheet showInView:self.view];
}

// Accepting Terms and Conditions before booking

#pragma mark - ACCPT T&C -
-(void)BookingTAndC:(NSString *)transactionId :(NSString *)lat :(NSString *)lng withInsurance:(BOOL)insuranceApproved
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *tempDict = [[self getPostBody] mutableCopy];
        [tempDict setObject:@"New" forKey:@"Tripstatus"];
        
        [tempDict setObject:[NSNumber numberWithBool:insuranceApproved] forKey:@"InsurancePaid"];
        [tempDict setObject:@"0" forKey:@"PreAuthStatus"];
        
        
        [tempDict setObject:carinfo.Model forKey:@"CarModelName"];
        [tempDict setObject:kPaymentDict[@"bookingId"] forKey:@"Bookingid"];
        
        [tempDict setObject:locationAddress forKey:@"sublocationAdd"];
        [tempDict setObject:locationAddress forKey:@"sublocationName"];
        [tempDict setObject:tempDict[@"PickUpDropOffAddress"] != nil ? tempDict[@"PickUpDropOffAddress"]:locationAddress forKey:@"HomePickDropAdd"];
        [tempDict setObject:@"Paid" forKey:@"PaymentStatus"];
        [tempDict setObject:[self srtdate:[self.kBookingDurationMD objectForKey:@"pDate"]] forKey:@"PickupDate"];
        [tempDict setObject:[self srtdate:[self.kBookingDurationMD objectForKey:@"dDate"]] forKey:@"DropoffDate"];
        
        [tempDict setValue:lat forKey:@"Lat"];
        [tempDict setValue:lng forKey:@"Lon"];
        //[self postBookingTAndC:transactionId];
        [self moveToConfirmController:tempDict withInsuranceTaken:insuranceApproved];
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:KAInternet];
    }
}

/*
 By divya to resolve post booking terms and condition bug
 */

-(void)postBookingTAndC:(NSString *)transactionId
{
    if ([CommonFunctions reachabiltyCheck])
    {        
        // https://www.mylescars.com/rests/postTermsResponse?name=Divya%2520Rai&email=divya.rai@mylescars.com&mobileno=8375025985&cityname=Delhi&bookingHours=148&coric=CORICI14745447177977880&status=Yes
        NSString *startDateStr = [self.kBookingDurationMD  valueForKey:@"pDate"];
        NSString *StartTimeStr = [self.kBookingDurationMD  valueForKey:@"pTime"];
        
        NSString *StartMinutes = [StartTimeStr substringFromIndex: [StartTimeStr length] - 2];
        NSString *StartHours = [StartTimeStr substringToIndex:2];
        
        NSString *StartDateAndTimeStr = [NSString stringWithFormat:@"%@ %@%@%@",startDateStr,StartHours,@":",StartMinutes];
        
        
        NSString *endDateStr = [self.kBookingDurationMD  valueForKey:@"dDate"];
        NSString *endTimeStr = [self.kBookingDurationMD  valueForKey:@"dTime"];
        NSString *EndMinutes = [endTimeStr substringFromIndex: [endTimeStr length] - 2];
        NSString *EndHours = [endTimeStr substringToIndex:2];
        
        NSString *endDateAndTimeStr = [NSString stringWithFormat:@"%@ %@%@%@",endDateStr,EndHours,@":",EndMinutes];
        NSInteger numbersOfHours = [self hoursBetweenDays:StartDateAndTimeStr second:endDateAndTimeStr];
        
        
        
        NSString *urlString = [[NSString stringWithFormat:@"https://www.mylescars.com/rests/postTermsResponse?name=%@&email=%@&mobileno=%@&cityname=%@&bookingHours=%ld&coric=%@&status=Yes",[DEFAULTS objectForKey:KUSER_NAME],[DEFAULTS stringForKey:KUSER_EMAIL],[DEFAULTS stringForKey:KUSER_PHONENUMBER],[DEFAULTS stringForKey:K_LastLocation],(long)numbersOfHours,transactionId] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        
        NSURL *requestUrl  = [NSURL URLWithString:urlString];
        NSData *data = [NSData dataWithContentsOfURL:requestUrl];
        if (data)
        {
            NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSString *content = [responseJson objectForKey:@"ResStatus"];
            if ([content isEqualToString:@"success"])
            {
                NSLog(@"bookingConfirmationApi call");
            }
            else
            {
                [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
            }
        }
        else
        {
            [CommonFunctions alertTitle:KSorry withMessage:KAInternet];
        }
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:KAInternet];
    }
}




#pragma mark- Right Button
- (void)rightButton : (UIButton *)sender
{
    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}

#pragma mark - AddressEditProtocol

- (void)getEditOptionSelection:(NSInteger)option{
    
    if (option == 1) {
//        PickupAddressVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
//        vc.addressDataMA = ([DEFAULTS valueForKey:k_PickupAddress]) ? [DEFAULTS valueForKey:k_PickupAddress] : nil;
//        [[self navigationController] pushViewController:vc animated:YES]
//        ;
        if ([CommonFunctions reachabiltyCheck]) {
            [self showLoader];
            [CommonFunctions SavePickUpAddress:@{@"PhoneNo":[DEFAULTS objectForKey:KUSER_PHONENUMBER],@"userId":[DEFAULTS objectForKey:KUSER_ID]} PassUrl:@"RestM1FetchCustomerAddress" :^(NSDictionary *DictSaveAddress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSArray *arrData = DictSaveAddress[@"response"];
                    
                    if (DictSaveAddress.count>0 && DictSaveAddress[@"response"] != nil && [arrData count] > 0) {
                        PickUpAddressList *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PickUpAddressList"];
                        // vc.maPickAddress = DictSaveAddress[@"response"] ;
                        [[self navigationController] pushViewController:vc animated:YES];
                        
                        
                    }
                    else
                    {
                        AddHomePickUpAddress *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
                        [[self navigationController] pushViewController:vc animated:YES];
                    }

                });
                
            }];
        }
        else
        {
            [CommonFunctions alertTitle:nil withMessage:KAInternet];
            
        }
        
    }
    else  if (option == 2)
    {
        [DEFAULTS removeObjectForKey:k_PickupAddress];
        [addressLabel setText:locationAddress];
        for (int i = 0; i < amenitiesMA.count; i++)
        {
            NSMutableDictionary *dict = amenitiesMA[i];
            NSString *serviceid = [[dict valueForKey:@"data"]valueForKey:@"ServiceID"];
            if ([serviceid isEqualToString:@"3"])
            {
                NSIndexPath *tableindex = [NSIndexPath indexPathForRow:i inSection:1];
                DetailCustomCell *cell = (DetailCustomCell *)[tableView cellForRowAtIndexPath:tableindex];
                AmenitiesInfo *items = dict[@"data"];
                userLocationLabel.hidden = true;
                [dict setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
                [self removeAdditionalCharges:[items.Amount integerValue]TypeOfservice:items.ServiceID];
                [DEFAULTS setBool:NO forKey:k_PickupEnabled];
                [cell.discloseBtn setSelected:false];
                if ([[[emTV sectionsOpened] objectAtIndex:1] integerValue] == 1)
                [tableView reloadRowsAtIndexPaths:@[tableindex] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
    }
    else{
        
    }
}

#pragma mark - ViewSetup
-(void)setUpUIshowLDW:(BOOL)showLDW
{
    @try {
        scrollView.scrollEnabled = YES;
        //NSLog(@"", carinfo.)
        
        NSLog(@"Promotion Code: %@", carinfo.PromotionCode);
        
        /*if (carinfo.PromotionCode != nil)
        {
            NSString *strDiscountAmount = carinfo.DiscountAmt;
            couponAmount = strDiscountAmount.integerValue;
            [self UpdateCouponStatus:true];
        }
        else
        {
            couponAmount = 0;
            [self UpdateCouponStatus:false];
        }*/ //Swati Commented This 7 April
        
        NSString *modelPrice  = [carinfo.IndicatedPrice substringToIndex:carinfo.IndicatedPrice.length - 3];
        NSString *packageType  = carinfo.PkgType;
        UIColor *textColor  = [UIColor colorWithRed:65.0f/255.0f green:64.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: packageType attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_REGULAR_Motor(17)}];
        
        NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:modelPrice attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_REGULAR_Motor(20)}];
        
        NSMutableAttributedString *costLabel = [[NSMutableAttributedString alloc]initWithString:@"Rs. " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_REGULAR_Motor(17)}];
        
        [costLabel appendAttributedString:attributeString2];
        [costLabel appendAttributedString:attributedString];
        
        priceLabel.hidden = true;
        
        priceLabel.attributedText = costLabel;
        
        seaterLabel.text = [NSString stringWithFormat:@"%@ Seater", carinfo.SeatingCapacity];
        
        carModelLabel.text = [NSString stringWithFormat: @"%@ l",carinfo.Model];
        
        [carModelLabel sizeToFit];
        
        if (carinfo.CarVariant != nil)
        {
            [self.lblVariantName setHidden:NO];
            self.lblVariantName.text = [NSString stringWithFormat:@"(%@)", carinfo.CarVariant];
        }
        else
        {
            [self.lblVariantName setHidden:YES];
            //self.lblVariantName.text = [NSString stringWithFormat:@"(%@)", carinfo.CarVariant];
        }
        
        tableView.tag = 1000;
        tableView.separatorColor = [UIColor clearColor];
        [tableView registerNib:[UINib nibWithNibName:@"DetailCustomCell" bundle:nil] forCellReuseIdentifier:@"detailCell"];
        [tableView setSectionHeaderHeight:kTableHeaderHeight];
        tableView.scrollEnabled = NO;
        tableView.bounces = NO;
        emTV = [[EMAccordionTableViewController alloc] initWithTable:tableView withAnimationType:EMAnimationTypeNone];
        [emTV setDelegate:self];
        [emTV setClosedSectionIcon:[UIImage imageNamed:@"arrow-down"]];
        [emTV setOpenedSectionIcon:[UIImage imageNamed:@"arrow-up"]];
        emTV.defaultOpenedSection = -1;
        
        EMAccordionSection *section01 = [[EMAccordionSection alloc] init];
        [section01 setBackgroundColor:[UIColor whiteColor]];
        [section01 setItems:fareDetailsMA];
        [section01 setTitle:@"Fare Details"]; //Extra Amenities
        [section01 setTitleFont: [UIFont fontWithName:@"OpenSans-Bold" size:12.5]];
        [section01 setTitleColor:UICOLOR_FontColor];
        
        // SOURABH-- To change the section . Increase one more section i.e Door step deleivery.
        //Change for Doorstepdelievery.
        if ([amenitiesMA count]== 0) {
            [emTV addAccordionSection:section01 initiallyOpened:NO];
            sections = [[NSArray alloc] initWithObjects: section01,nil];
        }
        else {
            EMAccordionSection *section02 = [[EMAccordionSection alloc] init];
            [section02 setBackgroundColor:[UIColor whiteColor]];
            [section02 setItems:amenitiesMA];
            [section02 setTitle:@"Extra Amenities"];
            [section02 setTitleColor:UICOLOR_FontColor];
            [section02 setTitleFont:[UIFont fontWithName:@"OpenSans-Bold" size:12.5]];
            [emTV addAccordionSection:section02 initiallyOpened:NO];
            [emTV addAccordionSection:section01 initiallyOpened:NO];
            sections = [[NSArray alloc] initWithObjects:section02, section01,  nil];
        }
        // addressLabel.font = UIFONT_Medium_RobotoFONT(13.33);
        //addressLabel.textColor = UIColorFromRedGreenBlue(128, 130, 133);
        //addressLabel.numberOfLines = 5;
        // NSMutableAttributedString *addressText = [[NSMutableAttributedString alloc] initWithString:locationAddress];
        //[addressText addAttribute:NSKernAttributeName
        
        //                  value:@(-.5)
        
        //                   range:NSMakeRange(0, locationAddress.length)];
        
        
        
        // addressLabel.text = addressText;
        
        [addressLabel setText:locationAddress];
        
        
        
        //addressLabel.contentMode = UIViewContentModeTop;
        if (_doorStepDelieverySelected) {
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doorStepDelieveryEditOption)];
            [gesture setNumberOfTapsRequired:1];
            addressLabel.userInteractionEnabled = true;
            [addressLabel addGestureRecognizer:gesture];
            [pickUpType setText:@"Home Address:"];
            
        }
        else{
            [pickUpType setText:@"Pickup Location:"];
            
        }
        
        
        
        //
        // userLocationLabel
        userLocationLabel.font = UIFONT_BOLD_RobotoFONT(13.33);
        userLocationLabel.textColor = UIColorFromRedGreenBlue(128, 130, 133);
        userLocationLabel.numberOfLines = 1;
        userLocationLabel.textAlignment = NSTextAlignmentLeft;
        userLocationLabel.lineBreakMode = NSLineBreakByWordWrapping;
        userLocationLabel.backgroundColor = [UIColor clearColor];
        //userLocationLabel.text = @"Car Pick Up / Drop Off";
        userLocationLabel.text = @"Pickup Address";
        
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addactionSheetonAddressEdit)];
        
        [gesture setNumberOfTapsRequired:1 ];
        
        userLocationLabel.userInteractionEnabled = true;
        
        [userLocationLabel addGestureRecognizer:gesture];
        
        
        
        userLocationLabel.hidden = true;
        
        [contentView addSubview:userLocationLabel];
        
        
        
        [contentView addSubview:addressLabel];
        
        [contentView bringSubviewToFront:addressLabel];
        
        
        
        
        
        
        
        // bookBtn.backgroundColor = UIColorFromRedGreenBlue(222., 70., 39.);
        
        //bookBtn.alpha  = 1.0f;
        
        //bookBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [bookBtn addTarget:self action:@selector(bookCarBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
        
        modelPrice = self.totalFare;
        packageType  = [NSString stringWithFormat:@" %@",carinfo.PkgType];
        printf("total insurance amount === %f",[carinfo.TotalInsuranceAmt floatValue]);
        
        ldwAmount.text = [NSString stringWithFormat:@"Rs. %.f",[carinfo.TotalInsuranceAmt floatValue]];
        if(isLdwAvailable && [carinfo.TotalInsuranceAmt floatValue] > 0){
            
            if (!showLDW) {
                isSecurity = true;
                lastSelectedButton = 1;
                
            }else{
                
                isSecurity = false;
                lastSelectedButton = 0;
            }
            
        }
        else{
            lastSelectedButton = 1;
            isSecurity = true;
            ldwAmount.text = @"";
            _lblNoSecurityDep.text = @"(Not available for this Car)";
            _lblNoSecurityDep.textColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
            _lblLdw.textColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
            ldwCheckbox.image = [UIImage imageNamed:k_greyRadio];
            _lblNoSecurityDep.alpha = 0.6;
            ldwCheckbox.alpha = 0.6;
            _lblLdw.alpha = 0.6;
            ldwAmount.alpha = 0.6;
            
            [selectPaymentOption setUserInteractionEnabled:NO];
            [_lblNoSecurityDep setUserInteractionEnabled:false];
        }
        
        securityAmount.text = [NSString stringWithFormat:@"Rs. %.f",[carinfo.DepositeAmt floatValue]];
        securityAmountInAlertLabel.text = [NSString stringWithFormat:@"Rs. %.f",[carinfo.DepositeAmt floatValue]];
        //Attributed String
        [self UpdateButtonPrizeLabel:modelPrice ];
        [self.view addSubview:bookBtn];
        
        [carImage hnk_setImageFromURL:[NSURL URLWithString:carImgUrl] placeholder:[UIImage imageNamed:k_defaultCarFullImg]];
        coupontxtFd.delegate = self;
    } @catch (NSException *exception) {
        NSLog(@"exception %@",exception);
    }
    
    
}

#pragma mark- Fare Table touch event

- (void)adjestTableAndViewSizeAmenitiesCount
{
    CGFloat currentHeight = tableViewHeight.constant;
    tableViewHeight.constant = 80 + amenitiesMA.count * 25.0;
    contentViewHieghtConstraint.constant += tableViewHeight.constant - currentHeight;
//    contentViewHieghtConstraint.constant += 150;
    [self.view layoutIfNeeded];
}

- (void)UpdateButtonPrizeLabel : (NSString *)modelPrice
{
    // NSString *booktext  = @"BOOK NOW";
    //[bookBtn setTitle:booktext forState:UIControlStateNormal];
    
    // round off model price to display on the detail form
    float myTotalAmount = [modelPrice floatValue];
    int rounded = roundf(myTotalAmount);
    modelPrice = [NSNumber numberWithInt:rounded].stringValue;
    //totalAmountLbl.text = [NSString stringWithFormat:@"TOTAL AMOUNT : ₹%@",modelPrice];
    NSString *amount =  [NSString stringWithFormat:@"Book and Pay Rs. %@",modelPrice];
    [UIView setAnimationsEnabled:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        [bookBtn setTitle:amount forState:UIControlStateNormal];
        [UIView setAnimationsEnabled:YES];
        
    });
    
    
    ktotalBookingAmount = modelPrice;
    kDepositeAmt = carinfo.DepositeAmt;
    depositAmt  = [carinfo.DepositeAmt intValue];
    
}

//Set Text Field Right view according to coupon status
- (void)textfieldRightView : (NSString *)imagename
{
    if (imagename)
    {
        coupontxtFd.rightViewMode = UITextFieldViewModeAlways;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-23, 5, 13, 13)];
        imgView.image = UIImageName(imagename);
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(-5, 0, 32, 32)];
        [paddingView addSubview:imgView];
        imgView.center = paddingView.center;
        [coupontxtFd setRightView:paddingView];
    }
    else
    {
        coupontxtFd.rightViewMode = UITextFieldViewModeNever;
    }
}


#pragma mark- GetCarDetails
    - (void) setCarDetails : (NSDictionary *)dic AndAddress : (NSString *)address City : (NSString *)cityId CarImg : (NSString *)carImg pickupType : (NSString *)pickup TripDuration : (NSString *)duration packageType : (NSString*) packaget
{
    carinfo = [[CarDetailInfo alloc]initWithDictionary:dic error:nil];
    //self.
    locationAddress = address;
    SelectcityId = cityId;
    carImgUrl = carImg;
    //pick
    locationAddress = address;
    addressLabel.text = address;

    pickupType =  pickup;
    tripDurationSelected = duration;
    packageTypeSelected = packaget;
    
    insuranceSecurityAmt = [carinfo.InsuranceSecurityAmt intValue];
}

#pragma mark- Fair And Details

- (void) setFairAndDetailsSectionContent:(float)extraAminitiesRate
{
    NSLog(@"%@",carinfo);
    isConvenienceCharge = false;
    [fareDetailsMA removeAllObjects];
    
    float extraAminitiesTaxes = 0.0;
    float airportTaxes = 0.0;
    float basicAmount = 0.0;
    float couponTaxes = 0.0;
    float GSTRate = 0.0;
    float allServiceRate = 0.0;
    float doorStepDeliveryTaxes = 0.0;
    
    NSString *weekDayOrHour =   (hourlyType == 1) ? k_weekdayHourDuration : k_weekdayDuration;
    
    // NSMutableDictionary *weekdayDuration = [[NSMutableDictionary alloc] initWithObjectsAndKeys: carinfo.WeekDayDuration,weekDayOrHour,nil];
    
    //[NSString stringWithFormat:@"%02f", vateAdditionalCharges]
    
    NSMutableDictionary *weekdayDuration = [[NSMutableDictionary alloc] initWithObjectsAndKeys: [NSString stringWithFormat:@"%f", [carinfo.WeekDayDuration floatValue]],weekDayOrHour,nil];
    if (carinfo.PkgRate == (id)[NSNull null] || carinfo.PkgRate.length == 0 ){
        [weekdayDuration setValue:@"0.0" forKey:K_price];
        
    }
    else
    {
        [weekdayDuration setValue:carinfo.PkgRate forKey:K_price];
    }
    //[weekdayDuration setValue:carinfo.PkgRate forKey:K_price];
    
    [fareDetailsMA addObject:weekdayDuration];
    
    weekDayOrHour = (hourlyType == 1) ? k_weekEndHourDuration : k_weekEndDuration;
    //NSMutableDictionary *weekEndDuration = [[NSMutableDictionary alloc] initWithObjectsAndKeys: carinfo.WeekEndDuration,weekDayOrHour,nil];
    
    NSMutableDictionary *weekEndDuration = [[NSMutableDictionary alloc] initWithObjectsAndKeys: [NSString stringWithFormat:@"%f", [carinfo.WeekEndDuration floatValue]],weekDayOrHour,nil];
    if (carinfo.PkgRateWeekEnd == (id)[NSNull null] || carinfo.PkgRateWeekEnd.length == 0 ){
        [weekEndDuration setValue:@"0.0" forKey:K_price];
        
    }
    else{
        [weekEndDuration setValue:carinfo.PkgRateWeekEnd forKey:K_price];
        
    }
    //[weekEndDuration setValue:carinfo.PkgRateWeekEnd forKey:K_price];
    [fareDetailsMA addObject:weekEndDuration];
    
    NSString *freetime =   (hourlyType == 1) ? k_freeHours : k_freedays;
    if ([freetime floatValue] > 0) {
        NSDictionary *freedays = [[NSDictionary alloc] initWithObjectsAndKeys: carinfo.FreeDuration,freetime,nil];
        [fareDetailsMA addObject:freedays];
        
    }
    
    NSString *discountbasefare = [NSString stringWithFormat:@"%.f",[carinfo.FreeDuration floatValue] * [carinfo.PkgRate floatValue]];
    if ([discountbasefare floatValue] > 0) {
        NSDictionary *discountOnbaseFare = [[NSDictionary alloc] initWithObjectsAndKeys: discountbasefare,k_discountonbaseFare,nil];
        [fareDetailsMA addObject:discountOnbaseFare];
    }
    
    //  NSDictionary *minimumBillling = [[NSDictionary alloc] initWithObjectsAndKeys: carinfo.BasicAmt,k_originalCharges,nil];
    //  [fareDetailsMA addObject:minimumBillling];
    
    GSTRate = ([carinfo.CGSTRate floatValue] + [carinfo.IGSTRate floatValue] + [carinfo.SGSTRate floatValue] + [carinfo.UTGSTRate floatValue] + [carinfo.CessRate floatValue])/100;
    
    allServiceRate = ([carinfo.CGSTRate_Service floatValue] + [carinfo.IGSTRate_Service floatValue] + [carinfo.SGSTRate_Service floatValue] + [carinfo.UTGSTRate_Service floatValue])/100;
    
    if ([carinfo.AirportCharges floatValue] > 0) {
        NSDictionary *convenienceCharge = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%.f",[carinfo.AirportCharges floatValue] ],k_convenienceCharge, nil];
        [fareDetailsMA addObject : convenienceCharge];
        float airportAmount = [carinfo.AirportCharges floatValue];
        
        airportTaxes = (airportAmount) * allServiceRate; //([carinfo.CGSTRate_Service floatValue] + [carinfo.IGSTAmount floatValue] + [carinfo.SGSTRate_Service floatValue] + [carinfo.UTGSTRate_Service floatValue])/100;
        isConvenienceCharge = true;
    }
    
    if ([carinfo.SubLocationCost floatValue] > 0) {
        NSDictionary *sublocationCost = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%.f",[carinfo.SubLocationCost floatValue] ],k_sublocationcost, nil];
        [fareDetailsMA addObject : sublocationCost];
    }
    
    if (additionalCharges > 0) {
        NSDictionary *additionalServiceCost = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%.f",additionalCharges],k_AdditionalService, nil];
        [fareDetailsMA addObject : additionalServiceCost];
        self.amenitiesTotalAmount = [additionalServiceCost objectForKey:k_AdditionalService];
        
        float aminitiesAmt = [self.amenitiesTotalAmount floatValue];
        
        //float aminitiesRate = ([self.bookingGSTPackageDetails])/100;
        if (extraAminitiesRate > 0.0)
        {
            extraAminitiesTaxes = (aminitiesAmt) * extraAminitiesRate;
        }
        else
        {
            extraAminitiesTaxes = (aminitiesAmt) * allServiceRate;
        }
        
         //([carinfo.CGSTRate_Service floatValue] + [carinfo.IGSTAmount floatValue] + [carinfo.SGSTRate_Service floatValue] + [carinfo.UTGSTRate_Service floatValue])/100;
    }
    
    if (_doorStepDelieverySelected && (([carinfo.AirportCharges floatValue] == 0.00)))
    {
        // Adding Doorstep Delievery
        NSDictionary *doorStepDelivery = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[DEFAULTS valueForKey:k_HomePickupCharge]],k_DoorStepDelivery, nil];
        [fareDetailsMA addObject:doorStepDelivery];
        
        float doorStepDeliveryAmount = [[DEFAULTS valueForKey:k_HomePickupCharge] floatValue];
        
        doorStepDeliveryTaxes = (doorStepDeliveryAmount) * allServiceRate;
        
        //([carinfo.CGSTRate_Service floatValue] + [carinfo.IGSTAmount floatValue] + [carinfo.SGSTRate_Service floatValue] + [carinfo.UTGSTRate_Service floatValue])/100;
    }
    
    //
    //discount =
    if (couponAmount > 0) {
        NSDictionary *discountAmt = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%.2ld",(long)couponAmount],k_discountAmt, nil];
        [fareDetailsMA addObject : discountAmt];
        basicAmount = [carinfo.BasicAmt floatValue]  - couponAmount;
        couponTaxes =   (basicAmount) * GSTRate; //([carinfo.CGSTRate floatValue] + [carinfo.IGSTRate floatValue] + [carinfo.SGSTRate floatValue] + [carinfo.UTGSTRate floatValue] + [carinfo.CessRate floatValue])/100;
    }
    else
    {
        basicAmount = [carinfo.BasicAmt floatValue]  - couponAmount;
        couponTaxes = (basicAmount) * GSTRate;
    }
    
    //This will depend on if homepickup is selected and selected location is Airport then in that case we have to eliminate the charges of HomePickUp
    //For that we have to maintain a variable to distinguish between these two scenarios.
    //Subtotal amount
    float subtotalAmount;
    //sourabh - add door step charges.
    if (_doorStepDelieverySelected && (([carinfo.AirportCharges floatValue] == 0.00)))
    {
        subtotalAmount = basicAmount + [carinfo.AirportCharges floatValue] + [carinfo.SubLocationCost floatValue] + additionalCharges + [[DEFAULTS valueForKey:k_HomePickupCharge] integerValue];
        
        //subtotalAmount = [carinfo.BasicAmt floatValue]  - couponAmount + [carinfo.AirportCharges floatValue] + [carinfo.SubLocationCost floatValue] + additionalCharges + [[DEFAULTS valueForKey:k_HomePickupCharge] integerValue];
    }
    else
    {
        subtotalAmount = basicAmount + [carinfo.AirportCharges floatValue] + [carinfo.SubLocationCost floatValue] + additionalCharges;
        
        //subtotalAmount = [carinfo.BasicAmt floatValue]  - couponAmount + [carinfo.AirportCharges floatValue] + [carinfo.SubLocationCost floatValue] + additionalCharges;
    }
    
    subtotalAmount = (subtotalAmount <= 0) ? 1 : subtotalAmount;
    NSDictionary *subtotal = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%.f",subtotalAmount],k_SubTotal, nil];
    
    kSubTotalAmount = [NSString stringWithFormat:@"%.f",subtotalAmount];
    
    [fareDetailsMA addObject : subtotal];
    
    self.totalFare = [subtotal objectForKey:k_IndicatedCharges];
    
    //Adding CGST/IGST/SGST/UTGST
    float totalFairAmount = 0.0 ;
    float localCGSTValue = 0.0;
    float localIGSTValue = 0.0;
    float localSGSTValue = 0.0;
    float localUTGSTValue = 0.0;
    float localCessAmount = 0.0;
    
    
    //Swati Commented
    
    /*if ([carinfo.CGSTRate floatValue] > 0) {
        //  printf("carinfo.cgst = %f\n",[carinfo.CGSTRate floatValue]);
        vateAdditionalCharges = (subtotalAmount * [carinfo.CGSTRate floatValue])/ 100 ;
        
        NSDictionary *CGSTCharge = [[NSDictionary alloc]initWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"(%.f%%)",[carinfo.CGSTRate floatValue]],@"CGST",
                                    [NSString stringWithFormat:@"%.2f",vateAdditionalCharges],k_Cgst, nil];
        
        //  printf("CGSTCharge = %f\n",CGSTCharge);
        [fareDetailsMA addObject:CGSTCharge];
        localCGSTValue = (subtotalAmount * [carinfo.CGSTRate floatValue])/ 100 ;
        //totalFairAmount =  subtotalAmount + (subtotalAmount * [carinfo.CGSTRate floatValue])/ 100 ;
        
    }
    
    if ([carinfo.IGSTRate floatValue] > 0) {
        
        //  printf("carinfo.IGSTRate = %f\n",[carinfo.IGSTRate floatValue]);
        vateAdditionalCharges = (subtotalAmount * [carinfo.IGSTRate floatValue])/ 100 ;
        //  printf("vateAdditionalCharges = %f\n",vateAdditionalCharges);
        NSDictionary *IGSTCharge = [[NSDictionary alloc]initWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"(%.f%%)",[carinfo.IGSTRate floatValue]],@"IGST",
                                    [NSString stringWithFormat:@"%.2f",vateAdditionalCharges],k_Igst, nil];
        //    printf("IGSTCharge = %f\n",IGSTCharge);
        [fareDetailsMA addObject:IGSTCharge];
        localIGSTValue = (subtotalAmount * [carinfo.IGSTRate floatValue])/ 100 ;
        
        //totalFairAmount =  subtotalAmount + (subtotalAmount * [carinfo.IGSTRate floatValue])/ 100 ;
        
    }
    
    if ([carinfo.SGSTRate floatValue] > 0) {
        //   printf("carinfo.SGSTRate = %f\n",[carinfo.SGSTRate floatValue]);
        vateAdditionalCharges = (subtotalAmount * [carinfo.SGSTRate floatValue])/ 100 ;
        //   printf("vateAdditionalCharges = %f\n",vateAdditionalCharges);
        NSDictionary *SGSTCharge = [[NSDictionary alloc]initWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"(%.f%%)",[carinfo.SGSTRate floatValue]],@"SGST/UTGST",
                                    [NSString stringWithFormat:@"%.2f",vateAdditionalCharges],k_Sgst, nil]; //"SGST/UTGST" Swati Changes
        [fareDetailsMA addObject:SGSTCharge];
        localSGSTValue = (subtotalAmount * [carinfo.SGSTRate floatValue])/ 100 ;
        
        //        totalFairAmount =  subtotalAmount + (subtotalAmount * [carinfo.SGSTRate floatValue])/ 100 ;
        
    }
    
    if ([carinfo.UTGSTRate floatValue] > 0) {
        //  printf("carinfo.UTGSTRate = %f",[carinfo.UTGSTRate floatValue]);
        vateAdditionalCharges = (subtotalAmount * [carinfo.UTGSTRate floatValue])/ 100 ;
        //  printf("vateAdditionalCharges = %f\n",vateAdditionalCharges);
        NSDictionary *UTGSTCharge = [[NSDictionary alloc]initWithObjectsAndKeys:
                                     [NSString stringWithFormat:@"(%.f%%)",[carinfo.UTGSTRate floatValue]],@"SGST/UTGST",
                                     [NSString stringWithFormat:@"%.2f",vateAdditionalCharges],k_Utgst, nil];
        
        //   printf("UTGSTCharge = %f\n",UTGSTCharge);
        [fareDetailsMA addObject:UTGSTCharge];
        localUTGSTValue = (subtotalAmount * [carinfo.UTGSTRate floatValue])/ 100 ;
    }
     //vateAdditionalCharges = (subtotalAmount * [carinfo.CGSTRate floatValue])/ 100 ;
     
     
     //    NSDictionary *totalTaxes = [[NSDictionary alloc]initWithObjectsAndKeys:
     //                                    [NSString stringWithFormat:@"(%.f%%)",[carinfo.CGSTRate floatValue]],@"CGST",
     //                                    [NSString stringWithFormat:@"%.2f",vateAdditionalCharges],k_Cgst, nil];
    
    
    if ([carinfo.CGSTAmount floatValue] > 0)
    {
        localCGSTValue = [carinfo.CGSTAmount floatValue];
    }
    
    if ([carinfo.SGSTAmount floatValue] > 0) {
        localIGSTValue = [carinfo.SGSTAmount floatValue];
    }
    
    if ([carinfo.UTGSTAmount floatValue] > 0) {
        localSGSTValue = [carinfo.UTGSTAmount floatValue];
    }
    
    if ([carinfo.IGSTAmount floatValue] > 0) {
        localUTGSTValue = [carinfo.IGSTAmount floatValue];
    }
    if ([carinfo.CessAmount floatValue] > 0)
    {
        localCessAmount = [carinfo.CessAmount floatValue];
    }
    
    float taxes = 0.0;
    
    taxes = localCGSTValue + localIGSTValue + localSGSTValue + localUTGSTValue + extraAminitiesTaxes + airportTaxes + couponTaxes;*/     //Swati Commented
    
    
    float taxes = 0.0;

    taxes = extraAminitiesTaxes + doorStepDeliveryTaxes + airportTaxes + couponTaxes;
    int roundedTaxes = roundf(taxes);
    
    strFinalTaxAmount = [NSString stringWithFormat:@"%d", roundedTaxes];
    
    //taxes = localCGSTValue + localIGSTValue + localSGSTValue + localUTGSTValue + extraAminitiesTaxes + airportTaxes + couponTaxes;
    
    
//    NSDictionary * totalTaxes = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%.2f",taxes],@"Taxes", nil];
    
    NSDictionary * totalTaxes = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",roundedTaxes],@"Total Taxes", nil];
        
    printf("taxes = %f\n",taxes);
    [fareDetailsMA addObject:totalTaxes];
    //localCGSTValue = (subtotalAmount * [carinfo.CGSTRate floatValue])/ 100;
    if (taxes <= 0.0)
    {
        taxes = 0.0;
    }
    printf("totalFairAmount: %f\n",totalFairAmount);
    printf("subtotalAmount: %f\n",subtotalAmount);
    printf("localCGSTValue: %f\n",localCGSTValue);
    printf("localIGSTValue: %f\n",localIGSTValue);
    printf("localSGSTValue: %f\n",localSGSTValue);
    printf("localUTGSTValue: %f\n",localUTGSTValue);
    printf("localCessAmount: %f\n",localCessAmount);
    
    totalFairAmount = subtotalAmount + taxes;
    
    //totalFairAmount = subtotalAmount + localCGSTValue + localIGSTValue + localSGSTValue + localUTGSTValue;        //Swati Changes
        
    //Sourabh - commenting
    //    NSDictionary *vaTcharge = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"@%@",carinfo.VatRate],k_VatRate,[NSString stringWithFormat:@"%02f", vateAdditionalCharges],k_VateAmount, nil];
    //
    //    [fareDetailsMA addObject:vaTcharge];
    
    // Total Fare = subtotal * (vat * vatRate )
    //totalFairAmount = (totalFairAmount < 0) ? 1 : totalFairAmount;
    
    //to round off values of totalFairAmount before passing to paynimo
    
    //NSDictionary *totalfair = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%02f",totalFairAmount],k_IndicatedCharges, nil];
    
    if(!isSecurity)
    {
        NSDictionary *insuranceAmt = [[NSDictionary alloc]initWithObjectsAndKeys:carinfo.TotalInsuranceAmt,@"WithINSURANCE", nil];
        
        [fareDetailsMA addObject : insuranceAmt];
        totalFairAmount += [carinfo.TotalInsuranceAmt floatValue];
        
    }
    ktotalBookingAmount = [NSString stringWithFormat:@"%.2f",totalFairAmount];
    int rounded = roundf(totalFairAmount);
    NSString *tempTotal = [NSNumber numberWithInt:rounded].stringValue;
    // NSString *tempTotal = [NSString stringWithFormat:@"%.f",ceil(totalFairAmount)];
    
    if (!kOriginalAmount)
    {
        kOriginalAmount = ktotalBookingAmount;
    }
    else
    {
        //kOriginalAmount = [NSString stringWithFormat:@"%.02f",[kOriginalAmount floatValue]+[self.amenitiesTotalAmount floatValue]];
    }
    NSDictionary *totalfair = [[NSDictionary alloc]initWithObjectsAndKeys:tempTotal,k_IndicatedCharges, nil];
    
    [fareDetailsMA addObject : totalfair];
    
    self.totalFare = [totalfair objectForKey:k_IndicatedCharges];
    
    
    NSLog(@"fareDetailsMA: %@",fareDetailsMA);
    
    // NSDictionary *orStr = [[NSDictionary alloc]initWithObjectsAndKeys:@"OR",@"OR", nil];
    
    //[fareDetailsMA addObject : orStr];
    
    // NSDictionary *depositAmt = [[NSDictionary alloc]initWithObjectsAndKeys:carinfo.DepositeAmt,k_DepositAmt, nil];
    //[fareDetailsMA addObject : depositAmt];
    
}


#pragma mark- UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // [self textfieldRightView:nil];
//    textField.layer.borderColor = UICOLOR_ORANGE.CGColor;
//    textField.layer.borderWidth = 2.0f;
    //[self animateViewMoving:true Value:210];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //textField.layer.borderWidth = 1.0f;
    // [self animateViewMoving:false Value:210];
    [coupontxtFd resignFirstResponder];
    /*
     if(trackTermsBtnClicked == true)
     {
     
     //NSLog(@"Don't need to move view");
     }
     else {
     
     [self animateViewMoving:false Value:210];
     }
     */
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    /*
     if (trackTermsBtnClicked == true) {
     [self animateViewMoving:false Value:210];
     }
     */
    return true;
}

- (void)applyCouponAction
{
    if ([coupontxtFd.text length] > 0) {
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Apply Discount"
                                                              action:@"Click"
                                                               label:@"APPLY"
                                                               value:@1] build]];
        if (couponAmount > 0)
        {
            [coupontxtFd resignFirstResponder];
            couponAmount = 0;
            
            float amount = [kOriginalAmount floatValue]+ [self.amenitiesTotalAmount floatValue];
            //[kOriginalAmount floatValue] + couponAmount ;
            
            ktotalBookingAmount =  [NSString stringWithFormat:@"%.f", amount];
            [self setFairAndDetailsSectionContent:0.0];
            [self UpdateButtonPrizeLabel:ktotalBookingAmount];
            if(fairDetailsOpened){
                tableViewHeight.constant = tableViewHeight.constant - 25.0;
                contentViewHieghtConstraint.constant -= 25.0;
                [self.view layoutIfNeeded];
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
              //  [tableView reloadData];
                [self tableViewReloadData];

            });
            coupontxtFd.rightViewMode = UITextFieldViewModeNever;
            [coupontxtFd setUserInteractionEnabled:YES];
            
            [self.applyCouponBtn setTitle:@"Apply" forState:UIControlStateNormal];
            couponCode = @"";
            coupontxtFd.text = nil;
        }
        else
        {
            [coupontxtFd resignFirstResponder];
            //[self animateViewMoving:true Value:210];
            couponCode  = [coupontxtFd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (couponCode.length > 0)
            {
                couponStatus = 0;
                int roundedUp = ceil([kSubTotalAmount floatValue]);
                
                NSString *bookingamount = (roundedUp >=5 ) ? [NSString stringWithFormat:@"%d",roundedUp] : [NSString stringWithFormat:@"%ld",(long)[kSubTotalAmount integerValue ]];
                
                
                //NSString *totalTmpDuration = [NSString stringWithFormat:@"%.2f",[carinfo.WeekDayDuration floatValue] + [carinfo.WeekEndDuration floatValue]];
                // NSString *totalTmpDuration = [NSString stringWithFormat:@"%.2f",[carinfo.WeekDayDuration floatValue] + [carinfo.WeekEndDuration floatValue]];
                // Sourabh
                // Changed on 29/11/2016
                float BasicAmt = [carinfo.BasicAmt floatValue];
                int rounded = roundf(BasicAmt);
                NSLog(@"%d",rounded);
                
                [self applyCouponCode:carinfo.ModelID
                               CITYID:SelectcityId
                             DISCOUNT:coupontxtFd.text
                           PICKUPDATE:[self.kBookingDurationMD objectForKey:@"pDate"]
                            TOTALFAIR:[NSString stringWithFormat:@"%d",rounded]
                            PACKAGEID:carinfo.PkgId
                      PACKAGEDURATION:carinfo.TotalDuration
                               USERID:[DEFAULTS objectForKey:KUSER_ID]
                          DROPOFFDATE:[self.kBookingDurationMD objectForKey:@"dDate"]
                        SUBLOCATIONID:self.subLocationID
                              WEEKDAY:carinfo.WeekDayDuration
                              WEEKEND:carinfo.WeekEndDuration];
                //WEEKEND:[NSString stringWithFormat:@"%.2f",[carinfo.WeekEndDuration floatValue]]];
            }
            else
            {
                [self showAlertWithMessage:kPleaseEnterCode andTitLe:KSorry];
            }
        }
    } else {
        [self showAlertWithMessage:kPleaseEnterCode andTitLe:KSorry];
    }
    
}

-(void)showAlertWithMessage:(NSString *)messageStr andTitLe:(NSString *)titleStr {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleStr message:messageStr preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertaction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertaction];
        [self presentViewController:alert animated:TRUE completion:nil];
    });
    
}
#pragma mark EMAccordionTableDelegate

- (NSInteger)tableView:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section
{
    printf(" section in nuofrows === %ld",(long)section);
    if (section == 0)
    {
        return amenitiesMA.count ;
    }
   
    else
    {
        return 0;
    }
}
- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 6.0;
    }
    
    return 1.0;
}
- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Fair Details";
}

- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *items = [self dataFromIndexPath:indexPath];
    static NSString *cellIdentifier = @"detailCell";
    DetailCustomCell *customCell;
    
    customCell = (DetailCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // If there is no cell to reuse, create a new one
    if(customCell == nil)
    {
        customCell = [[DetailCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    if (indexPath.section == 0)
    {
        if (amenitiesMA.count >0){
        //SOURABH- Change for Home pick up and drop off
        NSMutableDictionary *dict = items[indexPath.row];
        AmenitiesInfo *amenities = [dict objectForKey:@"data"];
        
        if ([amenities.ServiceID isEqualToString:@"3"]) {
            /// Do nothing in Home pick up drop off
        } else {
            BOOL selectedStatus = [[dict objectForKey:@"selected"] boolValue];
            customCell.discloseBtn.tag = indexPath.row;
            [customCell.discloseBtn addTarget:self action:@selector(updateAdditionalCharges:) forControlEvents:UIControlEventTouchUpInside];
            [customCell setAmenitiesContent:[dict objectForKey:@"data"] Selected: selectedStatus];
            customCell.discloseBtn.hidden = false;
           
           
        }
        }
        else
        {
            if (indexPath.row < 2)
            {
                [customCell setnumberofDays:items[indexPath.row]];
            }
            else if (indexPath.row == 2)
            {
                if (additionalCharges > 0){
                    [customCell setCellContent:items[indexPath.row]];
                }
                if (isConvenienceCharge){
                    [customCell setCellContent:items[indexPath.row]];
                }
                else{
                    NSArray *keyArray = [items[indexPath.row] allKeys];
                    if ( [[keyArray objectAtIndex:0] isEqualToString:k_SubTotal]){
                        [customCell setdiscountonbasePrice:items[indexPath.row]];

                    }
                    else{
                        [customCell setCellContent:items[indexPath.row]];

                    }


                }
                //  [customCell setnumberfreeday:items[indexPath.row]];
            }
            else if (indexPath.row == 3)
            {
                if (additionalCharges > 0){
                    
                }
                NSArray *keyArray = [items[indexPath.row] allKeys];
                if ( [[keyArray objectAtIndex:0] isEqualToString:k_SubTotal]){
                    [customCell setdiscountonbasePrice:items[indexPath.row]];
                    
                }
                else{
                    [customCell setCellContent:items[indexPath.row]];
                    
                }
               // [customCell setCellContent:items[indexPath.row]];
                //[customCell setdiscountonbasePrice:items[indexPath.row]];
            }else
            {
                NSArray *keyArray = [items[indexPath.row] allKeys];
                if ( [[keyArray objectAtIndex:0] isEqualToString:k_SubTotal]){
                    [customCell setdiscountonbasePrice:items[indexPath.row]];
                    
                }
                else{
                    [customCell setCellContent:items[indexPath.row]];
                    
                }            }
            customCell.discloseBtn.hidden = true;
        }
    }
    else
    {
        
        printf("indexpath.row == %ld\n",(long)indexPath.row);

        if (indexPath.row < 2)
        {
            [customCell setnumberofDays:items[indexPath.row]];
        }
        else if (indexPath.row == 2)
        {
            if (additionalCharges > 0){
                 [customCell setCellContent:items[indexPath.row]];
            }
            if (isConvenienceCharge){
                    [customCell setCellContent:items[indexPath.row]];
            }
            else{
                NSArray *keyArray = [items[indexPath.row] allKeys];
                if ( [[keyArray objectAtIndex:0] isEqualToString:k_SubTotal]){
                    [customCell setdiscountonbasePrice:items[indexPath.row]];
                    
                }
                else{
                    [customCell setCellContent:items[indexPath.row]];
                    
                }
                
            }
          //  [customCell setnumberfreeday:items[indexPath.row]];
        }
        else if (indexPath.row == 3)
        {
            if (additionalCharges > 0){
                
            }
            NSArray *keyArray = [items[indexPath.row] allKeys];
            if ( [[keyArray objectAtIndex:0] isEqualToString:k_SubTotal]){
                [customCell setdiscountonbasePrice:items[indexPath.row]];
                
            }
            else{
                [customCell setCellContent:items[indexPath.row]];
                
            }            //[customCell setdiscountonbasePrice:items[indexPath.row]];
        }else
        {
            NSArray *keyArray = [items[indexPath.row] allKeys];
            if ( [[keyArray objectAtIndex:0] isEqualToString:k_SubTotal]){
                [customCell setdiscountonbasePrice:items[indexPath.row]];
                
            }
            else{
                [customCell setCellContent:items[indexPath.row]];
                
            }
            
        }
        customCell.discloseBtn.hidden = true;
    }
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customCell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTableRowHeight;
}


- (void) tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
    DetailCustomCell *cell = (DetailCustomCell *)[tableview cellForRowAtIndexPath:indexPath];
    if (indexPath.section == 1) {
        
        NSMutableDictionary *dict = amenitiesMA[indexPath.row];
        
        AmenitiesInfo *items = dict[@"data"];
        if (([items.ServiceID integerValue] == 3) && (![cell.discloseBtn isSelected]))
        {
            
//            PickupAddressVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
//            vc.addressDataMA = ([DEFAULTS valueForKey:k_PickupAddress]) ? [[DEFAULTS valueForKey:k_PickupAddress] mutableCopy] : nil;
//            [[self navigationController] pushViewController:vc animated:YES]
//            ;
            
            if ([CommonFunctions reachabiltyCheck]) {
                [self showLoader];
                [CommonFunctions SavePickUpAddress:@{@"PhoneNo":[DEFAULTS objectForKey:KUSER_PHONENUMBER],@"userId":[DEFAULTS objectForKey:KUSER_ID]} PassUrl:@"RestM1FetchCustomerAddress" :^(NSDictionary *DictSaveAddress) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                        });
                        
                        if (DictSaveAddress.count>0 && DictSaveAddress[@"response"] != nil && [DictSaveAddress[@"response"] count] > 0) {
                            PickUpAddressList *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PickUpAddressList"];
                           // vc.maPickAddress = DictSaveAddress[@"response"] ;
                            [[self navigationController] pushViewController:vc animated:YES];
                            
                            
                        }
                        else
                        {
                            AddHomePickUpAddress *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
                            [[self navigationController] pushViewController:vc animated:YES];
                        }

                    });
                    
                }];
            }
            else
            {
                [CommonFunctions alertTitle:nil withMessage:KAInternet];

            }
            
            
           
        }
        else
        {
            if ([cell.discloseBtn isSelected])
            {
                userLocationLabel.hidden = true;
                [dict setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
                [self removeAdditionalCharges:[items.Amount integerValue]TypeOfservice:items.ServiceID];
                if ([cell.nameLbl.text isEqualToString:@"Home Pick Up / Drop Off"]) {
                    addressLabel.text = locationAddress;
                    [DEFAULTS removeObjectForKey:k_PickupAddress];
                }
                [cell.discloseBtn setSelected:false];
            }
            else
            {
                [dict setObject:[NSNumber numberWithBool:YES] forKey:@"selected"];
                [self addAdditionalCharges:[items.Amount integerValue]TypeOfservice:items.ServiceID :items.Description];
                [cell.discloseBtn setSelected:true];
            }
        }
    }
    else
    {
        [cell.discloseBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
     */
}


-(void)callAPIForSubscriptionsCheck:(NSString *)string
{
    if ([CommonFunctions reachabiltyCheck]) {
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        //  [self showLoader];
        
        NSMutableDictionary * infoDict = [NSMutableDictionary new];
        [infoDict setValue:[[NSUserDefaults standardUserDefaults]valueForKey:KUSER_ID] forKey:@"userid"];
        [infoDict setValue:string forKey:@"subscribe"];

        
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:infoDict session:session url:@"Updatesubscribe" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                NSJSONSerialization * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                                   NSLog(@"Updatesubscribe: %@",json);
            
            NSString * status = [NSString  stringWithFormat:@"%@",[json valueForKey:@"status"]];
            
            if ([status isEqualToString:@"1"])
            {
                [[NSUserDefaults standardUserDefaults] setValue:string forKey:KUSER_SUBSCRIBE];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSLog(@"Success User Subscribe");
            }
            else{
                NSLog(@"Error User Subscribe");
            }
            
        }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}


-(void)buttonActionYES:(NSString *)yesStr
{
    subscriptionsAnalytics = @"Promo_Consent_Booking_YES";
    [containerView removeFromSuperview];
    [self callAPIForSubscriptionsCheck:@"1"];
}

-(void)buttonActionNO:(NSString *)noStr
{
    subscriptionsAnalytics = @"Promo_Consent_Booking_NO";
    [containerView removeFromSuperview];
    [self callAPIForSubscriptionsCheck:@"0"];
}

- (IBAction)termsAndConditionTapped:(UIButton *)sender {
    
    NSString * subscribeStr = [[NSUserDefaults standardUserDefaults] valueForKey:KUSER_SUBSCRIBE];
    
    NSLog(@"Subscribe: %@",subscribeStr);
    
    if ([subscribeStr isEqualToString:@"2"])
    {
        containerView = [[[NSBundle mainBundle] loadNibNamed:@"AlertView" owner:self options:nil] lastObject];
        [containerView.yesBtn addTarget:self action:@selector(buttonActionYES:) forControlEvents:UIControlEventTouchUpInside];
        [containerView.noBtn addTarget:self action:@selector(buttonActionNO:) forControlEvents:UIControlEventTouchUpInside];

        [containerView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:containerView];
    }
    
    sender.selected = !sender.selected;
    UIColor *disable  = [UIColor colorWithRed:177.0f/255.0f green:178.0f/255.0f blue:179.0f/255.0f alpha:1.0f];
    UIColor *enable  = [UIColor colorWithRed:223.0f/255.0f green:70.0f/255.0f blue:39.0f/255.0f alpha:1.0f];
    if(sender.selected){
          isTermsConditionSelected = true;
        bookBtn.backgroundColor = enable;
       // bookBtn.userInteractionEnabled = true;
    }
    else{
          isTermsConditionSelected = false;
        bookBtn.backgroundColor = disable;
     //   bookBtn.userInteractionEnabled = false;
    }

}


-(IBAction)updateAdditionalCharges:(UIButton *)sender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    DetailCustomCell *cell = (DetailCustomCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSMutableDictionary *dict = amenitiesMA[indexPath.row];
    
    AmenitiesInfo *items = dict[@"data"];
    if (([items.ServiceID integerValue] == 3) && (![cell.discloseBtn isSelected]))
    {
//        PickupAddressVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
//        //vc.addressDataMA = ([DEFAULTS valueForKey:k_PickupAddress]) ? [DEFAULTS valueForKey:k_PickupAddress] : nil;
//        [[self navigationController] pushViewController:vc animated:YES]
//        ;
        
        if ([CommonFunctions reachabiltyCheck]) {
            [self showLoader];
            [CommonFunctions SavePickUpAddress:@{@"PhoneNo":[DEFAULTS objectForKey:KUSER_PHONENUMBER],@"userId":[DEFAULTS objectForKey:KUSER_ID]} PassUrl:@"RestM1FetchCustomerAddress" :^(NSDictionary *DictSaveAddress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSArray *arrData = DictSaveAddress[@"response"];
                    
                    if (DictSaveAddress.count>0 && DictSaveAddress[@"response"] != nil && [arrData count] > 0) {
                        PickUpAddressList *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PickUpAddressList"];
                        // vc.maPickAddress = DictSaveAddress[@"response"] ;
                        [[self navigationController] pushViewController:vc animated:YES];
                        
                        
                    }
                    else
                    {
                        AddHomePickUpAddress *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
                        [[self navigationController] pushViewController:vc animated:YES];
                    }
                });
                
            }];
        }
        else
        {
            [self showAlertWithMessage:KAInternet andTitLe:KSorry];
        }
        
    }
    else
    {
        if ([cell.discloseBtn isSelected])
        {
            
            [dict setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
            [self removeAdditionalCharges:[items.Amount integerValue] TypeOfservice:items.ServiceID];
             [cell.discloseBtn setSelected:!sender.selected];
           // [cell.discloseBtn setSelected:false];
            if ([cell.nameLbl.text isEqualToString:@"Home Pick Up / Drop Off"]) {
                addressLabel.text = locationAddress;
                [DEFAULTS removeObjectForKey:k_PickupAddress];
            }
          

        }
        else
        {
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"selected"];
            [self addAdditionalCharges:[items.Amount integerValue]TypeOfservice:items.ServiceID :items.Description];
            [cell.discloseBtn setSelected: sender.selected];
        }
    }
    
}



#pragma mark- Adding Charges
/*
 - (void) addAdditionalCharges : (NSInteger)prize TypeOfservice : (NSString *)serviceid
 {
 NSInteger days = (hourlyType == 1) ? 0 : ([carinfo.WeekDayDuration integerValue] + [carinfo.WeekEndDuration integerValue]) ;
 NSInteger amenitiesCharges ;
 if (![serviceid isEqualToString:@"3"])
 {
 if (days > 0)
 {
 amenitiesCharges = (prize * days);
 }
 else
 {
 amenitiesCharges = prize;
 }
 }
 else
 {
 amenitiesCharges = prize;
 }
 additionalCharges +=amenitiesCharges;
 [self setFairAndDetailsSectionContent];
 [self UpdateButtonPrizeLabel:ktotalBookingAmount];
 [tableView reloadData];
 }
 */

#pragma mark- Adding Charges
- (void) addAdditionalCharges : (NSInteger)prize TypeOfservice : (NSString *)serviceid :(NSString *)Details
{
    NSInteger days = (hourlyType == 1) ? 0 : ([carinfo.WeekDayDuration integerValue] + [carinfo.WeekEndDuration integerValue]) ;
    
    NSInteger amenitiesCharges;
    //CGFloat amenitiesCharges;
    NSString *strGST;
    strGST = [self.bookingGSTPackageDetails[serviceid.integerValue] valueForKey:@"ApplicableGstRate"];
    float GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
    if (![serviceid isEqualToString:@"3"])
    {
        if (days > 0)
        {
            //amenitiesCharges = (prize * days);
            //for flexi
            //amenitiesCharges = (int)ceil(prize * days);
            NSInteger tmpDays =ceil([carinfo.WeekDayDuration floatValue] + [carinfo.WeekEndDuration floatValue] + [carinfo.FreeDuration floatValue]);
            
            //amenitiesCharges = (int)ceil(prize * tmpDays);
//            CGFloat bookingGST = 0;
//            NSInteger totalAmount = prize * tmpDays;
//            CGFloat GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
//            bookingGST = totalAmount * GSTPercent;//([strGST floatValue]/100);
//            amenitiesCharges = totalAmount + bookingGST; //
            amenitiesCharges = prize * tmpDays;
        }
        else
        {
//            CGFloat bookingGST = 0;
//            NSInteger totalAmount = prize;
//            CGFloat GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
//            bookingGST = totalAmount * GSTPercent;
//            amenitiesCharges = totalAmount + bookingGST; //prize;
            amenitiesCharges = prize;
        }
        NSLog(@"ApplicableGstRate: %@", [self.bookingGSTPackageDetails[serviceid.integerValue] valueForKey:@"ApplicableGstRate"]);
    }
    else
    {
        NSLog(@"ApplicableGstRate: %@", [self.bookingGSTPackageDetails[serviceid.integerValue] valueForKey:@"ApplicableGstRate"]);
//        CGFloat bookingGST = 0;
//        NSInteger totalAmount = prize;
//        CGFloat GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
//        bookingGST = totalAmount * GSTPercent;
//        amenitiesCharges = totalAmount + bookingGST; //prize;
        
        amenitiesCharges = prize;
    }
    // find amenities
    [self addExtraAmenities:serviceid :Details :amenitiesCharges];
    //TotalDuration
    // additionalServiceCost = additionalServiceCost + (int)(Math.ceil( Double.parseDouble(mPkgDuration))) * Double.parseDouble(price);
    // additionalCharges = additionalCharges + (int)(ceil(TotalDuration)) *price;
    
    //float roundedup = ceil(otherfloat);
    // additionalCharges= additionalCharges + (int)ceil(days)*prize
    
    additionalCharges +=amenitiesCharges;
    [self setFairAndDetailsSectionContent:GSTPercent];
    [self UpdateButtonPrizeLabel:ktotalBookingAmount];
    dispatch_async(dispatch_get_main_queue(), ^{
//        [tableView reloadData];
        [self tableViewReloadData];

    });
}
NSMutableArray *maExtraAmenities;
-(NSArray*)addExtraAmenities :(NSString *)serviceId :(NSString *)desc :(int )totalPrize
{
    if (maExtraAmenities == nil)
        maExtraAmenities = [[NSMutableArray alloc]init];
    
    
    bool is = NO;
    for(NSDictionary *dic in maExtraAmenities){
        if([dic[@"aminiti_id"] isEqualToString:serviceId])
        {
            is = YES;
            break;
            
        }
    }
    
    if (!is)
    {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        dict[@"aminiti_id"] = serviceId;
        dict[@"amount"] = [NSString stringWithFormat:@"%d",totalPrize];
        dict[@"detail"] = desc;
        [maExtraAmenities addObject:dict];
    }
    
    if(maExtraAmenities.count == 1){
        if(fairDetailsOpened){
            tableViewHeight.constant = tableViewHeight.constant + 25.0;
            contentViewHieghtConstraint.constant += 25.0;
            [self.view layoutIfNeeded];
            
        }
    }
    return maExtraAmenities;
}

#pragma mark- Subtract Charges
- (void) removeAdditionalCharges : (NSInteger)prize TypeOfservice : (NSString *)serviceid
{
    NSInteger days = (hourlyType == 1) ? 0 : ([carinfo.WeekDayDuration integerValue] + [carinfo.WeekEndDuration integerValue]) ;
    NSInteger amenitiesCharges ;
    NSString *strGST;
    strGST = [self.bookingGSTPackageDetails[serviceid.integerValue] valueForKey:@"ApplicableGstRate"];
    float GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
    if (![serviceid isEqualToString:@"3"])
    {
        if (days > 0)
        {
            //amenitiesCharges = (prize * days);
            NSInteger tmpDays =ceil([carinfo.WeekDayDuration floatValue] + [carinfo.WeekEndDuration floatValue] +  [carinfo.FreeDuration floatValue]);
            
            //amenitiesCharges = (int)ceil(prize * tmpDays);
//            CGFloat bookingGST = 0;
//            NSInteger totalAmount = prize * tmpDays;
//            CGFloat GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
//            bookingGST = totalAmount * GSTPercent;
//            amenitiesCharges = totalAmount + bookingGST; //prize;
            amenitiesCharges = prize * tmpDays;
            
        }
        else
        {
//            CGFloat bookingGST = 0;
//            NSInteger totalAmount = prize;
//            CGFloat GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
//            bookingGST = totalAmount * GSTPercent;
//            amenitiesCharges = totalAmount + bookingGST; //prize;
            amenitiesCharges = prize;
        }
    }
    else
    {
        //amenitiesCharges = prize;
//        CGFloat bookingGST = 0;
//        NSInteger totalAmount = prize;
//        CGFloat GSTPercent =  [[NSString stringWithFormat:@"%.2f", (strGST.floatValue)/100] floatValue];
//        bookingGST = totalAmount * GSTPercent;
//        amenitiesCharges = totalAmount + bookingGST; //prize;
        amenitiesCharges = prize;
    }
    if (maExtraAmenities.count == 1){
        if(fairDetailsOpened){
            tableViewHeight.constant = tableViewHeight.constant - 25.0;
            contentViewHieghtConstraint.constant -= 25.0;

            [self.view layoutIfNeeded];
            
        }
        maExtraAmenities = nil;
    }
    else
    {
        for (NSDictionary *tempDict in maExtraAmenities)
        {
            if ([tempDict[@"aminiti_id"] isEqualToString:serviceid])
            {
                [maExtraAmenities removeObject:tempDict];
                break;
                
            }
        }
    }
    
    additionalCharges -=amenitiesCharges;
    [self setFairAndDetailsSectionContent:GSTPercent];
    [self UpdateButtonPrizeLabel:ktotalBookingAmount];
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        [tableView reloadData];
        [self tableViewReloadData];

    });
}



#pragma mark- Get array Index Wise
- (NSMutableArray *) dataFromIndexPath: (NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (amenitiesMA.count == 0){
            return fareDetailsMA;
        }
      return amenitiesMA;
    }
    else if (indexPath.section == 1)
           return fareDetailsMA;
    return NULL;
}
#pragma mark- Height
// Mainitaining Screen layouts as per table expanded/collapsed sections

- (void) latestSectionOpened
{
    fairDetailsOpened = false;
    float height = 0;
    int noOfSection = amenitiesMA.count == 0 ? 1 : 2;
    for (int  i = 0; i < noOfSection; i++)
    {
        printf("numberofsection ==== %d",noOfSection);
   //     if ([emTV sectionsOpened].count > 0){
        if ([[[emTV sectionsOpened] objectAtIndex:i] integerValue] == 1)
            
        {
            printf("height ==== %f",height);
            if (noOfSection ==2){
                if(i == 1){
                    fairDetailsOpened = true;
                }
                     height += (i == 0)? (amenitiesMA != nil ? (amenitiesMA.count*25) : 0):(fareDetailsMA.count*25) ;
            }
            else{
                fairDetailsOpened = true;
                height = fareDetailsMA.count *25;
                   height += (i == 0)? 0 :(fareDetailsMA.count*25) ;
            }
           
            printf("height ==== %f",height);
            
       
            printf("height ==== %f",height);
        }
    //    }
        
    }
    tableViewHeight.constant = (emTV.tableView.sectionHeaderHeight *2 )+height;

    if (amenitiesMA == nil || amenitiesMA.count == 0) {
        tableViewHeight.constant = (emTV.tableView.sectionHeaderHeight *1 )+height;

    }
    
    contentViewHieghtConstraint.constant = 610 + tableViewHeight.constant+contentValue;
    //contentViewHieghtConstraint.constant += 80;
    if (_speedGovernerIsActive) {
        contentViewHieghtConstraint.constant += 25;
    }
    if(isFirst){
        isFirst = false;
    }
    else{
        if(!isSecurity){
            contentViewHieghtConstraint.constant -= 50;

        }
    }
    [self.view layoutIfNeeded];
    
}

#pragma mark -
#pragma mark - BOOK CAR (PAYMENT) -


- (IBAction)editAddress:(id)sender {
    
    NSLog(@"address Tapped");
    if([carinfo.AirportCharges integerValue] > 0) {
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
        AddHomePickUpAddress *homePickupAddress = (AddHomePickUpAddress *)[storyboard instantiateViewControllerWithIdentifier:@"pickUpAddressIdentifier"];
        homePickupAddress.locationDataObj = _locationDataObj;
        //homePickupAddress.delegate = self;
        //homePickupAddress.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        //homePickupAddress.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        //[self presentViewController:homePickupAddress animated:YES completion:nil];
        [self.navigationController pushViewController:homePickupAddress animated:YES];
    }
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
//    AddressEditViewController *editController = (AddressEditViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AddressEditViewController"];
//    editController.delegate = self;
//    editController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    editController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:editController animated:YES completion:nil];
}


- (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}


/*
 Function to test whether user is above 23 or not
*/
-(BOOL)showAgeAlert {
    
    NSString *birthDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"dob"];
    if (birthDate != nil && birthDate.length > 0 && ![birthDate isEqualToString:@"1/1/1900 12:00:00 AM"]) {
        NSArray *arrBitrhD = [birthDate componentsSeparatedByString:@" "];
        //NSDate *todayDate = [NSDate date];
        //NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setDateFormat:@"MM/dd/yyyy"];
        //int time = [todayDate timeIntervalSinceDate:[dateFormatter dateFromString:arrBitrhD[0]]];
        //int allDays = (((time/60)/60)/24);
        //int days = allDays%365;
        //int years = (allDays-days)/365;
        //NSDate* birthday = ...;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"MM/dd/yyyy";

        //dateFormatter.locale = [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol];
        //[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        // see https://developer.apple.com/library/ios/qa/qa1480/_index.html
        
        NSDate *abc = [dateFormatter dateFromString:arrBitrhD[0]];
        //NSDate *birthdaydate = [dateFormatter dateFromString:abc];
        NSDate* now = [NSDate date];
        NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                           components:NSCalendarUnitYear
                                           fromDate:abc
                                           toDate:now
                                           options:0];
        NSInteger age = [ageComponents year];
        NSLog(@"You live since %li years and days",(long)age);
        if (age >= 21) {
            return TRUE;
        } else {
            return FALSE;
        }

    } else {
        
        //Send user tp profile page to update dob.
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Please update your date of birth on your profile" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* goToProfile = [UIAlertAction actionWithTitle:@"Update DOB" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            dispatch_async(dispatch_get_main_queue(), ^{

                
                
                [self fetchUserProfile:^(bool idGetDOB) {
                    
                    if (!idGetDOB) {
                        /**
                         Select Date of Birth
                         */
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n"
                                                      message:nil
                                                      preferredStyle:UIAlertControllerStyleActionSheet];
                        
                        
                        UIDatePicker *startTimePicker = [[UIDatePicker alloc] init];
                        
                        [startTimePicker setDatePickerMode:UIDatePickerModeDate];
                        startTimePicker.minuteInterval= 25;
                        startTimePicker.timeZone = [NSTimeZone localTimeZone];
                        [startTimePicker setMaximumDate: [NSDate date]];
                        [alert.view addSubview:startTimePicker];
                        [alert addAction:({
                            
                            UIAlertAction *action = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                
                                
                                // [cell.btnDob setTitle:[self IndianTimeConverter:startTimePicker.date] forState:UIControlStateNormal];
                                
                                
                                NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
                                NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
                                
                                
                                [body setObject: [NSString stringWithFormat:@"%@",userid] forKey:@"userid"];
                                // [body setObject: [NSString stringWithFormat:@"%@",md5Password] forKey:@"password"];
                                //                [updatedInfo setValue:[NSString stringWithFormat:@"%@ %@",[updatedInfo valueForKey:@"FName"],[updatedInfo valueForKey:@"LName"]] forKey:@"fullName"];
                                //NSLog(@"Before fullName =%@",[updatedInfo valueForKey:@"fullName"]);
                                [body setObject:userDictionary.FName forKey:@"fname"];
                                [body setObject:userDictionary.fullName forKey:@"fullName"];
                                [body setObject:userDictionary.LName forKey:@"lname"];
                                [body setObject: userDictionary.emailId forKey:@"emailid"];
                                [body setValue: userDictionary.ResAdd forKey:@"resadd"];
                                //body setObject: @"" forKey:@"peradd"];
                                //[updatedInfo valueForKey:@"ResAdd"] forKey:@"resadd"]
                                [body setValue: userDictionary.ResAdd forKey:@"peradd"];
                                
                                NSString *strDob = [CommonFunctions IndianTimeConverter:startTimePicker.date];
                                
                                [body setObject: strDob forKey:@"dob"];
                                
                                 NSString *setDateSucess = [CommonFunctions convertDate:strDob];
                                
                    
                                
                                NSURLSession *session = [CommonFunctions defaultSession];
                                [self showLoader];
                                NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
                                APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSERINFO_UPDATE completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [SVProgressHUD dismiss];
                                                                           });
                                                                           NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                                           NSString *statusStr = [headers objectForKey:@"Status"];
                                                                           if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                                           {
                                                                               if (!error)
                                                                               {
                                                                                   
                                                                                   //NSLog(@"strData : %@",strData);
                                                                                   NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                                                   //NSLog(@"RES : %@",responseJson);
                                                                                   
                                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                                       [self AlertView:[responseJson objectForKey:@"message"]];
                                                                                       
                                                                    [DEFAULTS setValue:setDateSucess forKey:@"dob"];

                                                                                     
                                                                                   });
                                                                               }
                                                                           } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                                   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired,  Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                                                   
                                                                                   UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                                                       [CommonFunctions logoutcommonFunction];
                                                                                       [self.navigationController popToRootViewControllerAnimated:true];
                                                                                       
                                                                                   }];
                                                                                   [alertController addAction:ok];
                                                                                   
                                                                                   [self presentViewController:alertController animated:YES completion:nil];
                                                                               });
                                                                           } else
                                                                           {
                                                                              
                                                                           }
                                                                           
                                                                           
                                                                       }];
                                [globalOperation addOperation:registrationOperation];
                                
                                
                                
                            }];
                            action;
                        })];
                        
                        [alert addAction:({
                            UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                NSLog(@"cancel");
                            }];
                            action;
                        })];
                        [self presentViewController:alert animated:YES completion:nil];
                        

                    }
                    
                }];
                
                
                
               // sasas
//                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_profileStoryBoard bundle:nil];
//                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_profileNavigation];
//                UserProfileVC *userProfileController = [storyBoard instantiateViewControllerWithIdentifier:k_profileController];
//                navController.viewControllers = @[userProfileController];
//                [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:2];
//                [(MenuViewController *)self.revealViewController.rearViewController setIndexValue:2];
//                [[(MenuViewController *)self.revealViewController.rearViewController menuTable] reloadData];
//                [self.revealViewController pushFrontViewController:navController animated:true];
//                [self.revealViewController revealToggleAnimated:true];
                
                
            });
        }];
        [alertController addAction:goToProfile];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            
        }];
        [alertController addAction:cancel];

        
        [self presentViewController:alertController animated:YES completion:nil];
        return FALSE;

    }
}
- (void)_btnMandatory {
    NSString *requestUrl  = [NSString stringWithFormat:@"https://www.mylescars.com/bookings/key_rental"];

#if DEBUG
    requestUrl  = [NSString stringWithFormat:@"https://qa2.mylescars.com/bookings/key_rental"];

    //requestUrl  = [NSString stringWithFormat:@"https://staging.mylescars.com/bookings/key_rental"];

#else
    requestUrl  = [NSString stringWithFormat:@"https://www.mylescars.com/bookings/key_rental"];
#endif
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    WebViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    controller.UrlString = requestUrl;
    controller.isTermsAndConditions = @"false";
    [[self navigationController] pushViewController:controller animated:YES];
}


/*
 Code for  the 'BOOK NOW' Button action
 */
-(void)bookCarBtnTouched:(UIButton *)sender
{
    
    if (!isTermsConditionSelected){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Terms & Conditions" message:@"Please accept Myles Terms and Conditions" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* call = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
            [scrollView setContentOffset:bottomOffset animated:YES];
            UIColor *enable  = [UIColor colorWithRed:223.0f/255.0f green:70.0f/255.0f blue:39.0f/255.0f alpha:1.0f];
            bookBtn.backgroundColor = enable;
            termsAndConditionButton.selected = true;
            isTermsConditionSelected = true;
            
        }];
       
        [alertController addAction:call];
     
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
        
    }
    if (totalAmount>0 && [ktotalBookingAmount intValue] != totalAmount)
    {
        kTrackId = [self generateCoricID];
    }
    //
    
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Rental Payment"
                                                          action:@"Click"
                                                           label:@"BOOK NOW"
                                                           value:@1] build]];
    
    //Sourabh- Adding Events Log //begin_checkout
   // [FIRAnalytics logEventWithName:kFIREventBeginCheckout
                     //   parameters:@{kFIRParameterItemName:carinfo.Model,
                                    // kFIRParameterItemID:carinfo.ModelID
                                    // }];
    
    // On booking: booking creation date, car city, car book date, car type, amount paid
    //carinfo.CarCatName
    // carinfo.CarCatName
    
    //carinfo.CarCatName
    //[self.kBookingDurationMD objectForKey:@"pDate"]
    //[self.kBookingDurationMD objectForKey:@"pDate"]
    
    //NSLog(@"----- ----- ----- %@, %@, %@, %@, %@",carinfo.CarCatName,carinfo.CityName,[self.kBookingDurationMD objectForKey:@"pDate"],[self.kBookingDurationMD objectForKey:@"dDate"]);

    // mommmmm
    [FIRAnalytics logEventWithName:@"booknow_clicked_ride_details"
                        parameters:@{@"pickup_city":carinfo.CityName,
                                     @"pickup_type":pickupType,
                                     @"selected_package":_selectedPackage.PkgDescription,
                                     @"selected_car_category":carinfo.CarCatName,
                                     @"fuel_type":carinfo.FuelType,
                                     @"transmission_type":carinfo.TransmissionType,
                                     @"trip_duration":tripDurationSelected,
                                     @"Package_type":packageTypeSelected,
                                     @"model_id":carinfo.ModelID,
                                     @"model_name":carinfo.Model,
                                     @"is_Offer_clicked": offerClicked
                                     }];
    if (coupontxtFd.text.length > 0)
    {
        [coupontxtFd resignFirstResponder];
    }
    //First status
    
    if (couponStatus == 1)
    {
        // hide terms & conditions check.
      //  if ([checkUncheckBtn isSelected])
        //{
            
            //Check Whether user is more than 23 years of age then only allow user to proceed or
            //show message
        [self showLoader];
        [self fetchUserProfile:^(bool idGetDOB) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            if (idGetDOB) {
            
                if ([self showAgeAlert]) {
                    totalAmount = [ktotalBookingAmount intValue];
                    
                    [self sendingDataToFIRAnalytics];
                    [self transferDataOnJuspay];
                } else {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"User should be at least 21 Years of age to make this booking" preferredStyle:UIAlertControllerStyleAlert];
                
                    UIAlertAction* call = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    }];
                    [alertController addAction:call];
                
                [   self presentViewController:alertController animated:YES completion:nil];
                }
            }
            else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"User should be at least 21 Years of age to make this booking" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* call = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                }];
                [alertController addAction:call];
                
                [   self presentViewController:alertController animated:YES completion:nil];
            }
            
        }];
          /*
        }
        else
        {
            [CommonFunctions alertTitle:KSorry withMessage:kTermsAndConditions];
        }
        */
    }
    
}
//-(IBAction)retry:(id)sender
//{
//    bRetry = false;
//}

//RecheckCarAvailability
/*
 
 {"DropOffTime":"1400","CarModelId":"124","CityId":"2","PickupTime":"1400","fromDate":"2016-02-11","DateIn":"2016-02-12","SubLocationId":"121","ClientId":"2205","PkgType":"Daily"}
 */



-(void)sendingDataToFIRAnalytics
{
    if ([rideBookingAnalytics isEqualToString:@"MylesSecureAvailable"])
    {
        [FIRAnalytics logEventWithName:@"Myles_Secure_Available" parameters:nil];
    }
    else if ([rideBookingAnalytics isEqualToString:@"MylesSecureNA"])
    {
        [FIRAnalytics logEventWithName:@"Myles_Secure_NA" parameters:nil];
    }
    else if ([rideBookingAnalytics isEqualToString:@"BookingWithMS"])
    {
        [FIRAnalytics logEventWithName:@"Booking_With_MS" parameters:nil];
    }
    else if ([rideBookingAnalytics isEqualToString:@"BookingWithSD"])
    {
        [FIRAnalytics logEventWithName:@"Booking_With_SD" parameters:nil];
    }
    
    
    if ([subscriptionsAnalytics isEqualToString:@"Promo_Consent_Booking_YES"])
    {
        [FIRAnalytics logEventWithName:@"Promo_Consent_Booking_YES" parameters:nil];
    }
    else if ([subscriptionsAnalytics isEqualToString:@"Promo_Consent_Booking_NO"])
    {
        [FIRAnalytics logEventWithName:@"Promo_Consent_Booking_NO" parameters:nil];
    }
}



-(void)onRecheckCarAvailabilityFailure {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.alertType    = 1;
    controller.navController = self.navigationController;
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];
    
}


-(void) BeforeBookingCarAvailability {
    /*
     RestM1RecheckCarAvailability(string CityId, string ClientId, string fromDate, string DateIn, string PickupTime, string DropOffTime, string SubLocationId, string PkgType, string CarModelId, string CoricId)

     
     */
    
    
    if ([CommonFunctions reachabiltyCheck]) {
        
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject: [self.kBookingDurationMD objectForKey:@"dTime"] forKey:@"DropOffTime"];
        [body setObject:carinfo.ModelID  forKey:@"CarModelId"];
        [body setObject:SelectcityId  forKey:@"CityId"];
        [body setObject:[self.kBookingDurationMD objectForKey:@"pTime"]  forKey:@"PickupTime"];
        [body setObject:[self.kBookingDurationMD objectForKey:@"pDate"]  forKey:@"fromDate"];
        [body setObject:[self.kBookingDurationMD objectForKey:@"dDate"]  forKey:@"DateIn"];
        [body setObject:self.subLocationID  forKey:@"SubLocationId"];
        [body setObject:@"2205"  forKey:@"ClientId"];
        [body setObject:carinfo.PkgType  forKey:@"PkgType"];
        [body setObject:kTrackId forKey:@"CoricId"];
        NSLog(@"Body =%@",body);
        
        /*DropOffTime, CarModelId, CityId, PickupTime, fromDate, DateIn, SubLocationId, ClientId, PkgType, CoricId
        (TripInsurance, IsSpeedGovernor, CarVariantId)*/
        
        
        [self showLoader];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:@"RestM1RecheckCarAvailability" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               //{ "BookingStatus": "Valid", "CarAvailability": 0 }
                                                               if ([[(NSMutableDictionary *)responseJson objectForKey:@"BookingStatus"] isEqualToString:@"Valid" ])
                                                               {
                                                                   //NSLog(@"responseJson = %@",responseJson);
                                                                   if ([[(NSMutableDictionary *)responseJson objectForKey:@"CarAvailability"] integerValue] == 1)
                                                                   {
                                                                       if ([self amountZeroCheck])
                                                                       {
                                                                           [self transferDataOnJuspay];
                                                                       }
                                                                   }
                                                                   else {
                                                                       [self onRecheckCarAvailabilityFailure];
                                                                       
                                                                   }
                                                               }else{
                                                                   //bRetry = false;
                                                               }
                                                           });
                                                        }
                                                       
                                                       
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               [self.navigationController popToRootViewControllerAnimated:TRUE];
                                                               //[self.revealViewController revealToggleAnimated:true];
                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });
                                                } else
                                                   {
                                                      
                                                   }

                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}


- (BOOL)amountZeroCheck
{
    BOOL status = false;
    if ((preAuth) &&  ([kDepositeAmt integerValue ] > 0 )) {
        status = true;
    }
    else if (preAuth)
    {
        [self moveToConfirmController:detailResponse withInsuranceTaken:NO];
    }
    else if (([ktotalBookingAmount integerValue] > 0))
    {
        // code to round off values
        float amountToRoundoff = [ktotalBookingAmount floatValue];
        int rounded = roundf(amountToRoundoff);
        ktotalBookingAmount = [NSString stringWithFormat:@"%d",rounded];
        //NSLog(@"Value in string after rounding off =%@",ktotalBookingAmount);
        status = true;
        
    }
    return status;
}

#pragma mark - moveToConfirmController
- (void)moveToConfirmController : (NSDictionary *)jsonDict withInsuranceTaken:(BOOL)insuranceTaken
{
    if (jsonDict)
    {
        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:jsonDict];
        if ([dic objectForKey:@"PreAuthStatus"] == nil || [dic objectForKey:@"PreAuthStatus"] == (id)[NSNull null]) {
            [dic setObject:@"0" forKey:@"PreAuthStatus"];
            [dic setObject:kDepositeAmt forKey:@"PreAuthAmount"];
        }
        
        if ([dic objectForKey:@"PreAuthAmount"] == nil || [dic objectForKey:@"PreAuthAmount"] == (id)[NSNull null]) {
            [dic setObject:kDepositeAmt forKey:@"PreAuthAmount"];
        }
        
        NSMutableDictionary *newDict = nil;
        newDict = dic.mutableCopy;
        if (insuranceTaken) {
            [newDict removeObjectForKey:@"PaymentAmount"];
            float totalValue = [ktotalBookingAmount floatValue] + [carinfo.TotalInsuranceAmt floatValue];
            [newDict setObject:[NSString stringWithFormat:@"%.f",totalValue] forKey:@"PaymentAmount"];
        } else {
            [newDict removeObjectForKey:@"PaymentAmount"];
            [newDict setObject:ktotalBookingAmount forKey:@"PaymentAmount"];
        }
        
        if (maExtraAmenities.count > 0) {
            //            NSDictionary *temp = @{@"AmenitiesDetails": maExtraAmenities};
            //            [newDict setObject:temp forKey:@"details"];
            
            //            NSDictionary *temp = @{@"AmenitiesDetails": maExtraAmenities};
            [newDict setObject:maExtraAmenities forKey:@"AmenitiesDetails"];
             
        }
        //NSLog(@"newDict = %@",newDict);
        NSLog(@"self.navigationController.viewControllers before -> %@",self.navigationController.viewControllers
              );
        //SWRevealViewController *revealController = self.revealViewController;
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_MyRideStoryBoard bundle:nil];
        //UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_RidesNavigation];
        BookingConfirmationVC *bookingConfirm = [storyBoard instantiateViewControllerWithIdentifier:@"bookingConfirm"];
        bookingConfirm.comingFromController = AfterBookingConfirmation;
        //NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:jsonDict];
        //[bookingConfirm setRevealController:revealController];
        [bookingConfirm setDisplayAlert:NO];
        bookingConfirm.showPaymentAlert = YES;
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isDocumentUpload"];
        bookingConfirm.ridePaymentData = newDict;
        [bookingConfirm setRideInfomation:newDict];
    
        maExtraAmenities = nil;
        
        [self.navigationController pushViewController:bookingConfirm animated:TRUE];

    }
}
#pragma mark - ACTIONSHEET DELEGATES -

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag == 1000)
    {
        if (buttonIndex == 0)
        {
//            PickupAddressVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
//            NSMutableArray *addresaarray = [NSMutableArray arrayWithArray:[DEFAULTS valueForKey:k_PickupAddress]];
//            vc.addressDataMA = ([DEFAULTS valueForKey:k_PickupAddress]) ? addresaarray : nil;
//            [[self navigationController] pushViewController:vc animated:YES]
//            ;
            
            if ([CommonFunctions reachabiltyCheck]) {
                [self showLoader];
                [CommonFunctions SavePickUpAddress:@{@"PhoneNo":[DEFAULTS objectForKey:KUSER_PHONENUMBER],@"userId":[DEFAULTS objectForKey:KUSER_ID]} PassUrl:@"RestM1FetchCustomerAddress" :^(NSDictionary *DictSaveAddress) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                    });
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSArray *arrData = DictSaveAddress[@"response"];
                        
                        if (DictSaveAddress.count>0 && DictSaveAddress[@"response"] != nil && [arrData count] > 0) {
                            PickUpAddressList *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PickUpAddressList"];
                            // vc.maPickAddress = DictSaveAddress[@"response"] ;
                            [[self navigationController] pushViewController:vc animated:YES];
                            
                            
                        }
                        else
                        {
                            AddHomePickUpAddress *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
                            [[self navigationController] pushViewController:vc animated:YES];
                        }

                    });
                    
                }];
            }
            else
            {
                [CommonFunctions alertTitle:nil withMessage:KAInternet];
                
            }
            
        }
        else  if (buttonIndex == 1)
        {
            [DEFAULTS removeObjectForKey:k_PickupAddress];
            [addressLabel setText:locationAddress];
            
            for (int i = 0; i < amenitiesMA.count; i++)
            {
                
                NSMutableDictionary *dict = amenitiesMA[i];
                NSString *serviceid = [[dict valueForKey:@"data"]valueForKey:@"ServiceID"];
                if ([serviceid isEqualToString:@"3"])
                {
                    NSIndexPath *tableindex = [NSIndexPath indexPathForRow:i inSection:1];
                    DetailCustomCell *cell = (DetailCustomCell *)[tableView cellForRowAtIndexPath:tableindex];
                    AmenitiesInfo *items = dict[@"data"];
                    userLocationLabel.hidden = true;
                    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
                    [self removeAdditionalCharges:[items.Amount integerValue]TypeOfservice:items.ServiceID];
                    [DEFAULTS setBool:NO forKey:k_PickupEnabled];
                    [cell.discloseBtn setSelected:false];
                    [tableView reloadRowsAtIndexPaths:@[tableindex] withRowAnimation:UITableViewRowAnimationFade];
                }
            }
        }
        
    }
}

#pragma mark - SorryALertProtocol

/*
 Rental payment net banking
 */

//- (void)netBanking{
//
//    if (kHoursBetweenDates <= 2 && (kBookingid.length >0))
//    {
//        [self addAlertForPreAuthPayment:kHoursBetweenDates];
//
//        return;
//    }
//    else {
//        //NSString * trackId = [@"CORIC" stringByAppendingString:TimeStamp];
//        //kTrackId = trackId;
//
//        paymentMethodTrack = @"Payment_Rental_Nb";
//        [self payNimoController:KNetbanking TranactionAmount:ktotalBookingAmount andTrackId:kTrackId];
//    }
//}
//
//- (void)cardBanking{
//
//    //NSString * trackId = [@"CORIC" stringByAppendingString:TimeStamp];
//    //kTrackId = trackId;
//    [self payNimoController:KCards TranactionAmount:ktotalBookingAmount andTrackId:kTrackId];
//}

//- (void)preAuthCardPayment{
//
//    if (kHoursBetweenDates <= 2 && (kBookingid.length >0))
//    {
//        [self addAlertForPreAuthPayment:kHoursBetweenDates];
//        return;
//    }
//    else if (preAuth)
//    {
//        [self payNimoController:KCards TranactionAmount:kDepositeAmt andTrackId:kTrackId];
//    }
//
//    else {
//
//        [self moveToConfirmController:detailResponse];
//    }
//}
//- (void)preAuthNetBanking{
//
//    if (preAuth)
//    {
//        paymentMethodTrack = @"Payment_Security_Nb";
//        [self payNimoController:KNetbanking TranactionAmount:kDepositeAmt andTrackId:kTrackId];
//        [self ecommerceTracking:paymentMethodTrack TranactionAmount:kDepositeAmt TrackId:kTrackId BookingId:kBookingid];
//
//    }
//    else {
//
//        //NSLog(@"check logic payment type id preauth");
//    }
//}


/**/
//-(void) preauthPaymentNetBanking : (NSNotification *) notification {
//
//    paymentMethodTrack = @"Payment_Security_Nb";
//
//    if (preAuth)
//    {
//        [self payNimoController:KNetbanking TranactionAmount:kDepositeAmt andTrackId:kTrackId];
//        [self ecommerceTracking:paymentMethodTrack TranactionAmount:kDepositeAmt TrackId:kTrackId BookingId:kBookingid];
//
//    }
//    else {
//
//        //NSLog(@"check logic payment type id preauth");
//    }
//}

//-(void) preauthPaymentCard : (NSNotification *) notification {
//
//    paymentMethodTrack = @"Payment_Security_Card";
//
//    if (kHoursBetweenDates <= 2 && (kBookingid.length >0))
//    {
//        [self addAlertForPreAuthPayment:kHoursBetweenDates];
//        return;
//    }
//    if (preAuth)
//    {
//        [self payNimoController:KCards TranactionAmount:kDepositeAmt andTrackId:kTrackId];
//        [self ecommerceTracking:paymentMethodTrack TranactionAmount:kDepositeAmt TrackId:kTrackId BookingId:kBookingid];
//
//    }
//    else {
//
//        [self moveToConfirmController:detailResponse];
//    }
//}


-(void) netBankingPayment : (NSNotification *) notification {
    
    //if minimum booking hours is less then 2 hours we cannot create booking
    
    /*
     Logic to get date and time from authenicated server and compare with booking time.If its less then 2 hours user should not be able to create booking
     
     */
    
    
    if (kHoursBetweenDates <= 2 && (kBookingid.length >0))
    {
        [self addAlertForPreAuthPayment:kHoursBetweenDates];
        
        return;
    }
    else {
        
        //NSString * trackId = [@"CORIC" stringByAppendingString:TimeStamp];
        //kTrackId = trackId;
        
//        [self payNimoController:KNetbanking TranactionAmount:ktotalBookingAmount andTrackId:kTrackId];
        
    }
    
    
    
    /*
     
     if (kHoursBetweenDates <= 2 && (kBookingid.length >0))
     {
     [self addAlertForPreAuthPayment:kHoursBetweenDates];
     
     return;
     }
     else if (preAuth)
     {
     [self moveToConfirmController:detailResponse];
     }
     
     if (preAuth)
     {
     [self payNimoController:KNetbanking TranactionAmount:kDepositeAmt andTrackId:kTrackId];
     }
     else
     {
     NSString * trackId = [@"CORIC" stringByAppendingString:TimeStamp];
     kTrackId = trackId;
     [self payNimoController:KNetbanking TranactionAmount:ktotalBookingAmount andTrackId:trackId];
     }
     */
    
}

-(void) cardPayment : (NSNotification *) notification {
    
    paymentMethodTrack = @"Payment_Rental_Card";
    /*
     NSString * trackId = [@"CORIC" stringByAppendingString:TimeStamp];
     kTrackId = trackId;
     */
//    [self payNimoController:KCards TranactionAmount:ktotalBookingAmount andTrackId:kTrackId];
}



//- (void)payNimoController : (NSString *)title TranactionAmount : (NSString *)totalamount andTrackId:(NSString *)trackId
//{
//    CommonApiClass *commonApi = [[CommonApiClass alloc]init];
//    commonApi.delegate = self;
//    [commonApi payNimoController:title TranactionAmount:totalamount Date:[self formatDate:[NSDate date]] withType:preAuth andEndDate:[self formatDate:[[NSDate date] dateByAddingTimeInterval:kHoursBetweenDates*3600]] withGap:kHoursBetweenDates/24 andTrackId:trackId];
//}

#pragma mark- CommonApiClassDelegate
- (void)PresentPaymentScreen:(id )paymentVC
{
    //    [self presentViewController:(PMPaymentOptionView *)paymentVC animated:YES completion:nil];
}
//- (void)PaymentSuccessDelagateWithData:(NSMutableDictionary *)dict
//{
//    kPaymentDict = dict.mutableCopy;
//    //NSLog(@"DICT  :%@",dict);
//    if (preAuth)
//    {
//
//        [self PreAuthApi:kBookingid Track:[dict objectForKey:@"trackid"] Transaction:[dict objectForKey:@"transactionid"] Amount:kDepositeAmt];
//    }
//    else
//    {
//        //[self BookingTAndC:[dict objectForKey:@"transactionid"]];
//        [self BookingTAndC:[dict objectForKey:@"trackid"]];
//    }
//
//
//}
//- (void)PaymentFailureDelagate
//{
//    if (preAuth)
//    {
//        [CommonFunctions alertTitle:kfailed withMessage:kpreauthFail];
//    }
//    else
//    {
//        [CommonFunctions alertTitle:kfailed withMessage:kpaymentFailuremsg];
//    }
//
//    if (kHoursBetweenDates <= 2 && (kBookingid.length >0))
//    {
//        [self bookCarBtnTouched:nil];
//    }
//    else if (kBookingid.length > 0)
//    {
//        [self moveToConfirmController:detailResponse];
//    }
//}
//
//- (void)PaymentCancelDelagte
//{
//    if (kHoursBetweenDates <= 2 && (kBookingid.length >0))
//    {
//        [self bookCarBtnTouched:nil];
//    }
//    else if (kBookingid.length > 0)
//    {
//        [self moveToConfirmController:detailResponse];
//    }
//    else
//    {
//        [CommonFunctions alertTitle:kCancelled withMessage:kpaymentcanceledmsg];
//    }
//}

- (void)postActiveNotifications:(NSString *)type
{
    [[NSNotificationCenter defaultCenter] postNotificationName:KACTIVE_BOOKING_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObject:type forKey:@"type"]];
}

-(NSString *)getDateInDesiredFormat:(NSString *)entryDate {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString:entryDate];
    
    // Convert date object into desired format
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];


}

#pragma mark- Get APi Body

- (NSMutableDictionary *)getPostBody
{
    NSInteger days;
    //carinfo.TotalDuration
    if ([carinfo.PkgType isEqualToString:@"Hourly"]){
        days = 1;
    }
    else{
      days =ceil([carinfo.WeekDayDuration floatValue] + [carinfo.WeekEndDuration floatValue] + [carinfo.FreeDuration floatValue]);
   // NSInteger days = (hourlyType == 1) ? 0 : ([carinfo.TotalDuration integerValue]) ;
    }
    NSMutableDictionary *body = [NSMutableDictionary new];
    
    NSString *amenitiesStr = @"";
    NSString *amenitiesAmountStr = @"";
    for (int i = 0; i < amenitiesMA.count; i++)
    {
        NSMutableDictionary *dict = amenitiesMA[i];
        
        AmenitiesInfo *amenities = [dict objectForKey:@"data"];
        
        if([[dict objectForKey:@"selected"] boolValue])
        {
            if (amenitiesStr.length)
            {
                amenitiesStr = [amenitiesStr stringByAppendingFormat:@",%@",amenities.ServiceID];
                
                if(days > 0){
                  NSInteger AmRate =   ([amenities.Amount integerValue] * days);
                    NSString *total =  [ NSString stringWithFormat:@"%ld", (long)AmRate];
                    amenitiesAmountStr = [amenitiesAmountStr stringByAppendingFormat:@",%@",total];

                }
                else{
                    amenitiesAmountStr = [amenitiesAmountStr stringByAppendingFormat:@",%@",amenities.Amount];

                }
                
            }
            else
            {
                if(days > 0){
                    NSInteger AmRate =   ([amenities.Amount integerValue] * days);
                    NSString *total =  [ NSString stringWithFormat:@"%ld", (long)AmRate];
                    amenitiesStr = amenities.ServiceID;
                    amenitiesAmountStr = total;
                }
                else{
                    amenitiesStr = amenities.ServiceID;
                    amenitiesAmountStr = amenities.Amount;
                }

               
                
            }
        }
    }
    long long milliseconds = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
    NSString *randomNumber = [NSString stringWithFormat:@"%lld",milliseconds];
    
    float totalFare4Paynimo = [self.totalFare floatValue];
    int roundedTotalForPaynimo = roundf(totalFare4Paynimo);
    NSString *totalFareString4Paynimo = [NSNumber numberWithInt:roundedTotalForPaynimo].stringValue;
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    [body setObject:[NSString stringWithFormat:@"%@",carinfo.PkgId] forKey:@"pkgId"];
    NSString *pickD = [self getDateInDesiredFormat:[self.kBookingDurationMD objectForKey:@"pDate"]];
    NSString *dropD = [self getDateInDesiredFormat:[self.kBookingDurationMD objectForKey:@"dDate"]];
    [body setObject:pickD forKey:@"PickupDate"];
    [body setObject:dropD forKey:@"DropoffDate"];
    [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"pTime"]] forKey:@"PickupTime"];
    [body setObject:[NSString stringWithFormat:@"%@",[self.kBookingDurationMD objectForKey:@"dTime"]] forKey:@"DropoffTime"];
    [body setObject:[NSString stringWithFormat:@"%@",[DEFAULTS objectForKey:KUSER_PHONENUMBER]] forKey:@"phone"];
    [body setObject:[NSString stringWithFormat:@"%@",[DEFAULTS objectForKey:KUSER_ID]] forKey:@"userId"];
    [body setObject:[NSString stringWithFormat:@"%@",totalFareString4Paynimo] forKey:@"paymentAmount"];
    [body setObject:@"1" forKey:@"paymentStatus"];
    [body setObject:[NSString stringWithFormat:@"%@",[kPaymentDict objectForKey:@"trackid"]] forKey:@"trackId"];
    [body setObject:[NSString stringWithFormat:@"%@",[kPaymentDict objectForKey:@"transactionid"]] forKey:@"transactionId"];
    [body setObject:amenitiesStr forKey:@"aminities"];
    [body setObject:amenitiesAmountStr forKey:@"aminitiesAmount"];
    [body setObject:carinfo.Model forKey:@"CarModelName"];
    [body setObject:@"New" forKey:@"Tripstatus"];
    
    
    
    [body setObject:[NSString stringWithFormat:@"%0.f",[self.amenitiesTotalAmount floatValue]] forKey:@"amitiesAmount"];
    [body setObject:[NSString stringWithFormat:@"%@",self.subLocationID] forKey:@"SubLocationID"];
    [body setObject:[NSString stringWithFormat:@"%@",carinfo.TotalDuration] forKey:@"pkgDuration"];
    [body setObject:[NSString stringWithFormat:@"%0.f",carinfo.AirportCharges.floatValue] forKey:@"SubAirportCost"];
    [body setObject:[NSString stringWithFormat:@"%.3f",carinfo.WeekDayDuration.floatValue] forKey:@"WeekDayDuration"];
    [body setObject:[NSString stringWithFormat:@"%.3f",carinfo.WeekEndDuration.floatValue] forKey:@"WeekEndDuration"];
    [body setObject:[NSString stringWithFormat:@"%.2f",carinfo.FreeDuration.floatValue] forKey:@"FreeDuration"];
    //[body setObject:[NSString stringWithFormat:@"%0.f"] forKey:[carinfo.IndicatedPrice floatValue]]
    if ([couponCode isEqualToString:@""]) {
        //NSLog(@"Invalid coupon code");
    }
    else {
        [body setObject:[NSString stringWithFormat:@"%@",couponCode] forKey:@"DiscountCode"];
        NSString *myString = [NSString stringWithFormat: @"%ld", (long)couponAmount];
        myString = [NSString stringWithFormat: @"%.f", [myString floatValue]];
        [body setObject:myString forKey:@"DiscountAmount"];
        
         [FIRAnalytics logEventWithName:@"booknow_withOffer_ride_details" parameters:nil];
    }
    [body setObject:addressLabel.text forKey:@"PickUpDropOffAddress"];
    [body setObject:[NSString stringWithFormat:@"%@",latitude] forKey:@"Latitude"];
    [body setObject:[NSString stringWithFormat:@"%@",longitude] forKey:@"Longitude"];
    
    //Latitude, string Longitude
    //[body setObject:[NSString stringWithFormat:@"%ld",(long)couponAmount] forKey:@"DiscountAmount"];
    //[body setObject:[NSString stringWithFormat:@"%.2ld",(long)couponAmount] forKey:@"DiscountAmount"];
    //[body setObject:[NSString stringWithFormat:@"%.0f",carinfo.FreeDuration.floatValue] forKey:@"FreeDuration"];
    
    //NSLog(@"body =%@",body);
    return body;
}


-(NSString *)srtdate :(NSString *)strDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:strDate];
    
    // Convert date object into desired format
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *newDateString = [dateFormatter stringFromDate:date];
    return newDateString;
}

//#pragma mark- Booking
//- (void)bookingConfirmationApi
//{
//    if (couponStatus == 1) {
//        if ([CommonFunctions reachabiltyCheck])
//        {
//            NSMutableDictionary *body = [self getPostBody];
//            //NSLog(@"body =%@",body);
//            //NSLog(@"Create booking api post data = %@",body);
//            NSURLSession *session = [CommonFunctions defaultSession];
//            [GMDCircleLoader setOnView:self.view withTitle:@"Please wait..." animated:YES];
//            NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
//            APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:@"createBookingfromAppV2" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                                                   {
//                                                       // //NSLog(@"response =%@",response);
//                                                       if (!error)
//                                                       {
//                                                           dispatch_async(dispatch_get_main_queue(), ^{
//                                                               NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//                                                               if (jsonDict) {
//                                                                   //NSLog(@"JSON : %@",jsonDict);
//
//                                                                   if (([jsonDict valueForKey:@"details"] != [NSNull null]) && ([jsonDict valueForKey:@"bookingid"] != [NSNull null]))
//                                                                   {
//
//                                                                       [self ecommerceTracking:paymentMethodTrack TranactionAmount:ktotalBookingAmount TrackId:[[jsonDict objectForKey:@"details"] objectForKey:@"trackID"] BookingId:[jsonDict valueForKey:@"bookingid"]];
//
//                                                                       kBookingid = [jsonDict valueForKey:@"bookingid"];
//                                                                       kTrackId = [[jsonDict objectForKey:@"details"] objectForKey:@"trackID"];
//                                                                       if (kBookingid.length > 0)
//                                                                       {
//                                                                           detailResponse = [NSMutableDictionary dictionaryWithDictionary:[jsonDict valueForKey:@"details"]];
//                                                                           if ([[detailResponse valueForKey:@"Tripstatus"] isEqualToString:k_tripstatusNew])
//                                                                           {
//                                                                               [DEFAULTS setValue:@"0" forKey:kBookingid];
//                                                                           }
//
//                                                                           NSInteger hoursBetweenDates =   [CommonFunctions getPreAuthStatusAsPerStartTime:[self.kBookingDurationMD objectForKey:@"pTime"] Date:[self.kBookingDurationMD objectForKey:@"pDate"]];
//                                                                           if ([kDepositeAmt integerValue] > 0)
//                                                                           {
//
//
//                                                                               if (hoursBetweenDates <= 14*24)
//                                                                               {
//                                                                                   [self addAlertForPreAuthPayment:hoursBetweenDates];
//                                                                               }
//                                                                               else
//                                                                               {
//                                                                                   //                                                                                    [self addAlertForPreAuthPayment:hoursBetweenDates];
//                                                                                   [self postActiveNotifications:@"0"];
//                                                                                   [self moveToConfirmController:detailResponse];
//                                                                               }
//                                                                           }
//                                                                           else
//                                                                           {
//                                                                               [self postActiveNotifications:@"1"];
//                                                                               [self bookCarBtnTouched:nil];
//                                                                           }
//                                                                           [CommonFunctions alertTitle:KAlert withMessage:[jsonDict valueForKey:k_ApiMessageKey]];
//                                                                       }
//                                                                   }
//                                                                   else
//                                                                   {
//                                                                       [CommonFunctions alertTitle:KMessage withMessage:[jsonDict valueForKey:k_ApiMessageKey]];
//                                                                   }
//                                                               }
//                                                           });
//                                                       }
//                                                       else
//                                                       {
//                                                           dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                               [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
//                                                           });
//
//                                                       }
//                                                       dispatch_async(dispatch_get_main_queue(), ^{
//                                                           [GMDCircleLoader hideFromView:self.view animated:YES];
//                                                       });
//                                                   }];
//            [globalOperation addOperation:registrationOperation];
//        }
//        else
//        {
//            [CommonFunctions alertTitle:nil withMessage:KAInternet];
//        }
//    }
//
//}


-(void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    if (preAuth)
    {
        [self postActiveNotifications:@"1"];
    }
}


- (void) addAlertForPreAuthPayment:(NSInteger)hoursBetweenDates
{
    kHoursBetweenDates = hoursBetweenDates;
    
    NSString *message = [NSString stringWithFormat:@"%ld", (long)[kDepositeAmt integerValue]];
    NSString *messageStr = (hoursBetweenDates > 2) ? kpreauthreminder(message) : [NSString stringWithFormat:@"%@ Rs:%ld",kSecurityAmount,(long)[kDepositeAmt integerValue]];
    
    if ([CommonFunctions getMajorSystemVersion] > 7)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:KMessage message:[NSString stringWithFormat:@"%@", messageStr] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *Continueaction = [UIAlertAction actionWithTitle:kContinue style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                         {
                                             
                                             [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
                                             id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                             
                                             [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"PreAuth Payment"
                                                                                                   action:@"Click"
                                                                                                    label:@"PreAuthPayment Alert"
                                                                                                    value:@1] build]];
                                             
                                             preAuth = true;
                                             [self bookCarBtnTouched:nil];
                                             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
                                             SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
                                             controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                                             controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                             controller.alertType    = 6;
                                             controller.delegate     = self;
                                             controller.navController = self.navigationController;
                                             [self presentViewController:controller animated:YES completion:nil];
                                         }];
        [alert addAction:Continueaction];
        // If hours are more than 2 then
        
        if (hoursBetweenDates > 2)
        {
            UIAlertAction *Cancelaction = [UIAlertAction actionWithTitle:kCancel style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                           {
                                               if (!preAuth)
                                               {
                                                   [self postActiveNotifications:@"0"];
                                               }
                                               [self moveToConfirmController:detailResponse withInsuranceTaken:NO];
                                           }];
            [alert addAction:Cancelaction];
        }
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:KMessage message:messageStr delegate:self cancelButtonTitle:kContinue otherButtonTitles:(hoursBetweenDates > 2)?kCancel:nil, nil];
        alert.tag = 1;
        [alert show];
    }
}



#pragma mark- Alert


/*
 - (void) addAlertForPreAuthPayment:(NSInteger)hoursBetweenDates
 {
 kHoursBetweenDates = hoursBetweenDates;
 
 NSString *message = [NSString stringWithFormat:@"%ld", [kDepositeAmt integerValue]];
 NSString *messageStr = (hoursBetweenDates > 2) ? kpreauthreminder(message) : [NSString stringWithFormat:@"%@ Rs:%ld",kSecurityAmount,(long)[kDepositeAmt integerValue]];
 
 if ([CommonFunctions getMajorSystemVersion] > 7)
 {
 UIAlertController *alert = [UIAlertController alertControllerWithTitle:KMessage message:[NSString stringWithFormat:@"%@", messageStr] preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction *Continueaction = [UIAlertAction actionWithTitle:kContinue style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
 {
 
 [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
 id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
 
 [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"PreAuth Payment"
 action:@"Click"
 label:@"PreAuthPayment Alert"
 value:@1] build]];
 
 preAuth = true;
 [self bookCarBtnTouched:nil];
 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
 SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
 controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
 controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
 controller.alertType    = 6;
 controller.delegate     = self;
 controller.navController = self.navigationController;
 [self presentViewController:controller animated:YES completion:nil];
 }];
 [alert addAction:Continueaction];
 // If hours are more than 2 then
 
 if (hoursBetweenDates > 2)
 {
 UIAlertAction *Cancelaction = [UIAlertAction actionWithTitle:kCancel style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
 {
 if (!preAuth)
 {
 [self postActiveNotifications:@"0"];
 }
 //detailResponse
 //carinfo.TotalDuration
 NSInteger days = (hourlyType == 1) ? 0 : ([carinfo.WeekDayDuration integerValue] + [carinfo.WeekEndDuration integerValue]) ;
 NSInteger tmpamenitiesCharges ;
 
 NSMutableDictionary *AminitiesResponseJson = [(NSMutableDictionary *)detailResponse objectForKey:@"AmenitiesDetails"];
 
 NSMutableArray *aminitiesDataArray;
 aminitiesDataArray = [[NSMutableArray alloc]init];
 aminitiesDataArray = [AminitiesResponseJson mutableCopy];
 //[[dataDictionary objectForKey:@"ds_history"] mutableCopy];
 
 for (int i = 0 ; i<aminitiesDataArray.count; i++) {
 NSMutableDictionary *Aminitiestmp= aminitiesDataArray[i];
 
 NSInteger aminitiesID = [[Aminitiestmp valueForKey:@"aminiti_id"] integerValue];
 
 
 if (days > 0 && aminitiesID == 1)
 {
 
 NSInteger tmpDays =ceil([carinfo.WeekDayDuration floatValue] + [carinfo.WeekEndDuration floatValue]);
 
 //amenitiesCharges = (int)ceil(prize * tmpDays);
 tmpamenitiesCharges = [[Aminitiestmp valueForKey:@"amount"] integerValue] * tmpDays;
 //NSLog(@"Update cost of baby seat");
 //conver integer to string tmpamenitiesCharges
 
 NSString *myNumberString =  [[NSNumber numberWithInt:tmpamenitiesCharges] stringValue];
 
 
 NSString *tmpChargeValue = [Aminitiestmp objectForKey:@"amount"];
 //NSLog(@"tmpChargeValue = %@",tmpChargeValue);
 
 NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
 NSDictionary *oldDict = (NSDictionary *)aminitiesDataArray[i];
 [newDict addEntriesFromDictionary:oldDict];
 [newDict setObject:myNumberString forKey:@"amount"];
 [aminitiesDataArray replaceObjectAtIndex:i withObject:newDict];
 //NSLog(@"detailResponse= %@",detailResponse);
 
 //[aminitiesDataArray removeObjectAtIndex:i];
 //[aminitiesDataArray insertObject:newDict atIndex:i];
 //Add velue to nsmutable array in iOS
 [detailResponse removeObjectForKey:@"AmenitiesDetails"];
 //NSLog(@"detailResponse =%@",detailResponse);
 [detailResponse setObject:aminitiesDataArray forKey:@"AmenitiesDetails"];
 //NSLog(@"updated detailResponse =%@",detailResponse);
 break;
 }
 //NSLog(@"aminitiesDataArray =%@",aminitiesDataArray);
 }
 
 [self moveToConfirmController:detailResponse];
 }];
 [alert addAction:Cancelaction];
 }
 [self presentViewController:alert animated:true completion:nil];
 }
 else
 {
 UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:KMessage message:messageStr delegate:self cancelButtonTitle:kContinue otherButtonTitles:(hoursBetweenDates > 2)?kCancel:nil, nil];
 alert.tag = 1;
 [alert show];
 }
 }
 */


#pragma mark- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 3)
    {
        if (buttonIndex == 1)
        {
            
            SWRevealViewController *revealController = self.revealViewController;
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_profileStoryBoard bundle:nil];
            UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_profileNavigation];
            
            [(MenuViewController *)revealController.rearViewController setSelectedIndex:2];
            
            ProfileViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_profileController];
            navController.viewControllers = @[homeController];
            
            ////NSLog(@"CONRTOL : %@",navController.viewControllers);
            
            
            [revealController pushFrontViewController:navController animated:YES];
        }
        else
        {
            
            //            NSDictionary *jsonDict = (NSDictionary *)objc_getAssociatedObject(alertView, (__bridge const void *)(kAssociationKeyForConfirmation));
            
            NSDictionary *jsonDict = kJsonDict;
            
            if (jsonDict)
            {
                
                //BookingConfirmationVC
                
                SWRevealViewController *revealController = self.revealViewController;
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_MyRideStoryBoard bundle:nil];
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_RidesNavigation];
                BookingConfirmationVC *bookingConfirm = [storyBoard instantiateViewControllerWithIdentifier:@"bookingConfirm"];
                NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:jsonDict];
                [bookingConfirm setDisplayAlert:YES];
                
                [bookingConfirm setRideInfomation:dic];
                navController.viewControllers = @[bookingConfirm];
                [revealController pushFrontViewController:navController animated:YES];
            }
            
            
        }
        
    }
    else if (alertView.tag == 6)
    {
        [self moveToConfirmController : detailResponse withInsuranceTaken:NO];
    }
    else if (alertView.tag == 101)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tabBarController.tabBar setHidden:FALSE];
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    }
    
    else
    {
        if (buttonIndex != 0)
        {
            if (!preAuth)
            {
                [self postActiveNotifications:@"0"];
            }
            
            [self moveToConfirmController:detailResponse withInsuranceTaken:NO];
        }
        else
        {
            preAuth = true;
          //  [self bookCarBtnTouched:nil];
            
        }
    }
}

#pragma mark- PreAuth
- (void)PreAuthApi : (NSString *)bookingid Track : (NSString *)trackid  Transaction : (NSString *)transactionid Amount : (NSString *)preauthamount
{
    
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:bookingid forKey:k_bookingid];
        [body setObject:trackid forKey:k_trackid];
        [body setObject:transactionid forKey:k_transactionid];
        [body setObject:preauthamount forKey:k_preauthamount];
        
        ////NSLog(@"body =%@",body);
        [self showLoader];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KpreAuthCreation completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       ////NSLog(@"response =%@",response);
                                                       NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                       
                                                       ////NSLog(@"Respose Json : %@",jsonreponse);
                                                       if (!error)
                                                       {
                                                           [detailResponse setValue:[jsonreponse objectForKey:@"status"] forKey:@"PreAuthStatus"];
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               
                                                               [self postActiveNotifications:@"0"];
                                                               
                                                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KMessage  message:[jsonreponse valueForKey:k_ApiMessageKey] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                               alert.tag = 6;
                                                               
                                                               [alert show];
                                                               
                                                               
                                                           });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:KMessage withMessage:[jsonreponse valueForKey:k_ApiMessageKey]];                                                      });
                                                           
                                                       }
                                                      
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               [self.navigationController popToRootViewControllerAnimated:TRUE];
                                                               //[self.revealViewController revealToggleAnimated:true];

                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });
}

                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}


#pragma mark- Coupon Code
- (void)applyCouponCode  : (NSString *)CarModelId CITYID : (NSString *)cityid  DISCOUNT : (NSString *)discountCode PICKUPDATE : (NSString *)pickupdate TOTALFAIR : (NSString *)totalFair PACKAGEID : (NSString *)pkgid PACKAGEDURATION : (NSString *)pkgDuration USERID : (NSString *)userid DROPOFFDATE:(NSString *)DropOffDate SUBLOCATIONID:(NSString *)SubLocationId WEEKDAY:(NSString *)WeekDay WEEKEND:(NSString *)weekEnd
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSInteger roundedpkgDuration =(int)ceil([pkgDuration floatValue]);
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setValue:CarModelId forKey:k_CarModelId];
        [body setValue:cityid forKey:k_cityIDCap];
        [body setValue:discountCode forKey:k_DiscountCode];
        [body setValue:pickupdate forKey:k_pickUpdate];
        [body setValue:totalFair forKey:k_totalFare];
        [body setValue:pkgid forKey:k_pkgId];
        //[NSString stringWithFormat:@"%0.2f",carinfo.DepositeAmt.floatValue] forKey:@"DepositeAmt"]
        //[body setValue:roundedpkgDuration forKey:k_pkgDuration];
        [body setValue:[NSString stringWithFormat:@"%0ld",(long)roundedpkgDuration] forKey:k_pkgDuration];
        [body setValue:userid forKey:@"userId"];
        [body setValue:[self.kBookingDurationMD objectForKey:@"dDate"] forKey:@"DropOffDate"];
        if (_doorStepDelieverySelected) {
            [body setValue:[[SubLocationId componentsSeparatedByString:@":"] count] > 0 ?  [SubLocationId componentsSeparatedByString:@":"][0] : SubLocationId  forKey:@"SubLocationId"];
        } else {
            [body setValue:self.subLocationID forKey:@"SubLocationId"];
        }
        [body setValue:WeekDay forKey:@"WeekDay"];
        [body setValue:weekEnd forKey:@"WeekEnd"];
        NSLog(@"body =%@",body);
        //WeekDay -Key , Value - Regular days  applyDiscountNewV1
        
        [self showLoader];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kapplyDiscount completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               NSError* error;
                                                               NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:kNilOptions
                                                                                                                      error:&error];
                                                               CouponResponse *response = [[CouponResponse alloc]initWithData:data error:nil];
                                                               couponStatus = 1;
                                                               if ([response.ValidYN isEqual:k_ValidYes])
                                                               {
                                                                   [self textfieldRightView:k_RightCheck];
                                                                   [self.applyCouponBtn setTitle:@"Reset" forState:UIControlStateNormal];
                                                                   [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
                                                                   id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                                                   [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Reset Coupon"
                                                                                                                         action:@"Click"
                                                                                                                          label:@"Reset"
                                                                                                                          value:@1] build]];
                                                                   
                                                                   [coupontxtFd setUserInteractionEnabled:NO];
                                                                   
                                                                   if (couponAmount > 0)
                                                                   {
                                                                       float amount = [ktotalBookingAmount floatValue] + couponAmount - roundf([response.PerDiscount floatValue]);
                                                                       if (amount <= 0)
                                                                       {
                                                                           ktotalBookingAmount = @"1";
                                                                       }
                                                                       else
                                                                       {
                                                                           ktotalBookingAmount =  [NSString stringWithFormat:@"%.2f", amount];
                                                                       }
                                                                       couponAmount = roundf([response.PerDiscount floatValue]);
                                                                   }
                                                                   else
                                                                   {
                                                                       float amount = [ktotalBookingAmount floatValue]  - roundf([response.PerDiscount floatValue]) ;
                                                                       if (amount <= 0)
                                                                       {
                                                                           ktotalBookingAmount = @"1";
                                                                       }
                                                                       else
                                                                       {
                                                                           ktotalBookingAmount =  [NSString stringWithFormat:@"%.2f",amount];
                                                                       }
                                                                       couponAmount =roundf([response.PerDiscount floatValue]);
                                                                   }
                                                                   [self setFairAndDetailsSectionContent:0.0];
                                                                   [self UpdateButtonPrizeLabel:ktotalBookingAmount];
                                                                   if(fairDetailsOpened){
                                                                            tableViewHeight.constant = tableViewHeight.constant + 25.0;
                                                                       contentViewHieghtConstraint.constant += 25.0;

                                                                       [self.view layoutIfNeeded];

                                                                   }
                                                                   
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
//                                                                        [tableView reloadData];
                                                                       [self tableViewReloadData];

                                                                   });
                                                                  
                                                                   
                                                               }
                                                               else
                                                               {
                                                                   if (couponAmount > 0)
                                                                   {
                                                                       ktotalBookingAmount =  [NSString stringWithFormat:@"%.2f",[ktotalBookingAmount floatValue] + couponAmount ];
                                                                       couponAmount = roundf([response.PerDiscount floatValue]);
                                                                       [self setFairAndDetailsSectionContent:0.0];
                                                                       [self UpdateButtonPrizeLabel:ktotalBookingAmount];
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
//                                                                           [tableView reloadData];
                                                                           [self tableViewReloadData];

                                                                       });
                                                                   }
                                                                   [self textfieldRightView:nil];
                                                                   [coupontxtFd setText:@""];
                                                                   //couponCode = @"";
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [self AlertView:kPleaseEnterValidCode];
                                                                   //[CommonFunctions alertTitle:KSorry withMessage:kPleaseEnterValidCode];
                                                                   });
                                                               }
                                                           });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [self AlertView:KATryLater];

//                                                               [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                           });
                                                       }
                                                       
                                                   } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired,  Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               [self.navigationController popToRootViewControllerAnimated:TRUE];
                                                               //[self.revealViewController revealToggleAnimated:true];

                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });

                                                   } else
                                                   {
                                                      
                                                   }
                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}




- (void)getCurrentLocation{
    CLLocationCoordinate2D coordinate = [self getLocation];
    latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    //NSLog(@"Latitude  = %@", latitude);
    //NSLog(@"Longitude = %@", longitude);
    [locationManager stopUpdatingLocation];
}

/*
 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
 SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
 controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
 
 addController.modalPresentationStyle = UIModalPresentationFullScreen;
 addController.transitionStyle = UIModalTransitionStyleCoverVertical;
 [self presentViewController:addController animated:YES completion: nil];
 }
 */


- (IBAction) offerButtonClick : (id)sender
{
    offerClicked = @"Yes";
    [FIRAnalytics logEventWithName:@"offer_clicked" parameters:nil];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    FlashOfferViewController *offerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"OfferViewController"];
    offerController.titleStr = @"OFFERS";
#if DEBUG
    offerController.urlToNavigate = [NSURL URLWithString:@"https://www.mylescars.com/marketings/myles_offers_app"];
#else
    offerController.urlToNavigate = [NSURL URLWithString:@"https://www.mylescars.com/marketings/myles_offers_app"];
#endif
    //offerController.urlToNavigate = [NSURL URLWithString:@"https://www.mylescars.com/marketings/myles_offers_app"];
    //offerController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:offerController animated:YES completion: nil];
}



-(CLLocationCoordinate2D) getLocation{
    //AppDelegate *delegate
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSString *latitudeValue = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    NSString *longtitudeValue = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    ////NSLog(@"latitudeValue =%@",latitudeValue);
    ////NSLog(@"longtitudeValue =%@",longtitudeValue);
    
}

-(void) ecommerceTracking:(NSString *)paymentTypeKeyword TranactionAmount : (NSString *)TrackingAmount TrackId:(NSString *)trackId BookingId:(NSString *)BookingId
{
    
    
        float amountValuefb = [TrackingAmount floatValue];
        
        [FBSDKAppEvents logPurchase:amountValuefb
                           currency:@"INR"
                         parameters:@{ @"trackId"   : trackId,
                                       @"BookingId" :  BookingId,
                                       @"EventName" :  paymentTypeKeyword
                                       }];
    
    NSString *CouponCode;
    
    
    NSDecimalNumber *myNumber = [NSDecimalNumber decimalNumberWithString:TrackingAmount];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    
    GAIEcommerceProduct *product = [[GAIEcommerceProduct alloc] init];
    [product setId:trackId];
    [product setName:carinfo.Model];
    [product setPrice:myNumber];
    
    GAIEcommerceProductAction *productAction = [[GAIEcommerceProductAction alloc] init];
    [productAction setAction:kGAIPAPurchase];
    [productAction setTransactionId:BookingId];
    [productAction setRevenue:myNumber];
    [productAction setCouponCode:CouponCode];
    
    //paymentTypeKeyword
    
    /*
     GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:@"MylesTransactionActivity"
     action:@"Purchase"
     label:nil
     value:nil];
     */
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:paymentTypeKeyword
                                                                           action:@"Purchase"
                                                                            label:nil
                                                                            value:nil];
    // Add the transaction data to the event.
    [builder setProductAction:productAction];
    [builder addProduct:product];
    
    [tracker set:kGAIScreenName value:@"Ride Detail Screen"];
    [tracker set:kGAICurrencyCode value:@"INR"];
    
    // Send the transaction data with the event.
    [tracker send:[builder build]];
    
}

//-(void) ecommerceTrackingFB:(NSString *)paymentTypeKeyword TranactionAmount : (NSString *)TrackingAmount TrackId:(NSString *)trackId BookingId:(NSString *)BookingId
//{
//   }

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


/**
     Goto payment page(juspay)
**/

-(NSString *)getOriginalAmount:(NSDictionary *)dataDict
{
    float originalAmount = 0.0;
    if (dataDict[@"SubAirportCost"] > 0) {
        originalAmount += [dataDict[@"SubAirportCost"] floatValue];
    }
    if (dataDict[@"amitiesAmount"] > 0) {
        originalAmount += [dataDict[@"amitiesAmount"] floatValue];
    }
    NSLog(@"%.f",originalAmount);
    originalAmount += [carinfo.BasicAmt floatValue];
    originalAmount = (originalAmount + ((originalAmount * [carinfo.VatRate floatValue])/100));
    float finalAmt = roundf(originalAmount);
    return [NSString stringWithFormat:@"%.f",finalAmt];
}

- (void)transferDataOnJuspay {

#ifdef DEBUG
    _previousDateTime = [CommonFunctions getCurrentDate];
    _previousMilliSec = [CommonFunctions getMilliSecond];
#endif
    NSLog(@"fareDetailsMA = %@",fareDetailsMA);
    //Get original amount
    NSDictionary *dict = [self getPostBody];
    NSString *originalAmount = [self getOriginalAmount:dict];
    NSLog(@"originalamount = %@",originalAmount);
    NSDictionary *dictAdd  = [self getUserDetailsBeforeBookingBody];
    NSArray *arr = [[DEFAULTS objectForKey:KUSER_NAME] componentsSeparatedByString:@" "];
    NSString *firstname;
    NSString *lastName;
   // b
    
    
    if (arr.count > 0)
    {
        firstname = arr[0];
    }
    if (arr.count > 0 && arr.count>1) {
        lastName = arr[1];
    }
    
    // Additinalamount
    NSString *additionalAmount = [dict objectForKey:@"aminitiesAmount"];
    //CHange new
    //[NSString stringWithFormat:@"%f",additionalCharges];
    
    NSString *homePickUpAmt;
   // NSDictionary *userDetail = [[NSUserDefaults standardUserDefaults]objectForKey:@"userDetail"];
    NSLog(@"User Name: %@",[DEFAULTS objectForKey:KUSER_NAME]);
    Juspay *juspay = [self.storyboard instantiateViewControllerWithIdentifier:@"Juspay"];
    juspay.delegate = self;
    if (_doorStepDelieverySelected && (([carinfo.AirportCharges floatValue] == 0.00))) {
        homePickUpAmt = [NSString stringWithFormat:@"%@",[DEFAULTS valueForKey:k_HomePickupCharge]];
    } else {
        homePickUpAmt = @"";
    }
    
    NSString *sublocationIDToSend = dict[@"SubLocationID"];
    if (sublocationIDToSend != nil) {
        NSArray *array = [sublocationIDToSend componentsSeparatedByString:@":"];
        sublocationIDToSend = array[0];
    }
    
    NSString *reqType;
    if(isSecurity){
        juspay.isldwBooking = false;
        reqType = @"S";
    }
    else{
        juspay.isldwBooking = true;
        reqType = @"I";

    }

    
    NSString * insurance_Amt = [NSString stringWithFormat:@"%d",insuranceSecurityAmt];

    
    // Post data on model class
    JusPayReuestModel * test = [[JusPayReuestModel alloc]initWithAddtionalService:[dict[@"aminities"] length] > 0? dict[@"aminities"]:@"" AirportCharges:[dict[@"SubAirportCost"] length] > 0? dict[@"SubAirportCost"] : @"0.00" BasicAmount:carinfo.BasicAmt CarCatName:carinfo.CarCatName CarCompName:carinfo.Model DiscountAmount:[dict[@"DiscountAmount"] length] > 0 ? dict[@"DiscountAmount"] : @"0.00"  DiscountCode:[dict[@"DiscountCode"] length] > 0 ? dict[@"DiscountCode"]: @"" DropOffDate:dict[@"DropoffDate"] DropOffTime:dict[@"DropoffTime"] EmailID:[DEFAULTS objectForKey:KUSER_EMAIL] Fname:[DEFAULTS objectForKey:KUSER_FNAME] Freeduration:[dict[@"FreeDuration"] length] > 0 ? dict[@"FreeDuration"]: @"0.00" IndicatedPkgID:dict[@"pkgId"] Lname:[DEFAULTS objectForKey:KUSER_LNAME] Mobile:dict[@"phone"] ModelName:carinfo.Model Modelid:carinfo.ModelID OriginId:dictAdd[@"CityId"] OriginName:carinfo.CityName OriginalAmount:dict[@"paymentAmount"] PackageType:carinfo.PkgType PayBackMember:@"1" PaymentAmount:dict[@"paymentAmount"] PickUpAdd: dict[@"PickUpDropOffAddress"] PickUpDate:dict[@"PickupDate"] PickUpTime:dict[@"PickupTime"] PickupdropoffAddress:  addressLabel.text duration:carinfo.TotalDuration SecurityAmt:dictAdd[@"DepositAmount"] ServiceAmount:additionalAmount Source:@"ios" SubLocationID:sublocationIDToSend SublocationAdd: _doorStepDelieverySelected ? _sublocationNameInDoorStep : locationAddress SublocationName:_doorStepDelieverySelected ? _sublocationNameInDoorStep : locationAddress TourType:@"Self Drive" TrackID:dictAdd[@"TrackId"] Userid:dict[@"userId"] VatRate:dictAdd[@"VatRate"] WeekEndduration:[dict[@"WeekEndDuration"] length] > 0 ? dict[@"WeekEndDuration"]:@"0.000" Weekdayduration:[dict[@"WeekDayDuration"] length] > 0 ? dict[@"WeekDayDuration"]:@"0.000" City:[DEFAULTS stringForKey:K_LastLocation] App_Version:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] insuranceAmount:carinfo.InsuranceAmt totalInsuranceAmount:carinfo.TotalInsuranceAmt homePickDropAmount:homePickUpAmt cgstRate:carinfo.CGSTRate sgstRate:carinfo.SGSTRate igstRate:carinfo.IGSTRate utgstRate:carinfo.UTGSTRate HomeLat:dictAdd[@"Latitude"] HomeLong:dictAdd[@"Longitude"] SpeedGovernor: _speedGovernerIsActive ? 1:0 TotalDurationForDisplay: _TotalDurationForDisplay KMIncluded:_selectedPackage.KMIncluded ExtraKMRate:_selectedPackage.ExtraKMRate TransmissionType:carinfo.TransmissionType FuelType:carinfo.FuelType SeatingCapacity: carinfo.SeatingCapacity Luggage:carinfo.LuggageCapacity RequestType:reqType InsuranceSecurityAmt:insurance_Amt IGSTRate_Service:carinfo.IGSTRate_Service CGSTRate_Service:carinfo.CGSTRate_Service SGSTRate_Service:carinfo.SGSTRate_Service UTGSTRate_Service:carinfo.UTGSTRate_Service CessRate:carinfo.CessRate TaxAmount:strFinalTaxAmount  CarVariant: carinfo.CarVariant CarVariantID: carinfo.CarVariantID];
    
        //OriginalAmount:originalAmount // Swati changed firstly this was used and changed to PaymentMethod
    
//       IGSTRate_Service:carinfo.IGSTRate_Service, CGSTRate_Service:carinfo.CGSTRate_Service, SGSTRate_Service:carinfo.SGSTRate_Service, UTGSTRate_Service:carinfo.UTGSTRate_Service, CessRate:CessRate, TaxAmount:TaxAmount
    
    NSLog(@"jusPay test Data = %@",test);
    juspay.jusPayReqData = test;
    isFromPayment = true;
#ifdef DEBUG
    
    juspay.previousDateTime = _previousDateTime;
    juspay.previousMilliSec = _previousMilliSec;
   
#endif
    [[self navigationController] pushViewController:juspay animated:YES];
    
    
}
#pragma marks booking confirm juspay(protocol)
- (void)ldwFailed{
    isSecurity = true;
    isLdwAvailable = false;
    if(isLdwAvailable && [carinfo.TotalInsuranceAmt floatValue] > 0){
        isSecurity = false;
        lastSelectedButton = 0;
        
    }
    else{
        lastSelectedButton = 1;
        isSecurity = true;
        ldwAmount.text = @"";
        _lblNoSecurityDep.text = @"(Not available for this Car)";
        _lblNoSecurityDep.textColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
        _lblLdw.textColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
        ldwCheckbox.image = [UIImage imageNamed:k_greyRadio];
        _lblNoSecurityDep.alpha = 0.6;
        ldwCheckbox.alpha = 0.6;
        _lblLdw.alpha = 0.6;
        ldwAmount.alpha = 0.6;
        
        [selectPaymentOption setUserInteractionEnabled:NO];
        [_lblNoSecurityDep setUserInteractionEnabled:false];
    }
    [self setUpPaymentMod: false];

    
}
- (void)bookingConfirmation:(NSString * _Nonnull)coricID booking:(NSString *)booking strLat:(NSString *)strLat strlng:(NSString *)strlng insuranceTaken:(BOOL)insuranceTaken
{
    // Sourabh- Adding firebase topic message
    
    NSLog(@"CoricID = %@",coricID);
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    
    // [START subscribe_topic]
    NSString *locStr = [NSString stringWithFormat:@"/topics/%@",carinfo.CityName];
    [[FIRMessaging messaging] subscribeToTopic:locStr];
    NSLog(@"Subscribed to  topic");
    // [END subscribe_topic]

    kPaymentDict = [[NSMutableDictionary alloc]init];
    [kPaymentDict setObject:coricID forKey:@"trackid"];
    [kPaymentDict setObject:booking forKey:@"bookingId"];
    //changing-SSS-PaymentconfirmationFLOW
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UINavigationController *myRideNavController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:1];
        MyRidesViewController *myRideObj = (MyRidesViewController *)myRideNavController.viewControllers.firstObject;
        myRideObj.comingFromCases = @"FromJuspay";
        myRideObj.bookingIDFromJuspayPage = booking;
        //myRideController.comingFromCases = Coming
        [self.tabBarController.tabBar setHidden:FALSE];
        [self.tabBarController setSelectedIndex:1];
        [self.navigationController popToRootViewControllerAnimated:FALSE];

    });
    //[self BookingTAndC:coricID :strLat :strlng withInsurance:insuranceTaken];
}
 /**
  Show popup when payment failure
  @param Set alertview tag 101.
     */
- (void)paymentFailure
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Sorry, The booking was not successful. If your money has got deducted, it will be refunded. Please search again to book a car and get driving."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        alert.tag = 101;
        [alert show];

    });
    
    
    
}

- (void)fetchUserProfile : (void (^)(bool))callbackBlock
{
    if ([CommonFunctions reachabiltyCheck]) {
        [self showLoader];
        NSMutableDictionary *body = [NSMutableDictionary new];
        NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
        [body setObject: userid forKey:@"userId"];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSERINFO_FATCH completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   NSLog(@"statusStr =%@",statusStr);
                                                   
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           NSLog(@"RES : %@",responseJson);
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                                                               if (status == 1)
                                                               {
                                                                   UserInfo *userInfo = [[UserInfo alloc]initWithDictionary:responseJson error:nil];
                                                                   
                                                                   userDictionary = (UserInfoResponse *) userInfo.response ;
                                                                   
                                                                   userDictionary.Dob = [NSString stringWithFormat:@"%@", userDictionary.Dob];
                                                                   
                                                                   if (userDictionary.Dob != nil && userDictionary.Dob.length>0 && ![userDictionary.Dob isEqualToString:@"1/1/1900 12:00:00 AM"]) {
                                                                       [DEFAULTS setValue:userDictionary.Dob forKey:@"dob"];
                                                                       callbackBlock(true);

                                                                   }
                                                                   else
                                                                       callbackBlock(false);

                                                                   
                                                                   
                                                                   
                                                                   
                                                                  
                                                               }
                                                               else
                                                               {
                                                                   [self AlertView:responseJson[k_ApiMessageKey]];
                                                                   //                                                                   [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                                   callbackBlock(false);

                                                               }
                                                               
                                                           });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               callbackBlock(false);

                                                               [self AlertView:serviceTimeoutUnabletoreach];
                                                           });
                                                       }
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           callbackBlock(false);

                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired, Please Login to continue" preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               //[self.revealViewController revealToggleAnimated:true];
                                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });
                                                   } else
                                                   {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           callbackBlock(false);

                                                       });
                                                   }
                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:@"Myles" withMessage:KAInternet];
    }
}

-(void)AlertView :(NSString *)strMessage
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Myles" message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)dealloc {
    tableView.dataSource = nil;
    tableView.delegate = nil;
}

-(void)emptyAddedView{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:[NSBundle mainBundle]];
    EmptyViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"EmptyViewController"];
   // [self addChildViewController:controller];
    [bookBtn removeFromSuperview];
    [self.view addSubview:controller.view];
    [self.navigationController setNavigationBarHidden:YES];
//    [self presentViewController:controller animated:YES completion:nil];
    
}

-(BOOL )hidesBottomBarWhenPushed{
    return NO;
}

-(void) UpdateCouponStatus:(BOOL)isApplied{
    dispatch_async(dispatch_get_main_queue(), ^{
        CouponView.layer.cornerRadius = 5;
        CouponView.layer.borderWidth = 0.5f;
        
        [removeOfferBtn setHidden:false];
        [removeOfferBtn setUserInteractionEnabled:true];
    });
    
    if (isApplied == false){
        dispatch_async(dispatch_get_main_queue(), ^{
           CouponView.backgroundColor = UICOLOR_CouponBGColorR;
            CouponImage.image = UIImageName(@"offer");
            CouponView.layer.borderColor = UICOLOR_CouponBorderColorR.CGColor;
            CouponLabel.text = @"Have a coupon code";
            [CouponButton setUserInteractionEnabled:true];
            [removeOfferBtn setHidden:true];
            [removeOfferBtn setUserInteractionEnabled:false];
            [nextImage setHidden:false];
            
        });
        
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
             CouponView.backgroundColor = UICOLOR_CouponBGColorG;
             CouponImage.image = UIImageName(@"offerApplied");
            CouponView.layer.borderColor = UICOLOR_CouponBorderColorG.CGColor;
            CouponLabel.text = @"Coupon code applied";
            [nextImage setHidden:true];
            [removeOfferBtn setHidden:false];
            [removeOfferBtn setUserInteractionEnabled:true];
            [CouponButton setUserInteractionEnabled:false];
            
        });
       
      
    }
}
- (IBAction)RemoveOffer:(id)sender {
    //Remove_coupon
     [FIRAnalytics logEventWithName:@"Remove_coupon" parameters:nil];
    if (couponAmount > 0)
    {
        [coupontxtFd resignFirstResponder];
        couponAmount = 0;
        
        float amount = [kOriginalAmount floatValue]+ [self.amenitiesTotalAmount floatValue];
        //[kOriginalAmount floatValue] + couponAmount ;
        
        ktotalBookingAmount =  [NSString stringWithFormat:@"%.f", amount];
        [self setFairAndDetailsSectionContent:0.0];
        [self UpdateButtonPrizeLabel:ktotalBookingAmount];
        [self UpdateCouponStatus:false];
        if(fairDetailsOpened){
            tableViewHeight.constant = tableViewHeight.constant - 25.0;
            contentViewHieghtConstraint.constant -= 25.0;
            [self.view layoutIfNeeded];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
//            [tableView reloadData];
            [self tableViewReloadData];

        });
        
        couponCode = @"";
    }
}
- (IBAction)CouponButton:(id)sender {
    
  
    [FIRAnalytics logEventWithName:@"offer_clicked"parameters:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];// Find name of storyboard in your project plist
    
   CouponViewController *couponController = [storyboard instantiateViewControllerWithIdentifier:@"CouponViewController"];
    float BasicAmt = [carinfo.BasicAmt floatValue];
    int rounded = roundf(BasicAmt);
 
    
    NSString * pickUpTime = [self getDateTimeFormate:[ self.kBookingDurationMD objectForKey:@"pTime"] date:[ self.kBookingDurationMD objectForKey:@"pDate"]];
    NSString * dropUpTime = [self getDateTimeFormate:[ self.kBookingDurationMD objectForKey:@"dTime"] date:[ self.kBookingDurationMD objectForKey:@"dDate"]];
 
//    NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
//    [dtF setDateFormat:@"dd-MM-yyyy HH:mm"];
//    NSDate *d = [dtF dateFromString:pickUpTime];
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
//    NSString *st = [dateFormat stringFromDate:d];
//    NSLog(@"%@",st);
  NSMutableDictionary *body = [NSMutableDictionary new];
    [body setValue:carinfo.ModelID forKey:k_CarModelId];
    [body setValue:SelectcityId forKey:k_cityIDCap];
    [body setValue: pickUpTime forKey:k_pickUpdate];
    [body setValue:[NSString stringWithFormat:@"%d",rounded] forKey:k_totalFare];
    [body setValue:carinfo.PkgId forKey:k_pkgId];
    //[NSString stringWithFormat:@"%0.2f",carinfo.DepositeAmt.floatValue] forKey:@"DepositeAmt"]
    //[body setValue:roundedpkgDuration forKey:k_pkgDuration];
    [body setValue:0 forKey:k_pkgDuration];
    [body setValue:[DEFAULTS objectForKey:KUSER_ID] forKey:@"userId"];
    [body setValue:dropUpTime forKey:@"DropOffDate"];
    if (_doorStepDelieverySelected) {
        [body setValue:[[self.subLocationID componentsSeparatedByString:@":"] count] > 0 ?  [self.subLocationID componentsSeparatedByString:@":"][0] : self.subLocationID  forKey:@"SubLocationId"];
    } else {
        [body setValue:self.subLocationID forKey:@"SubLocationId"];
    }
    [body setValue:carinfo.WeekDayDuration forKey:@"WeekDay"];
    [body setValue:carinfo.WeekEndDuration forKey:@"WeekEnd"];
    
    couponController.couponDict = body;
    couponController.delegate = self;
    couponController.fromScreen = @"RideDetails";
    
    dispatch_async(dispatch_get_main_queue(), ^{
       [self.navigationController presentViewController:couponController animated:true completion:nil];
    });
    
        


  
}

-(void)selectedCityWithCityM:(NSString *)cityM updatedCityList: (NSString *) cityList{
    NSLog(@"ggggggg======%@",cityM);
    couponCode = cityM;
    [self UpdateCouponStatus:true];
    if (couponAmount > 0)
    {
        float amount = [ktotalBookingAmount floatValue] + couponAmount - roundf([cityList floatValue]);
        if (amount <= 0)
        {
            ktotalBookingAmount = @"1";
        }
        else
        {
            ktotalBookingAmount =  [NSString stringWithFormat:@"%.2f", amount];
        }
        couponAmount = roundf([cityList floatValue]);
    }
    else
    {
        float amount = [ktotalBookingAmount floatValue]  - roundf([cityList floatValue]) ;
        if (amount <= 0)
        {
            ktotalBookingAmount = @"1";
        }
        else
        {
            ktotalBookingAmount =  [NSString stringWithFormat:@"%.2f",amount];
        }
        couponAmount =roundf([cityList floatValue]);
    }
    [self setFairAndDetailsSectionContent:0.0];
    [self UpdateButtonPrizeLabel:ktotalBookingAmount];
    if(fairDetailsOpened){
        tableViewHeight.constant = tableViewHeight.constant + 25.0;
        contentViewHieghtConstraint.constant += 25.0;
        
        dispatch_async(dispatch_get_main_queue(), ^{
           [self.view layoutIfNeeded];
        });
        
        
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        [tableView reloadData];
        [self tableViewReloadData];

    });
   
    
}
-(void)getCouponStatusAndUpdateView:(NSString*)discountCode discountAmount:(NSString*)discountAmount{
    NSLog(@"discountCode is =====%@",discountCode);
    
}

-(NSString*)getDateTimeFormate:(NSString*)time date:(NSString *)date{
    NSMutableString *mu = [NSMutableString stringWithString:time];
    [mu insertString:@":" atIndex:2];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *dates = [dateFormatter dateFromString:date];
    
    // Convert date object into desired format
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *newDateString = [dateFormatter stringFromDate:dates];
    NSString * dateTime ;
    dateTime = [[newDateString stringByAppendingString:@" "] stringByAppendingString:mu];
    
    return dateTime;
}

-(void)tableViewReloadData{
    
    if (isMylesCover == NO) {
        
        rideBookingAnalytics = @"MylesSecureAvailable";
        [self changesDoneInNewUI];

    }
    else
    {
        rideBookingAnalytics = @"MylesSecureNA";
    }
    
    [_prcLbl setText:[NSString stringWithFormat:@"Rs. %@",carinfo.DepositeAmt]];

    [tableView reloadData];
}


-(void)changesDoneInNewUI
{
   // [securityDepositeView setHidden:YES];
   // [ldwView setHidden:YES];

    [payView.layer setBorderColor:[UIColor colorWithRed:245/255.0 green:205/255.0 blue:132/255.0 alpha:1.0].CGColor];
    [payView.layer setBorderWidth:1];
    
    if (isLdwAvailable && carinfo.InsuranceSecurityAmt.floatValue >=0)
    {
        payViewHeight.constant = 60;
       // [self addNewFeatureLaunchViewWithframe:CGRectMake(payView.frame.origin.x, payView.frame.origin.y+65, payView.frame.size.width, 60)];
        
        CGFloat x = 20;
        CGFloat y = payView.frame.origin.y+95;

        //[self newLaunchFeatureWithX:x andYOffset:y radius:40 screen:@"First"];
        contentViewHieghtConstraint.constant += 60;
        [self.view layoutIfNeeded];
    }
    
    [self.refundablePriceLbl setText:[NSString stringWithFormat:@"Rs. %d",depositAmt]];
    [self.nonRefundablePriceLbl setText:[NSString stringWithFormat:@"Rs. %@",carinfo.TotalInsuranceAmt]];
    [self.oldPriceLbl setHidden:YES];
}


- (IBAction)checkboxButtonAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    contentValue = 0;
    
    if (sender.selected)
    {
        rideBookingAnalytics = @"BookingWithMS";

        isMylesCover = YES;
        isDone= NO;
        
        isSecurity = false;
        lastSelectedButton = 0;
        
        [self setFairAndDetailsSectionContent:0.0];
        
        [self UpdateButtonPrizeLabel:self.totalFare];
        
        [self latestSectionOpened];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [tableView reloadData];
        });
        
        checkboxStatus  = YES;
        
        
        payViewHeight.constant = 180;
        contentViewHieghtConstraint.constant += 180;
        contentValue = 180;
        [_prcLbl setText:[NSString stringWithFormat:@"Rs. %d",insuranceSecurityAmt]];
        [self.oldPriceLbl setText:[NSString stringWithFormat:@"Rs. %d",depositAmt]];
        [_refundablePriceLbl setText:[NSString stringWithFormat:@"Rs. %d",insuranceSecurityAmt]];
        
        [crossedLbl setText:[NSString stringWithFormat:@"Rs. %d",depositAmt]];
        
        [lineView setHidden:NO];
        
        [self.oldPriceLbl setHidden:NO];
        [plusLabel setTextColor:[UIColor blackColor]];
        [self.view layoutIfNeeded];
        
        
//        if (insuranceSecurityAmt !=0)
//        {
//            payViewHeight.constant = 180;
//            contentViewHieghtConstraint.constant += 180;
//            contentValue = 180;
//            [_prcLbl setText:[NSString stringWithFormat:@"Rs. %d",insuranceSecurityAmt]];
//            [self.oldPriceLbl setText:[NSString stringWithFormat:@"Rs. %d",depositAmt]];
//            [_refundablePriceLbl setText:[NSString stringWithFormat:@"Rs. %d",insuranceSecurityAmt]];
//
//            [crossedLbl setText:[NSString stringWithFormat:@"Rs. %d",depositAmt]];
//
//            [lineView setHidden:NO];
//
//            [self.oldPriceLbl setHidden:NO];
//            [plusLabel setTextColor:[UIColor blackColor]];
//            [self.view layoutIfNeeded];
//        }
//        else
//        {
//            payViewHeight.constant = 120;
//            contentViewHieghtConstraint.constant += 120;
//            contentValue = 120;
//            [plusLabel setTextColor:[UIColor clearColor]];
//            [self.view layoutIfNeeded];
//        }

    }
    else
    {
        rideBookingAnalytics = @"BookingWithSD";
                isSecurity = true;
        lastSelectedButton = 1;
        [lineView setHidden:YES];

        [_prcLbl setText:[NSString stringWithFormat:@"Rs. %d",depositAmt]];
        [self.oldPriceLbl setHidden:YES];

        [self setFairAndDetailsSectionContent:0.0];
        [self UpdateButtonPrizeLabel:self.totalFare];
        [self latestSectionOpened];
        
        // [self latestSectionOpened];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [tableView reloadData];
            //                contentViewHieghtConstraint.constant -=100;
            
        });
        
        checkboxStatus  = NO;
        isMylesCover = NO;
        payViewHeight.constant = 60;
        int finalValue = contentValue-60;
        contentViewHieghtConstraint.constant -= finalValue;
        [self.view layoutIfNeeded];
    }
}

-(void)callAPIForViewBenefits
{
    if ([CommonFunctions reachabiltyCheck]) {
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
      //  [self showLoader];
        
        NSMutableDictionary * infoDict = [NSMutableDictionary new];
        [infoDict setValue:@"d" forKey:@"abc"];
        
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:infoDict session:session url:@"BenefitPopuptext" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   NSJSONSerialization * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                                   NSLog(@"BenefitPopuptext: %@",json);
                                                   
                                                   NSString *benefitText = [[json valueForKey:@"response"] valueForKey:@"Benefittext"];

                                                   benefitText = [benefitText stringByAppendingString:@"<style>body{font-family:'Opensans'; font-size:'14';}</style>"];
                                                   /*Example:
                                                    
                                                    htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx;}</style>",_myLabel.font.fontName,_myLabel.font.pointSize]];
                                                    */
                                                   benefitAtributedStr = [[NSAttributedString alloc]
                                                                                           initWithData: [benefitText dataUsingEncoding:NSUnicodeStringEncoding]
                                                                                           options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                                           documentAttributes: nil
                                                                                           error: nil
                                                                                           ];
                                                   //
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}

- (IBAction)viewBenefitAction:(id)sender {

    [FIRAnalytics logEventWithName:@"View_MS_Benefits" parameters:nil];
    ViewBenefits *benefits = [[[NSBundle mainBundle] loadNibNamed:@"BenefitsView" owner:self options:nil] lastObject];
    [benefits.benefitTextView setAttributedText:benefitAtributedStr];
    [benefits.termsConditionBtn addTarget:self action:@selector(termsConditionButttonActions:) forControlEvents:UIControlEventTouchUpInside];
    [benefits setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:benefits];
}

-(IBAction)termsConditionButttonActions:(UIButton* )sender
{
    [FIRAnalytics logEventWithName:@"View_MS_TNC" parameters:nil];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
    TNCController *objTNC = (TNCController *)[storyBoard instantiateViewControllerWithIdentifier:@"tncID"];
    [self.navigationController pushViewController:objTNC animated:YES];
}

/*- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}*/ //Swati Commented This

-(void)removeWebView
{
    UIView * backView = (UIView *)[self.view viewWithTag:999999];
    [backView removeFromSuperview];
}

-(void)addNewFeatureLaunchViewWithframe:(CGRect)frame
{
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.view.bounds];
    UIBezierPath *transparentPath = [UIBezierPath bezierPathWithRect:frame];
    [overlayPath appendPath:transparentPath];
    [overlayPath setUsesEvenOddFillRule:YES];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7].CGColor;
    [self.tabBarController.view.layer addSublayer:fillLayer];
}

-(void)newLaunchFeatureWithX:(CGFloat)xOffestss andYOffset:(CGFloat)yOffsetss radius:(int)radius screen:(NSString *)screen
{
    UIView * clearView = (UIView *)[self.view viewWithTag:111];
    if (!clearView) {
        UIView * clearView = [self addViewWithFrame:self.view.frame bgColor:[UIColor clearColor]];
        [clearView setTag:111];
        [self.view addSubview:clearView];
        
        newLaunchView = [self createOverlayFrame:self.view.frame xOffset:xOffestss yOffset:yOffsetss radius:radius];
        [clearView addSubview:newLaunchView];
        
        UIView * horizontalLine = [self addViewWithFrame:CGRectMake(xOffestss+57,yOffsetss+10, 100, 1) bgColor:[UIColor whiteColor]];
        [clearView addSubview:horizontalLine];
        
        UIView * verticalLine = [self addViewWithFrame:CGRectMake(horizontalLine.frame.origin.x+horizontalLine.frame.size.width,horizontalLine.frame.origin.y-199, 1, 200) bgColor:[UIColor whiteColor]];
        [clearView addSubview:verticalLine];
        
        UILabel *label = [self addLabelWithFrame:CGRectMake(40, self.view.frame.size.height/2-150,self.view.frame.size.width-60 , 100) text:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500." textColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [clearView addSubview:label];
        
        UIButton * previousBtn = [self addButtonWithFrame:CGRectMake(10, newLaunchView.frame.size.height-85, 60, 25) title:@"<< Prev" tag:1001];
        [previousBtn setHidden:YES];
        [newLaunchView addSubview:previousBtn];
        
        UIButton * nextBtn = [self addButtonWithFrame:CGRectMake(newLaunchView.frame.size.width-70, previousBtn.frame.origin.y, 60, previousBtn.frame.size.height) title:@"Next >>" tag:1002];
        [nextBtn setTitle:@"Finish" forState:UIControlStateSelected];
        [newLaunchView addSubview:nextBtn];

        UIButton * skipBtn = [self addButtonWithFrame:CGRectMake(nextBtn.frame.origin.x, 80, 60, previousBtn.frame.size.height) title:@"Skip >" tag:1003];
        [newLaunchView addSubview:skipBtn];
        
        if ([screen isEqualToString:@"Second"])
        {
            if (_speedGovernerIsActive) {
                [verticalLine setFrame:CGRectMake(label.frame.origin.x+label.frame.size.width/2, label.frame.origin.y+label.frame.size.height, 1, IS_IPHONE_X?121:IS_IPHONE_6P?155:185)];
            }
            else
            {
                [verticalLine setFrame:CGRectMake(label.frame.origin.x+label.frame.size.width/2, label.frame.origin.y+label.frame.size.height, 1, IS_IPHONE_6P?115:140)];
            }
            
            
            //IsSpeedGovernor
            
            [horizontalLine setFrame:CGRectMake(verticalLine.frame.origin.x, verticalLine.frame.origin.y+verticalLine.frame.size.height, 80, 1)];
            
            [previousBtn setHidden:NO];
            [nextBtn setSelected:YES];
            [skipBtn setHidden:YES];
        }
    }
}


-(UILabel *)addLabelWithFrame:(CGRect)frame text:(NSString *)text textColor:(UIColor *)textColor
{
    UILabel * label = [[UILabel alloc] initWithFrame:frame];
    [label setText:text];
    [label setTextColor:textColor];
    [label setFont:UIFONT_REGULAR_FONT(16)];
    [label setNumberOfLines:0];
    return label;
}

-(UIView *)addViewWithFrame:(CGRect)frame bgColor:(UIColor *)bgColor
{
    UIView * view = [[UIView alloc] initWithFrame:frame];
    [view setBackgroundColor:bgColor];
    return view;
}

-(UIButton *)addButtonWithFrame:(CGRect)frame title:(NSString *)title tag:(int)tag
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setTitle:title forState:UIControlStateNormal];
    [button.layer setBorderColor:[UIColor whiteColor].CGColor];
    [button.layer setBorderWidth:1.0];
    [button setTag:tag];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [button.layer setCornerRadius:4];
    [button.titleLabel setFont:UIFONT_REGULAR_FONT(11)];
    return button;
}

-(IBAction)buttonAction:(UIButton *)sender
{
    UIView * clearView = (UIView *)[self.view viewWithTag:111];

    if (sender.tag ==1001)
    {
        CGFloat x = 20;
        CGFloat y = payView.frame.origin.y+95;
        
        isNextFeature = NO;

        if (clearView)
        {
            [clearView removeFromSuperview];
            clearView = nil;

            //[self newLaunchFeatureWithX:x andYOffset:y radius:40 screen:@"First"];

        }
        else
        {
            //[self newLaunchFeatureWithX:x andYOffset:y radius:40 screen:@"First"];
        }

    }
    else if (sender.tag==1002)
    {
        if ([sender.titleLabel.text isEqualToString:@"Next >>"])
        {
            if (isNextFeature) {
                [clearView removeFromSuperview];
                clearView = nil;
            }
            else
            {
                isNextFeature = YES;
                
                [clearView removeFromSuperview];
                clearView = nil;
                
               // [self newLaunchFeatureWithX:securityBackView.frame.size.width-30 andYOffset:securityBackView.frame.origin.y+securityBackView.frame.size.height+30 radius:53 screen:@"Second"];
            }
        }
        else
        {
            [clearView removeFromSuperview];
            clearView = nil;
        }
       
    }
    else
    {
        [clearView removeFromSuperview];
        clearView = nil;

    }
}

-(UIView* )createOverlayFrame:(CGRect)frame xOffset:(CGFloat)xOffset yOffset:(CGFloat)yOffset radius:(CGFloat)radius
{
    UIView * overlayView = [[UIView alloc] initWithFrame:frame];
    [overlayView setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:0.8]];
    
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddArc(path, nil, xOffset, yOffset, radius, 0.0, 2.0 * 3.14, false);
    CGPathAddRect(path, nil, CGRectMake(0, 0, overlayView.frame.size.width, overlayView.frame.size.height));
    
    CAShapeLayer * maskLayer = [[CAShapeLayer alloc] init];
    [maskLayer setBackgroundColor:[UIColor blackColor].CGColor];
    [maskLayer setPath:path];
    maskLayer.fillRule = kCAFillRuleEvenOdd;
    
    overlayView.layer.mask = maskLayer;
    overlayView.clipsToBounds = YES;
    
    return overlayView;
}


/*
 DONT DELETE IT
 
-(void)newFunction
{
    TransparentView* yourView = [[TransparentView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    yourView.overallColor = [UIColor blackColor];
    yourView.alpha = 0.5;
    yourView.rectForClearing = CGRectMake(0, payView.frame.origin.y+65, payView.frame.size.width, 60);
    [self.tabBarController.view addSubview:yourView];

}

*/



@end
