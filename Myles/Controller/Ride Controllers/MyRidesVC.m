//
//  MyRidesVC.m
//  Myles
//
//  Created by Myles on 03/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "MyRidesVC.h"

@interface MyRidesVC ()
{
    
}

@property (nonatomic)  UIBarButtonItem* revealButtonItem;

@end

@implementation MyRidesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:k_listImg] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector( revealToggle: )];
    self.navigationItem.leftBarButtonItem = self.revealButtonItem;
    [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
