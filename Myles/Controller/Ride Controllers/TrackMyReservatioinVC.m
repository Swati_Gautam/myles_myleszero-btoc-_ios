//
//  TrackMyReservatioinVC.m
//  VincentLimousin
//
//  Created by Shivam on 7/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TrackMyReservatioinVC.h"

@interface TrackMyReservatioinVC ()

    -(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded;
    -(void) updateRouteView;
    -(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) from to: (CLLocationCoordinate2D) to;
    -(void) centerMap;

@end

@implementation TrackMyReservatioinVC
@synthesize mapView;
@synthesize lineColor;
@synthesize prevReservationButton,anotherTripButton;
@synthesize scrollView;
@synthesize pickLocationTF;
@synthesize etaTF;
@synthesize reservationNumberTF;
@synthesize toolBar;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self)
    {
        [self.view addSubview:self.scrollView];
		mapView.showsUserLocation = YES;
		[mapView setDelegate:self];
         [self.scrollView addSubview:mapView];
		routeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, mapView.frame.size.width, mapView.frame.size.height)];
		routeView.userInteractionEnabled = NO;
		[mapView addSubview:routeView];
        
        [self.view addSubview:toolBar];
		
		self.lineColor = [UIColor blueColor];
	}
	return self;
}

- (void)viewDidLoad
{
    [CommonFunctions addCustomTitleToNavigationItem:self.navigationItem withTitleString:@"Track My Reservation"];
    
    self.navigationItem.hidesBackButton=YES;
    
    UIButton * backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setFrame:IS_IPAD?CGRectMake(0.0f,0.0f,65.0f,29.0f):CGRectMake(0.0f,0.0f,52.0f,32.0f)];
    [backBtn setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"backButton"]] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithCustomView:backBtn];
    [toolBar setFrame:CGRectMake(0, 480, 320, 44)];
    placeFromDic = [[NSMutableDictionary alloc] init];
    reservationArray = [[NSMutableArray alloc] init];
    
    [[prevReservationButton titleLabel] setFont:IS_IPAD?[UIFont fontWithName:@"HelveticaNeue LT 57 Cn" size:14.0f]:[UIFont fontWithName:@"HelveticaNeue LT 57 Cn" size:14.0f]];
    [[anotherTripButton titleLabel] setFont:IS_IPAD?[UIFont fontWithName:@"HelveticaNeue LT 57 Cn" size:14.0f]:[UIFont fontWithName:@"HelveticaNeue LT 57 Cn" size:14.0f]];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [HUD setDelegate:self];
    [self.view addSubview:HUD];
    [HUD showWhileExecuting:@selector(parsingTBXML) onTarget:self withObject:nil animated:TRUE];
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded {
	[encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
								options:NSLiteralSearch
								  range:NSMakeRange(0, [encoded length])];
	NSInteger len = [encoded length];
	NSInteger index = 0;
	NSMutableArray *array = [[NSMutableArray alloc] init];
	NSInteger lat=0;
	NSInteger lng=0;
	while (index < len) {
		NSInteger b;
		NSInteger shift = 0;
		NSInteger result = 0;
		do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lat += dlat;
		shift = 0;
		result = 0;
		do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lng += dlng;
		NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
		NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
		printf("[%f,", [latitude doubleValue]);
		printf("%f]", [longitude doubleValue]);
		CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
		[array addObject:loc];
	}
	
	return array;
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t {
	NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
	NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
	
	NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
	NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
	NSLog(@"api url: %@", apiUrl);
	NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSUTF8StringEncoding error:nil];
	NSString* encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
	
	return [self decodePolyLine:[encodedPoints mutableCopy]];
}

-(void) centerMap {
	MKCoordinateRegion region;
    
	CLLocationDegrees maxLat = -90;
	CLLocationDegrees maxLon = -180;
	CLLocationDegrees minLat = 90;
	CLLocationDegrees minLon = 180;
	for(int idx = 0; idx < routes.count; idx++)
	{
		CLLocation* currentLocation = [routes objectAtIndex:idx];
		if(currentLocation.coordinate.latitude > maxLat)
			maxLat = currentLocation.coordinate.latitude;
		if(currentLocation.coordinate.latitude < minLat)
			minLat = currentLocation.coordinate.latitude;
		if(currentLocation.coordinate.longitude > maxLon)
			maxLon = currentLocation.coordinate.longitude;
		if(currentLocation.coordinate.longitude < minLon)
			minLon = currentLocation.coordinate.longitude;
	}
	region.center.latitude     = (maxLat + minLat) / 2;
	region.center.longitude    = (maxLon + minLon) / 2;
	region.span.latitudeDelta  = maxLat - minLat+0.01;
	region.span.longitudeDelta = maxLon - minLon;
    
    ///When path coordinate not found//
	if( region.center.latitude == 0.0 && region.center.longitude == 0.0 )
    {
        return;
    }
	[mapView setRegion:region animated:YES];
}

-(void) showRouteFrom: (PlaceMark*) from to:(PlaceMark*) to
{
	if(routes)
    {
		[mapView removeAnnotations:[mapView annotations]];
    }
	
	[mapView addAnnotation:from];
	[mapView addAnnotation:to];
	
	routes = [self calculateRoutesFrom:from.coordinate to:to.coordinate];
	
	[self updateRouteView];
	[self centerMap];
}

-(void) updateRouteView {
	CGContextRef context = 	CGBitmapContextCreate(nil, 
												  routeView.frame.size.width, 
												  routeView.frame.size.height, 
												  8, 
												  4 * routeView.frame.size.width,
												  CGColorSpaceCreateDeviceRGB(),
												  kCGImageAlphaPremultipliedLast);
	
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);
	CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1.0);
	CGContextSetLineWidth(context, 3.0);
	
	for(int i = 0; i < routes.count; i++) {
		CLLocation* location = [routes objectAtIndex:i];
		CGPoint point = [mapView convertCoordinate:location.coordinate toPointToView:routeView];
		
		if(i == 0) {
			CGContextMoveToPoint(context, point.x, routeView.frame.size.height - point.y);
		} else {
			CGContextAddLineToPoint(context, point.x, routeView.frame.size.height - point.y);
		}
	}
    
	CGContextStrokePath(context);
	
	CGImageRef image = CGBitmapContextCreateImage(context);
	UIImage* img = [UIImage imageWithCGImage:image];
	
	routeView.image = img;
	CGContextRelease(context);
}

#pragma mark mapView delegate functions
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
	routeView.hidden = YES;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
	[self updateRouteView];
	routeView.hidden = NO;
	[routeView setNeedsDisplay];
}
- (MKAnnotationView *) mapView:(MKMapView *)mapView 
			 viewForAnnotation:(id <MKAnnotation>) annotation {
    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc] 
								  initWithAnnotation:annotation reuseIdentifier:@"pin"];
    if([[annotation title] isEqualToString:@"Konstant Info"])
        annView.pinColor = MKPinAnnotationColorGreen;
    else if([[annotation title] isEqualToString:@"Railway Station"])	
        annView.pinColor = MKPinAnnotationColorRed;
    else if([[annotation title] isEqualToString:@"Current Location"])	
        annView.pinColor = MKPinAnnotationColorPurple;
    return annView;
}


-(void) parsingTBXML
{
    NSLog(@"parsingTBXML called");
    TBXML *tbxml =[[TBXML alloc] initWithURL:[NSURL URLWithString:@"http://192.168.0.118/vincent/trackreservation.xml"]];
    NSLog(@"tbxml : %@",tbxml);
    if(tbxml)
    {
        TBXMLElement * root = tbxml.rootXMLElement; 
        if(root)
        {
            NSString *replyCode=[TBXML valueOfAttributeNamed:@"replyCode" forElement:root];
            NSLog(@"reply code : %@",replyCode);
            if([replyCode isEqualToString: @"success"])
            {
                TBXMLElement *placesElement = [TBXML childElementNamed:@"Reservations" parentElement:root];
                
                NSString  *totalCount  = [TBXML valueOfAttributeNamed:@"totalCount" forElement:placesElement];
                
                NSLog(@"totalCount =%@",totalCount);
                
                TBXMLElement *placeFrom = [TBXML childElementNamed:@"Reservation" parentElement:placesElement];
                
                while (placeFrom!= nil)  
                {
                NSString  *placeFromTitle  = [TBXML valueOfAttributeNamed:@"fromTitle" forElement:placeFrom];
                NSString  *placeFromSubTitle  = [TBXML valueOfAttributeNamed:@"fromSubtitle" forElement:placeFrom];
                NSString  *placeFromLat  = [TBXML valueOfAttributeNamed:@"fromLatitude" forElement:placeFrom];
                NSString  *placeFromLong  = [TBXML valueOfAttributeNamed:@"fromLongitude" forElement:placeFrom];
     
                
                NSString  *placeToTitle  = [TBXML valueOfAttributeNamed:@"toTitle" forElement:placeFrom];
                NSString  *placeToSubTitle  = [TBXML valueOfAttributeNamed:@"toSubtitle" forElement:placeFrom];
                NSString  *placeToLat  = [TBXML valueOfAttributeNamed:@"toLatitude" forElement:placeFrom];
                NSString  *placeToLong  = [TBXML valueOfAttributeNamed:@"toLongitude" forElement:placeFrom];
             
                placeFromDic = [NSDictionary dictionaryWithObjectsAndKeys:placeFromTitle,@"fromTitle",placeFromSubTitle,@"fromSubtitle",placeFromLat,@"fromLatitude",placeFromLong,@"fromLongitude",placeToTitle,@"toTitle",placeToSubTitle,@"toSubtitle",placeToLat,@"toLatitude",placeToLong,@"toLongitude", nil];
                    
                    [reservationArray addObject:placeFromDic];
                placeFrom = [TBXML nextSiblingNamed:@"Reservation" searchFromElement:placeFrom];
                
            }
            
            }
            else
            {
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"The Username Or Password is incorrect!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    NSLog(@"parsingTBXML exited");
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{    
    if ([textField isEqual:reservationNumberTF])
    {
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.30];
        toolBar.frame = CGRectMake(0, 156, 320, 44);
        [toolBar setHidden:NO];
        [UIView commitAnimations]; 
    }
    [self scrollViewToCenterOfScreen:textField];
}
- (void) scrollViewToCenterOfScreen:(UIView *)theView 
{  
    CGFloat viewCenterY = theView.center.y;  
    
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];  
    
    CGFloat availableHeight = applicationFrame.size.height - 200;            // Remove area covered by keyboard  
    
    CGFloat y = viewCenterY - availableHeight / 2.0;  
    
    if (y < 0) 
    {  
        y = 0;  
    }
    
    [scrollView setContentOffset:CGPointMake(0, y) animated:YES];  
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    /* 
     Tag is Given to Every Text Field As--
     ----------------------------
     TextField:            Tag:
     ----------------------------
     Pick Up Location       1
     ETA.                   2
     Reservation No         3
     ----------------------------
    */

    int i = textField.tag;
    if (i<3)
    {
            UITextField *nextTextField = (UITextField*)[[self view] viewWithTag:i+1];
            [nextTextField becomeFirstResponder];
    }
    else if (i==3)
    {        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.40];
        toolBar.frame = CGRectMake(0, 156, 320, 44);
        [toolBar setHidden:NO];
        [UIView commitAnimations]; 
    }
    else
    {
        [textField resignFirstResponder];
        [scrollView setContentOffset:CGPointMake(0.0f, 0.0f) animated:TRUE];

        //------------Code To Be done-------//
        //call API to get previous reservation Tracks
    }
    return TRUE;
}
- (IBAction)doneButtonPressed:(id)sender
{
    [reservationNumberTF resignFirstResponder];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.30];
    toolBar.frame = CGRectMake(0, 480, 320, 44);
    [toolBar setHidden:YES];
    [UIView commitAnimations];   
    [scrollView setContentOffset:CGPointMake(0.0f, 0.0f) animated:TRUE];
}
-(void) hudWasHidden
{
    [HUD removeFromSuperview];
    if ([reservationArray count] >0) 
    {
        placeFromDic = [reservationArray objectAtIndex:0];
    
    PlaceMark *from = [[PlaceMark alloc] initWithLatitude:[[placeFromDic objectForKey:@"fromLatitude"] doubleValue] Longitude:[[placeFromDic objectForKey:@"fromLongitude"] doubleValue] Title:[placeFromDic objectForKey:@"fromTitle"] andSubTitle:[placeFromDic objectForKey:@"fromSubTitle"]];
    PlaceMark *to = [[PlaceMark alloc] initWithLatitude:[[placeFromDic objectForKey:@"toLatitude"] doubleValue] Longitude:[[placeFromDic objectForKey:@"toLongitude"] doubleValue] Title:[placeFromDic objectForKey:@"toTitle"] andSubTitle:[placeFromDic objectForKey:@"toSubTitle"]];
    
    NSLog(@"PlaceFrom Discription %@",from.title);
    NSLog(@"PlaceFrom Discription %@",to.title);
    [self showRouteFrom:from to:to];
    }
}

- (IBAction)anotherTripButtonPressed:(id)sender
{
    PickUpLocationVC *pulvc = [[PickUpLocationVC alloc] initWithNibName:[CommonFunctions getNibNameForName:@"PickUpLocation"] bundle:nil];
    [[self navigationController] pushViewController:pulvc animated:YES];
}
- (IBAction)previousReservationButtonPressed:(id)sender
{
    NSLog(@"previous btn touched");
    SelectAnotherTripVC *electAnotherTripVC = [[SelectAnotherTripVC alloc] initWithNibName:[CommonFunctions getNibNameForName:@"SelectAnotherTrip"] bundle:nil];
    [[self navigationController] pushViewController:electAnotherTripVC animated:YES];
}

//Selector Method Of Back Button
- (void)backBtnPressed{ 
    UINavigationController *navController = (UINavigationController *)  self.navigationController;
    [navController popViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
