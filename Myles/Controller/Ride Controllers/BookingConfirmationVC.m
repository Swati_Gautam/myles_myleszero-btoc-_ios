
#import "BookingConfirmationVC.h"
#import "RideHistoryDetails.h"
#import "HomeViewController.h"
#import "Myles-Swift.h"
#import "UserProfileVC.h"
#import "MenuViewController.h"
#import  "SorryAlertViewController.h"
#import "RideHistoryResponse.h"
#import <Google/Analytics.h>
@import FirebaseAnalytics;

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;

@interface BookingConfirmationVC ()<SorryAlertProtocol>
{
    BookingConfirmationCustomCell *firstcell;
    BookingConfirmationCustomCell *mapCell;
    BOOL dragging;
    CGFloat lastContentOffset ;
    NSInteger numberofCell;
    NSString *title;
    BOOL activeRide;
    NSString *rideStatusstr;
    NSInteger hoursBetweenDates ;
    RideHistoryDetails *rideDetail;
    NSDictionary *tempInfo;
    SorryAlertViewController *controller;
    GMSMarker *locationMarker;
    NSTimer *timer;
    //Variable to manage color of payment status.
    NSAttributedString * attributeStrPaymentStatus;  //Paid - Red, Not Paid-Red, Click here to pay-Blue
    
    NSAttributedString * attributePaidPaymentStatus;
    NSAttributedString * attributeNotPaidPaymentStatus;
    NSAttributedString * attributePayPaymentStatus;
    
}

@end

@implementation BookingConfirmationVC

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (void)viewDidLoad {
       

    [super viewDidLoad];
    
    NSLog(@"self.navigationController.viewControllers after -> %@",self.navigationController.viewControllers
          );
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Booking Details"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //[self customizeLeftRightNavigationBar:@"Booking Details" RightTItle:k_phonecallRed Controller:self Present:true];
    
    [FIRAnalytics logEventWithName:kFIREventEcommercePurchase parameters:nil];
    
    //if (self.comingFromController == AfterBookingConfirmation) {
    //} else {
        [self customizeLeftRightNavigationBar:@"Booking Details" RightTItle:k_phonecall Controller:self Present:true];

    //}
    
    
    //k_phonecall
    //[self customizeLeftRightNavigationBar:@"My Rides" RightTItle:nil Controller:self Present:true];
    
    
    self.trackmyrideBtn.layer.masksToBounds = YES;
    self.trackmyrideBtn.layer.cornerRadius = 3.0;
    activeRide = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshViewData:)
                                                 name:@"preauthPaymentComplete" object:nil];
    
    
    //Navigation Title
    if ([rideDetail.Tripstatus isEqualToString:k_tripstatusNew])
    {
        rideStatusstr = k_cancelRed;
    }
    else
        rideStatusstr = @"";
    dragging = false;
    [self checkValidationForScreenSetUp];
//    if (self.comingFromController == AfterBookingConfirmation) {
//        //[self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:true];
//    } else {
        [self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:false];
   // }
    
    //TableView
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tag = 1000;
    numberofCell = 4 ;
    self.scrollview.delegate = self;
    
    ////NSLog(@"rideDetail.PreAuthAmount =%@",rideDetail.PreAuthAmount);
    
    
    if (self.navigationController.viewControllers.count > 1)
    {
        if (self.comingFromController == AfterBookingConfirmation) {
            if (self.displayAlert)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KMessage message:kUploadDocument delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Now", nil];
                alert.tag = 3;
                [alert show];
                self.displayAlert  = NO;
            }

        }
    }
    else
    {
        
        
    }
    
    if (self.showPaymentAlert && [[self.ridePaymentData valueForKey:@"Tripstatus"] isEqualToString:@"New"]) {
        
        NSInteger PreauthStatus = [[self.ridePaymentData valueForKey:@"PreAuthStatus"] isKindOfClass:[NSNull class]] == NO ? [[self.ridePaymentData valueForKey:@"PreAuthStatus"] integerValue] : -1;
        //BOOL insurancePaid = [[self.ridePaymentData valueForKey:@"InsurancePaid"] boolValue];
        if (PreauthStatus == 0 && !rideDetail.InsurancePaid) {
            NSMutableDictionary *paymentInfo = [[NSMutableDictionary alloc]init];
            [paymentInfo setValue:[self.ridePaymentData valueForKey:@"trackID"] forKey:@"trackID"];
            [paymentInfo setValue:[self.ridePaymentData valueForKey:@"PickupDate"] forKey:@"date"];
            [paymentInfo setValue:[self.ridePaymentData valueForKey:@"CarModelName"] forKey:@"model"];
            [paymentInfo setValue:[self.ridePaymentData valueForKey:@"Bookingid"] forKey:@"bookingId"];
            [paymentInfo setValue:[self.ridePaymentData valueForKey:@"PreAuthAmount"] forKey:@"preAuth"];
            [paymentInfo setValue:[self.ridePaymentData valueForKey:@"DropoffDate"] forKey:@"DropoffDate"];
            tempInfo = paymentInfo;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
            controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
            controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            controller.alertType    = 7;
            controller.delegate     = self;
            controller.navController = self.navigationController;
            //NSLog(@"paymentInfo =%@",paymentInfo);
            controller.secureInfo  = paymentInfo;
            if (controller != nil)
            {
                [self presentViewController:controller animated:YES completion:nil];
            }
        } else {
            //INSURANCE CASE
        }
    }
    
    if (hoursBetweenDates < 2) {
        
        timer = [NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(showPayment:) userInfo:nil repeats:YES];
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Booking Details"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidDisappear:(BOOL)animated{
    
    
    if (mapCell) {
        [mapCell.mapView removeFromSuperview];
        [mapCell.mapView setDelegate:nil];
        [mapCell.mapView removeAnnotations:mapCell.mapView.annotations];
        mapCell.mapView = nil;
    }
    
    if ([timer isValid]) {
        
        [timer invalidate];
        timer = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




//-(void) preauthPaymentNetBanking : (NSNotification *) notification {
//
//    NSDictionary *dataDic = self.paymentInfo;
//    if (dataDic)
//        [self payNimoController:KNetbanking TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
//
//        //[self ecommerceTracking:@"Payment_Security_Nb" TranactionAmount:[dataDic objectForKey:@"preAuth"] TrackId:[dataDic objectForKey:@"trackid"] BookingId:[dataDic objectForKey:@"bookingId"]];
//
//    [self ecommerceTracking:@"Payment_Security_Nb" TranactionAmount:[dataDic objectForKey:@"preAuth"] TrackId:[dataDic objectForKey:@"trackid"] BookingId:[dataDic objectForKey:@"bookingId"] CarModelName:[dataDic objectForKey:@"model"]];
//
//}

-(void) preauthPaymentCard : (NSNotification *) notification {
    NSDictionary *dataDic = self.paymentInfo;
    if (dataDic)
//        [self payNimoController:KCards TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
    //[self ecommerceTracking:@"Payment_Security_Card" TranactionAmount:[dataDic objectForKey:@"preAuth"] TrackId:[dataDic objectForKey:@"trackid"] BookingId:[dataDic objectForKey:@"bookingId"]];
    [self ecommerceTracking:@"Payment_Security_Card" TranactionAmount:[dataDic objectForKey:@"preAuth"] TrackId:[dataDic objectForKey:@"trackid"] BookingId:[dataDic objectForKey:@"bookingId"] CarModelName:[dataDic objectForKey:@"model"]];
}


//- (void)payNimoController : (NSString *)title TranactionAmount : (NSString *)totalamount andTrackId:(NSString *)trackId
//{
//    
//    CommonApiClass *commonApi = [[CommonApiClass alloc]init];
//    commonApi.delegate = self;
//    NSString *mytestTitle = title;
//    
//    //Payment amount 1.01
//    [commonApi payNimoController:mytestTitle TranactionAmount:totalamount Date:[self formatDate:[self.ridePaymentData valueForKey:@"PickupDate"]] withType:YES andEndDate:@"" withGap:0 andTrackId:[self.ridePaymentData valueForKey:@"trackID"]];
//}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = false;
    self.navigationController.navigationBar.translucent = NO;
    [self.tableView reloadData];
}

- (void)setLenghtAccordingToRide
{
    [self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:false];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.trackmyrideBtn.hidden = true;
    self.tableViewBottomLayout.constant = 0;
    [self.view layoutIfNeeded];
    self.buttonSubview.hidden = true;
    self.buttonheight.constant = 0;
    [self.scrollview updateConstraints];
    [self.trackmyrideBtn updateConstraints];
}


#pragma mark- Right Button
- (void)rightButton : (UIButton *)sender
{
    [self CancelBtnAction:sender];
}

- (void) callerButton : (UIButton *)sender
{
    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}

#pragma mark - SorryAlertProtocol

- (void)preAuthNetBanking{
    
    NSMutableDictionary *paymentInfo = [[NSMutableDictionary alloc]init];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"trackID"] forKey:@"trackID"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"PickupDate"] forKey:@"date"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"CarModelName"] forKey:@"model"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"Bookingid"] forKey:@"bookingId"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"PreAuthAmount"] forKey:@"preAuth"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"DropoffDate"] forKey:@"DropoffDate"];
    tempInfo = paymentInfo;
    
    NSDictionary *dataDic = tempInfo;
//    if (dataDic)
//        [self payNimoController:KNetbanking TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
}

- (void)preAuthCardPayment{
    
    NSMutableDictionary *paymentInfo = [[NSMutableDictionary alloc]init];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"trackID"] forKey:@"trackID"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"PickupDate"] forKey:@"date"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"CarModelName"] forKey:@"model"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"Bookingid"] forKey:@"bookingId"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"PreAuthAmount"] forKey:@"preAuth"];
    [paymentInfo setValue:[self.ridePaymentData valueForKey:@"DropoffDate"] forKey:@"DropoffDate"];
    tempInfo = paymentInfo;
    
    NSDictionary *dataDic = tempInfo;
//    if (dataDic)
//        [self payNimoController:KCards TranactionAmount:[dataDic objectForKey:@"preAuth"] andTrackId:[dataDic objectForKey:@"trackid"]];
}

#pragma mark- Left button
- (void)leftBtnTouchedPop
{
    if (self.navigationController.viewControllers.count > 1)
    {
        if (self.comingFromController == AfterBookingConfirmation) {
            [self.navigationController popToRootViewControllerAnimated:TRUE];
            
        } else {
            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:true];
            [super leftBtnTouchedPop];
        }
        
    }
    else
    {
        SWRevealViewController *revealController = self.revealViewController;
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
        UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
        HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
        navController.viewControllers = @[homeController];
        [revealController pushFrontViewController:navController animated:YES];
        
    }
}
#pragma mark- setRide Details

- (void)showPayment:(NSTimer *)timer{
    if (controller != nil)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void) setRideInfomation  : (NSDictionary *)dic;
{
    rideDetail = [[RideHistoryDetails alloc]initWithDictionary:dic error:nil];
    if(rideDetail == nil)
    {
        rideDetail = dic.copy;
    }
}

- (void)checkValidationForScreenSetUp
{
    if (([rideDetail.Tripstatus isEqualToString:k_tripstatusNew] ) || [rideDetail.Tripstatus isEqualToString:k_tripstatusOpen])
    {
        if (([rideDetail.PreAuthStatus integerValue] != 1) && ([rideDetail.PreAuthAmount integerValue] > 0))
        {
            
        }
        title = @"Booking Details";
        
        activeRide = true;
    }
    else if ([rideDetail.Tripstatus isEqualToString:k_tripstatusCancel] )
    {
        
        title = k_BookingTitleCancel;
        [self setLenghtAccordingToRide];
        activeRide = false;
        rideStatusstr = @"";
    }
    else
    {
        title = k_BookingTitleClose;
        activeRide = false;
        [self setLenghtAccordingToRide];
        rideStatusstr = @"";
    }
}

- (void)payPreAuth
{
    NSMutableArray *pDateAr = [[ NSMutableArray alloc]initWithArray:[rideDetail.PickupDate componentsSeparatedByString:@"-"]];
    [pDateAr exchangeObjectAtIndex:1 withObjectAtIndex:0];
    NSString *pDate = [pDateAr componentsJoinedByString:@"-"];
    hoursBetweenDates = [CommonFunctions getPreAuthStatusAsPerStartTime:rideDetail.PickupTime Date:pDate];
    if (hoursBetweenDates > 14*24)
    {
        //NSLog(@"Ride Date : %@  Ride Time : %@  \n Ride Status %@", rideDetail.PickupDate,rideDetail.PickupTime,rideDetail.Tripstatus);
    }
    else if (hoursBetweenDates >= 0)
    {
        //NSLog(@"Ride Date : %@  Ride Time : %@ \n Ride Status %@", rideDetail.PickupDate,rideDetail.PickupTime,rideDetail.Tripstatus);
        
        //Divya Date March 10, 2016
        [self securityAmountPayAction];
    }
}


#pragma mark- Button Action
- (IBAction)trackRIdeBtnAction:(UIButton *)sender
{
    
    /*
    if (([rideDetail.Tripstatus isEqualToString:k_tripstatusNew]) || ([rideDetail.Tripstatus isEqualToString:k_tripstatusOpen]))
    {
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:true];
        CLLocation *location = [[CLLocation alloc]initWithLatitude:[[rideDetail Lat] doubleValue] longitude:[[rideDetail Lon] doubleValue]];
        
        UIStoryboard *storyboard = k_storyBoard(k_MyRideStoryBoard);
        TrackRide *trackVC = [storyboard instantiateViewControllerWithIdentifier:k_TrackMyRideViewController ];
        trackVC.tripStatus = rideDetail.Tripstatus;
        NSArray *array;
        if (rideDetail.AmenitiesDetails.count > 0)
        {
            array = rideDetail.AmenitiesDetails.copy;
        }
        
        NSString *address = (rideDetail.sublocationAdd.length > 0) ? rideDetail.sublocationAdd : rideDetail.sublocationName;
        trackVC.MylesAddress = address;
        trackVC.bookingId = rideDetail.Bookingid;
        trackVC.destinationLocation = location;
        
        if ([rideDetail.Tripstatus isEqualToString:k_tripstatusNew])
        {
            [trackVC setSourceAndDestinationLocation:location address:address Id:rideDetail.Bookingid];
        }
        [self.navigationController pushViewController:trackVC animated:true];
    }
     */
}

#pragma mark- UITableViewDataSource, UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.row  == 0) && (indexPath.section == 0))
        return 50;
    else if ((indexPath.row ==1) && (indexPath.section == 0))
        return 120;
    else if ((indexPath.row ==2) && (indexPath.section == 0))
        return 120;
    
    else if ((indexPath.row ==3) && (indexPath.section == 0))
        return 93;
    else if ((indexPath.row == 0) && (indexPath.section == 2))
        return 300;
    else
        return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if  (section == 0)
        return 4;
    else if (section == 2)
        return 1;
    else
        return rideDetail.AmenitiesDetails.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ((section == 1) && (rideDetail.AmenitiesDetails.count > 0))
    {
        return @"Amenities";
    }
    else
    {
        return @"";
    }
}



/*
 UIButton *button = (UIButton *)[cell.contentView viewForTag:55];
 
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    NSInteger count ;
    NSString *paymentStatusStr = rideDetail.PreAuthStatus;
    
    
    if ((indexPath.row  == 0) && (indexPath.section == 0))
    {
        identifier = @"Cell";
        count = 0;
    }
    else if ((indexPath.row ==1) && (indexPath.section == 0))
    {
        identifier = k_BookingcarDetailCellIdentifier;
        count = 1;
    }
    else if ((indexPath.row ==2) && (indexPath.section == 0))
    {
        identifier = k_BookingPreauthPaymentCellIdentifier;
        count = 2;
        /*
         BookingConfirmationCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
         if ( cell == nil )
         {
         cell = [[BookingConfirmationCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
         }
         cell.preauthPaymentLink.tag=indexPath.row;
         [cell.preauthPaymentLink addTarget: self
         action: @selector(myPaymentAction:)
         forControlEvents: UIControlEventTouchUpInside];
         */
    }
    else if ((indexPath.row ==3) && (indexPath.section == 0))
    {
        identifier = k_BookingtimeDetailCellIdentifier;
        count = 3;
    }
    else if ((indexPath.row == 0) && (indexPath.section == 2))
    {
        identifier = k_BookingPickUpDetailCellIdentifier;
        count = 4;
    }
    else
    {
        count = 5;
        identifier = k_BookingAmenitesDetailCellIdentifier;
    }
    BookingConfirmationCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if ( cell == nil )
    {
        cell = [[BookingConfirmationCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (firstcell == nil)
    {
        firstcell = cell;
    }
    // roundoff price value
    
    float amountToRoundoff = [rideDetail.PaymentAmount floatValue];
    int rounded = roundf(amountToRoundoff);
    rideDetail.PaymentAmount = [NSString stringWithFormat:@"%d",rounded];
    // //NSLog(@"Value in string after rounding off =%@",ktotalBookingAmount);
    
    // setting cell attributes values
    switch (count)
    {
        case 0:
            //NSLog(@"Zeroth Index");
            [cell setZerothRow:(NSDictionary*)rideDetail];
            break;
        case 1:
            //NSLog(@"First Index");
            [cell setfirstRowCarModelPrize:rideDetail.CarModelName Prize:rideDetail.PaymentAmount Seating:rideDetail.SeatingCapacity BookingId:rideDetail.Bookingid];
            break;
        case 2:
            //
            //set title of the cell at this place
            
            //[cell.preauthPaymentLink setTitle:self.securityPaymentStatus forState:UIControlStateNormal];
            //cell.preauthPaymentLink.titleLabel.textAlignment = NSTextAlignmentCenter;
            // //NSLog(@"New button title: %@", cell.preauthPaymentLink.titleLabel.text);
            //[cell setPaymentRowDetailInformation:rideDetail.PaymentAmount PreauthAmount:rideDetail.PreAuthAmount PreauthStatus:self.securityPaymentStatus];
            //NSLog(@"second Index");
            if (rideDetail.InsurancePaid) {
                [cell.preauthPaymentLink addTarget: self
                                            action: @selector(openInsurance)
                                  forControlEvents: UIControlEventTouchUpInside];
            } else {
                [cell.preauthPaymentLink addTarget: self
                                            action: @selector(myPaymentAction:)
                                  forControlEvents: UIControlEventTouchUpInside];
            }
            
            [cell setPaymentRow:(NSDictionary*)rideDetail];
            cell.preauthPaymentLink.tag=indexPath.row;
            //            if (bPAid) {
            //                [cell.preauthPaymentLink setTitle:nil forState:UIControlStateNormal];
            //
            //                [cell.preauthPaymentLink setTitle:@"Paid" forState:UIControlStateNormal];
            //
            //                [cell.preauthPaymentLink setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            //
            //                [cell.preauthPaymentLink removeTarget:nil
            //                                   action:NULL
            //                         forControlEvents:UIControlEventAllEvents];
            //            }
            //            else
            //            {
            
            
           
            //            }
            break;
            
        case 3:
            [cell setSecondRowRideTime:rideDetail.PickupDate FromTime:rideDetail.PickupTime ToDate:rideDetail.DropoffDate ToTime:rideDetail.DropoffTime];
            break;
            
        case 4:
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            if (rideDetail.HomePickDropAdd != nil)
                [dict setObject:rideDetail.HomePickDropAdd forKey:@"pickUp"];
            [dict setObject:rideDetail.sublocationName forKey:@"sublocation"];
//            if (rideDetail.AmenitiesDetails.count > 0)
//            {
                cell.pickUpAddressLabel.hidden = false;
                cell.pickUpTitleLabel.hidden = false;
//            }
            mapCell = cell;
            [cell setThirdRowAddress:dict PointLocation:CLLocationCoordinate2DMake([rideDetail.Lat doubleValue], [rideDetail.Lon doubleValue]) Title: rideDetail.sublocationName];
        }
            break;
        default:
        {
            //NSLog(@"amenties");
            AmenitiesDetails *info = rideDetail.AmenitiesDetails[indexPath.row];
            [cell setAmenitiesDetails:info.detail  Amount:info.amount];
        }
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) replyButtonPressed: (id) sender withEvent: (UIEvent *) event
{
    UITouch * touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView: self.tableView];
    NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint: location];
}

//    [sender setTitle:@"show new title" forState:UIControlStateNormal];

- (IBAction)myButtonClicked:(UIButton*)sender
{
    //[sender setTitle:@"Paid" forState:UIControlStateNormal];
    //[sender setTitle:@" " forState:UIControlStateNormal];
    
}

-(void)openInsurance {
    
    [self getEncryptedBookingID:^(NSString* bookingID){
        NSLog(@"bookingID = %@",bookingID);
        NSString *valueOFR = @"dYaTbw58zog%3D";
        //http://staging.mylescars.com/bookings/LDW_certificate?book_id=fE0v6Mmt%2B8E%3D&type=dYaTbw58zog%3D
        
#ifdef DEBUG
       // NSString *strUrl = [NSString stringWithFormat:@"http://staging.mylescars.com/bookings/myles_secure_certificate?book_id=%@&type=%@",bookingID,valueOFR];
        NSString *strUrl = [NSString stringWithFormat:@"http://qa2.mylescars.com/bookings/LDW_certificate?book_id=%@&type=%@",bookingID,valueOFR]; // OLD URL
        
       // NSString *strUrl = [NSString stringWithFormat:@"http://qa2.mylescars.com/bookings/myles_secure_certificate?book_id=%@&type=%@",bookingID,valueOFR]; // new url
        
#else
//        NSString *strUrl = [NSString stringWithFormat:@"https://www.mylescars.com/Bookings/LDW_certificate?book_id=%@&type=%@",bookingID,valueOFR]; // old url
        
        NSString *strUrl = [NSString stringWithFormat:@"https://www.mylescars.com/Bookings/myles_secure_certificate?book_id=%@&type=%@",bookingID,valueOFR]; // new url

#endif
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            FlashOfferViewController *offerController = [mainStoryboard instantiateViewControllerWithIdentifier:@"OfferViewController"];
            offerController.titleStr = @"LDW Certificate";
            offerController.urlToNavigate = [NSURL URLWithString:strUrl];
            [self presentViewController:offerController animated:YES completion: nil];
        });
        
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://staging.mylescars.com/bookings/view_policy?app=ia"]];
    }];
}

- (void)getEncryptedBookingID:(void (^)(NSString *))completion {
    NSMutableDictionary *body = [NSMutableDictionary new];
    [body setObject:rideDetail.Bookingid forKey:@"Value"];
    [body setObject:[NSNumber numberWithInteger:1] forKey:@"PassingType"];


    NSURLSession *session = [CommonFunctions defaultSession];
    [self showLoader];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:@"EncryptDecrypt" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                           {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD dismiss];
                                               });
                                               NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                               NSString *statusStr = [headers objectForKey:@"Status"];
                                               if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                               {
                                                   NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                   //NSLog(@"Respose Json : %@",jsonreponse);
                                                   if (!error)
                                                   {
                                                       NSLog(@"jsonResponse = %@",jsonreponse);
                                                       completion([jsonreponse objectForKey:@"message"]);
                                                   }
                                               }

    }];
    [globalOperation addOperation:registrationOperation];


}

 /**
    Goto payment page(preauth)
     @param Pass booking id.
     */
-(IBAction)myPaymentAction:(id)sender {
    //dsds
    if ([CommonFunctions reachabiltyCheck])
    {
        
        NSLog(@"rideDetail %@",rideDetail.Bookingid);
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
        Juspay *jusPayVc = [storyboard instantiateViewControllerWithIdentifier:@"Juspay"];
        jusPayVc.bRentalorPreAurth = true;
        jusPayVc.strBookingId = rideDetail.Bookingid;
        jusPayVc.delegate = self;
        [self.navigationController pushViewController:jusPayVc animated:true];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}

/*
 search preauth amount for the newly created booking returning for preauth page for nil value
 
 */


- (void) animateView : (CGPoint)point
{
    UIImageView *image = (UIImageView *)firstcell.cell1ThumbIcon;
    UILabel *label = (UILabel *)firstcell.cell1label;
    [UIView animateWithDuration:0.1 animations:^{
        image.frame = CGRectMake(CGRectGetMinX(image.frame) + point.x, CGRectGetMinY(image.frame) + point.y, CGRectGetWidth(image.frame) + point.x, CGRectGetHeight(image.frame) + point.x);
        label.frame = CGRectMake(CGRectGetMinX(label.frame) + point.x, CGRectGetMinY(label.frame) + point.y, CGRectGetWidth(label.frame), CGRectGetHeight(label.frame));
        
    } completion:^(BOOL finished) {
        
    }];
    
}

#pragma mark- UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((firstcell != nil) && (self.navigationController) && (scrollView.tag == 1000))
    {
        NSIndexPath *index = [self.tableView indexPathForCell:firstcell];
        CGFloat firtindexheight = 5 ;
        CGFloat yaxis = scrollView.contentOffset.y;
        
        if ((index == nil) || (yaxis > firtindexheight))
        {
            
            [self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:false];
        }
        else
        {
            [self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:false];
            self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        }
    }
}


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSIndexPath *index = [self.tableView indexPathForCell:firstcell];
    CGFloat firtindexheight = 5 ;
    CGFloat yaxis = scrollView.contentOffset.y;
    if ((index == nil) || (yaxis > firtindexheight))
    {
        [self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:false];
    }
    else
    {
        [self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:false];
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    }
}

#pragma mark- Cancel Booking
- (IBAction)CancelBtnAction:(UIButton *)sender
{
    CancellationPolicyVC *bookingConfirm = [self.storyboard instantiateViewControllerWithIdentifier:@"CancellationPolicyVC"];
    bookingConfirm.strBookingID = [self.ridePaymentData valueForKey:@"Bookingid"];
    [self.navigationController pushViewController:bookingConfirm animated:true];

   // [self alertcontroller];
}
- (void)alertcontroller
{
    if ([CommonFunctions getMajorSystemVersion] > 7)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"CANCEL BOOKING" message:kCancelBookingConfirmMsg preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *Okaction = [UIAlertAction actionWithTitle:kYes style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self bookingCancleApi];
                                   }];
        [alert addAction:Okaction];
        UIAlertAction *Cancelaction = [UIAlertAction actionWithTitle:kNo style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           
                                       }];
        [alert addAction:Cancelaction];
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KMessage message:kCancelBookingConfirmMsg delegate:self cancelButtonTitle:kNo otherButtonTitles:kYes, nil];
        [alert show];
    }
}

#pragma mark - Booking Cancel Api
- (void)bookingCancleApi
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSString *userid = [ DEFAULTS valueForKey:KUSER_ID];
        [self showLoader];
        [CommonApiClass cancleBooking:rideDetail.Bookingid User:userid CompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            NSString *statusStr = [headers objectForKey:@"Status"];
            if (statusStr != nil && [statusStr isEqualToString:@"1"])
            {
                if (!error)
                {
                    NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *status = [responseJson valueForKey:@"status"];
                        
                        
                        if ([status integerValue] == 1)
                        {
                            [CommonFunctions alertTitle:KMessage withMessage:kCancleRideSuccess];
                            NSIndexPath *indexpath = [NSIndexPath indexPathForItem:0 inSection:0];
                            rideDetail.Tripstatus = @"Cancel";
                            [self setLenghtAccordingToRide];
                            [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
                            rideStatusstr = @"";
                            title = k_BookingTitleCancel;
                            [self customizeNavigationBarwithImage:title LeftImage:k_backBtnImg RightImage:rideStatusstr Caller:k_phonecallRed Controller:self Present:false];
                            self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
                            [self postActiveNotifications:@"1"];
                        }
                        else
                        {
                            [CommonFunctions alertTitle:KSorry withMessage : kCancleRidefail];
                        }
                        [self.tableView reloadData];
                        
                    });
                }

            }
            else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                NSLog(@"AccessToken Invalid");
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired, Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [CommonFunctions logoutcommonFunction];
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                        UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                        HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
                        navController.viewControllers = @[homeController];
                        [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:0];
                        [(MenuViewController *)self.revealViewController.rearViewController setIndexValue:0];
                        [[(MenuViewController *)self.revealViewController.rearViewController menuTable] reloadData];
                        
                        [self.revealViewController pushFrontViewController:navController animated:true];
                        [self.revealViewController revealToggleAnimated:true];
                        
                        
                    }];
                    [alertController addAction:ok];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                });
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [GMDCircleLoader hideFromView:self.view animated:YES];
//                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                        [CommonFunctions logoutcommonFunction];
//
//
//                    }];
//                    [alertController addAction:ok];
//
//                    [self presentViewController:alertController animated:YES completion:nil];
//                });
            }
            
            
            
            
        }];
        
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
    
    
}
#pragma mark- Payment

- (void)securityAmountPayAction
{
    NSString *amount = [rideDetail.PreAuthAmount substringToIndex:rideDetail.PreAuthAmount.length - 3];
    [self addAlertForPreAuthPayment:amount];
}


- (void) addAlertForPreAuthPayment : (NSString *)amount
{
    NSString *messageStr = (hoursBetweenDates > 2) ? kpreauthreminder(amount) : [NSString stringWithFormat:@"%@ Rs:%@",kSecurityAmount,amount];
    
    if ([CommonFunctions getMajorSystemVersion ] > 7)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:KMessage message:messageStr preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *Continueaction = [UIAlertAction actionWithTitle:kContinue style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                         {
                                             [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
                                             id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                             
                                             [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"PreAuth Payment"
                                                                                                   action:@"Click"
                                                                                                    label:@"PreAuthPayment Alert"
                                                                                                    value:@1] build]];
                                             
                                             [self presentViewController:controller animated:YES completion:nil];
                                             
                                         }];
        [alert addAction:Continueaction];
        //Gap houres Condition
        if (hoursBetweenDates > 2)
        {
            UIAlertAction *Cancelaction = [UIAlertAction actionWithTitle:kCancel style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                           {
                                           }];
            [alert addAction:Cancelaction];
        }
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KMessage message:messageStr delegate:self cancelButtonTitle:kCancel otherButtonTitles:kContinue, nil];
        [alert show];
        
    }
    
}




#pragma mark- PreAuth
- (void)PreAuthApi : (NSString *)bookingid Track : (NSString *)trackid  Transaction : (NSString *)transactionid Amount : (NSString *)preauthamount
{
    
    
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:bookingid forKey:k_bookingid];
        [body setObject:trackid forKey:k_trackid];
        [body setObject:transactionid forKey:k_transactionid];
        [body setObject:preauthamount forKey:k_preauthamount];
        NSURLSession *session = [CommonFunctions defaultSession];
        [self showLoader];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KpreAuthCreation completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                       //NSLog(@"Respose Json : %@",jsonreponse);
                                                       if (!error)
                                                       {
                                                           if ([timer isValid]) {
                                                               
                                                               [timer invalidate];
                                                               timer = nil;
                                                           }
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [self postActiveNotifications:@"0"];
                                                               rideDetail.PreAuthStatus = @"1";
                                                               self.securityPaymentStatus = @"Paid";
                                                               /*
                                                                NSIndexPath *indexpath = [NSIndexPath indexPathForItem:2 inSection:0];
                                                                [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                                                */
                                                               
                                                               
                                                               [CommonFunctions alertTitle:KMessage withMessage:[jsonreponse valueForKey:k_ApiMessageKey]];
                                                               //[self.tableView reloadData];
                                                               
                                                           });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:KSorry withMessage:[jsonreponse valueForKey:k_ApiMessageKey]];                                                     });
                                                           
                                                       }
                                                       
                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];
                                                               [self.revealViewController revealToggleAnimated:true];

                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });

                                                   }

                                                   
                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}
#pragma mark- UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 3)
    {
        if (buttonIndex == 1)
        {
            SWRevealViewController *revealController = self.revealViewController;
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_profileStoryBoard bundle:nil];
            UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_profileNavigation];
            [(MenuViewController *)revealController.rearViewController setSelectedIndex:2];
            [(MenuViewController *)revealController.rearViewController setSelectedRow:2];
            ProfileViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_profileController];
            navController.viewControllers = @[homeController];
            //NSLog(@"CONRTOL : %@",navController.viewControllers);
            [revealController pushFrontViewController:navController animated:YES];
        }
        
    }
    else
    {
        
        if (buttonIndex == 1)
        {
            if (![alertView.message isEqualToString:kCancelBookingConfirmMsg])
            {
                UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:KSelectPaymentMode delegate:self cancelButtonTitle:kCancel destructiveButtonTitle:nil otherButtonTitles:KCards,KNetbanking, nil];
                [sheet showInView:self.view];
            }
            else
            {
                [self bookingCancleApi];
            }
        }
        
    }
}


#pragma mark UIActionSheetDelegate
- (void)addactionSheet : (id)delegate
{
    UIViewController *topmostcontroller = [CommonFunctions returntopMostController];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:KSelectPaymentMode delegate:delegate cancelButtonTitle:kCancel destructiveButtonTitle:nil otherButtonTitles:KCards,KNetbanking, nil];
    [sheet showInView:topmostcontroller.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSArray *titleAr = @[KCards,KNetbanking,kCancel];
    NSString *index = titleAr[buttonIndex];
    
    if (buttonIndex == 2)
    {
        if (hoursBetweenDates <= 2)
        {
            [self securityAmountPayAction];
        }
        return;
    }
//    [self payNimoController:index TranactionAmount:rideDetail.PreAuthAmount];
}


-(void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    
}
#pragma mark- Set Notification and call PayNimo
//- (void)payNimoController : (NSString *)tit TranactionAmount : (NSString *)totalamount
//{
//    CommonApiClass *commonapi = [[CommonApiClass alloc]init];
//    commonapi.delegate = self;
//    
//    //paynimo test amount heardcoded
//    [commonapi payNimoController:tit TranactionAmount:totalamount Date:[self formatDate:[NSDate date]]withType:YES andEndDate:[self formatDate:[[NSDate date] dateByAddingTimeInterval:hoursBetweenDates*3600]] withGap:hoursBetweenDates/24 andTrackId:rideDetail.trackID];
//}

- (void)postActiveNotifications:(NSString *)type
{
    [[NSNotificationCenter defaultCenter] postNotificationName:KACTIVE_BOOKING_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObject:type forKey:@"type"]];
}


#pragma mark- CommonApiClassDelegate
- (void)PresentPaymentScreen:(id)paymentVC
{
    //    [self presentViewController:(PMPaymentOptionView *)paymentVC animated:YES completion:nil];
}
bool bPAid;
- (void)PaymentSuccessDelagateWithData:(NSMutableDictionary *)dict
{
    //NSLog(@"DICT  : %@",dict);
    NSIndexPath *indexpath = [NSIndexPath indexPathForItem:2 inSection:0];
    //bPAid = true;
    
    // self.securityPaymentStatus = @"Paid";
    //[self.tableView reloadData];
    /*
     NSIndexPath *indexpath = [NSIndexPath indexPathForItem:2 inSection:0];
     [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
     */
    [[NSNotificationCenter defaultCenter] postNotificationName:@"preauthPaymentComplete" object:nil];
    [self PreAuthApi:rideDetail.Bookingid Track:[dict objectForKey:@"trackid"] Transaction:[dict objectForKey:@"transactionid"] Amount:rideDetail.PreAuthAmount];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    
}

- (void) refreshViewData:(NSNotification *)note {
    //NSLog(@"Received Notification - Someone seems to have logged in");
    self.securityPaymentStatus = @"Paid";
    rideDetail.PreAuthStatus = @"1";
    
    //    NSIndexPath *indexpath = [NSIndexPath indexPathForItem:2 inSection:0];
    //    [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self.tableView reloadData];
}


//- (void)PaymentFailureDelagate
//{
//    if (hoursBetweenDates <= 2)
//    {
//        [self presentViewController:controller animated:YES completion:nil];
//        //[self securityAmountPayAction];
//    }
//    else
//    [CommonFunctions alertTitle:kfailed withMessage:kpreauthFail];
//}


- (void)PaymentCancelDelagte
{
    if (hoursBetweenDates <= 2)
    {
        if (controller != nil)
        {
            [self presentViewController:controller animated:YES completion:nil];
        }
        
    }
    else
        [CommonFunctions alertTitle:kCancelled withMessage:kpaymentcanceledmsg];
}

-(void) ecommerceTracking:(NSString *)paymentTypeKeyword TranactionAmount : (NSString *)TrackingAmount TrackId:(NSString *)trackId BookingId:(NSString *)BookingId CarModelName:(NSString *)CarModelName
{
    NSString *CouponCode;
    float amountValue = [TrackingAmount floatValue];
    
    
    NSDecimalNumber *myNumber = [NSDecimalNumber decimalNumberWithString:TrackingAmount];
    //NSLog(@"My Number : %@",myNumber);
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    
    GAIEcommerceProduct *product = [[GAIEcommerceProduct alloc] init];
    [product setId:trackId];
    [product setName:CarModelName];
    [product setPrice:myNumber];
    
    GAIEcommerceProductAction *productAction = [[GAIEcommerceProductAction alloc] init];
    [productAction setAction:kGAIPAPurchase];
    [productAction setTransactionId:BookingId];
    [productAction setRevenue:myNumber];
    [productAction setCouponCode:CouponCode];
    
    
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:paymentTypeKeyword
                                                                           action:@"Purchase"
                                                                            label:nil
                                                                            value:nil];
    // Add the transaction data to the event.
    [builder setProductAction:productAction];
    [builder addProduct:product];
    
    [tracker set:kGAIScreenName value:@"Ride Detail Screen"];
    [tracker set:kGAICurrencyCode value:@"INR"];
    
    // Send the transaction data with the event.
    [tracker send:[builder build]];
}

#pragma mark jusPay delegate
- (void)securityPaid:(BOOL)isPaid;
{
    [self postActiveNotifications:@"0"];
    rideDetail.PreAuthStatus = @"1";
    self.securityPaymentStatus = @"Paid";
}
-(void)paymentFailure
{
    [CommonFunctions alertTitle:nil withMessage:kpaymentFailuremsg];
}
-(void)documentUpload
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KMessage message:kUploadDocument delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Now", nil];
    alert.tag = 3;
    [alert show];

}

@end
