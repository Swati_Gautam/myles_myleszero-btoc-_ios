//
//  RideHistoryVC.m
//  Myles
//
//  Created by Myles   on 13/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "RideHistoryVC.h"
#import "RideHistoryCustomCell.h"
#import "Myles-Swift.h"
#import "RideHistoryResponse.h"
#import "FeedbackVC.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
@import FirebaseAnalytics;
@interface RideHistoryVC ()
@property (nonatomic)  UIBarButtonItem *revealButtonItem;

@end

@implementation RideHistoryVC
{
    RideHistoryResponse *rideHistory;
    RideHistoryCustomCell *selectedCell;
    NSInteger selectedIndex;
}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"My Rides"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    [FIRAnalytics logEventWithName:kFIREventViewItemList parameters:nil];

    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    [self customizeLeftRightNavigationBar:@"My Rides" RightTItle:nil Controller:self Present:true];
    
    [self.leftbackButton setImage:[UIImage imageNamed:k_backBtnImg] forState:UIControlStateNormal];
    [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    //self.leftmenuButton.hidden = true;
    //self.leftbackButton.hidden = false;
   
}

- (void) viewWillAppear:(BOOL)animated
{
    if (![DEFAULTS boolForKey:IS_LOGIN]) {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
        LoginProcess *editController = (LoginProcess *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginProcess"];
        
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:editController];
        //editController.delegate = self;
        editController.strOtherDestination = @"0";
        navigationController.viewControllers = @[editController];
        [self presentViewController:navigationController animated:YES completion:nil];
        
    } else{
        [super viewWillAppear:true];
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"My Rides"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        self.navigationController.navigationBarHidden = false;
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        
        [self getRideHistoryDetails];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)getRideHistoryDetails
{
    if ([CommonFunctions reachabiltyCheck])
        {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLoader];
        });
        
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:[DEFAULTS valueForKey:KUSER_ID]forKey:@"userid"];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KbookingHistory completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               NSString *status = [responseJson valueForKey:@"status"];
                                                               
                                                               if ([status integerValue] == 1)
                                                               {
                                                                   
                                                                   
                                                                   rideHistory = [[RideHistoryResponse alloc]initWithDictionary:responseJson error:nil];
                                                                   
                                                                   if (rideHistory.response.count == 0)
                                                                   {
                                                                       [self.tableView setHidden:YES];
                                                                   }
                                                                   else
                                                                   {
                                                                       [self.tableView setHidden:NO];
                                                                       [self.tableView reloadData];
                                                                       
                                                                   }
                                                               }
                                                               else
                                                               {
                                                                   [self.tableView setHidden:YES];
                                                                   NSLog(@"[status integerValue] = %@",status);
                                                                   [CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                                   
                                                                   UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                                                   
                                                                   SWRevealViewController *revealController = [storyBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                                                   
                                                                   UINavigationController *homeNavigationController = [storyBoard instantiateViewControllerWithIdentifier:@"HomeNavigation"];
                                                                   
                                                                   
                                                                   [revealController setFrontViewController:homeNavigationController];
                                                                   MenuViewController *menuController =  [storyBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                                                                   [(MenuViewController *)revealController.rearViewController setSelectedIndex:0];
                                                                   [revealController setRearViewController:menuController];
                                                                   
                                                                   //revealController.delegate = self;
                                                                   
                                                                   [[[AppDelegate getSharedInstance] window] setRootViewController:revealController];
                                                                   
                                                               }
                                                           });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                           });
                                                           
                                                       }
                                                       

                                                   }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                       NSLog(@"AccessToken Invalid");
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired, Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                               [CommonFunctions logoutcommonFunction];

                                                               UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                                               UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                                                               HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
                                                               navController.viewControllers = @[homeController];
                                                               [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:0];
                                                               [(MenuViewController *)self.revealViewController.rearViewController setIndexValue:0];
                                                               [[(MenuViewController *)self.revealViewController.rearViewController menuTable] reloadData];
                                                               
                                                               [self.revealViewController pushFrontViewController:navController animated:true];
                                                               [self.revealViewController revealToggleAnimated:true];
                                                               
                                                           }];
                                                           [alertController addAction:ok];
                                                           
                                                           [self presentViewController:alertController animated:YES completion:nil];
                                                       });


                                                   } else
                                                   {
                                                      
                                                   }

                                               }];
        [globalOperation addOperation:registrationOperation];
        
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
    
}
#pragma mark- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    /*
    if(alertView.tag==1) {
        HomeViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        [self.navigationController pushViewController:homeViewController animated:true];
        
    }
    else {
     */
    if (buttonIndex == 0)
    {
        [self cancleBooking:selectedCell.bookingId User:[DEFAULTS valueForKey:KUSER_ID]SelectedIndex:selectedIndex];
    }
    else
    {
        NSIndexPath *index = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
  //  }
}
- (void)alertcontroller
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:KMessage message:kCancelBookingConfirmMsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Okaction = [UIAlertAction actionWithTitle:kYes style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   [self cancleBooking:selectedCell.bookingId User:[DEFAULTS valueForKey:KUSER_ID]SelectedIndex:selectedIndex];
                               }];
    [alert addAction:Okaction];
    UIAlertAction *Cancelaction = [UIAlertAction actionWithTitle:kNo style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       NSIndexPath *index = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
                                       [self.tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
                                   }];
    [alert addAction:Cancelaction];
    [self presentViewController:alert animated:true completion:nil];
}
#pragma mark - UITableViewDataSource

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"       " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        RideHistoryCustomCell *cell = (RideHistoryCustomCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                                        
                                        // Delete something here
                                        if (cell.rideStatus)
                                        {
                                            selectedCell = cell;
                                            selectedIndex = indexPath.row;
                                            if ([CommonFunctions getMajorSystemVersion] > 7.0)
                                            {
                                                [self alertcontroller];
                                            }
                                            else
                                            {
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KMessage message:kCancelBookingConfirmMsg delegate:self cancelButtonTitle:kNo otherButtonTitles:kYes, nil                                            ];
                                                [alert show];
                                            }
                                            
                                        }
                                        
                                    }];
    delete.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:k_deleteIcon]];
    return @[delete];
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return NO;
    
    // Return YES if you want the specified item to be editable.
    RideHistoryCustomCell *cell = (RideHistoryCustomCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    // Delete something here
    if (cell.isEditing) {
        [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
    else
    {
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
    if (cell.rideStatus)
    {
        
        return YES;
    }
    else
    {
        return NO;
    }
    
    
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
        
    {
        
        RideHistoryCustomCell *cell = (RideHistoryCustomCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        
        // Delete something here
        if (!cell.rideStatus)
        {
            [self cancleBooking:cell.bookingId User:[DEFAULTS valueForKey:KUSER_ID] SelectedIndex:indexPath.row];
        }       // [arry removeObjectAtIndex:indexPath.row];
        
    }
    
}
// Override to support editing the table view.
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kCancel;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return rideHistory.response.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = k_rideHistoryCellIdentifier;
    RideHistoryCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if ( cell == nil )
    {
        cell = [[RideHistoryCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [cell getCellInfo:rideHistory.response[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [FIRAnalytics logEventWithName:kFIREventViewItem parameters:nil];

    BookingConfirmationVC *bookingConfirm = [self.storyboard instantiateViewControllerWithIdentifier:@"bookingConfirm"];
    //NSDictionary *dict = rideHistory.response[indexPath.row];
    //NSLog(@"dict : %@",dict);
    
    RideHistoryDetails *obj = rideHistory.response[indexPath.row];
    if ([obj.PreAuthStatus isEqualToString:@"2"]) {
        obj.InsurancePaid = TRUE;
        //[dict setObject:[NSNumber numberWithBool:TRUE] forKey:@"InsurancePaid"];
    }
    [bookingConfirm setRideInfomation:(NSDictionary *)obj]; //rideHistory.response[indexPath.row]];
    bookingConfirm.ridePaymentData = (NSDictionary *)obj;
    bookingConfirm.showPaymentAlert = YES;
    [self.navigationController pushViewController:bookingConfirm animated:true];
}
- (void)postActiveNotifications:(NSString *)type
{
    [[NSNotificationCenter defaultCenter] postNotificationName:KACTIVE_BOOKING_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObject:type forKey:@"type"]];
}

#pragma mark- Cancle Booking
- (void)cancleBooking : (NSString *)bookingId User : (NSString *)userid SelectedIndex : (NSInteger) index
{
    
    if ([CommonFunctions reachabiltyCheck])
    {
        [self showLoader];
        [CommonApiClass cancleBooking:bookingId User:userid CompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            NSString *statusStr = [headers objectForKey:@"Status"];
            if (statusStr != nil && [statusStr isEqualToString:@"1"])
            {
                if (!error)
                {
                    NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *status = [responseJson valueForKey:@"status"];
                        
                        if ([status integerValue] == 1)
                        {
                            [CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                            NSIndexPath *indexpath = [NSIndexPath indexPathForItem:index inSection:0];
                            RideHistoryDetails *history = rideHistory.response[index];
                            history.Tripstatus = @"Cancel";
                            [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
                            [self postActiveNotifications:@"1"];
                        }
                        else
                        {
                            [CommonFunctions alertTitle:KSorry withMessage : [responseJson valueForKey:k_ApiMessageKey]];
                            [self.tableView reloadData];
                        }
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                    });
                    
                }
            }
            else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                NSLog(@"AccessToken Invalid");
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired, Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [CommonFunctions logoutcommonFunction];
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                        UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:k_homeNavigation];
                        HomeViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_HomeViewController];
                        navController.viewControllers = @[homeController];
                        [(MenuViewController *)self.revealViewController.rearViewController setSelectedIndex:0];
                        [(MenuViewController *)self.revealViewController.rearViewController setIndexValue:0];
                        [[(MenuViewController *)self.revealViewController.rearViewController menuTable] reloadData];
                        
                        [self.revealViewController pushFrontViewController:navController animated:true];
                        [self.revealViewController revealToggleAnimated:true];

                        
                    }];
                    [alertController addAction:ok];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                });
//                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//                [appDelegate getAccessTokenWithCompletion:^{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [GMDCircleLoader hideFromView:self.view animated:YES];
//                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
//
//                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                            [self.navigationController popToRootViewControllerAnimated:NO];
//                        }];
//                        [alertController addAction:ok];
//                        
//                        [self presentViewController:alertController animated:YES completion:nil];
//                    });
//                }];
                
            }

            
            
        }];
        
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
