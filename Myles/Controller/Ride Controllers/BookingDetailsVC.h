//
//  BookingDetailsVC.h
//  Myles
//

#import <UIKit/UIKit.h>
#import "EMAccordionTableViewController.h"
#import "FRHyperLabel.h"
#import "NSUserDefaults+Utility.h"

//#import <Paynimolib/PMGlobal.h>
@class CarPackageDetails;
@class LocationData;

@interface BookingDetailsVC : UIViewController <EMAccordionTableDelegate , UITableViewDelegate , UITableViewDataSource,LocationDelegate, UITextFieldDelegate,CommonApiClassDelegate>
{
    __weak IBOutlet UIButton *editButton;
    __weak IBOutlet NSLayoutConstraint *contentViewHieghtConstraint;
    IBOutlet UITableView *tableView;//, *amenitiesDetailtable;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *contentView;
    IBOutlet UIView *couponSuperView;
    IBOutlet UITextField *coupontxtFd;
    
    
    __weak IBOutlet NSLayoutConstraint *tableViewHeight;
    NSInteger kHoursBetweenDates;
    
    NSDictionary *kJsonDict;
    
    BOOL trackTermsBtnClicked;
    NSString *paymentMethodTrack;
}
@property (weak, nonatomic) IBOutlet UILabel *lblVariantName;
@property (nonatomic , assign) BOOL speedGovernerIsActive;
@property (strong, nonatomic) NSMutableArray *bookingGSTPackageDetails;

@property (nonatomic , strong) NSString *sublocationNameInDoorStep;
@property (nonatomic, strong) LocationData *locationDataObj;
@property (assign, nonatomic) BOOL doorStepDelieverySelected;
@property (nonatomic, strong) NSString *TotalDurationForDisplay;
@property (weak, nonatomic) IBOutlet UIButton *applyCouponBtn;
@property (nonatomic , strong) NSLayoutConstraint *heightConstraint;
@property (nonatomic , strong) NSDictionary *kBookingDurationMD;
@property (nonatomic , strong) NSString *totalFare , *amenitiesTotalAmount,*subLocationID;
@property (nonatomic,assign) BOOL trackTermsBtnClicked;
@property (nonatomic,assign) NSString *paymentMethodTrack;
@property (nonatomic,copy) CarPackageDetails *selectedPackage;
@property (nonatomic, weak) IBOutlet FRHyperLabel *label;

@property (nonatomic , strong) NSString *previousDateTime;
@property (assign) NSTimeInterval previousMilliSec;


@property (weak, nonatomic) IBOutlet UILabel *prcLbl;

@property (weak, nonatomic) IBOutlet UILabel *oldPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *nonRefundablePriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *refundablePriceLbl;
@property (weak, nonatomic) IBOutlet UIButton *checkboxButton;













- (void) setCarDetails : (NSDictionary *)dic AndAddress : (NSString *)address City : (NSString *)cityId CarImg : (NSString *)carImg pickupType : (NSString *)pickup TripDuration : (NSString *)duration packageType : (NSString*) packaget;
-(void)setAmenitiesDetailsSectionContent : (NSString *)cityId CarCatId : (NSString *)carCatid;

-(void)bookCarBtnTouched:(UIButton *)sender;

-(void) ecommerceTracking:(NSString *)paymentTypeKeyword TranactionAmount : (NSString *)TrackingAmount TrackId:(NSString *)trackId BookingId:(NSString *)BookingId;
- (void)applyCouponCode  : (NSString *)CarModelId CITYID : (NSString *)cityid  DISCOUNT : (NSString *)discountCode PICKUPDATE : (NSString *)pickupdate TOTALFAIR : (NSString *)totalFair PACKAGEID : (NSString *)pkgid PACKAGEDURATION : (NSString *)pkgDuration USERID : (NSString *)userid DROPOFFDATE:(NSString *)DropOffDate SUBLOCATIONID:(NSString *)SubLocationId WEEKDAY:(NSString *)WeekDay WEEKEND:(NSString *)weekEnd;
- (void) offerButtonClick : (UIButton *)sender;
- (IBAction)checkboxButtonAction:(id)sender;
- (IBAction)viewBenefitAction:(id)sender;

@end
