//
//  PickupAddressVC.m
//  Myles
//
//  Created by Myles on 28/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import "PickupAddressVC.h"
#import "AddressCustomCell.h"
#import <Google/Analytics.h>
#import "PickUpAddress.h"

#define ACCEPTABLE_CHARECTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\,/-"


@interface PickupAddressVC ()
{
    NSArray *placeholderAr;
    NSMutableDictionary *dictLatLng;
    
}

@end

@implementation PickupAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _btnSearchCurrentLoc.layer.cornerRadius =_btnSearchOnMap.layer.cornerRadius = 5;
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Pick Up / Drop Off Address"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    
    //Rounded rect btn
    //saveBtn
    self.saveBtn.layer.masksToBounds = YES;
    self.saveBtn.layer.cornerRadius = 3.0;

    
    placeholderAr = @[@"City*",@"locality*",@"House/Flat Number*",@"Street*",@"Landmark"];
    
    //[self customizeLeftRightNavigationBar:@"Pick Up / Drop Off Address" RightTItle:k_phonecall Controller:self Present:false];
    
     [self customizeLeftRightNavigationBar:@"Pick Up / Drop Off Address" RightTItle:k_phonecallRed Controller:self Present:false];
    
    self.leftbackButton.hidden = false;
    self.leftmenuButton.hidden = true;
    [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    
//    self.addressDataMA = [DEFAULTS objectForKey:k_PickupAddress];
    if (self.addressDataMA == nil)
    {
        self.addressDataMA = [[NSMutableArray alloc] init];
        for (int i = 0; i  < placeholderAr.count; i++)
        {
            
            [self.addressDataMA addObject:@""];
        }
        
    }
    
    [DEFAULTS setBool:NO forKey:k_PickupEnabled];
    [self.addressTable reloadData];
}
- (void)leftBtnTouchedPop
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark- Right Button
- (void)rightButton : (UIButton *)sender
{
    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITableView Delegates & Datasources-

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeholderAr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = k_pickUpAddressCell;
    AddressCustomCell *customCell;
    
    customCell = (AddressCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    // If there is no cell to reuse, create a new one
    if(customCell == nil)
    {
        customCell = [[AddressCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if ([customCell.addressTF respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor darkGrayColor];
        customCell.addressTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customCell.addressTF.text attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    [customCell.addressTF setDelegate:self];
    customCell.addressTF.tag = indexPath.row;
    [CommonFunctions setLeftPaddingToTextField:customCell.addressTF andPadding:10];
    
    if (indexPath.row == placeholderAr.count-1)
    {
        UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
        toolbar.barStyle = UIBarStyleDefault;
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
        [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
        customCell.addressTF.keyboardType = UIKeyboardTypeNumberPad;
        customCell.addressTF.inputAccessoryView = toolbar;
    }
    
    customCell.addressTF.text = self.addressDataMA[indexPath.row];
    customCell.addressTF.placeholder = placeholderAr[indexPath.row];
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return customCell;
}

-(void)dismiss
{
    [self.addressTable setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [self.addressTable endEditing:YES];
    
}
#pragma mark - UITextField Delegates & Datasources-

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField.tag == 5)
    {
        textField.keyboardType  = UIKeyboardTypeNumberPad;
    }
    else
        textField.keyboardType  = UIKeyboardTypeDefault;
    
    if(textField.tag >= 4)
    {
        if (IS_IPHONE_5)
        {
            [self.addressTable setContentOffset:CGPointMake(0, 100) animated:YES];
            
        }
    }
    if(textField.tag >= 3)
    {
        if(IS_IPHONE_4_OR_LESS)
        {
            [self.addressTable setContentOffset:CGPointMake(0, 152.3) animated:YES];
            
        }
    }
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField.tag == 5)
    {
        //New code done on 6 oct
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if ([currentString length] > 6) {
            return NO;
        }
        
        NSMutableArray *tempMA = [self.addressDataMA mutableCopy];
        [tempMA replaceObjectAtIndex:textField.tag withObject:textField.text];
        self.addressDataMA = tempMA;

        return YES;
        //Earlier code
//        if (textField.text.length > 5)
//        {
//            return NO;
//        }
    }
    
    else{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
    }
    return YES;
}


/*
 
 - (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
 {
 NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
 int length = [currentString length];
 if (length > 4) {
 return NO;
 }
 return YES;
 }
 */



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSMutableArray *tempMA = [self.addressDataMA mutableCopy];
    [tempMA replaceObjectAtIndex:textField.tag withObject:textField.text];
    self.addressDataMA = tempMA;
    [self.addressTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:textField.tag inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

/*
if (range.location == 0 && [string isEqualToString:@" "]) {
    return NO;
}
 */


- (IBAction)saveBtnTouched:(UIButton *)sender
{
    BOOL valid = true;
    for (int i = 0 ; i < placeholderAr.count ; i++)
    {
        NSString *str = self.addressDataMA[i];
        if ([str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [str isEqualToString:@" "])
        // if ([str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
        {
            if ((i != 1) && (i != 2))
            {
                [CommonFunctions alertTitle:KSorry withMessage:[NSString stringWithFormat:@"Please enter your %@",[[placeholderAr[i] componentsSeparatedByString:@" "] firstObject]]];
                valid = false;
                break;
            }
        }
    }
    if (valid)
    {
        //NSLog( @"phone number %@",[DEFAULTS objectForKey:KUSER_PHONENUMBER]);
        NSDictionary *dictBody = @{
                                   @"PhoneNumber" : @"8750872990",
                                   @"Latitude" : dictLatLng[@"lat"] != nil ? dictLatLng[@"lat"] : @"",
                                   @"Longitude" : dictLatLng[@"lng"] != nil ? dictLatLng[@"lng"] : @"",
                                   @"City" : _addressDataMA[3] == nil ? @"":_addressDataMA[3],
                                   @"Locality" : _addressDataMA[0] == nil ? @"":_addressDataMA[0],
                                   @"Housenumber" : @"",
                                   @"Street" : @"",
                                   @"Landmark" : _addressDataMA[2] == nil ? @"":_addressDataMA[2],
                                   @"AddressId" : @"1"
                                   };
        
    
        [CommonFunctions SavePickUpAddress:dictBody PassUrl:insertAddress :^(NSDictionary *DictSaveAddress) {
            
        }];
        
        [self.navigationController popViewControllerAnimated:TRUE];
        [DEFAULTS setObject:self.addressDataMA forKey:k_PickupAddress];
        [DEFAULTS setBool:YES forKey:k_PickupEnabled];
        [DEFAULTS synchronize];
        [_addressDataMA removeAllObjects];
    }
}
-(IBAction)SearchLoctoGoogleApi:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    PickUpAddress *vcPickUpAddress = [storyboard instantiateViewControllerWithIdentifier:@"PickUpAddress"];
    vcPickUpAddress.delegate = self;
    [self.navigationController pushViewController:vcPickUpAddress animated:YES];

}
-(IBAction)btnCurrentAddress:(id)sender
{
    [CommonFunctions GetCurrentLocation:^(NSDictionary *dictData) {
        NSLog(@"Current Location %@",dictData);
//        dsd
        if (dictLatLng != nil) {
            dictLatLng = [[NSMutableDictionary alloc]init];

        }
        else
            [dictLatLng removeAllObjects];
        
        [dictLatLng addEntriesFromDictionary:dictData[@"latlang"]];
        [_addressDataMA removeAllObjects];
        
        [_addressDataMA addObject: dictData[@"Name"] != nil ? dictData[@"Name"]:@""];
        [_addressDataMA addObject: dictData[@"Street"] != nil ? dictData[@"Street"]:@""];
        [_addressDataMA addObject: dictData[@"Thoroughfare"] != nil ? dictData[@"Thoroughfare"]:@"" ];
        [_addressDataMA addObject: dictData[@"City"] != nil ? dictData[@"City"]:@""];
        [_addressDataMA addObject:  dictData[@"State"] != nil ? dictData[@"State"]:@""];
        [_addressDataMA addObject: dictData[@"ZIP"] != nil ? dictData[@"ZIP"]:@""];

        [_addressTable reloadData];
        
//        {
//            City = "New Delhi";
//            Country = India;
//            CountryCode = IN;
//            FormattedAddressLines =     (
//                                         "Deshbandhu Gupta Road 782",
//                                         "Karol Bagh",
//                                         "New Delhi",
//                                         "Delhi 110005",
//                                         India
//                                         );
//            Name = "Deshbandhu Gupta Road 782";
//            State = Delhi;
//            Street = "Deshbandhu Gupta Road 782";
//            SubAdministrativeArea = Delhi;
//            SubLocality = "Karol Bagh";
//            SubThoroughfare = 782;
//            Thoroughfare = "Deshbandhu Gupta Road";
//            ZIP = 110005;
//        }
        
    }];
}
-(void)setAddressThroughApi :(NSDictionary*)dictData
{
    dictLatLng = [[NSMutableDictionary alloc]init];
    [dictLatLng addEntriesFromDictionary:dictData[@"latlang"]];
    
    [_addressDataMA removeAllObjects];
    
    [_addressDataMA addObject: dictData[@"Name"] != nil ? dictData[@"Name"]:@""];
    [_addressDataMA addObject:@""];
    [_addressDataMA addObject: dictData[@"Thoroughfare"] != nil ? dictData[@"Thoroughfare"]:@"" ];
    [_addressDataMA addObject: dictData[@"City"] != nil ? dictData[@"City"]:@""];
    [_addressDataMA addObject:  dictData[@"State"] != nil ? dictData[@"State"]:@""];
    [_addressDataMA addObject: dictData[@"ZIP"] != nil ? dictData[@"ZIP"]:@""];
    
    [_addressTable reloadData];
}
@end
