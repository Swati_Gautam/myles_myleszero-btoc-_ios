//
//  AddressEditViewController.h
//  Myles
//
//  Created by IT Team on 13/03/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddressEditProtocol <NSObject>

- (void)getEditOptionSelection:(NSInteger)option;

@end

@interface AddressEditViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property(nonatomic, weak) id<AddressEditProtocol>  delegate;

@end
