//
//  LadakhbookingViewController.m
//  Myles
//
//  Created by IT Team on 12/08/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "LadakhbookingViewController.h"
#import <Google/Analytics.h>


@interface LadakhbookingViewController ()

@end

@implementation LadakhbookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.ladakhAlertContainerView.layer setCornerRadius:5.0f];
    [self setModalPresentationStyle:UIModalPresentationCurrentContext];
    self.ladakhMessageLbl.font = UIFONT_REGULAR_RobotoFONT(13.0);
    self.ladakhMessageLbl.text = @"Do you want to drive your Myles car to Ladakh?";
    
    
    [self.confirmationBtn setBackgroundColor:[UIColor clearColor]];
    [self.confirmationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.confirmationBtn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted]; // This will helps you during click time title color will be blue color
    [self.confirmationBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];

    [self.confirmationBtn setTitle:@"YES" forState:UIControlStateNormal];
    [self.confirmationBtn addTarget:self action:@selector(yesButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.cancleBtn setBackgroundColor:[UIColor clearColor]];
    [self.cancleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.cancleBtn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted]; // This will helps you during click time title color will be blue color
    [self.cancleBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];

    [self.cancleBtn setTitle:@"NO" forState:UIControlStateNormal];
    [self.cancleBtn addTarget:self action:@selector(noButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
  
}

- (void)yesButtonAction : (UIButton *)sender
{
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Ladakh View"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Ladakh View"
                                                          action:@"Click"
                                                           label:@"Ladakh"
                                                           value:@1] build]];
    

    self.confirmationBtnAction = YES;
    if ([_delegate respondsToSelector:@selector(BookNowAction:)])
    {
        [_delegate BookNowAction:self.confirmationBtnAction];
    }
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (void)noButtonAction : (UIButton *)sender
{
    
    self.confirmationBtnAction = NO;
    if ([_delegate respondsToSelector:@selector(BookNowAction:)])
    {
        [_delegate BookNowAction:self.confirmationBtnAction];
    }
    [self dismissViewControllerAnimated:YES completion:Nil];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
