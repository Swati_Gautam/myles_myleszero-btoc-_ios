//
//  PickupAddressVC.h
//  Myles
//
//  Created by Myles on 28/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickupAddressVC : BaseViewController <UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UITableView *addressTable;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) NSMutableArray *addressDataMA;
@property (strong, nonatomic) NSMutableDictionary *dictEditAddress;

@property (assign,nonatomic) BOOL       addressEditEnable;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchCurrentLoc;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchOnMap;

- (IBAction)saveBtnTouched:(UIButton *)sender;

@end
