//
//  AddressCustomCell.h
//  Myles

#import <UIKit/UIKit.h>

@interface AddressCustomCell : UITableViewCell
{
    
}

@property (nonatomic , strong) IBOutlet UITextField *addressTF;

@property (weak, nonatomic) IBOutlet UILabel *addressHeadingLabel;



@end
