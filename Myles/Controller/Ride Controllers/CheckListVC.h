//
//  CheckListVC.h
//  Myles
//
//  Created by Myles   on 28/09/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import "BaseViewController.h"

@interface CheckListVC : BaseViewController < UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *statusLb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contanerViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIView *uploadDocSuperView;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImg1;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImg2;

@property (weak, nonatomic) IBOutlet UIButton *unlockBtn;
- (IBAction)unLockButton:(UIButton *)sender;
- (void)getTabledata  : (NSString *)ID;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidthConstraint;
@end
