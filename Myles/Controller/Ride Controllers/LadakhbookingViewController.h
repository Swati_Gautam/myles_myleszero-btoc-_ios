//
//  LadakhbookingViewController.h
//  Myles
//
//  Created by IT Team on 12/08/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LadakhbookingViewController <NSObject>

@required
//- (void)dataFromController:(NSString *)confirmationStatus;
- (void)BookNowAction:(BOOL) LadakhStatus;
//- (void)testAction:(NSString*) testVAl;


@end


@interface LadakhbookingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *ladakhAlertContainerView;
@property (nonatomic,assign) BOOL confirmationBtnAction;
@property (weak, nonatomic) IBOutlet UIButton *confirmationBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;
@property (weak, nonatomic) IBOutlet UILabel *ladakhMessageLbl;


@property (nonatomic, weak) id<LadakhbookingViewController> delegate;



@end
