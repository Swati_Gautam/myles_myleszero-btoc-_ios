//
//  TrackMyReservatioinVC.h
//  VincentLimousin
//
//  Created by Shivam on 7/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RegexKitLite.h"
#import "PlaceMark.h"

@interface TrackMyReservatioinVC : UIViewController <MKMapViewDelegate,UITextFieldDelegate>
{
	UIImageView* routeView;
	NSArray* routes;
    NSMutableDictionary *placeFromDic;
    NSMutableArray *reservationArray;
    IBOutlet UITextField *reservationNumTF;
}

@property (nonatomic, retain) UIColor* lineColor;
@property (nonatomic, retain)IBOutlet  MKMapView* mapView;
@property (strong, nonatomic) IBOutlet UIButton *prevReservationButton;
@property (strong, nonatomic) IBOutlet UIButton *anotherTripButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic , strong) IBOutlet UITextField *pickLocationTF;
@property (nonatomic , strong) IBOutlet UITextField *etaTF;
@property (nonatomic , strong) IBOutlet UITextField *reservationNumberTF;

@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;

//IBAction 
- (IBAction)doneButtonPressed:(id)sender;


- (IBAction)anotherTripButtonPressed:(id)sender;
- (IBAction)previousReservationButtonPressed:(id)sender;

@end
