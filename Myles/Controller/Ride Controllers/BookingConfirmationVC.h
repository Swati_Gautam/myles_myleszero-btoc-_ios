
#import <UIKit/UIKit.h>
#import "BookingConfirmationCustomCell.h"
#import "CommonApiClass.h"

typedef enum ComingFromController {
    NormalRidesSection,
    AfterBookingConfirmation
}ComingFromController;

@interface BookingConfirmationVC : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate,UIActionSheetDelegate,CommonApiClassDelegate, UIAlertViewDelegate>
    
    

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomLayout;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) SWRevealViewController *revealController;
@property (assign, nonatomic) BOOL displayAlert;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)trackRIdeBtnAction:(UIButton *)sender;
- (void) setRideInfomation  : (NSDictionary *)dic;
@property (assign, nonatomic) BOOL showPaymentAlert;;
@property (weak, nonatomic) IBOutlet UIButton *trackmyrideBtn;
@property (weak, nonatomic) IBOutlet UIView *buttonSubview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewHeight;
//@property (nonatomic ,retain) NSDictionary *ridePaymentData;
@property (nonatomic, copy) NSDictionary *ridePaymentData;
@property (nonatomic, copy)NSMutableDictionary *paymentInfo;
@property (nonatomic, copy) NSString *securityPaymentStatus;
@property (nonatomic , assign)  ComingFromController comingFromController;

-(void) ecommerceTracking:(NSString *)paymentTypeKeyword TranactionAmount : (NSString *)TrackingAmount TrackId:(NSString *)trackId BookingId:(NSString *)BookingId CarModelName:(NSString *)CarModelName;





@end
