//
//  CheckListVC.m
//  Myles
//
//  Created by Myles   on 28/09/15.
//  Copyright © 2015  Divya Rai. All rights reserved.
//

#import "CheckListVC.h"
#import "DetailCustomCell.h"
#import "Myles-Swift.h"
#import "GetCheckListResponse.h"
#import <Google/Analytics.h>



@interface CheckListVC () <UIActionSheetDelegate>

@end
#define ktableViewCellheight    44.0
#define ktableViewheader    44.0
@implementation CheckListVC

{
    NSMutableArray *_aminitiesDatarray;
    NSMutableArray *_docDatarray;
    NSString *_bookingID;
    BOOL _confirmed;
    UIImagePickerController *_imagePicker;
    UIImage *_image1;
    UIImage *_image2;
    NSInteger _imageTouch;
}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"CheckList"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
   
    // Base Controller function
    [self customizeNavigationBar:@"CheckList" Present:false];
    
    //Navigation
    self.navigationItem.leftBarButtonItem =  nil;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = false;
    
    //Table View Setting
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    self.tableview.scrollEnabled = false;

    [self.tableview registerNib:[UINib nibWithNibName:@"DetailCustomCell" bundle:nil] forCellReuseIdentifier:@"detailCell"];
    _confirmed = false;
  
    // Add Gesture on Image to add action on image tab
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(firstImageTouchAction:)];
    self.uploadImg1.userInteractionEnabled = true;
    [self.uploadImg1 addGestureRecognizer:gesture];
  
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(SecondImageTouchAction:)];
    self.uploadImg2.userInteractionEnabled = true;
    [self.uploadImg2 addGestureRecognizer:gesture2];
    [self getChecklistinfo];
    
    // Delete Button
    UIButton *deleteBtn = [self getDeleteButton: CGRectMake(CGRectGetWidth(self.uploadImg1.frame) - 30, CGRectGetMinY(self.uploadImg1.frame), 25, 25) Tag:1 ];
    UIButton *deleteBtn2 = [self getDeleteButton:  CGRectMake(CGRectGetWidth(self.uploadImg2.frame) - 30, CGRectGetMinY(self.uploadImg2.frame), 25, 25) Tag:2];
    deleteBtn.hidden = true;
    deleteBtn2.hidden = true;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)firstImageTouchAction : (id)sender
{
    _imageTouch = 1;
    [self uploadDocumentAction:sender];
}
- (void)SecondImageTouchAction : (id)sender
{
    _imageTouch = 2;
    [self uploadDocumentAction:sender];
}
#pragma mark - Delete Image
- (void)deleteImage : (UIButton *)sender
{
    if (sender.tag == 1)
    {
        _image1 = nil;
        self.uploadImg1.image = UIImageName(k_defaultCarImg);
        self.uploadImg1.contentMode = UIViewContentModeScaleAspectFit;
        UIButton *button = (UIButton *) [self.uploadImg1 viewWithTag:1];
        button.hidden = true;
    }
    else
    {
        _image2 = nil;
        self.uploadImg2.image = UIImageName(k_defaultCarImg);
        self.uploadImg2.contentMode = UIViewContentModeScaleAspectFit;
        UIButton *button = (UIButton *) [self.uploadImg2 viewWithTag:2];
        button.hidden = true;
    }
}

- (UIButton *)getDeleteButton : (CGRect)rect Tag : (NSInteger)tag
{
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = rect;
    [deleteBtn setBackgroundImage:UIImageName(k_deleteImg) forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteImage :) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.tag = tag;
    return deleteBtn;
}
#pragma mark - GEtData
- (void)getTabledata : (NSString *)ID
{
    _bookingID = ID;
}
#pragma mark- UIImagePickerControllerDelegate


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage* image = info[UIImagePickerControllerEditedImage];
    UIImage *small = [UIImage imageWithCGImage:image.CGImage scale:0.5 orientation:image.imageOrientation];
    if (_imageTouch == 1)
    {
        _image1 = small;
        self.uploadImg1.image = small;
        self.uploadImg1.contentMode = UIViewContentModeScaleAspectFit;
        UIButton *button = (UIButton *) [self.uploadImg1 viewWithTag:1];
        button.hidden = false;
    }
    else
    {
        _image2 = small;
        self.uploadImg2.image = small;
        self.uploadImg2.contentMode = UIViewContentModeScaleAspectFit;
        UIButton *button = (UIButton *) [self.uploadImg2 viewWithTag:2];
        button.hidden = false;
    }
    [picker dismissViewControllerAnimated:true completion:nil];
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - Upload Document Button
- (void)uploadDocumentAction:(id )sender
{
    _imagePicker = [[UIImagePickerController alloc]init];
    _imagePicker.delegate = self;
    _imagePicker.allowsEditing = YES;
    [self addActionList];
}


#pragma mark- getChecklist

- (void) getChecklistinfo
{
    if ([CommonFunctions reachabiltyCheck])
    {
        [self showLoader];
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:_bookingID forKey:k_bookingid];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KgetChecklist completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD dismiss];
                                                 });
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     if (!error)
                                                     {
                                                         NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             NSInteger status = [[jsonResponse valueForKey:@"status"] integerValue];
                                                             // Temp Changes
                                                             
                                                             if (status == 1)
                                                             {
                                                                 GetCheckListResponse *responseData = [[GetCheckListResponse alloc]initWithDictionary:jsonResponse error:nil];
                                                                 
                                                                 _aminitiesDatarray = responseData.aminities;
                                                                 _docDatarray = responseData.document;
                                                                 
                                                                 self.contanerViewHeightConstraint.constant = self.bottomViewConstraint.constant + ktableViewCellheight * (_aminitiesDatarray.count + _docDatarray.count)  + 2 * ktableViewheader;
                                                                 
                                                                 [self.tableview reloadData];
                                                                 [self.view updateConstraints];
                                                                 [self.view updateConstraintsIfNeeded];
                                                                 
                                                             }
                                                             else
                                                             {
                                                                 [CommonFunctions alertTitle:@"" withMessage:KServiceUnavailable];
                                                             }
                                                         });
                                                     }
                                                     else
                                                     {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             
                                                             [CommonFunctions alertTitle:@"" withMessage:serviceTimeoutUnabletoreach];
                                                         });
                                                         
                                                     }
                                                    

                                                 } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                         
                                                         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                             [CommonFunctions logoutcommonFunction];
                                                             //[self.revealViewController revealToggleAnimated:true];
                                                             [self.navigationController popToRootViewControllerAnimated:TRUE];
                                                             
                                                         }];
                                                         [alertController addAction:ok];
                                                         
                                                         [self presentViewController:alertController animated:YES completion:nil];
                                                     });

                                                 } else
                                                 {
                                                }
                                            }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}
#pragma mark- Action Sheet
- (void)addActionList
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:kCancel destructiveButtonTitle:kCamera otherButtonTitles:kGallery, nil];
    [sheet showInView:self.view];
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex)
    {
        case 0:
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            }
            break;
        case 1:
            _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            break;
            
        default:
            return;
            break;
    }
    [self presentViewController:_imagePicker animated:YES completion:nil];
}


#pragma mark- UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ((section == 0) && (_aminitiesDatarray.count > 0))
    {
        return k_AmenitiesDetails;
    }
    else if ((section == 1) &&(_docDatarray.count > 0))
    {
        //return k_DocumentDetails;
        return @"";
    }
    else
        return @"";
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return   _aminitiesDatarray.count;
    }
    else
    {
        return  _docDatarray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = k_detailCell;
    DetailCustomCell *customCell= (DetailCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(customCell == nil)
    {
        customCell = [[DetailCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        
        [customCell.discloseBtn addTarget:self action:@selector(discloseBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (indexPath.section == 0)
    {
        [customCell setCheckAmenitiesContent:_aminitiesDatarray[indexPath.row] Selected:false];
    }
    else
        
    {
        [customCell setCheckDocumentContent:_docDatarray[indexPath.row] Selected:false];
    }
    CGRect rect = customCell.frame;
    rect.size.width = CGRectGetWidth(self.view.frame);
    customCell.frame = rect;
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailCustomCell *customCell= (DetailCustomCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if (customCell.discloseBtn.isSelected)
    {
        [ customCell.discloseBtn setSelected:false];
    }
    else
    {
        [customCell.discloseBtn setSelected:true];
    }
    
}

-(void)discloseBtnClicked:(UIButton *)btn
{
    btn.selected = !btn.selected;
}


- (NSString *)getserviceAvailable : (NSInteger )arrayCount Section : (NSInteger)section
{
    NSString *service;
    for (int i = 0; i < arrayCount; i++)
    {
        NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
        DetailCustomCell *customCell= (DetailCustomCell *)[self.tableview cellForRowAtIndexPath:index];
        if (customCell.discloseBtn.isSelected) {
            if (service.length > 0)
            {
                service = [NSString stringWithFormat:@"%@,%@",service,customCell.serviceId];
                
            }
            else
            {
                service = [NSString stringWithFormat:@"%@",customCell.serviceId];
            }
        }
    }
    return service;
    
}
#pragma mark - CheckList Api
- (void)checkListApi : (NSString *)bookingIDdetails DamageImage1 : (NSString *)damageimg1 DamageImage2 : (NSString *)damageimg2
{
    
    NSString *serviceId = [self getserviceAvailable:_aminitiesDatarray.count Section:0];
    NSString *documentId = [self getserviceAvailable:_docDatarray.count Section:1];
    if ([CommonFunctions reachabiltyCheck])
    {
        if (serviceId == nil) {
            serviceId = @"0";
        }
        if (documentId == nil) {
            documentId = @"0";
        }
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:bookingIDdetails forKey:k_bookingid];
        [body setObject: serviceId forKey:k_serviceid];
        [body setObject: documentId forKey:k_documentid];
        [body setObject: damageimg2 forKey:k_damageimg1];
        [body setObject: damageimg1 forKey:k_damageimg2];
        NSURLSession *session = [CommonFunctions defaultSession];
        [self showLoader];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KupdateCheckList completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD dismiss];
                                                 });
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     if (!error)
                                                     {
                                                         NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             NSInteger status = [[jsonResponse valueForKey:@"status"] integerValue];
                                                             // Temp Changes
                                                             
                                                             if (status == 1)
                                                             {
                                                                 _confirmed = true;
                                                                 [DEFAULTS setValue:@"1" forKey:_bookingID];
                                                                 [CommonFunctions alertTitle:KMessage withMessage:[jsonResponse valueForKey:k_ApiMessageKey]];
                                                                 [self.navigationController popViewControllerAnimated:TRUE];
                                                                 
                                                             }
                                                             else
                                                             {
                                                                 [CommonFunctions alertTitle:@"" withMessage:[jsonResponse valueForKey:k_ApiMessageKey]];
                                                             }
                                                         });
                                                     }
                                                     else
                                                     {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             
                                                             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:serviceTimeoutUnabletoreach delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                             [alert show];
                                                         });
                                                         
                                                     }
                                                     
                                                 }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired,  Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                                                         
                                                         UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                             [CommonFunctions logoutcommonFunction];
                                                             [self.navigationController popToRootViewControllerAnimated:TRUE];
                                                             //[self.revealViewController revealToggleAnimated:true];

                                                             
                                                         }];
                                                         [alertController addAction:ok];
                                                         
                                                         [self presentViewController:alertController animated:YES completion:nil];
                                                     });
                                                     
                                                 } else
                                                 {
                                                                                                      }

                                                 
                                             }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Move to Track View Controller
- (void)moveToTrackViewController
{
    /*
    UIStoryboard *storyboard = k_storyBoard(k_MyRideStoryBoard);
    TrackRide *trackVC = [storyboard instantiateViewControllerWithIdentifier:k_TrackMyRideViewController ];
    [self.parentViewController presentViewController:trackVC animated:true completion:nil];
     */
}
#pragma mark- Unlock Button
- (IBAction)unLockButton:(UIButton *)sender {
    
    
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Checklist Update"
                                                     message:@"You won't be able to change your response. Are you sure you want to continue?"
                                                    delegate:self
                                           cancelButtonTitle:@"No"
                                           otherButtonTitles: nil];
    [alert addButtonWithTitle:@"Yes"];
    [alert show];
    
    
    
   
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"You have clicked Cancel");
    }
    else if(buttonIndex == 1)
    {
        NSString *imageStr1 = @"";
        NSString *imageStr2 = @"";
        if (_image1)
        {
            NSData *pngData = UIImageJPEGRepresentation(_image1, 0.25);
            imageStr1 = [CommonFunctions encodeToBase64String:pngData];
        }
        if (_image2)
        {
            NSData *pngData = UIImageJPEGRepresentation(_image2, 0.25);
            imageStr2 = [CommonFunctions encodeToBase64String:pngData];
        }
        [self checkListApi:_bookingID DamageImage1:imageStr1 DamageImage2:imageStr2];
    }
}




@end
