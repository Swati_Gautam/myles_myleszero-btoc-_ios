
#import <UIKit/UIKit.h>


@interface BookingConfirmationCustomCell : UITableViewCell <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *directionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *directionTrailingConstraint;

//Cell 1
@property (weak, nonatomic) IBOutlet UIImageView *cell1ThumbIcon;
@property (weak, nonatomic) IBOutlet UILabel *cell1label;
//Cell 2
@property (weak, nonatomic) IBOutlet UILabel *cell2Modelname;
@property (weak, nonatomic) IBOutlet UILabel *cell2modelPrize;
@property (weak, nonatomic) IBOutlet UILabel *cell2ModelSeating;

@property (weak, nonatomic) IBOutlet UILabel *cell2BookingID;


//cell 3
@property (weak, nonatomic) IBOutlet UILabel *cellNewRentalTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *cellNewRentalValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *cellNewPreauthTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *cellNewPreauthValueLbl;
@property (weak, nonatomic) IBOutlet UIButton *preauthPaymentLink;


//Cell 3
@property (weak, nonatomic) IBOutlet UILabel *cell3FromDate;
@property (weak, nonatomic) IBOutlet UILabel *cell3FromTime;
@property (weak, nonatomic) IBOutlet UILabel *cell3ToDate;
@property (weak, nonatomic) IBOutlet UILabel *cell3ToTime;
//Cell 4
@property (strong, nonatomic) IBOutlet UIView *cellMapview;

@property (weak, nonatomic) IBOutlet UILabel *cell4AddressLabel;

@property (weak, nonatomic) IBOutlet UILabel *pickUpTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *amenitiesDescription;
@property (strong, nonatomic) MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *amenitiesAmount;

- (void)setZerothRow :( NSDictionary *)dic;

- (void)setfirstRowCarModelPrize : (NSString *)modelName Prize : (NSString *)modelPrize Seating : (NSString *)modelSeating BookingId : (NSString *)bookingid;

- (void) setSecondRowRideTime : (NSString *)fromDate FromTime : (NSString *)fromtime ToDate : (NSString *)todate ToTime : (NSString *)totime;
- (void)setAmenitiesDetails : (NSString *)description Amount : (NSString *)amount;
- (void)setThirdRowAddress : (NSMutableDictionary *)addressDict PointLocation : (CLLocationCoordinate2D )location Title : (NSString *)title;
- (void) setPaymentRowDetailInformation : (NSString *)RentalAmount PreauthAmount : (NSString *)preauthAmount PreauthStatus : (NSString *)preauthStatus;
- (void)setPaymentRow : (NSDictionary *)dic;



@end
