//
//  MealDealCustomCell.m
//  Myles
//
//  Created by Myles on 8/18/15.
//  Copyright (c) 2015 Divya Rai. All rights reserved.
//

#import "DetailCustomCell.h"
#import "AmenitiesInfo.h"
#import "AmenitiesDetails.h"
#import "DocumentModel.h"

@implementation DetailCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) setCellContent : (NSDictionary *)item
{
    self.nameLbl.text = @"";
    self.priceLbl.text = @"";
    NSString *modelPrice  = @"";
    for (NSString *string in item)
    {
        if ([string containsString:@"VatAmt"]) {
            continue;
        }
        NSLog(@"string = %@",string);
        self.nameLbl.text = [NSString stringWithFormat:@"%@", string ];
        if (item.count > 1)
        {
            NSString *value = [[item valueForKey:string] stringByReplacingOccurrencesOfString:@"" withString:@""];
            UIColor *textColor  = UICOLOR_FontColor;
            if ([self.nameLbl.text isEqualToString:@"Total Fare"])
            {
                textColor  = UICOLOR_RedFontColor;
            }
            
            NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@",value] attributes:@{ NSForegroundColorAttributeName : textColor }];
            
            NSMutableAttributedString *costLabel = [[NSMutableAttributedString alloc]initWithString:self.nameLbl.text attributes:@{ NSForegroundColorAttributeName : textColor}];
            [costLabel appendAttributedString:attributeString2];
            self.nameLbl.attributedText = costLabel;
        }
        break;
    }
    for (int i = 0 ; i < item.count; i++ )
    {
        NSArray *keyArray = [item allKeys];
        NSString *value = [item valueForKey:keyArray[i]];
        if ([value containsString:@"@"]) {
            continue;
        }
        
        NSLog(@"value = %@",value);
        modelPrice = [NSString stringWithFormat:@"%@", value ];
        NSLog(@"modelPrice = %@",modelPrice);

        // Configure the cell before it is displayed...
        UIColor *textColor  = UICOLOR_FontColor;
        
        NSAttributedString *attributeString2;
        NSMutableAttributedString *costLabel;
        
        
        
        if ([self.nameLbl.text isEqualToString:@"Total Fare"])
        {
            textColor  = UICOLOR_RedFontColor;
            costLabel = [[NSMutableAttributedString alloc]initWithString:self.nameLbl.text attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_BOLD_OPENSANS(11.1)}];
            self.nameLbl.attributedText = costLabel;
        }
        
        
      //  textColor  = UICOLOR_FontColor;
        if ([self.nameLbl.text isEqualToString:@"Sub Total"])
        {
            textColor  = [UIColor blackColor];
            NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:@" (on base fare)" attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}]; //UIFONT_BOLD_OPENSANS
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc]initWithString:keyArray[0] attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}]; //UIFONT_BOLD_OPENSANS
            [text appendAttributedString:attributeString2];
            
          //  costLabel = [[NSMutableAttributedString alloc]initWithString:self.nameLbl.text attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : [UIFont boldSystemFontOfSize:14]}];
            self.nameLbl.attributedText = text;
        }
        
        if ([self.nameLbl.text isEqualToString:k_discountonbaseFare] || [self.nameLbl.text isEqualToString:k_discountAmt])
        {
            attributeString2 = [[NSAttributedString alloc]initWithString:modelPrice attributes:@{ NSForegroundColorAttributeName : textColor }];
            costLabel= [[NSMutableAttributedString alloc]initWithString:@"- Rs. " attributes:@{ NSForegroundColorAttributeName : textColor}];
            [costLabel appendAttributedString:attributeString2];
        }
        
        else{
        attributeString2 = [[NSAttributedString alloc]initWithString:modelPrice attributes:@{ NSForegroundColorAttributeName : textColor }];
        costLabel = [[NSMutableAttributedString alloc]initWithString:@"Rs. " attributes:@{ NSForegroundColorAttributeName : textColor}];
        [costLabel appendAttributedString:attributeString2];
        }
        if ([value isEqualToString:@"OR"]) {
            
            self.priceLbl.text = @"";
            self.nameLbl.textAlignment = NSTextAlignmentCenter;
            self.nameLbl.textColor = [UIColor grayColor];
            
        } else {
            if ([self.nameLbl.text isEqualToString:@"Total Fare"]){
                attributeString2 = [[NSAttributedString alloc]initWithString:modelPrice attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_BOLD_OPENSANS(11.1)}];
                costLabel = [[NSMutableAttributedString alloc]initWithString:@"Rs. " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_BOLD_OPENSANS(11.1)}];
                [costLabel appendAttributedString:attributeString2];
            } else {
                self.nameLbl.textColor = textColor;
            }
            
            self.nameLbl.textAlignment = NSTextAlignmentLeft;

            NSLog(@"printing constant value = %f",self.namelabelleadingSpace.constant);
            self.namelabelleadingSpace.constant = -30;
            
            self.priceLbl.attributedText = costLabel;
        }
        
        if ([keyArray[i] isEqualToString:@"WithINSURANCE"]) {
            //UIColor *textColor  = UICOLOR_FontColor;
            UIColor *textColor  = [UIColor blackColor];

            NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:modelPrice attributes:@{ NSForegroundColorAttributeName : textColor, NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1) }];

            NSMutableAttributedString *costLabel = [[NSMutableAttributedString alloc]initWithString:@"Rs. " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}];

            [costLabel appendAttributedString:attributeString2];
            
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc]initWithString:@"Myles Secure (Inclusive GST)" attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}]; //UIFONT_BOLD_OPENSANS
            self.nameLbl.attributedText = text;
            self.priceLbl.attributedText = costLabel;
        }
    //    [self.discloseBtn setSelected:false];
    }
    [self.discloseBtn setHidden:YES];
}

- (void)setnumberofDays : (NSDictionary *)item
{
    self.nameLbl.text = @"";
    self.priceLbl.text = @"";
    NSArray *keyArray = [item allKeys];
    NSLog(@"keyarray====%@",keyArray);
    NSString *text = [[keyArray objectAtIndex:1] isEqualToString:K_price] ? keyArray[0] :keyArray[1];
    NSLog(@"text ==== %@",text);
    self.nameLbl.text = text;
    UIColor *textColor  = UICOLOR_FontColor;
   
    if (self.nameLbl.text == nil){
        
        self.nameLbl.text = text;
    }
   NSAttributedString *header = [[NSAttributedString alloc]initWithString:self.nameLbl.text attributes:@{ NSForegroundColorAttributeName : textColor }];
    self.nameLbl.attributedText = header;
    // Configure the cell before it is displayed...
    NSString *price = [item valueForKey:K_price];
    //NSAttributedString *attributeString1 = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%ld@ ₹%.02f",[[item valueForKey:text] integerValue],[price floatValue]]  attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(13)}];
    
    NSAttributedString *attributeString1 = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.01f* Rs. %.f",[[item valueForKey:text] floatValue],[price floatValue]]  attributes:@{ NSForegroundColorAttributeName : textColor }];
    
    if(attributeString1 == nil){
        self.priceLbl.text = @"Rs. 0.0";
        NSAttributedString *header = [[NSAttributedString alloc]initWithString:self.priceLbl.text attributes:@{ NSForegroundColorAttributeName : textColor }];
        self.priceLbl.attributedText = header;
    }
    else{
        self.priceLbl.attributedText = attributeString1;

    }
    [self.discloseBtn setSelected:false];
    self.namelabelleadingSpace.constant = -30;
    
}

- (void)setnumberfreeday : (NSDictionary *)item
{
    self.nameLbl.text = @"";
    self.priceLbl.text = @"";
    NSString *modelPrice  = @"";
    NSArray *keyArray = [item allKeys];
    self.nameLbl.text =  keyArray[0];
    NSString *value = [item valueForKey:keyArray[0]];
    //modelPrice = [NSString stringWithFormat:@"%.02f", value.floatValue ];
    modelPrice = [NSString stringWithFormat:@"%d", value.intValue ];
    UIColor *textColor  = [UIColor colorWithRed:65.0f/255.0f green:64.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
    NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:modelPrice attributes:@{ NSForegroundColorAttributeName : textColor}];
    self.priceLbl.attributedText = attributeString2;
    [self.discloseBtn setSelected:false];
    self.namelabelleadingSpace.constant = -30;
    
}

- (void)setdiscountonbasePrice : (NSDictionary *)item
{
    self.nameLbl.text = @"";
    self.priceLbl.text = @"";
    NSString *modelPrice  = @"";
    NSArray *keyArray = [item allKeys];
    UIColor *textColor  = [UIColor blackColor];
    NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:@" (on Base Fare)" attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]initWithString:keyArray[0] attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}];
    [text appendAttributedString:attributeString2];
    self.nameLbl.attributedText = text;
    NSString *value = [item valueForKey:keyArray[0]];
    modelPrice = [NSString stringWithFormat:@"%.f", [value floatValue]];
    attributeString2 = [[NSAttributedString alloc]initWithString:modelPrice attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}];
    NSMutableAttributedString *costLabel = [[NSMutableAttributedString alloc]initWithString:@"Rs. " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_SEMIBOLD_OPENSANS(11.1)}];
    [costLabel appendAttributedString:attributeString2];
    self.priceLbl.attributedText = costLabel;
    self.namelabelleadingSpace.constant = -30;
    [self.discloseBtn setSelected:false];
}

- (void) setAmenitiesContent : (NSDictionary *)dic Selected :(BOOL)select
{
    AmenitiesInfo *amenities = dic.copy;
    if ([amenities.ServiceID isEqualToString:@"3"]) {
        /// Do nothing in Home pick up drop off
    } else {
        self.nameLbl.text = amenities.Description;
        UIColor *textColor  = UICOLOR_FontColor;
        NSString *string = [NSString stringWithFormat:@"%.f", [amenities.Amount floatValue]];
        NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:string attributes:@{ NSForegroundColorAttributeName : textColor}];
        NSMutableAttributedString *costLabel = [[NSMutableAttributedString alloc]initWithString:@"Rs. " attributes:@{ NSForegroundColorAttributeName : textColor}];
        [costLabel appendAttributedString:attributeString2];
        self.priceLbl.attributedText = costLabel;
        [self.discloseBtn setSelected:select];
        self.serviceId = amenities.ServiceID;
        //NSLog(@"%@",[NSValue valueWithCGRect:self.discloseBtn.frame]);
        [self.discloseBtn setImage:[UIImage imageNamed:k_checkicon] forState:UIControlStateSelected];
        [self.discloseBtn setImage:[UIImage imageNamed:k_blankBoxIcon] forState:UIControlStateNormal];
        
        self.namelabelleadingSpace.constant = 0;
    }
}


- (void) setCheckAmenitiesContent : (NSDictionary *)dic Selected :(BOOL)select
{
    AmenitiesDetails *amenities = dic.copy;
    self.nameLbl.text = amenities.detail;
    self.nameLbl.textAlignment = NSTextAlignmentLeft;
    self.serviceId = amenities.aminiti_id;
    [self.discloseBtn setSelected:select];
    self.btnleadingSpace.constant = - (CGRectGetWidth(self.frame) - ((IS_IPHONE_4_OR_LESS)?30:(((IS_IPHONE_5)?40:15))));
    [self updateConstraints];
    [self.discloseBtn setImage:[UIImage imageNamed:k_checkicon] forState:UIControlStateSelected];
    [self.discloseBtn setImage:[UIImage imageNamed:k_blankBoxIcon] forState:UIControlStateNormal];
    
    
}

- (void) setCheckDocumentContent : (NSDictionary *)dic Selected :(BOOL)select
{
    DocumentModel *amenities = dic.copy;
    self.nameLbl.text = amenities.docname;
    self.nameLbl.textAlignment = NSTextAlignmentLeft;
    self.serviceId = amenities.docid;
    [self.discloseBtn setSelected:select];
    self.btnleadingSpace.constant = - (CGRectGetWidth(self.frame) -((IS_IPHONE_4_OR_LESS)?30:(((IS_IPHONE_5)?40:15))));
    [self updateConstraints];
    [self.discloseBtn setImage:[UIImage imageNamed:k_checkicon] forState:UIControlStateSelected];
    [self.discloseBtn setImage:[UIImage imageNamed:k_blankBoxIcon] forState:UIControlStateNormal];
}
- (IBAction)discloseBtnTouched:(UIButton *)sender {
}
@end
