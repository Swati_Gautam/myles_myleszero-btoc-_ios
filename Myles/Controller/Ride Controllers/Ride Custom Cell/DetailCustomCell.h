//
//  MealDealCustomCell.h
//  Myles
//
//  Created by Myles on 8/18/14.
//  Copyright (c) 2015 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCustomCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UIButton *discloseBtn;

@property (strong, nonatomic) NSString *serviceId;
- (IBAction)discloseBtnTouched:(UIButton *)sender;
- (void) setCellContent : (NSDictionary *)item;
- (void)setnumberofDays : (NSDictionary *)item;
- (void)setnumberfreeday : (NSDictionary *)item;
- (void)setdiscountonbasePrice : (NSDictionary *)item;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnleadingSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *namelabelleadingSpace;

- (void) setAmenitiesContent : (NSDictionary *)dic Selected :(BOOL)select;
- (void) setCheckAmenitiesContent : (NSDictionary *)dic Selected :(BOOL)select;
- (void) setCheckDocumentContent : (NSDictionary *)dic Selected :(BOOL)select;

@end
