
#import "BookingConfirmationCustomCell.h"
#import <CoreLocation/CoreLocation.h>
#import "RideHistoryDetails.h"
@import GoogleMaps;


@implementation BookingConfirmationCustomCell
{
    CLLocationCoordinate2D pointLocation;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (void)setZerothRow : (NSDictionary *)dic
{
    
    RideHistoryDetails *ridedetail = dic.copy;
    NSString *str = ridedetail.Tripstatus;
    NSDateFormatter *formet = [[NSDateFormatter alloc]init];
    [formet setDateFormat:@"MM-dd-yyyy"];
    //PickUpdate
    NSDate *pickupdate = [formet dateFromString:ridedetail.PickupDate];
    //Pickup time find out convert user time in right formet
    NSString *pickupTime = ridedetail.PickupTime;
    //
    NSTimeInterval interval = 60 * 60 * [[pickupTime substringToIndex:2] integerValue] + 60 * [[pickupTime substringFromIndex:2] integerValue];
    formet = [[NSDateFormatter alloc] init];
    [formet setDateFormat:@"hh:mm a"];
    pickupTime = [formet stringFromDate:[pickupdate dateByAddingTimeInterval:interval]];
    formet.dateFormat = @"MM-dd-YYYY hh:mm a";
    
    if (([str isEqualToString:k_tripstatusClose])  || [str isEqualToString:k_tripstatusCancel] )
    {
        self.contentView.backgroundColor = [UIColor redColor];
        UIImage *sourceImage = self.cell1ThumbIcon.image;
        self.cell1ThumbIcon.image = [UIImage imageWithCGImage:sourceImage.CGImage
                                                        scale:sourceImage.scale
                                                  orientation:UIImageOrientationDownMirrored];
    }
    else
    {
        self.cell1ThumbIcon.transform = CGAffineTransformMakeRotation(0);
    }
    if (([str isEqualToString:k_tripstatusNew] ) || [str isEqualToString:k_tripstatusOpen] )
    {
        self.cell1label.text = @"BOOKING CONFIRMED";
    }
    else if ([str isEqualToString:k_tripstatusClose])
    {
        self.cell1label.text = @"BOOKING CLOSED";
    }
    else
    {
        self.cell1label.text = @"BOOKING CANCELLED";
    }
        //////NSLog(@"Trip Status : %@",ridedetail.Tripstatus);
    
}
- (void)setfirstRowCarModelPrize : (NSString *)modelName Prize : (NSString *)modelPrize Seating : (NSString *)modelSeating BookingId : (NSString *)bookingid
{
    
    UIColor *textColor  = [UIColor colorWithRed:65.0f/255.0f green:64.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
    NSAttributedString *attributemodelName = [[NSAttributedString alloc]initWithString:modelName attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(13)}];
    
    self.cell2Modelname.attributedText = attributemodelName;
    //[self.cell2Modelname sizeToFit];
    if (modelPrize.length > 0)
    {
        UIColor *textColor  = [UIColor colorWithRed:65.0f/255.0f green:64.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
        
        NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:modelPrize attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(20)}];
        
        NSMutableAttributedString *costLabel = [[NSMutableAttributedString alloc]initWithString:@"₹ " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(17)}];
        
        [costLabel appendAttributedString:attributeString2];
        
        self.cell2modelPrize.attributedText = costLabel;
        
    }
    
    NSAttributedString *attributeBookingID = [[NSAttributedString alloc]initWithString:bookingid attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(13)}];
    
    
    self.cell2ModelSeating.text = [NSString stringWithFormat:@"%@ Seater",modelSeating];
    self.cell2BookingID.attributedText = attributeBookingID;
}

- (void) setSecondRowRideTime : (NSString *)fromDate FromTime : (NSString *)fromtime ToDate : (NSString *)todate ToTime : (NSString *)totime
{
    
    
    NSArray *array = [CommonFunctions gettimeAndDateinDisplayFormet:fromDate FromTime:fromtime ToDate:todate ToTime:totime DateFormet:@"MM-dd-yyyy"];
    self.cell3FromDate.text = array[0];
    self.cell3ToDate.text = array[1];
    self.cell3FromTime.text = array[2];
    self.cell3ToTime.text = array[3];
    
}

/*
 RentalAmount
 PreauthAmount
 PreauthStatus
 */

- (void) setPaymentRowDetailInformation : (NSString *)RentalAmount PreauthAmount : (NSString *)preauthAmount PreauthStatus : (NSString *)preauthStatus
{
    self.cellNewRentalValueLbl.text = RentalAmount;
    self.cellNewPreauthValueLbl.text = preauthAmount;
    [self.preauthPaymentLink setTitle:preauthStatus forState:UIControlStateNormal];
}

- (void)setPaymentRow : (NSDictionary *)dic
{
    
    ////NSLog(@"Values in dict =%@",dic);
    RideHistoryDetails *ridedetail = dic.copy;
    
    
    UIColor *textColor  = [UIColor colorWithRed:65.0f/255.0f green:64.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
    NSMutableAttributedString *rentalcostValue = [[NSMutableAttributedString alloc]initWithString:@"₹ " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(13)}];
    
    NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:ridedetail.PaymentAmount attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(13)}];
    
    NSMutableAttributedString *preauthcostValue = [[NSMutableAttributedString alloc]initWithString:@"₹ " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(13)}];
    
    NSAttributedString *attributeString3 = [[NSAttributedString alloc]initWithString:ridedetail.PreAuthAmount attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_Medium_RobotoFONT(13)}];
    
    [rentalcostValue appendAttributedString:attributeString2];
    [preauthcostValue appendAttributedString:attributeString3];
    
    self.cellNewRentalValueLbl.attributedText = rentalcostValue;
    self.cellNewPreauthValueLbl.attributedText = preauthcostValue;
    
    if ((ridedetail.PreAuthStatus == nil && !ridedetail.InsurancePaid)|| (ridedetail.PreAuthStatus == (id)[NSNull null] && !ridedetail.InsurancePaid)||([ridedetail.PreAuthStatus isEqualToString:@"0"] && !ridedetail.InsurancePaid))
    {
        
        self.cellNewRentalValueLbl.attributedText = rentalcostValue;
        self.cellNewPreauthValueLbl.attributedText = preauthcostValue;
        
        
        if([ridedetail.Tripstatus isEqualToString:@"New"]||[ridedetail.Tripstatus isEqualToString:@"Open"]) {
            //self.securityPaymentStatus = @"Click here to pay";
           // //NSLog(@"Default button title: %@", self.preauthPaymentLink.titleLabel.text);
            [self.preauthPaymentLink setTitle:@"Click here to pay" forState:UIControlStateNormal];
            //cell.preauthPaymentLink.titleLabel.textAlignment = NSTextAlignmentCenter;
            [self.preauthPaymentLink setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            ////NSLog(@"New button title: %@", self.preauthPaymentLink.titleLabel.text);
            self.preauthPaymentLink.enabled=YES;
            /*
           /[self.preauthPaymentLink addTarget: self
                                        action: @selector(myPaymentAction:)
                              forControlEvents: UIControlEventTouchUpInside];
             */
            
            
        }
        else {

        //self.securityPaymentStatus = @"Not Paid";
        ////NSLog(@"Default button title: %@", self.preauthPaymentLink.titleLabel.text);
            
        
        [self.preauthPaymentLink setTitle:@"Not Paid" forState:UIControlStateNormal];
        [self.preauthPaymentLink setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        //////NSLog(@"New button title: %@", self.preauthPaymentLink.titleLabel.text);
        self.preauthPaymentLink.enabled=NO;
        }
    }

    else
    {
    
      //  self.securityPaymentStatus = @"Paid";
        if (ridedetail.InsurancePaid) {
            self.cellNewPreauthTitleLbl.text = @"Myles Coer";
            self.cellNewPreauthValueLbl.text = @"";
            [self.preauthPaymentLink setTitle:@"View LDW Certificate" forState:UIControlStateNormal];
            [self.preauthPaymentLink setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            self.preauthPaymentLink.enabled= YES;
        } else {
            [self.preauthPaymentLink setTitle:@"Paid" forState:UIControlStateNormal];
            [self.preauthPaymentLink setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            //NSLog(@"New button title: %@", self.preauthPaymentLink.titleLabel.text);
            self.preauthPaymentLink.enabled= NO;
        }
    }
}


- (void)setAmenitiesDetails : (NSString *)description Amount : (NSString *)amount
{
    self.amenitiesDescription.text = description;
    UIColor *textColor  = [UIColor colorWithRed:65.0f/255.0f green:64.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
    //NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@/-",[amount substringToIndex:amount.length -3]] attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(13)}];
    
    NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@/-",amount] attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(13)}];
    
    
    NSMutableAttributedString *costLabel = [[NSMutableAttributedString alloc]initWithString:@"₹ " attributes:@{ NSForegroundColorAttributeName : textColor , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(13)}];
    [costLabel appendAttributedString:attributeString2];
    self.amenitiesAmount.attributedText = costLabel;
}

- (void)setThirdRowAddress : (NSMutableDictionary *)addressDict PointLocation : (CLLocationCoordinate2D )location Title : (NSString *)title
{
//    if ([[addressDict objectForKey:@"pickUp"] length] > 0)
//    {
        self.pickUpAddressLabel.lineBreakMode = UILineBreakModeWordWrap;
        self.pickUpAddressLabel.numberOfLines = 0;
    self.pickUpAddressLabel.text = [addressDict objectForKey:@"pickUp"] != nil ? [[addressDict objectForKey:@"pickUp"] length] > 0 ? [addressDict objectForKey:@"pickUp"] : title: title;
        self.pickUpAddressLabel.hidden = false;
        self.pickUpTitleLabel.hidden = false;

//    }
//    else
//    {
//        self.pickUpAddressLabel.hidden = YES;
//         self.pickUpTitleLabel.hidden = true;
//    }
    
    pointLocation = location;
    //self.cell4AddressLabel.text = [[addressDict objectForKey:@"sublocation"] uppercaseString];
    //Sourabh Change after home Pick up drop off
    self.cell4AddressLabel.text = @"";
    //[addressDict objectForKey:@"sublocation"];
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.cellMapview.bounds.size.width, self.cellMapview.bounds.size.height)];
    self.mapView.delegate = self;
    MKPointAnnotation *pointOfOrigin =  [[MKPointAnnotation alloc]init];
    pointOfOrigin.coordinate = location;
    pointOfOrigin.title = title;
    [self.mapView addAnnotation:pointOfOrigin];
    self.mapView.zoomEnabled = NO;
    self.mapView.rotateEnabled = NO;
    
    MKMapCamera *mapcamera = [[MKMapCamera alloc]init];
    mapcamera.centerCoordinate = location;
    mapcamera.pitch = 4;
    mapcamera.altitude = 5000;
    mapcamera.heading = 90.0;
    mapcamera.altitude = 500.0;
    [_mapView setCamera:mapcamera animated:YES];

    
    [self.cellMapview addSubview:self.mapView];
}


- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"Point"];
    if (![annotation isKindOfClass:[MKUserLocation class]]) {
       
        annotationView.image = UIImageName(k_pinIcon);
        annotationView.canShowCallout = YES;
    }
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    self.directionTrailingConstraint.constant = 20;
    
    [UIView animateWithDuration:0.2 animations:^{
       
        [self layoutIfNeeded];
    }];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    
    self.directionTrailingConstraint.constant = -71;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [self layoutIfNeeded];
    }];
}

- (IBAction)positionInMap:(id)sender {
 
    NSString *url = [NSString stringWithFormat: @"http://maps.google.com/?q=%f,%f", pointLocation.latitude, pointLocation.longitude];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (IBAction)directionInMap:(id)sender {
    
    NSString* url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=Current+Location&daddr=%f,%f", pointLocation.latitude, pointLocation.longitude];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}

/*
 
 -(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
 {
 static NSString * const kPinAnnotationIdentifier = @"PinIdentifier";
 if(annotation == self._mapView.userLocation)
 {
 return nil;
 }
 
 MyAnnotation *myAnnotation  = (MyAnnotation *)annotation;
 MKAnnotationView *newAnnotation = (MKAnnotationView*)[self._mapView dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationIdentifier];
 
 if(!newAnnotation){
 newAnnotation = [[MKAnnotationView alloc] initWithAnnotation:myAnnotation reuseIdentifier:@"userloc"];
 }
 
 NSDictionary *dict=[alertInfoArray objectAtIndex:myAnnotation.ann_tag];
 UIView *anView=[[UIView alloc] init];
 anView.backgroundColor=[UIColor clearColor];
 
 UIImageView *bgImg=[[UIImageView alloc] init];
 bgImg.image=[UIImage imageNamed:@""];
 bgImg.backgroundColor=[UIColor clearColor];
 
 UIImageView *imgView=[[UIImageView alloc] init];
 imgView.tag=myAnnotation.ann_tag;
 
 UILabel *lblName=[[UILabel alloc] init];
 lblName.font=[UIFont systemFontOfSize:12];
 lblName.textAlignment=UITextAlignmentCenter;
 lblName.textColor=[UIColor whiteColor];
 lblName.backgroundColor=[UIColor clearColor];
 lblName.text=TEXT YOU WANT ;
 
 newAnnotation.frame=CGRectMake(0, 0, 70, 212);
 anView.frame=CGRectMake(0, 0, 70, 212);
 bgImg.frame=CGRectMake(0, 0, 70, 106);
 bgImg.image=[UIImage imageNamed:@"thumb-needhelp.png"];
 
 imgView.frame=CGRectMake(8,25,55,48);
 imgView.image=[UIImage imageNamed:@"girl-default.png"];
 lblName.frame=CGRectMake(5,79,60,10);
 
 
 
 [anView addSubview:bgImg];
 [bgImg release];
 [anView addSubview:imgView];
 [imgView release];
 [newAnnotation addSubview:anView];
 [anView release];
 
 newAnnotation.canShowCallout=YES;
 [newAnnotation setEnabled:YES];
 
 return newAnnotation;
 }
 */




@end
