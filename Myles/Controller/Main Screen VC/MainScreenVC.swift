//
//  MainScreenVC.swift
//  Myles
//
//  Created by Swati Gautam on 22/03/21.
//  Copyright © 2021 Myles. All rights reserved.
//

import UIKit

class MainScreenVC: UIViewController {

    @IBOutlet weak var btnMylesZero: UIButton!
    @IBOutlet weak var viewMylesZero: UIView!
    @IBOutlet weak var btnMyles: UIButton!
    @IBOutlet weak var viewMyles: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        self.btnMyles.layer.cornerRadius = 4.0
        self.viewMyles.layer.cornerRadius = 4.0
        self.viewMylesZero.layer.cornerRadius = 4.0
        self.btnMylesZero.layer.cornerRadius = 4.0
        //print("Logged In: ", DefaultsValues.getBooleanValueFromUserDefaults_(forKey: IS_LOGIN))
        print("Logged In Myles: ", UserDefaults.standard.bool(forKey: IS_LOGIN))
    }
    
    @IBAction func btnMyles_Click(_ sender: Any)
    {
        print("Myles Tapped")
        /*let mainStoryboard: UIStoryboard = UIStoryboard(name: k_mainStoryBoard, bundle: nil)
        let objHomeVC = mainStoryboard.instantiateViewController(withIdentifier: k_HomeViewController) as! HomeViewController
        let revealController: SWRevealViewController  = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        let homeNavigationController: UINavigationController  = mainStoryboard.instantiateViewController(withIdentifier: "HomeNavigation") as! UINavigationController
        revealController.setFront(homeNavigationController, animated: true)
        let menuController: MenuViewController  = mainStoryboard.instantiateViewController(withIdentifier: k_MenuViewController) as! MenuViewController
        (revealController.rearViewController as? MenuViewController)?.selectedIndex = 0
        revealController.setRear(menuController, animated: true)
        //AppDelegate.getSharedInstance().configDynamicShortcutItems()
        AppDelegate.getSharedInstance()?.window.rootViewController = revealController*/
        
        let mainTabStoryboard: UIStoryboard = UIStoryboard(name: "MainTabStoryboard", bundle: nil)
        let viewController = mainTabStoryboard.instantiateViewController(withIdentifier: "MainTabController")
        AppDelegate.getSharedInstance()?.window.rootViewController = viewController
        //self.navigationController?.pushViewController(objHomeVC, animated: true)
    }

    @IBAction func btnMylesZero_Click(_ sender: Any)
    {
        print("Myles Zero Tapped")
        if CommonFunctions.reachabiltyCheck()
        {
            let objSearchCar = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCar) as! SearchCarVC
            let navigationController : UINavigationController = UINavigationController.init(rootViewController: objSearchCar)
            AppDelegate.getSharedInstance()?.window.rootViewController = navigationController
        }
        else
        {
            let mainTabStoryboard: UIStoryboard = UIStoryboard(name: "MainTabStoryboard", bundle: nil)
            //if (AppDelegate.getSharedInstance()?.window.rootViewController?.presentedViewController?.isKind(of: LoginProcess()))
            if (AppDelegate.getSharedInstance()?.window.rootViewController?.presentedViewController is LoginProcess)
            {
                AppDelegate.getSharedInstance()?.window.rootViewController?.dismiss(animated: false, completion: nil)
            }
            let noInternet = mainTabStoryboard.instantiateViewController(withIdentifier: "NoInternetConnViewController")
            AppDelegate.getSharedInstance()?.window.rootViewController = noInternet
            
            /*let noInternet = mainTabStoryboard.instantiateViewController(withIdentifier: "NoInternetConnViewController") as! NoInternetConnViewController
            let navigationController : UINavigationController = UINavigationController.init(rootViewController: noInternet)
            AppDelegate.getSharedInstance()?.window.rootViewController = navigationController*/
        }
        
        //self.navigationController?.pushViewController(objSearchCar, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
