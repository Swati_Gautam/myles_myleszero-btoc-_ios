//
//  FareDetailCell.swift
//  Myles
//
//  Created by iOS Team on 14/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

class FareDetailCell: UITableViewCell {

    //IBOUTLETS
    @IBOutlet weak var mainDetailView: UIView!
    
    @IBOutlet weak var lowerSummaryView: UIView!
    
    @IBOutlet weak var rentalChargeLabel: UILabel!
    @IBOutlet weak var extraChargeLabel: UILabel!
    @IBOutlet weak var homePickAirportChargeLabel: UILabel!
    @IBOutlet weak var ldwChargeLabel: UILabel!
    @IBOutlet weak var totalSumLabel: UILabel!
    
    @IBOutlet weak var amountPaidLabel: UILabel!
    
    @IBOutlet weak var pendingExtensionLabel: UILabel!
    
    @IBOutlet weak var doorstepOrAirportNameLabel: UILabel!
    
    
    
    @IBOutlet weak var ldwView: UIView!
    @IBOutlet weak var ldwHeadingLabel: UILabel!
    
    @IBOutlet weak var firstTaxView: UIView!
    
    @IBOutlet weak var firstTaxNameLabel: UILabel!
    @IBOutlet weak var firstTaxLabelValue: UILabel!

    
    @IBOutlet weak var secondTaxView: UIView!
    @IBOutlet weak var secondTaxLabelValue: UILabel!
    
    @IBOutlet weak var secondTaxNameLabel: UILabel!
    
    @IBOutlet weak var extensionView: UIView!
    
    @IBOutlet weak var extensionLabel: UILabel!
    
    @IBOutlet weak var extensionAmountLabel: UILabel!
    
    var gstTaxSlabs : [GSTSLabs] = []
    var GSTRate : Float = 0.0
    var allServicesRate : Float = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainDetailView.layer.cornerRadius = 4
        //gstTaxSlabs = [];
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.layer.masksToBounds = true

        // Configure the view for the selected state
    }
    
    //Swati Commented
    
    func calculateGSTTaxSlab(modelObj: RideHistoryDetails) {
        if (modelObj.cgstRate.floatValue > 0) {
            let gstSlabsObj = GSTSLabs(nameOfGSTSlab: "CGST",rateOfGSTSlab: modelObj.cgstRate,amountOfGSTSlab:modelObj.cgstAmount)
            gstTaxSlabs.append(gstSlabsObj)
        }
        if (modelObj.sgstRate.floatValue > 0) {
            let gstSlabsObj = GSTSLabs(nameOfGSTSlab: "SGST",rateOfGSTSlab: modelObj.sgstRate, amountOfGSTSlab: modelObj.sgstAmount)
            gstTaxSlabs.append(gstSlabsObj)
        }
        if (modelObj.utgstRate.floatValue > 0) {
            let gstSlabsObj = GSTSLabs(nameOfGSTSlab: "UTGST",rateOfGSTSlab: modelObj.utgstRate, amountOfGSTSlab: modelObj.utgstAmount)
            gstTaxSlabs.append(gstSlabsObj)
        }
        if (modelObj.igstRate.floatValue > 0) {
            let gstSlabsObj = GSTSLabs(nameOfGSTSlab: "IGST",rateOfGSTSlab: modelObj.igstRate, amountOfGSTSlab: modelObj.igstAmount)
            gstTaxSlabs.append(gstSlabsObj)
        }
        print("count of array = \(gstTaxSlabs.count)")
    }        //Swati Commented
    
    
    /*func calculateGSTTaxSlab(modelObj: RideHistoryDetails)
    {
        GSTRate = (modelObj.cgstRate.floatValue + modelObj.igstRate.floatValue + modelObj.sgstRate.floatValue + modelObj.utgstRate.floatValue + modelObj.cessRate.floatValue)/100;
        let stringGSTRate =  String(describing: GSTRate)
        
        allServicesRate = (modelObj.igstRate_Service.floatValue + modelObj.cgstRate_Service.floatValue + modelObj.sgstRate_Service.floatValue + modelObj.utgstRate_Service.floatValue)/100;
        let stringAllServicesRate =  String(describing: allServicesRate)
        
        let gstSlabsObj = GSTSLabs(nameOfTaxes: "Total Taxes", rateOfAllGST: stringGSTRate, rateOfAllService: stringAllServicesRate)
        gstTaxSlabs.append(gstSlabsObj)
    }*/
    
    func setUpDataForFareDetailCell(modelObj : RideHistoryDetails)
    {
        amountPaidLabel.text = String(format: "Amount Paid : ₹ %.f",modelObj.paidAmount.floatValue)
        
        if (modelObj.extensionDetail.count > 0) {
            // taken extension
            if (modelObj.pendingExtensionAmount.floatValue > 0) {
                //extension maount pending
                pendingExtensionLabel.text = String(format: "Pending Extension Amount : ₹ %.f",modelObj.pendingExtensionAmount.floatValue)
            } else {
                // 0 extension amount 
                pendingExtensionLabel.text = "No Pending Extension"
            }
        } else {
            //not taken extension
            pendingExtensionLabel.text = "No Extension Request"
        }
        
        if modelObj.extensionDetail.count > 0 {
            extensionView.isHidden = false
            
            extensionAmountLabel.text = String(format: "₹ %.f",modelObj.extensionPaidAmount.floatValue)

        } else {
            extensionView.isHidden = true
        }
        
        
        print("rentalcharge = \(String(describing: modelObj.rentalcharge))")
        rentalChargeLabel.text = String(format: "₹ %.f", modelObj.rentalcharge.floatValue)
            //modelObj.rentalcharge
        extraChargeLabel.text = String(format: "₹ %.f",modelObj.extracharge.floatValue)
        
        
        homePickAirportChargeLabel.text = String(format: "₹ %.f",modelObj.homeairportpickdropcharge.floatValue)
        print(gstTaxSlabs.count)

        print("ldw amount = \(modelObj.ldwAmount!)")
        
        //totalSumLabel.text = String(format: "Total = ₹ %.f",modelObj.totalAmount.floatValue)
        totalSumLabel.text = String(format: "Total = ₹ %.f",modelObj.paidAmount.floatValue) //Swati Changes
        
        //For Myles Secure (Wrong)
        /*if modelObj.securityAmount.floatValue > 0.0
        {
            //totalSumLabel.text = String(format: "Total = ₹ %.f",modelObj.totalAmount.floatValue)
            let total = modelObj.securityAmount.floatValue + modelObj.paidAmount.floatValue
            totalSumLabel.text = String(format: "Total = ₹ %d",Int(total)) //Swati Changes
            //totalSumLabel.text = String(format: "Total = ₹ %.f",modelObj.paidAmount.floatValue) //Swati Changes
            
        }
        else
        {
            //totalSumLabel.text = String(format: "Total = ₹ %.f",modelObj.totalAmount.floatValue)
            totalSumLabel.text = String(format: "Total = ₹ %.f",modelObj.paidAmount.floatValue) //Swati Changes
        }*/
        
        
        //var gstTax = [String:String]()
        if (modelObj.ldwAmount.floatValue > 0) {
            ldwHeadingLabel.text = "Myles Secure (Inclusive Tax)"
            ldwChargeLabel.text = String(format: "₹ %.f",modelObj.ldwAmount.floatValue)
            
            //Check VAT present or GST
            if (modelObj.vatAmount.floatValue > 0) {
                firstTaxNameLabel.text = "VAT"
                firstTaxLabelValue.text = String(format: "₹ %.f",modelObj.vatAmount.floatValue)
                secondTaxView.isHidden = true
            }
            else
            {
                firstTaxNameLabel.text = String(format: "Total Taxes")
                firstTaxLabelValue.text = modelObj.totalTax //String(format: "₹ %d",modelObj.totalTax)
                secondTaxView.isHidden = true
                //secondTaxNameLabel.isHidden = true
                //secondTaxLabelValue.isHidden = true
                
                //first calculate
                /*calculateGSTTaxSlab(modelObj: modelObj)
                if gstTaxSlabs.count > 0 {
                    print("array count = \(gstTaxSlabs.count)")
                    let gstSlabObj : GSTSLabs = gstTaxSlabs[0]
                    firstTaxNameLabel.text = "\(gstSlabObj.nameOfGSTSlab!)@\(gstSlabObj.rateOfGSTSlab!)"
                    firstTaxLabelValue.text = String(format: "₹ %.f",gstSlabObj.amountOfGSTSlab.floatValue)
                    //Show tax1view and hide tax 2view      //Swati Changes
                    /*let gstSlabObj2 : GSTSLabs = gstTaxSlabs[1]
                    secondTaxNameLabel.text = "\(gstSlabObj2.nameOfGSTSlab!)@\(gstSlabObj2.rateOfGSTSlab!)"
                    secondTaxLabelValue.text = String(format: "₹ %.f",gstSlabObj2.amountOfGSTSlab.floatValue)*/
                }   */
            }
            
        }
        else {
            //First check whether vat is applicable or GST
            // If vat then remove both downside taxes view and shift view upwards
            //else instead of ldw show gst values and hide last gst tax view
            
            if (modelObj.vatAmount.floatValue > 0 || modelObj.vatAmount.floatValue < 0) {
                ldwHeadingLabel.text = "VAT"
                ldwChargeLabel.text = String(format: "₹ %.f",modelObj.vatAmount.floatValue)
                firstTaxView.isHidden = true
                secondTaxView.isHidden = true
            }
            else
            {
                //first calculate
                firstTaxNameLabel.text = String(format: "Total Taxes")
                firstTaxLabelValue.text = modelObj.totalTax //String(format: "₹ %d",modelObj.totalTax)
                secondTaxView.isHidden = true
                ldwHeadingLabel.text = "Myles Secure (Inclusive Tax)"
                ldwChargeLabel.text = String(format: "₹ %.f",modelObj.ldwAmount.floatValue)
                
                //secondTaxNameLabel.isHidden = true
                //secondTaxLabelValue.isHidden = true
                
                /*calculateGSTTaxSlab(modelObj: modelObj)
                if gstTaxSlabs.count > 0
                {
                    let gstSlabObj : GSTSLabs = gstTaxSlabs[0]
                    ldwHeadingLabel.text = "\(gstSlabObj.nameOfGSTSlab!)@\(gstSlabObj.rateOfGSTSlab!)"
                    ldwChargeLabel.text = String(format: "₹ %.f",gstSlabObj.amountOfGSTSlab.floatValue)
                    //Show tax1view and hide tax 2view
                    if gstTaxSlabs.count > 1
                    {
                        let gstSlabObj2 : GSTSLabs = gstTaxSlabs[1]
                        firstTaxNameLabel.text = "\(gstSlabObj2.nameOfGSTSlab!)@\(gstSlabObj2.rateOfGSTSlab!)"
                        firstTaxLabelValue.text = String(format: "₹ %.f",gstSlabObj2.amountOfGSTSlab.floatValue)
                        //hide 2ndtax view
                        secondTaxView.isHidden = true
                    }                    
                }*/
            }
        }
    }
}


struct GSTSLabs
{    
    let nameOfGSTSlab : String!
    let rateOfGSTSlab : String!
    let amountOfGSTSlab : String!
    
    init(nameOfGSTSlab:String,rateOfGSTSlab:String,amountOfGSTSlab:String) {
        self.nameOfGSTSlab = nameOfGSTSlab
        self.rateOfGSTSlab = rateOfGSTSlab
        self.amountOfGSTSlab = amountOfGSTSlab
    }
    
    init(nameOfTaxes:String,rateOfAllGST:String,rateOfAllService:String) {
        self.nameOfGSTSlab = nameOfTaxes
        self.rateOfGSTSlab = rateOfAllGST
        self.amountOfGSTSlab = rateOfAllService
    }
}
