//
//  BookingDetailsNewController.swift
//  Myles
//
//  Created by iOS Team on 13/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD

fileprivate let kTableViewHeaderHeight: CGFloat = 200

class BookingDetailsNewController: UITableViewController,BackProtocol,ExtensionPaymentProtocol {

    fileprivate var totalNumberOfCell = 0
    var bookingID : String!
    fileprivate var expandedLabel: UILabel!
    fileprivate var extensionExpandedLabel : UILabel!
    fileprivate var localExtensionExpanded = false
    var expandedRows = Set<Int>()
    var localExpanded = false
    var rideHistoryDetailsObj : RideHistoryDetails!
    var selectedSegmentTapped : String!
    var headerValues = Dictionary<String, Any>()
    //let cancDesc = "1. If booking is cancelled within 24 hours of pickup time : ₹ 200 or 50 % of Total Booking Amount (whichever is greater)  will be deducted and 3 % processing fee will be charged on rest of the refund amount.\n2. If booking is cancelled at least 24 hours prior to pick up time, no rental will be deducted, only ₹ 200+ 3% processing fee will be charged on the refund amount.\n3. If booking is cancelled at or after pick up time, it’ll be treated as no-show and no refund will be provided.For \n\nHourly Rentals:\n\nNo refund will be provided in case of hourly rental booking"
    
    let extensionPolicyDesc = "1. When the extension request is made 24 hours or before scheduled drop-off time and car is not booked during the requested period, 30% charges will be levied on the extended duration + Loss Damage Waiver charges applicable on extended duration.\n2. When the extension request is made less than 24 hours prior to scheduled drop-off time and the car is not booked during the requested period, 50% additional charges will be charged on the extended duration + Loss Damage Waiver charges applicable on extended duration.\n3. For all other cases, you will have to pay 100% additional charges on rental amount for extended duration + Loss Damage Waiver charges applicable on extended duration."

    // IBOUTLETS
    
    @IBOutlet weak var headerCarNameLabel: UILabel!
    
    @IBOutlet weak var headerBookingIDLabel: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Registering Cell
        tableView.register(UINib(nibName: "BookingDetailCell", bundle: nil), forCellReuseIdentifier: "BookingDetailCell")
        
        tableView.register(UINib(nibName: "FareDetailCell", bundle: nil), forCellReuseIdentifier: "FareDetailCell")
        
        tableView.register(UINib(nibName: "BookingExtensionCell", bundle: nil), forCellReuseIdentifier: "BookingExtensionCell")
        //BookingExtensionCell
        //BookingHeaderExtensionCell
        //BookingExtensionFooterCell
        tableView.register(UINib(nibName: "BookingExtensionHeaderCell", bundle: nil), forCellReuseIdentifier: "BookingExtensionHeaderCell")
        
        tableView.register(UINib(nibName: "BookingExtensionFooterCell", bundle: nil), forCellReuseIdentifier: "BookingExtensionFooterCell")
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        self.navigationController?.isNavigationBarHidden = true
        //self.navigationController?.navigationBar.alpha = 0
        // HeaderView
        //headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.tableFooterView = nil
        tableView.addSubview(headerView)
        //tableView.tableHeaderView = headerView
        tableView.contentInset = UIEdgeInsets(top: kTableViewHeaderHeight , left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x:0,y:-kTableViewHeaderHeight)
        
        //print("passed = \(rideHistoryDetailsObj.bookingid)")
        tableView.bringSubviewToFront(headerView)
        updateHeaderView()
        
        if (rideHistoryDetailsObj != nil) {
            tableView.delegate = self
            tableView.dataSource = self
            //self.automaticallyAdjustsScrollViewInsets = false
            //self.tableView.estimatedRowHeight = 44.0
            //self.tableView.rowHeight = UITableViewAutomaticDimension
            headerCarNameLabel.text = rideHistoryDetailsObj.carcatname + " " + rideHistoryDetailsObj.carModelName
            headerBookingIDLabel.text = "BookingID - \(rideHistoryDetailsObj.bookingid!)"
            //        UIView.animateWithDuration(1.0, animations: {
            //            self.arrowImageView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
            //        })
            
            
            
            print("rideHistoryDetailsObj.extensionDetails.count= \(rideHistoryDetailsObj.extensionDetail.count)")
        } else {
            getCompleteBookingData(bookingIdStr: bookingID)
        }
        

    }
    
    func getCompleteBookingData(bookingIdStr: String) {
        showLoader()
        print("get complete data")

        let dictPost = ["userid": (UserDefaults.standard.object(forKey: KUSER_ID) as! String),"historylive": bookingIdStr]
        print("dictpost = \(dictPost)")
        if CommonFunctions.reachabiltyCheck()
        {
            headerValues =  ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]
            
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+"V2_bookingHistory")! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headerValues as NSDictionary) as? [String : String]
            
            print("headers = \(String(describing: request.allHTTPHeaderFields))")
            if JSONSerialization.isValidJSONObject(dictPost)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 30
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                let task = session.dataTask(with: request as URLRequest) {[weak self] data, response, error  in
                    
                    self?.dismissLoader()
                    
                    guard response != nil else{
                        DispatchQueue.main.async {
                            self?.showAlert(message:"Server not responding" )
                        }
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    print(headers);
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        //                            completionHandler(nil,false)
                        return
                    }
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                        //                            completionHandler(nil,true)
                        DispatchQueue.main.async {
                            self?.showAlert(message:"Server not responding" )
                        }
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                        //                            completionHandler(nil,true)
                        return
                    }
                    
                    print("data in string = \(String(data: data!, encoding: String.Encoding.utf8) as String!)")
                    
                    guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                        DispatchQueue.main.async {
                            self?.showAlert(message: "Server not responding")
                        }
                        return
                    }
                    //                        let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                    if (!(json["status"]! as! Bool)) {
                        print(json["status"]!)
                        DispatchQueue.main.async {
                            self?.showAlert(message: (json["message"] as? String)!)
                        }
                        return
                    }
                    
                    print("jhbashbda = \(String(describing: json["status"]))")
                    
                    let rideHistoryResponse = try? RideHistoryResponse(data: data)
                    
                    let arrRideHistoryDetail = rideHistoryResponse?.response
                    
                    //var localRideArray : [RideHistoryDetails] = []
                    for rideHistoryDict in arrRideHistoryDetail! as NSMutableArray {
                        self?.rideHistoryDetailsObj = try! RideHistoryDetails.init(dictionary: rideHistoryDict as! [AnyHashable : Any])
                        //print(rideDetailObj.bookingid ?? "No bookingid")
                        //localRideArray.append(rideDetailObj)
                        DispatchQueue.main.async {
                            self?.tableView.delegate = self
                            self?.tableView.dataSource = self
                            self?.headerCarNameLabel.text = (self?.rideHistoryDetailsObj.carcatname)! + " " + (self?.rideHistoryDetailsObj.carModelName)!
                            self?.headerBookingIDLabel.text = "BookingID - \(String(describing: (self?.rideHistoryDetailsObj.bookingid)!))"
                            self?.navigationController?.isNavigationBarHidden = true
                            self?.tableView.reloadData()
                        }
                    }
                }
                task.resume()
                
            }
        }else
        {
            //                completionHandler(nil,true)
            CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
            
        }
        //}
    }
    
    func showLoader () {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
            SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
            SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
            SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
            SVProgressHUD.show(withStatus: "Please wait")
        }
       
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        
        if (UserDefaults.standard.bool(forKey: IS_LOGIN)) {
            updateHeaderView()

        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.removeFromParent()
        self.navigationController?.isNavigationBarHidden = false
        //self.navigationController?.navigationBar.alpha = 1
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        //http://stackoverflow.com/questions/1080274/scroll-uitableview-so-that-the-header-isnt-visible
        //updateHeaderView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Internal Fucntions
    
    func showAlert(message:String)  {
        let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler : nil)
        // let defaultAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }

    func updateHeaderView() {
        var headerRect = CGRect(x: 0, y: -kTableViewHeaderHeight, width: tableView.bounds.width, height: kTableViewHeaderHeight)
        if tableView.contentOffset.y < -kTableViewHeaderHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
            headerView.frame = headerRect
        }
        else {
//            headerRect.size.height = -tableView.contentOffset.y
//            print(headerRect.size.height)
//            //headerView.frame = headerRect
//            
//            if headerRect.size.height >= 200 {
                headerView.frame = headerRect
           // }
            
        }
        
    }
    
    func openLinkController(urlString: String , headingStr: String) {
        DispatchQueue.main.async {
            let mainStoryboard = UIStoryboard(name: "MainStoryboard", bundle: nil)
            let offerController: FlashOfferViewController? = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as? FlashOfferViewController
            offerController?.titleStr = headingStr
            offerController?.urlToNavigate = URL(string: urlString)
            self.present(offerController!, animated: true)
        }
    }
    
    @objc func openInsurance() {
        getEncryptedBookingID { (bookingID) in
            print("booking id = \(bookingID)")
            let valueOfFR = "dYaTbw58zog%3D"
            #if DEBUG
            
            // old url
   //             let strUrl: String = "https://staging.mylescars.com/bookings/LDW_certificate?book_id=\(bookingID)&type=\(valueOfFR)"
                let strUrl: String = "https://qa2.mylescars.com/bookings/LDW_certificate?book_id=\(bookingID)&type=\(valueOfFR)"
            
            //// new url
                     //    let strUrl: String = "https://staging.mylescars.com/bookings/myles_secure_certificate?book_id=\(bookingID)&type=\(valueOfFR)"
           // let strUrl: String = "https://qa2.mylescars.com/bookings/myles_secure_certificate?book_id=\(bookingID)&type=\(valueOfFR)"

            #else
//                let strUrl: String = "https://www.mylescars.com/Bookings/LDW_certificate?book_id=\(bookingID)&type=\(valueOfFR)" // old url
            let strUrl: String = "https://www.mylescars.com/Bookings/myles_secure_certificate?book_id=\(bookingID)&type=\(valueOfFR)" // new url

            
            #endif
            
            print("urlstr = \(strUrl)")
            DispatchQueue.main.async {
                let mainStoryboard = UIStoryboard(name: "MainStoryboard", bundle: nil)
                let offerController: FlashOfferViewController? = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as? FlashOfferViewController
                offerController?.titleStr = "Myles Secure Certificate"
                offerController?.urlToNavigate = URL(string: strUrl)
                self.present(offerController!, animated: true)
            }
        }
    }
    
    func getEncryptedBookingID(_ completion: @escaping (_: String) -> Void) {
        
        let dictPost = ["Value": rideHistoryDetailsObj.bookingid,"PassingType": "1"]
        
        if CommonFunctions.reachabiltyCheck()
        {
            headerValues =  ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]
            
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+"EncryptDecrypt")! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headerValues as NSDictionary) as? [String : String]
            
            print("headers = \(String(describing: request.allHTTPHeaderFields))")
            if JSONSerialization.isValidJSONObject(dictPost)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 30
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                let task = session.dataTask(with: request as URLRequest) {[weak self] data, response, error  in
                    
                    self?.dismissLoader()
                    
                    guard response != nil else{
                        DispatchQueue.main.async {
                            self?.showAlert(message:"Server not responding" )
                        }
                        completion("")
                        return
                    }
                    let headers = (response as! HTTPURLResponse).allHeaderFields
                    print(headers);
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completion("")
                        return
                    }
                    guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                        DispatchQueue.main.async {
                            self?.showAlert(message: "Server not responding")
                        }
                        completion("")
                        return
                    }
                
                    if (!(json["status"]! as! Bool)) {
                        print(json["status"]!)
                        DispatchQueue.main.async {
                            self?.showAlert(message: (json["message"] as? String)!)
                        }
                        completion("")
                        return
                    }
                    
                    completion(json["message"] as! String)
                    
                }
                task.resume()
                
            }
        }else
        {
            //                completionHandler(nil,true)
            CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
            
        }
        //}

        
    }
    
    
    @IBAction func backButtontapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (rideHistoryDetailsObj != nil) {
            if rideHistoryDetailsObj.extensionDetail.count > 0 {
                totalNumberOfCell = 3 + rideHistoryDetailsObj.extensionDetail.count + 1
                return totalNumberOfCell
            } else {
                return 2
            }
        } else {
            return 0
        }
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Configure the cell...
        let cell : UITableViewCell!
        switch indexPath.row {
        case 0:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "BookingDetailCell", for: indexPath) as! BookingDetailCell
            cell1.cancellationLabel.text = rideHistoryDetailsObj.cancelPolicy
            //cell1.isExpandable = self.expandedRows.contains(indexPath.row)
            cell1.setUpDataForBookingCell(modelObj: rideHistoryDetailsObj,withSegment: selectedSegmentTapped)
            
            cell1.mylesSecureBtn.addTarget(self, action: #selector(openInsurance), for: .touchUpInside)

            //LDW Certificate
            cell1.paySecurityOrLDWbutton.didTouchUpInside = {[weak self] sender in
                if let localLDWAmount = self?.rideHistoryDetailsObj.ldwAmount.floatValue {

                    if localLDWAmount > 0 {
                        let valueSelected: BookingStatus.RawValue = cell1.paySecurityOrLDWbutton.title(for: .normal)!
                        print(valueSelected)
                        switch valueSelected {
                        case BookingStatus.DAMAGE_INVOICE.rawValue:
                            self?.openLinkController(urlString: (self?.rideHistoryDetailsObj.damageInvoice)!, headingStr: "Damage Invoice")
                            break
                        case BookingStatus.LDW_CERTIFICATE.rawValue:
                            self?.openInsurance()
                            break
                        default:
                            self?.performSegue(withIdentifier: "extensionPaymentSegueFromDetail", sender: "https://www.mylescars.com/Bookings/call_preauth?bookingid=\(String(describing: (self?.rideHistoryDetailsObj.bookingid)!).toBase64())")

                            break
                        }
                        //
                    } else {
                        //if (self?.selectedSegmentTapped == String(describing: selectedSegment.L)) || (self?.selectedSegmentTapped == String(describing: selectedSegment.U)) {
                            // open payment page
                            self?.performSegue(withIdentifier: "extensionPaymentSegueFromDetail", sender: "https://www.mylescars.com/Bookings/call_preauth?bookingid=\(String(describing: (self?.rideHistoryDetailsObj.bookingid)!).toBase64())")
                    //}
                    }
                }
                
            }
            cell1.cancelBookingButton.didTouchUpInside = { [weak self] (sender) in
                let valueSelected: BookingStatus.RawValue = cell1.cancelBookingButton.title(for: .normal)!
                print(valueSelected)
                switch valueSelected {
                case BookingStatus.CANCEL.rawValue:
                    //Cancel booking Case
                    DispatchQueue.main.async {
                        self?.performSegue(withIdentifier: "cancelBookingSegue", sender: sender)
                    }
                    break
                case BookingStatus.CANCELLED.rawValue:
                    //do nothing
                    break
                case BookingStatus.INVOICE.rawValue:
                    //Do nothing
                    self?.openLinkController(urlString: (self?.rideHistoryDetailsObj.invoice)!,headingStr: "Invoice")
                    
                    break
                case BookingStatus.EXTEND.rawValue:
                    DispatchQueue.main.async {
                        self?.performSegue(withIdentifier: "showExtendedTimePicker", sender: sender)
                    }
                    break
                case BookingStatus.EXTENDEDBUTNOTPAID.rawValue:
                    //check which extension Value status is VALID
                    // and scroll uitableview to that cell
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: ((self?.totalNumberOfCell)! - 1), section: 0)
                        self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                    }
                    break
                default :
                    break
                }
                
                
            }
            
            //locationMapButton
            cell1.mapOpenButton.didTouchUpInside = { [weak self] (sender) in
                DispatchQueue.main.async {
                    UIApplication.shared.openURL(URL(string:"http://www.google.com/maps/place/\((self?.rideHistoryDetailsObj.lat)!),\((self?.rideHistoryDetailsObj.lon)!)")!)

//                    if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
//                        UIApplication.shared.openURL(URL(string:
//                            "comgooglemaps://?center=,-73.975866&zoom=14")!)
//                    } else {
//                        print("Can't use comgooglemaps://");
//                    }
                }
                
            }
            
            
            // DropdownButton
            cell1.viewCancellationPoilcyButton.didTouchUpInside = { [weak self] (sender) in
                
                cell1.cancellationLabel.sizeToFit()
                cell1.cancellationLabel.text = self?.rideHistoryDetailsObj.cancelPolicy
                    //self?.cancDesc
                self?.expandedLabel = cell1.cancellationLabel

                
                if (self?.localExpanded)! {
                    self?.localExpanded = false
                } else {
                    self?.localExpanded = true
                }
                
                cell1.cancellationLabel.alpha = 0.4
                UIView.animate(withDuration: 0.5, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
                    //cell1.cancellationTextView.alpha = 0
                    
                    if (self?.localExpanded)! {
                        //ImageView.transform = ImageView.transform.rotated(by: CGFloat(M_PI_2))
                        cell1.dropDownButton.transform = cell1.transform.rotated(by: CGFloat(Double.pi/2))
                            //CGAffineTransform(rotationAngle: CGFloat(Double.pi/4))
                    } else {
                        cell1.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                        
                    }
                    //New one
                    //self?.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                    //self?.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .none, animated: true)
                    // old one
                    self?.tableView.beginUpdates()
                    self?.tableView.endUpdates()
                    cell1.cancellationLabel.alpha = 0.8
//                    if (self?.localExpanded)! {
//                        cell1.cancellationLabel.alpha = 0.8
//                    } else {
//                        cell1.cancellationLabel.alpha = 0.2
//                    }
                },completion:({ _ in
                    if (self?.localExpanded)! {
                        cell1.cancellationLabel.alpha = 1
                    } else {
                        cell1.cancellationLabel.alpha = 0
                    }
                }))
            }
            return cell1
        case 1:
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "FareDetailCell", for: indexPath) as! FareDetailCell
            
            cell2.setUpDataForFareDetailCell(modelObj: rideHistoryDetailsObj)
            return cell2
        case 2:
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "BookingExtensionHeaderCell", for: indexPath) as! BookingExtensionHeaderCell
            return cell3
        case totalNumberOfCell - 1:
            let cell4 = tableView.dequeueReusableCell(withIdentifier: "BookingExtensionFooterCell", for: indexPath) as! BookingExtensionFooterCell
            cell4.extensionPolicyLabel.text = extensionPolicyDesc
            // DropdownButton
            cell4.viewExtnPolicyButton.didTouchUpInside = { [weak self] (sender) in
                
                cell4.extensionPolicyLabel.sizeToFit()
                cell4.extensionPolicyLabel.text = self?.extensionPolicyDesc
                self?.extensionExpandedLabel = cell4.extensionPolicyLabel
            
                
                if (self?.localExtensionExpanded)! {
                    self?.localExtensionExpanded = false
                } else {
                    self?.localExtensionExpanded = true
                }
                cell4.extensionPolicyLabel.alpha = 0.4
                UIView.animate(withDuration: 0.5, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
                    //cell1.cancellationTextView.alpha = 0
                    
                    if (self?.localExtensionExpanded)! {
                        //ImageView.transform = ImageView.transform.rotated(by: CGFloat(M_PI_2))
                        cell4.arrowButton.transform = cell4.transform.rotated(by: CGFloat(Double.pi/2))
                        //CGAffineTransform(rotationAngle: CGFloat(Double.pi/4))
                    } else {
                        cell4.arrowButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                        
                    }

                    //New one
                    //self?.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                    //self?.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .none, animated: true)
                    // old one
                    self?.tableView.beginUpdates()
                    self?.tableView.endUpdates()
                    cell4.extensionPolicyLabel.alpha = 0.8
                    //                    if (self?.localExpanded)! {
                    //                        cell1.cancellationLabel.alpha = 0.8
                    //                    } else {
                    //                        cell1.cancellationLabel.alpha = 0.2
                    //                    }
                },completion:({ _ in
                    if (self?.localExtensionExpanded)! {
                        cell4.extensionPolicyLabel.alpha = 1
                    } else {
                        cell4.extensionPolicyLabel.alpha = 0
                    }
                }))
            }
            return cell4
        default:
            //cell = nil
            let cell4 = tableView.dequeueReusableCell(withIdentifier: "BookingExtensionCell", for: indexPath) as! BookingExtensionCell
            let extensionDetail : ExtensionDetail = rideHistoryDetailsObj.extensionDetail.object(at: (indexPath.row - 3)) as! ExtensionDetail
            print("extensiondetail cell = \(extensionDetail.extensionAmount)")
            cell4.setUpDataForExtensionCell(modelObj: extensionDetail)
            if extensionDetail.extensionStatus == ExtensionStatus.VALID.rawValue {
                cell4.payExtensionButton.didTouchUpInside = { [weak self] (sender) in
                    DispatchQueue.main.async {
                        self?.performSegue(withIdentifier: "extensionPaymentSegueFromDetail", sender: extensionDetail.extensionLink)
                    }
                }
            }
            
            return cell4
        }
        
        cell.contentView.superview?.clipsToBounds = true
        //return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            print("heightForRowAt= \(localExpanded))")
            if (localExpanded) {
                print("heightForRowAt= \(expandedLabel!.frame.height))")
                return 340 + expandedLabel!.frame.height
            } else {
                print("heightForRowAtno= \(localExpanded))")
                return 340
            }
            

        case 1:
            if (rideHistoryDetailsObj.vatAmount.floatValue > 0 && rideHistoryDetailsObj.ldwAmount.floatValue > 0) {
                if rideHistoryDetailsObj.extensionDetail.count > 0 {
                    return 100 //330
                } else {
                    return 100 //300
                }
                //return 330//300
            } else if (rideHistoryDetailsObj.vatAmount.floatValue > 0 && rideHistoryDetailsObj.ldwAmount.floatValue <= 0){
                if rideHistoryDetailsObj.extensionDetail.count > 0 {
                    return  100 //310
                } else {
                    return 100 //280
                }
//                return 310//280
            } else {
                if rideHistoryDetailsObj.extensionDetail.count > 0 && rideHistoryDetailsObj.ldwAmount.floatValue > 0
                {
                    return 100 //360
                } else {
                    return 100 //330
                }
//                return 360//330
            }
            
        case 2:
            return 44
        case totalNumberOfCell - 1:
            if (localExtensionExpanded) {
                return 50 + extensionExpandedLabel.frame.height
            } else {
                return 50
            }
        default:
            return 134
//            if rideHistoryDetailsObj.extensionDetail.count == 1 {
//                return 155
//            } else {
//                return CGFloat(230 + (140 * (rideHistoryDetailsObj.extensionDetail.count - 1)))
//            }
        }
    }

    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 64
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView.init(frame: CGRect.zero)
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init(frame : CGRect.zero)
    }
    
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return 200
//    }
    
    
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        updateHeaderView()
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    //MARK: - BackProtocol
    
    func backFromTimerController(paymentLink: String) {
        print("paymentlink = \(paymentLink)")
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "extensionPaymentSegueFromDetail", sender: paymentLink)
        }
    }
    
    
    //MARK: - ExtensionPaymentProtocol
    
    func paymentSuccessOrFailure() {
        //self.refresh
        print("sourabh success")
        //Here you just have to reload the view by calling the api 
        if let bookingIdStr : String = rideHistoryDetailsObj.bookingid {
            getCompleteBookingData(bookingIdStr:bookingIdStr)
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showExtendedTimePicker" {
            if let destinationVC = segue.destination as? TimePickerController {
                destinationVC.dropOffDate = rideHistoryDetailsObj.dropoffDate
                destinationVC.dropOffTime = rideHistoryDetailsObj.dropoffTime
                destinationVC.bookingID = rideHistoryDetailsObj.bookingid
                destinationVC.backDelegate = self
                if (rideHistoryDetailsObj.extensionDetail.count > 0) {
                    destinationVC.extensionTaken = true
                } else {
                    destinationVC.extensionTaken = false
                }
            }
        } else if (segue.identifier == "cancelBookingSegue") {
            if let destinationVC = segue.destination as? CancellationPolicyVC {
                destinationVC.strBookingID = rideHistoryDetailsObj.bookingid
            }
        } else if (segue.identifier == "extensionPaymentSegueFromDetail") {
            if let destinationVC = segue.destination as? ExtensionPaymentController {
                destinationVC.extensionPaymentDelegate = self
                destinationVC.extensionPaymentURL = (sender as? String)!
            }
        }
    }
    
//    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
//            changeTabBar(hidden: true, animated: true)
//        }else{
//            changeTabBar(hidden: false, animated: true)
//        }
//        
//    }
//    
//    func changeTabBar(hidden:Bool, animated: Bool){
//        let tabBar = self.tabBarController?.tabBar
//        if tabBar!.isHidden == hidden{ return }
//        let frame = tabBar?.frame
//        let offset = (hidden ? (frame?.size.height)! : -(frame?.size.height)!)
//        let duration:TimeInterval = (animated ? 0.5 : 0.0)
//        tabBar?.isHidden = false
//        if frame != nil
//        {
//            UIView.animate(withDuration: duration,
//                           animations: {tabBar!.frame = frame!.offsetBy(dx: 0, dy: offset)},
//                           completion: {
//                            print($0)
//                            if $0 {tabBar?.isHidden = hidden}
//            })
//        }
//    }
    
    
}

extension String {
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
}

extension String {
    var floatValue: Float {
        return NSString(string: self).floatValue
    }
}


