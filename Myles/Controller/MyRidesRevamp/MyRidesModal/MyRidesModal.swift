//
//  MyRidesModal.swift
//  Myles
//
//  Created by iOS Team on 19/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation


struct AmenitiesDetails {
    let amount : String
    let detail : String
    let aminiti_id : String
}


struct MyRidesModal {
    let Aadhar : String
    //let AmenitiesDetails : [AmenitiesDetails]
    let BookingSource : String
    let Bookingid : String
    let CarModelName : String
    let Carcatname : String
    let DiscountAmount : String
    let DoorStepAddress : String
    let DropoffDate : String
    let DropoffTime : String
    //let ExtensionDetail : [ExtensionDetail]
    let Extracharge : String
    let HomePickDropAdd : String
    let Homeairportpickdropcharge : String
    let HomepickupService : String
    let LDWAmount : String
    let Lat : String
    let License : String
    let Lon : String
    let PaidAmount : String
    let Passport : String
    let PaymentAmount : String
    let PaymentStatus : String
    let PendingExtensionAmount : String
    let PickUpCityID : String
    let PickupDate : String
    let PickupTime : String
    let PreAuthAmount : String
    let PreAuthStatus : String
    let Rentalcharge : String
    let SeatingCapacity : String
    let SecurityAmount : String
    let SecurityRefundAmount : String
    let SecurityRefundStatus : String
    let Tripstatus : String
    let VatAmount : String
    let sublocationAdd : String
    let sublocationName : String
    let trackID : String

}

enum SerializationError: Error {
    case missing(String)
    case invalid(String, Any)
}


extension MyRidesModal {
    init(json: [String: Any]) throws {
        // Extract properties
        guard let aadhar = json["Aadhar"] as? String else {
            throw SerializationError.missing("Aadhar")
        }
        guard let bookingSource = json["BookingSource"] as? String else {
            throw SerializationError.missing("BookingSource")
        }
        guard let bookingId = json["Bookingid"] as? String else {
            throw SerializationError.missing("Bookingid")
        }
        guard let carModelName = json["CarModelName"] as? String else {
            throw SerializationError.missing("CarModelName")
        }
        guard let carCatName = json["Carcatname"] as? String else {
            throw SerializationError.missing("Carcatname")
        }
        
        guard let discountAmount = json["DiscountAmount"] as? String else {
            throw SerializationError.missing("DiscountAmount")
        }
        guard let doorStepAddress = json["DoorStepAddress"] as? String else {
            throw SerializationError.missing("DoorStepAddress")
        }
        guard let dropOffDate = json["DropoffDate"] as? String else {
            throw SerializationError.missing("DropoffDate")
        }
        guard let dropOffTime = json["DropoffTime"] as? String else {
            throw SerializationError.missing("DropoffTime")
        }
        guard let extraCharge = json["Extracharge"] as? String else {
            throw SerializationError.missing("Extracharge")
        }
        guard let homePickDropAdd = json["HomePickDropAdd"] as? String else {
            throw SerializationError.missing("HomePickDropAdd")
        }
        guard let homeAirPortPickDropCharge = json["Homeairportpickdropcharge"] as? String else {
            throw SerializationError.missing("Homeairportpickdropcharge")
        }
        guard let homePickUpService = json["HomepickupService"] as? String else {
            throw SerializationError.missing("HomepickupService")
        }
        guard let ldwAmount = json["LDWAmount"] as? String else {
            throw SerializationError.missing("LDWAmount")
        }
        guard let license = json["License"] as? String else {
            throw SerializationError.missing("License")
        }
        guard let lon = json["Lon"] as? String else {
            throw SerializationError.missing("Lon")
        }
        
        guard let lat = json["Lat"] as? String else {
            throw SerializationError.missing("Lat")
        }
        guard let paidAmount = json["PaidAmount"] as? String else {
            throw SerializationError.missing("PaidAmount")
        }
        guard let passport = json["Passport"] as? String else {
            throw SerializationError.missing("Passport")
        }
        guard let paymentAmount = json["PaymentAmount"] as? String else {
            throw SerializationError.missing("PaymentAmount")
        }
        guard let paymentStatus = json["PaymentStatus"] as? String else {
            throw SerializationError.missing("PaymentStatus")
        }
        guard let pendingExtensionAmount = json["PendingExtensionAmount"] as? String else {
            throw SerializationError.missing("PendingExtensionAmount")
        }
            
            guard let pickUpCityID = json["PickUpCityID"] as? String else {
                throw SerializationError.missing("PickUpCityID")
            }
            guard let pickUpDate = json["PickupDate"] as? String else {
                throw SerializationError.missing("PickupDate")
            }
            guard let pickUpTime = json["PickupTime"] as? String else {
                throw SerializationError.missing("PickupTime")
            }
            guard let preAuthAmount = json["PreAuthAmount"] as? String else {
                throw SerializationError.missing("PreAuthAmount")
            }
            guard let preAuthStatus = json["PreAuthStatus"] as? String else {
                throw SerializationError.missing("PreAuthStatus")
            }
            guard let rentalCharge = json["Rentalcharge"] as? String else {
                throw SerializationError.missing("Rentalcharge")
            }
            guard let seatingCapacity = json["SeatingCapacity"] as? String else {
                throw SerializationError.missing("SeatingCapacity")
            }
            guard let securityAmount = json["SecurityAmount"] as? String else {
                throw SerializationError.missing("SecurityAmount")
            }
            guard let securityRefundAmount = json["SecurityRefundAmount"] as? String else {
                throw SerializationError.missing("SecurityRefundAmount")
            }
            guard let securityRefundStatus = json["SecurityRefundStatus"] as? String else {
                throw SerializationError.missing("SecurityRefundStatus")
            }
            guard let tripstatus = json["Tripstatus"] as? String else {
                throw SerializationError.missing("Tripstatus")
            }
            guard let vatAmount = json["VatAmount"] as? String else {
                throw SerializationError.missing("VatAmount")
            }
            guard let subLocationAdd = json["sublocationAdd"] as? String else {
                throw SerializationError.missing("sublocationAdd")
            }
            guard let sublocationName = json["sublocationName"] as? String else {
                throw SerializationError.missing("sublocationName")
            }
            guard let trackID = json["trackID"] as? String else {
                throw SerializationError.missing("trackID")
            }
            
//            guard let amenitiesDetailJson = json["AmenitiesDetails"] as? [AmenitiesDetails] else {
//                throw SerializationError.missing("AmenitiesDetails")
//            }
        
//            for amenitiesObj : AmenitiesDetails in amenitiesDetailJson {
//                let localAmenitiesObj = AmenitiesDetails()
//                localAmenitiesObj.amou
//            }
        
//        // Extract and validate meals
//        guard let mealsJSON = json["meals"] as? [String] else {
//            throw SerializationError.missing("meals")
//        }
//        
//        var meals: Set<Meal> = []
//        for string in mealsJSON {
//            guard let meal = Meal(rawValue: string) else {
//                throw SerializationError.invalid("meals", string)
//            }
//            
//            meals.insert(meal)
//        }
        
        // Initialize properties
        self.Aadhar = aadhar
        self.BookingSource = bookingSource
        self.Bookingid = bookingId
        self.CarModelName = carModelName
        self.Carcatname = carCatName
        self.DiscountAmount = discountAmount
        self.DoorStepAddress = doorStepAddress
        self.DropoffDate = dropOffDate
        self.DropoffTime = dropOffTime
        self.Extracharge = extraCharge
        self.HomePickDropAdd = homePickDropAdd
        self.Homeairportpickdropcharge = homeAirPortPickDropCharge
        self.HomepickupService = homePickUpService
        self.LDWAmount = ldwAmount
        self.Lat = lat
        self.License = license
        self.Lon = lon
        self.PaidAmount = paidAmount
        self.Passport = passport
        self.PaymentAmount = paymentAmount
        self.PaymentStatus = paymentStatus
        self.PendingExtensionAmount = pendingExtensionAmount
        self.PickUpCityID = pickUpCityID
        self.PickupDate = pickUpDate
        self.PickupTime = pickUpTime
        self.PreAuthAmount = preAuthAmount
        self.PreAuthStatus = preAuthStatus
        self.Rentalcharge = rentalCharge
        self.SeatingCapacity = seatingCapacity
        self.SecurityAmount = securityAmount
        self.SecurityRefundAmount = securityRefundAmount
        self.SecurityRefundStatus = securityRefundStatus
        self.Tripstatus = tripstatus
        self.VatAmount = vatAmount
        self.sublocationAdd = subLocationAdd
        self.sublocationName = sublocationName
        self.trackID = trackID
        
            
    }
    
    
}


