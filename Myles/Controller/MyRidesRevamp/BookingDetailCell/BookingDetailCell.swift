//
//  BookingDetailCell.swift
//  Myles
//
//  Created by iOS Team on 13/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

enum BookingStatus: String {
    case CANCELLED = "Booking Cancelled"
    case CANCEL    = "Cancel Booking"
    case EXTEND    = "Extend booking"
    case INVOICE   = "Invoice"
    case DAMAGE_INVOICE = "Damage Invoice"
    case LDW_CERTIFICATE = "LDW Certificate"
    case EXTENDEDBUTNOTPAID = "Pay Extension"
    case LDW_WITH_SECURITY = "LDW With Security"

}

class BookingDetailCell: UITableViewCell {

    //IBOutlets
    @IBOutlet weak var carNameWithBookingIDLabel: UILabel!
    
    @IBOutlet weak var mainContentView: UIView!
    
    @IBOutlet weak var pickUpView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var dropOffView: UIView!
    @IBOutlet weak var pickUpTimeLabel: UILabel!
    @IBOutlet weak var dropOffDayLabel: UILabel!
    @IBOutlet weak var dropOffTimeLabel: UILabel!
    @IBOutlet weak var dropOffDateLabel: UILabel!
    
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var locationMapButton: Button!
    
    @IBOutlet weak var securityDepositAmountLabel: UILabel!
    @IBOutlet weak var paySecurityOrLDWbutton: Button!
    
    @IBOutlet weak var cancelBookingButton: Button!
    @IBOutlet weak var viewCancellationPoilcyButton: Button!
    
    @IBOutlet weak var cancellationTextView: UITextView!
    
    @IBOutlet weak var cancellationLabel: UILabel!
    
    //Height constraint
    @IBOutlet weak var mainViewContraint: NSLayoutConstraint!
    
    @IBOutlet weak var cancellationHeightConstraint: NSLayoutConstraint!
    
    //@IBOutlet weak var dropDownButton: UIImageView!
    @IBOutlet weak var dropDownButton: Button!
    
    @IBOutlet weak var mapOpenButton: Button!
    @IBOutlet weak var mylesSecureBtn: UIButton!
    @IBOutlet weak var mylesCertiImageView: UIImageView!

    //    var isExpandable : Bool = false {
    //        didSet {
//            if !isExpandable {
//                self.cancellationHeightConstraint.constant = 2
//                
//            } else {
//                self.cancellationHeightConstraint.constant = 160
//
//            }
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainContentView.layer.cornerRadius = 4
        cancellationLabel.alpha = 0
        paySecurityOrLDWbutton.layer.cornerRadius = 3
        //paySecurityOrLDWbutton.layer.borderColor = UIColor(rgb : BUTTON_BORDER_COLOUR).cgColor
        //paySecurityOrLDWbutton.layer.borderWidth = 1
        paySecurityOrLDWbutton.backgroundColor = UIColor(rgb : BUTTON_BORDER_COLOUR)
        cancelBookingButton.layer.cornerRadius = 3
        //cancelBookingButton.layer.borderColor = UIColor(rgb : BUTTON_BORDER_COLOUR).cgColor
        cancelBookingButton.backgroundColor = UIColor(rgb : BUTTON_BORDER_COLOUR)
        //cancelBookingButton.layer.borderWidth = 1
        
        //cancellationTextView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func dropDownButtonTapped(_ sender: Any) {
                UIView.animate(withDuration: 1.0, animations: {
                    self.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
                })

    }
    
    func setUpDataForBookingCell(modelObj : RideHistoryDetails, withSegment:String) {
        carNameWithBookingIDLabel.text = modelObj.carcatname + " " + modelObj.carModelName
        
        
        if (modelObj.homepickupService.boolValue) {
            locationNameLabel.text = modelObj.homePickDropAdd
        } else {
            if modelObj.sublocationAdd.characters.count > 0 {
                locationNameLabel.text = modelObj.sublocationAdd
            } else {
                locationNameLabel.text = modelObj.sublocationName
            }
        }
        
       
        
//        DispatchQueue.main.async {
        if (modelObj.ldwAmount.floatValue > 0) {
            //self.cancelBookingButton.setTitle("No security deposit", for: .normal)
            self.securityDepositAmountLabel.text = "No security deposit"
            self.paySecurityOrLDWbutton.setTitle(BookingStatus.LDW_CERTIFICATE.rawValue, for:.normal)
        } else {
            let security = modelObj.preAuthAmount!
            let securityFAmout  = Float(security)
            let SecurityAmount = Int(securityFAmout!)
            if (withSegment == String(describing: selectedSegment.U) || withSegment == String(describing: selectedSegment.L)) {
               
                if (modelObj.preAuthStatus.boolValue) {
                    self.securityDepositAmountLabel.text = "Security Deposit: Rs. \(String(describing: SecurityAmount)) | Paid"
                    //self.paySecurityOrLDWbutton.setTitle("Paid :Rs. \(String(describing: SecurityAmount))", for: .normal) // chetan
                    self.paySecurityOrLDWbutton.setTitle("Security Paid", for: .normal)
                    self.paySecurityOrLDWbutton.backgroundColor = UIColor.lightGray

                    self.paySecurityOrLDWbutton.isEnabled = false
                } else {
//                    self.securityDepositAmountLabel.text = "Security Deposit : Rs. \(String(describing: SecurityAmount))"
//                    self.paySecurityOrLDWbutton.setTitle("Pay :Rs. \(String(describing: SecurityAmount))", for: .normal)
                    
                    self.securityDepositAmountLabel.text = "Security Deposit : Rs. \(String(describing: SecurityAmount)) | Not Paid"
                    self.paySecurityOrLDWbutton.setTitle("Pay Security", for: .normal)
                    paySecurityOrLDWbutton.backgroundColor = UIColor(rgb : BUTTON_BORDER_COLOUR)
                    self.paySecurityOrLDWbutton.isEnabled = true
                }
            } else {
                if (modelObj.preAuthStatus.boolValue) {
                    
                    //iphone 5
                    if UIScreen.main.bounds.size.height == 568.0 {
                        // your code here
                        self.securityDepositAmountLabel.font = UIFont.systemFont(ofSize:11)

                    } else {
                        self.securityDepositAmountLabel.font = UIFont.systemFont(ofSize:14)

                    }
                    self.securityDepositAmountLabel.sizeToFit()
                    
                    if (modelObj.securityRefundAmount.floatValue > 0) {
                        self.securityDepositAmountLabel.text = "Security Deposit: Rs.\(getConvertedString(value: modelObj.preAuthAmount!)) | Refund Amount: Rs.\(getConvertedString(value: modelObj.securityRefundAmount!))"
                    } else {
                        self.securityDepositAmountLabel.text = "Security Deposit: Rs.\(getConvertedString(value: modelObj.preAuthAmount!))"
                    }
                   
                    self.paySecurityOrLDWbutton.isHidden = true

                    self.paySecurityOrLDWbutton.isEnabled = false

                } else {
                    self.securityDepositAmountLabel.text = "Security Deposit : Rs. \(modelObj.preAuthAmount!)"
                    self.paySecurityOrLDWbutton.setTitle("Security Not Paid", for: .normal)
                    self.paySecurityOrLDWbutton.isEnabled = false
                }
            }
        }
//        }

        
        if (modelObj.bookingType == "I")
        {
            mylesSecureBtn.isHidden = false
            mylesCertiImageView.isHidden = false

            if (modelObj.preAuthStatus  == "1" && modelObj.preAuthAmount.floatValue > 0)
            {
                print("Paid Rs: ",modelObj.preAuthAmount)
                //paySecurityOrLDWbutton.setTitle(String(format: "Paid: Rs. \(modelObj.preAuthAmount!)"), for: UIControlState.normal)
                paySecurityOrLDWbutton.setTitle(String(format: "Security Paid"), for: UIControl.State.normal)
                paySecurityOrLDWbutton.backgroundColor = UIColor.lightGray
                self.securityDepositAmountLabel.text = (String(format: "Security Deposit: Rs. \(modelObj.preAuthAmount!) | Paid"))
                paySecurityOrLDWbutton.isUserInteractionEnabled = false
            }
            else
            {
                print("Pay Rs: ",modelObj.preAuthAmount)
               // paySecurityOrLDWbutton.setTitle(String(format: "Pay: Rs. \(modelObj.preAuthAmount!)"), for: UIControlState.normal)
                paySecurityOrLDWbutton.setTitle(String(format: "Pay Security"), for: UIControl.State.normal)
                paySecurityOrLDWbutton.backgroundColor = UIColor(rgb : BUTTON_BORDER_COLOUR)
                self.securityDepositAmountLabel.text = (String(format: "Security Deposit: Rs. \(modelObj.preAuthAmount!) | Not Paid"))
                paySecurityOrLDWbutton.isUserInteractionEnabled = true

                if (modelObj.preAuthAmount.floatValue <= 0.00 )
                {
                    paySecurityOrLDWbutton.setTitle(String(format: "No Security"), for: UIControl.State.normal)
                    paySecurityOrLDWbutton.backgroundColor = UIColor.lightGray
                    self.securityDepositAmountLabel.text = (String(format: "No Security Needed"))
                    paySecurityOrLDWbutton.isUserInteractionEnabled = false

                }
                
                

            }
        }
        else
        {
            mylesSecureBtn.isHidden = true
            mylesCertiImageView.isHidden = true
        }
        
        
        
        if (withSegment == String(describing: selectedSegment.L)) {
            if modelObj.pendingExtensionAmount.floatValue > 0 {
                paySecurityOrLDWbutton.isEnabled = true
                self.cancelBookingButton.setTitle(BookingStatus.EXTENDEDBUTNOTPAID.rawValue, for: .normal)
            } else {
                self.cancelBookingButton.setTitle(BookingStatus.EXTEND.rawValue, for: .normal)
            }
        } else if (withSegment == String(describing: selectedSegment.U)) {
            paySecurityOrLDWbutton.isEnabled = true
            self.cancelBookingButton.setTitle(BookingStatus.CANCEL.rawValue, for: .normal)
        } else if (withSegment == String(describing: selectedSegment.C)) {
            if modelObj.damageInvoice.characters.count > 0 {
                paySecurityOrLDWbutton.setTitle(BookingStatus.DAMAGE_INVOICE.rawValue, for: .normal)
                paySecurityOrLDWbutton.isEnabled = true
                securityDepositAmountLabel.text = "LDW Certificate"
                
            } else {
                if modelObj.ldwAmount.floatValue > 0 {
                    paySecurityOrLDWbutton.isEnabled = true
                } else {
                    paySecurityOrLDWbutton.isEnabled = false
                }
            }
            if modelObj.invoice.characters.count > 0 {
                self.cancelBookingButton.setTitle(BookingStatus.INVOICE.rawValue, for: .normal)

            } else {
                self.cancelBookingButton.setTitle(BookingStatus.CANCELLED.rawValue, for: .normal)
            }
            
        }
        
        //Pick - Up
        dayLabel.text = Helper.getDayOfWeek(modelObj.pickupDate)
        dateLabel.text = Helper.getDateWithMonth(dateString: modelObj.pickupDate)
        pickUpTimeLabel.text = Helper.getTimeInAMPM(timeString: modelObj.pickupTime)
        //Drop - off
        dropOffDayLabel.text = Helper.getDayOfWeek(modelObj.dropoffDate)
        dropOffDateLabel.text = Helper.getDateWithMonth(dateString: modelObj.dropoffDate)
        dropOffTimeLabel.text = Helper.getTimeInAMPM(timeString: modelObj.dropoffTime)

    }
    
    func getConvertedString(value : String) -> String {
        let convertedString = Int(value.floatValue)
        return "\(String(describing: convertedString))"
    }
    
    
}


//extension UITextView {
//    func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
//        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
//            self.alpha = 1.0
//        }, completion: completion)  }
//    
//    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
//        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
//            self.alpha = 0.0
//        }, completion: completion)
//    }
//}

