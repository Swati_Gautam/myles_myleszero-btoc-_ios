//
//  MyRidesHistoryCell.swift
//  Myles
//
//  Created by iOS Team on 20/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

class MyRidesHistoryCell: UITableViewCell {

    //IBoutlets
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var bookingIDLabel: UILabel!
    @IBOutlet weak var carPickUpTimeLabel: UILabel!
    @IBOutlet weak var carDropOffTimeLabel: UILabel!
    
    @IBOutlet weak var carPickUpDateView: UIView!
    @IBOutlet weak var carPickUpDateWithMonthLabel: UILabel!
    @IBOutlet weak var carPickUpDayLabel: UILabel!
    
    @IBOutlet weak var carDropOffDateView: UIView!
    @IBOutlet weak var carDropOffDateWithMonthLabel: UILabel!
    
    @IBOutlet weak var carDropOffDayLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
