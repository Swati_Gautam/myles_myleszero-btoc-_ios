//
//  BookingExtensionHeaderCell.swift
//  Myles
//
//  Created by iOS Team on 25/07/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

class BookingExtensionHeaderCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        mainView.layer.cornerRadius = 4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
