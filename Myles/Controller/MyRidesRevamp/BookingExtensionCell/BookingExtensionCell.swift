//
//  BookingExtensionCell.swift
//  Myles
//
//  Created by iOS Team on 14/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

let BUTTON_BORDER_COLOUR = UInt(0xDF4627)

import UIKit

class BookingExtensionCell: UITableViewCell {
    
    //IBOutlets

    @IBOutlet weak var extensionStartTime: UILabel!
    
    @IBOutlet weak var extensionEndTime: UILabel!
    
    @IBOutlet weak var extensionMainView: UIView!
    @IBOutlet weak var payExtensionButton: Button!

    @IBOutlet weak var viewExtensionButtonClicked: Button!
    @IBOutlet weak var extensionRequestTimeLabel: UILabel!
    
    @IBOutlet weak var extensionStartView: UIView!
    @IBOutlet weak var extensionStartDayLabel: UILabel!
    @IBOutlet weak var extensionStartDateAndMonthLabel: UILabel!
    
    @IBOutlet weak var dynamicExtensionView: UIView!
    
    
    
    @IBOutlet weak var extensionEndView: UIView!
    @IBOutlet weak var extensionEndDayLabel: UILabel!
    @IBOutlet weak var extensionEndDateAndMonthLabel: UILabel!
    
    
    //New
    
    
    //https://spin.atomicobject.com/2016/02/25/uistackview-stretch-content/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //extensionMainView.layer.cornerRadius = 4
        payExtensionButton.layer.cornerRadius = 3
        //payExtensionButton.layer.borderWidth = 1
        //payExtensionButton.layer.borderColor = UIColor(rgb : BUTTON_BORDER_COLOUR).cgColor
        
    }
    
//    fileprivate func scrollToEnd(_ addedView: UIView) {
//        let contentViewHeight = stackScrollView.contentSize.height + addedView.bounds.height + dynamicStackView.spacing
//        let offsetY = contentViewHeight - stackScrollView.bounds.height
//        if (offsetY > 0) {
//            stackScrollView.setContentOffset(CGPoint(x: stackScrollView.contentOffset.x, y: offsetY), animated: true)
//        }
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: - Internal Functions
    func setUpDataForExtensionCell(modelObj : ExtensionDetail) {
        extensionStartDayLabel.text = Helper.getDayOfWeek(modelObj.extensionFromDate)
        extensionStartDateAndMonthLabel.text = Helper.getDateWithMonth(dateString: modelObj.extensionFromDate)
        extensionStartTime.text = Helper.getTimeInAMPM(timeString: modelObj.extensionFromTime)
        
        extensionEndDayLabel.text = Helper.getDayOfWeek(modelObj.extensionDate)
        extensionEndDateAndMonthLabel.text = Helper.getDateWithMonth(dateString: modelObj.extensionDate)
        extensionEndTime.text = Helper.getTimeInAMPM(timeString: modelObj.extensionTime)
        extensionRequestTimeLabel.text = "Extension request time : \(modelObj.requestDateForExtension!)  \(Helper.getTimeInAMPM(timeString: modelObj.requestTimeForExtension))"
        
        if (modelObj.extensionStatus == ExtensionStatus.VALID.rawValue) {
            payExtensionButton.setTitle("Pay Extension Charges ₹:\(getConvertedString(value:(modelObj.extensionAmount!)))", for: .normal)
            payExtensionButton.backgroundColor = UIColor(rgb : BUTTON_BORDER_COLOUR)
            payExtensionButton.setTitleColor( UIColor.white, for: .normal)
        } else if (modelObj.extensionStatus == ExtensionStatus.EXPIRED.rawValue) {
            payExtensionButton.setTitleColor( UIColor.darkGray, for: .normal)
            payExtensionButton.setTitle("Payment Extension Link Expired", for: .normal)

        } else if (modelObj.extensionStatus == ExtensionStatus.PAID.rawValue) {
            payExtensionButton.setTitleColor( UIColor.darkGray, for: .normal)

            payExtensionButton.setTitle("Paid ₹: \(modelObj.extensionAmount!)", for: .normal)

        }
        
        
    }

    fileprivate func getConvertedString(value : String) -> String {
        let convertedString = Int(value.floatValue)
        return "\(String(describing: convertedString))"
    }
    
    
}

enum ExtensionStatus: String {
    case VALID = "Valid"
    case EXPIRED  = "Expired"
    case PAID    = "Paid"
}


extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

