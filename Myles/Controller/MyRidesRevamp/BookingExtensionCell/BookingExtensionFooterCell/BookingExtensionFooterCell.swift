//
//  BookingExtensionFooterCell.swift
//  Myles
//
//  Created by iOS Team on 25/07/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

class BookingExtensionFooterCell: UITableViewCell {

    @IBOutlet weak var mainBackView: UIView!
    
    @IBOutlet weak var viewExtnPolicyButton: Button!
    
    @IBOutlet weak var extensionPolicyLabel: UILabel!
    
    @IBOutlet weak var arrowButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainBackView.layer.cornerRadius = 4
        extensionPolicyLabel.alpha = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
