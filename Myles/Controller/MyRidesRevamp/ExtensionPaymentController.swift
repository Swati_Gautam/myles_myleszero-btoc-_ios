//
//  ExtensionPaymentController.swift
//  Myles
//
//  Created by iOS Team on 20/07/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD
import WebKit

protocol ExtensionPaymentProtocol  {
    func paymentSuccessOrFailure()
}

class ExtensionPaymentController: UIViewController, WKNavigationDelegate { //UIWebViewDelegate {

    var extensionPaymentDelegate : ExtensionPaymentProtocol!
    
    var extensionPaymentURL : String!
    
    //IBOUTLETS
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBOutlet weak var progressBar: UIProgressView!
    //@IBOutlet weak var paymentWebview: UIWebView!
    var paymentWebview: WKWebView!
    var myTimer:Timer!
    var isComplete = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        progressBar.progress = 0.0
        showLoader()
        // Do any additional setup after loading the view.
        // load uiwebview
        //http://www.mylescars.com/extensions/extension_payment?extensionid=Q09SSUMxNTAwNDY2Njk4NTM3RXgwMQ==
        //payment after filling details
        //https://sandbox.juspay.in/vbv/mc_secure
        
        //after password 1221
        //https://sandbox.juspay.in/vbv/checkVisaCode
        
        //after successful payment
        //https://qa2.mylescars.com/extensions/extension_thanks
        
        paymentWebview = WKWebView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 20, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(paymentWebview)
        
        var newURLString = ""
        #if DEBUG
            //sourabh staging change
           newURLString = extensionPaymentURL.replacingOccurrences(of: "www", with: "qa1") //qa2
           // newURLString = extensionPaymentURL.replacingOccurrences(of: "www", with: "staging")

        #else
            newURLString = extensionPaymentURL
        #endif
        
        print("Extension URL: %@", newURLString)

        //Load webview
        paymentWebview.load(URLRequest(url: URL(string : newURLString)!))

        //paymentWebview.loadRequest(URLRequest(url: extensionPaymentURL))
        paymentWebview.navigationDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
    }
    override func viewDidDisappear(_ animated: Bool) {
        if(myTimer != nil){
            myTimer.invalidate()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    //MARK: - Internal function
    
    func showLoader () {
        /*SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")*/
        if(myTimer != nil){
            myTimer.invalidate()
        }
        progressBar.progress = 0.0
        isComplete = false
        progressBar.isHidden = false
        myTimer =  Timer.scheduledTimer(timeInterval: 0.01667,target: self,selector: #selector(ExtensionPaymentController.timerCallback),userInfo: nil,repeats: true)
    }
    @objc func timerCallback(){
        if isComplete {
            if progressBar.progress >= 1 {
                progressBar.isHidden = true
                myTimer.invalidate()
            }else{
                progressBar.progress += 0.1
            }
        }else{
            progressBar.isHidden = false
            progressBar.progress += 0.05
            if progressBar.progress >= 0.95 {
                progressBar.progress = 0.95
            }
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func showAlert(message:String)  {
        let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!)  in
            
        })

        // let defaultAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }

    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.extensionPaymentDelegate.paymentSuccessOrFailure()
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!)
    {
        debugPrint("didCommit")
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
    /*func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
     debugPrint("didFinish")
     }*/
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        print("Webview fail with error \(error)")
    }
    
    
    func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        isComplete = true
        print("Webview did finish load")
        if (webView.isLoading){
            return
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        
        print("webView:\(webView) decidePolicyForNavigationAction:\(navigationAction) decisionHandler:\(decisionHandler)")
        
        print("URL loading = \(String(describing: navigationAction.request.url))")
        
        if (navigationAction.request.url?.lastPathComponent == "extension_thanks") {
            DispatchQueue.main.async {
                //code
                self.extensionPaymentDelegate.paymentSuccessOrFailure()
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
        } else if (((navigationAction.request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("checkVisaCode").lowercased())) != nil {
            print("checkVisaCode done")
            DispatchQueue.main.async {
                self.showLoader()
            }
        } else if (navigationAction.request.url?.lastPathComponent == "thanks") {
            //Security payment
            self.extensionPaymentDelegate.paymentSuccessOrFailure()
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        } else if (navigationAction.request.url?.lastPathComponent == "booking_fail") {
            self.extensionPaymentDelegate.paymentSuccessOrFailure()
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        decisionHandler(.allow)
    }
    

    //MARK: - UIWebview Delegate
    
    /*func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("Webview fail with error \(error)");
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        print("URL loading = \(String(describing: request.url))")
        
        if (request.url?.lastPathComponent == "extension_thanks") {
            DispatchQueue.main.async {
                //code
                self.extensionPaymentDelegate.paymentSuccessOrFailure()
                self.dismiss(animated: true, completion: nil)
            }
        } else if (((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: ("checkVisaCode").lowercased())) != nil {
            print("checkVisaCode done")
            DispatchQueue.main.async {
                self.showLoader()
            }
        } else if (request.url?.lastPathComponent == "thanks") {
            //Security payment
            self.extensionPaymentDelegate.paymentSuccessOrFailure()
            self.dismiss(animated: true, completion: nil)
        } else if (request.url?.lastPathComponent == "booking_fail") {
            self.extensionPaymentDelegate.paymentSuccessOrFailure()
            self.dismiss(animated: true, completion: nil)
        }
        
        //(((request as NSURLRequest).url?.absoluteString)?.lowercased().range(of: (strMainUrl+"thanks").lowercased())) != nil
        return true;
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("Webview started Loading")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        isComplete = true
        print("Webview did finish load")
        if (webView.isLoading){
            return
        }
        
        //SVProgressHUD.dismiss()
    }*/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
