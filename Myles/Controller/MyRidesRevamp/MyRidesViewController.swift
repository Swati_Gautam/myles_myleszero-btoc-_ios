//
//  MyRidesViewController.swift
//  Myles
//
//  Created by iOS Team on 01/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD

enum calenderDay : Int {
    case Sun = 1,Mon,Tue,Wed,Thu,Fri,Sat
}

enum selectedSegment : String {
    case L,U,C
}

enum ComingAfterCompletingModule: String {
    case Normal = "Normal"
    case FromJuspayPayment = "FromJuspay"
    case FromExtensionPayment = "FromExtension"
}

enum calendarMonth : Int {
    case Jan = 1,Feb,Mar,Apr,May,June,July,Aug,Sept,Oct,Nov,Dec
}

class MyRidesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @objc var comingFromCases : String!
    @objc var bookingIDFromJuspayPage : String!
    //IBOUTLETS
    @IBOutlet weak var ridesTableView: UITableView!
    
    @IBOutlet weak var ridesSegmentedControl: UISegmentedControl!
    var lastSegmentedControlTapped : String = "L"

    @IBOutlet weak var nonLoginUserView: UIView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var searchNowButton: UIButton!
    
    fileprivate let formatter  = DateFormatter()

    
    var myRidesOngoingModelArr : [RideHistoryDetails] = []
    var myRidesUpcomingModelArr : [RideHistoryDetails] = []
    var myRidesHistoryModelArr : [RideHistoryDetails] = []

    var headerValues = Dictionary<String, Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.hidesBarsOnSwipe = true
        
        if (self.view.frame.size.width > 320) {
            ridesSegmentedControl.frame.size.width = 343
        } else {
            ridesSegmentedControl.frame.size.width = 290;
        }

        ridesSegmentedControl.tintColor = UIColor(rgb:BUTTON_BORDER_COLOUR)
        formatter.dateFormat = "MM-dd-yyyy"//"yyyy-MM-dd"
        print(calenderDay(rawValue: 2)!)

        signInButton.layer.cornerRadius = 4
        signInButton.layer.borderWidth = 1
        //signInButton.layer.border borderColor = UIColor.black as! CGColor
        
        searchNowButton.layer.cornerRadius = 4
        searchNowButton.layer.borderWidth = 1
        //searchNowButton.layer.borderColor = UIColor.black as! CGColor
        // Do any additional setup after loading the view.
        // For the extended navigation bar effect to work, a few changes
        // must be made to the actual navigation bar.  Some of these changes could
        // be applied in the storyboard but are made in code for clarity.
        
        // Translucency of the navigation bar is disabled so that it matches with
        // the non-translucent background of the extension view.
        //navigationController!.navigationBar.isTranslucent = false
        // The navigation bar's shadowImage is set to a transparent image.  In
        // addition to providing a custom background image, this removes
        // the grey hairline at the bottom of the navigation bar.  The
        // ExtendedNavBarView will draw its own hairline.
        //navigationController!.navigationBar.shadowImage = #imageLiteral(resourceName: "TransparentPixel")
        // "Pixel" is a solid white 1x1 image.
        //navigationController!.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "Pixel"), for: .default)
        
        //print("abcdddd=\(getDayOfWeek("06-25-2017")!)")
//        print("\(getTimeInAMPM(timeString: "0930"))")
        
        print("working on viewDidLoad")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        // Check Whether user is login or not
        if UserDefaults.standard.bool(forKey: IS_LOGIN) {
            nonLoginUserView.isHidden = true
            
            print("comingFromCases1 = \(comingFromCases)")
            if comingFromCases != nil {
                if comingFromCases == ComingAfterCompletingModule.Normal.rawValue {
                    showLoader()
                    getMyRidesFromAPI(typeOfRide: lastSegmentedControlTapped)
                    
                } else if (comingFromCases == ComingAfterCompletingModule.FromJuspayPayment.rawValue){
                    lastSegmentedControlTapped = "U"
                    ridesSegmentedControl.selectedSegmentIndex = 1;
                    self.performSegue(withIdentifier: "showBookingDetails", sender: self)
                    
                } else if (comingFromCases == ComingAfterCompletingModule.FromExtensionPayment.rawValue) {
                    lastSegmentedControlTapped = "L"
                }
            } else {
                print("comingFromCases2 = \(comingFromCases)")
                comingFromCases = ComingAfterCompletingModule.Normal.rawValue
                showLoader()
                switch lastSegmentedControlTapped {
                case selectedSegment.L.rawValue:
                    ridesSegmentedControl.selectedSegmentIndex = 0
                case selectedSegment.U.rawValue:
                    ridesSegmentedControl.selectedSegmentIndex = 1
                case selectedSegment.C.rawValue :
                    ridesSegmentedControl.selectedSegmentIndex = 2
                default:
                    break
                }
                getMyRidesFromAPI(typeOfRide: lastSegmentedControlTapped)
            }
            print("comingFromCases3 = \(comingFromCases)")
            
            
        } else {
            comingFromCases = ComingAfterCompletingModule.Normal.rawValue
            nonLoginUserView.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func ridesSegementTapped(_ sender: Any) {
//        [_tableView setContentOffset:_tableView.contentOffset animated:NO];
        ridesTableView.setContentOffset(ridesTableView.contentOffset, animated: false)
        showLoader()
        switch ridesSegmentedControl.selectedSegmentIndex {
        case 0:
            lastSegmentedControlTapped = "L"
            //myRidesOngoingModelArr.removeAll()
            getMyRidesFromAPI(typeOfRide: "L")
        case 1:
            lastSegmentedControlTapped = "U"
            //myRidesUpcomingModelArr.removeAll()
            getMyRidesFromAPI(typeOfRide: "U")
        case 2:
            lastSegmentedControlTapped = "C"
            //myRidesHistoryModelArr.removeAll()
            getMyRidesFromAPI(typeOfRide: "C")
        default:
            getMyRidesFromAPI(typeOfRide: "L")
        }

    }
    
    // MARK: - Internal Fucntion

    func showLoader () {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
            SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
            SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
            SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
            SVProgressHUD.show(withStatus: "Please wait")
        }
        
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }

    
    //MARK: - Calling Data from API's
    func getMyRidesFromAPI(typeOfRide : String) {
        
        //public func SearchCarApi (completionHandler: @escaping (MylesCarDetailsResponse?,Bool) ->Void) {
        switch ridesSegmentedControl.selectedSegmentIndex {
        case 0:
            if (self.myRidesOngoingModelArr.count > 0)
            {
                //ridesTableView.isHidden = true
                self.myRidesOngoingModelArr.removeAll()
                //self.ridesTableView.isHidden = true
                //self.ridesTableView.reloadData()
            }
            
        case 1:
            if (self.myRidesUpcomingModelArr.count > 0)
            {
                //ridesTableView.isHidden = true
                self.myRidesUpcomingModelArr.removeAll()
                //self.ridesTableView.isHidden = true
                //self.ridesTableView.reloadData()
            }
        case 2:
            if (self.myRidesHistoryModelArr.count > 0)
            {
                //ridesTableView.isHidden = true
                self.myRidesHistoryModelArr.removeAll()
                //self.ridesTableView.isHidden = true
                //self.ridesTableView.reloadData()
            }
        default: break
        }

        print("typeOfRide = \(lastSegmentedControlTapped)")
        let dictPost = ["userid": (UserDefaults.standard.object(forKey: KUSER_ID) as! String),"historylive": typeOfRide]
print("dictPost==== \(dictPost)")
            if CommonFunctions.reachabiltyCheck()
            {
                headerValues =  ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]

                
                let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+"V2_bookingHistory")! as URL)
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpMethod = "POST"
                request.allHTTPHeaderFields = (headerValues as NSDictionary) as? [String : String]
                
                print("headers = \(String(describing: request.allHTTPHeaderFields))")
                if JSONSerialization.isValidJSONObject(dictPost)
                {
                    request.httpBody = try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let session = URLSession.shared
                    session.configuration.timeoutIntervalForRequest = 30
                    //print("try! request: \(try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted))")
                    
                    let task = session.dataTask(with: request as URLRequest) {[unowned self] data, response, error  in
                        
                        self.dismissLoader()
                        
                        guard response != nil else{
                            DispatchQueue.main.async {
                                self.showAlert(message:"Server not responding" )
                            }
                            return
                        }
                        
//                        if let httpResponse = response as? HTTPURLResponse {
//                            // use contentType here
//                            let contentLength = httpResponse.allHeaderFields["Content-Length"] as? String
//                            print("content length = \(String(describing: contentLength))")
//                            if ((contentLength?.intValue())! > 0) {
//                                // do nothing
//                            } else {
//                                DispatchQueue.main.async {
//                                    self.showAlert(message:"Server not responding" )
//                                }
//                                return
//                            }
//                        }
                        
                        var headers = (response as! HTTPURLResponse).allHeaderFields
                        print(headers);
                        let statusStr = (headers["Status"] as! String)
                        guard !statusStr.isEmpty && (statusStr == "1")else{
//                            completionHandler(nil,false)
                            return
                        }
                        
                        guard error?.localizedDescription == nil else{
                            print("error.debugDescription: \(String(describing: error?.localizedDescription))")
//                            completionHandler(nil,true)
                            DispatchQueue.main.async {
                                self.showAlert(message:"Server not responding" )
                            }
                            return
                        }
                        
                        
                        guard data != nil else {
                            print("no data found: \(String(describing: error))")
//                            completionHandler(nil,true)
                            return
                        }
                        
                        //print("data in string = \(String(data: data!, encoding: String.Encoding.utf8) as String!)")
                        
                        guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                            DispatchQueue.main.async {
                                self.showAlert(message: "Server not responding")
                            }
                            return
                        }
//                        let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                        
                        print(json)
                        if (!(json["status"]! as! Bool)) {
                            print(json["status"]!)
                            DispatchQueue.main.async {
                                if typeOfRide == "L" {
                                    self.showAlert(message: "You have no ongoing bookings.")
                                } else if typeOfRide == "U" {
                                    self.showAlert(message: "You have no upcoming bookings.")
                                } else {
                                    self.showAlert(message: "You do not have any booking history.")
                                }
                            }
                            return
                        }
                        
                        print("jhbashbda = \(String(describing: json["status"]))")
                        
                        let rideHistoryResponse = try? RideHistoryResponse(data: data)
                        
                        let arrRideHistoryDetail = rideHistoryResponse?.response
                        //let arrRideHistoryDetail = (rideHistoryResponse?.response)?.mutableCopy() as? NSMutableArray
                        
                        var localRideArray : [RideHistoryDetails] = []
                        for rideHistoryDict in arrRideHistoryDetail! as NSMutableArray
                        {
                            let rideDetailObj = try! RideHistoryDetails.init(dictionary: rideHistoryDict as? [AnyHashable : Any])
                            print(rideDetailObj.bookingid ?? "No bookingid")
                            localRideArray.append(rideDetailObj)
                        }
                        DispatchQueue.main.async {

                        //if(localRideArray.count > 0) {
                            switch self.ridesSegmentedControl.selectedSegmentIndex {
                            case 0:
                                //self?.myRidesOngoingModelArr.removeAll()
                                self.myRidesOngoingModelArr = localRideArray
                            case 1:
                                //self?.myRidesUpcomingModelArr.removeAll()
                                self.myRidesUpcomingModelArr = localRideArray
                            case 2:
                                //self?.myRidesHistoryModelArr.removeAll()
                                self.myRidesHistoryModelArr = localRideArray
                            default:
                                //self?.myRidesOngoingModelArr.removeAll()
                                self.myRidesOngoingModelArr = localRideArray
                            }
                                //self.ridesTableView.isHidden = false
                                self.ridesTableView.reloadData()
                            }
                        //}
                    }
                    task.resume()
                    
                }
            }else
            {
//                completionHandler(nil,true)
                CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
                
            }
        //}

    }
    
    func showAlert(message:String)  {
        let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default) {[weak self] (action) in
                self?.ridesTableView.reloadData()     //Swati Changes
        }
       // let defaultAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showBookingDetails" {
            if let destinationVC = segue.destination as? BookingDetailsNewController {
                if comingFromCases == ComingAfterCompletingModule.Normal.rawValue {
                    destinationVC.rideHistoryDetailsObj = sender as! RideHistoryDetails
                }
                destinationVC.bookingID = bookingIDFromJuspayPage
                destinationVC.selectedSegmentTapped = lastSegmentedControlTapped
                comingFromCases = ComingAfterCompletingModule.Normal.rawValue
            }
        }
    }
    
    //MARK: - Internal Functions
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
        let editController: LoginProcess? = (storyBoard.instantiateViewController(withIdentifier: "LoginProcess") as? LoginProcess)
        let navigationController = UINavigationController(rootViewController: editController!)
        editController?.strOtherDestination = "0"
        navigationController.viewControllers = [editController!]
        present(navigationController, animated: true, completion: nil)
        
    }
    
    @IBAction func searchNowButtonTapped(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    // MARK: - UITableViewDatasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch ridesSegmentedControl.selectedSegmentIndex {
        case 0:
                return myRidesOngoingModelArr.count
        case 1:
                return myRidesUpcomingModelArr.count
        case 2:
                return myRidesHistoryModelArr.count
        default:
            return myRidesOngoingModelArr.count
        }
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = self.ridesTableView.dequeueReusableCell(withIdentifier: "RidesCell") as! NoRidesTableViewCell
//        cell.signInButton.didTouchUpInside = { [weak weakSelf = self] (sender) in
//            weakSelf?.presentLoginController()
//        }
//        cell.searchNowButton.didTouchUpInside = { [weak weakSelf = self] (sender) in
//            weakSelf?.tabBarController?.selectedIndex = 0
//        }
        // create a new cell if needed or reuse an old one
        let cell:MyRidesHistoryCell = self.ridesTableView.dequeueReusableCell(withIdentifier: "RidesCell") as! MyRidesHistoryCell
        
        // set the text from the data model
        
        let myRideObj : RideHistoryDetails!
        switch ridesSegmentedControl.selectedSegmentIndex {
        case 0:
            myRideObj =  myRidesOngoingModelArr[indexPath.row]
        case 1:
            myRideObj =  myRidesUpcomingModelArr[indexPath.row]
        case 2:
            myRideObj =  myRidesHistoryModelArr[indexPath.row]
        default:
            myRideObj =  myRidesOngoingModelArr[indexPath.row]
        }
        
        cell.carNameLabel.text = myRideObj.carcatname + " " + myRideObj.carModelName
        cell.bookingIDLabel.text = " ID: \(myRideObj.bookingid!)"
        print("carname = \(myRideObj.carModelName)")
        //let carName = myRideObj.carModelName+".jpg"
        //https://www.mylescars.com/myles-campaign/images_thumb/Toyota-Fortuner-AT.jpg
        //let fullCarName = myRideObj.carModelName + "-"
        //Toyota-Fortuner-AT.jpg
        cell.carImageView.hnk_setImage(from: URL.init(string: myRideObj.carImageThumb), placeholder: UIImage.init(named: k_defaultCarFullImg))
        
        cell.carPickUpDayLabel.text = Helper.getDayOfWeek(myRideObj.pickupDate)
        cell.carPickUpTimeLabel.text = Helper.getTimeInAMPMCarList(timeString: myRideObj.pickupTime)
        cell.carDropOffTimeLabel.text = Helper.getTimeInAMPMCarList(timeString: myRideObj.dropoffTime)
        cell.carPickUpDateWithMonthLabel.text = Helper.getDateWithMonth(dateString: myRideObj.pickupDate)
        cell.carDropOffDayLabel.text = Helper.getDayOfWeek(myRideObj.dropoffDate)
        cell.carDropOffDateWithMonthLabel.text = Helper.getDateWithMonth(dateString: myRideObj.dropoffDate)
        
        
        return cell
    }
    
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rideHistoryObj : RideHistoryDetails!
        
        switch self.ridesSegmentedControl.selectedSegmentIndex {
        case 0:
//            print("indexPath.row === \(indexPath.row)")
//            print("myRidesHistoryModelArr[indexPath.row] === \(myRidesHistoryModelArr[indexPath.row])")
            rideHistoryObj = myRidesOngoingModelArr[indexPath.row]
        case 1:
//            print("indexPath.row === \(indexPath.row)")
//            print("myRidesHistoryModelArr[indexPath.row] === \(myRidesHistoryModelArr[indexPath.row])")
            rideHistoryObj = myRidesUpcomingModelArr[indexPath.row]
        case 2:
//            print("indexPath.row === \(indexPath.row)")
//            print("myRidesHistoryModelArr[indexPath.row] === \(myRidesHistoryModelArr[indexPath.row])")
            rideHistoryObj = myRidesHistoryModelArr[indexPath.row]
        default:
            rideHistoryObj = myRidesOngoingModelArr[indexPath.row]
        }
        self.performSegue(withIdentifier: "showBookingDetails", sender: rideHistoryObj)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    


}






