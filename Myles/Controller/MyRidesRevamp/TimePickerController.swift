//
//  TimePickerController.swift
//  Myles
//
//  Created by iOS Team on 18/07/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol BackProtocol  {
    func backFromTimerController(paymentLink : String)
    //func paymentSuccessOrFailure(bookingID: String)
}

enum TypeOfMethodCall {
    case calculate
    case update
}

class TimePickerController: UIViewController {
    
    var backDelegate : BackProtocol!

    
    var dropOffDate : String!
    var dropOffTime : String!
    var bookingID : String!
    var extensionTaxDiff : String!
    var extensionTaken : Bool!
    
    fileprivate var currentSelectedDateObj: CurrentSelectedDateAndTime! = nil
    fileprivate var typeOfMethodCall : TypeOfMethodCall!
    //IBOUTLETS
    @IBOutlet weak var dateAndTimePicker: UIDatePicker!
    //@IBOutlet weak var titleToolbar: UIToolbar!
    
    @IBOutlet weak var cancelButton: UIButton!
    // @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBOutlet weak var ExtensionHeadingButton: UILabel!
    //@IBOutlet weak var extensionHeadingButton: UIBarButtonItem!
    
    @IBOutlet weak var extendButton: UIButton!
    //@IBOutlet weak var extendButton: UIBarButtonItem!
    
    var headerValues = Dictionary<String, Any>()

    @IBOutlet weak var upperView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //extendButton.isEnabled = false
        // Do any additional setup after loading the view.

        dropOffTime.insert(":", at: dropOffTime.index(dropOffTime.startIndex,offsetBy:2))
        let splitStrArr = dropOffTime.components(separatedBy: ":")
        let dateString = "\(dropOffDate!) \(splitStrArr[0]):\(splitStrArr[1]):00"
        //"12-11-2015 10:50:00"
        print(dateString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        print(date ?? Date())
        dateAndTimePicker.minimumDate = date
        print("Data = \(dropOffDate) and time = \(dropOffTime)")
        
        //Adding Tap Gesture 
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(sender:)))
        tapGesture.numberOfTapsRequired = 1
        upperView.addGestureRecognizer(tapGesture)
        let componenets = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute,.second], from: (dateAndTimePicker as AnyObject).date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year, let hour = componenets.hour, let minutes = componenets.minute, let seconds = componenets.second {
            print("\(day) \(month) \(year) \(hour) \(minutes) \(seconds)")
            currentSelectedDateObj = CurrentSelectedDateAndTime(day:"\(day)", month: "\(month)", year: "\(year)", minute: "\(minutes)", hour: "\(hour)")
        }
        
    }

    
    @objc func tapBlurButton(sender: UITapGestureRecognizer) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func extensionButtonTapped(_ sender: Any) {
        if let buttonTitle = (sender as AnyObject).title(for: .normal) {
            if (buttonTitle == "Extend") {
                typeOfMethodCall = TypeOfMethodCall.calculate
            } else {
                typeOfMethodCall = TypeOfMethodCall.update
            }
            calculateAndUpdateExtension()

        }
        

    }
    
    @IBAction func datePickerValueChanged(_ sender: Any) {
        extendButton.isEnabled = true
        let componenets = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute,.second], from: (sender as AnyObject).date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year, let hour = componenets.hour, let minutes = componenets.minute, let seconds = componenets.second {
            print("\(day) \(month) \(year) \(hour) \(minutes) \(seconds)")
            currentSelectedDateObj = CurrentSelectedDateAndTime(day:"\(day)", month: "\(month)", year: "\(year)", minute: "\(minutes)", hour: "\(hour)")
        }
        typeOfMethodCall = TypeOfMethodCall.calculate
        calculateAndUpdateExtension()

        print(currentSelectedDateObj.createExtensionTime())
    }
    
    
    
    // Calling function to calculate and update extension data
    func calculateAndUpdateExtension() {
        self.showLoader()
        //public func SearchCarApi (completionHandler: @escaping (MylesCarDetailsResponse?,Bool) ->Void) {
        //string bookingid, string Extendeddateforentry, string Extendedtimeforentry, string userid, string carid, string sourceinfo
        var dictPost = [String:String]()
        //let request : NSMutableURLRequest!
        if (typeOfMethodCall == .calculate) {
            dictPost = ["userid": (UserDefaults.standard.object(forKey: KUSER_ID) as! String),"Extendeddateforentry": currentSelectedDateObj.createExtensionDate(),"Extendedtimeforentry":currentSelectedDateObj.createExtensionTime(),"carid":"0","sourceinfo":"ios","bookingid":bookingID!]
        } else if (typeOfMethodCall == .update) {
            //string bookingid, string Extendeddateforentry, string Extendedtimeforentry, string userid, string carid, string sourceinfo,string username
           dictPost = ["userid": (UserDefaults.standard.object(forKey: KUSER_ID) as! String),"Extendeddateforentry": currentSelectedDateObj.createExtensionDate(),"Extendedtimeforentry":currentSelectedDateObj.createExtensionTime(),"carid":"0","sourceinfo":"ios","bookingid":bookingID!,"username": (UserDefaults.standard.object(forKey: KUSER_NAME) as! String),"taxdiff":extensionTaxDiff]
        }
        
        print(dictPost)
        if CommonFunctions.reachabiltyCheck()
        {
            headerValues =  ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]
            
            let request : NSMutableURLRequest!
            if (typeOfMethodCall == .calculate) {
                request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+"ExtensionCalculation")! as URL)
            } else {
                request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+"ExtensionUpdate")! as URL)
            }
            
            
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headerValues as NSDictionary) as? [String : String]
            
            print("headers = \(String(describing: request.allHTTPHeaderFields))")
            if JSONSerialization.isValidJSONObject(dictPost)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 30
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                let task = session.dataTask(with: request as URLRequest) {[weak self] data, response, error  in
                    
                    self?.dismissLoader()
                    
                    guard response != nil else{
                        DispatchQueue.main.async {
                            self?.showAlert(message:"Server not responding",toShow: false )
                        }
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    print(headers);
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        return
                    }
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                        DispatchQueue.main.async {
                            self?.showAlert(message:"Server not responding",toShow: false )
                        }
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                        return
                    }
                    
                    print("data in string = \(String(data: data!, encoding: String.Encoding.utf8) as String!)")
                    
                    guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                        DispatchQueue.main.async {
                            self?.showAlert(message: "Server not responding",toShow: false)
                        }
                        return
                    }
                    
                    print(json)
                    //                        let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                    if (!(json["status"]! as! Bool)) {
                        print(json["status"]!)
                        DispatchQueue.main.async {
                            self?.showAlert(message: (json["message"] as? String)!,toShow: true)
                        }
//                        //Dismissing controller
//                        self?.dismiss(animated: true, completion: nil)
                        
                        return
                    }
                    
                    print("jhbashbda = \(String(describing: json["status"]))")
                    if (self?.typeOfMethodCall == .calculate) {
                        //FOR CALCULATE
                        if let etc = json["EO"] as? [[String : Any]] {
                            let etcObj = etc[0]
                        //if let etc = json["EO"]?[0] as? [String : Any] {
                            let extensionCalculationObj = ExtensionCalculation(etcObj)
                            self?.extensionTaxDiff = extensionCalculationObj.ExtensionTaxDiff
                            DispatchQueue.main.async {
                                var totalAmount : String = ""
                                var extensionAmt : Float = 0.0
                                
                                var taxAmount : Float = 0.0
                                taxAmount = (extensionCalculationObj.ExtensionCGSTAmount.floatValue) + (extensionCalculationObj.ExtensionSGSTAmount.floatValue) + (extensionCalculationObj.ExtensionIGSTAmount.floatValue) + (extensionCalculationObj.ExtensionUTGSTAmount.floatValue)
                                
                                extensionAmt = (extensionCalculationObj.LDWAmountForExtension.floatValue) + (extensionCalculationObj.ExtensionTaxDiff.floatValue) + taxAmount + (extensionCalculationObj.extensioncalculatedamount.floatValue)
                                
                                //let strExtensionAmount = String(format: "%@", extensionAmt)
                                //totalAmount = extensionCalculationObj.calculateTotalExtensionAmount() //Swati Changes
                                
                                totalAmount = String(format: "%d", Int(extensionAmt))
                                self?.showAlert(message: String(format: "Extension amount calculated is ₹ %@", totalAmount), toShow: false)
                                //self?.showAlert(message: "Extension amount calculated is ₹ \(totalAmount)",toShow: false)
                                self?.ExtensionHeadingButton.text = String(format: "Extension Amt ₹ %@", totalAmount)//"Extension Amt ₹ \(totalAmount)"
                                if (totalAmount.floatValue > 0) {
                                    self?.extendButton.setTitle("PAY", for: .normal)
                                }
                                else{
                                    self?.extendButton.setTitle("Extend", for: .normal)
                                }
                            }
                            
                            
//                            for etcObj in etc {
//                                if let extensionAmount = etcObj["extensioncalculatedamount"] as? String {
//                                    print(extensionAmount)
//                                    
//                                    let extensionDetail = ExtensionDetail()
//                                    let ldwExtensionAmount = etcObj["LDWAmountForExtension"] as? String
//                                    
//                                    
//                                    DispatchQueue.main.async {
//                                        self?.showAlert(message: "Extension amount calculated is ₹\(extensionAmount.floatValue + (ldwExtensionAmount?.floatValue)!)",toShow: false)
//                                        self?.extensionHeadingButton.title = "Extension Amt ₹ \(extensionAmount)"
//                                        self?.extendButton.title = "PAY"
//                                    }
//                                }
//                            }
                        }
                    } else {
                        // For UPDATE
                        if let paymentLink = json["PaymentLink"] {
                            print("payment link = \(paymentLink)")
                            DispatchQueue.main.async {
                                //dismiss the controller and pass the delgate to call for extension payment
                                self?.backDelegate.backFromTimerController(paymentLink: paymentLink as! String)
                                self?.dismiss(animated: true, completion: nil)
                                //self?.performSegue(withIdentifier: "extensionPaymentSegue", sender: paymentLink)
                            }
                        }
                        

                        
                    }
                    
                }
                
                
                task.resume()
            }
            
        }
        else
        {
            CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
            
        }
    }
    
    
    func showAlert(message:String,toShow:Bool)  {
        let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
        var defaultAction = UIAlertAction()
        
        if toShow {
            defaultAction = UIAlertAction(title: "OK", style: .default, handler:{(alert: UIAlertAction!) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            })
        } else {
            defaultAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        }
        
        // let defaultAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        //extensionPaymentSegue
        print(sender ?? "")
        if segue.identifier == "extensionPaymentSegue" {
            if let destinationVC = segue.destination as? ExtensionPaymentController {
                destinationVC.extensionPaymentURL = (sender as? String)!
               
            }
        }
    }
    

}

class CurrentSelectedDateAndTime {
    let day : String!
    let month : String!
    let year : String!
    let minute : String!
    let hour : String!

    init(day : String,month : String,year : String, minute : String,hour : String) {
        self.day = day
        self.month = month
        self.year = year
        self.minute = minute
        self.hour = hour
    }
    func createExtensionDate() -> String {
        return "\(month!)/\(day!)/\(year!)"
    }
    
    func createExtensionTime() -> String {
        var changedHour = ""
        var changedMinutes = ""
        if (Int(hour)! < 10) {
            changedHour = "0\(hour!)"
        } else {
            changedHour = hour!
        }
        if (Int(minute)! < 30) {
            changedMinutes = "00"
        } else if (Int(minute)! > 30){
            changedMinutes = "30"
        }

        else{
            changedMinutes = "\(minute!)"
        }
        print("changedHour == \(changedHour) + changedMinutes === \(changedMinutes)")
        return "\(changedHour)\(changedMinutes)"
    }
}


class ExtensionCalculation {
    
    fileprivate var ExtensionCGSTAmount : String!
    fileprivate var ExtensionIGSTAmount : String!
    fileprivate var ExtensionSGSTAmount : String!
    fileprivate var ExtensionTaxDiff : String!
    fileprivate var ExtensionUTGSTAmount : String!
    fileprivate var ExtensionVatAmount : String!
    fileprivate var Extensiontype : String!
    fileprivate var LDWAmountForExtension : String!
    fileprivate var extensioncalculatedamount : String!
    fileprivate var ExtensionCessAmount : String!
    
    
    init() {
        
    }
    
    convenience init(_ dictionary: [String : Any]) {
        self.init()
        
        LDWAmountForExtension = dictionary["LDWAmountForExtension"] as? String
        ExtensionCGSTAmount = dictionary["ExtensionCGSTAmount"] as? String
        ExtensionIGSTAmount = dictionary["ExtensionIGSTAmount"] as? String
        ExtensionSGSTAmount = dictionary["ExtensionSGSTAmount"] as? String
        ExtensionTaxDiff = dictionary["ExtensionTaxDiff"] as? String
        ExtensionUTGSTAmount = dictionary["ExtensionUTGSTAmount"] as? String
        ExtensionVatAmount = dictionary["ExtensionVatAmount"] as? String
        Extensiontype = dictionary["Extensiontype"] as? String
        extensioncalculatedamount = dictionary["extensioncalculatedamount"] as? String
        ExtensionCessAmount = dictionary["ExtensionCessAmount"] as? String
        
    }
    
    func calculateTotalExtensionAmount() -> String
    {
        var extensionAmt : Float = 0.0
        
        var taxAmount : Float = 0.0
        taxAmount = (ExtensionCGSTAmount.floatValue) + (ExtensionSGSTAmount.floatValue) + (ExtensionIGSTAmount.floatValue) + (ExtensionUTGSTAmount.floatValue)
        
        extensionAmt = (LDWAmountForExtension.floatValue) + (ExtensionTaxDiff.floatValue) + taxAmount + (ExtensionCessAmount.floatValue) + (extensioncalculatedamount.floatValue)
        
        let strExtensionAmount = String(format: "%@", extensionAmt)
        
        return strExtensionAmount
        
        
//        return "\(LDWAmountForExtension.floatValue + ExtensionCGSTAmount.floatValue + ExtensionSGSTAmount.floatValue + ExtensionIGSTAmount.floatValue + ExtensionTaxDiff.floatValue + ExtensionUTGSTAmount.floatValue + ExtensionVatAmount.floatValue + extensioncalculatedamount.floatValue)"
        
        //return ""
    }
    
}
