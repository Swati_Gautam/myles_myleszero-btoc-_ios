//
//  NoDataCell.swift
//  Myles
//
//  Created by iOS Team on 22/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit

class NoDataCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
