//
//  FlashOfferViewController.swift
//  Myles
//
//  Created by ITTeam on 23/12/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import UIKit

class FlashOfferViewController: UIViewController {
    
    @IBOutlet weak var offerWebview: UIWebView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var CancelBtn: UIBarButtonItem!
    
    
    
    @IBAction func cancleBtnAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(CommonFunctions.reachabiltyCheck()){
            GMDCircleLoader.setOn(self.view, withTitle: "Please wait...", animated: true)
            
            // Do any additional setup after loading the view.
            let request = NSMutableURLRequest(url: URL(string: "https://www.mylescars.com/marketings/myles_offers_app")!,
                                              cachePolicy: .reloadIgnoringLocalCacheData,
                                              timeoutInterval: 30.0)
            
            self.offerWebview.loadRequest(request as URLRequest)
        } else {
            CommonFunctions.alertTitle("MYLES", withMessage:"No Network available. Please connect and try again.")
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        //http://stackoverflow.com/questions/5995210/disabling-user-selection-in-uiwebview
        if (webView.isLoading){
            return
        }
        GMDCircleLoader.hide(from: self.view, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
