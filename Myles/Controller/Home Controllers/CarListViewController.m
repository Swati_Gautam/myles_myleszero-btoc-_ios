
//HEREALSOTODO
#import "CarInfoListTableViewCell.h"
#import "CarListNormalCell.h"
#import "SublocationDetails.h"
#import "PackageType.h"
#import "PackageResponse.h"
#import "PackegeFilterViewController.h"
#import <Google/Analytics.h>
#import "BookingDetailsVC.h"
#import "CarDetailInformation.h"
#import "CarDetailInfo.h"
#import "SublocationListViewController.h"
#import "FilterViewController.h"
#import "SortViewController.h"
#import "Haneke.h"
#import "LoginViewController.h"
#import "Myles-Swift.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "NextAvailableCell.h"
#import "NextSectionHeaderCell.h"
#import "Sublocation.h"

@import Firebase;
@import FirebaseAnalytics;
@import GooglePlacePicker;

typedef enum completionResults
{
    COMPLETION_SUCCESS,
    COMPLETION_FAILURE,
    COMPLETION_NODATA,
    COMPLETION_SESSION_EXPIRED
} LocationCheckResults;

@interface CarListViewController ()<SublocationListViewControllerDelegate,LoginProtocol,LocationDataDelegate,UIScrollViewDelegate>
{
    SublocationDetails *responseData;
    PackageResponse *activePackageTypes;
    UIView *popUpview;
    UIButton *selectedButton;
//    NSInteger _selectedCityId;
    NSInteger cityName;
    NSInteger sublocationIdDetails;
    NSString *sublocationAddress;
    NSString *sublocationID;
    NSString *packageType;
    NSMutableArray *packageMA,*arrFilterData,*nextArrFilterData, *dictSelectCollectionLoc;
    NSMutableDictionary*dictSelectLoc ,*dictFilterData,*dictFilter ;
    BOOL isFilter,nextIsFilter;
    NSString *indexpath;
    CLLocation *locationOfDoorStepDeleivery;
    //NSMutableArray *packageMA;
    //NSMutableDictionary*dictSelectLoc;
    //NSMutableArray *modelImagesMA;
    //modelImageArray
    
    NSMutableArray *requiredArray;// For tab 1
    NSMutableArray *nextRequiredArray;
    NSMutableArray *requiredDoorStepArray;//tab 2
    NSMutableArray *nextRequiredDoorstepArray;
    NSIndexPath *index;
    int roundedRate;
    LocationData *locationDataObj;
    NSInteger globalSection,globalSelectedRow;
    __weak IBOutlet NSLayoutConstraint *homePickUpViewContraint;
    NSString *finalTimeStr;
    BOOL cityIsChanged;
    //BOOL showPopUp;
    NSString *city;
    NSString *userType;
    NSString *pickupCity;
    NSString  *userLocation;
    NSString *daysToTripStart;
    NSString *tripDuration;
    NSString *packageTypeSelected;
    NSString *isWeekendtrip;
    NSInteger availableCount;
    NSInteger nexAvailableCount;
    NSString *packageDetails;
    NSString *pickupType;
    NSString *speedG;
    BOOL *fireEvent;
    NSString *previousDateTime, *currentDateTime;
    NSTimeInterval currentMilliSec,previousMilliSec;

    __weak IBOutlet NSLayoutConstraint *tableViewBottomHeightConstraint;
    BOOL viewIsScrolling;
    
    CarPackageDetails *globalSelectedPackage;
}

@end

@implementation CarListViewController {
   // GMSPlacePicker *_placePicker;  //Swati Commented
     //GMSPlace *_placePicker; //not in used

}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}


- (void)viewDidLoad
{   
    [super viewDidLoad];
    //[self.navigationItem.backBarButtonItem setTitle:@""];
    
    viewIsScrolling = FALSE;
    fireEvent = TRUE;
    self.navigationController.navigationBar.hidden = FALSE;
    self.title = @"Select Car";
    city = _sublocationTitle;
    //Registering new Tableview cell
    [_tableView registerNib:[UINib nibWithNibName:@"CarListNormalCell" bundle:nil] forCellReuseIdentifier:@"CarListNormalCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"NextAvailableCell" bundle:nil] forCellReuseIdentifier:@"NextAvailableCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"NextSectionHeaderCell" bundle:nil] forCellReuseIdentifier:@"NextSectionHeaderCell"];
    //showPopUp = FALSE;
    if (![DEFAULTS integerForKey:k_HomePickupAvailable]) {
        [UIView animateWithDuration:1.0f
                         animations:^{
                             self.pickUpDropOffView.hidden = TRUE;
                             self.pickupSegementedControl.hidden = TRUE;
                             self.pickUpDropOffViewheightConstraint.constant = 0;
                             self.segmentedControlHeightConstraint.constant = 0;
                             homePickUpViewContraint.constant = 0;
                             //self.locationImageview.hidden = TRUE;
                             
                         }];
    } else {
        NSString *priceOfHomeickup = [NSString stringWithFormat:@"Doorstep Delivery (₹%ld)",[[DEFAULTS valueForKey:k_HomePickupCharge] integerValue]];

        [self.pickupSegementedControl setTitle:priceOfHomeickup forSegmentAtIndex:1];
    }
    
    [[UISegmentedControl appearance] setTintColor:UIColorFromRGBForNavigationBar(0xDF4731)];
    [[UILabel appearanceWhenContainedIn:[UISegmentedControl class], nil] setNumberOfLines:2];
    [UIView animateWithDuration:1.0f
                     animations:^{
                         self.pickUpDropOffViewheightConstraint.constant -= 35;
                         self.locationImageview.hidden = TRUE;
                        
                     }];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationNameLabelTapped:)];
    tapGesture.numberOfTapsRequired = 1;
    self.locationNameLabel.userInteractionEnabled = TRUE;
    [self.locationNameLabel addGestureRecognizer:tapGesture];
    _isSublocationSelected = false;
    
#ifdef DEBUG
#else
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"SitePickUp SearchPage"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
#endif 
    
    requiredArray = [NSMutableArray array];
    requiredDoorStepArray = [NSMutableArray array];
    nextRequiredArray = [NSMutableArray array];
    nextRequiredDoorstepArray = [NSMutableArray array];
    [self callToUpdateDataOnList];
    [FIRAnalytics logEventWithName:kFIREventViewSearchResults parameters:nil];
    
}

- (void)logEvent:(NSString *)searchType {
    NSLog(@"%@", city);
    NSLog(@"%@", self.packageType);
    NSLog(@"%@", self.timeToTripStart);
    NSLog(@"%@", self.tripDurationSelected);
    NSInteger myInt = [_tripDurationSelected integerValue];
    pickupType = searchType;
    if(myInt> 24){
       // _tripDurationSelected  = [NSString stringWithFormat:@"%ld", (long)myInt / 24]; // convert integer to string to display result

        packageTypeSelected = @"Daily";
    }
    else{
        packageTypeSelected = @"Hourly";

    }

    if (![DEFAULTS boolForKey:IS_LOGIN]) {
        userType = @"Guest";
    }
    else{
        userType = @"Logged in";

    }
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            availableCount =  [requiredArray count];
        } else {
            availableCount = [requiredDoorStepArray count];
        }
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            nexAvailableCount = [nextRequiredArray count];
        } else {
            nexAvailableCount = [nextRequiredDoorstepArray count];
        }
    
    NSString *current = [NSString stringWithFormat:@"%ld", (long)availableCount]; // convert integer to string to display result
    NSString *next = [NSString stringWithFormat:@"%ld", (long)nexAvailableCount]; // convert integer to string to display result


    [FIRAnalytics logEventWithName:@"search_car"
                        parameters:@{
                                    @"user_type":userType,
                                    @"pickup_city":city,
                                    @"hours_to_trip_start":_timeToTripStart,
                                    @"trip_duration":_tripDurationSelected,
                                    @"package_type":packageTypeSelected,
                                    @"available_cars_current":current,
                                    @"available_cars_next":next,
                                     }];
}
//Method to get whether we need to show 2nd section or not
-(BOOL)returnBothSection {
    if (_nextCarDetailInfoMA.count > 0 || _nextDoorStepDetailInfoArray.count > 0) {
        return TRUE;
    } else
        return FALSE;
}

-(void)callToUpdateDataOnList {
    BOOL detailIDisPresent = false;

    //Call api first
    if (self.carDetailInfoMA.count <= 0) {
        UIAlertController *alertController = [UIAlertController
                                              
                                              alertControllerWithTitle:@"Message"
                                              
                                              message:@"We are sold out for the time-period you have selected! Please search again with minimum booking period of 48hrs, or with a different location or travel date"
                                              
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        
        
        UIAlertAction *okAction = [UIAlertAction
                                   
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   
                                   style:UIAlertActionStyleDefault
                                   
                                   handler:^(UIAlertAction *action)
                                   
                                   {
                                       
                                       
                                       [self.navigationController popViewControllerAnimated:YES];
                                       
                                   }];
        
        
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    } else {
        for (int i = 0 ; i < self.carDetailInfoMA.count; i++)
        {
            @autoreleasepool {
                CarDetailInformation *aCarDetailInfoTest = self.carDetailInfoMA[i];
                
                if ([aCarDetailInfoTest.IsAvailable boolValue]) {
                    
                    if ([aCarDetailInfoTest.Sublocation count] > 0) {
                        detailIDisPresent = true;
                        //////NSLog(@"acardeta.detail = %@",aCarDetailInfoTest.DetailID);
                        
                        NSString *setingCApacity = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.SeatingCapacity];
                        NSString *displayRate = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.IndicatedPrice];
                        if (dictFilter == nil)
                        {
                            dictFilter = [[NSMutableDictionary alloc]init];
                            [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                            [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                            [dictFilter setValue:@[aCarDetailInfoTest.CarCatName] forKey:@"catCategory"];
                            [dictFilter setValue:@[aCarDetailInfoTest.FuelType] forKey:@"fuel"];
                            [dictFilter setValue:@[aCarDetailInfoTest.SeatingCapacity] forKey:@"seats"];
                            [dictFilter setValue:@[[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"] forKey:@"transmission"];
                            
                            
                        }
                        else
                        {
                            
                            if (dictFilter[@"highprice"] != nil && [displayRate floatValue] > [dictFilter[@"highprice"] integerValue])
                            {
                                [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                            }
                            else if (dictFilter[@"lowprice"] != nil && [displayRate floatValue] < [dictFilter[@"lowprice"] integerValue])
                            {
                                [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                            }
                            
                            if ( dictFilter[@"fuel"] != nil && ![dictFilter[@"fuel"] containsObject:aCarDetailInfoTest.FuelType])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"fuel"] mutableCopy];
                                [arrtemp addObject:aCarDetailInfoTest.FuelType];
                                [dictFilter setValue:arrtemp forKey:@"fuel"];
                                
                            }
                            
                            if (dictFilter[@"catCategory"] != nil && ![dictFilter[@"catCategory"] containsObject:aCarDetailInfoTest.CarCatName])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"catCategory"] mutableCopy];
                                [arrtemp addObject:aCarDetailInfoTest.CarCatName];
                                [dictFilter setValue:arrtemp forKey:@"catCategory"];
                                
                            }
                            
                            if (dictFilter[@"seats"] != nil && ![dictFilter[@"seats"] containsObject:aCarDetailInfoTest.SeatingCapacity])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"seats"] mutableCopy];
                                [arrtemp addObject:aCarDetailInfoTest.SeatingCapacity];
                                [dictFilter setValue:arrtemp forKey:@"seats"];
                                
                            }
                            
                            if (dictFilter[@"transmission"] != nil &&  ![dictFilter[@"transmission"] containsObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"transmission"] mutableCopy];
                                [arrtemp addObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"];
                                [dictFilter setValue:arrtemp forKey:@"transmission"];
                                
                            }
                        }
                        //PATCH
                        aCarDetailInfoTest.Detail= @"";
                        aCarDetailInfoTest.DetailID = @"";
                        for (int i = 0; i < aCarDetailInfoTest.Sublocation.count; i++) {
                            Sublocation *sublocationDetail = (Sublocation *)aCarDetailInfoTest.Sublocation[i];
                        
                            if (i == aCarDetailInfoTest.Sublocation.count - 1) {
                                aCarDetailInfoTest.Detail = [aCarDetailInfoTest.Detail stringByAppendingString:sublocationDetail.Detail];
                                aCarDetailInfoTest.DetailID = [aCarDetailInfoTest.DetailID stringByAppendingString:sublocationDetail.DetailID];
                            } else {
                                NSLog(@"cardetailInfoTest = %@",aCarDetailInfoTest.Detail);
                                NSLog(@"cardetailInfoTest = %@",sublocationDetail);

                                aCarDetailInfoTest.Detail = [aCarDetailInfoTest.Detail stringByAppendingString:[NSString stringWithFormat:@"%@:",sublocationDetail.Detail]];
                                aCarDetailInfoTest.DetailID = [aCarDetailInfoTest.DetailID stringByAppendingString:[NSString stringWithFormat:@"%@:",sublocationDetail.DetailID]];
                            }
                        }
                        [requiredArray addObject:aCarDetailInfoTest];
                    }
                }
            }
        }
        
        //Changing
        if (self.nextCarDetailInfoMA.count > 0) {
            for (int i = 0 ; i < self.nextCarDetailInfoMA.count; i++) {
                @autoreleasepool {
                    

                    CarDetailInformation *aCarDetailInfoTest = self.nextCarDetailInfoMA[i];
                    NSLog(@"sssss: %@",aCarDetailInfoTest);

                    if ([aCarDetailInfoTest.IsAvailable boolValue]) {
                        
                        if ([aCarDetailInfoTest.Sublocation count] > 0) {
                            detailIDisPresent = true;
                            //////NSLog(@"acardeta.detail = %@",aCarDetailInfoTest.DetailID);
                            
                            NSString *setingCApacity = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.SeatingCapacity];
                            NSString *displayRate = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.IndicatedPrice];
                            if (dictFilter == nil)
                            {
                                dictFilter = [[NSMutableDictionary alloc]init];
                                [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                                [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                                [dictFilter setValue:@[aCarDetailInfoTest.CarCatName] forKey:@"catCategory"];
                                [dictFilter setValue:@[aCarDetailInfoTest.FuelType] forKey:@"fuel"];
                                [dictFilter setValue:@[aCarDetailInfoTest.SeatingCapacity] forKey:@"seats"];
                                [dictFilter setValue:@[[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"] forKey:@"transmission"];
                                
                                
                            }
                            else
                            {
                                
                                if (dictFilter[@"highprice"] != nil && [displayRate floatValue] > [dictFilter[@"highprice"] integerValue])
                                {
                                    [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                                }
                                else if (dictFilter[@"lowprice"] != nil && [displayRate floatValue] < [dictFilter[@"lowprice"] integerValue])
                                {
                                    [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                                }
                                
                                if ( dictFilter[@"fuel"] != nil && ![dictFilter[@"fuel"] containsObject:aCarDetailInfoTest.FuelType])
                                {
                                    NSMutableArray *arrtemp =[dictFilter[@"fuel"] mutableCopy];
                                    [arrtemp addObject:aCarDetailInfoTest.FuelType];
                                    [dictFilter setValue:arrtemp forKey:@"fuel"];
                                    
                                }
                                
                                if (dictFilter[@"catCategory"] != nil && ![dictFilter[@"catCategory"] containsObject:aCarDetailInfoTest.CarCatName])
                                {
                                    NSMutableArray *arrtemp =[dictFilter[@"catCategory"] mutableCopy];
                                    [arrtemp addObject:aCarDetailInfoTest.CarCatName];
                                    [dictFilter setValue:arrtemp forKey:@"catCategory"];
                                    
                                }
                                
                                if (dictFilter[@"seats"] != nil && ![dictFilter[@"seats"] containsObject:aCarDetailInfoTest.SeatingCapacity])
                                {
                                    NSMutableArray *arrtemp =[dictFilter[@"seats"] mutableCopy];
                                    [arrtemp addObject:aCarDetailInfoTest.SeatingCapacity];
                                    [dictFilter setValue:arrtemp forKey:@"seats"];
                                    
                                }
                                
                                if (dictFilter[@"transmission"] != nil &&  ![dictFilter[@"transmission"] containsObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"])
                                {
                                    NSMutableArray *arrtemp =[dictFilter[@"transmission"] mutableCopy];
                                    [arrtemp addObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"];
                                    [dictFilter setValue:arrtemp forKey:@"transmission"];
                                    
                                }
                            }
                            
                            aCarDetailInfoTest.Detail= @"";
                            aCarDetailInfoTest.DetailID = @"";

                            
                            for (int i = 0; i < aCarDetailInfoTest.Sublocation.count; i ++) {
                                Sublocation *sublocationDetail = (Sublocation *)aCarDetailInfoTest.Sublocation[i];
                                
                                if (i == aCarDetailInfoTest.Sublocation.count - 1) {
                                    aCarDetailInfoTest.Detail = [aCarDetailInfoTest.Detail stringByAppendingString:sublocationDetail.Detail];
                                    aCarDetailInfoTest.DetailID = [aCarDetailInfoTest.DetailID stringByAppendingString:sublocationDetail.DetailID];
                                } else {
                                    NSLog(@"cardetailInfoTest = %@",aCarDetailInfoTest.Detail);
                                    NSLog(@"cardetailInfoTest = %@",sublocationDetail);
                                    
                                    aCarDetailInfoTest.Detail = [aCarDetailInfoTest.Detail stringByAppendingString:[NSString stringWithFormat:@"%@:",sublocationDetail.Detail]];
                                    aCarDetailInfoTest.DetailID = [aCarDetailInfoTest.DetailID stringByAppendingString:[NSString stringWithFormat:@"%@:",sublocationDetail.DetailID]];
                                }
                            }
                            
                            [nextRequiredArray addObject:aCarDetailInfoTest];
                        }
                    }
                }
            }
        }

        NSLog(@"requiredArrayCount = %lu",(unsigned long)[requiredArray count]);
        
        if (([requiredArray count] == 0) && (!detailIDisPresent)) {
            
            UIAlertController *alertController = [UIAlertController
                                                  
                                                  alertControllerWithTitle:@"Message"
                                                  
                                                  message:@"The location is closed during the time you have selected. Please try again with different timings. Thank you."
                                                  
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            
            
            UIAlertAction *okAction = [UIAlertAction
                                       
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       
                                       style:UIAlertActionStyleDefault
                                       
                                       handler:^(UIAlertAction *action)
                                       
                                       {
                                           
                                           //NSLog(@"OK action");
                                           
                                           [self.navigationController popViewControllerAnimated:YES];
                                           
                                       }];
            
            
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            return;
            
        }else {
            
            self.tableView.delegate = self;
            
            self.tableView.dataSource = self;
            [self logEvent:@"Site pickup"];

            [self.tableView reloadData];
            
            
            
        }
        
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}



-(void) viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = FALSE;
    viewIsScrolling = FALSE;

    self.navigationController.navigationBarHidden = FALSE;
    self.navigationController.navigationBar.tintColor = UIColor.whiteColor;
    self.navigationController.navigationBar.translucent = true;
    //UINavigationBar.appearance().tintColor = UIColor.darkGray

    //[self customizeNavigationBar:@"Select Car" Present:false];
    //self.navigationController.navigationBarHidden = false;
    ///self.navigationController.navigationBar.hidden = false;
    //self.navigationController.navigationBar.barTintColor = UIColorFromRGBForNavigationBar(0xDF4731);
    
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Select Car"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)initilise : (NSString *)title CityId : (NSInteger)cityid Sublocation : (NSInteger )sublocationID SubLocationAddress : (NSString *)sublocationAdd
{
    _selectedCityId = cityid;
    sublocationIdDetails = sublocationID;
    sublocationAddress = sublocationAdd;
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    BOOL *isRawCountZero = true;
    if (section == 0) {
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            if(isFilter){
                if(arrFilterData.count > 0){
                    isRawCountZero = false;
                }
            }
            else{
                if(requiredArray.count > 0){
                    isRawCountZero = false;
                    
                }
            }
            //if(isRawCountZero){
            [self showError: isRawCountZero];
            //}
            
            return isFilter ? arrFilterData.count : [requiredArray count];
        } else {
            if(isFilter){
                if(arrFilterData.count > 0){
                    isRawCountZero = false;
                }
            }
            else{
                if(requiredDoorStepArray.count > 0){
                    isRawCountZero = false;
                    
                }
            }
            //if(isRawCountZero){
            [self showError : isRawCountZero];
            //}
            return isFilter ? arrFilterData.count : [requiredDoorStepArray count];
        }
    } else {
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            if(isFilter){
                if(nextArrFilterData.count > 0){
                    isRawCountZero = false;
                }
            }
            else{
                if(nextRequiredArray.count > 0){
                    isRawCountZero = false;
                    
                }
            }
            //if(isRawCountZero){
            [self showError : isRawCountZero];
            //}
            return isFilter ? nextArrFilterData.count : [nextRequiredArray count];
        } else {
            if(isFilter){
                if(nextArrFilterData.count > 0){
                    isRawCountZero = false;
                }
            }
            else{
                if(nextRequiredDoorstepArray.count > 0){
                    isRawCountZero = false;
                    
                }
            }
            //if(isRawCountZero){
            [self showError: isRawCountZero];
            //}
            return isFilter ? nextArrFilterData.count : [nextRequiredDoorstepArray count];
        }
    }
}

-(void) showError: (BOOL *)isRawCountZero {
    if (!isRawCountZero)
    {
        //yourTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        //numOfSections                = 1;
        _tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, _tableView.bounds.size.height)];
        noDataLabel.text             = @"No cars available";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        _tableView.backgroundView = noDataLabel;
        //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

-(void) createPromoCodeView
{
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_pickupSegementedControl.selectedSegmentIndex == 0) {
    
        if (_nextCarDetailInfoMA.count > 0) {
            if (isFilter) {
                if (nextArrFilterData.count > 0)
                    return 2;
                else
                    return 1;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    } else {
        if (_nextDoorStepDetailInfoArray.count > 0) {
            if (isFilter) {
                if (nextArrFilterData.count > 0)
                    return 2;
                else
                    return 1;
            } else
                return 2;
        } else {
            return 1;
        }
    }
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50.0f;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *viewPromoCode = [[UIView alloc] init];
    viewPromoCode.frame = CGRectMake(SCREEN_WIDTH/2, 5, self.view.frame.size.width - 20, 40);
    viewPromoCode.backgroundColor = [UIColor blackColor];
    return viewPromoCode;
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = k_carListNormalCell;
    
    NextAvailableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    if ( cell == nil )
    {
        cell = [[NextAvailableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    //indexPath.section*1000 +indexPath.row;
    cell.bookNowButton.tag = indexPath.section*1000 +indexPath.row;
    cell.chooseSiteLocationLabel.tag  = indexPath.section*1000 +indexPath.row;
    //indexPath.row;
//    cell.subLocationLbl.tag = indexPath.row;
    if (indexPath.section == 0) {
        cell.nextAvailableTimingView.hidden = TRUE;
    } else {
        cell.nextAvailableTimingView.hidden = FALSE;
    }
    [cell.bookNowButton addTarget:self action:@selector(bookNowButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *sublocationGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goSublocationViewUILbl:)];
    sublocationGestureRecognizer.view.tag = indexPath.section*1000 +indexPath.row;//indexPath.row;
    
    sublocationGestureRecognizer.numberOfTapsRequired = 1;
    cell.chooseSiteLocationLabel.userInteractionEnabled = YES;

    CarDetailInformation *aCarDetailInfo;

    if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        
        if (indexPath.section == 0) {
            aCarDetailInfo = isFilter ? arrFilterData[indexPath.row]: requiredArray[indexPath.row];
        } else {
            aCarDetailInfo = isFilter ? nextArrFilterData[indexPath.row] : nextRequiredArray[indexPath.row];
        }
        
        if ([cell.chooseSiteLocationLabel.text isEqualToString:@"Choose Site Location"]) {
            [cell.chooseSiteLocationLabel addGestureRecognizer:sublocationGestureRecognizer];

        }


        //cell.bookingBtn.backgroundColor = [UIColor whiteColor];
        //cell.bookingBtn.layer.cornerRadius = 5;
        //[cell.subLocationLbl addGestureRecognizer:sublocationGestureRecognizer];

        //[cell.sublocationBtn addTarget:self action:@selector(goSublocationView:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        
        if (indexPath.section == 0) {
            aCarDetailInfo = isFilter ? arrFilterData[indexPath.row]: requiredDoorStepArray[indexPath.row];
        } else {
            aCarDetailInfo = isFilter ? nextArrFilterData[indexPath.row]: nextRequiredDoorstepArray[indexPath.row];
        }
        
        //removing target for sublocationBtn
        //[cell.sublocationBtn addTarget:self action:@selector(goSublocationView:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    cell.carDetails = aCarDetailInfo;
    if (aCarDetailInfo.IsSpeedGovernor) {
        cell.upperHeightConstraint.constant = 25;
        cell.speedGovernerLabel.hidden = FALSE;
    } else {
        cell.upperHeightConstraint.constant = 15;
        cell.speedGovernerLabel.hidden = TRUE;
    }
    if( [[aCarDetailInfo.Model pathExtension] caseInsensitiveCompare:@" at"] == NSOrderedSame ) {
        cell.carTypeLabel.text = @"Automatic";

    }
    else{
        cell.carTypeLabel.text = @"Manual";
    }
    /*NSDictionary *attribModelName = @{
                              NSFontAttributeName: [UIFont fontWithName:@"Roboto-Bold" size:15]
                              };
    NSDictionary *attribVariantName = @{
                              NSFontAttributeName: [UIFont fontWithName:@"Roboto-Medium" size:12.0]
                              };
    
    NSMutableAttributedString *attributedText1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", aCarDetailInfo.Model] attributes:attribModelName];
    
     NSMutableAttributedString *attributedText2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"(%@)", aCarDetailInfo.CarVariant] attributes:attribVariantName];
    
    NSMutableAttributedString *attributedFinal = [[NSMutableAttributedString alloc]init];
    [attributedFinal appendAttributedString:attributedText1];
    [attributedFinal appendAttributedString:attributedText2];*/
    
    cell.carNameLabel.text = aCarDetailInfo.Model;
    if (aCarDetailInfo.CarVariant != nil)
    {
        [cell.lblCarVariantName setHidden:NO];
        cell.lblCarVariantName.text = [NSString stringWithFormat:@"(%@)", aCarDetailInfo.CarVariant];
    }
    else
    {
        [cell.lblCarVariantName setHidden:YES];
        //cell.lblCarVariantName.text = [NSString stringWithFormat:@"(%@)", carinfo.CarVariant];
    }
    
    cell.fuelTypeLabel.text = [NSString stringWithFormat:@"%@",aCarDetailInfo.FuelType];
    cell.seatingCapacityLabel.text = [NSString stringWithFormat:@"%@ Seater",aCarDetailInfo.SeatingCapacity];
    
    NSLog(@"image Url %@ ----- %@",aCarDetailInfo.CarCatName,[NSURL URLWithString:aCarDetailInfo.CarImageThumb]);
    [cell.carImageView hnk_setImageFromURL:[NSURL URLWithString:aCarDetailInfo.CarImageThumb] placeholder:[UIImage imageNamed:k_defaultCarFullImg] success:^(UIImage *image) {
        cell.carImageView.image = image;
        [cell.carImageIndicator stopAnimating];
        cell.carImageIndicator.hidden = true;
        cell.carImageIndicator.contentMode = HNKScaleModeAspectFit;
    } failure:^(NSError *error) {
        NSLog(@"Error = %@",[error description]);
    }];
    
    if ([aCarDetailInfo.TransmissionType isEqualToString:@"1"]){
        cell.carTypeLabel.text = @"Automatic";
    } else {
        cell.carTypeLabel.text = @"Manual";
    }
    
    //NEXT-AVAILABLE
    
    if ([self returnBothSection])
    {
        cell.pickUpDateAndTimeLabel.text = [NSString stringWithFormat:@"%@, %@",[Helper getDateWithMonthWithoutyearWithDateString:aCarDetailInfo.PickDate],[Helper getTimeInAMPMCarListWithTimeString:aCarDetailInfo.PickTime]];
        cell.dropOffDateAndTimeLabel.text = [NSString stringWithFormat:@"%@, %@",[Helper getDateWithMonthWithoutyearWithDateString:aCarDetailInfo.DropDate],[Helper getTimeInAMPMCarListWithTimeString:aCarDetailInfo.DropTime]];
        cell.timeDifferenceLabel.text = aCarDetailInfo.PickEarlyLateHour;
        cell.dropOffTimeDifferenceLabel.text = aCarDetailInfo.DropEarlyLateHour;

    }
    
    cell.bookNowButton.backgroundColor = [UIColor colorWithRed:225.0f/255.0f green:87.0f/255.0f blue:60.0f/255.0f alpha:1.0f];
    
    [cell.bookNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    
    [cell.bookNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    //&&dictSelectLoc[@"model"] == aCarDetailInfo.ModelID
    if (dictSelectLoc != nil && globalSection == indexPath.section && globalSelectedRow == indexPath.row)
    {
        
        //cell.chooseSiteLocationLabel.text = nil;
        cell.chooseSiteLocationLabel.text = dictSelectLoc[@"locName"];
//        cell.bookNowButton.backgroundColor = [UIColor colorWithRed:225.0f/255.0f green:87.0f/255.0f blue:60.0f/255.0f alpha:1.0f];
//
//        [cell.bookNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//
//
//        [cell.bookNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        //cell.bookNowButton.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
        cell.chooseSiteLocationLabel.textColor = [UIColor blackColor];
        
    }
    else
    {
        cell.chooseSiteLocationLabel.text = @"Choose Site Location";
        cell.chooseSiteLocationLabel.textColor = [UIColor grayColor];

//        cell.chooseSiteLocationLabel.textColor = [UIColor blackColor];

//        cell.bookNowButton.layer.borderWidth = 1.0;
//        cell.bookNowButton.backgroundColor = [UIColor clearColor];
//        [cell.bookNowButton setTitleColor:[UIColor colorWithRed:225.0f/255.0f green:87.0f/255.0f blue:60.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        //[UIColor colorWithRed:225.0f/255.0f green:87.0f/255.0f blue:60.0f/255.0f alpha:1.0f];
        
        
        //cell.bookingBtn.titleLabel.textColor = [UIColor redColor];
        cell.bookNowButton.layer.cornerRadius = 5;
    }
    
    if (self.pickupSegementedControl.selectedSegmentIndex == 1) {
        [cell.chooseSiteLocationView setHidden:TRUE];
    } else {
        [cell.chooseSiteLocationView setHidden:FALSE];
    }

    if(aCarDetailInfo.AllPackages.count > 1){
            [cell.collectionView setHidden:FALSE];
            [cell.indicatedPriceLabel setHidden:TRUE];
            [cell.collectionView reloadData];
            [cell.seaterLabel setHidden:TRUE];
            [cell.priceAmountLabel setHidden:TRUE];
            BOOL *match  = FALSE; // to find initial loading or not , we need to select first package initially
            for (int i = 0; i < [aCarDetailInfo.AllPackages count]; i++)
            {
                CarPackageDetails *package = aCarDetailInfo.AllPackages[i];
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
                if(package.isSelected){
                    
                    NSString *displaykilometer = [NSString stringWithFormat:@"%@",package.KMIncluded];
                    float kilometer = [displaykilometer doubleValue];
                    NSLog(@"kilometer tapped = %f",kilometer);
                    //dispatch_async(dispatch_get_main_queue(), ^{
                    if(kilometer == 0){
                        cell.additionalPaymentLabel.text = @"*No Additional KM Charge";
                        
                    }
                    else{
                        cell.additionalPaymentLabel.text = [NSString stringWithFormat:@"*Additional ₹ %@/KM | Exclusive Taxes",package.ExtraKMRate];
                        
                    }
                    
                   
                    
                    packageDetails = package.PkgDescription;
                   /* cell.additionalPaymentLabel.text = @"*No Additional KM Charge";

                    if ([package.ExtraKMRate isEqualToString:@"0"]) {
                        cell.additionalPaymentLabel.text = @"*No Additional KM Charge";
                    } else {
                        cell.additionalPaymentLabel.text = [NSString stringWithFormat:@"*Additional ₹ %@/KM",package.ExtraKMRate];
                    }*/
                    
                    [cell.collectionView selectItemAtIndexPath:indexpath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
                    match = TRUE;
                    
                }
                else{
                    [cell.collectionView deselectItemAtIndexPath:indexpath animated:YES];
                }
                
            }
            if(!match){
                CarPackageDetails *package = aCarDetailInfo.AllPackages[0];
                package.isSelected = TRUE;
                packageDetails = package.PkgDescription;
                cell.additionalPaymentLabel.text = [NSString stringWithFormat:@"*Additional ₹ %@/KM | Exclusive Taxes",package.ExtraKMRate];

//                if ([package.PkgType isEqualToString:@"Hourly"]) {
//                    cell.fuelPackageLabel.text = @"Free";
//
//
//
//                } else {
//                    cell.fuelPackageLabel.text = @"Full to Full";
//
//
//                }
               
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [cell.collectionView selectItemAtIndexPath:indexpath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
            }
            
        }
        else{
            NSLog(@"indexofcell = %ld",(long)indexPath.row);
//            [cell.additionalPaymentLabel setHidden:TRUE];
            cell.additionalPaymentLabel.text = @"*No Additional KM Charge";
            [cell.collectionView setHidden:TRUE];
            [cell.indicatedPriceLabel setHidden:FALSE];
            [cell.collectionView reloadData];
            [cell.seaterLabel setHidden:FALSE];
            [cell.priceAmountLabel setHidden:FALSE];
            CarPackageDetails *package = aCarDetailInfo.AllPackages[0];
            packageDetails = package.PkgDescription;
            NSString *displaykilometer = [NSString stringWithFormat:@"%@",package.KMIncluded];
            float kilometer = [displaykilometer doubleValue];
            if(kilometer == 0){
                cell.seaterLabel.text = package.PkgDescription;
            }
            else{                
                cell.additionalPaymentLabel.text = [NSString stringWithFormat:@"*Additional ₹ %@/KM | Exclusive Taxes",package.ExtraKMRate];
                
                /*if ([package.PkgType isEqualToString:@"Hourly"]) {
                    cell.additionalPaymentLabel.text = [NSString stringWithFormat:@"*Additional ₹ %@/KM | Exclusive Taxes",package.ExtraKMRate];
                }*/ //Swati Commented This If Condition on 15 May 2020
                cell.seaterLabel.text = [NSString stringWithFormat:@"%@ Kms Free",package.KMIncluded];
            }
            
            NSString *displayRate = [NSString stringWithFormat:@"%@",package.AfterDiscountAmount];   //[NSString stringWithFormat:@"%@",package.IndicatedPrice];  //Swati Commmented This
            
            float rate = [displayRate doubleValue];
            roundedRate = roundf(rate);
            
            
            NSString *displayRate1 = [NSString stringWithFormat:@"%@",package.IndicatedPrice];  //Swati Commmented This
            
            float rate1 = [displayRate1 doubleValue];
            int roundedRate1 = roundf(rate1);
            // NSLog(@"%@",aCarDetailInfo.AllPackages);
            //cell.priceAmountLabel.text = [NSString stringWithFormat:@"₹ %d\n ₹ %d",roundedRate1,roundedRate];
            cell.priceAmountLabel.text = [NSString stringWithFormat:@"₹ %d",roundedRate];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" ₹ %d ",roundedRate1]];
            [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                    value:@2
                                    range:NSMakeRange(0, [attributeString length])];
            cell.indicatedPriceLabel.attributedText = attributeString; //[NSString stringWithFormat:@"₹ %d",roundedRate1];
            if (package.PromotionCode != nil) {
                NSLog(@"Promo Code = %@",package.PromotionCode);
                cell.viewPromoCode.hidden = false;
                cell.lblPromoCode.hidden = false;
                cell.lblPromoCode.text = [NSString stringWithFormat:@"Promo Code Applied: %@", package.PromotionCode];
            }
            else {
                cell.viewPromoCode.hidden = true;
                cell.lblPromoCode.hidden = true;
            }
        }
    
    if ([aCarDetailInfo.PkgType isEqualToString:@"Hourly"]) {
        cell.fuelPackageLabel.text = @"Free";
    } else {
        cell.fuelPackageLabel.text = @"Full to Full";
        
        
    }

    __block NextAvailableCell *nextCellSelf = cell;
    //Calling block method to get tap on cell
    [cell setDidTapCollectionViewBlock:^(NSInteger index , BOOL isSelected )
     {
         NSLog(@"Index tapped = %ld",(long)index);
         CarPackageDetails *selectedPackage = aCarDetailInfo.AllPackages[index];
         NSString *displaykilometer = [NSString stringWithFormat:@"%@",selectedPackage.KMIncluded];
         float kilometer = [displaykilometer doubleValue];
         NSLog(@"kilometer tapped = %f",kilometer);
         //dispatch_async(dispatch_get_main_queue(), ^{
         
         NSLog(@"Extra KM Charger: %@",selectedPackage.ExtraKMRate);
             if(kilometer == 0){
                 
                 if (selectedPackage.ExtraKMRate.floatValue>0) {
                     nextCellSelf.additionalPaymentLabel.text = [NSString stringWithFormat:@"*Additional ₹ %@/KM",selectedPackage.ExtraKMRate];
                 }
                 else
                 {
                     nextCellSelf.additionalPaymentLabel.text = @"*No Additional KM Charge";
                 }
                 
                 
                 
             }
             else{
                 nextCellSelf.additionalPaymentLabel.text = [NSString stringWithFormat:@"*Additional ₹ %@/KM",selectedPackage.ExtraKMRate];
                 
             }
         
         //});
         
//         if ([package.PkgType isEqualToString:@"Hourly"]) {
//             nextCellSelf.fuelPackageLabel.text = @"Free";
//
//
//
//         } else {
//             nextCellSelf.fuelPackageLabel.text = @"Full to Full";
//
//
//         }
         if(isSelected){
             selectedPackage.isSelected = TRUE;
            // packageDetails = [NSString stringWithFormat:@"%@ %@", selectedPackage.IndicatedPrice ,selectedPackage.KMIncluded];
             packageDetails = selectedPackage.PkgDescription;
         }
         else{
             selectedPackage.isSelected = FALSE;
         }
     }];
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self movetoBookingScreen:indexPath];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        if (indexPath.section == 0) {
            return 250; //198;
        } else {
            return 298;
        }
    } else {
        if (indexPath.section == 0) {
            return 220;  //163
        } else {
            return 298;
        }
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return [tableView dequeueReusableCellWithIdentifier:@"NextSectionHeaderCell"];
    } else {
        return nil;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 80;
    } else {
        return -1;
    }
}


#pragma mark- Cell Button Action
- (void) bookNowButtonClick : (UIButton *)sender
{
    
    //_isSublocationSelected = issublocationSelected;
    
    NSInteger section = (sender.tag)/1000;
    NSInteger row = (sender.tag)%1000;
    globalSection = section;
    globalSelectedRow = row;
    index = [NSIndexPath indexPathForRow:row inSection:section];
    CarDetailInformation *aCarDetailInfo;
   if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        if (index.section == 0)
            aCarDetailInfo = isFilter ? arrFilterData[index.row]:requiredArray[index.row];
        else
            aCarDetailInfo = isFilter ? nextArrFilterData[index.row]:nextRequiredArray[index.row];
    } else {
        if (index.section == 0)
            aCarDetailInfo = isFilter ? arrFilterData[index.row]:requiredDoorStepArray[index.row];
        else
            aCarDetailInfo = isFilter ? nextArrFilterData[index.row]:nextRequiredDoorstepArray[index.row];
        
        //aCarDetailInfo = isFilter ? arrFilterData[indexSent.row]:requiredDoorStepArray[indexSent.row];
    }
    /*if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        if (section == 0)
            aCarDetailInfo = requiredArray[index.row];
        else
            aCarDetailInfo = nextRequiredArray[index.row];
    } else {
        if (section == 0)
            aCarDetailInfo = requiredDoorStepArray[index.row];
        else
            aCarDetailInfo = nextRequiredDoorstepArray[index.row];
            
        //aCarDetailInfo = requiredDoorStepArray[index.row];
    }*/
    
    NextAvailableCell *cell =(NextAvailableCell *) [self.tableView cellForRowAtIndexPath:index];
    //cell.subLocationLbl.text = nil;
    NSString *lblText = cell.chooseSiteLocationLabel.text;
    
    if(fireEvent){
        
        fireEvent = FALSE;
        if(aCarDetailInfo.IsSpeedGovernor){
            speedG = @"Yes";
        }
        else{
            speedG = @"No";
            
        }
        
        
        [FIRAnalytics logEventWithName:@"booknow_clicked_carlist"
                            parameters:@{@"pickup_city":aCarDetailInfo.CityName,
                                         @"pickup_type":pickupType,
                                         @"selected_package":packageDetails,
                                         @"selected_car_category":aCarDetailInfo.CarCatName,
                                         @"fuel_type":aCarDetailInfo.FuelType,
                                         @"transmission_type":aCarDetailInfo.TransmissionType,
                                         @"trip_duration":_tripDurationSelected,
                                         @"Package_type":packageTypeSelected,
                                         @"model_id":aCarDetailInfo.ModelID,
                                         @"model_name":aCarDetailInfo.Model,
                                         @"is_selected_carSG":speedG
                                         }];
    }
    else if (self.pickupSegementedControl.selectedSegmentIndex == 0 && _isSublocationSelected == YES && ![lblText isEqualToString:@"Choose Site Location"] && [DEFAULTS boolForKey:IS_LOGIN]) {
            
        if(aCarDetailInfo.IsSpeedGovernor){
            speedG = @"Yes";
        }
        else{
            speedG = @"No";
            
        }
        
        
        [FIRAnalytics logEventWithName:@"booknow_clicked_carlist"
                            parameters:@{@"pickup_cit'y":aCarDetailInfo.CityName,
                                         @"pickup_type":pickupType,
                                         @"selected_package":packageDetails,
                                         @"selected_car_category":aCarDetailInfo.CarCatName,
                                         @"fuel_type":aCarDetailInfo.FuelType,
                                         @"transmission_type":aCarDetailInfo.TransmissionType,
                                         @"trip_duration":_tripDurationSelected,
                                         @"Package_type":packageTypeSelected,
                                         @"model_id":aCarDetailInfo.ModelID,
                                         @"model_name":aCarDetailInfo.Model,
                                         @"is_selected_carSG":speedG
                                         }];
    }
    
    //NSLog(@"lblText =%@",lblText);
    
    //Sourabh- Adding Events Log //begin_checkout
   // [FIRAnalytics logEventWithName:kFIREventAddToCart
                        //parameters:@{kFIRParameterItemName:aCarDetailInfo.Model,
                                     //kFIRParameterItemID:aCarDetailInfo.ModelID
                                     //}];
   /* [FIRAnalytics logEventWithName:@"search_car"
                        parameters:@{
                                     @"user_type":userType,
                                     @"pickup_city":city,
                                     @"hours_to_trip_start":_timeToTripStart,
                                     @"trip_duration":_tripDurationSelected,
                                     @"package_type":packageTypeSelected,
                                     @"available_cars_current":current,
                                     @"available_cars_next":next,
                                     }];*/
    NSLog(@"%@",pickupType);
    NSLog(@"%@",packageDetails);
    NSLog(@"%@",_tripDurationSelected);
    NSLog(@"%@",packageTypeSelected);


    
    
    
    if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        if (_isSublocationSelected == YES && ![lblText isEqualToString:@"Choose Site Location"]) {
            
            if (![DEFAULTS boolForKey:IS_LOGIN]) {
                
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
                LoginProcess *editController = (LoginProcess *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginProcess"];
                
                UINavigationController *navigationController =
                [[UINavigationController alloc] initWithRootViewController:editController];
                editController.delegate = self;
                editController.strOtherDestination = @"0";
                editController.fromScreen = @"From_Booking";
                navigationController.viewControllers = @[editController];
                navigationController.modalPresentationStyle = UIModalPresentationFullScreen;

                [self presentViewController:navigationController animated:YES completion:nil];
                
                
            }else
            {
                
                if ([CommonFunctions reachabiltyCheck]) {
#ifdef DEBUG
                    previousDateTime = [CommonFunctions getCurrentDate];
                    previousMilliSec = [CommonFunctions getMilliSecond];
#endif
                    [self movetoBookingScreen:index];
                    
                }else {
                    
                    [self callForBooking];
                }
            }
        }
        else {
            
           /* //change as per ashish sir
            SublocationListViewController *subLocationListController = [self.storyboard instantiateViewControllerWithIdentifier:@"SublocationListViewController"];
            //NSIndexPath *index = [NSIndexPath indexPathForRow:sender.tag inSection:0];
            
            //CarDetailInformation *aCarDetailInfo = isFilter ? arrFilterData[index.row]:requiredArray[index.row];
            NSString *SublocationString = aCarDetailInfo.Detail;
            NSString *SublocationIDString = aCarDetailInfo.DetailID;
            //CarInfoListTableViewCell *cell =(CarInfoListTableViewCell *) [self.tableView cellForRowAtIndexPath:index];
            //cell.subLocationLbl.text = nil;
            if ([cell.chooseSiteLocationLabel.text isEqualToString:@"Choose Site Location"])
                subLocationListController.strSelectedData = @"";
            else
                subLocationListController.strSelectedData = cell.chooseSiteLocationLabel.text;
            
            subLocationListController.strModelId = aCarDetailInfo.ModelID;
            
            
            
            NSArray *sublocationList ,*sublocationIDList;
            if (isFilter && dictFilterData[@"location"] != nil)
            {
                NSArray *arrtemp =[SublocationString componentsSeparatedByString:@":"];
                NSArray *arrtempid =[SublocationIDString componentsSeparatedByString:@":"];
                NSString *SubLoc = @"" ,*Subid = @"";
                int i;
                for (i=0; i<[arrtemp count]; i++)
                {
                    if ([dictFilterData[@"location"] containsObject:arrtemp[i]])
                    {
                        
                        SubLoc = [SubLoc stringByAppendingString:[SubLoc.length>0 ? @":":SubLoc stringByAppendingString:arrtemp[i]]];
                        
                        Subid = [Subid stringByAppendingString:[Subid.length>0 ? @":":Subid stringByAppendingString:arrtempid[i]]];
                        
                    }
                }
                
                sublocationList = [SubLoc componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
                sublocationIDList = [Subid componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
            }
            else
            {
                sublocationList = [SublocationString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
                sublocationIDList = [SublocationIDString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
                
            }
            
            
            
            
            //CarInfoListTableViewCell *clickedCell1 = (CarInfoListTableViewCell *)[[sender superview] superview];
            
            //NSDictionary *sublocationDict = [NSDictionary dictionaryWithObjects:sublocationList forKeys:sublocationIDList];
            NSDictionary *sublocationDict = [NSDictionary dictionaryWithObjects:sublocationIDList forKeys:sublocationList];
            //NSLog(@"%@", sublocationDict);
            //NSLog(@"sublocationList count =%lu",(unsigned long)[sublocationList count]);
            
            
            
            subLocationListController.sublocationList = sublocationList;
            subLocationListController.sublocationData = sublocationDict;
            subLocationListController.selSublocationindex = index;
            subLocationListController.delegate = self;
            _isSublocationSelected = true;
            [self.navigationController pushViewController: subLocationListController animated:YES];
            
            //[CommonFunctions alertTitle:@"Message" withMessage:@"Please choose nearest Myles site location to proceed"];*/
            [self moveToSublocationView:index];

        }

    } else {
        if (![DEFAULTS boolForKey:IS_LOGIN]) {
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
            LoginProcess *editController = (LoginProcess *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginProcess"];
            
            UINavigationController *navigationController =
            [[UINavigationController alloc] initWithRootViewController:editController];
            editController.delegate = self;
            editController.strOtherDestination = @"0";
            navigationController.viewControllers = @[editController];
            [self presentViewController:navigationController animated:YES completion:nil];
            
            
        } else{
            
            if ([CommonFunctions reachabiltyCheck]) {
               /* if(aCarDetailInfo.IsSpeedGovernor){
                    speedG = @"Yes";
                }
                else{
                    speedG = @"No";
                    
                }
                [FIRAnalytics logEventWithName:@"booknow_clicked_carlist"
                                    parameters:@{@"pickup_city":aCarDetailInfo.CityName,
                                                 @"pickup_type":pickupType,
                                                 @"selected_package":packageDetails,
                                                 @"selected_car_category":aCarDetailInfo.CarCatName,
                                                 @"fuel_type":aCarDetailInfo.FuelType,
                                                 @"transmission_type":aCarDetailInfo.TransmissionType,
                                                 @"trip_duration":_tripDurationSelected,
                                                 @"Package_type":packageTypeSelected,
                                                 @"model_id":aCarDetailInfo.ModelID,
                                                 @"model_name":aCarDetailInfo.Model,
                                                 @"is_selected_carSG":speedG

                                                 }];*/
                [self movetoBookingScreen:index];
                
            }else {
                
                [self callForBooking];
            }
        }
    }
}

// By DIvya to handle no network validation

-(void)callForBooking {
    
    NSString *rsaNumber = [DEFAULTS valueForKey:kRoadsideSupport];
    NSString *mylesCenterNum = [DEFAULTS valueForKey:k_mylesCenterNumber];
    
    NSString *messageStr =[NSString stringWithFormat:@"Call on %@  to book your Myles ride",mylesCenterNum];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"No Internet Connection"
                                          message:messageStr
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"Cancel action");
                               }];
    
    UIAlertAction *callButton = [UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *url =[NSString stringWithFormat:@"tel:%@",mylesCenterNum];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:callButton];
    [self presentViewController:alertController animated:YES completion:nil];
}

//-(IBAction)goSublocationView:(UIButton *)sender {
//    
//    SublocationListViewController *subLocationListController = [self.storyboard instantiateViewControllerWithIdentifier:@"SublocationListViewController"];
//    NSIndexPath *index = [NSIndexPath indexPathForRow:sender.tag inSection:0];
//    
//    CarDetailInformation *aCarDetailInfo = isFilter ? arrFilterData[index.row] : requiredArray[index.row];
//    NSString *SublocationString = aCarDetailInfo.Detail;
//    NSString *SublocationIDString = aCarDetailInfo.DetailID;
//    
//    
//    CarInfoListTableViewCell *cell =(CarInfoListTableViewCell *) [self.tableView cellForRowAtIndexPath:index];
//    //cell.subLocationLbl.text = nil;
//    if ([cell.subLocationLbl.text isEqualToString:@"Choose Nearest Myles Site Location"])
//        subLocationListController.strSelectedData = @"";
//    else
//        subLocationListController.strSelectedData = cell.subLocationLbl.text;
//    //NSLog(@"SublocationString =%@",SublocationString);
//    
//    NSArray *sublocationList ,*sublocationIDList;
//    if (isFilter && dictFilterData[@"location"] != nil)
//    {
//        NSArray *arrtemp =[SublocationString componentsSeparatedByString:@":"];
//        NSArray *arrtempid =[SublocationIDString componentsSeparatedByString:@":"];
//        NSString *SubLoc = @"" ,*Subid = @"";
//        int i;
//        for (i=0; i<[arrtemp count]; i++)
//        {
//            if ([dictFilterData[@"location"] containsObject:arrtemp[i]])
//            {
//                
//                SubLoc = [SubLoc stringByAppendingString:[SubLoc.length>0 ? @":":SubLoc stringByAppendingString:arrtemp[i]]];
//                
//                Subid = [Subid stringByAppendingString:[Subid.length>0 ? @":":Subid stringByAppendingString:arrtempid[i]]];
//                
//            }
//        }
//        
//        sublocationList = [SubLoc componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
//        sublocationIDList = [Subid componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
//    }
//    else
//    {
//        sublocationList = [SublocationString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
//        sublocationIDList = [SublocationIDString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
//        
//    }
//    
//    //NSArray *sublocationList = [SublocationString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
//    //NSArray *sublocationIDList = [SublocationIDString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
//    
//    //NSDictionary *sublocationDict = [NSDictionary dictionaryWithObjects:sublocationList forKeys:sublocationIDList];
//    NSDictionary *sublocationDict = [NSDictionary dictionaryWithObjects:sublocationIDList forKeys:sublocationList];
//    //NSLog(@"%@", sublocationDict);
//    //NSLog(@"sublocationList count =%lu",(unsigned long)[sublocationList count]);
//    subLocationListController.strModelId = aCarDetailInfo.ModelID;
//    subLocationListController.sublocationList = sublocationList;
//    subLocationListController.sublocationData = sublocationDict;
//    subLocationListController.selSublocationindex = index;
//    subLocationListController.delegate = self;
//    
//    // CarInfoListTableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
//    
//    //subLocationListController.strSelectedData = sublocatio
//    //_isSublocationSelected = true;
//    [self.navigationController pushViewController: subLocationListController animated:YES];
//}

//Call for sublocation
-(IBAction)goSublocationViewUILbl:(UITapGestureRecognizer *)sender {
        NSInteger section = (sender.view.tag)/1000;
        NSInteger row = (sender.view.tag)%1000;
        globalSelectedRow = row;
        globalSection = section;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [self moveToSublocationView:indexPath];
}

- (void)moveToSublocationView:(NSIndexPath *)indexPath {
    if (self.pickupSegementedControl.selectedSegmentIndex == 0) {

        SublocationListViewController *subLocationListController = [self.storyboard instantiateViewControllerWithIdentifier:@"SublocationListViewController"];
        CarDetailInformation *aCarDetailInfo;
        if (indexPath.section == 0) {
            aCarDetailInfo  = isFilter ? arrFilterData[indexPath.row]:requiredArray[indexPath.row];
        } else {
            aCarDetailInfo = isFilter ? nextArrFilterData[indexPath.row]:nextRequiredArray[indexPath.row];
        }
    
        NSString *SublocationString = aCarDetailInfo.Detail;
        NSString *SublocationIDString= aCarDetailInfo.DetailID;
    
        NextAvailableCell *cell =(NextAvailableCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        //cell.subLocationLbl.text = nil;
        if ([cell.chooseSiteLocationLabel.text isEqualToString:@"Choose Site Location"])
            subLocationListController.strSelectedData = @"";
        else
            subLocationListController.strSelectedData = cell.chooseSiteLocationLabel.text;

        subLocationListController.strModelId = aCarDetailInfo.ModelID;
        
        SublocationListVC * objSUblocation = [SublocationListVC new];
//        objSUblocation.nnnnnnnnn = @"aaaaa"; xxxxxxxxx
    
        //For Filter - NextAvailable CAR
        NSArray *sublocationList ,*sublocationIDList;
        if (isFilter && dictFilterData[@"location"] != nil)
        {
            NSArray *arrtemp =[SublocationString componentsSeparatedByString:@":"];
            NSArray *arrtempid =[SublocationIDString componentsSeparatedByString:@":"];
            NSString *SubLoc = @"" ,*Subid = @"";
            int i;
            for (i=0; i<[arrtemp count]; i++)
            {
                if ([dictFilterData[@"location"] containsObject:arrtemp[i]])
                {
                
                    SubLoc = [SubLoc stringByAppendingString:[SubLoc.length>0 ? @":":SubLoc stringByAppendingString:arrtemp[i]]];
                
                    Subid = [Subid stringByAppendingString:[Subid.length>0 ? @":":Subid stringByAppendingString:arrtempid[i]]];
                
                }
            }
        
            sublocationList = [SubLoc componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
            sublocationIDList = [Subid componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
        }
        else
        {
            sublocationList = [SublocationString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
            sublocationIDList = [SublocationIDString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
        
        }
    
        NSDictionary *sublocationDict = [NSDictionary dictionaryWithObjects:sublocationIDList forKeys:sublocationList];
    
        subLocationListController.sublocationList = sublocationList;
        subLocationListController.sublocationData = sublocationDict;
        subLocationListController.selSublocationindex = index;
        subLocationListController.delegate = self;
        _isSublocationSelected = true;
        [self.navigationController pushViewController: subLocationListController animated:YES];
    }
}
/*
 @dev Abhishek
 */

- (void)selectedSublocationData:(NSString *)SublocationID SublocationTitle : (NSString *)title Sublocationindex : (NSIndexPath *)sublocationindex isSublocationSelected : (BOOL)issublocationSelected :(NSString *)strModelId {
    
    sublocationAddress = title;
    _isSublocationSelected = issublocationSelected;
    
    sublocationID = SublocationID;
    
    NSIndexPath *index = sublocationindex;
    _SublocationTitleVal = title;
    _sublocationIDVal = SublocationID;
    [self.tableView reloadData];
    
    if (dictSelectLoc == nil)
        dictSelectLoc = [[NSMutableDictionary alloc]init];
    else
        [dictSelectLoc removeAllObjects];
    if (title != nil) {
        [dictSelectLoc setObject:title forKey:@"locName"];
    }
    if (index != nil) {
        [dictSelectLoc setObject:index forKey:@"index"];
        
    }
    if (strModelId != nil) {
        [dictSelectLoc setObject:strModelId forKey:@"model"];
    }
    
    NextAvailableCell *cell = (NextAvailableCell *)[self.tableView cellForRowAtIndexPath:index];
    cell.chooseSiteLocationLabel.text = nil;
    //cell.bookingBtn.backgroundColor = [UIColor redColor];
    cell.bookNowButton.backgroundColor = [UIColor colorWithRed:225.0f/255.0f green:87.0f/255.0f blue:60.0f/255.0f alpha:1.0f];
    //cell.bookingBtn.layer.cornerRadius = 5;
    // cell.bookingBtn.titleLabel.textColor = [UIColor whiteColor];
    [cell.bookNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //cell.chooseSiteLocationLabel.textColor = [UIColor blackColor];
    cell.chooseSiteLocationLabel.text = title;
}

#pragma mark- Move To Sublocation Screen
- (void) movetoSublocationScreen : (NSIndexPath *)index
{    
    CarInfoListTableViewCell *cell =(CarInfoListTableViewCell *) [self.tableView cellForRowAtIndexPath:index];
    CarDetailInformation *aCarDetailInfo = requiredArray[index.row];
    
    NSString *carmodelID = cell.carModelID;
    carmodelID  = aCarDetailInfo.ModelID;
    
    //NSLog(@"sublocationID =%@",sublocationID);
    cell.subLocationId = sublocationID;
    
    NSString *CityID = [NSString stringWithFormat:@"%ld",(long)_selectedCityId];
    NSString *sublocationid = (cell.subLocationId == nil) ? @"0" : cell.subLocationId;
    
    BookingDetailsVC *bookingDetail = [self.storyboard instantiateViewControllerWithIdentifier:k_BookingDetailsVC];
    NSDictionary *durationDict = [[NSDictionary alloc] initWithObjects:@[self.pickUpDateStr,self.pickUpTime,self.DropOffDateStr,self.dropOffTime] forKeys:@[@"pDate",@"pTime",@"dDate",@"dTime"]];
    
    NSDictionary *carData = [[NSDictionary alloc]initWithObjectsAndKeys: CityID,k_cityIDCap,carmodelID,k_CarModelId, sublocationid,k_SubLocationsID ,sublocationAddress,k_SubLocationAddress,aCarDetailInfo.PkgType,k_PkgType,aCarDetailInfo.CarImageHD,k_urlFullImage, nil];
    
    //NSLog(@"carData =%@",carData);
    bookingDetail.subLocationID = [NSString stringWithFormat:@"%@", [carData valueForKey:k_SubLocationsID]];
    [bookingDetail setKBookingDurationMD:durationDict];
    NSLog(@"self.carGSTPackageDetails: %@", self.carGSTPackageDetails);
    bookingDetail.bookingGSTPackageDetails = self.carGSTPackageDetails;
    
    NSDictionary *carDataDetail = [[NSDictionary alloc] initWithObjects:@[aCarDetailInfo.AgeMessage,aCarDetailInfo.AirportCharges,aCarDetailInfo.BasicAmt,aCarDetailInfo.CarCatID,aCarDetailInfo.CarCatName,aCarDetailInfo.CarImageHD,aCarDetailInfo.CarImageThumb,aCarDetailInfo.CityName,aCarDetailInfo.DepositeAmt,aCarDetailInfo.Detail,aCarDetailInfo.DetailID,aCarDetailInfo.ExtraKMRate,aCarDetailInfo.FreeDuration,aCarDetailInfo.FuelType,aCarDetailInfo.IndicatedPrice,aCarDetailInfo.IsAvailable,aCarDetailInfo.KMIncluded,aCarDetailInfo.LuggageCapacity,aCarDetailInfo.Model,aCarDetailInfo.ModelID,aCarDetailInfo.OriginalAmt,aCarDetailInfo.PkgId,aCarDetailInfo.PkgRate,aCarDetailInfo.PkgRateWeekEnd,aCarDetailInfo.PkgType,aCarDetailInfo.SeatingCapacity,aCarDetailInfo.SubLocationCost,aCarDetailInfo.TotalDuration,aCarDetailInfo.TransmissionType,aCarDetailInfo.VatAmt,aCarDetailInfo.VatRate,aCarDetailInfo.WeekDayDuration,aCarDetailInfo.WeekEndDuration,aCarDetailInfo.CGSTAmount,aCarDetailInfo.CGSTRate,aCarDetailInfo.IGSTAmount,aCarDetailInfo.IGSTRate,aCarDetailInfo.SGSTAmount,aCarDetailInfo.SGSTRate,aCarDetailInfo.UTGSTAmount,aCarDetailInfo.UTGSTRate, aCarDetailInfo.CessRate, aCarDetailInfo.CessAmount, aCarDetailInfo.UTGSTRate_Service, aCarDetailInfo.IGSTRate_Service, aCarDetailInfo.SGSTRate_Service, aCarDetailInfo.CGSTRate_Service, aCarDetailInfo.CarVariant,aCarDetailInfo.CarVariantID] forKeys:@[@"AgeMessage",@"AirportCharges",@"BasicAmt",@"CarCatID",@"CarCatName",@"CarImageHD",@"CarImageThumb",@"CityName",@"DepositeAmt",@"Detail",@"DetailID",@"ExtraKMRate",@"FreeDuration",@"FuelType",@"IndicatedPrice",@"IsAvailable",@"KMIncluded",@"LuggageCapacity",@"Model",@"ModelID",@"OriginalAmt",@"PkgId",@"PkgRate",@"PkgRateWeekEnd",@"PkgType",@"SeatingCapacity",@"SubLocationCost",@"TotalDuration",@"TransmissionType",@"VatAmt",@"VatRate",@"WeekDayDuration",@"WeekEndDuration",@"CGSTAmount",@"CGSTRate",@"IGSTAmount",@"IGSTRate",@"SGSTAmount",@"SGSTRate",@"UTGSTAmount",@"UTGSTRate",@"CessRate",@"CessAmount",@"UTGSTRate_Service",@"IGSTRate_Service",@"SGSTRate_Service",@"CGSTRate_Service",@"CarVariant", @"CarVariantID"]];
    
    
    //NSLog(@"carDataDetail = %@",carDataDetail);
    [bookingDetail setCarDetails:carDataDetail AndAddress:[NSString stringWithFormat:@"%@",[carData valueForKey:k_SubLocationAddress]] City:[carData valueForKey:k_cityIDCap]CarImg:[carData valueForKey:k_urlFullImage] pickupType: pickupType TripDuration:_tripDurationSelected packageType : packageTypeSelected ];
    
    if (aCarDetailInfo.PromotionCode != nil) {
        cell.lblPromoCode.text = [NSString stringWithFormat:@"(%@)", aCarDetailInfo.PromotionCode];
    }
    else {
        // No joy...
    }
    
#ifdef DEBUG
    bookingDetail.previousDateTime = previousDateTime;
    bookingDetail.previousMilliSec = [CommonFunctions getMilliSecond];
    
#endif
    [self.navigationController pushViewController:bookingDetail animated:true];
}

#pragma mark- Move To Booking Screen
- (void) movetoBookingScreen : (NSIndexPath *)indexSent
{
    NextAvailableCell *cell =(NextAvailableCell *) [self.tableView cellForRowAtIndexPath:indexSent];
    CarDetailInformation *aCarDetailInfo;
    if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        if (indexSent.section == 0)
            aCarDetailInfo = isFilter ? arrFilterData[indexSent.row]:requiredArray[indexSent.row];
        else
            aCarDetailInfo = isFilter ? nextArrFilterData[indexSent.row]:nextRequiredArray[indexSent.row];
    } else {
        if (indexSent.section == 0)
            aCarDetailInfo = isFilter ? arrFilterData[indexSent.row]:requiredDoorStepArray[indexSent.row];
        else
            aCarDetailInfo = isFilter ? nextArrFilterData[indexSent.row]:nextRequiredDoorstepArray[indexSent.row];
        
        //aCarDetailInfo = isFilter ? arrFilterData[indexSent.row]:requiredDoorStepArray[indexSent.row];
    }
    
    
    NSString *carmodelID; //= cell.carModelID;
    carmodelID  = aCarDetailInfo.ModelID;
    
    //NSLog(@"sublocationID =%@",sublocationID);
    //cell.subLocationId = sublocationID;

    NSString *CityID = [NSString stringWithFormat:@"%ld",(long)_selectedCityId];
    NSString *sublocationid = (sublocationID == nil) ? @"0" : sublocationID;
    
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject: sublocationid forKey:@"SublocationID"];
        
        //CarDetailInformation *aCarDetailInfo = requiredArray[index.row];
        
        
        
        NSString *carmodelID; //= aCarDetailInfo.ModelID
        
        carmodelID  = aCarDetailInfo.ModelID;
        
        // Assign the globalSelectedPackage
        if (aCarDetailInfo.AllPackages.count > 1) {
            for (CarPackageDetails *tempSelectedPackage in aCarDetailInfo.AllPackages){
                if (tempSelectedPackage.isSelected) {
                    globalSelectedPackage = (CarPackageDetails *)[tempSelectedPackage copy];
                }
            }
        } else {
            CarPackageDetails *package = aCarDetailInfo.AllPackages[0];
            globalSelectedPackage = [package copy];
        }
        
        
        NSString *CityID = [NSString stringWithFormat:@"%ld",(long)_selectedCityId];
        
        //NSString *sublocationid = (cell.subLocationId == nil) ? @"0" : cell.subLocationId;
        
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            NSLog(@"Swati CarDetail Information: %@",aCarDetailInfo);
            NSMutableArray *sublocationArray = aCarDetailInfo.Sublocation;
            
            for (int i = 0 ; i < sublocationArray.count; i++)
            {
                Sublocation *tempSubLocation = sublocationArray[i];
                if(sublocationid == tempSubLocation.DetailID){
                    if ([tempSubLocation.ServiceType isEqualToString:@"C"]) {
                        
                        aCarDetailInfo.AirportCharges = @"0.00";
                        
                    }
                    
                    else {
                        
                        aCarDetailInfo.AirportCharges = tempSubLocation.AirportCharges;
                        globalSelectedPackage.AirportCharges = tempSubLocation.AirportCharges;
                    }
                    break;
                }
            }
            BookingDetailsVC *bookingDetail = [self.storyboard instantiateViewControllerWithIdentifier:k_BookingDetailsVC];
            
            
            
            NSDictionary *durationDict = [[NSDictionary alloc] initWithObjects:@[aCarDetailInfo.PickDate,aCarDetailInfo.PickTime,aCarDetailInfo.DropDate,aCarDetailInfo.DropTime] forKeys:@[@"pDate",@"pTime",@"dDate",@"dTime"]];
            NSLog(@"durationDict = %@",durationDict);
            //NSLog(@"durationDict1 = %@",durationDict1);
            NSLog(@"self.carGSTPackageDetails: %@", self.carGSTPackageDetails);
            bookingDetail.bookingGSTPackageDetails = self.carGSTPackageDetails;
            bookingDetail.doorStepDelieverySelected = FALSE;
            
            NSDictionary *carData = [[NSDictionary alloc]initWithObjectsAndKeys: CityID,k_cityIDCap,carmodelID,k_CarModelId, _sublocationIDVal,k_SubLocationsID ,sublocationAddress,k_SubLocationAddress,globalSelectedPackage.PkgType,k_PkgType,aCarDetailInfo.CarImageHD,k_urlFullImage, nil];
            
            
            
            //NSLog(@"carData =%@",carData);
            
            bookingDetail.subLocationID = [NSString stringWithFormat:@"%@", [carData valueForKey:k_SubLocationsID]];
            
            [bookingDetail setKBookingDurationMD:durationDict];
            
            NSLog(@"Insurance Security Amt: %@",aCarDetailInfo.InsuranceSecurityAmt);
            
            NSString* insuranceStr = aCarDetailInfo.InsuranceSecurityAmt;
            if ([insuranceStr isKindOfClass:[NSNull class]] || insuranceStr == nil)
            {
                aCarDetailInfo.InsuranceSecurityAmt = @"0.00";
            }
            
            NSDictionary *carDataDetail = [[NSDictionary alloc] initWithObjects:@[aCarDetailInfo.AgeMessage,globalSelectedPackage.AirportCharges,globalSelectedPackage.BasicAmt,aCarDetailInfo.CarCatID,aCarDetailInfo.CarCatName,aCarDetailInfo.CarImageHD,aCarDetailInfo.CarImageThumb,aCarDetailInfo.CityName,globalSelectedPackage.DepositeAmt,aCarDetailInfo.Detail,aCarDetailInfo.DetailID,globalSelectedPackage.ExtraKMRate,globalSelectedPackage.FreeDuration,aCarDetailInfo.FuelType,globalSelectedPackage.IndicatedPrice,aCarDetailInfo.IsAvailable,globalSelectedPackage.KMIncluded,aCarDetailInfo.LuggageCapacity,aCarDetailInfo.Model,aCarDetailInfo.ModelID,globalSelectedPackage.OriginalAmt,globalSelectedPackage.PkgId,globalSelectedPackage.PkgRate,globalSelectedPackage.PkgRateWeekEnd,globalSelectedPackage.PkgType,aCarDetailInfo.SeatingCapacity,globalSelectedPackage.SubLocationCost,globalSelectedPackage.TotalDuration,aCarDetailInfo.TransmissionType,globalSelectedPackage.VatAmt,globalSelectedPackage.VatRate,globalSelectedPackage.WeekDayDuration,globalSelectedPackage.WeekEndDuration,globalSelectedPackage.TotalInsuranceAmt,aCarDetailInfo.CoricId,globalSelectedPackage.InsuranceAmt,globalSelectedPackage.CGSTAmount,globalSelectedPackage.CGSTRate,globalSelectedPackage.IGSTAmount,globalSelectedPackage.IGSTRate,globalSelectedPackage.SGSTAmount,globalSelectedPackage.SGSTRate,globalSelectedPackage.UTGSTAmount,globalSelectedPackage.UTGSTRate,aCarDetailInfo.TransmissionType,aCarDetailInfo.FuelType,aCarDetailInfo.LuggageCapacity,aCarDetailInfo.SeatingCapacity,aCarDetailInfo.InsuranceSecurityAmt,aCarDetailInfo.CessRate, aCarDetailInfo.CessAmount, aCarDetailInfo.UTGSTRate_Service, aCarDetailInfo.IGSTRate_Service, aCarDetailInfo.SGSTRate_Service, aCarDetailInfo.CGSTRate_Service,aCarDetailInfo.CarVariant,aCarDetailInfo.CarVariantID,aCarDetailInfo.PromotionCode,aCarDetailInfo.AfterDiscountAmount, aCarDetailInfo.DiscountAmt] forKeys:@[@"AgeMessage",@"AirportCharges",@"BasicAmt",@"CarCatID",@"CarCatName",@"CarImageHD",@"CarImageThumb",@"CityName",@"DepositeAmt",@"Detail",@"DetailID",@"ExtraKMRate",@"FreeDuration",@"FuelType",@"IndicatedPrice",@"IsAvailable",@"KMIncluded",@"LuggageCapacity",@"Model",@"ModelID",@"OriginalAmt",@"PkgId",@"PkgRate",@"PkgRateWeekEnd",@"PkgType",@"SeatingCapacity",@"SubLocationCost",@"TotalDuration",@"TransmissionType",@"VatAmt",@"VatRate",@"WeekDayDuration",@"WeekEndDuration",@"TotalInsuranceAmt",@"CoricId",@"InsuranceAmt",@"CGSTAmount",@"CGSTRate",@"IGSTAmount",@"IGSTRate",@"SGSTAmount",@"SGSTRate",@"UTGSTAmount",@"UTGSTRate",@"TransmissionType",@"FuelType",@"Luggage",@"SeatingCapacity",@"InsuranceSecurityAmt",@"CessRate",@"CessAmount",@"UTGSTRate_Service",@"IGSTRate_Service",@"SGSTRate_Service",@"CGSTRate_Service",@"CarVariant", @"CarVariantID",@"PromotionCode", @"AfterDiscountAmount", @"DiscountAmt"]];
            
            
            
            bookingDetail.speedGovernerIsActive = aCarDetailInfo.IsSpeedGovernor;
            bookingDetail.TotalDurationForDisplay = globalSelectedPackage.TotalDurationForDisplay;
            
            
            [bookingDetail setCarDetails:carDataDetail AndAddress:[NSString stringWithFormat:@"%@",[carData valueForKey:k_SubLocationAddress]] City:[carData valueForKey:k_cityIDCap]CarImg:[carData valueForKey:k_urlFullImage] pickupType: pickupType TripDuration:_tripDurationSelected packageType : packageTypeSelected ]  ;
            
            bookingDetail.selectedPackage = globalSelectedPackage;
#ifdef DEBUG
            bookingDetail.previousDateTime = previousDateTime;
            bookingDetail.previousMilliSec = [CommonFunctions getMilliSecond];

#endif
            [self.navigationController pushViewController:bookingDetail animated:true];
           /* if ([CommonFunctions reachabiltyCheck]) {
                
                NSMutableDictionary *body = [NSMutableDictionary new];
                
                [body setObject: sublocationid forKey:@"SublocationID"];
                
                NSURLSession *session = [CommonFunctions defaultSession];
                
                [self showLoader];
                
                NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
                
                APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kGetSublocationAirportCharges completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                                       
                                                       {
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [SVProgressHUD dismiss];
                                                           });
                                                           
                                                           NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                           NSString *statusStr = [headers objectForKey:@"Status"];
                                                           if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                           {
                                                               
                                                               if (!error){
                                                                   
                                                                   
                                                                   
                                                                   NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                                   
                                                                   
                                                                   
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       
                                                                       NSInteger status = [[responseJson valueForKey:@"status"] integerValue];
                                                                       
                                                                       if (status == 1)
                                                                           
                                                                       {
                                                                           
                                                                           NSMutableDictionary *finalResponseJson = [(NSMutableDictionary *)responseJson objectForKey:@"response"];
                                                                           
                                                                           
                                                                           
                                                                           NSString *isSublocationType = [finalResponseJson valueForKey:@"SublocationType"];
                                                                           
                                                                           
                                                                           
                                                                           if ([isSublocationType isEqualToString:@"C"]) {
                                                                               
                                                                               aCarDetailInfo.AirportCharges = @"0.00";
                                                                               
                                                                           }
                                                                           
                                                                           else {
                                                                               
                                                                               aCarDetailInfo.AirportCharges = [finalResponseJson valueForKey:@"AirportCharges"];
                                                                               globalSelectedPackage.AirportCharges = [finalResponseJson valueForKey:@"AirportCharges"];

                                                                           }
                                                                           
                                                                           
                                                                           
                                                                           BookingDetailsVC *bookingDetail = [self.storyboard instantiateViewControllerWithIdentifier:k_BookingDetailsVC];
                                                                           
                                                                          
                                                                           
                                                                           NSDictionary *durationDict = [[NSDictionary alloc] initWithObjects:@[aCarDetailInfo.PickDate,aCarDetailInfo.PickTime,aCarDetailInfo.DropDate,aCarDetailInfo.DropTime] forKeys:@[@"pDate",@"pTime",@"dDate",@"dTime"]];
                                                                           NSLog(@"durationDict = %@",durationDict);
                                                                           //NSLog(@"durationDict1 = %@",durationDict1);

                                                                           bookingDetail.doorStepDelieverySelected = FALSE;
                                                                           
                                                                           NSDictionary *carData = [[NSDictionary alloc]initWithObjectsAndKeys: CityID,k_cityIDCap,carmodelID,k_CarModelId, _sublocationIDVal,k_SubLocationsID ,sublocationAddress,k_SubLocationAddress,globalSelectedPackage.PkgType,k_PkgType,aCarDetailInfo.CarImageHD,k_urlFullImage, nil];
                                                                           
                                                                           
                                                                           
                                                                           //NSLog(@"carData =%@",carData);
                                                                           
                                                                           bookingDetail.subLocationID = [NSString stringWithFormat:@"%@", [carData valueForKey:k_SubLocationsID]];
                                                                           
                                                                           [bookingDetail setKBookingDurationMD:durationDict];
                                                                           
                                                                           
                                                                           
                                                                           NSDictionary *carDataDetail = [[NSDictionary alloc] initWithObjects:@[aCarDetailInfo.AgeMessage,globalSelectedPackage.AirportCharges,globalSelectedPackage.BasicAmt,aCarDetailInfo.CarCatID,aCarDetailInfo.CarCatName,aCarDetailInfo.CarImageHD,aCarDetailInfo.CarImageThumb,aCarDetailInfo.CityName,globalSelectedPackage.DepositeAmt,aCarDetailInfo.Detail,aCarDetailInfo.DetailID,globalSelectedPackage.ExtraKMRate,globalSelectedPackage.FreeDuration,aCarDetailInfo.FuelType,globalSelectedPackage.IndicatedPrice,aCarDetailInfo.IsAvailable,globalSelectedPackage.KMIncluded,aCarDetailInfo.LuggageCapacity,aCarDetailInfo.Model,aCarDetailInfo.ModelID,globalSelectedPackage.OriginalAmt,globalSelectedPackage.PkgId,globalSelectedPackage.PkgRate,globalSelectedPackage.PkgRateWeekEnd,globalSelectedPackage.PkgType,aCarDetailInfo.SeatingCapacity,globalSelectedPackage.SubLocationCost,globalSelectedPackage.TotalDuration,aCarDetailInfo.TransmissionType,globalSelectedPackage.VatAmt,globalSelectedPackage.VatRate,globalSelectedPackage.WeekDayDuration,globalSelectedPackage.WeekEndDuration,globalSelectedPackage.TotalInsuranceAmt,aCarDetailInfo.CoricId,globalSelectedPackage.InsuranceAmt,globalSelectedPackage.CGSTAmount,globalSelectedPackage.CGSTRate,globalSelectedPackage.IGSTAmount,globalSelectedPackage.IGSTRate,globalSelectedPackage.SGSTAmount,globalSelectedPackage.SGSTRate,globalSelectedPackage.UTGSTAmount,globalSelectedPackage.UTGSTRate,aCarDetailInfo.TransmissionType,aCarDetailInfo.FuelType,aCarDetailInfo.LuggageCapacity,aCarDetailInfo.SeatingCapacity] forKeys:@[@"AgeMessage",@"AirportCharges",@"BasicAmt",@"CarCatID",@"CarCatName",@"CarImageHD",@"CarImageThumb",@"CityName",@"DepositeAmt",@"Detail",@"DetailID",@"ExtraKMRate",@"FreeDuration",@"FuelType",@"IndicatedPrice",@"IsAvailable",@"KMIncluded",@"LuggageCapacity",@"Model",@"ModelID",@"OriginalAmt",@"PkgId",@"PkgRate",@"PkgRateWeekEnd",@"PkgType",@"SeatingCapacity",@"SubLocationCost",@"TotalDuration",@"TransmissionType",@"VatAmt",@"VatRate",@"WeekDayDuration",@"WeekEndDuration",@"TotalInsuranceAmt",@"CoricId",@"InsuranceAmt",@"CGSTAmount",@"CGSTRate",@"IGSTAmount",@"IGSTRate",@"SGSTAmount",@"SGSTRate",@"UTGSTAmount",@"UTGSTRate",@"TransmissionType",@"FuelType",@"Luggage",@"SeatingCapacity"]];
                                                                           
                                                                           bookingDetail.speedGovernerIsActive = aCarDetailInfo.IsSpeedGovernor;
                                                                           bookingDetail.TotalDurationForDisplay = globalSelectedPackage.TotalDurationForDisplay;
                                                                           
                                                                           
                                                                           [bookingDetail setCarDetails:carDataDetail AndAddress:[NSString stringWithFormat:@"%@",[carData valueForKey:k_SubLocationAddress]] City:[carData valueForKey:k_cityIDCap]CarImg:[carData valueForKey:k_urlFullImage] pickupType: pickupType TripDuration:_tripDurationSelected packageType : packageTypeSelected ]  ;

                                                                           bookingDetail.selectedPackage = globalSelectedPackage;
                                                                   [self.navigationController pushViewController:bookingDetail animated:true];
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                       }
                                                                       
                                                                       else
                                                                           
                                                                       {
                                                                           
                                                                           [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                                           
                                                                       }
                                                                       
                                                                   });
                                                                   
                                                               }
                                                               
                                                               else
                                                                   
                                                               {
                                                                   
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       
                                                                       [CommonFunctions alertTitle:@"" withMessage:serviceTimeoutUnabletoreach];
                                                                       
                                                                   });
                                                                   
                                                               }
                                                               
                                                               
                                                           }
                                                           else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                               NSLog(@"AccessToken Invalid");
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [CommonFunctions logoutcommonFunction];
                                                                   AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                                                                   [appDelegate getAccessTokenWithCompletion:^{}];
                                                               });
                                                               
                                                           }
                                                           else
                                                           {
                                                           }
                                                           
                                                           
                                                           
                                                       }];
                
                [globalOperation addOperation:registrationOperation];
                
            }
            
            else
                
            {
                
                [CommonFunctions alertTitle:nil withMessage:KAInternet];
                
            }*/
        } else {
            BookingDetailsVC *bookingDetail = [self.storyboard instantiateViewControllerWithIdentifier:k_BookingDetailsVC];
//            NSLog(@"pickUpDateStr = %@",self.pickUpDateStr);
//            NSLog(@"pickUpTime = %@",self.pickUpTime);
//            NSLog(@"DropOffDateStr = %@",self.DropOffDateStr);
//            NSLog(@"dropOffTime = %@",self.dropOffTime);
//            
//            NSLog(@"searchCarModel.pickUpDateStr = %@",self.searchCarModel.pickUpTime);
//            NSLog(@"searchCarModel.pickUpTime = %@",self.searchCarModel.dropOffTime);
//            NSLog(@"searchCarModel.DropOffDateStr = %@",self.searchCarModel.toDate);
//            NSLog(@"searchCarModel.dropOffTime = %@",self.searchCarModel.fromDate);


            //NSDictionary *durationDict = [[NSDictionary alloc] initWithObjects:@[self.searchCarModel.fromDate,self.searchCarModel.pickUpTime,self.searchCarModel.toDate,self.searchCarModel.dropOffTime] forKeys:@[@"pDate",@"pTime",@"dDate",@"dTime"]];
            
            NSDictionary *durationDict = [[NSDictionary alloc] initWithObjects:@[aCarDetailInfo.PickDate,aCarDetailInfo.PickTime,aCarDetailInfo.DropDate,aCarDetailInfo.DropTime] forKeys:@[@"pDate",@"pTime",@"dDate",@"dTime"]];
            NSLog(@"durationDict =%@",durationDict);
            
            
            NSDictionary *carData = [[NSDictionary alloc]initWithObjectsAndKeys: CityID,k_cityIDCap,carmodelID,k_CarModelId, aCarDetailInfo.DetailID,k_SubLocationsID ,self.locationNameLabel.text,k_SubLocationAddress,globalSelectedPackage.PkgType,k_PkgType,aCarDetailInfo.CarImageHD,k_urlFullImage, nil];
            
            NSLog(@"carData =%@",carData);

            bookingDetail.locationDataObj = locationDataObj;
            //NSLog(@"carData =%@",carData);
            bookingDetail.doorStepDelieverySelected = TRUE;
            bookingDetail.bookingGSTPackageDetails = self.carGSTPackageDetails;
            bookingDetail.subLocationID = [NSString stringWithFormat:@"%@", [carData valueForKey:k_SubLocationsID]];
            
            bookingDetail.sublocationNameInDoorStep = aCarDetailInfo.Detail;
//            if ([[aCarDetailInfo.Detail componentsSeparatedByString:@":"] count] > 0) {
//                bookingDetail.sublocationNameInDoorStep = [aCarDetailInfo.Detail componentsSeparatedByString:@":"][0];
//            } else {
//                bookingDetail.sublocationNameInDoorStep = @"";
//            }
            
            
            bookingDetail.speedGovernerIsActive = aCarDetailInfo.IsSpeedGovernor;

            
            [bookingDetail setKBookingDurationMD:durationDict];
            bookingDetail.TotalDurationForDisplay = globalSelectedPackage.TotalDurationForDisplay;
            
            NSDictionary *carDataDetail = [[NSDictionary alloc] initWithObjects:@[aCarDetailInfo.AgeMessage,globalSelectedPackage.AirportCharges,globalSelectedPackage.BasicAmt,aCarDetailInfo.CarCatID,aCarDetailInfo.CarCatName,aCarDetailInfo.CarImageHD,aCarDetailInfo.CarImageThumb,aCarDetailInfo.CityName,globalSelectedPackage.DepositeAmt,aCarDetailInfo.Detail,aCarDetailInfo.DetailID,globalSelectedPackage.ExtraKMRate,globalSelectedPackage.FreeDuration,aCarDetailInfo.FuelType,globalSelectedPackage.IndicatedPrice,aCarDetailInfo.IsAvailable,globalSelectedPackage.KMIncluded,aCarDetailInfo.LuggageCapacity,aCarDetailInfo.Model,aCarDetailInfo.ModelID,globalSelectedPackage.OriginalAmt,globalSelectedPackage.PkgId,globalSelectedPackage.PkgRate,globalSelectedPackage.PkgRateWeekEnd,globalSelectedPackage.PkgType,aCarDetailInfo.SeatingCapacity,globalSelectedPackage.SubLocationCost,globalSelectedPackage.TotalDuration,aCarDetailInfo.TransmissionType,globalSelectedPackage.VatAmt,globalSelectedPackage.VatRate,globalSelectedPackage.WeekDayDuration,globalSelectedPackage.WeekEndDuration,globalSelectedPackage.TotalInsuranceAmt,aCarDetailInfo.CoricId,globalSelectedPackage.InsuranceAmt,globalSelectedPackage.CGSTAmount,globalSelectedPackage.CGSTRate,globalSelectedPackage.IGSTAmount,globalSelectedPackage.IGSTRate,globalSelectedPackage.SGSTAmount,globalSelectedPackage.SGSTRate,globalSelectedPackage.UTGSTAmount,globalSelectedPackage.UTGSTRate,aCarDetailInfo.TransmissionType,aCarDetailInfo.FuelType,aCarDetailInfo.LuggageCapacity,aCarDetailInfo.SeatingCapacity,aCarDetailInfo.InsuranceSecurityAmt, globalSelectedPackage.CessRate, globalSelectedPackage.CessAmount, globalSelectedPackage.UTGSTRate_Service, globalSelectedPackage.IGSTRate_Service, globalSelectedPackage.SGSTRate_Service, globalSelectedPackage.CGSTRate_Service,aCarDetailInfo.CarVariant, aCarDetailInfo.CarVariantID, aCarDetailInfo.PromotionCode, aCarDetailInfo.AfterDiscountAmount, aCarDetailInfo.DiscountAmt]  forKeys:@[@"AgeMessage",@"AirportCharges",@"BasicAmt",@"CarCatID",@"CarCatName",@"CarImageHD",@"CarImageThumb",@"CityName",@"DepositeAmt",@"Detail",@"DetailID",@"ExtraKMRate",@"FreeDuration",@"FuelType",@"IndicatedPrice",@"IsAvailable",@"KMIncluded",@"LuggageCapacity",@"Model",@"ModelID",@"OriginalAmt",@"PkgId",@"PkgRate",@"PkgRateWeekEnd",@"PkgType",@"SeatingCapacity",@"SubLocationCost",@"TotalDuration",@"TransmissionType",@"VatAmt",@"VatRate",@"WeekDayDuration",@"WeekEndDuration",@"TotalInsuranceAmt",@"CoricId",@"InsuranceAmt",@"CGSTAmount",@"CGSTRate",@"IGSTAmount",@"IGSTRate",@"SGSTAmount",@"SGSTRate",@"UTGSTAmount",@"UTGSTRate",@"TransmissionType",@"FuelType",@"Luggage",@"SeatingCapacity",@"InsuranceSecurityAmt",@"CessRate",@"CessAmount",@"UTGSTRate_Service",@"IGSTRate_Service",@"SGSTRate_Service",@"CGSTRate_Service",@"CarVariant", @"CarVariantID",@"PromotionCode", @"AfterDiscountAmount", @"DiscountAmt"]]; ////globalSelectedPackage.CarVariant, globalSelectedPackage.CarVariantID
            
            
                NSLog:(@"carDataDetail =%@",carDataDetail);

            
            bookingDetail.selectedPackage = globalSelectedPackage;
            [bookingDetail setCarDetails:carDataDetail AndAddress:[NSString stringWithFormat:@"%@",[carData valueForKey:k_SubLocationAddress]] City:[carData valueForKey:k_cityIDCap]CarImg:[carData valueForKey:k_urlFullImage] pickupType: pickupType TripDuration:_tripDurationSelected packageType : packageTypeSelected ]  ;

#ifdef DEBUG
            bookingDetail.previousDateTime = previousDateTime;
            bookingDetail.previousMilliSec = [CommonFunctions getMilliSecond];

#endif
            [self.navigationController pushViewController:bookingDetail animated:true];
        }
    }
    
}




- (void) movetoBookingScreenTwo : (NSIndexPath *)index
{
    CarInfoListTableViewCell *cell =(CarInfoListTableViewCell *) [self.tableView cellForRowAtIndexPath:index];
    NSString *carmodelID = cell.carModelID;
    NSString *CityID = [NSString stringWithFormat:@"%ld",(long)_selectedCityId];
    NSString *sublocationid = (cell.subLocationId == nil) ? @"0" : cell.subLocationId;
    NSDictionary *carData = [[NSDictionary alloc]initWithObjectsAndKeys: CityID,k_cityIDCap,carmodelID,k_CarModelId, sublocationid,k_SubLocationsID ,sublocationAddress,k_SubLocationAddress,selectedButton.titleLabel.text,k_PkgType,cell.carImgurl,k_urlFullImage, nil];
}



#pragma  mark - Filter protocol
-(void)ApplyFilter :(NSDictionary *)dict;
{
//    [self.tabBarController.tabBar setHidden:TRUE];

    if (dict.count > 0) {
        
        sublocationAddress = nil;
        _isSublocationSelected = false;
        sublocationID = nil;
        _SublocationTitleVal = nil;
        _sublocationIDVal = nil;
        dictSelectLoc = nil;
        
        
        dictFilterData == nil ? dictFilterData = [[NSMutableDictionary alloc]init] :[dictFilterData removeAllObjects];
        arrFilterData== nil ? arrFilterData = [[NSMutableArray alloc]init] : [arrFilterData removeAllObjects];
        nextArrFilterData == nil ? nextArrFilterData = [[NSMutableArray alloc] init] : [nextArrFilterData removeAllObjects];
        [dictFilterData addEntriesFromDictionary:dict];
        isFilter=true;
        
        NSMutableArray *localArray;
        NSMutableArray *nextLocalArray;
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            if ([self returnBothSection]) {
                localArray = self.carDetailInfoMA;
                nextLocalArray = self.nextCarDetailInfoMA;
            } else {
                localArray = self.carDetailInfoMA;
            }
        } else {
            //HEREALSOTODO
            if ([self returnBothSection]) {
                localArray = self.doorStepDetailInfoArray;
                nextLocalArray = self.nextDoorStepDetailInfoArray;
            } else {
                localArray = self.doorStepDetailInfoArray;
            }
            
        }
        
        if ([self returnBothSection]) {
            for (CarDetailInformation *aCarDetailInfo in localArray) {
                
                bool bSortValue = false;
                
                if (dict[@"lowprice"] != nil  && dict[@"lowprice"] != nil)
                {
                    if ([aCarDetailInfo.IndicatedPrice integerValue] >= [dict[@"lowprice"] integerValue] && [aCarDetailInfo.IndicatedPrice integerValue] <= [dict[@"highprice"] integerValue])
                    {
                        [arrFilterData addObject:aCarDetailInfo];
                    }
                    else
                    {
                        bSortValue = true;
                    }
                    
                }
                
                if (dict[@"Transmission"] != nil && !bSortValue)
                {
                    NSLog( @"aCarDetailInfo.TransmissionType %@",aCarDetailInfo.TransmissionType);
                    if ([dict[@"Transmission"] containsObject: [aCarDetailInfo.TransmissionType  isEqual: @"1"] ? @"Automatic":@"Manual"])
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo]) {
                            [arrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                    
                }
                if (dict[@"Seats"] != nil && !bSortValue )
                {
                    if ([dict[@"Seats"] containsObject: aCarDetailInfo.SeatingCapacity])
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo]) {
                            [arrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                    
                    
                }
                if (dict[@"Fuel"] != nil &&  !bSortValue)
                {
                    if ([dict[@"Fuel"] containsObject: aCarDetailInfo.FuelType])
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo]) {
                            [arrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                }
                if (dict[@"CarCategory"] != nil && !bSortValue)
                {
                    if ([dict[@"CarCategory"] containsObject: aCarDetailInfo.CarCatName] )
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo])
                            [arrFilterData addObject:aCarDetailInfo];
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                }
                if (dict[@"location"] != nil  && !bSortValue)
                {
                    NSArray *locNameArr =[aCarDetailInfo.Detail componentsSeparatedByString:@":"];
                    bool bSelectedloc = false;
                    for (NSString *locName in dict[@"location"])
                    {
                        if ([locNameArr containsObject:locName]) {
                            bSelectedloc = false;
                            break;
                        }
                        else
                            bSelectedloc = true;
                    }
                    if (bSelectedloc) {
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                    else
                        if (![arrFilterData containsObject:aCarDetailInfo])
                            [arrFilterData addObject:aCarDetailInfo];
                }
            }
            
            //for second array
            for (CarDetailInformation *aCarDetailInfo in nextLocalArray) {
                
                bool bSortValue = false;
                
                if (dict[@"lowprice"] != nil  && dict[@"lowprice"] != nil)
                {
                    if ([aCarDetailInfo.IndicatedPrice integerValue] >= [dict[@"lowprice"] integerValue] && [aCarDetailInfo.IndicatedPrice integerValue] <= [dict[@"highprice"] integerValue])
                    {
                        [nextArrFilterData addObject:aCarDetailInfo];
                    }
                    else
                    {
                        bSortValue = true;
                    }
                    
                }
                
                if (dict[@"Transmission"] != nil && !bSortValue)
                {
                    NSLog( @"aCarDetailInfo.TransmissionType %@",aCarDetailInfo.TransmissionType);
                    if ([dict[@"Transmission"] containsObject: [aCarDetailInfo.TransmissionType  isEqual: @"1"] ? @"Automatic":@"Manual"])
                    {
                        if (![nextArrFilterData containsObject:aCarDetailInfo]) {
                            [nextArrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [nextArrFilterData removeObject:aCarDetailInfo];
                    }
                    
                }
                if (dict[@"Seats"] != nil && !bSortValue )
                {
                    if ([dict[@"Seats"] containsObject: aCarDetailInfo.SeatingCapacity])
                    {
                        if (![nextArrFilterData containsObject:aCarDetailInfo]) {
                            [nextArrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [nextArrFilterData removeObject:aCarDetailInfo];
                    }
                    
                    
                }
                if (dict[@"Fuel"] != nil &&  !bSortValue)
                {
                    if ([dict[@"Fuel"] containsObject: aCarDetailInfo.FuelType])
                    {
                        if (![nextArrFilterData containsObject:aCarDetailInfo]) {
                            [nextArrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [nextArrFilterData removeObject:aCarDetailInfo];
                    }
                }
                if (dict[@"CarCategory"] != nil && !bSortValue)
                {
                    if ([dict[@"CarCategory"] containsObject: aCarDetailInfo.CarCatName] )
                    {
                        if (![nextArrFilterData containsObject:aCarDetailInfo])
                            [nextArrFilterData addObject:aCarDetailInfo];
                    }
                    else
                    {
                        bSortValue = true;
                        [nextArrFilterData removeObject:aCarDetailInfo];
                    }
                }
                if (dict[@"location"] != nil  && !bSortValue)
                {
                    NSArray *locNameArr =[aCarDetailInfo.Detail componentsSeparatedByString:@":"];
                    bool bSelectedloc = false;
                    for (NSString *locName in dict[@"location"])
                    {
                        if ([locNameArr containsObject:locName]) {
                            bSelectedloc = false;
                            break;
                        }
                        else
                            bSelectedloc = true;
                    }
                    if (bSelectedloc) {
                        [nextArrFilterData removeObject:aCarDetailInfo];
                    }
                    else
                        if (![nextArrFilterData containsObject:aCarDetailInfo])
                            [nextArrFilterData addObject:aCarDetailInfo];
                    
                    
                }
                
            }
            
        } else {
            for (CarDetailInformation *aCarDetailInfo in localArray) {
                
                bool bSortValue = false;
                
                if (dict[@"lowprice"] != nil  && dict[@"lowprice"] != nil)
                {
                    if ([aCarDetailInfo.IndicatedPrice integerValue] >= [dict[@"lowprice"] integerValue] && [aCarDetailInfo.IndicatedPrice integerValue] <= [dict[@"highprice"] integerValue])
                    {
                        [arrFilterData addObject:aCarDetailInfo];
                    }
                    else
                    {
                        bSortValue = true;
                    }
                    
                }
                
                if (dict[@"Transmission"] != nil && !bSortValue)
                {
                    NSLog( @"aCarDetailInfo.TransmissionType %@",aCarDetailInfo.TransmissionType);
                    if ([dict[@"Transmission"] containsObject: [aCarDetailInfo.TransmissionType  isEqual: @"1"] ? @"Automatic":@"Manual"])
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo]) {
                            [arrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                    
                }
                if (dict[@"Seats"] != nil && !bSortValue )
                {
                    if ([dict[@"Seats"] containsObject: aCarDetailInfo.SeatingCapacity])
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo]) {
                            [arrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                    
                    
                }
                if (dict[@"Fuel"] != nil &&  !bSortValue)
                {
                    if ([dict[@"Fuel"] containsObject: aCarDetailInfo.FuelType])
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo]) {
                            [arrFilterData addObject:aCarDetailInfo];
                        }
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                }
                if (dict[@"CarCategory"] != nil && !bSortValue)
                {
                    if ([dict[@"CarCategory"] containsObject: aCarDetailInfo.CarCatName] )
                    {
                        if (![arrFilterData containsObject:aCarDetailInfo])
                            [arrFilterData addObject:aCarDetailInfo];
                    }
                    else
                    {
                        bSortValue = true;
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                }
                if (dict[@"location"] != nil  && !bSortValue)
                {
                    NSArray *locNameArr =[aCarDetailInfo.Detail componentsSeparatedByString:@":"];
                    bool bSelectedloc = false;
                    for (NSString *locName in dict[@"location"])
                    {
                        if ([locNameArr containsObject:locName]) {
                            bSelectedloc = false;
                            break;
                        }
                        else
                            bSelectedloc = true;
                    }
                    if (bSelectedloc) {
                        [arrFilterData removeObject:aCarDetailInfo];
                    }
                    else
                        if (![arrFilterData containsObject:aCarDetailInfo])
                            [arrFilterData addObject:aCarDetailInfo];
                    
                    
                }
                
                
                
            }
        }
        
    }
    else
    {
        isFilter=false;
        [dictFilterData removeAllObjects];
        
    }
    
    
    
    if (dictkey.count>0 ) {
        [self sortData:[dictkey[@"ascending"] boolValue] :dictkey[@"sort"]];
        
    }
    else
    {
        
        
        [_tableView reloadData];
        [_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        
    }
    _lblSort.enabled = true;
    if (arrFilterData.count == 0 && isFilter && nextArrFilterData == 0){
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alertController = [UIAlertController
                                                  
                                                  alertControllerWithTitle:KSorry
                                                  
                                                  message:@"No Car available for selected filters"
                                                  
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction *okAction = [UIAlertAction
                                       
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       
                                       style:UIAlertActionStyleDefault
                                       
                                       handler:^(UIAlertAction *action)
                                       
                                       {
                                           isFilter = FALSE;
                                           [self.tableView reloadData];
                                           
                                           
                                       }];
            
            
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
//            UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:KSorry
//                                                             message:@"No Car available for selected filters"
//                                                            delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//            [alert show];
            
        });
        _lblSort.enabled = false;
    }
}

-(void)sortData :(BOOL)bAscending :(NSString *)sortedkey
{
    
    NSArray *sortedArray;
    NSArray *nextSortedArray;
    
    
    if ([sortedkey isEqualToString:@"Model"])
    {
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortedkey
                                                     ascending:bAscending];
        
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            if ([self returnBothSection]) {
                sortedArray = [isFilter ? arrFilterData:requiredArray sortedArrayUsingDescriptors:@[sortDescriptor]];
                nextSortedArray = [isFilter ? arrFilterData:nextRequiredArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            } else {
                sortedArray = [isFilter ? arrFilterData:requiredArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            }
        } else {
            if ([self returnBothSection]) {
                sortedArray = [isFilter ? arrFilterData:requiredDoorStepArray sortedArrayUsingDescriptors:@[sortDescriptor]];
                nextSortedArray = [isFilter ? arrFilterData:nextRequiredDoorstepArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            } else {
                sortedArray = [isFilter ? arrFilterData:requiredDoorStepArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            }
        }
        
    }
    else
    {
        if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
            if ([self returnBothSection]) {
                sortedArray = [isFilter ? arrFilterData:requiredArray sortedArrayUsingComparator:^NSComparisonResult(CarDetailInformation *obj1, CarDetailInformation * obj2)
                               {
                                   CarPackageDetails *package1 = obj1.AllPackages[0];
                                   CarPackageDetails *package2 = obj2.AllPackages[0];

                                   return [sortedkey isEqualToString:@"SeatingCapacity"]?  [!bAscending ?obj2.SeatingCapacity : obj1.SeatingCapacity compare:!bAscending ?obj1.SeatingCapacity : obj2.SeatingCapacity options:NSNumericSearch]:[!bAscending ?package2.IndicatedPrice : package1.IndicatedPrice compare:!bAscending ?package1.IndicatedPrice : package2.IndicatedPrice options:NSNumericSearch];
                               }];
                
                nextSortedArray = [isFilter ? arrFilterData:nextRequiredArray sortedArrayUsingComparator:^NSComparisonResult(CarDetailInformation *obj1, CarDetailInformation * obj2)
                               {
                                   CarPackageDetails *package1 = obj1.AllPackages[0];
                                   CarPackageDetails *package2 = obj2.AllPackages[0];
                                   
                                   return [sortedkey isEqualToString:@"SeatingCapacity"]?  [!bAscending ?obj2.SeatingCapacity : obj1.SeatingCapacity compare:!bAscending ?obj1.SeatingCapacity : obj2.SeatingCapacity options:NSNumericSearch]:[!bAscending ?package2.IndicatedPrice : package1.IndicatedPrice compare:!bAscending ?package1.IndicatedPrice : package2.IndicatedPrice options:NSNumericSearch];
                               }];
            } else {
                sortedArray = [isFilter ? arrFilterData:requiredArray sortedArrayUsingComparator:^NSComparisonResult(CarDetailInformation *obj1, CarDetailInformation * obj2)
                               {
                                   
                                   CarPackageDetails *package1 = obj1.AllPackages[0];
                                   CarPackageDetails *package2 = obj2.AllPackages[0];
                                   return [sortedkey isEqualToString:@"SeatingCapacity"]?  [!bAscending ?obj2.SeatingCapacity : obj1.SeatingCapacity compare:!bAscending ?obj1.SeatingCapacity : obj2.SeatingCapacity options:NSNumericSearch]:[!bAscending ?package2.IndicatedPrice : package1.IndicatedPrice compare:!bAscending ?package1.IndicatedPrice : package2.IndicatedPrice options:NSNumericSearch];
                               }];
            }
            
        } else {
            if ([self returnBothSection]) {
                sortedArray = [isFilter ? arrFilterData:requiredDoorStepArray sortedArrayUsingComparator:^NSComparisonResult(CarDetailInformation *obj1, CarDetailInformation * obj2)
                               {
                                   CarPackageDetails *package1 = obj1.AllPackages[0];
                                   CarPackageDetails *package2 = obj2.AllPackages[0];
                                   return [sortedkey isEqualToString:@"SeatingCapacity"]?  [!bAscending ?obj2.SeatingCapacity : obj1.SeatingCapacity compare:!bAscending ?obj1.SeatingCapacity : obj2.SeatingCapacity options:NSNumericSearch]:[!bAscending ?package2.IndicatedPrice : package1.IndicatedPrice compare:!bAscending ?package1.IndicatedPrice : package2.IndicatedPrice options:NSNumericSearch];
                               }];
                
                nextSortedArray = [isFilter ? arrFilterData:nextRequiredDoorstepArray sortedArrayUsingComparator:^NSComparisonResult(CarDetailInformation *obj1, CarDetailInformation * obj2)
                                   {
                                       CarPackageDetails *package1 = obj1.AllPackages[0];
                                       CarPackageDetails *package2 = obj2.AllPackages[0];
                                       return [sortedkey isEqualToString:@"SeatingCapacity"]?  [!bAscending ?obj2.SeatingCapacity : obj1.SeatingCapacity compare:!bAscending ?obj1.SeatingCapacity : obj2.SeatingCapacity options:NSNumericSearch]:[!bAscending ?package2.IndicatedPrice : package1.IndicatedPrice compare:!bAscending ?package1.IndicatedPrice : package2.IndicatedPrice options:NSNumericSearch];
                                   }];
            } else {
                sortedArray = [isFilter ? arrFilterData:requiredDoorStepArray sortedArrayUsingComparator:^NSComparisonResult(CarDetailInformation *obj1, CarDetailInformation * obj2)
                               {
                                   CarPackageDetails *package1 = obj1.AllPackages[0];
                                   CarPackageDetails *package2 = obj2.AllPackages[0];
                                   return [sortedkey isEqualToString:@"SeatingCapacity"]?  [!bAscending ?obj2.SeatingCapacity : obj1.SeatingCapacity compare:!bAscending ?obj1.SeatingCapacity : obj2.SeatingCapacity options:NSNumericSearch]:[!bAscending ?package2.IndicatedPrice : package1.IndicatedPrice compare:!bAscending ?package1.IndicatedPrice : package2.IndicatedPrice options:NSNumericSearch];
                               }];
            }
           
        }
        
    }
    
    
    if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        if ([self returnBothSection]) {
            [isFilter ? arrFilterData:requiredArray removeAllObjects];
            [isFilter ? arrFilterData:requiredArray addObjectsFromArray:sortedArray];
            
            [isFilter ? arrFilterData:nextRequiredArray removeAllObjects];
            [isFilter ? arrFilterData:nextRequiredArray addObjectsFromArray:nextSortedArray];
        } else {
            [isFilter ? arrFilterData:requiredArray removeAllObjects];
            [isFilter ? arrFilterData:requiredArray addObjectsFromArray:sortedArray];
        }
    } else {
        [isFilter ? arrFilterData:requiredDoorStepArray removeAllObjects];
        [isFilter ? arrFilterData:requiredDoorStepArray addObjectsFromArray:sortedArray];
    }
   
    
    [_tableView reloadData];
    [_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
}
-(IBAction)btnFilterBy:(id)sender
{
    FilterViewController *FilterVc = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    if (self.pickupSegementedControl.selectedSegmentIndex == 0) {
        FilterVc.isDoorStepDelievery = FALSE;
    } else {
        FilterVc.isDoorStepDelievery = TRUE;
    }
    FilterVc.isCityChangedApplied = cityIsChanged;
    FilterVc.delegate = self;
    if (isFilter) {
        FilterVc.selectedfilterDict = dictFilterData;
    }
    FilterVc.strCityId = [NSString stringWithFormat:@"%ld",(long)_selectedCityId];
    FilterVc.filterDict = dictFilter;
    //FilterVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    //self.tabBarController.tabBar.hidden = TRUE;

    //    [self.navigationController pushViewController:FilterVc animated:YES];
    [self presentViewController:FilterVc animated:YES completion:nil];
}

-(IBAction)btnSortBy:(id)sender
{
    SortViewController *FilterVc = [self.storyboard instantiateViewControllerWithIdentifier:@"SortViewController"];
    
    
    if (indexpath != nil ) {
        FilterVc.index = indexpath;
    }
    FilterVc.delegate = self;
    FilterVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:FilterVc animated:YES completion:nil];
}

#pragma mark - sort protocol

NSMutableDictionary  *dictkey;
-(void)Sortdata :(NSString *)strSortTitle :(BOOL)bLowHigh :(NSString *)index
{
    sublocationAddress = nil;
    _isSublocationSelected = false;
    sublocationID = nil;
    _SublocationTitleVal = nil;
    _sublocationIDVal = nil;
    dictSelectLoc = nil;
    [self.tabBarController.tabBar setHidden:NO];
    indexpath = index;
    dictkey = nil;
    if (dictkey==nil)
        dictkey = [[NSMutableDictionary alloc]init];
    else
        [dictkey removeAllObjects];
    
    [dictkey setValue:strSortTitle forKey:@"sort"];
    [dictkey setObject:[NSString stringWithFormat:@"%hhd",bLowHigh] forKey:@"ascending"];
    [self sortData:bLowHigh :strSortTitle];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        [[SubLocationData sharedInstance] deletSublocation];

    }
    

//    if (![self tabBarIsVisible]){
//        [self.tabBarController.tabBar setHidden:FALSE];
//    }
    //showPopUp = FALSE;

    //    dictkey = nil;
    //    indexpath= nil;
}

#pragma mark - Protocol for login response
- (void)afterLoginChangeDestinationWithStrDestination:(NSString * _Nonnull)strDestination
{
//    if ([CommonFunctions reachabiltyCheck]) {
//        [self movetoBookingScreen:index];
//    }
//    else {
//        [self callForBooking];
//    }
}

- (void)dealloc {
    _tableView.dataSource = nil;
    _tableView.delegate = nil;
}

#pragma mark - New Changes
- (IBAction)pickUpSegmentTapped:(id)sender {
    
    [_tableView setContentOffset:_tableView.contentOffset animated:NO];
    if (_pickupSegementedControl.selectedSegmentIndex == 0) {
        NSLog(@"requiredArray count =%ld",(unsigned long)requiredArray.count);
        if (cityIsChanged) {
            dictFilter = nil;
            dictFilterData = nil;
            self.pickUpDropOffViewheightConstraint.constant -= 35;
            self.locationImageview.hidden = TRUE;

            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                                options:UIViewAnimationCurveEaseIn
                             animations:^{
                                 [self.view layoutIfNeeded];
                             }
                             completion:^(BOOL finished) {
                                 [self callSitePickApiWithNewTimingsAndNewCity];
                             }];
            
//            [UIView animateWithDuration:0.5 animations:^{
//            }];

            //Refresh value
        } else {
            self.pickUpDropOffViewheightConstraint.constant -= 35;
            self.locationImageview.hidden = TRUE;
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
                [self.tableView reloadData];
            }];

        }
    } else {
        //Load mapViewController
        // check if current time is different
        NSLog(@"requiredDoorStepArray count =%ld",(unsigned long)requiredDoorStepArray.count);

        if (locationOfDoorStepDeleivery != nil) {
            self.pickUpDropOffViewheightConstraint.constant += 35;
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
                [self.tableView reloadData];
                self.locationImageview.hidden = FALSE;
                if (locationOfDoorStepDeleivery != nil) {
                    if([requiredDoorStepArray count] == 0) {
                        //show a message no car is available for this time period
                        UIAlertController *alertController = [UIAlertController
                                                              alertControllerWithTitle:@"Message"
                                                              message:[NSString stringWithFormat:@"Apologies for not being able to serve your location! You may pick your car from one of our site locations. For Assistance please call our Customer Care"]
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   
                                                   actionWithTitle:@"OK"
                                                   
                                                   style:UIAlertActionStyleDefault
                                                   
                                                   handler:^(UIAlertAction *action)
                                                   {
                                                       self.pickupSegementedControl.selectedSegmentIndex = 0;
                                                       self.pickUpDropOffViewheightConstraint.constant -= 35;
                                                       self.locationImageview.hidden = TRUE;
                                                       [UIView animateWithDuration:0.5 animations:^{
                                                           [self.view layoutIfNeeded];
                                                           [self.tableView reloadData];
                                                       }];
                                                }];


                        [alertController addAction:okAction];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }else {
                    [self performSegueWithIdentifier:@"showMap" sender:sender];
                }
//                } else {
//                    self.pickupSegementedControl.selectedSegmentIndex = 0;
//                    [self pickUpSegmentTapped:self.pickupSegementedControl];
//                }
            }];
        } else {
           // if (!showPopUp){
             //   showPopUp = TRUE;
                NSDate *date = [NSDate date];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
                NSInteger hour = [components hour];
                NSInteger minute = [components minute];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date.
                [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
                NSString *convertedDateString = [dateFormatter stringFromDate:date];// Here convert date in NSString
                BOOL toCheck = FALSE;
                NSString *amPm;
            
            if (hour >= 22) {
                date = [calendar dateByAddingUnit:NSCalendarUnitDay
                                            value:1
                                           toDate:[NSDate date]
                                          options:0];
                NSLog(@"date = %@",date);
//              NSDatecomponents = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
                hour = [components hour];
                minute = [components minute];
                convertedDateString = [dateFormatter stringFromDate:date];
            }
            
            NSLog(@"convertedString = %@",convertedDateString);
            NSLog(@"hour = %ld",(long)hour);
            NSLog(@"minute = %ld",(long)minute);

                if ([convertedDateString isEqualToString:_pickUpDateStr]) {
                    //need to check otherwise no need
                    if (minute >= 30) {
                        hour += 3;
                        minute = 0;
                    } else {
                        hour += 2;
                        minute = 30;
                    }
                    if (hour >= 24) {
                        hour = hour - 24;
                    }
                    amPm = [NSString stringWithFormat:@"%.2ld%.2ld",(long)hour,(long)minute];
                    if ([amPm isEqualToString:_pickUpTime]) {
                        toCheck = TRUE;
                    } else {
                        toCheck = FALSE;
                    }
                } else {
                    
                    toCheck = FALSE;
                }
                
                if (toCheck) {
                        NSInteger hour = [components hour];
                        NSInteger minute = [components minute];
                        
                        if (minute >= 30) {
                            hour += 4;
                            minute = 0;
                        } else {
                            hour += 3;
                            minute = 30;
                        }
                    
                    if (hour >= 24) {
                        hour = hour - 24;
                    }
                    
                        NSInteger localHour = hour;
                        NSInteger localMinute = minute;
                        
                        
                        if (hour > 12) {
                            hour -= 12;
                            amPm = @"PM";
                        } else {
                            amPm = @"AM";
                        }

                    
                    
                    NSLog(@"dropofftime = %@",self.searchCarModel.dropOffTime);
                    NSLog(@"duration = %@",self.searchCarModel.duration);
                    
                    NSInteger hourMinute = [self.searchCarModel.dropOffTime integerValue] + 100;
                    
                    NSDate *tomorrow;
                    if (hourMinute >= 2400) {
                        hourMinute = hourMinute - 2400;
                        
                        //change dropOffDate also
                        tomorrow = [calendar dateByAddingUnit:NSCalendarUnitDay
                                                                value:1
                                                               toDate:[NSDate date]
                                                              options:0];
                        
                    }
//                    else if (hourMinute == 2400) {
//                        hourMinute = 0;
//                        //change dropOffDate also
//                        tomorrow = [calendar dateByAddingUnit:NSCalendarUnitDay
//                                                        value:1
//                                                       toDate:[NSDate date]
//                                                      options:0];
//                    }
                    
                    
                    
                    // First update the model with new values of extended 3 hours gap.
                    
                    finalTimeStr = [NSString stringWithFormat:@"%.2ld:%.2ld %@",(long)hour,(long)minute,amPm];
                    
                    
                    
                    
                    UIAlertController *alertController = [UIAlertController
                                                          alertControllerWithTitle:@"Message"
                                                          message:[NSString stringWithFormat:@"Door step delivery is available only after %@",finalTimeStr]
                                                          preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction
                                               
                                               actionWithTitle:@"Update Booking Time"
                                               
                                               style:UIAlertActionStyleDefault
                                               
                                               handler:^(UIAlertAction *action)
                                               
                                               {
                                                   
                                                   self.searchCarModel.pickUpTime = [NSString stringWithFormat:@"%.2ld%.2ld",localHour,localMinute];
                                                   self.searchCarModel.dropOffTime = [NSString stringWithFormat:@"%.4ld",hourMinute];
                                                   if(tomorrow != nil) {
                                                       self.searchCarModel.toDate = [dateFormatter stringFromDate:tomorrow];
                                                   }
                                                   
                                                   self.pickUpDropOffViewheightConstraint.constant += 35;
                                                   [UIView animateWithDuration:0.5 animations:^{
                                                       [self.view layoutIfNeeded];
                                                       self.locationImageview.hidden = FALSE;
                                                   }];
                                                   if (locationOfDoorStepDeleivery != nil) {
                                                       
                                                   }else {
                                                       [self performSegueWithIdentifier:@"showMap" sender:sender];
                                                   }
                                               }];
                    
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel, Pick up from Site" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        self.pickupSegementedControl.selectedSegmentIndex = 0;
                        //self.pickUpDropOffViewheightConstraint.constant -= 35;
//                        self.locationImageview.hidden = TRUE;
//                        [UIView animateWithDuration:0.5 animations:^{
//                            [self.view layoutIfNeeded];
//                            [self.tableView reloadData];
//                        }];

                    }];
                    
                    
                    
                    [alertController addAction:okAction];
                    [alertController addAction:cancelAction];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                    
                } else {
                
                    finalTimeStr = @"";
                    //finalTimeStr = [NSString stringWithFormat:@"%.2ld:%.2ld",(long)hour,(long)minute];
                    self.pickUpDropOffViewheightConstraint.constant += 35;
                    [UIView animateWithDuration:0.5 animations:^{
                        [self.view layoutIfNeeded];
                        
                        [self.tableView reloadData];
                    }];
                    self.locationImageview.hidden = FALSE;
                    if (locationOfDoorStepDeleivery != nil) {
                        
                    }else {
                        [self performSegueWithIdentifier:@"showMap" sender:sender];
                    }
                    
                }
//            } else {
//                self.pickUpDropOffViewheightConstraint.constant += 35;
//                [UIView animateWithDuration:0.5 animations:^{
//                    [self.view layoutIfNeeded];
//                    if([requiredDoorStepArray count] > 0) {
//                        [self.tableView reloadData];
//                        self.locationImageview.hidden = FALSE;
//                        if (locationOfDoorStepDeleivery != nil) {
//                            
//                        }else {
//                            [self performSegueWithIdentifier:@"showMap" sender:sender];
//                        }
//                    } else {
//                        self.pickupSegementedControl.selectedSegmentIndex = 0;
//                        [self pickUpSegmentTapped:self.pickupSegementedControl];
//                    }
//                }];
//                
//            }
        }
        
    }
}

//Navigation method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showMap"]) {
        MapViewController *mapViewController = [segue destinationViewController];
        mapViewController.locationDataDelegate = self;
        mapViewController.cityID = [NSString stringWithFormat:@"%ld",(long)_selectedCityId];
        mapViewController.finalTimePassed = finalTimeStr;
        mapViewController.cityModelArray = _cityModelArray.copy;
        if (locationOfDoorStepDeleivery != nil) {
            mapViewController.selectedLocation = locationOfDoorStepDeleivery;
        }
    }
}

-(IBAction)unwindForSegue:(UIStoryboardSegue *)unwindSegue {
    if ([unwindSegue.sourceViewController isKindOfClass:[MapViewController class]]) {
        MapViewController *mapController = unwindSegue.sourceViewController;
    }
}

-(NSDictionary *)postDataOnApiWith:(NSString *)selectedCityID
{
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    [body setObject:selectedCityID forKey:@"CityID"];
    [body setObject:self.searchCarModel.fromDate forKey:@"FromDate"];
    [body setObject:self.searchCarModel.toDate forKey:@"ToDate"];
    [body setObject:self.searchCarModel.pickUpTime forKey:@"PickUpTime"];
    [body setObject:self.searchCarModel.dropOffTime  forKey:@"DropOffTime"];
    [body setObject:@"0" forKey:@"SubLocations"];
    [body setObject:self.searchCarModel.duration forKey:@"Duration"];
    [body setObject:self.searchCarModel.customPkgYN forKey:@"CustomPkgYN"];
    [body setObject:[NSString stringWithFormat:@"%f",locationOfDoorStepDeleivery.coordinate.longitude] forKey:@"Lon"];
    [body setObject:[NSString stringWithFormat:@"%f",locationOfDoorStepDeleivery.coordinate.latitude] forKey:@"Lat"];
    //    (string CityID, string FromDate, string ToDate, string PickUpTime, string DropOffTime, string SubLocations, string Duration, string CustomPkgYN, string Lon, string Lng)
    return body;
}

-(void)updateData {
    
    BOOL isAirport = false;
    NSString *airportCharges;
    if ([requiredDoorStepArray count] > 0) {
        [requiredDoorStepArray removeAllObjects];
    }
        for (int i = 0 ; i < self.doorStepDetailInfoArray.count; i++)
        {
            @autoreleasepool {
                
                
                CarDetailInformation *aCarDetailInfoTest = self.doorStepDetailInfoArray[i];
                
                if ([aCarDetailInfoTest.IsAvailable boolValue]) {
                    
                    if ([aCarDetailInfoTest.Sublocation count] > 0) {
                        
                        if ([aCarDetailInfoTest.AirportYN isEqualToString:@"Y"]) {
                            airportCharges = aCarDetailInfoTest.AirportCharges;
                            isAirport = TRUE;
                        } else {
                            airportCharges = aCarDetailInfoTest.AirportCharges;
                            isAirport = FALSE;
                        }
                        NSString *setingCApacity = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.SeatingCapacity];
                        NSString *displayRate = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.IndicatedPrice];
                        if (dictFilter == nil)
                        {
                            dictFilter = [[NSMutableDictionary alloc]init];
                            [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                            [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                            [dictFilter setValue:@[aCarDetailInfoTest.CarCatName] forKey:@"catCategory"];
                            [dictFilter setValue:@[aCarDetailInfoTest.FuelType] forKey:@"fuel"];
                            [dictFilter setValue:@[aCarDetailInfoTest.SeatingCapacity] forKey:@"seats"];
                            [dictFilter setValue:@[[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"] forKey:@"transmission"];
                            
                            
                        }
                        else
                        {
                            
                            if (dictFilter[@"highprice"] != nil && [displayRate floatValue] > [dictFilter[@"highprice"] integerValue])
                            {
                                [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                            }
                            else if (dictFilter[@"lowprice"] != nil && [displayRate floatValue] < [dictFilter[@"lowprice"] integerValue])
                            {
                                [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                            }
                            
                            if ( dictFilter[@"fuel"] != nil && ![dictFilter[@"fuel"] containsObject:aCarDetailInfoTest.FuelType])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"fuel"] mutableCopy];
                                [arrtemp addObject:aCarDetailInfoTest.FuelType];
                                [dictFilter setValue:arrtemp forKey:@"fuel"];
                                
                            }
                            
                            if (dictFilter[@"catCategory"] != nil && ![dictFilter[@"catCategory"] containsObject:aCarDetailInfoTest.CarCatName])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"catCategory"] mutableCopy];
                                [arrtemp addObject:aCarDetailInfoTest.CarCatName];
                                [dictFilter setValue:arrtemp forKey:@"catCategory"];
                                
                            }
                            
                            if (dictFilter[@"seats"] != nil && ![dictFilter[@"seats"] containsObject:aCarDetailInfoTest.SeatingCapacity])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"seats"] mutableCopy];
                                [arrtemp addObject:aCarDetailInfoTest.SeatingCapacity];
                                [dictFilter setValue:arrtemp forKey:@"seats"];
                                
                            }
                            
                            if (dictFilter[@"transmission"] != nil &&  ![dictFilter[@"transmission"] containsObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"])
                            {
                                NSMutableArray *arrtemp =[dictFilter[@"transmission"] mutableCopy];
                                [arrtemp addObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"];
                                [dictFilter setValue:arrtemp forKey:@"transmission"];
                                
                            }
                        }
                        
                        //PATCH
                        aCarDetailInfoTest.Detail= @"";
                        aCarDetailInfoTest.DetailID = @"";
                        Sublocation *sublocationDetail = (Sublocation *)aCarDetailInfoTest.Sublocation[0];
                        aCarDetailInfoTest.Detail = sublocationDetail.Detail;
                        aCarDetailInfoTest.DetailID = sublocationDetail.DetailID;
                        
                        
                        
                        [requiredDoorStepArray addObject:aCarDetailInfoTest];
                    }
                }
            }
        }
    
    if (nextRequiredDoorstepArray.count > 0) {
        [nextRequiredDoorstepArray removeAllObjects];
        //Changing
        
    }
    for (int i = 0 ; i < self.nextDoorStepDetailInfoArray.count; i++)
    {
        @autoreleasepool {
            
            CarDetailInformation *aCarDetailInfoTest = self.nextDoorStepDetailInfoArray[i];
            
            if ([aCarDetailInfoTest.IsAvailable boolValue]) {
                
                //if ([aCarDetailInfoTest.DetailID length] > 0) {
                    
                    if ([aCarDetailInfoTest.AirportYN isEqualToString:@"Y"]) {
                        airportCharges = aCarDetailInfoTest.AirportCharges;
                        isAirport = TRUE;
                    } else {
                        airportCharges = aCarDetailInfoTest.AirportCharges;
                        isAirport = FALSE;
                    }
                    NSString *setingCApacity = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.SeatingCapacity];
                    NSString *displayRate = [NSString stringWithFormat:@"%@",aCarDetailInfoTest.IndicatedPrice];
                    if (dictFilter == nil)
                    {
                        dictFilter = [[NSMutableDictionary alloc]init];
                        [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                        [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                        [dictFilter setValue:@[aCarDetailInfoTest.CarCatName] forKey:@"catCategory"];
                        [dictFilter setValue:@[aCarDetailInfoTest.FuelType] forKey:@"fuel"];
                        [dictFilter setValue:@[aCarDetailInfoTest.SeatingCapacity] forKey:@"seats"];
                        [dictFilter setValue:@[[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"] forKey:@"transmission"];
                        
                        
                    }
                    else
                    {
                        
                        if (dictFilter[@"highprice"] != nil && [displayRate floatValue] > [dictFilter[@"highprice"] integerValue])
                        {
                            [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"highprice"];
                        }
                        else if (dictFilter[@"lowprice"] != nil && [displayRate floatValue] < [dictFilter[@"lowprice"] integerValue])
                        {
                            [dictFilter setValue:[NSString stringWithFormat:@"%ld",(long)[displayRate integerValue]] forKey:@"lowprice"];
                        }
                        
                        if ( dictFilter[@"fuel"] != nil && ![dictFilter[@"fuel"] containsObject:aCarDetailInfoTest.FuelType])
                        {
                            NSMutableArray *arrtemp =[dictFilter[@"fuel"] mutableCopy];
                            [arrtemp addObject:aCarDetailInfoTest.FuelType];
                            [dictFilter setValue:arrtemp forKey:@"fuel"];
                            
                        }
                        
                        if (dictFilter[@"catCategory"] != nil && ![dictFilter[@"catCategory"] containsObject:aCarDetailInfoTest.CarCatName])
                        {
                            NSMutableArray *arrtemp =[dictFilter[@"catCategory"] mutableCopy];
                            [arrtemp addObject:aCarDetailInfoTest.CarCatName];
                            [dictFilter setValue:arrtemp forKey:@"catCategory"];
                            
                        }
                        
                        if (dictFilter[@"seats"] != nil && ![dictFilter[@"seats"] containsObject:aCarDetailInfoTest.SeatingCapacity])
                        {
                            NSMutableArray *arrtemp =[dictFilter[@"seats"] mutableCopy];
                            [arrtemp addObject:aCarDetailInfoTest.SeatingCapacity];
                            [dictFilter setValue:arrtemp forKey:@"seats"];
                            
                        }
                        
                        if (dictFilter[@"transmission"] != nil &&  ![dictFilter[@"transmission"] containsObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"])
                        {
                            NSMutableArray *arrtemp =[dictFilter[@"transmission"] mutableCopy];
                            [arrtemp addObject:[aCarDetailInfoTest.TransmissionType  isEqual: @"1"] ? @"automatic":@"manual"];
                            [dictFilter setValue:arrtemp forKey:@"transmission"];
                            
                        }
                    }
                    //PATCH
                    aCarDetailInfoTest.Detail= @"";
                    aCarDetailInfoTest.DetailID = @"";
                    Sublocation *sublocationDetail = (Sublocation *)aCarDetailInfoTest.Sublocation[0];
                    aCarDetailInfoTest.Detail = sublocationDetail.Detail;
                    aCarDetailInfoTest.DetailID = sublocationDetail.DetailID;
                
                
                    [nextRequiredDoorstepArray addObject:aCarDetailInfoTest];
                }
            //}
        }
    }
    
    
        NSLog(@"requiredDoorStepArray = %lu",(unsigned long)[nextRequiredDoorstepArray count]);
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (isAirport) {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Message"
                                                  message:[NSString stringWithFormat:@"Airport Delivery Charges of Rs %ld are applicable at this location",(long)[airportCharges integerValue]]
                                                  
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       
                                       style:UIAlertActionStyleDefault
                                       
                                       handler:^(UIAlertAction *action)
                                       
                                       {
                                           NSString *priceOfHomeickup = [NSString stringWithFormat:@"Airport Delivery (₹ %ld)",(long)[airportCharges integerValue]];
                                           
                                           [self.pickupSegementedControl setTitle:priceOfHomeickup forSegmentAtIndex:1];
                                           [self logEvent:@"Dorstep delivery"];
                                           [self.tableView reloadData];
                                           
                                       }];
            
            
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self logEvent:@"Dorstep delivery"];
                 [self.tableView reloadData];
             });
        }
        
    });
    

}


-(void)searchDoorStepCarsWithSelectedCityID:(NSString *)cityID {
   
    //Remove if any filter is allready applied
    //isFilter ? arrFilterData
    isFilter = FALSE;
    if (arrFilterData.count > 0 ){
        [arrFilterData removeAllObjects];
    }
    if ([CommonFunctions reachabiltyCheck]) {
        [self showLoader];
        __block CarListViewController *carListControllerSelf = self;
        NSDictionary *localDict = [self postDataOnApiWith:cityID];
        SearchCarModel *searchMobel = [[SearchCarModel alloc]initWithDict:localDict];

        ApiClass *callApi = [[ApiClass alloc] initWithDict:localDict];
        [callApi doorStepDeleieveryWithCompletionHandler:^(id responseObj, BOOL isValidToken,id sitePickupData) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            
            if (isValidToken) {
                if (responseObj!=nil) {
                   

                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (![DEFAULTS integerForKey:k_HomePickupAvailable]) {
                            NSString *priceOfHomeickup = [NSString stringWithFormat:@"Doorstep Delivery"];
                            [self.pickupSegementedControl setTitle:priceOfHomeickup forSegmentAtIndex:1];

                        }
                        else{
                            NSString *priceOfHomeickup = [NSString stringWithFormat:@"Doorstep Delivery (₹%ld)",[[DEFAULTS valueForKey:k_HomePickupCharge] integerValue]];
                            [self.pickupSegementedControl setTitle:priceOfHomeickup forSegmentAtIndex:1];

                        }

              //          NSLog(@"obj = %@",responseObj);
                        if ([responseObj isKindOfClass:[MylesCarDetailsResponse class]]) {
                            MylesCarDetailsResponse *obj = (MylesCarDetailsResponse *)responseObj;
                            if (carListControllerSelf.doorStepDetailInfoArray != nil) {
                                if (carListControllerSelf.doorStepDetailInfoArray.count > 0) {
                                    [carListControllerSelf.doorStepDetailInfoArray removeAllObjects];
                                    SearchCarDetails *searchCarDetailsObj = (SearchCarDetails *)obj.response;
                                    carListControllerSelf.doorStepDetailInfoArray = (NSMutableArray *)searchCarDetailsObj.CarSearchDetailNormal;
                                    if (searchCarDetailsObj.CarSearchDetailNextAvailable.count > 0) {
                                        carListControllerSelf.nextDoorStepDetailInfoArray = (NSMutableArray *)searchCarDetailsObj.CarSearchDetailNextAvailable;
                                    }
                                    if (searchCarDetailsObj.GSTDetails.count > 0) {
                                        carListControllerSelf.carGSTPackageDetails = (NSMutableArray *)searchCarDetailsObj.GSTDetails;
                                        
                                       // [DEFAULTS setObject:carListControllerSelf.carGSTPackageDetails forKey:k_gstDetails];
                                    }

                                    //carListControllerSelf.doorStepDetailInfoArray = obj.response;
                                    [carListControllerSelf updateData];
                                    //We have to update values for DOORSTEP tab
                                    //make a different array for doorstep filter and then asign here as it is done for SITEPICKUP tab.
                                    
                                    //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    // [self.tableView reloadData];
                                    //});
                                }
                            } else {
                                self.doorStepDetailInfoArray = [NSMutableArray array];
                                [carListControllerSelf.doorStepDetailInfoArray removeAllObjects];
                                SearchCarDetails *searchCarDetailsObj = (SearchCarDetails *)obj.response;
                                carListControllerSelf.doorStepDetailInfoArray = (NSMutableArray *)searchCarDetailsObj.CarSearchDetailNormal;
                                if (searchCarDetailsObj.CarSearchDetailNextAvailable.count > 0) {
                                    carListControllerSelf.nextDoorStepDetailInfoArray = (NSMutableArray *)searchCarDetailsObj.CarSearchDetailNextAvailable;
                                }
                                if (searchCarDetailsObj.GSTDetails.count > 0) {
                                    carListControllerSelf.carGSTPackageDetails = (NSMutableArray *)searchCarDetailsObj.GSTDetails;
                                   // [DEFAULTS setObject:carListControllerSelf.carGSTPackageDetails forKey:k_gstDetails];
                                }
                                
                                [carListControllerSelf updateData];
                                
                            }
                        } else {
                            
                            // json will come
                            
                            if ([responseObj isKindOfClass:[NSDictionary class]]) {
                                
                                
                                if (![[responseObj objectForKey:@"status"] boolValue]) {
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        // show alert and do accordingly
                                        UIAlertController *alertController = [UIAlertController
                                                                              
                                                                              alertControllerWithTitle:@"Message"
                                                                              
                                                                              message:[responseObj objectForKey:@"message"]
                                                                              
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
                                        UIAlertAction *okAction = [UIAlertAction
                                                                   
                                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                                   
                                                                   style:UIAlertActionStyleDefault
                                                                   
                                                                   handler:^(UIAlertAction *action)
                                                                   
                                                                   {
                                                                       if ([[responseObj objectForKey:@"is_result_for_site_pickup"] boolValue]) {
                                                                           if ([sitePickupData isKindOfClass:[MylesCarDetailsResponse class]]) {
                                                                               MylesCarDetailsResponse *obj = (MylesCarDetailsResponse *)sitePickupData;
                                                                               if([requiredDoorStepArray count] > 0) {
                                                                                   [requiredDoorStepArray removeAllObjects];
                                                                               }
                                                                               locationOfDoorStepDeleivery = nil;
                                                                               self.pickupSegementedControl.selectedSegmentIndex = 0;
                                                                               self.pickUpDropOffViewheightConstraint.constant -= 35;
                                                                               self.locationImageview.hidden = TRUE;
                                                                               [UIView animateWithDuration:0.5 animations:^{
                                                                                   [self.view layoutIfNeeded];
                                                                               }];
                                                                               [self seedSitepickupData:obj searchData:searchMobel];
                                                                               
                                                                           }
                                                                           
                                                                       }
                                                                       else{
                                                                       
                                                                       if ([[_sitePickUpDataDict objectForKey:@"CustomPkgYN"] integerValue]) {
                                                                           [carListControllerSelf.navigationController popViewControllerAnimated:TRUE];
                                                                       } else {
                                                                           if([requiredDoorStepArray count] > 0) {
                                                                               [requiredDoorStepArray removeAllObjects];
                                                                           }
                                                                           locationOfDoorStepDeleivery = nil;
                                                                           [carListControllerSelf.pickupSegementedControl setSelectedSegmentIndex:0];
                                                                           [carListControllerSelf.pickupSegementedControl sendActionsForControlEvents:UIControlEventValueChanged];
                                                                          

                                                                       }
                                                                       }
                                                                       
                                                                   }];
                                        
                                        
                                        
                                        [alertController addAction:okAction];
                                        
                                        [self presentViewController:alertController animated:YES completion:nil];
                                        
                                        return ;

                                    });
                                    

                                    
                                }
                            }
                            
                        }
                        
                    });
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // show alert and do accordingly
                        UIAlertController *alertController = [UIAlertController
                                                              
                                                              alertControllerWithTitle:@"Message"
                                                              
                                                              message:@"Unable to connect server"
                                                              
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   
                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                   
                                                   style:UIAlertActionStyleDefault
                                                   
                                                   handler:^(UIAlertAction *action)
                                                   
                                                   {
                                                       
                                                       if ([[_sitePickUpDataDict objectForKey:@"CustomPkgYN"] integerValue]) {
                                                           [carListControllerSelf.navigationController popViewControllerAnimated:TRUE];
                                                       } else {
                                                           if([requiredDoorStepArray count] > 0) {
                                                               [requiredDoorStepArray removeAllObjects];
                                                           }
                                                           locationOfDoorStepDeleivery = nil;
                                                           [carListControllerSelf.pickupSegementedControl setSelectedSegmentIndex:0];
                                                           [carListControllerSelf.pickupSegementedControl sendActionsForControlEvents:UIControlEventValueChanged];
                                                           
                                                           
                                                       }
                                                       
                                                   }];
                        
                        
                        
                        [alertController addAction:okAction];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        return ;
                        
                    });
                }
            } else {
                // First get AccessToken and then save it and
                // then call same method with new accessToken
                //get a block variable of self
                __block CarListViewController *homeSelf = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CommonFunctions logoutcommonFunction];
                    AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                    [appDelegate getAccessTokenWithCompletion:^{
                        [homeSelf searchDoorStepCarsWithSelectedCityID:cityID];
                    }];
                    
                });
            }
        }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        [CommonFunctions alertTitle:@"Myles" withMessage:KAInternet];
    }
    //  }
}

//-(NSInteger)checkDateWithCurrentTime {
//    NSTimeInterval secondsBetween = [_finalSelectedPassedDate timeIntervalSinceDate:[NSDate date]];
//    NSLog(@"secondtime = %f",secondsBetween);
//    NSInteger hours = secondsBetween/3600;
//    if (hours >= 3) {
//        return 0;
//    } else {
//        return secondsBetween;
//    }
//    
//}


//Gesture method
-(void)locationNameLabelTapped:(UITapGestureRecognizer *)tapGesture {
    //self.pickUpDropOffViewheightConstraint.constant += 35;
    self.locationImageview.hidden = FALSE;
    [self performSegueWithIdentifier:@"showMap" sender:self];

}

//LocationDataDelegate method

-(void)locationDataWithData:(LocationData *)data {
    __block CarListViewController *carListControllerSelf = self;

    if (data.nameOfLocation.length == 0) {
        // no data
        // switch to 1st tab.
        self.pickupSegementedControl.selectedSegmentIndex = 0;
        self.pickUpDropOffViewheightConstraint.constant -= 35;
        self.locationImageview.hidden = TRUE;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
            [self.tableView reloadData];
        }];
    } else {
        //    LocationData *locationData = [[LocationData alloc] initWithLocation:data.location nameOfLocation:data.nameOfLocation toRefreshList:data.toRefreshList];
        locationDataObj = data;
        //Check whether the city selected is different with coming cities.
        
        if (self.selectedCityId == [data.doorStepDeliveryObj.cityID integerValue]) {
            //city is not changed
            cityIsChanged = FALSE;
        } else {
            // City is changed
            cityIsChanged = TRUE;
        }
        
        
        self.selectedCityId = [data.doorStepDeliveryObj.cityID integerValue];
        if (  self.selectedCityId == 2 || self.selectedCityId == 9 || self.selectedCityId == 11 || self.selectedCityId == 3) {
            // Do nothing for ladakh
        }
        else if ([[_sitePickUpDataDict objectForKey:@"CustomPkgYN"] integerValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController
                                                      
                                                      alertControllerWithTitle:@"Message"
                                                      
                                                      message:@"Sorry! We do not provide cars for Ladakh in your city."
                                                      
                                                      preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                
                
                UIAlertAction *okAction = [UIAlertAction
                                           
                                           actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                           
                                           style:UIAlertActionStyleDefault
                                           
                                           handler:^(UIAlertAction *action)
                                           
                                           {
                                               
                                               //NSLog(@"OK action");
                                               
                                               [carListControllerSelf.navigationController popViewControllerAnimated:YES];
                                               
                                           }];
                
                
                
                [alertController addAction:okAction];
                
                [carListControllerSelf presentViewController:alertController animated:YES completion:nil];
                
                return ;
            });
            return ;
            
        }
        if (data.toRefreshList) {
            
            locationOfDoorStepDeleivery = data.location;
            self.locationNameLabel.text = data.nameOfLocation;
            //To call API with kMylesCarDetailsWithHomePickUpAndDropOff
            //And refresh the list.
            //first check the location is active or not
            
           /* [self checkLocationActivewithCityId:locationDataObj.doorStepDeliveryObj.cityID andComletion:^(LocationCheckResults completion) {
                NSLog(@"completion = %d",completion);
                
                if (completion == COMPLETION_SUCCESS) {*/
                    [carListControllerSelf searchDoorStepCarsWithSelectedCityID:locationDataObj.doorStepDeliveryObj.cityID];
               /* } else if (completion == COMPLETION_FAILURE) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController *alertController = [UIAlertController
                                                              
                                                              alertControllerWithTitle:@"Message"
                                                              
                                                              message:@"The location is closed during the time you have selected. Please try again with different timings. Thank you."
                                                              
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   
                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                   
                                                   style:UIAlertActionStyleDefault
                                                   
                                                   handler:^(UIAlertAction *action)
                                                   
                                                   {
                                                       
                                                       //NSLog(@"OK action");
                                                       
                                                       [carListControllerSelf.navigationController popViewControllerAnimated:YES];
                                                       
                                                   }];
                        
                        
                        
                        [alertController addAction:okAction];
                        
                        [carListControllerSelf presentViewController:alertController animated:YES completion:nil];
                        
                        return ;
                    });
                    
                } else if (completion == COMPLETION_SESSION_EXPIRED) {
                    //TO DO WITH LOGOUT COMLETION
                    
                    //                __block CarListViewController *homeSelf = self;
                    //                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [CommonFunctions logoutcommonFunction];
                    //                    AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                    //                    [appDelegate getAccessTokenWithCompletion:^{
                    //                        [homeSelf SearchCar:sender];
                    //                    }];
                    //
                    //                });
                    
                } else if (completion == COMPLETION_NODATA) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController *alertController = [UIAlertController
                                                              
                                                              alertControllerWithTitle:@"Message"
                                                              
                                                              message:@"Apologies for not being able to serve your location! You may pick your car from one of our site locations."
                                                              
                                                              preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        UIAlertAction *okAction = [UIAlertAction
                                                   
                                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                   
                                                   style:UIAlertActionStyleDefault
                                                   
                                                   handler:^(UIAlertAction *action)
                                                   
                                                   {
                                                       
                                                       //NSLog(@"OK action");
                                                       if([requiredDoorStepArray count] > 0) {
                                                           [requiredDoorStepArray removeAllObjects];
                                                       }
                                                       locationOfDoorStepDeleivery = nil;
                                                       //[carListControllerSelf pickUpSegmentTapped:carListControllerSelf];
                                                       [carListControllerSelf.pickupSegementedControl setSelectedSegmentIndex:0];
                                                       [carListControllerSelf.pickupSegementedControl sendActionsForControlEvents:UIControlEventValueChanged];
                                                       //[carListControllerSelf pickUpSegmentTapped:carListControllerSelf.pickupSegementedControl];
                                                       //carListControllerSelf.pickupSegementedControl.selectedSegmentIndex = UISegmentedControlNoSegment;
                                                       //NSLog(@"OK action");
                                                       //[];
                                                       
                                                   }];
                        
                        
                        
                        [alertController addAction:okAction];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        return ;
                    });
                }
            }];*/
            
        } else {
            // switch to 1st tab.
            self.pickupSegementedControl.selectedSegmentIndex = 0;
            self.pickUpDropOffViewheightConstraint.constant -= 35;
            self.locationImageview.hidden = TRUE;
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
                [self.tableView reloadData];
            }];
            
        }
    }
}

- (NSMutableDictionary *)postDataOnApi1:(NSString *)withCityID
{
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    [body setObject:_searchCarModel.pickUpTime forKey:@"TimeFrom"];
    [body setObject:_searchCarModel.dropOffTime  forKey:@"TimeTo"];
    [body setObject:withCityID forKey:@"CityID"];
    [body setObject:@"0" forKey:@"SublocationID"];
    return body;
}


// function for location closed
- (void)checkLocationActivewithCityId:(NSString *)cityID andComletion:(void (^)(LocationCheckResults))completion
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSLog(@"postDictionary = %@",[self postDataOnApi1:cityID]);
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        [self showLoader];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:[self postDataOnApi1:cityID] session:session url:@"checksublocationtimimg" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                  
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                  if (data != nil) {
                                                       NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                       NSString *statusStr = [headers objectForKey:@"Status"];
                                                       if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                       {
                                                           if (!error)
                                                           {
                                                               NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                               NSLog(@"RES : %@",responseJson);
                                                               if ([[responseJson objectForKey:@"status"] integerValue]){
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       // First check whether this city supports DoorStepDelievry or not
                                                                       //Then Proceed HomePickupAvail
                                                                       if ([[responseJson objectForKey:k_HomePickupAvailable] integerValue]){
                                                                           NSString *priceOfHomeickup = [NSString stringWithFormat:@"Doorstep Delivery (₹%ld)",[[responseJson objectForKey:k_HomePickupCharge] integerValue]];
                                                                           
                                                                           [self.pickupSegementedControl setTitle:priceOfHomeickup forSegmentAtIndex:1];
                                                                           [[UILabel appearanceWhenContainedIn:[UISegmentedControl class], nil] setNumberOfLines:2];

                                                                           [DEFAULTS setInteger:[[responseJson objectForKey:k_HomePickupAvailable] integerValue] forKey:k_HomePickupAvailable];
                                                                           [DEFAULTS setInteger:[[responseJson objectForKey:k_HomePickupCharge] integerValue] forKey:k_HomePickupCharge];
                                                                           completion(COMPLETION_SUCCESS);
                                                                       } else {
                                                                           completion(COMPLETION_NODATA);
                                                                       }
                                                                       
                                                                   });
                                                               } else {
                                                                   completion(COMPLETION_FAILURE);
                                                               }
                                                               
                                                           }
                                                       }else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"]))
                                                       {
                                                           completion(COMPLETION_SESSION_EXPIRED);
                                                           
                                                       }
                                                       
                                                   } else {
                                                       completion(COMPLETION_NODATA);
                                                   }
                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        completion(COMPLETION_NODATA);
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}

- (void)seedSitepickupData:(MylesCarDetailsResponse *) mylesdetailresponse searchData:(SearchCarModel *) searchMobel
{
    __block CarListViewController *carListSelf = self;
    __block CarListViewController *carListControllerSelf = self;
    carListSelf.searchCarModel = searchMobel;
    carListSelf.pickUpDateStr = searchMobel.fromDate;
    carListSelf.pickUpTime =searchMobel.pickUpTime ;
    carListSelf.DropOffDateStr =searchMobel.toDate;
    carListSelf.dropOffTime =searchMobel.dropOffTime;
    
    if ([carListSelf.carDetailInfoMA count] > 0)
    {
        [carListSelf.carDetailInfoMA removeAllObjects];
        [carListSelf.nextCarDetailInfoMA removeAllObjects];
        [requiredArray removeAllObjects];
        [nextRequiredArray removeAllObjects];
    }
    
    SearchCarDetails *searchCarDetailsObj = (SearchCarDetails *)mylesdetailresponse.response;
    carListControllerSelf.carDetailInfoMA = (NSMutableArray *)searchCarDetailsObj.CarSearchDetailNormal;
    //carListControllerSelf.carGSTPackageDetails = (NSMutableArray *)searchCarDetailsObj.GSTPackagesDetail;
    if (searchCarDetailsObj.CarSearchDetailNextAvailable.count > 0) {
        carListControllerSelf.nextCarDetailInfoMA = (NSMutableArray *)searchCarDetailsObj.CarSearchDetailNextAvailable;
    }
    if (searchCarDetailsObj.GSTDetails.count > 0) {
        carListControllerSelf.carGSTPackageDetails = (NSMutableArray *)searchCarDetailsObj.GSTDetails;
        //k_gstDetails[DEFAULTS setObject:carListControllerSelf.carGSTPackageDetails forKey:k_gstDetails];
        //NSLog(@"GST Defaults Detail: %@", [DEFAULTS objectForKey:k_gstDetails]);
    }
    //carListSelf.carDetailInfoMA =mylesdetailresponse.response;
    [carListSelf callToUpdateDataOnList];
}

-(void)callSitePickApiWithNewTimingsAndNewCity
{
    __block CarListViewController *carListSelf = self;
    if ([CommonFunctions reachabiltyCheck]) {
        
        NSMutableDictionary *localDict = _sitePickUpDataDict;
        [self showLoader];
        if (_selectedCityId == 2 || _selectedCityId == 9 || _selectedCityId == 11 || _selectedCityId == 3) {
            // Do nothing for ladakh
        } else {
            //Change the ladh status in data dict to send
            [localDict removeObjectForKey:@"CustomPkgYN"];
            [localDict setObject:@"0" forKey:@"CustomPkgYN"];
        }
        // Changing time
        //DropOffTime PickUpTime
        [localDict removeObjectForKey:@"PickUpTime"];
        [localDict setObject:_searchCarModel.pickUpTime forKey:@"PickUpTime"];
        [localDict removeObjectForKey:@"DropOffTime"];
        [localDict setObject:_searchCarModel.dropOffTime forKey:@"DropOffTime"];
        //Changing CityId
        [localDict removeObjectForKey:@"CityID"];
        [localDict setObject:[NSString stringWithFormat:@"%ld",(long)_selectedCityId] forKey:@"CityID"];
        [localDict removeObjectForKey:@"ToDate"];
        [localDict setObject:_searchCarModel.toDate forKey:@"ToDate"];
        NSLog(@"localDict = %@",localDict);
        //ApiClass *callApi = [[ApiClass alloc]initWithDict:localDict];
        SearchCarModel *searchMobel = [[SearchCarModel alloc]initWithDict:localDict];
        
        
        if ([CommonFunctions reachabiltyCheck]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self showLoader];
            });
            [self showLoader];
            ApiClass *callApi = [[ApiClass alloc]initWithDict:localDict];
           // searchMobel = [[SearchCarModel alloc]initWithDict:localDict];
            [callApi SearchCarApiWithCompletionHandler:^(MylesCarDetailsResponse * _Nullable obj, BOOL isValidToken) {
                
                
                if (isValidToken) {
                    if (obj!=nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                        });
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (![DEFAULTS integerForKey:k_HomePickupAvailable]) {
                                NSString *priceOfHomeickup = [NSString stringWithFormat:@"Doorstep Delivery"];
                                [self.pickupSegementedControl setTitle:priceOfHomeickup forSegmentAtIndex:1];
                                
                            }
                            else{
                                NSString *priceOfHomeickup = [NSString stringWithFormat:@"Doorstep Delivery (₹%ld)",[[DEFAULTS valueForKey:k_HomePickupCharge] integerValue]];
                                [self.pickupSegementedControl setTitle:priceOfHomeickup forSegmentAtIndex:1];
                                
                            }

                            [self seedSitepickupData:obj searchData:searchMobel];
                        });
                        
                        
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                        });
                    }
                    
                } else {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [CommonFunctions logoutcommonFunction];
                        AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                        [appDelegate getAccessTokenWithCompletion:^{
                            [self callSitePickApiWithNewTimingsAndNewCity];
                        }];
                        
                    });
                }
                
                
                
                
            }];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            [CommonFunctions alertTitle:@"Myles" withMessage:KAInternet];
        }
        
        
      /*  NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
       

        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:localDict session:session url:kMylesCarDetailsV1 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });
                                                   
                                                   NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           NSLog(@"RES : %@",responseJson);
                                                           if ([[responseJson objectForKey:@"status"] integerValue]){
                                                               
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   
                                                                   
                                                                   MylesCarDetailsResponse *mylesdetailresponse = [[MylesCarDetailsResponse alloc] initWithData:data error:nil];
                                                                   
                                                                   [self seedSitepickupData:mylesdetailresponse searchData:searchMobel];
                                                               });
                                                               
                                                           } else {
                                                               NSLog(@"Sourabh shekhar singh");
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   UIAlertController *alertController = [UIAlertController
                                                                                                         
                                                                                                         alertControllerWithTitle:@"Message"
                                                                                                         
                                                                                                         message:[responseJson objectForKey:@"message"]
                                                                                                         
                                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   UIAlertAction *okAction = [UIAlertAction
                                                                                              
                                                                                              actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                                                              
                                                                                              style:UIAlertActionStyleDefault
                                                                                              
                                                                                              handler:^(UIAlertAction *action)
                                                                                              
                                                                                              {
                                                                                                  
                                                                                                  //NSLog(@"OK action");
                                                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                                                                  
                                                                                                  
                                                                                                  
                                                                                              }];
                                                                   
                                                                   
                                                                   
                                                                   [alertController addAction:okAction];
                                                                   
                                                                   [self presentViewController:alertController animated:YES completion:nil];
                                                                   
                                                                   return ;
                                                               });
                                                           
                                                           }
                                                           
                                                       } else {
                                                           // First get AccessToken and then save it and
                                                           // then call same method with new accessToken
                                                           //get a block variable of self
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [CommonFunctions logoutcommonFunction];
                                                               AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                                                               [appDelegate getAccessTokenWithCompletion:^{
                                                                   [carListSelf callSitePickApiWithNewTimingsAndNewCity];
                                                               }];
                                                               
                                                           });
                                                       }
                                                           }
                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];*/

        
    }

    
}

//- (void)scrollViewWillBeginDragging:(UIScrollView *)activeScrollView {
//    //logic here
//    NSLog(@"Hide search labels and uitab bar controller ");
//    if (!viewIsScrolling) {
//        [self.navigationItem setHidesBackButton:YES];
//        viewIsScrolling = TRUE;
//        tableViewBottomHeightConstraint.constant = 0;
//        
//        [self setTabBarVisible:![self tabBarIsVisible] animated:YES completion:^(BOOL finished) {
//            NSLog(@"finished");
//            
//        }];
//    }
//    
//
//}
//
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    __block CarListViewController *blockSelf = self;
//    NSLog(@"show search labels and uitab bar controller ");
//    [self.navigationItem setHidesBackButton:NO];
//
//    [self setTabBarVisible:![self tabBarIsVisible] animated:YES completion:^(BOOL finished) {
//        NSLog(@"finished");
//        blockSelf->viewIsScrolling = FALSE;
//
//        blockSelf->tableViewBottomHeightConstraint.constant = 41;
//
//    }];
//
//}
//
//// pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion
//- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion {
//    
//    // bail if the current state matches the desired state
//    if ([self tabBarIsVisible] == visible) return (completion)? completion(YES) : nil;
//    NSLog(@"visible ");
//    // get a frame calculation ready
//    CGRect frame = self.tabBarController.tabBar.frame;
//    CGFloat height = frame.size.height;
//    CGFloat offsetY = (visible)? -height : height;
//    
//    // zero duration means no animation
//    CGFloat duration = (animated)? 0.3 : 0.0;
//
//    [UIView animateWithDuration:duration animations:^{
//        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
//    } completion:completion];
//}
//
////Getter to know the current state
//- (BOOL)tabBarIsVisible {
//    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
//}

@end
