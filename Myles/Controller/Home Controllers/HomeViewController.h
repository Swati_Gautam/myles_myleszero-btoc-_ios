//
//  HomeViewController.h
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <GoogleMaps/GoogleMaps.h>
#import "CityList.h"
#import <Google/Analytics.h>

/*
 To transfer data from home view comtroller to the alertview controller
 */

@protocol HomeViewControllerDelegate <NSObject>

@required
- (void)sublocationFromHomeViewController:(NSString *)nameData;
- (void)dataFromHomeViewControllerFunction : (NSArray *)array NavigationTitle : (NSString *)title CityId : (NSInteger)cityid Sublocation : (NSInteger )sublocationID SubLocationAddress : (NSString *)sublocationAdd;

@end



@interface HomeViewController : UIViewController < UITableViewDataSource, UITableViewDelegate,SWRevealViewControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *bookNowBtn;
@property (nonatomic, weak) id<HomeViewControllerDelegate> delegate;
    
/*
 List button in iOS
 */

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;

@property (weak, nonatomic) IBOutlet UIButton *btnAge;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ladackViewHeight;
@property (weak, nonatomic) IBOutlet UIView *ladackView;
@property (weak, nonatomic) IBOutlet UIView *offerView;

/*
 Property declaration for the new HomeView ---- Search Sublocation
 */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITableView *tblHome;


@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (weak, nonatomic) IBOutlet UICollectionView *offerCollectionView;

@end
