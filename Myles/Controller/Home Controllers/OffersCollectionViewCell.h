//
//  CustomCollectionViewCell.h
//  Myles
//
//  Created by Myles   on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>
//#import <SDWebImage/SDWebImageDownloader.h>
//#import <SDWebImage/SDWebImageManager.h>


@interface OffersCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgOffers;

@end
