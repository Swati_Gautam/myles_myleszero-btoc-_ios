//
//  PackegeFilterViewController.h
//  Myles
//
//  Created by IT Team on 29/02/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PackageFilterProtocol <NSObject>

- (void)getFilterOption:(NSInteger)filterOption;

@end

@interface PackegeFilterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *hourlyButton;
@property (weak, nonatomic) IBOutlet UIButton *monthlyButton;
@property (weak, nonatomic) IBOutlet UIButton *weeklyButton;
@property (weak, nonatomic) IBOutlet UIButton *dailyButton;
@property (weak, nonatomic) id<PackageFilterProtocol> delegate;

@end
