

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "PackegeFilterViewController.h"
#import "NSUserDefaults+Utility.h"

@class SearchCarModel;
@class Helper;
@interface CarListViewController : UIViewController < UITableViewDataSource, UITableViewDelegate> {
    
}

@property (strong, nonatomic) NSMutableArray *carDetailInfoMA;
@property (strong, nonatomic) NSMutableArray *carGSTPackageDetails;
@property (strong, nonatomic) NSMutableArray *nextCarDetailInfoMA;
@property (strong,nonatomic) NSMutableArray *doorStepDetailInfoArray;
@property (strong,nonatomic) NSMutableArray *nextDoorStepDetailInfoArray;
@property (strong, nonatomic) NSArray *cityModelArray;
@property (strong, nonatomic) NSMutableArray *modelImagesMA;
@property (assign, nonatomic)  NSInteger selectedCityId;
@property (assign, nonatomic)  NSInteger tempCityId;

@property (assign, nonatomic)  NSInteger sublocationIdDetails;
@property (strong, nonatomic) NSString *sublocationAddress;
@property (strong, nonatomic) NSString *packageType;
@property (strong, nonatomic) NSMutableArray *packageMA;
@property (strong, nonatomic) NSString *sublocationTitle;
@property (strong, nonatomic) NSDictionary *durationDict;
@property (strong, nonatomic) NSString *pickUpDateStr,*pickUpTime,*DropOffDateStr,*dropOffTime;
@property (strong, nonatomic) NSString *ageMessageStr;
@property (assign, nonatomic) BOOL isSublocationSelected;
@property (strong, nonatomic) SearchCarModel *searchCarModel;
@property (strong, nonatomic) NSString *SublocationTitleVal;
@property (strong, nonatomic) NSString *sublocationIDVal;
@property (strong, nonatomic) NSString *tripDurationSelected;
@property (strong, nonatomic) NSString *timeToTripStart;

    
@property (strong , nonatomic) NSMutableDictionary *sitePickUpDataDict;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (void)initilise : (NSString *)title CityId : (NSInteger)cityid Sublocation : (NSInteger )sublocationID SubLocationAddress : (NSString *)sublocationAdd;

@property(nonatomic , weak) IBOutlet UIButton *lblSort;

- (IBAction)listIconAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *filterIcon;
@property (weak, nonatomic) IBOutlet UISegmentedControl *pickupSegementedControl;
@property (weak, nonatomic) IBOutlet UIView *pickUpDropOffView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickUpDropOffViewheightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *locationImageview;
@property (weak, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentedControlHeightConstraint;

@property (strong , nonatomic) NSString *previousMilliSec;
@property (strong , nonatomic) NSString *previousDateTime;

@end
