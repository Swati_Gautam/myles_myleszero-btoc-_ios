//
//  SublocationListViewController.h
//  Myles
//
//  Created by IT Team on 17/08/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SublocationDetails;

@protocol SublocationListViewControllerDelegate <NSObject>

@required
- (void)selectedSublocationData:(NSString *)SublocationID SublocationTitle : (NSString *)title Sublocationindex : (NSIndexPath *)sublocationindex isSublocationSelected : (BOOL)issublocationSelected :(NSString *)strModelId;

@end


@interface SublocationListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource> {
    BOOL isSublocation;
    NSString *selectedSublocation;
    NSString *selectedSublocationID;
    NSIndexPath *selectedSublocationindex;
    BOOL issublocationSelected;
}

@property (strong, nonatomic) NSArray *sublocationList;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) NSString *selectedSublocation;
@property (weak, nonatomic) NSString *selectedSublocationID;
@property (nonatomic ,retain) NSDictionary *sublocationData;
@property (strong, nonatomic) NSIndexPath *selSublocationindex;
@property (assign, nonatomic) BOOL issublocationSelected;
@property NSString *strSelectedData;
@property NSString *strModelId;

@property (strong,nonatomic) NSString * currentSelectedLocation;



@property (nonatomic, weak) id<SublocationListViewControllerDelegate> delegate;

@end

//NSArray *sublocationList
