    //
//  HomeViewController.m
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "HomeViewController.h"
#import "CarDetailInformation.h"
#import "LadakhbookingViewController.h"
#import "Myles-Swift.h"
#import "LoginViewController.h"
#import "MenuViewController.h"
#import "UserProfileVC.h"
#import "Haneke.h"
#import <SafariServices/SafariServices.h>
#import "CTShowcase.h"
#import "PageContentViewController.h"
#import "OffersCollectionViewCell.h"
#import "OfferWebVC.h"

@import FirebaseMessaging;
@import FirebaseInstanceID;


@import FirebaseAnalytics;

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

typedef enum completionResults
{
    COMPLETION_SUCCESS,
    COMPLETION_FAILURE,
    COMPLETION_NODATA,
    COMPLETION_SESSION_EXPIRED
} DataCompletionResults;

@interface HomeViewController ()<LadakhbookingViewController,CityProtocol,LoginProtocol,SFSafariViewControllerDelegate, UIPageViewControllerDelegate, UIPageViewControllerDataSource, UICollectionViewDelegate, UICollectionViewDataSource>
{
    
    BOOL kLadakhStatus;
    NSInteger selectedCityId;

    
    __weak IBOutlet UIImageView *saleImageview;
    LadakhbookingViewController *secondViewController;
    NSMutableArray *maCityModel;
    SearchCarModel *searchMobel;
    //New Object
    BOOL isDateSet,isConfirmAge, isLadakhAvail;
    NSArray *arrTitle;
    NSMutableArray *maTitleDetail;
    UIDatePicker *startTimePicker;
    NSDateFormatter *dateFormatter;
    NSDate *finalSelectedDate;
    NSString *tripDuration;
    NSString *timeToTripStart;
    NSString * urlPath, *redirectURl;
    NSString *previousDateTime, *currentDateTime;
    NSTimeInterval currentMilliSec,previousMilliSec;
    //__weak IBOutlet NSLayoutConstraint *_tblHeight;
    //__weak IBOutlet UIView *_offerView;
   // __weak IBOutlet UIScrollView *_scrollView;
    
    
    
    NSArray * contentArr;
    NSMutableArray * imagessssArray;

    
    
}

@property (weak, nonatomic) IBOutlet UILabel *_lblErrorLadakh;

@property (nonatomic, assign) DataCompletionResults completionResults;
@end

@implementation HomeViewController

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

#pragma -mark Default Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    imagessssArray = [NSMutableArray new];
    
    [self callAPIForPagination];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:239/255.0 green:35/255.0 blue:0/255.0 alpha:1.0];
    
    /*
    [InviteReferrals launch:@"22369" Email:@"chetan.r@mylescars.com" mobile:@"9810709386" name:@"Chetan Rajauria" SubscriptionID:nil];

    [InviteReferrals setupUserID:@"chetan.r@mylescars.com" mobile:@"9810709386" name:@"Chetan Rajauria" gender:@"Male" shareLink:@"https://www.google.com" shareTitle:@"SHare Title" shareDesc:@"SHare Description" shareImg:imageStr customValue:@"Custom Value" campaignID:@"22369" flag:@"1" SubscriptionID:nil];
    
    [InviteReferrals tracking: @"Install App"  orderID: @"" purchaseValue: nil email: @"" mobile: @""  name: @""  referCode: @""  uniqueCode: @"" isDebugMode: YES ComplitionHandler:^(NSMutableDictionary * irtrackingResponse) {
        NSLog(@"Tracking Response callback json will come here i.e. = %@",irtrackingResponse);
    }];
     
     */



   // [self Initialsetup];
    [self.view endEditing:YES];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    arrTitle = [[NSArray alloc]initWithObjects:@"City",@"Start Time",@"End Time", nil];
    maTitleDetail = [[NSMutableArray alloc]initWithObjects:@"Select city",@"Select start time",@"Select end time", nil];
    isDateSet = false;
    
  
   // _bookNowBtn.layer.cornerRadius = 5;
    _offerView.hidden = true;
    [self updateScrollContentHeight];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadFlashView:) name:@"ContainerAvailable" object:nil];

    //Adding GTM notification Observer.
    
    [FIRAnalytics logEventWithName:kFIREventAppOpen
                        parameters:nil];
    
    secondViewController = [[LadakhbookingViewController alloc] init];
    secondViewController.delegate = self;
    
    /*
     Variables declaration
     */
    
   // self.automaticallyAdjustsScrollViewInsets = NO;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(saleImageViewTaped)];
    [saleImageview addGestureRecognizer:singleTap];
    //[saleImageview setUserInteractionEnabled:true];
    
#ifdef DEBUG
#else
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Home Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

#endif
    // open login screen through menu
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"openLogin"
                                               object:nil];
    

    // Placing code from viewWillAppear to viewDidLoad
    AppDelegate *appdelegate = [AppDelegate getSharedInstance];
    appdelegate.delegate = self;
    self.navigationController.navigationBar.hidden = TRUE;
    self.revealViewController.delegate = self;
    
    NSString *accessToken = [DEFAULTS objectForKey:K_AccessToken];
    //[self topCityListJson]; //SWati Commented becoz City List Json File In the Project
    if (accessToken.length > 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           [self callApiForCityList:true]; //Swati Uncommented becoz City List From API
        //[self getCurrentLocation];     //Swati Uncommented

        });
    } else {
       // [GMDCircleLoader setOnView:self.view withTitle:@"Please wait..." animated:YES];
        
        [appdelegate getAccessTokenWithCompletion:^{
            //
            dispatch_async(dispatch_get_main_queue(), ^{
               [self callApiForCityList:true];
                //[self getCurrentLocation];    //Swati Uncommented
            });
        }];
    }
    
    self.btnAge.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.btnAge.layer.borderWidth = .5;
    self.btnAge.layer.cornerRadius = 5;
   
    [self.btnAge setImage:[UIImage imageNamed: @"RectCheckBoxEmpty"] forState:UIControlStateNormal];
    //sdsd
    //Register Nib
    [self.tblHome registerNib:[UINib nibWithNibName:@"HomeCell" bundle:nil] forCellReuseIdentifier:@"HomeCell1"];
    self.tblHome.separatorColor = [UIColor lightGrayColor];
    
   // [self animateNewFeature];
}

// Mummy
-(void)animateNewFeature
{
    CTShowcaseView * showcase = [[CTShowcaseView alloc] initWithTitle:@"You can search a Car just by clicking here." message:@"" key:nil dismissHandler:^{
        NSLog(@"Button Dismissed");
    }];
    
    CTDynamicGlowHighlighter * highlighter = [CTDynamicGlowHighlighter new];
    [highlighter setHighlightType:CTHighlightTypeRect];
    [highlighter setAnimDuration:0.5];
    [highlighter setGlowSize:25];
    [highlighter setMaxOffset:15];
    
    [showcase setHighlighter:highlighter];
    
    [showcase setupForView:_bookNowBtn offset:CGPointZero margin:0];
    [showcase show];
}


-(void)updateScrollContentHeight{
    int height = 210;
    if(_btnAge.hidden){
        _ladackViewHeight.constant = 0;
    }
    else{
        _ladackViewHeight.constant = 75;
        height = height + 75;
    }
    if(!_offerView.hidden){
        height = height + 270;
    }

    CGFloat heightF = (CGFloat) height;
    
    _scrollContentViewHeight.constant = heightF;
    [self.view layoutIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = TRUE;
    __lblErrorLadakh.hidden = true;
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    if ([userInfo[@"Selectedindex"] isEqualToString:@"0"])
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
        LoginProcess *editController = (LoginProcess *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginProcess"];
        
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:editController];
        
        if (userInfo[@"otherdestination"] != nil) {
            editController.delegate = self;
            editController.strOtherDestination = userInfo[@"otherdestination"];
            
        }
        navigationController.viewControllers = @[editController];
        
        [self presentViewController:navigationController animated:YES completion:nil];
    }
}


#pragma -mark Protocol for login with other destination
- (void)afterLoginChangeDestinationWithStrDestination:(NSString * _Nonnull)strDestination
{
        //[revealController pushFrontViewController:navController animated:YES];
        
        SWRevealViewController *revealController = self.revealViewController;        
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:(strDestination.integerValue == 1)?k_MyRideStoryBoard:k_profileStoryBoard bundle:nil];
        UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:(strDestination.integerValue == 1)?k_RidesNavigation:k_profileNavigation];
        
        [(MenuViewController *)revealController.rearViewController setSelectedIndex:strDestination.integerValue];
        
        if (strDestination.integerValue ==2) {
            ProfileViewController *homeController = [storyBoard instantiateViewControllerWithIdentifier:k_profileController];
            navController.viewControllers = @[homeController];
        }
        else if (strDestination.integerValue ==1)
        {
            RideHistoryVC *homeController = [storyBoard instantiateViewControllerWithIdentifier:@"rideHistory"];
            navController.viewControllers = @[homeController];
        }
        ////NSLog(@"CONRTOL : %@",navController.viewControllers);
    
        [revealController pushFrontViewController:navController animated:YES];
        //  [self.revealViewController revealToggleAnimated:YES];
}

/**
   call api to get city list.
**/
-(void)getCurrentLocation
{
    if ([CommonFunctions reachabiltyCheck] && [CLLocationManager locationServicesEnabled]) {

        //[self showLoader];
    [CommonFunctions GetCurrentLocation:^(NSDictionary *dictData)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             [SVProgressHUD dismiss];
         });
         NSLog(@"Current Location %@",dictData);
         int index = 0;
         BOOL isMatch = false;
         if (dictData != nil && maTitleDetail != nil) {
             for (int i = 0; i< maCityModel.count ; i++){
                 cityModel *city = maCityModel[i];
                 if ([dictData[@"City"] rangeOfString:city.cityname].location == NSNotFound) {
                     isMatch = false;
                 }
                 else{
                     index = i;
                     isMatch = true;
                     break;
                 }
             }
             
             if(isMatch){
                 cityModel *city = maCityModel[index];
                 NSLog(@"matched Location %@",city.cityId);
                 maTitleDetail[0] = city.cityname;//maCityModel[index][@"CityName"];
                 selectedCityId = [city.cityId integerValue];//maCityModel[index][@"CityId"];
                 if ([maTitleDetail containsObject:@"Delhi"] || [maTitleDetail containsObject:@"Gurugram"]|| [maTitleDetail containsObject:@"Chandigadh"]|| [maTitleDetail containsObject:@"Noida/Ghazaibad"] || [maTitleDetail containsObject:@"Delhi/NCR"]){
                     _btnAge.hidden = false;
                     
                     [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
             }
             else{
                 _btnAge.hidden = true;
                 isConfirmAge = false;
                 [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
             }
             //NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF contains%@",[[dictData[@"State"] lowercaseString] containsString:@"delhi"] ? @"Delhi":dictData[@"State"]];
         /*   NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.cityname contains[cd] %@",[[dictData[@"City"] lowercaseString] containsString:@"delhi"] ? @"Delhi":dictData[@"City"]];
             NSMutableArray * filteredArray = [[maCityModel filteredArrayUsingPredicate:bPredicate] mutableCopy];
             NSLog(@"HERE %@",filteredArray);
             if (filteredArray != nil && filteredArray.count > 0 ) {
                 maTitleDetail[0] = filteredArray[0][@"CityName"];
                 selectedCityId = filteredArray[0][@"CityId"];
                 if ([maTitleDetail containsObject:@"Delhi"] || [maTitleDetail containsObject:@"Gurugram"]|| [maTitleDetail containsObject:@"Chandigadh"]|| [maTitleDetail containsObject:@"Noida/Ghazaibad"]){
                        _btnAge.hidden = false;
                     
                     [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
                 }
                 else{
                         _btnAge.hidden = true;
                         isConfirmAge = false;
                         [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];

                     
                 }*/
                 //([cityM.cityname isEqualToString:@"Delhi"] || [cityM.cityname isEqualToString:@"Noida/Ghazaibad"] || [cityM.cityname isEqualToString:@"Chandigadh"] ||[cityM.cityname isEqualToString:@"Gurugram"])
                 [self updateScrollContentHeight];

                 dispatch_async(dispatch_get_main_queue(), ^{
                     [_tblHome reloadData];
                 });
             }
         }
     }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *errorAlert = [[UIAlertView alloc]
                                       initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];
        });
    }
}

//GTM Notification Method
-(void)loadFlashView:(NSNotification *)notification {

    NSDictionary *dictionary = [notification userInfo];
    urlPath = dictionary[@"BannerImageURL"];
    redirectURl = dictionary[@"RedirectURL"];
    NSURL *url =   [[NSURL alloc] initWithString:urlPath];
    if (urlPath.length > 0){
       // _offerView.hidden = false;
        saleImageview.hidden = true;
        _offerView.hidden = true;
        saleImageview.userInteractionEnabled = false;
      //  NSData * imageData = [[NSData alloc] initWithContentsOfURL: url];
        
        
        [saleImageview hnk_setImageFromURL:[NSURL URLWithString:urlPath] placeholder:[UIImage imageNamed:k_defaultCarFullImg] success:^(UIImage *image) {
            //saleImageview.hidden = false; //changed - 19th feb 2019
            _offerView.hidden = false;

            saleImageview.userInteractionEnabled = false; //changed - 19th feb 2019
            saleImageview.image = image;
            [self updateScrollContentHeight];
        } failure:^(NSError *error) {
            NSLog(@"Error = %@",[error description]);
        }];
         
         
         // uncomment this line -- chetan
         

        // [_scrollView setContentSize:(CGSizeMake(_scrollView.frame.size.width, tableViewHeight))];
    }
    else{
        saleImageview.hidden = true;
        saleImageview.userInteractionEnabled = false;
        _offerView.hidden = true;
        //_offerView.hidden = true;
       // [_scrollView setContentSize:(CGSizeMake(_scrollView.frame.size.width, tableViewHeight))];
        [self updateScrollContentHeight];

    }
  

}

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}
-(void)saleImageViewTaped{
    
    [FIRAnalytics logEventWithName:@"offer_clicked_Mkt"
                        parameters:nil];
//    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:redirectURl]];
//    svc.delegate = self;
//    [self presentViewController:svc animated:YES completion:nil];
    UIStoryboard *storyboard = k_storyBoard(k_mainStoryBoard);
    CouponViewController *offerController = [storyboard instantiateViewControllerWithIdentifier:@"CouponViewController"];
    offerController.fromScreen = @"SalesImage";
    //offerController.urlToNavigate =  [NSURL URLWithString:redirectURl];
      [self.navigationController presentViewController:offerController animated:true completion:nil];
    
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)topCityListJson
{
    NSDictionary *dict = [self JSONFromFile];
    
    NSArray *Cities = [dict objectForKey:@"TopList"];
    maCityModel = [[NSMutableArray alloc]init];
    for (NSDictionary *city in Cities) {
        NSString *cityId = [city valueForKey:@"CityId"];
        NSString *cityName = [city valueForKey:@"CityName"];
        cityModel *cityM =[[cityModel alloc]initWithCityN:cityName cityI:cityId];
        [maCityModel addObject:cityM];
    }
    NSDate *previousDate = [DEFAULTS valueForKey:@"previousDate"];
    if(previousDate != nil){
        NSDate* date = [NSDate date];
        NSTimeInterval secondsBetween = [date timeIntervalSinceDate:previousDate];
        if( secondsBetween < 86400){
            NSArray *cityList = [DEFAULTS valueForKey:@"CityListing"];
            if(cityList != nil){
                for (int i = 0 ; i < cityList.count; i++){
                    NSDictionary *city = cityList[i];
                    NSString *cityId = [city valueForKey:@"CityId"];
                    NSString *cityName = [city valueForKey:@"CityName"];
                    cityModel *cityM =[[cityModel alloc]initWithCityN:cityName cityI:cityId];
                    [maCityModel addObject:cityM];
                }
            }
            
        }

    }

    
}

- (NSDictionary *)JSONFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CityList" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}


-(void)callApiForCityList :(BOOL)isCurrentLoc
{
    if ([CommonFunctions reachabiltyCheck] && maCityModel == nil) {
      //  [self showLoader];
        ApiClass *callApi = [[ApiClass alloc]init];
        TICK;
        [callApi cityListApiWithCompletionHandler:^(NSArray * _Nullable arrCityList, BOOL isValidToken,NSString *responseString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            NSLog(@"%@", responseString);
            
            if ([responseString isEqualToString:@"Success"]) {
                if (isValidToken) {
                    
                    if (arrCityList.count > 0) {
                        if (arrCityList != nil && arrCityList.count > 0) {
                            maCityModel = [[NSMutableArray alloc]init];
                            for (NSDictionary *cityDict in arrCityList)
                            {
                                
                                cityModel *cityM =[[cityModel alloc]initWithCityN:[cityDict valueForKey:k_cityName] cityI:[cityDict valueForKey:k_cityId]];
                                [maCityModel addObject:cityM];
                                
                            }
                            TOCK;
                            if (isCurrentLoc)
                                [self getCurrentLocation];
                            else
                                [self selectCityClick:self];
                        }
                        else
                        {
                            
                        }
                        
                    } else {
                        
                    }
                }
                else {
                    // First get AccessToken and then save it and
                    // then call same method with new accessToken
                    //get a block variable of self
                    //__block HomeViewController *homeSelf = self;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [CommonFunctions logoutcommonFunction];
                        AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                        [appDelegate getAccessTokenWithCompletion:^{
                            //[homeSelf searchCar];
                        }];
                        
                    });
                }

            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MYLES" message:responseString delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                });

            }
        }];
    }
    else
    {
        if (![CommonFunctions reachabiltyCheck])
        {
            AppDelegate *appDelegate = [AppDelegate getSharedInstance];
            [appDelegate noNetworkConnectionAlert];
    }
        
}
}

-(void)viewDidDisappear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = FALSE;

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];

    kLadakhStatus = false;
   // isConfirmAge = false;
   // [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
    //self.navigationController.navigationBarHidden = FALSE;
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    }); 
}




#pragma mark:- RevealView Controller Delegate
-(void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    switch (position) {
        case FrontViewPositionLeft:
        {
            //Keyboard is resigned AND TABLE IS HIDDED
            self.revealViewController.delegate = nil;

        }
            break;
        case FrontViewPositionRight:
        {
            //////NSLog(@"RIGHT ");
            [revealController tapGestureRecognizer];
            self.revealViewController.delegate = self;
            
        }
            break;
        default:
            break;
    }
}


#pragma -mark Initial SetUP
- (void)Initialsetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    [self.leftBtn setImage:UIImageName(k_listImg) forState:UIControlStateNormal];
    if ( revealViewController )
    {
        self.revealViewController.delegate = self;
        UITapGestureRecognizer *gestureRecognizer;
        if (gestureRecognizer == nil) {
            gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goAction)];
        }
        gestureRecognizer = self.revealViewController.tapGestureRecognizer;
        [self.leftBtn addTarget:self.revealViewController action: @selector( revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
    
//    [self.leftBtn addTarget:self action:@selector(customMenuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    

}


-(void)goAction {
    [self.revealViewController revealToggle:self];
}
#pragma mark:- Custom Menu Button
//- (void)customMenuButtonAction:(UIButton *)leftBtn
//{
//   
//}

#pragma mark- Appdelegate Delagetes

- (void) locationDidChanged : (NSString *)address location : (CLLocation *)location
{
    if (address.length && maTitleDetail != nil)
    {
        int index = 0;
        BOOL isMatch = false;
            for (int i = 0; i< maCityModel.count ; i++){
                cityModel *city = maCityModel[i];
                if ([address rangeOfString:city.cityname].location == NSNotFound) {
                    isMatch = false;
                }
                else{
                    index = i;
                    isMatch = true;
                    break;
                }
            }
        
            if(isMatch){
                cityModel *city = maCityModel[index];
                maTitleDetail[0] = city.cityname;//maCityModel[index][@"CityName"];
                selectedCityId = [city.cityId integerValue];//maCityModel[index][@"CityId"];
                if ([maTitleDetail containsObject:@"Delhi"] || [maTitleDetail containsObject:@"Gurugram"]|| [maTitleDetail containsObject:@"Chandigadh"]|| [maTitleDetail containsObject:@"Noida/Ghazaibad"]){
                    _btnAge.hidden = false;
                    
                    [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
                }
                else{
                    _btnAge.hidden = true;
                    isConfirmAge = false;
                    [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
                }
                NSLog(@"address get city :%@",address);
                
                [[FIRMessaging messaging] subscribeToTopic:address];
        
       /* NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.cityname contains[cd] %@",[[address lowercaseString] containsString:@"delhi"] ? @"Delhi":address];
        
        NSMutableArray * filteredArray = [[maCityModel filteredArrayUsingPredicate:bPredicate] mutableCopy];
        NSLog(@"HERE %@",filteredArray);
        if (filteredArray != nil && filteredArray.count > 0 ) {
            cityModel *cityM = filteredArray[0];
            maTitleDetail[0] = cityM.cityname;
            selectedCityId = [cityM.cityId integerValue];
            if ([maTitleDetail containsObject:@"Delhi"] || [maTitleDetail containsObject:@"Gurugram"]|| [maTitleDetail containsObject:@"Chandigadh"]|| [maTitleDetail containsObject:@"Noida/Ghazaibad"]){
                _btnAge.hidden = false;
                 [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
            }
            else{
                    _btnAge.hidden = true;
                    isConfirmAge = false;
                    [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
                    
            }*/
            [self updateScrollContentHeight];

            dispatch_async(dispatch_get_main_queue(), ^{
                [_tblHome reloadData];
            });
            }
     
//        currentCityid =  [self searching:address];
    }
    else
    {
        ////NSLog(@"not called");
        
        
    }
}


- (void)locationDidDisable
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    if ([CommonFunctions getMajorSystemVersion] > 7.0 )
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kSetting message:kLocationServiceOnMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           //[CommonFunctions alertTitle:KMessage withMessage:KServiceNotAvailble];
                                       }];
        [alert addAction:cancelaction];
        
        UIAlertAction *alertaction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                      {
                                          @try {
                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                          }
                                          @catch (NSException *exception) {
                                              NSLog(@"exception = %@",exception);
                                          }
                                      }];
        [alert addAction:alertaction];
        
        
        
        [self presentViewController:alert animated:true completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:KMessage message:kLocationServiceOnMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag = 5;
        [alert show];
    }
}


/*
 Functions to handle delegate class
*/
- (void)passDataToSorryNotAvailableAlertView
{
    
    if ([_delegate respondsToSelector:@selector(sublocationFromHomeViewController:)])
    {
        [_delegate sublocationFromHomeViewController:@"sublocation address from home view"];
    }
    
}

- (NSInteger)hoursBetween:(NSDate *)firstDate and:(NSDate *)secondDate {
    NSUInteger unitFlags = NSCalendarUnitHour;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:firstDate toDate:secondDate options:0];
    return [components hour];
}



#pragma mark PickerView DataSource

+ (void)setPresentationStyleForSelfController:(UIViewController *)selfController presentingController:(UIViewController *)presentingController
{
    if ([NSProcessInfo instancesRespondToSelector:@selector(isOperatingSystemAtLeastVersion:)])
    {
        //iOS 8.0 and above
        presentingController.providesPresentationContextTransitionStyle = YES;
        presentingController.definesPresentationContext = YES;
        
        [presentingController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    else
    {
        [selfController setModalPresentationStyle:UIModalPresentationCurrentContext];
        [selfController.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}


-(void)callForBooking {
    
    NSString *rsaNumber = [DEFAULTS valueForKey:kRoadsideSupport];
    NSString *mylesCenterNum = [DEFAULTS valueForKey:k_mylesCenterNumber];
    
    NSString *messageStr =[NSString stringWithFormat:@"Call on %@  to book your Myles ride",mylesCenterNum];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"No Internet Connection"
                                          message:messageStr
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"Cancel action");
                               }];
    
    UIAlertAction *callButton = [UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *url =[NSString stringWithFormat:@"tel:%@",mylesCenterNum];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:callButton];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - UICollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self->imagessssArray.count;
}

- (OffersCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OffersCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"idOffersCell" forIndexPath:indexPath];
    //self->imagessssArray objectAtIndex:index
    
    NSString *strImageUrl = [self->imagessssArray objectAtIndex:indexPath.row];
    NSString* strAddPercentEncodeURL = [strImageUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    cell.layer.cornerRadius = 4.0;
    cell.imgOffers.layer.cornerRadius = 4.0;
    [cell.imgOffers hnk_setImageFromURL:[NSURL URLWithString:strAddPercentEncodeURL] placeholder: [UIImage imageNamed:@"placeholder_scroll"]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [FIRAnalytics logEventWithName:@"Carousel_Clicked"
                        parameters:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    
    NSString * str = [[self->contentArr objectAtIndex:indexPath.row] valueForKey:@"url"];
    
    if ([CommonFunctions reachabiltyCheck]) {
        
        if ([str containsString:@"discount-offers"])
        {
            UIStoryboard *storyboard = k_storyBoard(k_mainStoryBoard);
            CouponViewController *offerController = [storyboard instantiateViewControllerWithIdentifier:@"CouponViewController"];
            [self.navigationController presentViewController:offerController animated:true completion:nil];
        }
        else
        {
            OfferWebVC * objOfferWebVC= [storyboard instantiateViewControllerWithIdentifier:@"idOfferWebVC"];
            objOfferWebVC.webURLString = str;
            objOfferWebVC.dict = [self->contentArr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:objOfferWebVC animated:YES];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *errorAlert = [[UIAlertView alloc]
                                       initWithTitle:@"Myles" message:KAInternet delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];
        });
    }
}

#pragma mark Collection view layout things
// Layout: Set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(130, 110);
    return mElementSize;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

/**
 @brief Select city
 @dev Abhishek singh
 */

-(IBAction)selectCityClick:(id)sender
{
    if (maCityModel != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIStoryboard *storyboard = k_storyBoard(k_mainStoryBoard);
            selectCity *selectCityController = [storyboard instantiateViewControllerWithIdentifier:@"selectCity"];
            selectCityController.maCity = maCityModel;
            selectCityController.delegate = self;
            [self.navigationController presentViewController:selectCityController animated:true completion:nil];
        });

    }
    else
    {
        [self callApiForCityList:false];
    }
    

}
/**
 @brief selectcity delegate
 @dev Abhishek singh
 */
-(void)selectedCityWithCityM:(cityModel *)cityM updatedCityList: (NSMutableArray *) cityList
{
    maCityModel = cityList;
    if(cityM != nil){
        NSLog(@"Selected City %@-%@",cityM.cityname,cityM.cityId);
        maTitleDetail[0] = cityM.cityname;
        selectedCityId = [cityM.cityId integerValue];
        [[SubLocationData sharedInstance] deletSublocation];
        
        if ((cityM.cityId.intValue == 9) || (cityM.cityId.intValue == 2) || (cityM.cityId.intValue == 3) || (cityM.cityId.intValue == 11) ){
            _btnAge.hidden = false;
            [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
            
        }
        else{
            _btnAge.hidden = true;
            isConfirmAge = false;
            [_btnAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
            
        }
        //    if ([cityM.cityname isEqualToString:@"Delhi"] || [cityM.cityname isEqualToString:@"Noida/Ghaziabad"] || [cityM.cityname isEqualToString:@"Chandigarh"] || [cityM.cityname isEqualToString:@"Gurugram"]){
        //        _btnAge.hidden = false;
        //    }
        //    else{
        //        _btnAge.hidden = true;
        //    }
        [self updateScrollContentHeight];

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_tblHome reloadData];
        });
    }
    
    
}
/**
 @brief Select booking date
 @dev Abhishek singh
 */
-(void)ActionSheet :(BOOL)bDPTime
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"Click");
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n"
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    
        //if (startTimePicker== nil)
        startTimePicker = [[UIDatePicker alloc] init];
    
        [startTimePicker setDatePickerMode:UIDatePickerModeDateAndTime];
        if (@available(iOS 14.0, *)) {
            [startTimePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels]; //UIDatePickerStyleInline
        } else {
            // Fallback on earlier versions
        }
        startTimePicker.minuteInterval=30;
        //startTimePicker.datePickerStyle = UIDatePicker.
        startTimePicker.timeZone = [NSTimeZone localTimeZone];
    
        NSString *strDate ;
    
        if((!bDPTime && !isDateSet) || (bDPTime && !isDateSet))
        {
           strDate = [dateFormatter stringFromDate:[NSDate date]];
    
            NSTimeInterval secondsInEightHours;
            NSMutableArray *arr = [[strDate componentsSeparatedByString:@":"] mutableCopy];
            if ([arr[1] intValue] >= 30) {
                secondsInEightHours = 3 * 60 * 60;
                arr[1] = @"00";
            }else
            {
                secondsInEightHours = 2 * 60 * 60;
                arr[1] = @"30";
            }
            strDate = [arr componentsJoinedByString:@":"];
    
            [startTimePicker setMinimumDate: [[dateFormatter dateFromString:strDate] dateByAddingTimeInterval:secondsInEightHours]];
    
        }
        else
        {
            strDate = !bDPTime ?maTitleDetail[1]:maTitleDetail[2];
    
            if (!bDPTime) {
                NSInteger hours = floor([self intervalBetweenDate:[self stringtoNsDate:maTitleDetail[1]] andDate:[NSDate date]]/(60*60));
    
                if (hours>2) {
                    [startTimePicker setDate:[dateFormatter dateFromString:strDate] animated:YES];
    
                }
                else
                {
                    strDate = [dateFormatter stringFromDate:[NSDate date]];
    
                    NSTimeInterval secondsInEightHours;
                    NSMutableArray *arr = [[strDate componentsSeparatedByString:@":"] mutableCopy];
                    if ([arr[1] intValue] >= 30) {
                        secondsInEightHours = 3 * 60 * 60;
                        arr[1] = @"00";
                    }else
                    {
                        secondsInEightHours = 2 * 60 * 60;
                        arr[1] = @"30";
                    }
                    strDate = [arr componentsJoinedByString:@":"];
    
                    [startTimePicker setMinimumDate: [[dateFormatter dateFromString:strDate] dateByAddingTimeInterval:secondsInEightHours]];
                }
    
            }
            else
            {
                [startTimePicker setDate:[dateFormatter dateFromString:strDate] animated:YES];

            }
        }
    
    
        [alert.view addSubview:startTimePicker];
        [alert addAction:({
    
            UIAlertAction *selectAction = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                // Formatter
    
    
    
                if (!bDPTime) {
                    maTitleDetail[1] = [self IndianTimeConverter:startTimePicker.date];
                    isDateSet = true;
    
    
                }
                
                maTitleDetail[2] = [dateFormatter stringFromDate: !bDPTime ? [startTimePicker.date dateByAddingTimeInterval:(24*60*60)] : startTimePicker.date];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_tblHome reloadData];
                });
    
            }];
            selectAction;
        })];
    
        
        
    [alert addAction:({
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    NSLog(@"cancel");
                }];
                cancelAction;
            })];
 
    
    [self presentViewController:alert animated:YES completion:nil];
    });
}
/**
 @brief Convert nsdate to string
 @dev Abhishek singh
 */
-(NSString *)IndianTimeConverter : (NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm"];
    dateFormatter1.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [dateFormatter1 stringFromDate:startTimePicker.date];
    return strDate;
}
-(NSDate *)stringtoNsDate : (NSString *)strDate
{
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm"];
    dateFormatter2.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter2 setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *Date = [dateFormatter2 dateFromString:strDate];
    return Date;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HomeCell";
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//    if (cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblTitle.text = arrTitle[indexPath.row];
    cell.lblTitleDetail.text = maTitleDetail[indexPath.row];
    cell.imgIcon.image = [UIImage imageNamed:indexPath.row == 0 ? @"location_selected":@"CalenderImg"];

    NSLog(@"cell = %@",cell);
    return cell;
} 
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   /* CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        return 55;
       
        // iPhone Classic
    } else {*/
        return 70;
    //}
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 71.0;
//}
//-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//  //  static NSString *CellIdentifier = @"footer";
//    HomeCellFooter *cell = [[HomeCellFooter alloc]init];
//    
//   // HomeCellFooter *cell = [[HomeCellFooter alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//
//    if (cell == nil){
//        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
//    }
//    cell.btnConfirmAge.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    cell.btnConfirmAge.layer.borderWidth = .5;
//    cell.btnConfirmAge.layer.cornerRadius = 5;
//    [cell.btnConfirmAge addTarget:self action:@selector(ConfirmAge:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.btnConfirmAge setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];
////
//    [cell.btnConfirmAge layoutIfNeeded];
//    return cell;
//}

-( void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __lblErrorLadakh.hidden = true;

    switch (indexPath.row) {
        case 0:
            [self selectCityClick:self];
            break;
        case 1:
            [self ActionSheet:false];
            break;
        case 2:
            [self ActionSheet:true];
            break;
            
        default:
            break;
    }
}
-(IBAction)ConfirmAge:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    isConfirmAge = !isConfirmAge;
    
    [btn setImage:[UIImage imageNamed:!isConfirmAge? @"RectCheckBoxEmpty":@"RectCheckBoxFilled"] forState:UIControlStateNormal];

}


-(void)showAlertOnHome:(NSString *)titleStr withTitle:(NSString *)messageStr{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController *alertController = [UIAlertController
                                              
                                              alertControllerWithTitle:titleStr
                                              
                                              message:messageStr
                                              
                                              preferredStyle:UIAlertControllerStyleAlert];
        

        
        UIAlertAction *okAction = [UIAlertAction
                                   
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   
                                   style:UIAlertActionStyleDefault
                                   
                                   handler:^(UIAlertAction *action)
                                   
                                   {
                                       
                                       //NSLog(@"OK action");
                                       
                                       
                                   }];
        
        
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    });

}


-(IBAction)SearchCar:(id)sender
{
    __lblErrorLadakh.hidden = true;
    
    [FIRAnalytics logEventWithName:@"search"
                        parameters:nil];
    
    if (selectedCityId == 0 && !isDateSet)
    {
        [self showAlertOnHome:@"Alert" withTitle:@"Please fill all given fields"];
       //[CommonFunctions alertTitle:@"Alert" withMessage:@"Please fill all given fields"];
        return;
    }
   else if (selectedCityId == 0) {
       [self showAlertOnHome:@"Alert" withTitle:KCitySelect];

//        [CommonFunctions alertTitle:@"Alert" withMessage:KCitySelect];
       return;
    }
   else if (!isDateSet) {
       [self showAlertOnHome:@"Alert" withTitle:[maTitleDetail[1] isEqualToString:@"Select start time"] ? KStartDateSelect:KEndDateSelect];

       
        //[CommonFunctions alertTitle:@"Alert" withMessage:[maTitleDetail[1] isEqualToString:@"Select start time"] ? KStartDateSelect:KEndDateSelect];
        return;
    }
//   else if (!isConfirmAge) {
//       [self showAlertOnHome:@"Message" withTitle:@"Please confirm your age to proceed"];
//
//        //[CommonFunctions alertTitle:@"Message" withMessage:@"Please confirm your age to proceed"];
//        return;
//    }

   else
    {
//         NSTimeInterval timeInterval = [[self stringtoNsDate:maTitleDetail[1]] timeIntervalSinceDate:[NSDate date]];
        NSInteger hours = floor([self intervalBetweenDate:[NSDate date] andDate:[self stringtoNsDate:maTitleDetail[1]]]/(60*60));
        NSString *result1 = [NSString stringWithFormat:@"%ld", (long)hours]; // convert integer to string to display result

        timeToTripStart = result1;
        if (hours < 2)
        {
            [self showAlertOnHome:KMessage withTitle:@"You can book at least 2 hours before pickup time."];

         //[CommonFunctions alertTitle:KMessage withMessage:@"You can book at least 2 hours before pickup time."];
            
        }
       else if ([[self stringtoNsDate:maTitleDetail[1]] compare:[self stringtoNsDate:maTitleDetail[2]]] == NSOrderedDescending)
        {
            [self showAlertOnHome:KMessage withTitle:@"Please enter an appropriate pickup date & time."];

            //[CommonFunctions alertTitle:KMessage withMessage:@"Please enter an appropriate pickup date & time."];
    
        }
        else
        {
            
            NSInteger hours = floor([self intervalBetweenDate:[self stringtoNsDate:maTitleDetail[1]] andDate:[self stringtoNsDate:maTitleDetail[2]]]/(60*60));
            NSString *result2 = [NSString stringWithFormat:@"%ld", (long)hours]; // convert integer to string to display result
            tripDuration = result2;


            if (hours < 4) {
                 [self showAlertOnHome:KMessage withTitle:@"Booking duration cannot be less than 4 hrs"];
                //[CommonFunctions alertTitle:KMessage withMessage:@"Booking duration cannot be less than 4 hrs"];

            }
    
            else
            {
                if (isConfirmAge && hours <24){
                    __lblErrorLadakh.hidden = false;
                    __lblErrorLadakh.text = @"Ladakh Booking cannot be less than 24 hour.";
                    NSLog(@"Ladakh Booking cannot be less than 24 hour.");
                }
                else if (hours >= 24){
                    
                    [self BookNowAction:isConfirmAge];
                }
//                if (([maTitleDetail[0] isEqualToString:@"Delhi"] && hours >= 24)|| ([maTitleDetail[0] isEqualToString:@"Chandigarh"] && hours >= 24) || ([maTitleDetail[0] isEqualToString:@"Gurugram"] && hours >= 24) || ([maTitleDetail[0] isEqualToString:@"Noida/Ghaziabad"] && hours >= 24))
//                {
//                    LadakhbookingViewController *controller;
//                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
//                    controller = [storyboard instantiateViewControllerWithIdentifier:@"LadakhbookingViewController"];
//                    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//                    controller.delegate     = self;
//                    [self presentViewController:controller animated:YES completion:nil];
//                }
                else
                {
                   /* [self checkLocationActive:^(DataCompletionResults completion) {
                        NSLog(@"completion = %d",completion);

                        if (completion == COMPLETION_SUCCESS) {*/
                            [self searchCar];
                        /*} else if (completion == COMPLETION_FAILURE) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertController *alertController = [UIAlertController
                                                                      
                                                                      alertControllerWithTitle:@"Message"
                                                                      
                                                                      message:@"The location is closed during the time you have selected. Please try again with different timings. Thank you."
                                                                      
                                                                      preferredStyle:UIAlertControllerStyleAlert];
                                
                                
                                
                                
                                
                                UIAlertAction *okAction = [UIAlertAction
                                                           
                                                           actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                           
                                                           style:UIAlertActionStyleDefault
                                                           
                                                           handler:^(UIAlertAction *action)
                                                           
                                                           {
                                                               
                                                               //NSLog(@"OK action");
                                                               
                                                               [self.navigationController popViewControllerAnimated:YES];
                                                               
                                                           }];
                                
                                
                                
                                [alertController addAction:okAction];
                                
                                [self presentViewController:alertController animated:YES completion:nil];
                                
                                return ;
                            });
                            
                        } else if (completion == COMPLETION_SESSION_EXPIRED) {
                            __block HomeViewController *homeSelf = self;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunctions logoutcommonFunction];
                                AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                                [appDelegate getAccessTokenWithCompletion:^{
                                    [homeSelf SearchCar:sender];
                                }];
                                
                            });
                            
                        } else if (completion == COMPLETION_NODATA) {
                            
                        }
                    }];*/
                }

            }
            
        }
        
      
        
    }
    
}
//Method to check whether swrevealcontroller is where
//if it is visible then no action on self
//and we will toggle the swrevealcontroller
//-(void)checkRevealController:(void (^)(BOOL finished))completion {
//    //do stuff
//    if (completion) {
//        if (self.revealViewController.frontViewPosition == FrontViewPositionRight) {
//            [self.revealViewController revealToggleAnimated:YES];
//            completion(FALSE);
//        } else {
//            completion(TRUE);
//        }
//    }
//    
//    
//}
//ss


-(void)searchCar
{
    //[self checkRevealController:^(BOOL finished) {
        //if (finished) {
            NSLog(@"position = %ld",(long)self.revealViewController.frontViewPosition);
    __block HomeViewController *homeSelf = self;
            if ([CommonFunctions reachabiltyCheck]) {
                dispatch_async(dispatch_get_main_queue(), ^{

                [self showLoader];
                });
                #ifdef DEBUG
             previousDateTime = [CommonFunctions getCurrentDate];
             previousMilliSec = [CommonFunctions getMilliSecond];
                #endif
                NSDictionary *localDictToSend = [self postDataOnApi];
                ApiClass *callApi = [[ApiClass alloc]initWithDict:localDictToSend];
                searchMobel = [[SearchCarModel alloc]initWithDict:localDictToSend];
                [callApi SearchCarApiWithCompletionHandler:^(MylesCarDetailsResponse * _Nullable obj, BOOL isValidToken) {
                    
                    
                    if (isValidToken) {
                        if (obj!=nil) {
                            
                            
                           dispatch_async(dispatch_get_main_queue(), ^{
                            #ifdef DEBUG
                               currentDateTime = [CommonFunctions getCurrentDate];
                               currentMilliSec = [CommonFunctions getMilliSecond];
                               NSString *milliSec = [CommonFunctions finalMilliSecond:previousMilliSec current:currentMilliSec];
                               NSString *dataToAdd = [CommonFunctions createStrigForSavingData:@"Home_page" key:@"Search_result_page" startTime:previousDateTime endTime:(NSString *)currentDateTime milliSec:milliSec];
                               NSLog(@"final data to be added in txt file ==== %@",dataToAdd);
                            #endif
                               [self performSegueWithIdentifier:@"selectCar" sender:obj.response];
                           });

                            
                        } else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [SVProgressHUD dismiss];
                            });
                        }
                        
                    } else {
                       
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                            [CommonFunctions logoutcommonFunction];
                            AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                            [appDelegate getAccessTokenWithCompletion:^{
                                //[homeSelf searchCar];
                            }];

                        });
                    }
                }];
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });

                [CommonFunctions alertTitle:@"Myles" withMessage:KAInternet];
            }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
       // [SVProgressHUD dismiss];
    });
    //dispatch_async(dispatch_get_main_queue(), ^{
    CarListViewController *carlistController = (CarListViewController *) [segue destinationViewController];
    NSLog(@"matitleDetail1 = %@",maTitleDetail[1]);
    NSLog(@"matitleDetail2 = %@",maTitleDetail[2]);
        
    carlistController.searchCarModel = searchMobel;
    carlistController.pickUpDateStr = searchMobel.fromDate;
    carlistController.pickUpTime =searchMobel.pickUpTime ;
    carlistController.DropOffDateStr =searchMobel.toDate;
    carlistController.dropOffTime =searchMobel.dropOffTime;
    
    #ifdef DEBUG
    //carlistController.previousMilliSec  = previousMilliSec;
    //carlistController.previousDateTime = previousDateTime;
    #endif
    
    SearchCarDetails *searchCarDetailsObj = (SearchCarDetails *)sender;
    carlistController.carDetailInfoMA = (NSMutableArray *)searchCarDetailsObj.CarSearchDetailNormal;
    if (searchCarDetailsObj.CarSearchDetailNextAvailable.count > 0)
    {
       carlistController.nextCarDetailInfoMA = (NSMutableArray *) searchCarDetailsObj.CarSearchDetailNextAvailable;
    }
    
    if (searchCarDetailsObj.GSTDetails.count > 0) {
        carlistController.carGSTPackageDetails = (NSMutableArray *)searchCarDetailsObj.GSTDetails;
        NSLog(@"GSTDetails: %@ ", carlistController.carGSTPackageDetails);
    }
    
    carlistController.sublocationTitle = maTitleDetail[0];
    AppDelegate *appDelegate = [AppDelegate getSharedInstance];
    appDelegate.Useraddress = maTitleDetail[0];
    carlistController.selectedCityId = selectedCityId;
    carlistController.sitePickUpDataDict = [self postDataOnApi];
    [DEFAULTS setValue:maTitleDetail[0] forKey:K_LastLocation];
    carlistController.cityModelArray = maCityModel.copy;
    carlistController.tripDurationSelected = tripDuration;
    carlistController.timeToTripStart = timeToTripStart;
    //});
}

-(NSInteger) hoursBetweenDays:(NSString *)date1 second:(NSString *)date2 {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss a"];
    [formatter setDateFormat:@"YYY-MM-dd HH:mm"];
    NSDate *startDate = [formatter dateFromString:date1];
    NSDate *endDate = [formatter dateFromString:date2];
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    return hoursBetweenDates;
}

-(NSMutableDictionary *)postDataOnApi1
{
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    NSArray *arrPickUp = [maTitleDetail[1] componentsSeparatedByString:@" "];
    NSArray *arrDropUp = [maTitleDetail[2] componentsSeparatedByString:@" "];
   
    [body setObject:[arrPickUp[1] stringByReplacingOccurrencesOfString:@":" withString:@""] forKey:@"TimeFrom"];
    [body setObject:[arrDropUp[1] stringByReplacingOccurrencesOfString:@":" withString:@""]  forKey:@"TimeTo"];
    [body setObject:[NSString stringWithFormat:@"%ld",(long)selectedCityId] forKey:@"CityID"];
    [body setObject:@"0" forKey:@"SublocationID"];

    return body;
}



// function for location closed
-(void)checkLocationActive:(void (^)(DataCompletionResults))completion {
    if ([CommonFunctions reachabiltyCheck]) {
        NSLog(@"postDictionary = %@",[self postDataOnApi1]);
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        [self showLoader];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:[self postDataOnApi1] session:session url:@"checksublocationtimimg" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [SVProgressHUD dismiss];
                                                   });

                                                   if (data != nil) {
                                                       NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                       NSString *statusStr = [headers objectForKey:@"Status"];
                                                       if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                       {
                                                           if (!error)
                                                           {
                                                               NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                               NSLog(@"RES : %@",responseJson);
                                                               if ([[responseJson objectForKey:@"status"] integerValue]){
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [DEFAULTS setInteger:[[responseJson objectForKey:k_HomePickupAvailable] integerValue] forKey:k_HomePickupAvailable];
                                                                       [DEFAULTS setInteger:[[responseJson objectForKey:k_HomePickupCharge] integerValue] forKey:k_HomePickupCharge];
                                                                   });
                                                                   completion(COMPLETION_SUCCESS);
                                                               } else {
                                                                   completion(COMPLETION_FAILURE);
                                                               }
                                                               
                                                           }
                                                       }else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"]))
                                                           {
                                                               completion(COMPLETION_SESSION_EXPIRED);
                                                               
                                                           }
                                                           
                                                   } else {
                                                       completion(COMPLETION_NODATA);
                                                   }
                                                   
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        completion(COMPLETION_NODATA);
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}



// Ladakh Protocolsss
- (void)BookNowAction:(BOOL) LadakhStatus
{
    kLadakhStatus = LadakhStatus;
    //[self searchCar];
    /*[self checkLocationActive:^(DataCompletionResults completion) {
        NSLog(@"completion = %d",completion);
        
        if (completion == COMPLETION_SUCCESS) {*/
            [self searchCar];
        /*} else if (completion == COMPLETION_FAILURE) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController
                                                      
                                                      alertControllerWithTitle:@"Message"
                                                      
                                                      message:@"The location is closed during the time you have selected. Please try again with different timings. Thank you."
                                                      
                                                      preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                
                
                UIAlertAction *okAction = [UIAlertAction
                                           
                                           actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                           
                                           style:UIAlertActionStyleDefault
                                           
                                           handler:^(UIAlertAction *action)
                                           
                                           {
                                               
                                               //NSLog(@"OK action");
                                               
                                               [self.navigationController popViewControllerAnimated:YES];
                                           }];
         
                [alertController addAction:okAction];
                
                [self presentViewController:alertController animated:YES completion:nil];
                
                return ;
            });
            
        } else if (completion == COMPLETION_SESSION_EXPIRED) {
//            __block HomeViewController *homeSelf = self;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [CommonFunctions logoutcommonFunction];
////                AppDelegate *appDelegate = [AppDelegate getSharedInstance];
////                [appDelegate getAccessTokenWithCompletion:^{
////                    [homeSelf SearchCar:sender];
////                }];
//                
//            });
            
        } else if (completion == COMPLETION_NODATA) {
            
        }
    }];*/

    
}
- (NSTimeInterval)intervalBetweenDate:(NSDate *)dt1 andDate:(NSDate *)dt2 {
    
    NSTimeInterval interval = [dt2 timeIntervalSinceDate:dt1];
    NSLog(@"%f",interval);
    return interval;
}

-(NSDictionary *)postDataOnApi
{
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    NSInteger noofHours = [self hoursBetween:[self stringtoNsDate:maTitleDetail[1]] and:[self stringtoNsDate:maTitleDetail[2]]];
    
    NSArray *arrPickUp = [maTitleDetail[1] componentsSeparatedByString:@" "];
    NSArray *arrDropUp = [maTitleDetail[2] componentsSeparatedByString:@" "];
        
    [body setObject:[NSString stringWithFormat:@"%ld",(long)selectedCityId] forKey:@"CityID"];
    
    finalSelectedDate = [self stringtoNsDate:maTitleDetail[1]];
    [body setObject:arrPickUp[0] forKey:@"FromDate"];
    [body setObject:arrDropUp[0] forKey:@"ToDate"];
    [body setObject:[arrPickUp[1] stringByReplacingOccurrencesOfString:@":" withString:@""] forKey:@"PickUpTime"];
    [body setObject:[arrDropUp[1] stringByReplacingOccurrencesOfString:@":" withString:@""]  forKey:@"DropOffTime"];
    [body setObject:@"0" forKey:@"SubLocations"];
    
    [body setObject:[NSString stringWithFormat:@"%ld",(long)noofHours] forKey:@"Duration"];
    
    if (kLadakhStatus == YES) {
        [body setObject:@"1" forKey:@"CustomPkgYN"];
    }
    else {
        [body setObject:@"0" forKey:@"CustomPkgYN"];
    }
    
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if (appVersionString != nil)
    {
        [body setObject:appVersionString forKey:@"userAppVersion"];
    }
    else
    {
        [body setObject:@"" forKey:@"userAppVersion"];
    }
    return body;
}

- (void)dealloc {
    _tblHome.dataSource = nil;
    _tblHome.delegate = nil;
}

#pragma mark - Page View Controller Data Source
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    [FIRAnalytics logEventWithName:[NSString stringWithFormat:@"Carousel_Before"]
                        parameters:nil];

    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;

    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }

    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    [FIRAnalytics logEventWithName:[NSString stringWithFormat:@"Carousel_Before"]
                        parameters:nil];

    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self->imagessssArray count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self->imagessssArray count] == 0) || (index >= [self->imagessssArray count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"idPageContentVCCC"];
    pageContentViewController.imageFile = [self->imagessssArray objectAtIndex:index];
    //pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    pageContentViewController.arrrrr = contentArr;
    return pageContentViewController;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return imagessssArray.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}



-(void)callAPIForPagination{
    
    if ([CommonFunctions reachabiltyCheck])
    {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:(id)self delegateQueue:nil];
        
        #ifdef DEBUG
        NSURL *url = [NSURL URLWithString:@"https://qa1.mylescars.com/Rests/getSliderDetalis"];
        //NSURL *url = [NSURL URLWithString:@"https://qa2.mylescars.com/Rests/getSliderDetalis"];
        
        #else
        NSURL *url = [NSURL URLWithString:@"https://www.mylescars.com/Rests/getSliderDetalis"];
        
        #endif
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"GET"];
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (response == nil)
            {
                return;
            } // Swati Added These Line Please Remove while uploading App
            
            NSDictionary *jsonObject=[NSJSONSerialization
                                      JSONObjectWithData:data
                                      options:NSJSONReadingMutableLeaves
                                      error:nil];
            
            NSLog(@"jsonObject is %@",jsonObject);
            self->contentArr = [jsonObject valueForKey:@"ResStatus"];
            
            if (self->contentArr.count>0)
            {
                for (int i = 0; i<self->contentArr.count; i++)
                {
#ifdef DEBUG
//                    NSString * str = [NSString stringWithFormat:@"https://qa2.mylescars.com/slider/%@",[[self->contentArr objectAtIndex:i] valueForKey:@"images"]];
                    
                    NSString * str = [NSString stringWithFormat:@"https://qa1.mylescars.com/slider/%@",[[self->contentArr objectAtIndex:i] valueForKey:@"images"]];
#else
                    NSString * str = [NSString stringWithFormat:@"https://www.mylescars.com/slider/%@",[[self->contentArr objectAtIndex:i] valueForKey:@"images"]];
#endif
                    [self->imagessssArray addObject:str];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
                    flow.itemSize = CGSizeMake(130, 110);
                    flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
                    flow.minimumInteritemSpacing = 0;
                    flow.minimumLineSpacing = 0;
                    self.offerCollectionView.delegate = self;
                    self.offerCollectionView.dataSource = self;
                });
                
                /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
                
                // Create page view controller
                self.pageViewController = [storyboard instantiateViewControllerWithIdentifier:@"idPageViewControllerrr"];
                self.pageViewController.dataSource = (id)self;
                
                PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
                NSArray *viewControllers = @[startingViewController];
                [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
                
                // Change the size of page view controller
                self.pageViewController.view.frame = CGRectMake(saleImageview.frame.origin.x, saleImageview.frame.origin.y, saleImageview.frame.size.width, saleImageview.frame.size.height);
                
                [self addChildViewController:self->_pageViewController];
                [self.offerView addSubview:self->_pageViewController.view];
                [self.pageViewController didMoveToParentViewController:self];*/
                
                NSLog(@"Images Url: %@",self->imagessssArray);                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.offerView.hidden = true;
                    [self updateScrollContentHeight];
                });
            }
            
            
            //https://qa1.mylescars.com/slider/358344_promo_banner_3.jpg
        }];
        
        [postDataTask resume];
    }
    else
    {

    }
    
}



@end

