//
//  PackegeFilterViewController.m
//  Myles
//
//  Created by IT Team on 29/02/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "PackegeFilterViewController.h"


@interface PackegeFilterViewController ()

@end

@implementation PackegeFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dailyAction:(UIButton *)sender {
    
    if ([sender isSelected]) {
        
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    
    [self.delegate getFilterOption:sender.tag];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)weeklyAction:(UIButton *)sender {
    
    if ([sender isSelected]) {
        
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    
    [self.delegate getFilterOption:sender.tag];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)monthlyAction:(UIButton *)sender {
    
    if ([sender isSelected]) {
        
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    
    [self.delegate getFilterOption:sender.tag];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)hourlyAction:(UIButton *)sender {
    
    if ([sender isSelected]) {
        
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    
    [self.delegate getFilterOption:sender.tag];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)closeAction:(UIButton *)sender {
    
    if ([sender isSelected]) {
        
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    
    [self.delegate getFilterOption:sender.tag];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
