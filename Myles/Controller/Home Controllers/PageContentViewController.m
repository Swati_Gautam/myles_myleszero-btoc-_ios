//
//  PageContentViewController.m
//  Myles
//
//  Created by Chetan Rajauria on 18/02/19.
//  Copyright © 2019 Myles. All rights reserved.
//

#import "PageContentViewController.h"
#import "UIImageView+WebCache.h"
#import "OfferWebVC.h"
#import "Haneke.h"



@import FirebaseAnalytics;


@class CouponViewController;
@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentImageView hnk_setImageFromURL:[NSURL URLWithString:self.imageFile] placeholder: [UIImage imageNamed:@"placeholder_scroll"]];
    
    //[self.contentImageView hnk_setImageFromURL:[NSURL URLWithString:self.imageFile] placeholder:[UIImage imageNamed:@"placeholder_scroll"]]

    /*[self.contentImageView hnk_setImageFromURL:[NSURL URLWithString:self.imageFile] placeholder:[UIImage imageNamed:@"placeholder_scroll"] success:^(UIImage *image) {
        
        [self.contentImageView setImage:image];


    } failure:^(NSError *error) {
        NSLog(@"Error = %@",[error description]);
    }];*/
    
    
    [self.contentImageView setUserInteractionEnabled:YES];
    [self.contentImageView setTag:self.pageIndex];
    [self addGestureOnImageView:self.contentImageView];
}

-(void)addGestureOnImageView:(UIImageView *)imageView
{
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    [tapGesture setNumberOfTapsRequired:1];
    [imageView addGestureRecognizer:tapGesture];
}

-(void)tapGestureAction:(UITapGestureRecognizer *)gesture
{
    NSLog(@"Gesture :%ld",gesture.view.tag);

    [FIRAnalytics logEventWithName:@"Carousel_Clicked"
                        parameters:nil];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];

    NSString * str = [[self.arrrrr objectAtIndex:self.pageIndex] valueForKey:@"url"];
    
    if ([CommonFunctions reachabiltyCheck]) {
        
        if ([str containsString:@"discount-offers"])
        {            
            UIStoryboard *storyboard = k_storyBoard(k_mainStoryBoard);
            CouponViewController *offerController = [storyboard instantiateViewControllerWithIdentifier:@"CouponViewController"];
            [self.navigationController presentViewController:offerController animated:true completion:nil];
        }
        else
        {
            OfferWebVC * objOfferWebVC= [storyboard instantiateViewControllerWithIdentifier:@"idOfferWebVC"];
            objOfferWebVC.webURLString = str;
            objOfferWebVC.dict = [self.arrrrr objectAtIndex:self.pageIndex];
            [self.navigationController pushViewController:objOfferWebVC animated:YES];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *errorAlert = [[UIAlertView alloc]
                                       initWithTitle:@"Myles" message:KAInternet delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];
        });
    }
    
    

   
}

@end
