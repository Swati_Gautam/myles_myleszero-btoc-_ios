//
//  PageContentViewController.h
//  Myles
//
//  Created by Chetan Rajauria on 18/02/19.
//  Copyright © 2019 Myles. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PageContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;
@property NSArray * arrrrr;


@end

NS_ASSUME_NONNULL_END
