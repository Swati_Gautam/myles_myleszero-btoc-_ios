//
//  SublocationListVC.swift
//  Myles
//
//  Created by Chetan Rajauria on 31/01/19.
//  Copyright © 2019 Myles. All rights reserved.
//

import UIKit

protocol SublocationListViewControllerDelegate: NSObjectProtocol {
    func selectedSublocationData(_ SublocationID: String?, sublocationTitle title: String?, sublocationindex: IndexPath?, isSublocationSelected issublocationSelected: Bool, strModelId: String?)
}

var isSublocation = false
var selectedSublocation = ""
var selectedSublocationID = ""
var selectedSublocationindex: IndexPath?
var issublocationSelected = false

var sublocationList = NSArray()
var sublocationData = NSDictionary()
var selSublocationindex: IndexPath?
var strSelectedData = ""
var strModelId = ""
var currentSelectedLocation = ""
var nnnnnnnnn = String?.self
weak var tableView: UITableView!

weak var delegate: SublocationListViewControllerDelegate?

class SublocationListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        print(String(format: "sublocationList count =%lu", UInt(sublocationList.count)))
        print("subLocationListController.sublocationData =\(sublocationData)")
        print("selectedSublocationindex = \(String(describing: selSublocationindex))")

        #if DEBUG
        #else
        GAI.sharedInstance().tracker(withTrackingId: "UA-64851985-4")
        let tracker: GAITracker? = GAI.sharedInstance()?.defaultTracker
        tracker?.set(kGAIScreenName, value: "Select Sublocation")
        tracker?.send(GAIDictionaryBuilder.createScreenView().build() as! [AnyHashable : Any])
        tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: "Select Sublocation", action: "Click", label: "Sublocation", value: NSNumber(value: 1)).build() as! [AnyHashable : Any])
        #endif
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sublocationList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell:SublocationCell = tableView.dequeueReusableCell(withIdentifier: "SublocationCell") as! SublocationCell

        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(didTapBringCheckBoxBtn), for: .touchUpInside)

        if !(strSelectedData == "") && (strSelectedData == sublocationList[indexPath.row] as? String) {
            isSublocation = true
            selectedSublocation = sublocationList[indexPath.row] as! String
            selectedSublocationID = sublocationData[strSelectedData] as! String
            issublocationSelected = true
            cell.setvaluesofObject(false, locationName: sublocationList[indexPath.row] as? String)
        }
        else
        {
            cell.setvaluesofObject(true, locationName: sublocationList[indexPath.row] as? String)
        }
        cell.sublocationName.numberOfLines = 2
        cell.sublocationName.layoutIfNeeded()
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell
    }

    @objc func didTapBringCheckBoxBtn(_ sender: Any?) {

        let button = sender as? UIButton
        button?.isSelected = !(button?.isSelected ?? false)
        print(String(format: "button.tag =%ld", Int(button?.tag ?? 0)))

        let indexPath = IndexPath(row: Int(button?.tag ?? 0), section: 0)
        print("user de-selected \(sublocationList[Int(button?.tag ?? 0)])")

        if button?.imageView?.image?.isEqual(UIImage(named: "blankBoxIcon")) ?? false {
            button?.setImage(UIImage(named: "checkBoxIcon"), for: .normal)
            isSublocation = true
            selectedSublocation = sublocationList.object(at: (button?.tag)!) as! String
            selectedSublocationID = sublocationData[selectedSublocation] as! String
            issublocationSelected = true
        }
        
        delegate?.selectedSublocationData(selectedSublocationID, sublocationTitle: selectedSublocation, sublocationindex: selSublocationindex, isSublocationSelected: issublocationSelected, strModelId: strModelId)
        
        navigationController?.popViewController(animated: true)

    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SublocationCell
        cell?.accessoryType = UITableViewCell.AccessoryType.none
        
        print("IndexPath: \(indexPath.description)")
        
        print("user de-selected \(sublocationList[indexPath.row])")
        isSublocation = true
        selectedSublocation = sublocationList[indexPath.row ?? 0] as! String
        selectedSublocationID = sublocationData[selectedSublocation] as! String
        issublocationSelected = true
        delegate!.selectedSublocationData(selectedSublocationID, sublocationTitle: selectedSublocation, sublocationindex: selSublocationindex, isSublocationSelected: issublocationSelected, strModelId: strModelId)
        self.navigationController?.popViewController(animated: true)
    }
}
