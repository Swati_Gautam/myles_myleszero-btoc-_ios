//
//  FlashSaleController.swift
//  Myles
//
//  Created by iOS Team on 18/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import Foundation
import UIKit
import Haneke

class FlashSaleController: UIViewController {
    
    var infoDictionary: [String: String]?
    var myInt = 10

    @IBOutlet weak var saleImageview: UIImageView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(FlashSaleController.handleTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.saleImageview.isUserInteractionEnabled = true
        self.saleImageview.addGestureRecognizer(tapGesture)
        //let str = String(validatingUTF8: "\u{2715}".cString(using: String.Encoding.utf8)!)
        
        self.cancelButton.setTitle("\u{2715}", for: UIControl.State())
        
        self.loadImage()
       
        
    }
    
//    func showLoader () {
//        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
//        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
//        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
//        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12))
//        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
//        SVProgressHUD.show(withStatus: "Please wait")
//    }

    
    
    func loadImage() {
        let urlPath = self.infoDictionary!["BannerImageURL"]
        let url =   URL(string: urlPath!)!
       
        self.saleImageview.hnk_setImage(from: url)

//        DispatchQueue.main.async(execute: {
//            let data = try? Data(contentsOf: url)
//            if let dataTemp = data
//            {
//                let image = UIImage(data: dataTemp)!
//                self.saleImageview.image = image
//            }
//           
//         })

        /*
          let qosClass = QOS_CLASS_BACKGROUND
          let backgroundQueue = dispatch_get_global_queue(qosClass, 0)
           dispatch_async(backgroundQueue, {
            print("Work on background queue")
            let data = NSData(contentsOfURL: url)
            let image = UIImage(data: data!)!
            self.saleImageview.image = image
          })
          */
    }

    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let redirectURLFlag = self.infoDictionary!["redirectURLFlag"]
        if redirectURLFlag == "true" {
            let RedirectURL = self.infoDictionary!["RedirectURL"]
            self.dismiss(animated: true) {
                DispatchQueue.main.async {
                    
                    let mainStoryboard = UIStoryboard.init(name: "MainStoryboard", bundle: nil)
                    let offerController = mainStoryboard.instantiateViewController(withIdentifier: "OfferViewController") as! FlashOfferViewController
                    offerController.urlToNavigate = URL(string: RedirectURL!)
                    offerController.titleStr = ""
                    AppDelegate.getSharedInstance().window.rootViewController?.present(offerController, animated: true, completion: nil)
                }
               // UIApplication.shared.openURL(URL(string: RedirectURL!)!)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    

    @IBAction func cancelBtnClicked(_ sender: AnyObject) {
        self.dismiss(animated: true) {
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
