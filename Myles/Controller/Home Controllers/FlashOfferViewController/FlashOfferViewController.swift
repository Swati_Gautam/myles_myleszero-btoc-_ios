//
//  FlashOfferViewController.swift
//  Myles
//
//  Created by ITTeam on 23/12/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD
import WebKit

@objc class FlashOfferViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var progressBar: UIProgressView!
    //@IBOutlet weak var offerWebview: UIWebView!
    @objc var webViewOffers: WKWebView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var CancelBtn: UIBarButtonItem!
    @objc  var urlToNavigate : URL!
    @objc var titleStr : String!
    @objc var myTimer:Timer!
    @objc var isComplete = Bool()
    
    @IBAction func cancleBtnAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
        }
    }
    
    func showLoader () {
        /*SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")*/
        if(myTimer != nil){
            myTimer.invalidate()
        }
        progressBar.progress = 0.0
        isComplete = false
        progressBar.isHidden = false
        myTimer =  Timer.scheduledTimer(timeInterval: 0.01667,target: self,selector: #selector(FlashOfferViewController.timerCallback),userInfo: nil,repeats: true)
    }
    @objc func timerCallback(){
        if isComplete {
            if progressBar.progress >= 1 {
                progressBar.isHidden = true
                myTimer.invalidate()
            }else{
                progressBar.progress += 0.1
            }
        }else{
            progressBar.isHidden = false
            progressBar.progress += 0.05
            if progressBar.progress >= 0.95 {
                progressBar.progress = 0.95
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
         webViewOffers = WKWebView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 20, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(webViewOffers)
        if(CommonFunctions.reachabiltyCheck()){
            showLoader()
            //"https://www.mylescars.com/marketings/myles_offers_app"
            // Do any additional setup after loading the view.
            let request = NSMutableURLRequest(url: urlToNavigate,cachePolicy: .reloadIgnoringLocalCacheData,
                                              timeoutInterval: 30.0)
            
            webViewOffers.load(request as URLRequest)
            
            
            //self.offerWebview.loadRequest(request as URLRequest)
        } else {
            CommonFunctions.alertTitle("MYLES", withMessage:"No Network available. Please connect and try again.")
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        if(myTimer != nil){
            myTimer.invalidate()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.topItem?.title = titleStr

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        DispatchQueue.main.async(execute: {
            self.isComplete = true
            
            if (webView.isLoading){
                return
            }
            SVProgressHUD.dismiss()
        })
    }
    
    /*func webViewDidFinishLoad(_ webView: UIWebView)
    {
        isComplete = true
        //http://stackoverflow.com/questions/5995210/disabling-user-selection-in-uiwebview
        if (webView.isLoading){
            return
        }
        SVProgressHUD.dismiss()
    }*/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
