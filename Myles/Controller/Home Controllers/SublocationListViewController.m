//
//  SublocationListViewController.m
//  Myles
//
//  Created by IT Team on 17/08/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "SublocationListViewController.h"
#import "SublocationCell.h"
#import <Google/Analytics.h>

@interface SublocationListViewController ()

@end

@implementation SublocationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"sublocationList count =%lu",(unsigned long)[_sublocationList count]);
    NSLog(@"subLocationListController.sublocationData =%@",_sublocationData);
    NSLog(@"selectedSublocationindex = %@",_selSublocationindex);
    //[self customizeNavigationBar:@"Select Sublocation" Present:false];

#ifdef DEBUG
#else
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Select Sublocation"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Select Sublocation"
                                                          action:@"Click"
                                                           label:@"Sublocation"
                                                           value:@1] build]];
#endif
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_sublocationList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"SublocationCell";
    SublocationCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if ( cell == nil )
    {
        cell = [[SublocationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.btnCheck.tag = indexPath.row;
    [cell.btnCheck addTarget:self action:@selector(didTapBringCheckBoxBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    if (![_strSelectedData  isEqual: @""] && [_strSelectedData isEqualToString:[_sublocationList objectAtIndex:indexPath.row]])
    {
       // [cell.btnCheck
        // setImage:[UIImage imageNamed:@"checkBoxIcon"] forState:UIControlStateNormal];
        isSublocation = YES;
        selectedSublocation = [_sublocationList objectAtIndex:indexPath.row];
        selectedSublocationID = [_sublocationData valueForKey:_strSelectedData];
        issublocationSelected = YES;
        [cell setvaluesofObject:NO LocationName:[_sublocationList objectAtIndex:indexPath.row]];

    }
    else
    {
        [cell setvaluesofObject:YES LocationName:[_sublocationList objectAtIndex:indexPath.row]];

    }
    cell.sublocationName.numberOfLines = 2;
    [cell.sublocationName layoutIfNeeded];
//    cell.sublocationName.numberOfLines = 0;
//    [cell.sublocationName sizeToFit];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
     return cell;
}

/*
 NSString *selectedSublocation;
 NSString *selectedSublocationID;
 */

- (void)didTapBringCheckBoxBtn:(id)sender {

    //[self.tableView reloadData];
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    NSLog(@"button.tag =%ld",(long)button.tag);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(int)button.tag inSection:0];
    NSLog(@"user de-selected %@",[_sublocationList objectAtIndex:(long)button.tag]);
    
   
    if ([button.imageView.image isEqual:[UIImage imageNamed:@"blankBoxIcon"]]) {
        [button setImage:[UIImage imageNamed:@"checkBoxIcon"] forState:UIControlStateNormal];
        isSublocation = YES;
        selectedSublocation = [_sublocationList objectAtIndex:(int)button.tag];
        selectedSublocationID = [_sublocationData valueForKey:selectedSublocation];
        issublocationSelected = YES;
    }
    
    [_delegate selectedSublocationData:selectedSublocationID SublocationTitle:selectedSublocation  Sublocationindex:_selSublocationindex isSublocationSelected:issublocationSelected :_strModelId];
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)path {
    
    SublocationCell *cell = [tableView cellForRowAtIndexPath:path];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    NSLog(@"IndexPath: %@", [path description]);
    
    NSIndexPath *indexPath = path;
    NSLog(@"user de-selected %@",[_sublocationList objectAtIndex:path.row]);
    
    
//    if ([button.imageView.image isEqual:[UIImage imageNamed:@"blankBoxIcon"]]) {
//        [button setImage:[UIImage imageNamed:@"checkBoxIcon"] forState:UIControlStateNormal];
        isSublocation = YES;
        selectedSublocation = [_sublocationList objectAtIndex:indexPath.row];
        selectedSublocationID = [_sublocationData valueForKey:selectedSublocation];
        issublocationSelected = YES;
//    }
    
    [_delegate selectedSublocationData:selectedSublocationID SublocationTitle:selectedSublocation  Sublocationindex:_selSublocationindex isSublocationSelected:issublocationSelected :_strModelId];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
