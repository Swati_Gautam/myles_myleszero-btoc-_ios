//
//  TermsAndConditionsVC.swift
//  Myles
//
//  Created by Chetan Rajauria on 13/02/19.
//  Copyright © 2019 Myles. All rights reserved.
//

import UIKit
import WebKit

class TermsAndConditionsVC: BaseViewController, WKNavigationDelegate {

    //, UIWebViewDelegate
    //@IBOutlet var webViewTermAndCondition: UIWebView!
    @IBOutlet var wkWebViewTerms: WKWebView!
    var type: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.title = "Terms & Conditions"
    }
    
    func showLoader(){
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        DispatchQueue.main.async(execute: {
            self.showLoader()
        })
    }*/
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!)
    {
        debugPrint("didCommit")
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
    /*func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("didFinish")
    }*/
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        debugPrint("didFail")
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
    
    func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //[self showLoader];
        GAI.sharedInstance().tracker(withTrackingId: "UA-64851985-4")
        let tracker: GAITracker? = GAI.sharedInstance()?.defaultTracker
        tracker?.set(kGAIScreenName, value: "Terms And Conditions")
        tracker?.send((GAIDictionaryBuilder.createScreenView().build() as! [AnyHashable : Any]))
        super.viewWillAppear(animated)
        wkWebViewTerms = WKWebView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 20, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        viewSetUp()
    }
    
    func viewSetUp()
    {
        // init and load request in webview.
        wkWebViewTerms.navigationDelegate = self
        if type == 1 {
            self.bookingTAndC()
        }
        else
        {
            //let myBlog = "https://iosdevcenters.blogspot.com/"
            //let url = NSURL(string: myBlog)
            //let request = NSURLRequest(URL: url!)
            let url = URL(string: "https://www.mylescars.com/pages/terms_of_use_app")
            var request: URLRequest? = nil
            if let url = url {
                request = URLRequest(url: url)
            }
            if let request = request {
                //webViewTermAndCondition.loadRequest(request)
                wkWebViewTerms.load(request)
                self.view.addSubview(wkWebViewTerms)
            }
        }
    }
    
    func bookingTAndC() {
        if CommonFunctions.reachabiltyCheck() {
            let requestUrl = URL(string: "https://www.mylescars.com/rests/getPaymentTerms/\(kBookingTermsAndConditions)")
            
            var data: NSData? = nil
            if let requestUrl = requestUrl {
                data = NSData(contentsOf: requestUrl)
            }
            
            if data != nil {
                var responseJson: [AnyHashable : Any]? = nil
                if let data = data {
                    responseJson = try! JSONSerialization.jsonObject(with: data as Data, options: []) as? [AnyHashable : Any]
                }
                let content = ((responseJson?["ResStatus"] as? [Any])?.last as? [AnyHashable : Any])?["content"] as? String
                
                let htmlString = "<html><head></head><body><p>\(content ?? "")</p></body></html>"
                wkWebViewTerms.loadHTMLString(htmlString, baseURL: nil)
                //self.view.addSubview(wkWebViewTerms)
                
                //webViewTermAndCondition.loadHTMLString(htmlString, baseURL: nil)
            }
            else
            {
                CommonFunctions.alertTitle(KSorry, withMessage: KAInternet)
            }
        }
        else
        {
            CommonFunctions.alertTitle(KSorry, withMessage: KAInternet)
        }
    }
    
}
