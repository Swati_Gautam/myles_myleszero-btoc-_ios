//
//  FeaturesTabController.swift
//  Myles
//
//  Created by iOS Team on 27/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import  Firebase
import SVProgressHUD

enum ShareTypes: String {
    case FACEBOOK = "Share on Facebook"
    case WHATSAPP = "Share on WhatsApp"
    case TWITTER = "Share on Twitter"
}

class FeaturesTabController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //IBOutlets
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var featuresArray : [String] = []
    
    @IBOutlet weak var featuresTableView: UITableView!
    @IBOutlet weak var logigLogoutButton: UIBarButtonItem!
    
    @IBOutlet weak var imageClickableButton: UIButton!
    fileprivate let sharingText = "Drive yourself with Myles, India's fastest growing self-drive brand. Book your ride now! https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579?mt=8"
    fileprivate let imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionLabel.text = "Build Version - \(version)"
        }
        featuresArray = ["FAQ","OFFERS","RATE US","ROADSIDE ASSISTANCE","CONTACT US","REFER & EARN","SUBSCRIBE A CAR"]
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        
        
        
//        [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"MenuView"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//        
//        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MenuView"
//            action:@"Click"
//            label:@"LogOut"
//            value:@1] build]];
//
        
    }
    override func viewWillAppear(_ animated: Bool) {
        guard let tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-64851985-4") else
        { return }
        tracker.set(kGAIScreenName, value: "More Tab")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (UserDefaults.standard.bool(forKey: IS_LOGIN)) {
            let name = UserDefaults.standard.object(forKey: KUSER_NAME) as? String
            let newStr = "(null) (null)"
            if (name == newStr) {
                self.nameLabel.text = ""//UserDefaults.standard.object(forKey: KUSER_FNAME) as? String
            } else {
                self.nameLabel.text = UserDefaults.standard.object(forKey: KUSER_NAME) as? String
            }
            logigLogoutButton.title = "Logout"
        } else {
            nameLabel.text = "Guest User"
            logigLogoutButton.title = "Login"
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Internal System
    
    @IBAction func imageButtonTapped(_ sender: Any) {
        /*
        if (UserDefaults.standard.bool(forKey: IS_LOGIN)) {
            let actionSheet = UIAlertController.init(title: "MYLES", message: "Please choose a source type", preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction.init(title: "Take Photo", style: UIAlertActionStyle.default, handler: { (action) in
                self.openCamera()
            }))
            actionSheet.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertActionStyle.default, handler: { (action) in
                self.openPhotoLibrary()
            }))
            actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) in
                // self.dismissViewControllerAnimated(true, completion: nil) is not needed, this is handled automatically,
                //Plus whatever method you define here, gets called,
                //If you tap outside the UIAlertController action buttons area, then also this handler gets called.
            }))
            //Present the controller
            self.present(actionSheet, animated: true, completion: nil)
        } else {
            let alertView = UIAlertController.init(title: "Please login to change your profile pic", message: nil, preferredStyle: .alert)
            alertView.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            }))
            self.present(alertView, animated: true, completion: nil)

            
        }
        */
        
        
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            imagePicker.sourceType = UIImagePickerController.SourceType.camera;
            
            self.present(imagePicker, animated: true, completion: nil)
        }

    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        DispatchQueue.main.async { //Swati Added 14 June
        SVProgressHUD.show(withStatus: "Please wait")
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func rateApp() {
        
        let openAppStoreForRating = "itms-apps://itunes.apple.com/gb/app/id1061852579?action=write-review&mt=8"
        if UIApplication.shared.canOpenURL(URL(string: openAppStoreForRating)!) {
            UIApplication.shared.openURL(URL(string: openAppStoreForRating)!)
        } else {
            showAlert(message: "Please select our app from the AppStore and write a review for us. Thanks!!")
        }
//        //  Converted with Swiftify v1.0.6381 - https://objectivec2swift.com/
//        let iOS7AppStoreURLFormat: String = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=\(APP_STORE_ID)&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software"
//        //"itms-apps://itunes.apple.com/app/\(APP_STORE_ID)"
//        //let iOSAppStoreURLFormat: String = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d"
//        //var url = URL(string: String(format: (CFloat(UIDevice.current.systemVersion) >= 7.0) ? iOS7AppStoreURLFormat : iOSAppStoreURLFormat, APP_STORE_ID))
//        let url = URL.init(string: iOS7AppStoreURLFormat)
//        UIApplication.shared.openURL(url!)
    }
    
    func showAlert(message:String)  {
            let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default,handler : nil)
            // let defaultAction = UIAlertAction(title: "OK", style: .default, handler:nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)

    }
    
    @IBAction func logoutButtonClicked(_ sender: Any) {
        
        if (UserDefaults.standard.bool(forKey: IS_LOGIN)) {
            guard let tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-64851985-4") else
            { return }
            tracker.set(kGAIScreenName, value: "LogOut")
            
            guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
            tracker.send(builder.build() as [NSObject : AnyObject])
            
            showLoader()
            CommonApiClass.userLogoutApi {[weak self] (data , response, error) in
                print("resonse")
                self?.dismissLoader()
                
                guard response != nil else{
                    DispatchQueue.main.async {
                        self?.showAlert(message:"Server not responding" )
                    }
                    return
                }
                var headers = (response as! HTTPURLResponse).allHeaderFields
                print(headers);
                let statusStr = (headers["Status"] as! String)
                guard !statusStr.isEmpty && (statusStr == "1")else{
                    //                            completionHandler(nil,false)
                    return
                }
                
                guard error?.localizedDescription == nil else{
                    print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                    //                            completionHandler(nil,true)
                    CommonFunctions.alertTitle("Myles", withMessage: "Server not responding")
                    return
                }
                
                
                guard data != nil else {
                    print("no data found: \(String(describing: error))")
                    //                            completionHandler(nil,true)
                    return
                }
                
                guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                    DispatchQueue.main.async {
                        self?.showAlert(message: "Server not responding")
                    }
                    return
                }
                
                //let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                print("jsonresponse = \(json) ")
                if ((json["status"]! as! Bool)) {
                    print(json["status"]!)
                    DispatchQueue.main.async {
                        self?.nameLabel.text = "Guest User"
                        self?.logigLogoutButton.title = "Login"
                        UserDefaults.standard.set("", forKey: K_SessionID)
                        UserDefaults.standard.set("", forKey: KUSER_PHONENUMBER)
                        UserDefaults.standard.set("", forKey: K_AccessToken)
                        UserDefaults.standard.set(false, forKey: IS_LOGIN)
                        CommonFunctions.logoutcommonFunction()
                        self?.showAlert(message: (json["message"] as? String)!)
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.showAlert(message: (json["message"] as? String)!)
                    }
                }
                return
                
                
            }

        } else {
            let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
            let editController: LoginProcess? = (storyBoard.instantiateViewController(withIdentifier: "LoginProcess") as? LoginProcess)
            let navigationController = UINavigationController(rootViewController: editController!)
            editController?.strOtherDestination = "0"
            navigationController.viewControllers = [editController!]
            present(navigationController, animated: true, completion: nil)
        }
        
    }
    
    //MARK: - UIPickerControllerDelegate
    //func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
    func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        
        if let pickedImage = info[.originalImage] as? UIImage { //info[UIImagePickerControllerOriginalImage]
//            let imageData = UIImageJPEGRepresentation(pickedImage, 0.6)
//            let compressedJPGImage = UIImage(data: imageData!)
            profileImageView.contentMode = .scaleAspectFit
            profileImageView.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewDatasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return featuresArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = featuresTableView.dequeueReusableCell(withIdentifier: "FeaturesCell")!
        cell.imageView?.image = UIImage.init(named: "\(indexPath.row + 1)")
        cell.textLabel?.text = featuresArray[indexPath.row]
        
        return cell
    }
    
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.row == 0) {
            DispatchQueue.main.async { //Swati added on 14 June
                self.performSegue(withIdentifier: self.featuresArray[indexPath.row], sender: self.featuresTableView)
            }
        }
        else if (indexPath.row == 1){
            let mainStoryboard = UIStoryboard(name: "MainStoryboard", bundle: nil)
            let CouponViewController: CouponViewController! = mainStoryboard.instantiateViewController(withIdentifier: "CouponViewController") as? CouponViewController
            CouponViewController.fromScreen = "MenuScreen";
             FIRAnalytics.logEvent(withName: "offer_clicked_nav", parameters: nil)
            self.present(CouponViewController, animated: true)
        }
        else {
            switch (indexPath.row - 2){
            case 0:
                rateApp()
            case 1:
                let url = URL.init(string: "tel://18001035079")
                UIApplication.shared.openURL(url!)
            case 2:
                let url = URL.init(string: "tel://8882222222")
                UIApplication.shared.openURL(url!)
            case 3:
                
                if UserDefaults.standard.bool(forKey: IS_LOGIN) {
                    DispatchQueue.main.async {
                    self.referAndEarn()
                    }
                }
                else
                {
                    self.presentLoginActionSheet()
                }
            case 4:
                self.subscribeACar()
                /*
                let actionSheet = UIAlertController.init(title: "MYLES", message: "Please choose platform to share", preferredStyle: .actionSheet)
                actionSheet.addAction(UIAlertAction.init(title: ShareTypes.FACEBOOK.rawValue, style: UIAlertActionStyle.default, handler: { (action) in
                    self.shareOnFacebook()
                }))
                actionSheet.addAction(UIAlertAction.init(title: ShareTypes.TWITTER.rawValue, style: UIAlertActionStyle.default, handler: { (action) in
                    self.shareOnTwitter()
                }))
                actionSheet.addAction(UIAlertAction.init(title: ShareTypes.WHATSAPP.rawValue, style: UIAlertActionStyle.default, handler: { (action) in
                    self.shareOnWhatsApp()
                }))
                actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) in
                    // self.dismissViewControllerAnimated(true, completion: nil) is not needed, this is handled automatically,
                    //Plus whatever method you define here, gets called,
                    //If you tap outside the UIAlertController action buttons area, then also this handler gets called.
                }))
                //Present the controller
                self.present(actionSheet, animated: true, completion: nil)
*/
            default:
                break
            }
            
        }
    }
    
    func presentLoginActionSheet(){
        let alertController = UIAlertController(title: "MYLES", message: "Please login to Refer & Earn", preferredStyle: UIAlertController.Style.actionSheet)
        let LoginAction = UIAlertAction(title: "Login", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            let storyBoard = UIStoryboard(name: k_registrationStoryBoard, bundle: nil)
            let viewController  = storyBoard.instantiateViewController(withIdentifier: "LoginProcess") as! LoginProcess
            let navigationController = UINavigationController(rootViewController: viewController)
            viewController.strOtherDestination = "0"
            viewController.fromScreen = ""
            navigationController.viewControllers = [viewController]
            
            DispatchQueue.main.async {
                self.present(navigationController, animated: true, completion: nil)
            }
            
        }
        
        let okAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        alertController.addAction(LoginAction)
        alertController.addAction(okAction)

        DispatchQueue.main.async { //Added by Swati 14 June
        self.tabBarController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func subscribeACar() {
        
        /*var userName = String()
        userName = UserDefaults.standard.value(forKey: KUSER_NAME) as! String
        
        var userEmail = String()
        userEmail = UserDefaults.standard.value(forKey: KUSER_EMAIL)as! String
        
        var userPhone = String()
        userPhone = UserDefaults.standard.value(forKey: KUSER_PHONENUMBER)as! String*/
        
        let objSearchCar = Constants.sideMenuStoryboard.instantiateViewController(withIdentifier: Constants.VC_SearchCar) as! SearchCarVC
        let navigationController : UINavigationController = UINavigationController.init(rootViewController: objSearchCar)
        AppDelegate.getSharedInstance()?.window.rootViewController = navigationController
        //self.navigationController?.pushViewController(objSearchCar, animated: true)
        
    }
    
    func referAndEarn() {
        
        var userName = String()
        userName = UserDefaults.standard.value(forKey: KUSER_NAME) as! String

        var userEmail = String()
        userEmail = UserDefaults.standard.value(forKey: KUSER_EMAIL)as! String

        var userPhone = String()
        userPhone = UserDefaults.standard.value(forKey: KUSER_PHONENUMBER)as! String
        
        DispatchQueue.main.async
        { //Swati Added on 14 June
            InviteReferrals.launch("0", email: userEmail as String, mobile: userPhone as String, name: userName as String, subscriptionID: nil)
        
            InviteReferrals.getShareData(withCampaignID: "0", email: userEmail as String, mobile: userPhone as String, name: userName as String, subscriptionID: nil, showErrorAlerts: true, showActivityIndicatorViewWhileLoading: true) { (response) in
            
                print("Response: ",response!)
            }
                
            InviteReferrals.tracking("Register", orderID: nil, purchaseValue: nil, email: userEmail, mobile: userPhone, name: userName, referCode: "", uniqueCode: "", isDebugMode: false) { (response) in
            
            print("Tracking Response callback json will come here i.e. = \(response!)")

            }
        }
    }
    
    func shareOnFacebook() {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
            //vc.add(URL(string: "http://www.example.com/"))
            vc?.setInitialText(sharingText)
            vc?.title = "MYLES"
            
            vc?.completionHandler = { (result:SLComposeViewControllerResult) -> Void in
                switch result {
                case SLComposeViewControllerResult.cancelled:
                    print("Cancelled") // Never gets called
                    break
                    
                case SLComposeViewControllerResult.done:
                    print("Done")
                    break
                }
            }
            DispatchQueue.main.async {
            self.present(vc!, animated: true, completion: nil)
            }
        } else {
            showAlert(message: "Facebook Not Configured")
        }
    }
    func shareOnWhatsApp() {
        //Whatsapp
        var urlWhats: String = sharingText
        urlWhats = urlWhats.replacingOccurrences(of: " ", with: "%20")
        urlWhats = urlWhats.replacingOccurrences(of: ":", with: "%3A")
        urlWhats = urlWhats.replacingOccurrences(of: "/", with: "%2F")
        urlWhats = urlWhats.replacingOccurrences(of: "?", with: "%3F")
        urlWhats = urlWhats.replacingOccurrences(of: ",", with: "%2C")
        urlWhats = urlWhats.replacingOccurrences(of: "=", with: "%3D")
        urlWhats = urlWhats.replacingOccurrences(of: "&", with: "%26")
        urlWhats = urlWhats.replacingOccurrences(of: ".", with: "%2E")
        let final: String = "whatsapp://send?text=\(urlWhats)"
        let whatsappURL = URL(string: final)
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.openURL(whatsappURL!)
        }
        else {
            showAlert(message: "Your device has no WhatsApp installed.")
        }
        
    }
    func shareOnTwitter() {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
            let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
            //vc.add(URL(string: "http://www.example.com/"))
            vc?.setInitialText(sharingText)
            vc?.title = "MYLES"

            vc?.completionHandler = { (result:SLComposeViewControllerResult) -> Void in
                switch result {
                case SLComposeViewControllerResult.cancelled:
                    print("Cancelled") // Never gets called
                    break
                    
                case SLComposeViewControllerResult.done:
                    print("Done")
                    break
                }
            }
            DispatchQueue.main.async {
            self.present(vc!, animated: true, completion: nil)
            }
        } else {
            showAlert(message: "Twitter Not Configured")
        }
        
    }

}


