//
//  MobileVerificationViewController.h
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

typedef enum
{
    KMobileVerification,
    KResendCode
}mobileVerification;

@interface MobileVerificationViewController : BaseViewController
{
    mobileVerification kAPI_Type;
    
}
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;


@end
