//
//  RegistrationViewController.h
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

@protocol SignUpComplete

-(void)signUpComplete:(BOOL)isSucess;

@end
@class SocialModel;
@interface RegistrationViewController : BaseViewController <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *termsBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnDOB;
- (IBAction)subscribeButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *subscribeButton;



@property  NSString *strMobileNumber;
@property  NSString *fromScreen;
@property BOOL isSocialLogin;
@property (strong, nonatomic) SocialModel *socialM;
@property id delegate;
@end
