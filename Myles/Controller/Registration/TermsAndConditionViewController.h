//
//  TermsAndConditionViewController.h
//  Myles
//
//  Created by Myles on 30/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TermsAndConditionViewController : BaseViewController <WKNavigationDelegate > //<UIWebViewDelegate >
{
    
}

@property (weak, nonatomic) IBOutlet WKWebView *wkWebViewTerms;
@property (nonatomic , assign) NSInteger type;

@end
