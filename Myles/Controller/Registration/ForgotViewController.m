//
//  ForgotViewController.m
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "ForgotViewController.h"
#import "Validate.h"
#import "CommonFunctions.h"
#import "AlertTitles.h"
#import <Google/Analytics.h>
#import "MobileVerificationViewController.h"

@interface ForgotViewController () <UIAlertViewDelegate>
{
    IBOutlet UITextField *txtMobileNo;
    
}

- (IBAction)viewTouched:(UIControl *)sender;

@end

@implementation ForgotViewController

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Forgot Password"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    [self customizeBackButton:false];    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [txtMobileNo resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    [super  viewWillAppear:YES];
    [self viewSetUp];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden=YES;
}
-(void)viewSetUp
{
    txtMobileNo.layer.cornerRadius = 2;
    UIColor *color = [UIColor colorWithRed:235. green:235. blue:235. alpha:0.6];
    txtMobileNo.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:txtMobileNo.text
     attributes:@{NSForegroundColorAttributeName:color}];
    [CommonFunctions setLeftPaddingToTextField:txtMobileNo andPadding:15];
    
    [CommonFunctions setTextFieldPlaceHolderText:txtMobileNo withText:txtMobileNo.placeholder];
    [CommonFunctions setLeftPaddingToTextField:txtMobileNo andPadding:15];
    [CommonFunctions setTextFieldPlaceHolderText:txtMobileNo withText:@"Mobile No"];
    
}



#pragma mark -
#pragma mark -  UINavigationItems & Action -





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TextFiled Delegates
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField==txtMobileNo) {
        
        /* for backspace */
        if([string length]==0){
            return YES;
        }
        
        NSInteger length = [CommonFunctions getLength:textField.text];
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        /*  limit to only numeric characters  */
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                if ([textField.text length]<16) {
                    return YES;
                    
                }else{
                    return NO;
                    
                }
            }
        }
        return NO;
        
    }
    
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark- Button Actions

- (IBAction)btnSendClicked:(id)sender {
    
    NSString *postTxt = [txtMobileNo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (postTxt.length>0)
    {
        if (postTxt.length==10) {
            if ([CommonFunctions reachabiltyCheck])
            {
                AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                [self showLoader];
                [appDelegate getAccessTokenWithCompletion:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                    });
                    [self mobileVerificationAPI];
                }];
                
            }
            else
            {
                [CommonFunctions alertTitle:nil withMessage:KAInternet];
            }
        }
        else
        {
            [CommonFunctions alertTitle:KSorry withMessage:KAValidMobile];
        }
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:KAValidMobile];
    }
    
}

- (IBAction)btnReSendClicked:(id)sender {
    
}


#pragma mark -
#pragma mark - SERVER INTERACTION -
-(void)mobileVerificationAPI
{
    NSMutableDictionary *body = [NSMutableDictionary new];
    
    NSString *postTxt = [txtMobileNo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [body setObject: [CommonFunctions formatNumber:postTxt] forKey:@"phonenumber"];

    NSURLSession *session = [CommonFunctions defaultSession];
    
    NSString *userid = [DEFAULTS valueForKey:KUSER_ID];
    if (userid != nil){
        [body setObject:userid forKey:@"userid"];
    }
    
    NSLog(@"KFORGOT_PASSWORD post body =%@",body);
    
    [self showLoader];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KFORGOT_PASSWORD completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                         {
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 [SVProgressHUD dismiss];
                                             });
                                             NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                             NSString *statusStr = [headers objectForKey:@"Status"];
                                             if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                             {
                                                 if (!error)
                                                 {
                                                     NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                     
                                                     NSLog(@"Forgot password response json = %@",responseJson);
                                                     
                                                     NSInteger status = [[responseJson objectForKey:@"status"] integerValue];
                                                     if (status == 1)
                                                     {
                                                         
                                                         dispatch_async(dispatch_get_main_queue(), ^
                                                                        {
                                                                            
                                                                            
                                                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[responseJson valueForKey:k_ApiMessageKey] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                            alert.tag = 1;
                                                                            [alert show];
                                                                        });
                                                         
                                                     }
                                                     else if (status == 2){
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             //[DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:[responseJson valueForKey:k_ApiMessageKey] preferredStyle:UIAlertControllerStyleAlert];
                                                             
                                                             UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                                 //[self.navigationController popViewControllerAnimated:YES];
                                                                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RegistrationProcess" bundle:[NSBundle mainBundle]];
                                                                 MobileVerificationViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"mobileIdentifier"];
                                                                 [self.navigationController pushViewController:controller animated:YES];
                                                                 
                                                             }];
                                                             [alertController addAction:ok];
                                                             
                                                             [self presentViewController:alertController animated:YES completion:nil];
                                                             
                                                         });

                                                     }
                                                     else {
                                                         dispatch_async(dispatch_get_main_queue(), ^{

                                                             [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                             
                                                         });
                                                         
                                                     }
                                                     
                                                 }

                                             }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                 NSLog(@"AccessToken Invalid");
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"MYLES" message:@"Session Expired" preferredStyle:UIAlertControllerStyleAlert];
                                                     
                                                     UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                         [CommonFunctions logoutcommonFunction];
                                                         //[self.revealViewController revealToggleAnimated:true];

                                                         
                                                     }];
                                                     [alertController addAction:ok];
                                                     
                                                     [self presentViewController:alertController animated:YES completion:nil];
                                                 });

                                             } else
                                             {
                                            }
                                             
                                            
                                             
                                         }];
    
    [globalOperation addOperation:registrationOperation];
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}



- (IBAction)viewTouched:(UIControl *)sender
{
    [self.view endEditing:YES];
}
@end
