//
//  MobileVerificationViewController.m
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "MobileVerificationViewController.h"
#import "Validate.h"
#import "CommonFunctions.h"
#import "AlertTitles.h"
#import "MenuViewController.h"
#import "LoginViewController.h"
#import <Google/Analytics.h>


@interface MobileVerificationViewController () <SWRevealViewControllerDelegate>
{
    IBOutlet UIView *viewForCode;
    IBOutlet UITextField *txtCode;
    
}
- (IBAction)btnSendClicked:(id)sender;
- (IBAction)btnReSendClicked:(id)sender;
@end

@implementation MobileVerificationViewController

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Verify Code"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [CommonFunctions setLeftPaddingToTextField:txtCode andPadding:15];
    [CommonFunctions setTextFieldPlaceHolderText:txtCode withText:@"Enter Code"];
    
    // Do any additional setup after loading the view.
    [self customizeBackButton:false];
    //[self customizeNavigationBar:@"MOBILE VERIFICATION"];
    
    [self setupUI];
}


-(void)setupUI
{
    txtCode.layer.cornerRadius = 2;
    UIColor *color = [UIColor colorWithRed:235. green:235. blue:235. alpha:0.6];
    txtCode.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:txtCode.text
     attributes:@{NSForegroundColorAttributeName:color}];
    txtCode.keyboardType=UIKeyboardTypeNumberPad;
    [CommonFunctions setLeftPaddingToTextField:txtCode andPadding:15];
    [CommonFunctions setTextFieldPlaceHolderText:txtCode withText:@"Code"];
    self.messageLabel.adjustsFontSizeToFitWidth = true;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark- TextFiled Delegates
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark- Button Actions

- (IBAction)btnSendClicked:(id)sender {
    
    NSString *postTxt = [txtCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (postTxt.length>0)
    {
        if ([CommonFunctions reachabiltyCheck])
        {
            [self mobileVerificationAPI:KMobileVerification];
            
        }
        else
        {
            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        }
    }
    else
    {
        [CommonFunctions alertTitle:KSorry  withMessage:kvaildCode];
    }
    
}

- (IBAction)btnReSendClicked:(id)sender
{
    if ([CommonFunctions reachabiltyCheck])
    {
        [self mobileVerificationAPI:KResendCode];
        
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
    
}


#pragma mark -   
#pragma mark - SERVER INTERACTION -
/*
-(void)mobileVerificationAPI:(mobileVerification )type
{
    NSMutableDictionary *body = [NSMutableDictionary new];
    
    
    switch (type)
    {
        case KMobileVerification:
        {
            NSString *postTxt = [txtCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [body setObject: postTxt forKey:@"code"];
        }
            
            break;
        case KResendCode:
        {
        }
            
            break;
            
        default:
            break;
    }
    
    [body setObject:[DEFAULTS objectForKey:KUSER_ID] forKey:@"userid"];
    NSURLSession *session = [CommonFunctions defaultSession];
    [GMDCircleLoader setOnView:self.view withTitle:@"Please wait..." animated:YES];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:(type == KMobileVerification)?KPHONE_NUMBER_VERIFICATION:KRESEND_CODE completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                         {
                                             if (!error)
                                             {
                                                 NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                 
                                                 if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 1)
                                                 {
                                                     
                                                     if(type == KMobileVerification)
                                                     {
                                                         [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                         
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             
                                                             [CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                             UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                                             
                                                             SWRevealViewController *revealController = [storyBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                                             
                                                             UINavigationController *homeNavigationController = [storyBoard instantiateViewControllerWithIdentifier:@"HomeNavigation"];
                                                             
                                                             
                                                             [revealController setFrontViewController:homeNavigationController];
                                                             MenuViewController *menuController =  [storyBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                                                             [revealController setRearViewController:menuController];
                                                             
                                                             revealController.delegate = self;
                                                             
                                                             [[[AppDelegate getSharedInstance] window] setRootViewController:revealController];
                                                         });
                                                     }
                                                     else
                                                     {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             
                                                             [CommonFunctions alertTitle:@"" withMessage:[(NSMutableDictionary *)responseJson objectForKey:@"message"]];
                                                             
                                                             
                                                         });
                                                         
                                                     }
                                                 }
                                                 else
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                         
                                                         
                                                         
                                                     });
                                                     
                                                 }
                                             }
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                                 [GMDCircleLoader hideFromView:self.view animated:YES];
                                                 
                                                 
                                             });
                                         }];
    [globalOperation addOperation:registrationOperation];
}
 */

/*

-(void)mobileVerificationAPI:(mobileVerification )type
{
    NSMutableDictionary *body = [NSMutableDictionary new];
    
    
    switch (type)
    {
        case KMobileVerification:
        {
            NSString *postTxt = [txtCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [body setObject: postTxt forKey:@"code"];
        }
            
            break;
        case KResendCode:
        {
        }
            
            break;
            
        default:
            break;
    }
    
    [body setObject:[DEFAULTS objectForKey:KUSER_ID] forKey:@"userid"];
    NSURLSession *session = [CommonFunctions defaultSession];
    [GMDCircleLoader setOnView:self.view withTitle:@"Loading..." animated:YES];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:(type == KMobileVerification)?KPHONE_NUMBER_VERIFICATION:KRESEND_CODE completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                           {
                                               if (!error)
                                               {
                                                   NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                   
                                                   if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 1)
                                                   {
                                                       
                                                       if(type == KMobileVerification)
                                                       {
                                                           [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                           [DEFAULTS synchronize];
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:KMessage withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                               UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
                                                               
                                                               SWRevealViewController *revealController = [storyBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                                               
                                                               UINavigationController *homeNavigationController = [storyBoard instantiateViewControllerWithIdentifier:@"HomeNavigation"];
                                                               
                                                               
                                                               [revealController setFrontViewController:homeNavigationController];
                                                               MenuViewController *menuController =  [storyBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
                                                               [revealController setRearViewController:menuController];
                                                               
                                                               revealController.delegate = self;
                                                               
                                                               [[[AppDelegate getSharedInstance] window] setRootViewController:revealController];
                                                           });
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:@"" withMessage:[(NSMutableDictionary *)responseJson objectForKey:@"message"]];
                                                               
                                                               
                                                           });
                                                           
                                                       }
                                                   }
                                                   else
                                                   {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           
                                                           [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                           
                                                           
                                                           
                                                       });
                                                       
                                                   }
                                               }
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   
                                                   [GMDCircleLoader hideFromView:self.view animated:YES];
                                                   
                                                   
                                               });
                                               
                                           }];
    
    [globalOperation addOperation:registrationOperation];
    
    
    
}
 */

-(void)mobileVerificationAPI:(mobileVerification )type
{
    NSMutableDictionary *body = [NSMutableDictionary new];
    
    
    switch (type)
    {
        case KMobileVerification:
        {
            NSString *postTxt = [txtCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [body setObject: postTxt forKey:@"code"];
        }
            
            break;
        case KResendCode:
        {
        }
            
            break;
            
        default:
            break;
    }
    
    NSLog(@"[DEFAULTS valueForKey:KUSER_ID] =%@",[DEFAULTS valueForKey:KUSER_ID]);
    [body setObject:[DEFAULTS valueForKey:KUSER_ID] forKey:@"userid"];
    NSLog(@"body otp = %@",body);
    
    
    [self showLoader];
    
    NSURLSession *session = [CommonFunctions defaultSession];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:(type == KMobileVerification)?KPHONE_NUMBER_VERIFICATION:KRESEND_CODE completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                           {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD dismiss];
                                               });

                                               NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                               NSString *statusStr = [headers objectForKey:@"Status"];
                                               if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                               {
                                                   if (!error && data != nil)
                                                   {
                                                       NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                       
                                                       if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 1)
                                                       {
                                                           
                                                           if(type == KMobileVerification)
                                                           {
                                                               [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                               [DEFAULTS synchronize];
                                                               
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   
                                                                   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RegistrationProcess" bundle:[NSBundle mainBundle]];
                                                                   
                                                                   LoginViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                                   
                                                                   [self.navigationController pushViewController:controller animated:YES];
                                                        
                                                               });
                                                           }
                                                           else
                                                           {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   
                                                                   [CommonFunctions alertTitle:@"" withMessage:[(NSMutableDictionary *)responseJson objectForKey:@"message"]];
                                                                   
                                                                   
                                                               });
                                                               
                                                           }
                                                       }
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [CommonFunctions alertTitle:KSorry withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                               
                                                           });
                                                           
                                                       }
                                                   }
                                                   
                                                   

                                               }  else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                   NSLog(@"AccessToken Invalid");
                                                                                                 }
                                               else
                                               {
                                                   
                                                                                                  }
                                               
                                           }];
    
    [globalOperation addOperation:registrationOperation];
    
    
    
}



@end
