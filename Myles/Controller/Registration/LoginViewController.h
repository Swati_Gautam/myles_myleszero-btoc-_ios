//
//  LoginViewController.h
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>


//@protocol LoginCompleteProtocol
//@optional
//-(void)loginWithOtherDestination:(NSString*)strDestination;
//@end
@interface LoginViewController : BaseViewController <SWRevealViewControllerDelegate>
{
    IBOutlet UIButton *btnTerms;

}
@property id delegate;
@property NSString *strOtherDestination;
@end
