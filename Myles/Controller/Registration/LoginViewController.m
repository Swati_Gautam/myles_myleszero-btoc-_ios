//
//  LoginViewController.m
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "LoginViewController.h"
#import "Validate.h"
#import "CommonFunctions.h"
#import "AlertTitles.h"
#import "MenuViewController.h"
#import <CoreText/CoreText.h>
#import "MobileVerificationViewController.h"
#import <Google/Analytics.h>
#import "NSString+MD5.h"
#import "Myles-Swift.h"
#import "HomeViewController.h"
#import "UserProfileVC.h"
#import "RideHistoryVC.h"
#import "CarListViewController.h"
#import "RegistrationViewController.h"

@import FirebaseAnalytics;

@interface LoginViewController ()<UITextFieldDelegate,AfterNumberVerify>
{
    IBOutlet UIView *viewForTextFields;
    IBOutlet UITextField *txtMobileNo;
    IBOutlet UITextField *txtPassword;
    
}
- (IBAction)viewTouched:(UIControl *)sender;

- (IBAction)btnSignInClicked:(id)sender;
- (IBAction)btnSignUpClicked:(id)sender;
- (IBAction)btnTermClicked:(id)sender;

-(void)viewSetup;
@end

@implementation LoginViewController

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Sign In"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    
    // Do any additional setup after loading the view.
    [self viewSetup];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Sign In"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    self.navigationController.navigationBarHidden = YES;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear: animated];
    self.navigationController.navigationBarHidden=YES;
    //[self.view layoutIfNeeded];
    
    
}
-(void)viewSetup
{
    self.navigationController.navigationBarHidden=YES;
    txtMobileNo.layer.cornerRadius = 2;
    txtPassword.layer.cornerRadius = 2;
    UIColor *color = [UIColor colorWithRed:235. green:235. blue:235. alpha:0.6];
    txtMobileNo.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:txtMobileNo.text
     attributes:@{NSForegroundColorAttributeName:color}];
    
    txtPassword.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:txtPassword.text
     attributes:@{NSForegroundColorAttributeName:color}];
    
    NSMutableAttributedString *temString=[[NSMutableAttributedString alloc]initWithString:btnTerms.titleLabel.text];
    [temString addAttribute:NSUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:1]
                      range:(NSRange){0,[temString length]}];
    //[self.view layoutIfNeeded];
    btnTerms.titleLabel.attributedText = temString;
    btnTerms.titleLabel.textColor = [UIColor colorWithWhite:1 alpha:.6];
    
    [CommonFunctions setLeftPaddingToTextField:txtMobileNo andPadding:15];
    [CommonFunctions setLeftPaddingToTextField:txtPassword andPadding:15];
    [CommonFunctions setTextFieldPlaceHolderText:txtPassword withText:@"Mobile No"];
    [CommonFunctions setTextFieldPlaceHolderText:txtMobileNo withText:@"Password"];
    
    self.navigationController.navigationBar.backgroundColor=[UIColor redColor];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark- TextFiled Delegates
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField==txtPassword) {
        
        /* for backspace */
        if([string length]==0){
            return YES;
        }
        
        NSInteger length = [CommonFunctions getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if(length == 10)
        {
            if(range.length == 0)
            {
                return NO;
            }
        }
        
        
        //
        /*  limit to only numeric characters  */
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                if ([textField.text length]<16) {
                    return YES;
                    
                }else{
                    return NO;
                    
                }
            }
        }
        return NO;
        
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txtPassword && textField.text.length ==10)
    {
        [textField resignFirstResponder];
        [self showLoader];
        [CommonApiClass checkUserExistorNot:textField.text :^(NSDictionary *dict,BOOL isValidToken) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            if (isValidToken && dict != nil) {
                switch ([dict[@"status"] integerValue]) {
                    case NumberExist:
                    {
                        NSLog(@"NumberExist %@",dict[@"message"]);
                        
//                        [textField becomeFirstResponder];
                        
                    }
                        break;
                    case NumberNotExist:
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [textField resignFirstResponder];
                            
                            UIStoryboard *storyboard = k_storyBoard(k_registrationStoryBoard);
                            OtpViewController *OtpController = [storyboard instantiateViewControllerWithIdentifier:@"OtpViewController"];
                            OtpController.strMobileNumber = textField.text;
                            OtpController.delegate = self;
                            [self.navigationController presentViewController:OtpController animated:true completion:nil];
                        });
                    }
                        
                        NSLog(@"NumberExist %@",dict[@"message"]);
                        
                        
                        break;
                    case NumberDeactiveByAdmin:
                        NSLog(@"NumberExist %@",dict[@"message"]);
                        
                        break;
                        
                    default:
                        
                        break;
                }
               
            }
            else
            {
                
            }
        }];
        
    }

}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
        return YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark- button Actions
- (IBAction)viewTouched:(UIControl *)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnSignInClicked:(id)sender {

    
    NSString *strMobileNo= [CommonFunctions formatNumber:txtPassword.text];
    if ([self lenghtValidateOfTextFeilds])
    {
        if (strMobileNo.length==10) {
            
            
            if ([CommonFunctions reachabiltyCheck])
            {
                [txtPassword resignFirstResponder];
                [txtMobileNo resignFirstResponder];
                __weak LoginViewController *blockSelf = self;
                NSString *accessToken = [DEFAULTS objectForKey:K_AccessToken];
                
                if (accessToken.length > 0)
                {
                    [self loginUserAPI:^{
                        //[blockSelf registerCustomerID];
                    }];
                } else {
                    [self showLoader];

                    [CommonFunctions getAccessTokenWithCompletion:^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                        });
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [GMDCircleLoader hideFromView:self.view animated:YES];
//                        });
                        [blockSelf loginUserAPI:^{
                            //[blockSelf registerCustomerID];
                        }];
                    }];
                }
                
            }
            else
            {
                
                [CommonFunctions showNoNetworkAlert];
                //[CommonFunctions alertTitle:nil withMessage:KAInternet];
            }
        }else{
            [CommonFunctions alertTitle:KSorry withMessage:KAValidMobile];
        }
        
    }
}



-(void)registerCustomerID {
    if ([CommonFunctions reachabiltyCheck]) {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache" };
        
        NSDictionary *parameters = @{ @"tokenKey": [DEFAULTS objectForKey:K_AccessToken],
                                      @"customerID": [DEFAULTS objectForKey:KUSER_PHONENUMBER],
                                      @"sessionID": [DEFAULTS objectForKey:K_SessionID]
                                    };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:registerCutomerIDURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                           timeoutInterval:KTIMEOUT_INTERVAL];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                               // [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                            });
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                            
                                                            
                                                            NSString *statusStr = [jsonreponse objectForKey:@"status"];
                                                            NSLog(@"string = %@",statusStr);
                                                            if (statusStr != nil) {
                                                                if ([statusStr isEqualToString:@"1"])
                                                                {
                                                                    NSLog(@"statusstr=%@",statusStr);
                                                                }
                                                            }
                                                            
                                                            
                                                        }
                                                    }];
        [dataTask resume];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        });
    }

}

#pragma mark -
#pragma mark - SERVER INTERACTION -
#pragma mark -
#pragma mark - SERVER INTERACTION -
-(void)loginUserAPI:(void (^)(void))completion
{
    
    NSString *md5Password=[txtMobileNo.text MD5];
    NSMutableDictionary *body = [NSMutableDictionary new];
    NSString *number = [CommonFunctions formatNumber:txtPassword.text];
    [body setObject:md5Password forKey:@"password"];
    
    
    
    if (number.length > 0) {
        [body setObject: number forKey:@"phonenumber"];
        
    }
    
#if TARGET_IPHONE_SIMULATOR
    
    //Simulator
    deviceToken = @"0a826c47e82a3b2aeb14131357d470e1653b0757d4a985a8d452ad485a8951a7";
    [body setObject:deviceToken forKey:@"devicetoken"];
    
    
#else
    // Device
    deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"devicetoken"];
    if (deviceToken.length > 0) {
        [body setObject:deviceToken forKey:@"devicetoken"];
    }
    else{
        [body setObject:@"" forKey:@"devicetoken"];
    }
    
#endif
    [body setObject:@"ios" forKey:@"devicetype"];
    NSURLSession *session = [CommonFunctions defaultSession];
    [self showLoader];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSER_LOGIN completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                           {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD dismiss];
                                               });
                                               NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                               NSString *statusStr = [headers objectForKey:@"Status"];
                                               if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                               {
                                                   if (!error)
                                                   {
                                                       NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                       
                                                       NSLog(@"responseJson =%@",responseJson);
                                                       // SUCCESSFUL LOGIN
                                                       if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 1)
                                                       {
                                                        
                                                           
                                                               [[NSUserDefaults standardUserDefaults] setObject:responseJson forKey:@"userDetail"];
                                                           
                                                           [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"emailId"] forKey:KUSER_EMAIL];
                                                           
                    if ([[responseJson objectForKey:@"response"] objectForKey:@"lname"])
                                                           {
                                                               NSString *name = [NSString stringWithFormat:@"%@ %@",[[responseJson objectForKey:@"response"] objectForKey:@"fname"],[[responseJson objectForKey:@"response"] objectForKey:@"lname"]];
                                                               [DEFAULTS setObject:name forKey:KUSER_NAME];
                                                               
                                                           }
                                                           else
                                                           {
                                                               [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"fname"] forKey:KUSER_NAME];
                                                               
                                                           }
                                                           [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                           NSLog(@"username =%@ and Password = %@",txtPassword.text,txtMobileNo.text);
                                                           [CommonFunctions updateKeychainForUsername:txtPassword.text Password:txtMobileNo.text];
                                                           [CommonFunctions searchKeyChainForUserName:txtPassword.text :^(NSString *password) {
                                                               
                                                           }];
                                                                                                                     
                                                           
                                                           [DEFAULTS setObject:txtPassword.text forKey:KUSER_PHONENUMBER];
                                                           [DEFAULTS setBool:YES forKey:IS_LOGIN];
                                                           [DEFAULTS synchronize];
                                                           
                                                           if (completion) {
                                                               
                                                               completion();
                                                           }
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [self dismissViewControllerAnimated:true completion:^{
                                                                   if (_strOtherDestination != nil) {
//                                                                        [self.delegate loginWithOtherDestination:_strOtherDestination];
                                                                       _strOtherDestination = nil;
                                                                   }
                                                              
                                                               }];
                                                               
                                                               

//
//                                                               UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
//                                                               
//                                                               [self postActiveNotifications:@"0"];
//
//                                                               SWRevealViewController *revealController = [storyBoard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//                                                               
//                                                               UINavigationController *homeNavigationController = [storyBoard instantiateViewControllerWithIdentifier:@"HomeNavigation"];
//                                                               
//                                                               [revealController setFrontViewController:homeNavigationController];
//                                                               
//                                                               MenuViewController *menuController =  [storyBoard instantiateViewControllerWithIdentifier:k_MenuViewController];
//                                                               [revealController setRearViewController:menuController];
//                                                               
//                                                               revealController.delegate = self;
//                                                               
//                                                               [[[AppDelegate getSharedInstance] window] setRootViewController:revealController];
//                                                               if ([[UIApplication sharedApplication].shortcutItems count] > 0)
//                                                               {
//                                                                   AppDelegate *appDelegate = [AppDelegate getSharedInstance];
//                                                                   [appDelegate configDynamicShortcutItems];
//                                                               }
//
                                                           });
                                                       }
                                                       // MOBILE NUMBER IS NOT VERIFIED YET
                                                       else if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 2)
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                            [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                               
                                                               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RegistrationProcess" bundle:[NSBundle mainBundle]];
                                                               MobileVerificationViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"mobileIdentifier"];
                                                               [self.navigationController pushViewController:controller animated:YES];
                                                           });
                                                           
                                                       }
                                                       // LOGIN FAILED
                                                       else
                                                       {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               
                                                               
                                                               [CommonFunctions alertTitle:nil withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                           });
                                                       }
                                                   }
                                                   
                                               } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                   NSLog(@"AccessToken Invalid");
                                                   
                                                   
                                               } else
                                               {
                                                   
                                               }
                                               
                                               
                                           }];
    
    [globalOperation addOperation:registrationOperation];
}

- (void)postActiveNotifications:(NSString *)type
{
    [[NSNotificationCenter defaultCenter] postNotificationName:KACTIVE_BOOKING_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObject:type forKey:@"type"]];
}


- (IBAction)btnSignUpClicked:(id)sender {
}

- (IBAction)btnTermClicked:(id)sender {
}

-(void)showMessageToUser:(NSString *)messageString {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"MYLES" message:messageString preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            NSLog(@"cancel");
        }];
        action;
    })];
    
    
    [self presentViewController:alert animated:YES completion:nil];

    
}

-(BOOL)lenghtValidateOfTextFeilds{
    
    
    
    if (txtMobileNo.text.length < 1 && txtPassword.text.length < 1){
        [self showMessageToUser:@"Enter Username and Password"];
        //[CommonFunctions alertTitle:KSorry withMessage:@"Enter Username and Password"];
        return FALSE;
    }
    else if (txtMobileNo.text.length > 5 && txtPassword.text.length> 0) {
        return YES;
    }
    else if (txtPassword.text.length> 0)
    {
        if ((txtPassword.text.length > 0) && (txtPassword.text.length < 10))
        {
            [self showMessageToUser:kphonelessthen10msg];

           // [CommonFunctions alertTitle:KSorry withMessage:kphonelessthen10msg];
            return NO;
        }
        else
        {
            if (txtMobileNo.text.length < 6)
            {
                [self showMessageToUser:kAPasswordLenghtValidation];
                //[CommonFunctions alertTitle:KSorry withMessage:kAPasswordLenghtValidation];
            } else {
                [self showMessageToUser:kEmptypasswordFieldMesg];

                //[CommonFunctions alertTitle:KSorry withMessage:kEmptypasswordFieldMesg];
            }
            return NO;
        }
    }else if ((txtMobileNo.text.length > 0) && (txtMobileNo.text.length < 6))
        {
            [self showMessageToUser:kAPasswordLenghtValidation];

           // [CommonFunctions alertTitle:KSorry withMessage:kAPasswordLenghtValidation];
            return NO;
        }
        else
        {
            [self showMessageToUser:kEmptypasswordFieldMesg];

//            [CommonFunctions alertTitle:KSorry withMessage:kEmptypasswordFieldMesg];
            return NO;
        }

}
-(IBAction)btnTouchId:(id)sender
{
    
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_mainStoryBoard bundle:nil];
////
//    LoginWithTouch *dealVC = (LoginWithTouch *)[storyBoard instantiateViewControllerWithIdentifier:@"LoginWithTouch"];
//    [self.navigationController pushViewController:dealVC animated:YES];
    
    LoginWithTouchID *loginWithTouchId = [[LoginWithTouchID alloc]init];
    loginWithTouchId.delegate = self;
    
    [loginWithTouchId checkDeviceSupportedTouchID];
    
     //    [CommonFunctions searchKeyChainForUserName:@"8750872990"];
}

- (void)loginSucessWithResponse:(id _Nonnull)response;
{
    if ([response isKindOfClass:[NSString class]])
    {
        [self showMessageToUser:response];
        //[CommonFunctions alertTitle:@"Myles" withMessage:response withDelegate:nil :self.navigationController];
    }
    else
    {
//        [CommonFunctions alertTitle:@"Myles" withMessage:@"Login sucess"];
        [self showLoader];
          [CommonFunctions searchKeyChainForUserName:[DEFAULTS objectForKey:KUSER_PHONENUMBER]:^(NSString *password) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [SVProgressHUD dismiss];
              });
              txtMobileNo.text =password;
              txtPassword.text = [DEFAULTS objectForKey:KUSER_PHONENUMBER];
              dispatch_async(dispatch_get_main_queue(), ^{
                  __weak LoginViewController *blockSelf = self;
                  NSString *accessToken = [DEFAULTS objectForKey:K_AccessToken];
                  
                  if (accessToken.length > 0)
                  {
                      [self loginUserAPI:^{
                          //[blockSelf registerCustomerID];
                      }];
                  } else {
                      
                      [CommonFunctions getAccessTokenWithCompletion:^{
                         
                          [blockSelf loginUserAPI:^{
                          }];
                      }];
                  }

              });
              
          }];
       

    }
}
- (UIViewController *)backViewController
{
    NSInteger myIndex = [self.navigationController.viewControllers indexOfObject:self];
    
    if ( myIndex != 0 && myIndex != NSNotFound )
    {
        return [self.navigationController.viewControllers objectAtIndex:myIndex-1];
    }
    else
    {
        return nil;
    }
}

-(IBAction)skipEvent:(id)sender
{
        [self dismissViewControllerAnimated:YES completion:nil];
        
}
- (void)callSignupPage
{
    UIStoryboard *storyboard = k_storyBoard(k_registrationStoryBoard);

    RegistrationViewController *RegistrationController = [storyboard instantiateViewControllerWithIdentifier:@"RegistrationController"];
    RegistrationController.strMobileNumber = txtPassword.text;
    [self.navigationController pushViewController:RegistrationController animated:true];
}
@end
