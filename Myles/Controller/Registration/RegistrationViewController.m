//
//  RegistrationViewController.m
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited    //

#import "RegistrationViewController.h"
#import "RegistrationTblCell.h"
#import "Validate.h"
#import "CommonFunctions.h"
#import "AlertTitles.h"
#import "MobileVerificationViewController.h"
#import <Google/Analytics.h>
#import "NSString+MD5.h"
#import "Myles-Swift.h"
#import <QuartzCore/QuartzCore.h>

#define KValue @"Value"
#define KPlaceHolder @"PlaceHolder"
#define KKey @"Key"

@import FirebaseAnalytics;

@interface RegistrationViewController ()<UITextFieldDelegate>
{
    IBOutlet UIView *viewForTable;
    //__weak IBOutlet UIButton *btnDOB;
    IBOutlet UITableView *tblRegistration;
    IBOutlet UILabel *lblTermsandCondition;
    NSMutableArray *arrRegistration;
    NSOperationQueue *globalOperation;
    IBOutlet NSLayoutConstraint *viewFortableYconstraint;
    
    UITapGestureRecognizer *labelTapGesture;
    
    //Taxt fields
    IBOutlet UITextField *firstNameTxt;
    IBOutlet UITextField *lastNameTxt;
    IBOutlet UITextField *emailidTxt;
    IBOutlet UITextField *mobileNumberTxt;
    IBOutlet UITextField *passwordTxt;
    IBOutlet UIButton *signUpBtn;
    
    __weak IBOutlet NSLayoutConstraint *referralButtonHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *referralTextFieldHeightConstraint;
    
    __weak IBOutlet UITextField *referralCodeTxt;
    __weak IBOutlet UILabel *referralCodeLabel;
    BOOL isDOBSelected;
    
    NSString * subscribeStr;
    
    
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) UITextField *activeField;

- (IBAction)btnTermsAndConditionClicked:(id)sender;
- (IBAction)btnSignInClicked:(id)sender;
- (IBAction)btnRedeemClicked:(UIButton *)sender;
@end

@implementation RegistrationViewController

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    mobileNumberTxt.text = _strMobileNumber;
    subscribeStr = @"0";
    [_subscribeButton setImage:[UIImage imageNamed:@"checkbox-selected"] forState:UIControlStateSelected];
    
    labelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    labelTapGesture.numberOfTapsRequired = 1;
    
    if (_isSocialLogin) {
        referralButtonHeightConstraint.constant = 17;
        referralTextFieldHeightConstraint.constant = 17;
    }
    
    
   
//        firstNameTxt.layer.borderColor = [UIColor darkGrayColor].CGColor;
//    firstNameTxt.layer.borderWidth = 1.0;
//    firstNameTxt.layer.cornerRadius= 5.0;
//
//       lastNameTxt.layer.borderColor = [UIColor darkGrayColor].CGColor;    lastNameTxt.layer.borderWidth = 1.0;
//    lastNameTxt.layer.cornerRadius= 5.0;
//
//
//       emailidTxt.layer.borderColor = [UIColor darkGrayColor].CGColor;    emailidTxt.layer.borderWidth = 1.0;
//    emailidTxt.layer.cornerRadius= 5.0;
//
//
//       mobileNumberTxt.layer.borderColor = [UIColor darkGrayColor].CGColor;    mobileNumberTxt.layer.borderWidth = 1.0;
//    mobileNumberTxt.layer.cornerRadius= 5.0;
//
       _btnDOB.layer.borderColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0].CGColor;
    _btnDOB.layer.borderWidth = 1.0;

        _btnDOB.layer.cornerRadius= 5.0;
    signUpBtn.layer.cornerRadius= 5.0;

    
    if (_isSocialLogin && _socialM.fName) {
        firstNameTxt.text = _socialM.fName;
    }
    
    if (_isSocialLogin && _socialM.lName) {
        lastNameTxt.text = _socialM.lName;
    }
    
    if (_isSocialLogin && _socialM.email) {
        emailidTxt.text = _socialM.email;
        emailidTxt.userInteractionEnabled = false;
    }
    
    if (_isSocialLogin && _socialM.dob) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // this is imporant - we set our input date format to match our input string
        // if format doesn't match you'll get nil from your string, so be careful
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateFromString = [[NSDate alloc] init];
        // voila!
        isDOBSelected =true;
        dateFromString = [dateFormatter dateFromString:_socialM.dob];
        
 [self.btnDOB setTitle:[self IndianTimeConverter:dateFromString] forState:UIControlStateNormal];
        [self.btnDOB setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    }
    
    if (_isSocialLogin) {
        passwordTxt.hidden = YES;
    }
    
  //  mobileNumberTxt.text = @"8750872990";
    mobileNumberTxt.userInteractionEnabled = false;
    
    [_btnDOB setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];

    referralCodeTxt.userInteractionEnabled = TRUE;
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Sign Up"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
   self.navigationController.navigationBarHidden = true;
    globalOperation = [[NSOperationQueue alloc] init];
    arrRegistration=[[NSMutableArray alloc]init];
    [self viewSetUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UITapGestureRecognizer *yourTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollTap:)];
    [self.scrollView addGestureRecognizer:yourTap];
    [self.view addSubview:self.scrollView];
    

    
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

- (void) hideKeyboard {
    
}

- (void)scrollTap:(UIGestureRecognizer*)gestureRecognizer {
    
    //make keyboard disappear , you can use resignFirstResponder too, it's depend.
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = true;
}
#pragma mark -
#pragma mark -  UINavigationItems & Action -



/* --------------------------------------------------------------------------------
 LEFT BUTTON ACTION
 -------------------------------------------------------------------------------- */

-(void)leftBtnTouched
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewSetUp
{
//    NSMutableAttributedString *temString=[[NSMutableAttributedString alloc]initWithString:self.termsBtn.titleLabel.text];
//    [temString addAttribute:NSUnderlineStyleAttributeName
//                      value:[NSNumber numberWithInt:1]
//                      range:(NSRange){0,[temString length]}];
//    [self.view layoutIfNeeded];
//    self.termsBtn.titleLabel.attributedText = temString;
    
   
    
    NSArray *arrTile=@[@"First name",@"Last name",@"Email",@"Mobile no",@"Password"];
    NSArray *arrValue=@[@"",@"",@"",@"",@""];
    NSArray *arrKeys=@[@"first_name",@"last_name",@"email",@"mobile_no",@"password"];
    for (int i=0; i<arrTile.count; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:arrValue[i] forKey:KValue];
        [dict setObject:arrTile[i] forKey:KPlaceHolder];
        [dict setObject:arrKeys[i] forKey:KKey];
        [arrRegistration addObject:dict];
    }
    
    [CommonFunctions setLeftPaddingToTextField:firstNameTxt andPadding:10];
    [CommonFunctions setLeftPaddingToTextField:lastNameTxt andPadding:10];
    [CommonFunctions setLeftPaddingToTextField:emailidTxt andPadding:10];
    [CommonFunctions setLeftPaddingToTextField:mobileNumberTxt andPadding:10];
    [CommonFunctions setLeftPaddingToTextField:passwordTxt andPadding:10];
    [CommonFunctions setLeftPaddingToTextField:referralCodeTxt andPadding:10];
//    [CommonFunctions setTextFieldPlaceHolderText:firstNameTxt withText:firstNameTxt.placeholder];
//    [CommonFunctions setTextFieldPlaceHolderText:lastNameTxt withText:lastNameTxt.placeholder];
//    [CommonFunctions setTextFieldPlaceHolderText:emailidTxt withText:emailidTxt.placeholder];
//    [CommonFunctions setTextFieldPlaceHolderText:mobileNumberTxt withText:mobileNumberTxt.placeholder];
//    [CommonFunctions setTextFieldPlaceHolderText:passwordTxt withText:passwordTxt.placeholder];
    
    firstNameTxt.keyboardType=UIKeyboardTypeDefault;
    firstNameTxt.autocapitalizationType =UITextAutocapitalizationTypeWords;
    firstNameTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    firstNameTxt.delegate = self;
    firstNameTxt.tag = 1;
    
    lastNameTxt.keyboardType=UIKeyboardTypeDefault;
    lastNameTxt.autocapitalizationType =UITextAutocapitalizationTypeWords;
    lastNameTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    lastNameTxt.delegate = self;

    
    emailidTxt.keyboardType=UIKeyboardTypeEmailAddress;
    emailidTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    emailidTxt.delegate = self;

    
    mobileNumberTxt.keyboardType=UIKeyboardTypeNumberPad;
    mobileNumberTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    mobileNumberTxt.delegate = self;
    mobileNumberTxt.tag = 101;

    
    passwordTxt.keyboardType=UIKeyboardTypeDefault;
    passwordTxt.secureTextEntry=YES;
    passwordTxt.delegate = self;
    
    
    referralCodeTxt.delegate = self;
    [signUpBtn addTarget:self action:@selector(btnSignUpClicked:) forControlEvents:UIControlEventTouchUpInside];
    referralCodeLabel.userInteractionEnabled = TRUE;
    [referralCodeLabel addGestureRecognizer:labelTapGesture];
    
}

// Gesture for label
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    referralCodeLabel.hidden = TRUE;
    referralCodeTxt.hidden = FALSE;
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    //Do stuff here...
}

-(IBAction)btnSignUpClicked:(id)sender
{
    NSString *strAlert=@"";
    
    if ([firstNameTxt.text isEqualToString:@""]) {
        //[firstNameTxt resignFirstResponder];
        
        [self alertView:@"Please enter first name."];
//        [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter first name."];
        
        
        [firstNameTxt becomeFirstResponder];

    }
   else if ([lastNameTxt.text isEqualToString:@""]) {
        //[firstNameTxt resignFirstResponder];
        
        [self alertView:@"Please enter last name."];
        //        [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter first name."];
        
        
        [firstNameTxt becomeFirstResponder];
        
    }
    else if ([emailidTxt.text isEqualToString:@""]||(![Validate isValidEmailId:emailidTxt.text])) {
        if ([emailidTxt.text isEqualToString:@""]) {
          //  [emailidTxt resignFirstResponder];
            [self alertView:@"Please enter an email id."];

//            [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter an email id."];
            [emailidTxt becomeFirstResponder];

        }
        else {
            //[emailidTxt resignFirstResponder];
            [self alertView:@"Please enter a valid email id."];

//            [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter a valid email id."];
            [emailidTxt becomeFirstResponder];
        }
    }
    else if([mobileNumberTxt.text isEqualToString:@""]|| (![Validate isValidMobileNumber:mobileNumberTxt.text]) || (mobileNumberTxt.text.length != 10)){
        if ([mobileNumberTxt.text isEqualToString:@""]) {
            //[mobileNumberTxt resignFirstResponder];
            [self alertView:@"Please enter your mobile number."];

//            [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter your mobile number."];
            [mobileNumberTxt becomeFirstResponder];

        }
        else {
            //[mobileNumberTxt resignFirstResponder];
            [self alertView:@"Please enter correct mobile number."];

//            [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter correct mobile number."];
            [mobileNumberTxt becomeFirstResponder];
        }
    }
    else if ((!_isSocialLogin && [passwordTxt.text isEqualToString:@""])|| (!_isSocialLogin && (passwordTxt.text.length < 6))|| (!_isSocialLogin && (passwordTxt.text.length >16))) {
        //passwordTxt
        //else if (passwordTxt.text isEqualToString:@""]||(![Validate isValidPassword:passwordTxt.text])) {
        if (!_isSocialLogin && [passwordTxt.text isEqualToString:@""]) {
            //[passwordTxt resignFirstResponder];
            [self alertView:@"Please enter your password."];

//            [CommonFunctions alertTitle:@"Error" withMessage:@"Please enter your password."];
            [passwordTxt becomeFirstResponder];

        }
        else {
            //[passwordTxt resignFirstResponder];
            [self alertView:@"Your password should contain 6 to 16 characters."];

//            [CommonFunctions alertTitle:@"Error" withMessage:@"Your password should contain 6 to 16 characters."];
            [passwordTxt becomeFirstResponder];

            //isValidPassword
        }
    }
    else if (!isDOBSelected)
    {
        [self alertView:@"Please select DOB"];

//        [CommonFunctions alertTitle:@"Error" withMessage:@"Please select DOB"];

    }
    else {
        
        NSLog(@"All values are valid , need to process login user api");
        if ([CommonFunctions reachabiltyCheck])
        {
        
            __weak RegistrationViewController *blockSelf = self;
            NSString *accessToken = [DEFAULTS objectForKey:K_AccessToken];
            
            if (accessToken.length > 0)
            {
                if (_isSocialLogin) {
                    NSMutableDictionary *body = [NSMutableDictionary new];
                    [body setValue:firstNameTxt.text forKey:@"fname"];
                    [body setValue:lastNameTxt.text forKey:@"lname"];
                    [body setValue:emailidTxt.text forKey:@"emailId"];
                    [body setValue:mobileNumberTxt.text forKey:@"mobileNo"];
                    [body setObject:@"ios" forKey:@"devicetype"];
                    
                    [body setValue:_socialM.type forKey:@"socialType"];
                    if (referralCodeTxt.text.length > 0 && referralCodeTxt.text != NULL) {
                        [body setObject:referralCodeTxt.text forKey:@"ReferralCode"];
                    }
                    
                    [body setObject:_btnDOB.titleLabel.text forKey:@"dob"];
                    if (_socialM.socialId != nil)
                    {
                        [body setObject:_socialM.socialId forKey:@"socialId"];
                    }
                    else{
                        [body setObject:@"12345678" forKey:@"socialId"];
                    }
                    
                    //[body setObject:_socialM.socialId forKey:@"socialId"];
                    [body setObject:[DEFAULTS objectForKey:@"devicetoken"] != nil ? [DEFAULTS objectForKey:@"devicetoken"]: @"" forKey:@"deviceid"];
                    [body setObject:subscribeStr forKey:@"subscribe"];
                   
                    
                    
                    if ([subscribeStr isEqualToString:@"1"])
                    {
                        [FIRAnalytics logEventWithName:@"Promo_Consent_Signup_YES" parameters:nil];
                    }
                    else
                    {
                        [FIRAnalytics logEventWithName:@"Promo_Consent_Signup_NO" parameters:nil];
                    }
                                        
                    [self showLoader];
                    
                    ApiClass *objApiClass = [[ApiClass alloc]init];
                    [objApiClass SocialSignInorUPWithDict:body strUrl:@"RestM1Registration_Social_new" completionHandler:^(NSDictionary<NSString *,id> * _Nullable dict)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD dismiss];
                        });
                        if (dict == nil)
                        {
                            [self alertView:KATryLater];

                            return;
                        }
                        signUpResponceModel *objSignUp = [[signUpResponceModel alloc]initWithDict:dict];
                        if (objSignUp.status == 1) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{

                            [[AppDelegate getSharedInstance] configDynamicShortcutItems];
                                [FIRAnalytics logEventWithName:@"sign_up" parameters:nil];

                                if ([self.fromScreen isEqualToString:@"From_Booking"]){
                                    [FIRAnalytics logEventWithName:@"signUp_Booking" parameters:nil];
                                
                                }
                                
                                
                            [DEFAULTS setObject:[CommonFunctions convertDate:[body objectForKey:@"dob"]] forKey:@"dob"];
                            [DEFAULTS setObject:objSignUp.response.userId forKey:KUSER_ID];
                            [DEFAULTS setObject:[body objectForKey:@"fname"] forKey:KUSER_FNAME];
                            [DEFAULTS setObject:[body objectForKey:@"lname"] forKey:KUSER_LNAME];
                            [DEFAULTS setValue:[NSString stringWithFormat:@"%@, %@",[body objectForKey:@"fname"],[body objectForKey:@"lname"]] forKey:KUSER_NAME];
                            [DEFAULTS setObject:[body objectForKey:@"mobileNo"] forKey:KUSER_PHONENUMBER];
                            //[DEFAULTS setObject:[body objectForKey:@"email"] forKey:KUSER_EMAIL];
                            [DEFAULTS setObject:[body objectForKey:@"emailid"] forKey:KUSER_EMAIL];
                            
//                            UserDefaults.standard.set(false, forKey: "socialLoginType")

                            if ([_socialM.type isEqualToString:@"fb"]) {
                                [DEFAULTS setBool:true forKey:@"socialLoginType"];

                            }else
                            {
                                [DEFAULTS setBool:true forKey:@"socialLoginType"];

                            }

                            
                            
                            [DEFAULTS setBool:true forKey:IS_LOGIN];
                            
//                               [self alertView:objSignUp.message];
                               
                               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Myles" message:objSignUp.message preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                   [self dismissViewControllerAnimated:NO completion:^{
                                       [self.delegate signUpComplete:true];

                                   }];

                               }];
                               [alert addAction:okAction];
                               [self presentViewController:alert animated:YES completion:nil];

                               
                            });
                            
                        }
                        else if (objSignUp.status == 0||objSignUp.status == 3||objSignUp.status == 4|| objSignUp.status == 5)
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Myles" message:objSignUp.message preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    if (!_isSocialLogin) {
                                        [self dismissViewControllerAnimated:NO completion:^{
                                        }];
                                    }
                                }];
                                [alert addAction:okAction];
                                [self presentViewController:alert animated:YES completion:nil];
                            });
                            
                        }
                    }];
                    
                }
                else
                [self registerUserAPI];
                
            }
            else
            {
                [self showLoader];
                AppDelegate *delegate = [AppDelegate getSharedInstance];
                
                [delegate getAccessTokenWithCompletion:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [blockSelf registerUserAPI];
                    });
                }];
            }
            
        }
        else
        {
            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        }

    }
    
}


-(BOOL)isValidEmail:(NSString *)string
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:string];
}

-(BOOL)validateNullValues {
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
     if (textField.tag == mobileNumberTxt.tag && textField.text.length+string.length>10)
    {
        NSLog(@"text length %lu",mobileNumberTxt.text.length);
        return false;

    }
     return true;
    
}


- (IBAction)textFieldDidBeginEditing:(UITextField *)sender
{
    if (sender == referralCodeTxt) {
        [self animateViewMoving:TRUE Value:200];
    } else {
        self.activeField = sender;
    }
}

- (IBAction)textFieldDidEndEditing:(UITextField *)sender
{
    if (sender == referralCodeTxt) {
        [self animateViewMoving:FALSE Value:200];
    } else {
        self.activeField = nil;
    }
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    // If you are using Xcode 6 or iOS 7.0, you may need this line of code. There was a bug when you
    // rotated the device to landscape. It reported the keyboard as the wrong size as if it was still in portrait mode.
    //kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
//    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
//    {
//        if ([[UIScreen mainScreen] bounds].size.height > 568)
//        {
//            CGRect aRect = self.view.frame;
//            aRect.size.height -= 240;
//            if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
//                [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
//            }
//        } else {
//            CGRect aRect = self.view.frame;
//            aRect.size.height -= kbRect.size.height;
//            if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
//                [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
//            }
//        }
//    }
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

-(void)animateViewMoving:(BOOL)up Value:(double)moveValue {
    NSTimeInterval movementDuration = 0.3;
    CGFloat movement = (up ? -moveValue : moveValue);
    [UIView beginAnimations:@"animateView" context:nil];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    [UIView setAnimationDuration:movementDuration];
    
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
//func animateViewMoving (up:Bool, moveValue :CGFloat){
//    let movementDuration:NSTimeInterval = 0.3
//    let movement:CGFloat = ( up ? -moveValue : moveValue)
//
//    UIView.beginAnimations("animateView", context: nil)
//    UIView.setAnimationBeginsFromCurrentState(true)
//    UIView.setAnimationDuration(movementDuration)
//
//    self.view.frame = CGRectOffset(self.view.frame, 0, movement)
//    UIView.commitAnimations()
//}
#pragma mark- Button Actions

#pragma mark -
#pragma mark - SERVER INTERACTION -
-(void)registerUserAPI
{

    NSString *md5Password=[passwordTxt.text MD5];
    NSArray *keysAr = @[@"fname",@"lname",@"emailid",@"phonenumber",@"password"];
    NSMutableDictionary *body = [NSMutableDictionary new];
    
    for (int i = 0; i < arrRegistration.count; i++)
    {
        NSMutableDictionary *dict = [arrRegistration objectAtIndex:i];
        [body setObject:[dict objectForKey:KValue] forKey:keysAr[i]];
        
    }
    [body setValue:firstNameTxt.text forKey:@"fname"];
    [body setValue:lastNameTxt.text forKey:@"lname"];
    [body setValue:emailidTxt.text forKey:@"emailid"];
    [body setValue:mobileNumberTxt.text forKey:@"phonenumber"];
    [body setValue:md5Password forKey:@"password"];
    [body setObject:@"ios" forKey:@"devicetype"];
    [body setObject:_btnDOB.titleLabel.text forKey:@"dob"];
    if (referralCodeTxt.text.length > 0 && referralCodeTxt.text != nil) {
        [body setObject:referralCodeTxt.text forKey:@"ReferralCode"];
    }
//    [body setObject:@"ios" forKey:@"devicetype"];
    [body setObject:[DEFAULTS objectForKey:@"devicetoken"] != nil ? [DEFAULTS objectForKey:@"devicetoken"]: @"" forKey:@"devicetoken"];
    
    
    [body setObject:subscribeStr forKey:@"subscribe"];
    
    if ([subscribeStr isEqualToString:@"1"])
    {
        [FIRAnalytics logEventWithName:@"Promo_Consent_Signup_YES" parameters:nil];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Promo_Consent_Signup_NO" parameters:nil];
    }
    
    
    NSLog(@"Post Boby =%@",body);
    
    NSURLSession *session = [CommonFunctions defaultSession];
    
    [self showLoader];
    
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSER_REGISTRATION completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                           {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD dismiss];
                                               });
                                               NSLog(@"url = %@",response.URL);
                                               NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                               //NSString *statusStr = [headers objectForKey:@"Status"];
                                               NSLog(@"headers = %@",headers);
                                               NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                               NSLog(@"response =%@",responseJson);

                                               
                                               NSString *statusStr = [NSString stringWithFormat:@"%@",[responseJson valueForKey:@"status"]];

                                               if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                               {
                                                   NSLog(@"response =%@",response);
                                                   if ([[responseJson objectForKey:@"status"] integerValue]) {
                                                       if (!error && data != nil)
                                                       {
                                                           NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                           
                                                           
                                                           NSString *message  = @"";
                                                           if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 1)
                                                           {
                                                               [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                               //[DEFAULTS setObject:[body objectForKey:@"password"] forKey:KUSER_PASSWORD];
                                                               [FIRAnalytics logEventWithName:@"sign_up" parameters:nil];
                                                               if ([self.fromScreen isEqualToString:@"From_Booking"]){
                                                                   [FIRAnalytics logEventWithName:@"signUp_Booking" parameters:nil];
                                                                   
                                                               }
                                                               [CommonFunctions writeToKeyChainUserName:[body objectForKey:@"phonenumber"] Password:[body objectForKey:@"password"]];
                                                               [CommonFunctions searchKeyChainForUserName:[body objectForKey:@"phonenumber"] :^(NSString *password) {
                                                                   
                                                               }];
                                                               
                                                               [DEFAULTS setObject:[CommonFunctions convertDate:[body objectForKey:@"dob"]] forKey:@"dob"];
                                                               
                                                               [DEFAULTS setObject:[body objectForKey:@"fname"] forKey:KUSER_FNAME];
                                                               [DEFAULTS setObject:[body objectForKey:@"lname"] forKey:KUSER_LNAME];
                                                               [DEFAULTS setValue:[NSString stringWithFormat:@"%@, %@",[body objectForKey:@"fname"],[body objectForKey:@"lname"]] forKey:KUSER_NAME];
                                                               [DEFAULTS setObject:[body objectForKey:@"phonenumber"] forKey:KUSER_PHONENUMBER];
                                                               //[DEFAULTS setObject:[body objectForKey:@"email"] forKey:KUSER_EMAIL];
                                                               [DEFAULTS setObject:[body objectForKey:@"emailid"] forKey:KUSER_EMAIL];
                                                               [DEFAULTS synchronize];
                                                               
                                                               message = [(NSMutableDictionary *)responseJson objectForKey:@"message"];
                                                               
                                                               [DEFAULTS setBool:true forKey:IS_LOGIN];
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   
                                                                   //                                                               [self alertView:message];
                                                                   [[AppDelegate getSharedInstance] configDynamicShortcutItems];
                                                                   
                                                                   [self dismissViewControllerAnimated:NO completion:^{
                                                                       [self.delegate signUpComplete:true];
                                                                   }];
                                                                   
                                                                   
                                                               });
                                                               
                                                           }
                                                           else if ([[(NSMutableDictionary *)responseJson objectForKey:@"status"] integerValue] == 2){
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RegistrationProcess" bundle:[NSBundle mainBundle]];
                                                                   MobileVerificationViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"mobileIdentifier"];
                                                                   [self.navigationController pushViewController:controller animated:YES];
                                                               });
                                                           }
                                                           else
                                                           {
                                                               if ([[responseJson valueForKey:k_ApiMessageKey] isEqualToString:@"Invalid MobileNo"]){
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       
                                                                       [CommonFunctions alertTitle:@"MYLES" withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                                   });
                                                                   
                                                               }else{
                                                                   [DEFAULTS setObject:[[responseJson objectForKey:@"response"] objectForKey:@"userId"] forKey:KUSER_ID];
                                                                   //[DEFAULTS setObject:[body objectForKey:@"password"] forKey:KUSER_PASSWORD];
                                                                   [DEFAULTS synchronize];
                                                                   
                                                                   message = KARegistrationMessage;
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [self.activeField resignFirstResponder];
                                                                       [self.navigationController popToRootViewControllerAnimated:YES];
                                                                   });
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                        [self alertView:[responseJson valueForKey:k_ApiMessageKey]];
//                                                                       [CommonFunctions alertTitle:nil withMessage:[responseJson valueForKey:k_ApiMessageKey]];
                                                                       
                                                                   });
                                                               }
                                                               
                                                               
                                                           }
                                                       }
                                                       
                                                       
                                                   } else {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           [self alertView:[responseJson valueForKey:k_ApiMessageKey]];
                                                           
                                                           
                                                       });
                                                   }
                                                  

                                               } else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                   NSLog(@"AccessToken Invalid");
                                                   
                                               } else
                                               {
                                                   
                                               }
                                               
                                               
                                           }];
    
    [globalOperation addOperation:registrationOperation];
    
    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && alertView.tag == 1)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RegistrationProcess" bundle:[NSBundle mainBundle]];
        MobileVerificationViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"mobileIdentifier"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}



- (IBAction)btnTermsAndConditionClicked:(id)sender
{
    NSLog(@"Btn terms and condition clicked");
}

- (IBAction)btnSignInClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnRedeemClicked:(UIButton *)sender
{
    
    //Message required
    [CommonFunctions alertTitle:nil withMessage:@"Coming Soon!"];
    
}
/**
 Select Date of Birth
 */
- (IBAction)btnDOBEvent:(id)sender
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n"
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIDatePicker *startTimePicker = [[UIDatePicker alloc] init];
    
    [startTimePicker setDatePickerMode:UIDatePickerModeDate];
    if (@available(iOS 14.0, *))
    {
        [startTimePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels];
        //UIDatePickerStyleInline
    }
    else
    {
        // Fallback on earlier versions
    }
    startTimePicker.minuteInterval=30;
    startTimePicker.timeZone = [NSTimeZone localTimeZone];
    [startTimePicker setMaximumDate: [NSDate date]];
    [alert.view addSubview:startTimePicker];
    [alert addAction:({
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            isDOBSelected  = true;
            [self.btnDOB setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.btnDOB setTitle:[self IndianTimeConverter:startTimePicker.date] forState:UIControlStateNormal];
            
        }];
        action;
    })];
    
    [alert addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            NSLog(@"cancel");
        }];
        action;
    })];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)alertView :(NSString *)strMessage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Myles" message:strMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    });
    
}

/**
 @brief Convert nsdate to string
 @dev Abhishek singh
 */
-(NSString *)IndianTimeConverter : (NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
    dateFormatter1.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [dateFormatter1 stringFromDate:date];
    return strDate;
}

- (IBAction)backEvent:(id)sender {
    [self.navigationController dismissViewControllerAnimated:true completion:^{
        
    }];
}

- (IBAction)subscribeButtonAction:(UIButton*)sender {
    
    sender.selected = !sender.selected;
    
    if (sender.selected)
    {
        subscribeStr = @"1";
    }
    else
    {
        subscribeStr = @"0";
    }
    
    
}
@end
