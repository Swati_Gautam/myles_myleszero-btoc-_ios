//
//  OtpViewController.swift
//  Myles
//
//  Created by Itteam2 on 17/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD

@objc protocol AfterNumberVerify {
    @objc func callSignupPage(isSignUp :Bool)
}
@objc class OtpViewController: UIViewController,UITextFieldDelegate {
    @objc var strMobileNumber = String()
    @objc public weak var delegate : AfterNumberVerify?
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var txt: UITextField!
    
    @objc var fromScreen = String()
    @IBOutlet weak var hCenter: NSLayoutConstraint!

    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl1.clipsToBounds = true
        
        addBoder(changeColor: true)
        // Do any additional setup after loading the view.
        
        let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let yourOtherAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)]
        
        let partOne = NSMutableAttributedString(string: "We have just sent a code to\n", attributes: yourAttributes)
        let partTwo = NSMutableAttributedString(string: strMobileNumber, attributes: yourOtherAttributes)
        
        let combination = NSMutableAttributedString()
        combination.append(partOne)
        combination.append(partTwo)
        
        headerLbl.attributedText = combination
        headerLbl.numberOfLines = 0
        headerLbl.sizeToFit()
        headerLbl.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        txt.becomeFirstResponder()

    }
    func addBoder(changeColor: Bool )  {
        let rightBorder: CALayer = CALayer(layer: lbl1.layer)
        rightBorder.borderColor = changeColor ? UIColor.darkGray.cgColor : UIColor.red.cgColor
        rightBorder.borderWidth = 2
        rightBorder.frame = CGRect(x: -1, y: lbl1.frame.size.height-2, width: lbl1.frame.width, height: 2)
        lbl1.textColor = changeColor ? UIColor.darkGray : UIColor.red
        lbl1.layer.addSublayer(rightBorder)
        
        lbl2.clipsToBounds = true
        let rightBorder2: CALayer = CALayer(layer: lbl2.layer)
        rightBorder2.borderColor = changeColor ? UIColor.darkGray.cgColor : UIColor.red.cgColor
        rightBorder2.borderWidth = 2
        rightBorder2.frame = CGRect(x: -1, y: lbl2.frame.size.height-2, width: lbl2.frame.width, height: 2)
        lbl2.textColor = changeColor ? UIColor.darkGray : UIColor.red
        lbl2.layer.addSublayer(rightBorder2)
        
        lbl3.clipsToBounds = true
        let rightBorder3: CALayer = CALayer(layer: lbl3.layer)
        rightBorder3.borderColor = changeColor ? UIColor.darkGray.cgColor : UIColor.red.cgColor
        rightBorder3.borderWidth = 2
        rightBorder3.frame = CGRect(x: -1, y: lbl3.frame.size.height-2, width: lbl3.frame.width, height: 2)
        lbl3.textColor = changeColor ? UIColor.darkGray : UIColor.red
        lbl3.layer.addSublayer(rightBorder3)
        
        lbl4.clipsToBounds = true
        let rightBorder4: CALayer = CALayer(layer: lbl4.layer)
        rightBorder4.borderColor = changeColor ? UIColor.darkGray.cgColor : UIColor.red.cgColor
        rightBorder4.borderWidth = 2
        rightBorder4.frame = CGRect(x: -1, y: lbl4.frame.size.height-2, width: lbl4.frame.width, height: 2)
        lbl4.textColor = changeColor ? UIColor.darkGray : UIColor.red
        lbl4.layer.addSublayer(rightBorder4)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
     {
        let newLength:Int = (textField.text?.characters.count)! + string.characters.count - range.length
        
        if newLength < 5
        {
            if newLength == 0
            {
                lbl1.text=nil;
                lbl2.text=nil;
                lbl3.text=nil;
                lbl4.text=nil;
            }
            else if newLength == 1 && string.characters.count > 0
            {
                lbl1.text=string;
                lbl2.text=nil;
            }
            else if (newLength==2 && string.characters.count>0)
            {
                lbl2.text=string;
                lbl3.text=nil;
            }
            else if (newLength==3 && string.characters.count>0)
            {
                lbl3.text=string;
                lbl4.text=nil;
            }
                
            else if (newLength==4 && string.characters.count>0)
            {
                lbl4.text=string;
                
                verifyNumberOrOTP(strOtp: txt!.text! + string)
               
               
            }
            else
            {
                switch (newLength) {
                case 1:
                    lbl2.text=nil;
                    break;
                case 2:
                    lbl3.text=nil;
                    break;
                case 3:
                    
                        self.lbl4.text=nil;
                        self.addBoder(changeColor: true)

                    
                    break;
                    
                default:
                    break;
                }
            }
            return true
        }
        return false
        
        
        
     }
   
@IBAction func cancelOtp()
{
    txt.resignFirstResponder()
    DispatchQueue.main.async {
    self.dismiss(animated: true, completion: nil)
    }
}
    
@IBAction func resendOtp()
{
    showLoader()
    CommonApiClass.checkUserExistorNot(strMobileNumber, { [ weak weakSelf = self](dict,isValidToken) in
        DispatchQueue.main.async {
        SVProgressHUD.dismiss()
        }
        guard dict != nil else{
            return
        }
    })
}
func verifyNumberOrOTP(strOtp: String)
{
    let apiClass = ApiClass()
    var dictData = [String:String]()
    dictData["OTP"] = strOtp
    dictData["mobileNo"] = strMobileNumber
    
    apiClass.verifyOtp(dict: dictData) { [weak weakSelf = self] (isSuccess,isValidToken,isInternet) in
        print(isSuccess)
        
        
            guard isValidToken else
            {
                DispatchQueue.main.async {
                    CommonFunctions.getAccessToken(completion: {
                    })//
                }
                return
            }
        
        
        if isSuccess
        {
             DispatchQueue.main.async {
                //firebase event
                if self.fromScreen == "From_Booking"{
                    self.BookingOtpVerifySubmit()
                }
            weakSelf?.delegate?.callSignupPage(isSignUp: true)
            weakSelf?.cancelOtp()
        

            }
        }
        else{
            DispatchQueue.main.async {
                
            if isInternet
            {
            weakSelf?.addBoder(changeColor: false)
            UIView.animate(withDuration: 0.07, animations: {
                //                    first time
                weakSelf?.hCenter.constant = +5
                weakSelf?.stackView.layoutIfNeeded()
                
            }, completion: {
                
                (value: Bool) in
               

                if value
                {
                    UIView.animate(withDuration: 0.07, animations: {
                        weakSelf?.hCenter.constant = -10
                        weakSelf?.stackView.layoutIfNeeded()
                        
                        
                    }, completion: {
                        
                        (value: Bool) in
                        if value
                        {
                            
                            UIView.animate(withDuration: 0.07, animations: {
                                weakSelf?.hCenter.constant = 0
                                weakSelf?.stackView.layoutIfNeeded()
                                //
                                
                            }, completion: nil)
                        }
                        
                        //                        self.hCenter.isActive = true;
                        
                    })
                    
                    
                }
               
                
            })
          }else
            {
                
            }
        }
        }
    }
    
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
