//
//  ForgotViewController.h
//  Myles
//
//  Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

@interface ForgotViewController : BaseViewController
{
    
}

// Actions
- (IBAction)btnSendClicked:(id)sender;
- (IBAction)btnReSendClicked:(id)sender;



@end
