//
//  TermsAndConditionViewController.m
//  Myles
//
//  Created by Myles on 30/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "TermsAndConditionViewController.h"
#import <Google/Analytics.h>
#import <WebKit/WebKit.h>

@interface TermsAndConditionViewController ()
{
    //IBOutlet UIWebView *webViewTermAndCondition;
}
@end

@implementation TermsAndConditionViewController

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)viewDidLoad {
    self.navigationController.navigationBar.hidden = false;

    [super viewDidLoad];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Terms And Conditions"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    // Do any additional setup after loading the view.
    
    
    //set navigation title
    //    self.title=@"Terms and conditions";
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    //[self showLoader];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Terms And Conditions"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [super viewWillAppear:animated];
    [self viewSetUp];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
//-(void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBarHidden=YES;
//}

-(void)viewSetUp
{
    
    //[self customizeNavigationBar:(self.type == 0)?@"Terms And Conditions":@"MYLES AGREEMENT" Present:false];
    
    _wkWebViewTerms.frame = CGRectMake(self.view.frame.origin.x + 25,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:_wkWebViewTerms];
    
    
    if (self.type == 1)
    {
        [self BookingTAndC];
        
        
        
    }
    else
    {
        /*NSURL *url=[NSURL URLWithString:@"https://www.mylescars.com/pages/terms_of_use_app"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [webViewTermAndCondition loadRequest:request];*/
        
        NSURL *url = [NSURL URLWithString:@"https://www.mylescars.com/pages/terms_of_use_app"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        //_wkWebView = [[WKWebView alloc] initWithFrame:self.view.frame];
        [_wkWebViewTerms loadRequest:request];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*- (void)webViewDidStartLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showLoader];
    });
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}*/

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showLoader];
    });
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

#pragma mark -
#pragma mark - SERVER INTERACTION -
-(void)BookingTAndC
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSURL *requestUrl  = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.mylescars.com/rests/getPaymentTerms/%@",kBookingTermsAndConditions]];
        
        NSData *data = [NSData dataWithContentsOfURL:requestUrl];
        
        if (data)
        {
            NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSString *content = [[(NSArray *)[responseJson objectForKey:@"ResStatus"] lastObject] objectForKey:@"content"];
            
            NSString *htmlString = [NSString stringWithFormat:@"<html><head></head><body><p>%@</p></body></html>",content];
            
            //[webViewTermAndCondition loadHTMLString: htmlString baseURL: nil];
            [_wkWebViewTerms loadHTMLString:htmlString baseURL:nil];
        }
        else
        {
            [CommonFunctions alertTitle:KSorry withMessage:KAInternet];
        }
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:KAInternet];
    }
}


@end
