//
//  NextAvailableCell.h
//  Myles
//
//  Created by iOS Team on 21/08/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarDetailInformation.h"

@interface NextAvailableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *nextAvailableTimingView;
@property (weak, nonatomic) IBOutlet UIView *pickUpView;
@property (weak, nonatomic) IBOutlet UILabel *pickUpDateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeDifferenceLabel;

@property (weak, nonatomic) IBOutlet UIView *dropOffView;
@property (weak, nonatomic) IBOutlet UILabel *dropOffDateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropOffTimeDifferenceLabel;

@property (weak, nonatomic) IBOutlet UIView *mainContentView;

@property (weak, nonatomic) IBOutlet UIImageView *carImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *carImageIndicator;

@property (weak, nonatomic) IBOutlet UILabel *lblCarVariantName;
@property (weak, nonatomic) IBOutlet UILabel *carNameLabel;
@property (weak, nonatomic) IBOutlet UIView *carPropertiesMainView;
@property (weak, nonatomic) IBOutlet UILabel *seaterLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelLabel;
@property (weak, nonatomic) IBOutlet UILabel *freeLabel;
@property (weak, nonatomic) IBOutlet UILabel *kMFreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *perKMPriceLabel;

@property (weak, nonatomic) IBOutlet UIView *chooseSiteLocationView;
@property (weak, nonatomic) IBOutlet UILabel *chooseSiteLocationLabel;

@property (weak, nonatomic) IBOutlet UIButton *bookNowButton;
@property (weak, nonatomic) IBOutlet UILabel *priceAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedGovernerLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *upperHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *seatingCapacityLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelPackageLabel;
@property (weak, nonatomic) IBOutlet UILabel *fuelTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *carTypeLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) CarDetailInformation *carDetails;
@property (copy, nonatomic) void (^didTapCollectionViewBlock)(NSInteger index , BOOL isSelected);
@property (weak, nonatomic) IBOutlet UILabel *additionalPaymentLabel;

@property (weak, nonatomic) IBOutlet UIView *viewPromoCode;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoCode;

@property (weak, nonatomic) IBOutlet UILabel *indicatedPriceLabel;


@end
