//
//  NextAvailableCell.m
//  Myles
//
//  Created by iOS Team on 21/08/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

#import "NextAvailableCell.h"
#import "CarPackageCollectionViewCell.h"
@implementation NextAvailableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.viewPromoCode.layer.cornerRadius = 4.0f;
    self.viewPromoCode.layer.borderColor = UIColor.redColor.CGColor;
    self.viewPromoCode.layer.borderWidth = 1.0;
    
    _mainContentView.layer.cornerRadius = 4.0f;
   // _mainContentView.layer.borderWidth = 1.0f;
    //_mainContentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
   // _chooseSiteLocationView.layer.borderWidth = 1.0f;
   // _chooseSiteLocationView.layer.borderColor = [UIColor grayColor].CGColor;
   // _chooseSiteLocationView.layer.cornerRadius = 4.0f;
    _bookNowButton.layer.cornerRadius = 2.0f;
    _bookNowButton.layer.borderColor = UICOLOR_ORANGE.CGColor;
    _bookNowButton.layer.borderWidth = 1.0f;
    //_mainContentView.layer.shadowColor = [UIColor blackColor].CGColor;
    //_mainContentView.layer.shadowRadius = 2.0f;
    //_mainContentView.layer.shadowOpacity = 0.5;
    //_mainContentView.layer.shadowOffset = CGSizeMake(-0.2f,-0.2f);
   // _mainContentView.layer.masksToBounds = NO;
    [_chooseSiteLocationLabel sizeToFit];
    
    
    _chooseSiteLocationView.layer.cornerRadius = 2.0f;
    _chooseSiteLocationView.layer.borderWidth = 1.0f;
    _chooseSiteLocationView.layer.borderColor = [UIColor grayColor].CGColor;
    
    _nextAvailableTimingView.layer.cornerRadius = 2.0f;
    _nextAvailableTimingView.layer.borderWidth = 1.0f;
    _nextAvailableTimingView.layer.borderColor = [UIColor grayColor].CGColor;
//    _timeDifferenceLabel.layer.cornerRadius = 1.0f;
//    _timeDifferenceLabel.layer.borderWidth = 0.5f;
//    _timeDifferenceLabel.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:_timeDifferenceLabel.bounds cornerRadius:2].CGPath;
//    _timeDifferenceLabel.layer.shouldRasterize = YES;
////    _timeDifferenceLabel.layer.rasterizationScale = Uiscre
////    _timeDifferenceLabel.layer.shadowColor = [UIColor darkGrayColor].CGColor;
////    _timeDifferenceLabel.layer.shadowOpacity = 0.8;
////    _timeDifferenceLabel.layer.shadowOffset = CGSizeMake(2,2);
    [self.collectionView registerNib:[UINib nibWithNibName:@"CarPackageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PackageCollectionCell"];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (!_carDetails.AllPackages || !_carDetails.AllPackages.count){
        
        return 0;

    }
    else{
        if(_carDetails.AllPackages.count > 0){
            return _carDetails.AllPackages.count;

        }
        else{
            return 0;
        }
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

- (CarPackageCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CarPackageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PackageCollectionCell" forIndexPath:indexPath];
    CarPackageDetails *packageDetails = _carDetails.AllPackages[indexPath.row] ;
    NSString *displayRate = [NSString stringWithFormat:@"%@",packageDetails.IndicatedPrice];
    float rate = [displayRate doubleValue];
    int roundedRate = roundf(rate);
    NSString *displaykilometer = [NSString stringWithFormat:@"%@",packageDetails.KMIncluded];
    float kilometer = [displaykilometer doubleValue];
//    if(kilometer == 0){
//        cell.kilometerLabel.text = packageDetails.PkgDescription;
//
//    }
//    else{
//        cell.kilometerLabel.text = [NSString stringWithFormat:@"%@ Kms Then ₹%@",packageDetails.KMIncluded,packageDetails.ExtraKMRate];
//
//    }
    cell.kilometerLabel.text = packageDetails.PkgDescription;
    cell.priceLabel.text = [NSString stringWithFormat:@"₹ %d",roundedRate];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //CarPackageCollectionViewCell *cell = (CarPackageCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
   
    self.didTapCollectionViewBlock(indexPath.row , TRUE);

}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    self.didTapCollectionViewBlock(indexPath.row , FALSE);


}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//
//    return [(NSString*)[arrayOfStats objectAtIndex:indexPath.row] sizeWithAttributes:NULL];
//}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        return CGSizeMake(CGRectGetWidth(collectionView.frame) / 2.5, (CGRectGetHeight(collectionView.frame)));
    } else {
        return CGSizeMake(CGRectGetWidth(collectionView.frame) / 3, (CGRectGetHeight(collectionView.frame)));
    }
    

}
@end
