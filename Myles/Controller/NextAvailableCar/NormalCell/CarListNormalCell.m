//
//  CarListNormalCell.m
//  Myles
//
//  Created by iOS Team on 11/08/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

#import "CarListNormalCell.h"

@implementation CarListNormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _mainContentView.layer.cornerRadius = 4.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
