//
//  TNCController.swift
//  Myles
//
//  Created by Chetan Rajauria on 08/08/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import UIKit
import SVProgressHUD
import WebKit


class TNCController: UIViewController, WKNavigationDelegate { //UIWebViewDelegate {

    //@IBOutlet weak var webView: UIWebView!
    var webNewView : WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webNewView = WKWebView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 20, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(webNewView)
        
        //tncID
        
        
        
        /*
        [FIRAnalytics logEventWithName:@"View_MS_TNC" parameters:nil];
        
        UIView * backView = [[UIView alloc] initWithFrame:CGRectMake(25, 75, self.view.frame.size.width-50, self.view.frame.size.height-150)];
        [backView setBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
        [backView setTag:999999];
        [self.view addSubview:backView];
        
        [self showLoader];
        
        UIWebView * webView = [[UIWebView alloc] initWithFrame:CGRectMake(5, 5, backView.frame.size.width-10, backView.frame.size.height-10)];
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://qa2.mylescars.com/bookings/myles_secure?source=app"]]];
        [webView setDelegate:(id)self];
        [backView addSubview:webView];
        
        UIButton * closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeBtn setFrame:CGRectMake(webView.frame.size.width-60, 10, 50, 20)];
        [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(removeWebView) forControlEvents:UIControlEventTouchUpInside];
        [closeBtn setTitleColor:[UIColor colorWithRed:18/255.0 green:106/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];
        [webView addSubview:closeBtn];
        */
        
        

    }
    
    /*func webViewDidStartLoad(_ webView: UIWebView) {
        print("webViewDidStartLoad")

    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        print("didFailLoadWithError")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        print("webViewDidFinishLoad")
        self.dismissLoader()
    }*/
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!)
    {
        debugPrint("didCommit")
        DispatchQueue.main.async(execute: {
            self.dismissLoader()
        })
    }
    
    /*func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
     debugPrint("didFinish")
     }*/
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        debugPrint("didFail")
        DispatchQueue.main.async(execute: {
            self.dismissLoader()
        })
    }
    
    
    func webView(_ webView: WKWebView, didFinish  navigation: WKNavigation!)
    {
        DispatchQueue.main.async(execute: {
            self.dismissLoader()
        })
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.showLoader()
        self.webNewView.load(URLRequest.init(url: URL(string: "https://www.mylescars.com/bookings/myles_secure?source=app")!))
        self.webNewView.navigationDelegate = self
    }

    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }

    
    func dismissLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
