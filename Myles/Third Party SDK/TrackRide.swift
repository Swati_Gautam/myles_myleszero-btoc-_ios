//
//  ViewController.swift
//  Maptest
//
//  Created by Jimmy Jose on 18/08/14.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


import UIKit
import MapKit
//#import <Google/Analytics.h>



class TrackRide: BaseViewController,MKMapViewDelegate , CLLocationManagerDelegate,LocationDelegate
{
    
    @IBOutlet var mapView:MKMapView?

    @IBOutlet weak var trackAddressLabel: UILabel!
    @IBOutlet weak var lockunlockBtn: UIButton!
    var LockUnlockBoolState = 0
    
    var mapManager : MapManager? = MapManager()
    var destinationLocation = CLLocation()
    var currentLocation  = CLLocation()
    var locationManager: CLLocationManager!
    var lockunlockResponse : LockUnlockResponse?
    var trackaddress = NSString()
    var bookingId = NSString()
    var tripStatus = NSString()
    var MylesAddress = NSString()
    var  bookingIdCheckListStatus : NSString?
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func  viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden =  false;
        
        /*
        GAI.sharedInstance().
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: name)
        let builder = GAIDictionaryBuilder.createScreenView()
          tracker.send(builder.build() as [NSObject : AnyObject])
          */

        
        self.mapView?.delegate = self

        //set zoom level of the map to reduce data download
       // self.mapView.camera = .cameraWithTarget(target, zoom:6)
        
        self.trackAddressLabel.text = self.MylesAddress as String
        
        //self.LockUnlockBoolState
        //self.LockUnlockBoolState.layer.masksToBounds = YES;
        //self.LockUnlockBoolState.layer.cornerRadius = 3.0;
        self.lockunlockBtn.layer.masksToBounds = true ;
        self.lockunlockBtn.layer.cornerRadius = 3.0;

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if self.mapManager == nil{
            
            self.mapManager = MapManager()
        }
        
        self.mapView?.delegate = self
        
        self.customizeNavigationBar("TRACK MY RIDE", present: false)
        self.fetchCarLocationAndStatus()
        self.lockunlockBtn.setTitle("UNLOCK RIDE", for: UIControlState())
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.lockunlockResponse = nil
        locationManager = nil
        self.mapManager = nil
        self.mapView?.delegate = nil;
    }
    
    
    func mapViewWillStartLocatingUser(_ mapView: MKMapView)
    {
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.orange
            polylineRenderer.lineWidth = 5
            print("done", terminator: "")
            return polylineRenderer
        }
        return MKPolylineRenderer(overlay: overlay)
    }
    
    func setSourceAndDestinationLocation (_ destination : CLLocation , address : NSString , Id : NSString) -> Void
    {
        let appdelegate = AppDelegate.getSharedInstance()
        let locationAllowed : Bool = AppDelegate.locationAuthorized()
        self.destinationLocation = destination
        self.trackaddress = address;
        self.bookingId = Id;
        
        if (self.tripStatus == "New")
        {
            if(appdelegate.currentlocation != nil)
            {
                self.currentLocation = appdelegate.currentlocation;
            }
            else if (!locationAllowed)
            {
                let alert = UIAlertView(title: "Message", message: "Unable to find your current location. Please turn on the location services from Settings and try again.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
            }
                self.getPathforTrack()
        }
        else if(self.currentLocation.coordinate.latitude != 0.0)
        {
            self.getPathforTrack()
        }
        else
        {
            let alert = UIAlertView(title: "Message", message: "Unable to find your current location. Please turn on the location services from Settings and try again.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
        }
    }
    
    @IBAction func getPathforTrack()
    {
        
        let currentTestLocation = currentLocation.coordinate
        print(currentTestLocation)
        
        let currentTmpDestination = destinationLocation.coordinate
        print(currentTmpDestination)
        
      self.view.endEditing(true)
      if (CommonFunctions.reachabiltyCheck())
        {
            GMDCircleLoader.setOnView(self.view , withTitle: "Please wait...", animated: true)
            mapManager!.directionsUsingGoogle(from: currentLocation.coordinate, to: destinationLocation.coordinate) { (route, directionInformation, boundingRegion, error) -> () in
            
            DispatchQueue.main.async {
            
            if(error != nil){
                GMDCircleLoader.hideFromView(self.view, animated:true)
                print(error, terminator: "")
            }else{
                GMDCircleLoader.hideFromView(self.view, animated:true)
                let pointOfOrigin = MKPointAnnotation()
                
                pointOfOrigin.coordinate = route!.coordinate
                pointOfOrigin.title = directionInformation?.object(forKey: "start_address") as! NSString as String
                pointOfOrigin.subtitle = "S"//directionInformation?.objectForKey("duration") as! NSString as String
                
                let pointOfDestination = MKPointAnnotation()
                pointOfDestination.coordinate = route!.coordinate
                pointOfDestination.title = directionInformation?.object(forKey: "end_address") as! NSString as String
                pointOfDestination.subtitle = directionInformation?.object(forKey: "distance") as! NSString as String
                
                let start_location = directionInformation?.object(forKey: "start_location") as! NSDictionary
                let originLat = (start_location.object(forKey: "lat") as AnyObject).doubleValue
                let originLng = (start_location.object(forKey: "lng") as AnyObject).doubleValue
                
                let end_location = directionInformation?.object(forKey: "end_location") as! NSDictionary
                let destLat = (end_location.object(forKey: "lat") as AnyObject).doubleValue
                let destLng = (end_location.object(forKey: "lng") as AnyObject).doubleValue
                
                let coordOrigin = CLLocationCoordinate2D(latitude: originLat!, longitude: originLng!)
                let coordDesitination = CLLocationCoordinate2D(latitude: destLat!, longitude: destLng!)
                
                pointOfOrigin.coordinate = coordOrigin
                pointOfDestination.coordinate = coordDesitination
                
                if let web = self.mapView{
                    DispatchQueue.main.async {
                        self.removeAllPlacemarkFromMap(shouldRemoveUserLocation: true)
                        web.add(route!)
                        web.addAnnotation(pointOfOrigin)
                        web.addAnnotation(pointOfDestination)
                        web.setVisibleMapRect(boundingRegion!, animated: true)
                    }
                }
            }
        }
        }
        }
    else
    {
        CommonFunctions.alertTitle("Error", withMessage: "No Network available. Please connect and try again.");
    }

    
    }

    //MARK- 
     func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        let origin = MKAnnotationView(annotation: annotation, reuseIdentifier: "StartPoint")
        if (annotation.subtitle! == "S")
        {
            
            origin.image = UIImage(named:"blackdot")
        }
        else
        {
            origin.image = UIImage (named:"destinationicon")
        }
        return origin

    }
    
    func locationManager(_ manager: CLLocationManager,
        didChangeAuthorization status: CLAuthorizationStatus) {
            var hasAuthorised = false
            var locationStatus:NSString = ""
            let verboseKey = status
            
            switch verboseKey {
            case CLAuthorizationStatus.restricted:
                locationStatus = "Restricted Access"
            case CLAuthorizationStatus.denied:
                locationStatus = "Denied access"
            case CLAuthorizationStatus.notDetermined:
                locationStatus = "Not determined"
            default:
                locationStatus = "Allowed access"
                hasAuthorised = true
            }
            
            if(hasAuthorised == true){
                
                
            }else {
                
                print("locationStatus \(locationStatus)", terminator: "")
                
            }
    }
    
    func removeAllPlacemarkFromMap(shouldRemoveUserLocation:Bool){
        
        if let mapView = self.mapView {
            for annotation in mapView.annotations{
                if shouldRemoveUserLocation {
                    if annotation as? MKUserLocation !=  mapView.userLocation {
                        mapView.removeAnnotation(annotation )
                    }
                }
            }
        }
    }
    
    
    // Method used to Lock/Unloack car
    
    
    
  @IBAction  func UnlockRideBtn(_ sender : UIButton)
  {
    
    
    // Checklist is already submiited then only lock/unlock api will be called
    if self.LockUnlockBoolState == 1
    {
        let status = self.lockunlockResponse?.status as NSString?

        
        if status?.integerValue == 0
        {
            CommonFunctions.alertTitle("Message", withMessage:  self.lockunlockResponse?.message)

        }
        else
        {
            self.lockUnlockCheck(self.bookingId, sender: sender)
        }
    }
        
        // Switching to checklist screen if not submitted yet
    else
    {
        
        if self.tripStatus == "Open"
        {
            
            let checklist = self.storyboard?.instantiateViewControllerWithIdentifier("checklistcontroller") as? CheckListVC
            
            checklist?.getTabledata(bookingId as String)
            self.navigationController?.pushViewController(checklist!, animated: true)
        }
        else
        {
            let alert = UIAlertView(title: "Message", message: "Key less lock/unlock feature is not available.", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
    }
  }
    
    // Get device location and checklist status
    func fetchCarLocationAndStatus ()-> Void
    {
        if (CommonFunctions.reachabiltyCheck())
        {
            GMDCircleLoader.setOnView(self.view , withTitle: "Please wait...", animated: true)
            
            [CommonApiClass .ChecklockUnlockApi(self.bookingId as String, completionHandler: { (data, response, error) -> Void in
                
                DispatchQueue.main.async (execute: { () -> Void in
                    
                    if (error == nil)
                    {
                        do
                        {
                            let appdelegate = AppDelegate.getSharedInstance()

                            self.lockunlockResponse =  try LockUnlockResponse (data: data)
                            
                            let status = self.lockunlockResponse?.status as NSString?
                            
                            self.LockUnlockBoolState = ((self.lockunlockResponse?.checkliststatus as NSString?)?.integerValue)!
                            
                            self.trackaddress = self.MylesAddress
                            
                            
                            if status?.integerValue == 1
                            {
                                if self.tripStatus == "Open"
                                {
                                    self.currentLocation = CLLocation(latitude: ((self.lockunlockResponse?.lat)! as NSString).doubleValue, longitude: ((self.lockunlockResponse?.lon)! as NSString).doubleValue)
                                    
                                    if(self.currentLocation.coordinate.latitude == 0.0)
                                    {
                                        self.currentLocation = appdelegate.currentlocation;

                                    }
                                    self.setSourceAndDestinationLocation(self.destinationLocation, address: self.MylesAddress, Id: self.bookingId)
                                }
                                
                            }
                            else
                            {
                                
                                if self.tripStatus == "Open"
                                {
                                
                                if(appdelegate.currentlocation != nil)
                                {
                                    self.currentLocation = appdelegate.currentlocation;
                                    
                                }
                                
                                self.setSourceAndDestinationLocation(self.destinationLocation, address: self.MylesAddress, Id: self.bookingId)
                                }
                                GMDCircleLoader.hideFromView(self.view, animated:true)
                            }
                        }
                        catch let error as NSError
                        {
                            print(error)
                            GMDCircleLoader.hideFromView(self.view, animated:true)
                        }
                    }
                    
                })
                
            })]
        }
        else
        {
            CommonFunctions.alertTitle("Error!", withMessage: "No Network available. Please connect and try again.");
            
        }
    }
    
    
    // Lock/unloak car as  per the current status
    func lockUnlockCheck (_ bookingid : NSString  ,sender : UIButton) -> Void
    {

        if (CommonFunctions.reachabiltyCheck())
        {
            GMDCircleLoader.setOnView(self.view , withTitle: "Please wait...", animated: true)

            let state = (self.LockUnlockBoolState == 1) ? "U" : "L"
            [CommonApiClass.LockUnlockCar(self.lockunlockResponse?.gpsdeviceno, LOCKUNLOCK: state, completionHandler: { (data, response, error) -> Void in
                
                DispatchQueue.main.async (execute: { () -> Void in
                    if (error == nil)
                    {
                        do
                        {
                            let jsonresponse  = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                            
                            let status = jsonresponse.valueForKey("status")
                            let message = jsonresponse.valueForKey("message") as? NSString
                            if status!.integerValue == 1
                            {
//                                self.setSourceAndDestinationLocation(self.currentLocation, address: self.trackaddress, Id: self.bookingId)
                                let btnTitle = (sender.titleLabel?.text?.uppercaseString)! as NSString;
                                
                                let title = (btnTitle == "Lock Ride".uppercaseString) ? "UnLock Ride".uppercaseString :"lock Ride".uppercaseString

                                sender.setTitle(title, forState: UIControlState.Normal)

                                CommonFunctions.alertTitle(nil, withMessage: jsonresponse.valueForKey("message") as! String);
                            }
                            else
                            {
                                CommonFunctions.alertTitle("Message", withMessage: message as! String);
                            }
                        }
                        catch let error as NSError
                        {
                            print(error)
                        }
                    }
                    GMDCircleLoader.hideFromView(self.view, animated:true)
                })
            })]
        }
        else
        {
            CommonFunctions.alertTitle("Error!", withMessage: "No Network available. Please connect and try again.");

        }
    }
}


