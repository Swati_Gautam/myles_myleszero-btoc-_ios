//
//  APIOperation.m
//  APIOperation
//


#import "APIOperation.h"

#define KVOBlock(KEYPATH, BLOCK) \
    [self willChangeValueForKey:KEYPATH]; \
    BLOCK(); \
    [self didChangeValueForKey:KEYPATH];





@implementation APIOperation {
    BOOL _finished;
    BOOL _executing;
}

- (instancetype)initWithSession:(NSURLSession *)session URL:(NSString *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    if (self = [super init]) {
        
        __weak typeof(self) weakSelf = self;
        
        session.configuration.timeoutIntervalForRequest = KTIMEOUT_INTERVAL;

        NSURL *requestUrl  = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SiteAPIURL,url]];
        NSDictionary *headers = @{ @"hashVal": [DEFAULTS objectForKey:K_AccessToken] != nil ? [DEFAULTS objectForKey:K_AccessToken]:@"",
                                   @"sessionID": [DEFAULTS objectForKey:K_SessionID] != nil ? [DEFAULTS objectForKey:K_SessionID]:@"",
                                   @"customerID": [DEFAULTS boolForKey:IS_LOGIN] ? [DEFAULTS objectForKey:KUSER_ID]:@"-99"
                                   };
        
        NSLog(@"HelloHeaders = %@",headers);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl];
        
        request.allHTTPHeaderFields = headers;
        _task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (error)
            {
                //NSLog(@" ERROR  : %@",[error localizedDescription]);
                completionHandler(data , response, error);
            }
            else
            {

                [weakSelf completeOperationWithBlock:completionHandler data:data response:response error:error];
            }
        }];
    }
    return self;
}

- (instancetype)initWithSession:(NSURLSession *)session request:(NSMutableURLRequest *)request completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    if (self = [super init]) {
        __weak typeof(self) weakSelf = self;
        
        session.configuration.timeoutIntervalForRequest = KTIMEOUT_INTERVAL;
        
        NSDictionary *headers = @{ @"hashVal": [DEFAULTS objectForKey:K_AccessToken] != nil ? [DEFAULTS objectForKey:K_AccessToken]:@"",
                                   @"sessionID": [DEFAULTS objectForKey:K_SessionID] != nil ? [DEFAULTS objectForKey:K_SessionID]:@"",
                                   @"customerID": [DEFAULTS boolForKey:IS_LOGIN] ? [DEFAULTS objectForKey:KUSER_ID]:@"-99"
                                   };
        
        NSLog(@"HelloHeaders = %@",headers);
        
        request.allHTTPHeaderFields = headers;

        
        _task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            if (error)
            {
                //NSLog(@" ERROR  : %@",[error localizedDescription]);
                completionHandler(data , response, error);
            }
            else
            {
                [weakSelf completeOperationWithBlock:completionHandler data:data response:response error:error];
            }
        }];
    }
    return self;
}

- (instancetype)initWithBody:(NSMutableDictionary *)body session:(NSURLSession *)session url:(NSString *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    if (self = [super init]) {
        __weak typeof(self) weakSelf = self;
        
        session.configuration.timeoutIntervalForRequest = KTIMEOUT_INTERVAL;

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SiteAPIURL,url]]];
        
        NSLog(@"URL Request: %@",request);
        
        NSDictionary *headers = @{ @"hashVal": [DEFAULTS objectForKey:K_AccessToken] != nil ? [DEFAULTS objectForKey:K_AccessToken]:@"",
                                   @"sessionID": [DEFAULTS objectForKey:K_SessionID] != nil ? [DEFAULTS objectForKey:K_SessionID]:@"",
                                   @"customerID": [DEFAULTS boolForKey:IS_LOGIN]  ? [DEFAULTS objectForKey:KUSER_ID] != nil ? [DEFAULTS objectForKey:KUSER_ID]:@"-99"
                                  :@"-99" };
        NSLog(@"headers: %@", headers);
        NSLog(@"paymentUrl = %@%@",SiteAPIURL,url);
        //NSLog(@"payment HelloHeaders for payment  = %@",headers);
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];

        
        NSData *data = [NSJSONSerialization dataWithJSONObject:body options:kNilOptions error:nil];
        if (data)
        {
            [request setHTTPBody:data];
        }
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
   
        
        _task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            if (error)
            {
                //NSLog(@" ERROR  : %@",[error localizedDescription]);
                completionHandler(data , response, error);
            }
            else
            {

            [weakSelf completeOperationWithBlock:completionHandler data:data response:response error:error];
            }
        }];
    }
    return self;
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    //NSLog(@" ERROR2  : %@",[error localizedDescription]);

}

- (void)cancel {
    [super cancel];
    [self.task cancel];
}

- (void)start {
    if (self.isCancelled) {
        KVOBlock(@"isFinished", ^{ _finished = YES; });
        return;
    }
    KVOBlock(@"isExecuting", ^{
        [self.task resume];
        _executing = YES;
    });
}

- (BOOL)isExecuting {
    return _executing;
}

- (BOOL)isFinished {
    return _finished;
}

- (BOOL)isConcurrent {
    return YES;
}

- (void)completeOperationWithBlock:(void (^)(NSData *, NSURLResponse *, NSError *))block data:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error {
    if (!self.isCancelled && block)
        block(data, response, error);
    [self completeOperation];
}

- (void)completeOperation {
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];

    _executing = NO;
    _finished = YES;

    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

/*
 @brief Get sublocation through API(GET)
 @dev Abhishek Singh
 @Date 6sep
 */



-(void)getFaqData :(void(^)(NSDictionary *dictResponce))completionBlock
{
//    NSURL *url = [NSURL URLWithString:@"https://www.mylescars.com/Rests/getFaqDeatils"];
    
    NSURL *url = [NSURL URLWithString:@"https://www.mylescars.com/Rests/getFaqDeatils"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if ( data.length > 0 && connectionError == nil)
         {
             NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:0
                                                                        error:NULL];
            
             completionBlock(greeting);
         }
     }];
}

@end

