//
//  APIRequestManager.h
//  Myles
//


#import <Foundation/Foundation.h>

@interface APIRequestManager : NSObject

@property (strong, nonatomic) NSOperationQueue *queue;
           
+ (id)sharedInstance;

@end
