//
//  APIRequestManager.m
//  Myles
//

#import "APIRequestManager.h"

@implementation APIRequestManager

+ (id)sharedInstance {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

- (id)init {
    if (self = [super init]) {
        self.queue = [NSOperationQueue new];
    }
    
    return self;
}

@end
