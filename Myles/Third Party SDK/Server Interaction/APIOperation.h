
#import <Foundation/Foundation.h>

@interface APIOperation : NSOperation <NSURLSessionDelegate>

- (instancetype)initWithSession:(NSURLSession *)session URL:(NSString *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler;
- (instancetype)initWithSession:(NSURLSession *)session request:(NSURLRequest *)request completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler;

- (instancetype)initWithBody:(NSMutableDictionary *)body session:(NSURLSession *)session url:(NSString *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler;


-(void)getFaqData :(void(^)(NSDictionary *dictResponce))completionBlock;
@property (nonatomic, strong, readonly) NSURLSessionDataTask *task;

@end
