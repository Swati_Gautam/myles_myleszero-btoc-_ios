//
//  MapConstant.h
//  Myles
//
//  Created by Myles   on 31/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

//#ifndef Myles_MapConstant_h
//#define Myles_MapConstant_h

#define KACTIVE_BOOKING_NOTIFICATION  @"KACTIVE_BOOKING_NOTIFICATION"

//Map constant



#define k_defaultLatitude                               @"28.6667"
#define k_defaultLongitude                              @"77.2167"

//Api identifier


#define k_clientCoID                                    @"ClientCoID"
#define k_cityId                                        @"CityId"
#define k_cityName                                      @"CityName"
#define k_latitude                                      @"latitude"
#define k_longitude                                     @"longitude"
#define k_sublocationId                                 @"sublocationId"
#define k_sublocationName                               @"sublocationName"
#define k_modelImages                                   @"modelImages"
#define k_modelName                                     @"modelName"
#define k_imgUrl                                        @"url"
#define k_carModelIDCap                                 @"CarModelID"
#define k_Noofcars                                      @"Noofcars"


//Api for Car Avalaviblity
#define k_cityIDCap                                     @"CityID"
#define k_pickUpdate                                    @"PickUpDate"
#define k_pickUpTime                                    @"PickUpTime"
#define k_dropOffTime                                   @"DropOffTime"
#define k_dropOffDate                                   @"DropOffDate"
#define k_SubLocationsID                                @"SubLocations"
#define k_SubLocationAddress                            @"SubLocationAddress"
#define k_Durationtime                                  @"Duration"
#define k_PkgType                                       @"PkgType"
#define k_CustomPkgYN                                   @"CustomPkgYN"
#define k_CarModelId                                    @"CarModelId"
//Coupon
#define k_DiscountCode                                  @"DiscountCode"
#define k_totalFare                                     @"totalFare"
#define k_pkgId                                         @"pkgId"
#define k_pkgIdCap                                      @"PkgId"
#define k_pkgDuration                                   @"pkgDuration"
#define k_ReasonValid                                   @"Valid"
#define k_CouponTypeFixed                               @"Fixed"
#define k_pickupDate                                    @"PickupDate"
#define k_TotalFare                                     @"TotalFare"
#define k_ValidYes                                       @"Yes"




//Api For Car Detail Information
#define K_price                                         @"price"
#define k_urlFullImage                                  @"CarImageHD"
#define k_weekdayDuration                               @"Regular days"
#define k_weekdayHourDuration                           @"Regular Hours"

#define k_weekEndDuration                               @"Premium days"
#define k_weekEndHourDuration                           @"Premium Hours"

#define k_weekDayCharges                                @"PkgRate"
#define k_weekEndCharges                                @"PkgRateWeekEnd"
#define k_VatRate                                       @"VAT"
#define k_VateAmount                                    @"VatAmt"
#define k_Sgst                                          @"SGST Amt"
#define k_Cgst                                          @"CGST Amt"
#define k_Igst                                          @"IGST Amt"
#define k_Utgst                                         @"UTGST Amt"
#define k_TotalDuration                                 @"TotalDuration"
#define k_IndicatedCharges                              @"Total Fare"
#define k_SubTotal                                      @"Sub Total"
#define k_discountonbaseFare                            @"Free Day Discount"
#define k_originalCharges                               @"Minimum Billing"
#define k_DepositAmt                                    @"Refundable Security Deposit"
#define k_AdditionalService                             @"Extra Amenities"
#define k_DoorStepDelivery                              @"Door-Step Charge"
#define k_HomePickupAvailable                           @"HomePickupAvail"
#define k_HomePickupCharge                              @"HomePickupChrg"
#define k_sublocationcost                               @"Sub Location Cost"
#define k_convenienceCharge                             @"Convenience Charge"
#define k_discountAmt                                   @"Coupon Discount"
#define k_freedays                                      @"Free days"
#define k_freeHours                                     @"Free Hours"
#define k_Daily                                         @"Daily"
#define k_hourly                                        @"Hourly"
#define k_CarVariant                                    @"CarVariant"
#define k_CarVariantID                                  @"CarVariantID"




//Api Addional Service
#define k_CarCatId                                      @"CarCatId"
#define k_serviceid                                     @"serviceid"
#define k_CarCatIdCap                                   @"CarCatID"
#define k_documentid                                    @"documentid"
#define k_damageimg1                                    @"damageimg1"
#define k_damageimg2                                    @"damageimg2"

#define k_detailCell                                    @"detailCell"
#define k_searchTableCellIdentifier                     @"Cell"
#define k_mapCollectionCellIdentifier                   @"CollectionCell"
#define k_unselectedPinImg                              @"UnselectedPin"
#define k_selectedPinImg                                @"selectedPin"
#define k_searchImg                                     @"searchImg"
#define k_listImg                                       @"listImg"
#define k_upsignImg                                     @"upsignImg"
#define k_menuImg                                       @"menuIcon"
#define k_Closeicon                                     @"Closeicon"
#define k_Crossicon                                     @"crossIcon"
#define k_dropdown                                      @"dropdown"
#define k_bookinggap                                    @"bookinggap"
#define k_contactno                                     @"contactno"

#define k_pickUpAddressIdentifier                       @"pickUpAddressIdentifier"
#define k_pickUpAddressCell                             @"pickUpAddressCell"
#define k_carListTableCellIdentifier                     @"carListCell"
#define k_carListNormalCell                                 @"NextAvailableCell"
#define k_rideHistoryCellIdentifier                       @"rideListCell"
#define k_BookingcarDetailCellIdentifier                       @"bookingcarCell"
#define k_BookingtimeDetailCellIdentifier                       @"bookingtimeCell"
#define k_BookingPreauthPaymentCellIdentifier                       @"preauthPaymentCell"
#define k_BookingAmenitesDetailCellIdentifier                       @"bookingAmenitesCell"
#define k_BookingPickUpDetailCellIdentifier                       @"bookingPickUpCell"

//PreAuth
#define k_bookingid                             @"bookingid"
#define k_trackid                               @"trackid"
#define k_transactionid                         @"transactionid"
#define k_preauthamount                         @"preauthamount"

//Lock Unlock
#define k_GPSDeviceNo                           @"GPSDeviceNo"
#define k_IsLock                                 @"IsLock"

//Ride Constant
#define k_highlightCarIcon                              @"highlightCarIcon"
#define k_unhighlightCarIcon                            @"unhighlightCarIcon"
#define k_deleteIcon                                    @"deleteIcon"
#define k_defaultCarFullImg                             @"defaultCarFullImg"
#define k_defaultCarImg                                 @"defaultCarImg"
#define k_deleteImg                                     @"deleteImg"

#define k_crossBoxIcon                                   @"crossBoxIcon"
#define k_checkicon                                     @"checkBoxIcon"
#define k_blankBoxIcon                                  @"blankBoxIcon"
#define k_uploadDocIcon                                 @"uploadDocIcon"
#define k_radioButton                                   @"radioButton"
#define k_SelectedRadioButton                           @"SelectedRadioButton"
#define k_greyRadio                                     @"grayRadio"

#define k_vouchercheckicon                              @"voucherCheckBox"
#define k_RightCheck                                    @"rightcheck"
#define k_wrongCheck                                    @"wrongCheck"


#define k_babySeater                                    @"Baby Seat"
#define k_gpsnavigation                                 @"Gps Navigation"

#define k_timeTableCellIdentifier                       @"TimeTableCell"
#define k_tripstatusClose                                   @"Close"
#define k_tripstatusCancel                                  @"Cancel"
#define k_tripstatusOpen                                  @"Open"
#define k_tripstatusNew                                  @"New"
// Booking Confirmation

//#define k_BookingTitleCONFIRMED                                  @"Booking Details"
#define k_BookingTitleCONFIRMED                                  @"Myles Booking Location"
//
#define k_BookingTitleCancel                            @"BOOKING CANCELLED"
#define k_BookingTitleClose                            @"BOOKING CLOSED"

#define k_thumbIcon                                     @"smallThumbUp"
#define k_dustbinIcon                                     @"CancelBtn"
#define k_cancelRed                                     @"CancelRed"

#define k_phonecall                                     @"phone"
#define k_phonecallRed                                     @"CallRed"

//Profile Constant

#define k_filename                                      @"filename"
#define k_seletedDocumentBtnImg                         @"selectedDocument"
#define k_unseletedDocumentBtnImg                       @"unselectedDocument"
#define k_VOTER                 @"Voter Card"
#define k_PANCARD               @"Pan Card"
#define k_DL                    @"Drivers License"
#define k_PASSPORT              @"Passport"
#define k_ADHAR                 @"Aadhar Card"

//Track Ride
#define k_rideStartPoint        @"blackdot"
#define k_rideDestination        @"destinationicon"
#define k_pinIcon        @"pinIcon"
//Feedback
#define k_star                  @"star"
#define k_starhighlighted       @"starhighlighted"

//UserDefault
#define DEFAULTS [NSUserDefaults standardUserDefaults]
#define k_userappStatus                                 @"userappStatus"
#define k_mylesCenterNumber                 @"mylesCenterNumber"
#define k_gstDetails                        @"GSTDetails"

#define k_PickupAddress                 @"PickupAddress"
#define k_PickupEnabled                 @"PickupEnabled"


//Controllers Title

#define k_BookingTitleStartRideTitle                @"RIDE STARTS ON"
#define k_BookingTitleEndRideTitle                @"RIDE ENDS ON"
#define k_ContactUsVcTitle              @"CONTACT US"
#define k_AboutUsVcTitle                @"ROAD SIDE ASSISTANCE"



//Payment

#define K_Mylesbooking              @"MYLES BOOKING"
//#endif

#define K_LastLocation              @"kUserLastLocation"


#define K_AccessToken               @"accessToken"
#define K_SessionID                 @"sessionID"

//MAP
#define kMapsAPIKey        @"AIzaSyCkZGXtthXoOIomBu_NqK7JjPDjm-6K5Sk"
//@"AIzaSyAIwuR8bMOwRrR9-RBzWclX49fDYPGBnyE"

//@"AIzaSyA1phOQbUQTr71s7UM9TpGrgYtKnuzOGqM"
//@"AIzaSyDbOSNodOKfBp9xUka94bZLP5dyPx5uxJU" //@"AIzaSyCkZGXtthXoOIomBu_NqK7JjPDjm-6K5Sk"


//@"AIzaSyCiWw7kvX297ZN_NbGDo9awWiDhZuPIHjw"

#define kPlacesAPIKey        @"AIzaSyCkZGXtthXoOIomBu_NqK7JjPDjm-6K5Sk" //@"AIzaSyAIwuR8bMOwRrR9-RBzWclX49fDYPGBnyE"

//#define AWS_ACCESS_KEY1      @"AKIAJM3EUJGYK546JWCA";
//#define AWS_SECRET_KEY1      @"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";

//@"AIzaSyA1phOQbUQTr71s7UM9TpGrgYtKnuzOGqM"
//@"AIzaSyDbOSNodOKfBp9xUka94bZLP5dyPx5uxJU"  //@"AIzaSyCkZGXtthXoOIomBu_NqK7JjPDjm-6K5Sk"


//@"AIzaSyBbJf7chx86Xw1XnMXkhsmT5jC7t_uJgTE"


#define UIColorFromRGBForNavigationBar(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]
