//
//  AppVersionInfo.h
//  Myles
//
//  Created by IT Team on 28/09/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>

@protocol AppVersionInfo

@end

@interface AppVersionInfo : JSONModel

@property (strong, nonatomic) NSString *appSupport;
@property (strong, nonatomic) NSString *appVersion;
@property (strong, nonatomic) NSString *applyOnWeek;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *AppVersionsdescription;

@end
