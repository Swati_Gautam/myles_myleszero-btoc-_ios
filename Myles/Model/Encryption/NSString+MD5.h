//
//  NSString+MD5.h
//  Myles
//
//  Created by IT Team on 10/10/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5;

@end
