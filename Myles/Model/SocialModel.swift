//
//  SocialModel.swift
//  Myles
//
//  Created by Abhishek on 2/1/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation

@objc class SocialModel: NSObject
{
    @objc var dob : String?
    @objc var email : String?
    @objc var socialId : String?
    @objc var lName : String?
    @objc var fName : String?
    @objc var fullName : String?
    @objc var phoneNumber : String?
    @objc var type : String?

    @objc init(dict: Dictionary<String,Any>)
    {
        fullName = dict["name"] as? String ?? ""
        fName = dict["first_name"] as? String ?? ""
        lName = dict["last_name"] as? String ?? ""
        phoneNumber = dict["mNumber"] as? String ?? ""
        socialId = dict["id"] as? String ?? ""
        dob = dict["birthday"] as? String ?? ""
        email = dict["email"] as? String ?? ""
        type = dict["type"] as? String ?? "fb"

    }
    
    @objc init(gDict:Dictionary<String,Any>)
    {
        fullName = gDict["displayName"] as? String ?? ""
        lName = gDict["familyName"] as? String ?? ""
        fName = gDict["givenName"] as? String ?? ""
        phoneNumber = gDict["mNumber"] as? String ?? ""
        socialId = gDict["id"] as? String ?? ""
        dob = gDict["birthday"] as? String ?? ""
        email = gDict["email"] as? String ?? ""
        type = gDict["type"] as? String ?? "gp"
    }
    
    @objc init(appleDict: Dictionary<String,Any>)
    {
        fullName = appleDict["fullName"] as? String ?? ""
        fName = appleDict["first_name"] as? String ?? ""
        lName = appleDict["last_name"] as? String ?? ""
        phoneNumber = appleDict["mNumber"] as? String ?? ""
        socialId = appleDict["socialId"] as? String ?? ""
        dob = appleDict["dob"] as? String ?? ""
        email = appleDict["email"] as? String ?? ""
        type = appleDict["type"] as? String ?? "apple"

    }
}

//class googleNameSubModel
//{
//    var lName : String?
//    var fName : String?
//    
//    init(dict: Dictionary<String,Any>)
//    {
//
//    }
//
//}

@objc class signUpResponceModel : NSObject
{
    @objc var status = 0
    @objc var message : String?
    //var userId  : String?
    @objc var response : ResponceModel?
    
    
    @objc init(dict: Dictionary<String,Any>)
    {
        status = dict["status"] as? Int ?? 0
        message = dict["message"] as? String ?? nil
        response = ResponceModel(dict: dict["response"] as? Dictionary<String, Any>) 
    }
}

@objc class ResponceModel : NSObject
{
    @objc var dob : String?
    @objc var email : String?
    @objc var lName : String?
    @objc var fName : String?
    @objc var userId  : String?
    
    @objc init(dict: Dictionary<String,Any>?)
    {
        dob = dict?["DOB"] as? String ?? nil
        email = dict?["emailId".lowercased()] as? String ?? nil
        fName = dict?["fname".lowercased()] as? String ?? nil
        lName = dict?["lname".lowercased()] as? String ?? nil
        userId = dict?["userId"] as? String ?? nil
        
    }
}

