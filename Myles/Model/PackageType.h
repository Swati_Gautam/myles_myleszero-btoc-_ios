//
//  PackageType.h
//  Myles
//
//  Created by Myles on 15/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <jsonmodel//JSONModel.h>

#import "PkgResponseModel.h"

@interface PackageType : JSONModel
{
    
}
@property (nonatomic , strong) NSString *message;
@property (strong , nonatomic) NSString *status;
@property (nonatomic , strong) NSArray <PkgResponseModel , Optional> *response;
@end
