//
//  PickUpPackageDetail.h
//  Myles
//
//  Created by Myles   on 20/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>

@interface PickUpPackageDetail : JSONModel


@property (strong, nonatomic) NSString *BookingID;
@property (strong, nonatomic) NSString *CarModelName;
@property (strong, nonatomic) NSString *DropoffDate;
@property (strong, nonatomic) NSString *DropoffTime;
@property (strong, nonatomic) NSString *Lat;
@property (strong, nonatomic) NSString *Lon;
@property (strong, nonatomic) NSString *PaymentAmount;
@property (strong, nonatomic) NSString *PaymentStatus;
@property (strong, nonatomic) NSString *PickupDate;
@property (strong, nonatomic) NSString *PickupTime;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *sublocationAdd;
@property (strong, nonatomic) NSString *sublocationName;
@property (strong, nonatomic) NSString *urlFullimage;

@end
