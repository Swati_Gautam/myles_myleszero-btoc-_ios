//
//  UserInfoResponse.h
//  Myles
//
//  Created by Myles   on 14/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"


@protocol UserInfoResponse <NSObject>

@end
@interface UserInfoResponse : JSONModel
// test pull request
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *FName;
@property (strong, nonatomic) NSString *LName;
@property (strong, nonatomic) NSString *emailId;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *PerAdd;
@property (strong, nonatomic) NSString *ResAdd;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSString *Dob;

@property (strong, nonatomic) NSString *AdharUrl;
@property (strong, nonatomic) NSString *DlUrl;
@property (strong, nonatomic) NSString *PassportUrl;
@property (strong, nonatomic) NSString *VoterUrl;

@property (strong, nonatomic) NSString *AdharFile;
@property (strong, nonatomic) NSString *VoterFile;
@property (strong, nonatomic) NSString *AdharStatus;
@property (strong, nonatomic) NSString *LiecenceFile;

@property (strong, nonatomic) NSString *VoterExpiry;
@property (strong, nonatomic) NSString *VoterID;
@property (strong, nonatomic) NSString *VoterIssuingCity;

@property (strong, nonatomic) NSString *dlExpiry;
@property (strong, nonatomic) NSString *dlNo;
@property (strong, nonatomic) NSString *dlIssueCity;

@property (strong, nonatomic) NSString *passValidity;
@property (strong, nonatomic) NSString *passportNo;
@property (strong, nonatomic) NSString *passIssuingCity;

@property (strong, nonatomic) NSString *LiecenceStatus;
@property (strong, nonatomic) NSString *PassportFile;
@property (strong, nonatomic) NSString *PassportStatus;
@property (strong, nonatomic) NSString *VoterStatus;

@property (strong , nonatomic) NSString *PanFile;
@property (strong , nonatomic) NSString *PanNo;
@property (strong , nonatomic) NSString *PanStatus;
@property (strong , nonatomic) NSString *PanUrl;

@end
