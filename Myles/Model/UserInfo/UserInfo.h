//
//  UserInfo.h
//  Myles
//
//  Created by Myles   on 14/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"
#import "UserInfoResponse.h"

@interface UserInfo : JSONModel

@property (strong, nonatomic) UserInfoResponse *response;
@property (strong , nonatomic) NSString *status;

@end
