//
//  pkgResponseModel.h
//  Myles
//
//  Created by Myles on 15/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <jsonmodel//JSONModel.h>


@protocol PkgResponseModel <NSObject>

@end

@interface PkgResponseModel : JSONModel

@property (nonatomic , strong) NSString *CarModelID;
@property (nonatomic , strong) NSString *CarModelName;
@property (nonatomic , strong) NSString *PkgID;
@property (nonatomic , strong) NSString *PkgRate;
@property (nonatomic , strong) NSString *PkgType;


@end
