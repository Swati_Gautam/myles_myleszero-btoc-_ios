//
//  PromotionalAlert.swift
//  Myles
//
//  Created by Chetan Rajauria on 26/07/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import UIKit

class PromotionalAlert: UIView {

    @IBOutlet weak var alertBackView: UIView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    @IBAction func yesButtonAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func noButtonAction(_ sender: Any) {
        self.removeFromSuperview()

    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        self.yesBtn.layer.cornerRadius = 4
        self.noBtn.layer.cornerRadius = self.yesBtn.layer.cornerRadius
        
        self.yesBtn.layer.borderColor = UIColor(red: 241/255.0, green: 17/255.0, blue: 0/255.0, alpha: 1.0).cgColor
        
        self.noBtn.layer.borderColor = UIColor(red: 241/255.0, green: 17/255.0, blue: 0/255.0, alpha: 1.0).cgColor
        
        self.yesBtn.layer.borderWidth = 1
        self.noBtn.layer.borderWidth = 1
        self.alertBackView.layer.cornerRadius = self.yesBtn.layer.cornerRadius
    }
    
    
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }

}
