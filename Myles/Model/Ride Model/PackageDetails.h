//
//  PackageDetails.h
//  Myles
//
//  Created by Myles   on 10/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <jsonmodel//JSONModel.h>



@protocol PackageDetails

@end
@interface PackageDetails : JSONModel

@property (strong, nonatomic) NSString *PkgType;
@property (strong, nonatomic) NSString *PkgTypeID;

@end
