
#import <jsonmodel//JSONModel.h>


@protocol AmenitiesInfo

@end
@interface AmenitiesInfo : JSONModel

@property (strong, nonatomic) NSString *Amount;
@property (strong, nonatomic) NSString *Charges;
@property (strong, nonatomic) NSString *Description;
@property (strong, nonatomic) NSString *MaxAvailable;
@property (strong, nonatomic) NSString *ServiceID;


@end
