//
//  CarDetailInformation.h
//  Myles
//
//  Created by IT Team on 13/06/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"
#import "Sublocation.h"
#import "CarPackageDetails.h"
#import "GSTPackageDetails.h"

@interface CarDetailInformation : JSONModel

@property (strong, nonatomic) NSString *AgeMessage;
@property (strong, nonatomic) NSString *AirportYN;
@property (strong, nonatomic) NSString *CarCatID;
@property (strong, nonatomic) NSString *CarCatName;
@property (strong, nonatomic) NSString *CarImageHD;
@property (strong, nonatomic) NSString *CarImageThumb;
@property (strong, nonatomic) NSString *CityName;
@property (strong, nonatomic) NSString *CoricId;
@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , Sublocation> *Sublocation;
@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , CarPackageDetails> *AllPackages;
@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , GSTPackageDetails> *GSTDetails;
@property (strong, nonatomic) NSString *Detail;
@property (strong, nonatomic) NSString *DetailID;
@property (strong, nonatomic) NSString *FuelType;
@property (strong, nonatomic) NSString *IsAvailable;
@property (strong, nonatomic) NSString *LuggageCapacity;
@property (strong, nonatomic) NSString *Model;
@property (strong, nonatomic) NSString *ModelID;
@property (strong, nonatomic) NSString *SeatingCapacity;
@property (strong, nonatomic) NSString *TransmissionType;
@property (assign, nonatomic) BOOL IsSpeedGovernor;

//New Additions
@property (strong, nonatomic) NSString *DropDate;
@property (strong, nonatomic) NSString *DropEarlyLateHour;
@property (strong, nonatomic) NSString *DropTime;
@property (strong, nonatomic) NSString *PickDate;
@property (strong, nonatomic) NSString *PickEarlyLateHour;
@property (strong, nonatomic) NSString *PickTime;

// we can remove it by adding package details object

@property (strong, nonatomic) NSString *AirportCharges;
@property (strong, nonatomic) NSString *BasicAmt;
@property (strong, nonatomic) NSString *CGSTAmount;
@property (strong, nonatomic) NSString *CGSTRate;
@property (strong, nonatomic) NSString *DepositeAmt;
@property (strong, nonatomic) NSString *ExtraKMRate;
@property (strong, nonatomic) NSString *FreeDuration;
@property (strong, nonatomic) NSString *IGSTAmount;
@property (strong, nonatomic) NSString *IGSTRate;
@property (strong, nonatomic) NSString *IndicatedPrice;
@property (strong, nonatomic) NSString *InsuranceAmt;
@property (strong, nonatomic) NSString *KMIncluded;
@property (strong, nonatomic) NSString *OriginalAmt;
@property (strong, nonatomic) NSString *PkgId;
@property (strong, nonatomic) NSString *PkgRate;
@property (strong, nonatomic) NSString *PkgRateWeekEnd;
@property (strong, nonatomic) NSString *PkgType;
@property (strong, nonatomic) NSString *SGSTAmount;
@property (strong, nonatomic) NSString *SGSTRate;
@property (strong, nonatomic) NSString *SubLocationCost;
@property (strong, nonatomic) NSString *TotalDuration;
@property (strong, nonatomic) NSString *TotalInsuranceAmt;
@property (strong, nonatomic) NSString *UTGSTAmount;
@property (strong, nonatomic) NSString *UTGSTRate;
@property (strong, nonatomic) NSString *VatAmt;
@property (strong, nonatomic) NSString *VatRate;
@property (strong, nonatomic) NSString *WeekDayDuration;
@property (strong, nonatomic) NSString *WeekEndDuration;

@property (strong, nonatomic) NSString *PromotionCode;
@property (strong, nonatomic) NSString *AfterDiscountAmount;
@property (strong, nonatomic) NSString *DiscountAmt;

@property (strong, nonatomic) NSString *CessRate;
@property (strong, nonatomic) NSString *CessAmount;
@property (strong, nonatomic) NSString *CGSTRate_Service;
@property (strong, nonatomic) NSString *IGSTRate_Service;
@property (strong, nonatomic) NSString *SGSTRate_Service;
@property (strong, nonatomic) NSString *UTGSTRate_Service;

@property (strong, nonatomic) NSString *CarVariant;
@property (strong, nonatomic) NSString *CarVariantID;

@property (strong, nonatomic) NSString *InsuranceSecurityAmt;

@end

@protocol CarDetailInformation
@end

