//
//  PackageResponse.h
//  Myles
//
//  Created by Myles   on 10/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <jsonmodel//JSONModel.h>

#import "PackageDetails.h"

@interface PackageResponse : JSONModel

@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , PackageDetails> *response;
@property (assign , nonatomic) NSInteger status;


@end
