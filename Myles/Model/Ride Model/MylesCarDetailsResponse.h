//
//  MylesCarDetailsResponse.h
//  Myles
//
//  Created by IT Team on 17/06/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"
#import "SearchCarDetails.h"

@interface MylesCarDetailsResponse : JSONModel

@property (strong, nonatomic) SearchCarDetails *response;

//@property (strong,nonatomic) NSMutableArray<ConvertOnDemand,>
@end


