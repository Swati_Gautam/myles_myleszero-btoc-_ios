//
//  DocumentModel.h
//  Myles
//
//  Created by Myles   on 10/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>

@protocol DocumentModel

@end

@interface DocumentModel : JSONModel

@property (strong, nonatomic) NSString *docname;
@property (strong, nonatomic) NSString *docid;

@end
