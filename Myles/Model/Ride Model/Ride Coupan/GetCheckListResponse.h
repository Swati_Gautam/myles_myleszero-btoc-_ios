//
//  GetCheckListResponse.h
//  Myles
//
//  Created by Myles   on 10/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>
#import "AmenitiesDetails.h"
#import "DocumentModel.h"

@interface GetCheckListResponse : JSONModel

@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , AmenitiesDetails> *aminities;
@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , DocumentModel> *document;
@property (strong, nonatomic) NSString *message;

@end
