//
//  CouponResponse.h
//  Myles
//
//  Created by Myles   on 09/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>


@interface CouponResponse : JSONModel

@property (strong, nonatomic) NSString *DiscountCode;
@property (strong, nonatomic) NSString *PerDiscount;
@property (strong, nonatomic) NSString *Reason;
@property (strong, nonatomic) NSString *Type;
@property (strong, nonatomic) NSString *ValidYN;



@end
