//
//  DorstepModel.h
//  Myles
//
//  Created by Nowfal E Salam on 13/02/18.
//  Copyright © 2018 Divya Rai. All rights reserved.
//

//#import <JSONModel/JSONModel.h>
#import "JSONModel.h"

@interface DorstepModel : JSONModel
@property (strong, nonatomic) NSString *Amount;
@end

@protocol DorstepModel

@end
