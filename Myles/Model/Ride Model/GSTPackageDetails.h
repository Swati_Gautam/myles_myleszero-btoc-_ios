//
//  GSTPackageDetails.h
//  Myles
//
//  Created by skpissay on 06/04/20.
//  Copyright © 2020 Myles. All rights reserved.
//

//#import <JSONModel/JSONModel.h>
#import "JSONModel.h"

@interface GSTPackageDetails : JSONModel

@property (strong, nonatomic) NSString *ApplicableGstRate;
@property (strong, nonatomic) NSString *Description;
@property (strong, nonatomic) NSString *IsCessApplicable;
@property (strong, nonatomic) NSString *ServiceId;

@end

@protocol GSTPackageDetails
@end

