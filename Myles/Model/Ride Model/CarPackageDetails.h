//
//  CarPackageDetails.h
//  Myles
//
//  Created by Nowfal E Salam on 25/10/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

//#import <JSONModel/JSONModel.h>
#import "JSONModel.h"

@interface CarPackageDetails : JSONModel

@property (strong, nonatomic) NSString *AirportCharges;
@property (strong, nonatomic) NSString *BasicAmt;
@property (strong, nonatomic) NSString *CGSTAmount;
@property (strong, nonatomic) NSString *CGSTRate;
@property (strong, nonatomic) NSString *DepositeAmt;
@property (strong, nonatomic) NSString *ExtraKMRate;
@property (strong, nonatomic) NSString *FreeDuration;
@property (strong, nonatomic) NSString *IGSTAmount;
@property (strong, nonatomic) NSString *IGSTRate;
@property (strong, nonatomic) NSString *IndicatedPrice;
@property (strong, nonatomic) NSString *InsuranceAmt;
@property (strong, nonatomic) NSString *KMIncluded;
@property (strong, nonatomic) NSString *OriginalAmt;
@property (strong, nonatomic) NSString *PkgDescription;
@property (strong, nonatomic) NSString *PkgId;
@property (strong, nonatomic) NSString *PkgRate;
@property (strong, nonatomic) NSString *PkgRateWeekEnd;
@property (strong, nonatomic) NSString *PkgType;
@property (strong, nonatomic) NSString *SGSTAmount;
@property (strong, nonatomic) NSString *SGSTRate;
@property (strong, nonatomic) NSString *SubLocationCost;
@property (strong, nonatomic) NSString *TotalDuration;
@property (strong, nonatomic) NSString *TotalInsuranceAmt;
@property (strong, nonatomic) NSString *UTGSTAmount;
@property (strong, nonatomic) NSString *UTGSTRate;
@property (strong, nonatomic) NSString *VatAmt;
@property (strong, nonatomic) NSString *VatRate;
@property (strong, nonatomic) NSString *WeekDayDuration;
@property (strong, nonatomic) NSString *WeekEndDuration;
@property (strong, nonatomic) NSString *TotalDurationForDisplay;
@property (assign) BOOL isSelected;

@property (strong, nonatomic) NSString *CessRate;
@property (strong, nonatomic) NSString *CessAmount;
@property (strong, nonatomic) NSString *CGSTRate_Service;
@property (strong, nonatomic) NSString *IGSTRate_Service;
@property (strong, nonatomic) NSString *SGSTRate_Service;
@property (strong, nonatomic) NSString *UTGSTRate_Service;
@property (strong, nonatomic) NSString *CarVariantID;
@property (strong, nonatomic) NSString *CarVariant;

@property (strong, nonatomic) NSString *PromotionCode;
@property (strong, nonatomic) NSString *AfterDiscountAmount;
@property (strong, nonatomic) NSString *DiscountAmt;

//@property (strong, nonatomic) NSString *ExtraKMRate;


@end

@protocol CarPackageDetails
@end
