//
//  ActiveRidesResponse.h
//  Myles
//
//  Created by Myles   on 10/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>
#import "ActiveRideDetails.h"

@interface ActiveRidesResponse : JSONModel

@property (strong, nonatomic) NSMutableArray< ConvertOnDemand, ActiveRideDetails> *response;

@end
