//
//  RideHistoryResponse.h
//  Myles
//
//  Created by Myles   on 18/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"

#import "RideHistoryDetails.h"

@interface RideHistoryResponse : JSONModel

@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , RideHistoryDetails> *response;

@end
