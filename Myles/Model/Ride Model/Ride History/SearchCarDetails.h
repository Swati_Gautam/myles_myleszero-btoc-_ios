//
//  SearchCarDetails.h
//  Myles
//
//  Created by iOS Team on 22/08/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"
#import "CarDetailInformation.h"
#import "DorstepModel.h"
#import "GSTPackageDetails.h"

@interface SearchCarDetails : JSONModel

@property (strong, nonatomic) NSMutableArray< ConvertOnDemand, CarDetailInformation> *CarSearchDetailNextAvailable;
@property (strong, nonatomic) NSMutableArray<ConvertOnDemand,CarDetailInformation> *CarSearchDetailNormal;
@property (strong, nonatomic) NSMutableArray<ConvertOnDemand,GSTPackageDetails> *GSTDetails;
//@property (strong, nonatomic) NSMutableDictionary<ConvertOnDemand,DorstepModel> *DoorStepDelivery;

@end

