//
//  Sublocation.h
//  Myles
//
//  Created by iOS Team on 30/08/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

#import "JSONModel.h"

@interface Sublocation : JSONModel

@property (strong,nonatomic) NSString *Detail;
@property (strong, nonatomic) NSString *DetailID;
@property (strong, nonatomic) NSString *AirportCharges;
@property (strong, nonatomic) NSString *ServiceType;

@end

@protocol Sublocation
@end
