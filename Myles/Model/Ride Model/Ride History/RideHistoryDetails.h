//
//  RideHistoryDetails.h
//  Myles
//
//  Created by Myles   on 18/09/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"
#import "AmenitiesDetails.h"
#import "ExtensionDetails.h"




@interface RideHistoryDetails : JSONModel

@property (strong, nonatomic) NSMutableArray< ConvertOnDemand, AmenitiesDetails> *AmenitiesDetails;
@property (strong, nonatomic) NSMutableArray<ConvertOnDemand,ExtensionDetail> *ExtensionDetail;
@property (strong, nonatomic) NSString *BookingSource;
@property (strong, nonatomic) NSString *Bookingid;
@property (strong, nonatomic) NSString *CarModelName;
@property (strong, nonatomic) NSString *Carcatname;
@property (strong, nonatomic) NSString *DiscountAmount;
@property (strong, nonatomic) NSString *DropoffDate;
@property (strong, nonatomic) NSString *DropoffTime;
@property (strong, nonatomic) NSString *Lat;
@property (strong, nonatomic) NSString *Lon;
@property (strong, nonatomic) NSString *PaymentAmount;
@property (strong, nonatomic) NSString *PaymentStatus;
@property (strong, nonatomic) NSString *PickupDate;
@property (strong, nonatomic) NSString *PickupTime;
@property (strong, nonatomic) NSString *SeatingCapacity;
@property (strong, nonatomic) NSString *Tripstatus;
@property (strong, nonatomic) NSString *sublocationAdd;
@property (strong, nonatomic) NSString *sublocationName;
@property (strong, nonatomic) NSString *trackID;
@property (strong, nonatomic) NSString *PreAuthAmount;
@property (strong, nonatomic) NSString *PreAuthStatus;
@property (strong, nonatomic) NSString *HomePickDropAdd;
@property (nonatomic, assign) BOOL      InsurancePaid;
@property (nonatomic, strong) NSString *CreditCard;
@property (nonatomic, strong) NSString *DoorStepAddress;
@property (nonatomic, strong) NSString *HomepickupService;
@property (nonatomic, strong) NSString *LDWAmount;
@property (nonatomic, strong) NSString *License;
@property (nonatomic, strong) NSString *PaidAmount;
@property (nonatomic, strong) NSString *Passport;
@property (nonatomic, strong) NSString *PendingExtensionAmount;
@property (nonatomic, strong) NSString *PickUpCityID;
@property (nonatomic, strong) NSString *SecurityAmount;
@property (nonatomic, strong) NSString *SecurityRefundAmount;
@property (nonatomic, strong) NSString *SecurityRefundStatus;
@property (nonatomic, strong) NSString *Extracharge;
@property (nonatomic, strong) NSString *Homeairportpickdropcharge;
@property (nonatomic, strong) NSString *Rentalcharge;
@property (nonatomic, strong) NSString *CGSTRate;
@property (nonatomic, strong) NSString *SGSTRate;
@property (nonatomic, strong) NSString *IGSTRate;
@property (nonatomic, strong) NSString *UTGSTRate;
@property (nonatomic, strong) NSString *CGSTAmount;
@property (nonatomic, strong) NSString *SGSTAmount;
@property (nonatomic, strong) NSString *IGSTAmount;
@property (nonatomic, strong) NSString *UTGSTAmount;
@property (nonatomic, strong) NSString *VatAmount;
@property (nonatomic, strong) NSString *CarImageThumb;
@property (nonatomic, strong) NSString *CarImageHD;
@property (nonatomic, strong) NSString *Invoice;
@property (nonatomic, strong) NSString *DamageInvoice;
@property (nonatomic, strong) NSString *TotalAmount;
@property (nonatomic, strong) NSString *ExtensionPaidAmount;
@property (nonatomic, strong) NSString *CancelPolicy;

@property (nonatomic, strong) NSString *TotalTax;

@property (nonatomic, strong) NSString *CessRate;
@property (nonatomic, strong) NSString *CessAmount;
@property (strong, nonatomic) NSString *CGSTRate_Service;
@property (strong, nonatomic) NSString *IGSTRate_Service;
@property (strong, nonatomic) NSString *SGSTRate_Service;
@property (strong, nonatomic) NSString *UTGSTRate_Service;

@property (strong, nonatomic) NSString *CarVariant;
@property (strong, nonatomic) NSString *CarVariantID; 


@property (nonatomic, strong) NSString *BookingType;



// PreAuthSattus
//2 - LDW
//1 - security paid
//0 - security not paid
/*
 Aadhar = True;
 AmenitiesDetails =     (
 );
 BookingSource = ios;
 Bookingid = 6655648;
 CarModelName = Innova;
 Carcatname = Toyota;
 DiscountAmount = "0.00";
 DoorStepAddress = "Delhi-205-208, Rani Jhansi Road, Block E 3, Jhandewalan ";
 DropoffDate = "05-24-2017";
 DropoffTime = 0130;
 ExtensionDetail =     (
 );
 Extracharge = "0.0000";
 HomePickDropAdd = "";
 Homeairportpickdropcharge = "0.00";
 HomepickupService = "<null>";
 LDWAmount = "88.50";/Users/iosteam/Documents/MYLES/Myles/Model/Ride Model
 Lat = "28.62882";
 License = False;
 Lon = "77.21945";
 PaidAmount = "1216.0000";
 Passport = False;
 PaymentAmount = "1216.00";
 PaymentStatus = Paid;
 PendingExtensionAmount = "0.00";
 PickUpCityID = 2;
 PickupDate = "05-23-2017";
 PickupTime = 2130;
 PreAuthAmount = "0.00";
 PreAuthStatus = 2;
 Rentalcharge = "692.00000";
 SeatingCapacity = 7;
 SecurityAmount = "0.00";
 SecurityRefundAmount = 0;
 SecurityRefundStatus = 0;
 Tripstatus = New;
 VatAmount = "101.46";
 sublocationAdd = "Delhi-205-208, Rani Jhansi Road, Block E 3, Jhandewalan ";
 sublocationName = "Connaught Place";
 trackID = CORIC1495543972355;
 },

 
 
 
 
 
 
 
 

AmenitiesDetails =     (
);
BookingSource = ios;
Bookingid = 6655648;
CarModelName = Innova;
CreditCard = "<null>";-
DiscountAmount = "0.00";
DoorStepAddress = "Delhi-205-208, Rani Jhansi Road, Block E 3, Jhandewalan ";-
DropoffDate = "05-24-2017";
DropoffTime = 0130;
HomePickDropAdd = "";
HomepickupService = "<null>";-
LDWAmount = "88.50";-
Lat = "28.62882";
License = "<null>";-
Lon = "77.21945";
PaidAmount = "1216.0000";-
Passport = "<null>";-
PaymentAmount = "1216.00";
PaymentStatus = Paid;
PendingExtensionAmount = "0.00";-
PickUpCityID = 2;-
PickupDate = "05-23-2017";
PickupTime = 2130;
PreAuthAmount = "0.00";
PreAuthStatus = 2;
SeatingCapacity = 7;
SecurityAmount = "0.00";-
SecurityRefundAmount = 0;-
SecurityRefundStatus = 0;-
Tripstatus = New;
VatAmount = "101.46";
extracharge = "0.0000";-
homeairportpickdropcharge = "0.00";-
rentalcharge = "692.00000";-
sublocationAdd = "Delhi-205-208, Rani Jhansi Road, Block E 3, Jhandewalan ";
sublocationName = "Connaught Place";
trackID = CORIC1495543972355;

 */


@end

@protocol RideHistoryDetails <NSObject>

@end
