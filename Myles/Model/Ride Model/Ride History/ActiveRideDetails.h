//
//  ActiveRideDetails.h
//  Myles
//
//  Created by Myles   on 10/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>

@protocol ActiveRideDetails


@end

@interface ActiveRideDetails : JSONModel

@property (strong, nonatomic) NSString *BookingId;
@property (strong, nonatomic) NSString *BookingAmount;
@property (strong, nonatomic) NSString *PickupDate;
@property (strong, nonatomic) NSString *PickupTime;
@property (strong, nonatomic) NSString *PkgType;
@property (strong, nonatomic) NSString *PreAuthAmount;
@property (strong, nonatomic) NSString *PreAuthStatus;
@property (strong, nonatomic) NSString *TripStatus;
@property (strong, nonatomic) NSString *ModelName;
@property (strong, nonatomic) NSString *TrackId;

@end
