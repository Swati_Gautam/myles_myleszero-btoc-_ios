//
//  ExtensionDetails.h
//  Myles
//
//  Created by iOS Team on 19/06/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"
@protocol ExtensionDetail

@end

@interface ExtensionDetail : JSONModel

@property (strong, nonatomic) NSString *ExtensionType;
@property (strong, nonatomic) NSString *ExtensionFromDate;
@property (strong, nonatomic) NSString *ExtensionFromTime;
@property (strong, nonatomic) NSString *ExtensionDate;
@property (strong, nonatomic) NSString *ExtensionTime;
@property (strong, nonatomic) NSString *ExtensionAmount;
@property (strong, nonatomic) NSString *RequestDateForExtension;
@property (strong, nonatomic) NSString *RequestTimeForExtension;

//RequestTimeForExtension
@property (strong, nonatomic) NSString *ExtensionStatus;
@property (strong, nonatomic) NSString *ExtensionLink;



@end
