

#import <jsonmodel//JSONModel.h>

#import "AmenitiesInfo.h"

@interface AdditionalServiceInfo : JSONModel

@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , AmenitiesInfo> *AditionalServices;


@end
