//
//  additionalServiceResponse.h
//  Myles
//
//  Created by Nowfal E Salam on 22/02/18.
//  Copyright © 2018 Divya Rai. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "AdditionalServiceInfo.h"
@interface additionalServiceResponse : JSONModel
@property (strong, nonatomic) AdditionalServiceInfo *response;

@end
