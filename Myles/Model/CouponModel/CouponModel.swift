//
//  CouponModel.swift
//  Myles
//
//  Created by Akanksha Singh on 09/04/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import Foundation

@objc class CouponModel: NSObject
{
    var expiry_date : String?
    var is_bank_offer : String?
    var promo_conditions : String?
    var promotion_code : String?
    var title : String?
    var maximum_discount_amount : String?
    
    init(dict: Dictionary<String,Any>)
    {
        expiry_date = dict["expiry_date"] as? String ?? nil
        is_bank_offer = dict["is_bank_offer"] as? String ?? nil
        promotion_code = dict["promotion_code"] as? String ?? nil
        title = dict["title"] as? String ?? nil
        maximum_discount_amount = dict["maximum_discount_amount"] as? String ?? nil
        
    }
    
    init(gdict: Dictionary<String,Any>)
    {
        expiry_date = gdict["expiry_date"] as? String ?? nil
        is_bank_offer = gdict["is_bank_offer"] as? String ?? nil
        promotion_code = gdict["promotion_code"] as? String ?? nil
        title = gdict["title"] as? String ?? nil
        maximum_discount_amount = gdict["maximum_discount_amount"] as? String ?? nil
        
    }

}

class termsConditionModel: NSObject {
    var termsCondition: [String]?
    
    init(array: [String]){
        termsCondition = array
    }
}

