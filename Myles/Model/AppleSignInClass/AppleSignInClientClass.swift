//
//  AppleSignInClientClass.swift
//  Myles
//
//  Created by Swati Gautam on 10/06/21.
//  Copyright © 2021 Myles. All rights reserved.
//

import UIKit
import AuthenticationServices

class AppleSignInClientClass: NSObject {
    
    var completionHandler: (_ fullName: String?, _ email: String?, _ token: String?, _ userID: String?) -> Void = {_, _, _, _ in }
    
    @available(iOS 13.0, *)
    @objc func handleAppleIdRequest(block: @escaping (_ fullName: String?, _ email: String?, _ token: String?, _ userID: String?) -> Void) {
        completionHandler = block
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13.0, *)
    @objc func getCredentialsState(userID: String) {
        //completionHandler = block
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: userID) { (credentialState: ASAuthorizationAppleIDProvider.CredentialState, error: Error?) in
          if let error = error {
            print(error)
            // Something went wrong check error state
            return
          }
          switch (credentialState) {
          case .authorized:
            //User is authorized to continue using your app
            break
          case .revoked:
            //User has revoked access to your app
            break
          case .notFound:
            //User is not found, meaning that the user never signed in through Apple ID
            break
          default: break
          }
        }
    }
    
    /*func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential else { return }

        print("User ID: \(appleIDCredential.user)")

        if let userEmail = appleIDCredential.email {
          print("Email: \(userEmail)")
        }

        if let userGivenName = appleIDCredential.fullName?.givenName,
          let userFamilyName = appleIDCredential.fullName?.familyName {
          print("Given Name: \(userGivenName)")
          print("Family Name: \(userFamilyName)")
        }

        if let authorizationCode = appleIDCredential.authorizationCode,
          let identifyToken = appleIDCredential.identityToken {
          print("Authorization Code: \(authorizationCode)")
          print("Identity Token: \(identifyToken)")
          //First time user, perform authentication with the backend
          //TODO: Submit authorization code and identity token to your backend for user validation and signIn
          return
        }
        //TODO: Perform user login given User ID
      }
      
      func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Authorization returned an error: \(error.localizedDescription)")
      }
    
    private func setupAppleIDCredentialObserver() {
      let authorizationAppleIDProvider = ASAuthorizationAppleIDProvider()

      authorizationAppleIDProvider.getCredentialState(forUserID: "currentUserIdentifier") { (credentialState: ASAuthorizationAppleIDProvider.CredentialState, error: Error?) in
        if let error = error {
          print(error)
          // Something went wrong check error state
          return
        }
        switch (credentialState) {
        case .authorized:
          //User is authorized to continue using your app
          break
        case .revoked:
          //User has revoked access to your app
          break
        case .notFound:
          //User is not found, meaning that the user never signed in through Apple ID
          break
        default: break
        }
      }
    }
    
    private func registerForAppleIDSessionChanges() {
      let notificationCenter = NotificationCenter.default
      let sessionNotificationName = NSNotification.Name.ASAuthorizationAppleIDProviderCredentialRevoked

      appleIDSessionObserver = notificationCenter.addObserver(forName: sessionNotificationName, object: nil, queue: nil) { (notification: Notification) in
        //Sign user out
      }
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
      return view.window!
    }*/
}

@available(iOS 13.0, *)
extension AppleSignInClientClass: ASAuthorizationControllerDelegate
{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization)
    {
        //guard let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential else { return }
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName =  appleIDCredential.fullName
            let email = appleIDCredential.email
            print("User Id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))")
            
            //if let authorizationCode = appleIDCredential.authorizationCode,
            if let identifyTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identifyTokenData, encoding: .utf8)
            {
                //let identityTokenString = String(data: identifyTokenData, encoding: .utf8)
                //print("Authorization Code: \(authorizationCode)")
                //print("Identity Token: \(identifyTokenData)")
                print("Identity Token: \(identityTokenString)")
                completionHandler(fullName?.givenName, email, identityTokenString, userIdentifier)
            }
            else
            {
                completionHandler(fullName?.givenName, email, nil, userIdentifier)
                //First time user, perform authentication with the backend
                //TODO: Submit authorization code and identity token to your backend for user validation and signIn
              
            }
            getCredentialsState(userID: userIdentifier)
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
      print("Authorization returned an error: \(error.localizedDescription)")
    }
    
    
}


