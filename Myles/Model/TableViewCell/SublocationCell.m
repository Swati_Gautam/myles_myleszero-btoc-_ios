//
//  SublocationCell.m
//  Myles
//
//  Created by IT Team on 19/08/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "SublocationCell.h"

@implementation SublocationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark- Setting Values
- (void)setvaluesofObject :(BOOL)selectionStatus LocationName: (NSString *)locationName
{
    
    UIFont * font = [UIFont fontWithName:@"OpenSans-Regular"
                                    size:15];
    
    [self.sublocationName setFont:font];
    if (selectionStatus)
    {
        UIImage *buttonImage = [UIImage imageNamed:@"blankBoxIcon"];
        [self.btnCheck setImage:buttonImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *buttonImage = [UIImage imageNamed:@"checkBoxIcon"];
        [self.btnCheck setImage:buttonImage forState:UIControlStateNormal];
    }
    self.sublocationName.text = locationName;
//    [self.sublocationName sizeToFit];
}


@end
