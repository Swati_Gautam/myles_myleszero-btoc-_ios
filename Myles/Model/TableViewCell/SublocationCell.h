//
//  SublocationCell.h
//  Myles
//
//  Created by IT Team on 19/08/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SublocationCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *sublocationName;
@property (strong, nonatomic) IBOutlet UIButton *btnCheck;
@property (nonatomic,assign) BOOL btnCheckStatus;

- (void)setvaluesofObject :(BOOL)selectionStatus LocationName: (NSString *)locationName;

@end
