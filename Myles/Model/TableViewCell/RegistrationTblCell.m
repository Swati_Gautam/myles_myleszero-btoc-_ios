//
//  RegistrationTblCell.m
//  Myles
//
//  Created by Myles on 30/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "RegistrationTblCell.h"

@implementation RegistrationTblCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self viewSetup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)viewSetup
{
    self.txtField.layer.cornerRadius = 2;
    UIColor *color = [UIColor colorWithRed:235. green:235. blue:235. alpha:0.6];
    self.txtField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:self.txtField.text
     attributes:@{NSForegroundColorAttributeName:color}];
    [CommonFunctions setLeftPaddingToTextField:self.txtField andPadding:15];
    [CommonFunctions setTextFieldPlaceHolderText:self.txtField withText:self.txtField.placeholder];
}

@end
