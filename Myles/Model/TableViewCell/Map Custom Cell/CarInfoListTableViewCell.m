

#import "CarInfoListTableViewCell.h"

@implementation CarInfoListTableViewCell

- (void)awakeFromNib {
   // [self.indicator  startAnimating];
    // Initialization code
    self.viewPromoCode.layer.cornerRadius = 4.0;
    self.viewPromoCode.layer.borderWidth = 1.0;
    self.viewPromoCode.layer.borderColor = UIColor.redColor.CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (NSString *)withUppercasedFirstChar:(NSString *) word  {
 NSMutableString *firstCharacter = [word mutableCopy];
 NSString *pattern = @"(^|\\.|\\?|\\!)\\s*(\\p{Letter})";
 NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:NULL];
 [regex enumerateMatchesInString:word options:0 range:NSMakeRange(0, [word length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
 //NSLog(@"%@", result);
 NSRange r = [result rangeAtIndex:2];
 [firstCharacter replaceCharactersInRange:r withString:[[word substringWithRange:r] uppercaseString]];
 }];
 NSLog(@"%@", firstCharacter);
 return firstCharacter;
}

/*
 NSLog(@"aCarDetailInfo.PkgType =%@",aCarDetailInfo.PkgType);
 NSLog(@"aCarDetailInfo.KMIncluded =%@",aCarDetailInfo.KMIncluded);
 NSLog(@"aCarDetailInfo.ExtraKMRate =%@",aCarDetailInfo.ExtraKMRate);
 
 NSString *modelFacilitiesFuel = @"Fuel: full to full";
 NSString *modelFacilitiesKMValue = @"Unlimited";
 [cell setvaluesofCarCellFinal:aCarDetailInfo.Model modelPrize:displayRate modelFacilitiesType:modelFacilitiesFuel modelFacilitiesValue:modelFacilitiesKMValue ImageName:aCarDetailInfo.CarImageThumb BookingType:aCarDetailInfo.PkgType SeatingCapacity:setingCApacity SubLocationId:sublocationIdDetails];
 */

- (void)setvaluesofCarCellFinal : (NSString *)ModelName modelPrize : (NSString *)ModelPrize modelFacilitiesType: (NSString *)ModelFacilitiesType modelFacilitiesValue: (NSString *)ModelFacilitiesValue ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat SubLocationId  : (NSInteger )sublocationID SublocationName : (NSString *)sublocationName  FuelType:(NSString *)fuelType{
    
    NSLog(@"sublocationIDLIST = %ld",(long)sublocationID);
    NSLog(@"ModelPrize =%@",ModelPrize);
    self.subLocationId = [NSString stringWithFormat:@"%ld",(long)sublocationID];
    
    self.FuelTypeLbl.text = fuelType;
    self.FuelTypeLbl.textColor = [UIColor grayColor];

    
    
    if ([sublocationName isEqualToString:@"Choose Nearest Myles Site Location"]) {
        [self.sublocationBtn setHidden:FALSE];
        [self.sublocationBottomLabel setHidden:FALSE];
        self.subLocationLbl.font = [UIFont systemFontOfSize:10];
    }
    else {
        [self.sublocationBtn setHidden:TRUE];
        [self.sublocationBottomLabel setHidden:TRUE];
        self.subLocationLbl.font = [UIFont fontWithName:@"Helvetica-Bold"
                                                   size:11];
    }
    /*if (package.PromotionCode != nil) {
        //cell.lblPromoCode.text = [NSString stringWithFormat:@"%@ Kms Free",package.PromotionCode];
    }
    else {
        // No joy...
    }*/
    self.subLocationLbl.text= sublocationName;
    //self.subLocationAddress = address;
    
//    self.carImgurl = modelImgName;
    self.modelNameLb.text = ModelName;
    //;
    
    
    if (ModelPrize.length > 0) {
        //NSRange range = [ModelPrize rangeOfString:@"."];
        
        /*
         UIFont * font = [UIFont fontWithName:@"Helvetica-Bold"
         size:[UIFont systemFontSize]];
         */
        UIFont * font = [UIFont fontWithName:@"Helvetica-Bold"
                                        size:11];
        
        //[textField setFont:font];
        
        //NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: type attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
        
        NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:ModelPrize attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName :font}];
        
        NSAttributedString *attributedSpace = [[NSAttributedString alloc] initWithString: @" " attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
        
        NSMutableAttributedString *prizeLabel = [[NSMutableAttributedString alloc]initWithString:@"₹" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
        
        [prizeLabel appendAttributedString:attributeString2];
        [prizeLabel appendAttributedString:attributedSpace];
        //[prizeLabel appendAttributedString:attributedString];
        self.modelPrizeLb.attributedText = prizeLabel;
        
        
    }
    
    if (type.length > 0) {
        
        if ([type isEqualToString:@"Hourly"]) {
            UIFont * font = [UIFont fontWithName:@"Helvetica"
                                            size:11];
            
            //ModelFacilitiesType  Fuel: Free
            //ModelFacilitiesValue   40 KMs,Thereafter $10/km
            
             NSArray *fullFacilityName = [ModelFacilitiesType componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
            
             NSArray *fullFacilityValArr = [ModelFacilitiesValue componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
            
            
            NSString *freeKMsInfoStr = [NSString stringWithFormat:@"%@%@", fullFacilityValArr[0], @" Free"];

            NSAttributedString *attributedString2 = [[NSAttributedString alloc] initWithString:fullFacilityName[1] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            
            NSMutableAttributedString *facilityLabel = [[NSMutableAttributedString alloc]initWithString:fullFacilityName[0] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            [facilityLabel appendAttributedString:attributedString2];
            
            NSMutableAttributedString *prizeLabelVal = [[NSMutableAttributedString alloc]initWithString:@"KMs:" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            
            NSMutableAttributedString *attributeString4 = [[NSMutableAttributedString alloc]initWithString:freeKMsInfoStr attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            //modelFacilitiesKMValue
            //ModelFacilitiesValue
            
            
           // NSAttributedString *attributeString5 = [[NSAttributedString alloc]initWithString:@" Thereafter ₹10/km" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName :font}];
            
             //NSString *extraKMsInfoStr = [NSString stringWithFormat:@"%@%@%@", @"Thereafter ₹",ModelFacilitiesValue, @"/km"];
            
            NSAttributedString *attributeString5 = [[NSAttributedString alloc]initWithString:fullFacilityValArr[1] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName :font}];
            
            [prizeLabelVal appendAttributedString:attributeString4];
            
            self.extrafacilitiesLbl.attributedText = facilityLabel;
            self.extrafacilitiesValLbl.attributedText = prizeLabelVal;
            self.extraDisplayTxt.attributedText = attributeString5;
            
        }
        else {
            
            UIFont * font = [UIFont fontWithName:@"Helvetica"
                                            size:11];
            NSArray *fullFacilityName = [ModelFacilitiesType componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
            
            NSAttributedString *attributedString2 = [[NSAttributedString alloc] initWithString: fullFacilityName[1] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            NSMutableAttributedString *facilityLabel = [[NSMutableAttributedString alloc]initWithString:fullFacilityName[0] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            
            NSMutableAttributedString *prizeLabelVal = [[NSMutableAttributedString alloc]initWithString:@"KMs:" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            
            NSMutableAttributedString *attributeString4 = [[NSMutableAttributedString alloc]initWithString:ModelFacilitiesValue attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            
            [facilityLabel appendAttributedString:attributedString2];
            [prizeLabelVal appendAttributedString:attributeString4];
            
            self.extrafacilitiesLbl.attributedText = facilityLabel;
            self.extrafacilitiesValLbl.attributedText = prizeLabelVal;
            self.extraDisplayTxt.text = @" ";
        }
    }
    
    
    self.seaterLb.text = [NSString stringWithFormat:@"%@ Seater",seat];
    self.seaterLb.textColor = [UIColor grayColor];

//    [self.indicator startAnimating];
    
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//        
//        /* Fetch the image from the server... */
//        NSURL *url=[NSURL URLWithString:modelImgName];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            SDWebImageManager *manager = [SDWebImageManager sharedManager];
//            [manager downloadImageWithURL:url
//                                  options:0
//                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                     // progression tracking code
//                                 }
//                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                                    if (image) {
//                                        // do something with image
//                                        //self.carImg.contentMode = UIViewContentModeScaleAspectFit;
//                                        self.carImg.contentMode = UIViewContentModeScaleAspectFill;
//                                        //self.carImg.contentMode = UIViewContentModeScaleToFill;
//                                        //[self.carImg setClipsToBounds:YES];
//                                        
//                                        self.carImg.backgroundColor = [UIColor clearColor];
//                                        self.carImg.image = image;
//                                    }
//                                    else
//                                    {
//                                        //self.carImg.contentMode = UIViewContentModeScaleAspectFit;
//                                        self.carImg.contentMode = UIViewContentModeScaleAspectFill;
//                                        //self.carImg.contentMode = UIViewContentModeScaleToFill;
//                                        //[self.carImg setClipsToBounds:YES];
//                                        
//                                        self.carImg.backgroundColor = [UIColor clearColor];
//                                        
//                                        
//                                        //self.carImg.backgroundColor = [UIColor clearColor];
//                                        self.carImg.image = [UIImage imageNamed:k_defaultCarFullImg];
//                                    }
//                                }];
//            [self.indicator stopAnimating];
//            self.indicator.hidden = true;
//            //self.carImg.contentMode=UIViewContentModeScaleAspectFit;
//        });
//    });
    
}



/*
- (void)setvaluesofCarCellFinal : (NSString *)ModelName modelPrize : (NSString *)ModelPrize modelFacilitiesType: (NSString *)ModelFacilitiesType modelFacilitiesValue: (NSString *)ModelFacilitiesValue ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat SubLocationId  : (NSInteger )sublocationID{


    NSLog(@"ModelPrize =%@",ModelPrize);
    self.subLocationId = [NSString stringWithFormat:@"%ld",(long)sublocationID];
    //self.subLocationAddress = address;
    
    self.carImgurl = modelImgName;
    self.modelNameLb.text = ModelName;
    //;

    
    if (ModelPrize.length > 0) {
        NSRange range = [ModelPrize rangeOfString:@"."];
        
        
        UIFont * font = [UIFont fontWithName:@"Helvetica-Bold"
                                        size:11];
        
        //[textField setFont:font];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: type attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
        
        NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:[ModelPrize substringToIndex:range.location] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName :font}];
        
         NSAttributedString *attributedSpace = [[NSAttributedString alloc] initWithString: @" " attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
        
        NSMutableAttributedString *prizeLabel = [[NSMutableAttributedString alloc]initWithString:@"₹" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
        
        [prizeLabel appendAttributedString:attributeString2];
        [prizeLabel appendAttributedString:attributedSpace];
        [prizeLabel appendAttributedString:attributedString];
        self.modelPrizeLb.attributedText = prizeLabel;
        
       
    }
    
    if (type.length > 0) {
        
        if ([type isEqualToString:@"Hourly"]) {
            UIFont * font = [UIFont fontWithName:@"Helvetica"
                                            size:11];
           
            NSAttributedString *attributedString2 = [[NSAttributedString alloc] initWithString: @"Free" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            
            NSMutableAttributedString *facilityLabel = [[NSMutableAttributedString alloc]initWithString:@"Fuel: " attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            [facilityLabel appendAttributedString:attributedString2];
  
             NSMutableAttributedString *prizeLabelVal = [[NSMutableAttributedString alloc]initWithString:@"KMs:" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            
            NSMutableAttributedString *attributeString4 = [[NSMutableAttributedString alloc]initWithString:@"40 KMs Free" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            
            NSAttributedString *attributeString5 = [[NSAttributedString alloc]initWithString:@" Thereafter ₹10/km" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName :font}];

            [prizeLabelVal appendAttributedString:attributeString4];
            
            self.extrafacilitiesLbl.attributedText = facilityLabel;
            self.extrafacilitiesValLbl.attributedText = prizeLabelVal;
            self.extraDisplayTxt.attributedText = attributeString5;

        }
        else {
            
            UIFont * font = [UIFont fontWithName:@"Helvetica"
                                            size:11];
            NSAttributedString *attributedString2 = [[NSAttributedString alloc] initWithString: @"full to full" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            NSMutableAttributedString *facilityLabel = [[NSMutableAttributedString alloc]initWithString:@"Fuel:" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            
            NSMutableAttributedString *prizeLabelVal = [[NSMutableAttributedString alloc]initWithString:@"KMs:" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
            
            NSMutableAttributedString *attributeString4 = [[NSMutableAttributedString alloc]initWithString:@"Unlimited" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
            
            [facilityLabel appendAttributedString:attributedString2];
            [prizeLabelVal appendAttributedString:attributeString4];

            self.extrafacilitiesLbl.attributedText = facilityLabel;
            self.extrafacilitiesValLbl.attributedText = prizeLabelVal;
            self.extraDisplayTxt.text = @" ";
        }
    }
    
    
    self.seaterLb.text = [NSString stringWithFormat:@"%@ Seater",seat];
    [self.indicator startAnimating];
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        
        // Fetch the image from the server...
        NSURL *url=[NSURL URLWithString:modelImgName];
        dispatch_async(dispatch_get_main_queue(), ^{
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:url
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // do something with image
                                        //self.carImg.contentMode = UIViewContentModeScaleAspectFit;
                                        self.carImg.contentMode = UIViewContentModeScaleAspectFill;
                                        //self.carImg.contentMode = UIViewContentModeScaleToFill;
                                        //[self.carImg setClipsToBounds:YES];

                                        self.carImg.backgroundColor = [UIColor yellowColor];
                                        self.carImg.image = image;
                                    }
                                    else
                                    {
                                        //self.carImg.contentMode = UIViewContentModeScaleAspectFit;
                                        self.carImg.contentMode = UIViewContentModeScaleAspectFill;
                                        //self.carImg.contentMode = UIViewContentModeScaleToFill;
                                        //[self.carImg setClipsToBounds:YES];
                                        
                                        self.carImg.backgroundColor = [UIColor yellowColor];


                                        //self.carImg.backgroundColor = [UIColor clearColor];
                                        self.carImg.image = [UIImage imageNamed:k_defaultCarFullImg];
                                    }
                                }];
            [self.indicator stopAnimating];
            self.indicator.hidden = true;
            //self.carImg.contentMode=UIViewContentModeScaleAspectFit;
        });
    });

}
 */


- (void)setvaluesofCarCell : (NSString *)ModelName modelPrize : (NSString *)ModelPrize
 ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat {
    
    NSLog(@"ModelPrize =%@",ModelPrize);
    
    self.carImgurl = modelImgName;
    self.modelNameLb.text = ModelName;
    if (ModelPrize.length > 0) {
        NSRange range = [ModelPrize rangeOfString:@"."];
       
        /*
         UIFont * font = [UIFont fontWithName:@"Helvetica-Bold"
                                        size:[UIFont systemFontSize]];
         */
        UIFont * font = [UIFont fontWithName:@"Helvetica-Bold"
                                        size:11];

        
        //[textField setFont:font];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: type attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
        
        NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:[ModelPrize substringToIndex:range.location] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName :font}];
       
        NSMutableAttributedString *prizeLabel = [[NSMutableAttributedString alloc]initWithString:@"₹" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : font}];
       
        [prizeLabel appendAttributedString:attributeString2];
        [prizeLabel appendAttributedString:attributedString];
        self.modelPrizeLb.attributedText = prizeLabel;
    }
    self.seaterLb.text = [NSString stringWithFormat:@"%@ Seater",seat];
//    [self.indicator startAnimating];
//    
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//        
//        /* Fetch the image from the server... */
//        NSURL *url=[NSURL URLWithString:modelImgName];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            SDWebImageManager *manager = [SDWebImageManager sharedManager];
//            [manager downloadImageWithURL:url
//                                  options:0
//                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                     // progression tracking code
//                                 }
//                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                                    if (image) {
//                                        // do something with image
//                                        self.carImg.backgroundColor = [UIColor clearColor];
//                                        self.carImg.image = image;
//                                    }
//                                    else
//                                    {
//                                        self.carImg.backgroundColor = [UIColor clearColor];
//                                        self.carImg.image = [UIImage imageNamed:k_defaultCarFullImg];
//                                        
//                                    }
//                                }];
//            [self.indicator stopAnimating];
//            self.indicator.hidden = true;
//            self.carImg.contentMode=UIViewContentModeScaleAspectFit;
//        });
//    });
}



- (void)setvaluesofObject : (NSString *)modelPrize ModelName : (NSString *)modelName ImageName : (NSString *)modelImgName ButtonStatus : (NSString *)btnlabel BookingType : (NSString *)type SeatingCapacity : (NSString *)seating CarModelId : (NSString *)carModelID SubLocationId  : (NSInteger )sublocationID SubLocationAddress : (NSString *)address
{
    self.carImgurl = modelImgName;
    self.modelNameLb.text = modelName;
    if (modelPrize.length > 0) {
        NSRange range = [modelPrize rangeOfString:@"."];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: type attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(11)}];
        NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:[modelPrize substringToIndex:range.location] attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(14)}];
        NSMutableAttributedString *prizeLabel = [[NSMutableAttributedString alloc]initWithString:@"₹" attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_REGULAR_Motor(14)}];
        [prizeLabel appendAttributedString:attributeString2];
        [prizeLabel appendAttributedString:attributedString];
        self.modelPrizeLb.attributedText = prizeLabel;
    }
    self.seaterLb.text = [NSString stringWithFormat:@"%@ Seater",seating];
//    [self.indicator startAnimating];
//    
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//        
//        /* Fetch the image from the server... */
//        NSURL *url=[NSURL URLWithString:modelImgName];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            SDWebImageManager *manager = [SDWebImageManager sharedManager];
//            [manager downloadImageWithURL:url
//                                  options:0
//                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                     // progression tracking code
//                                 }
//                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                                    if (image) {
//                                        // do something with image
//                                        self.carImg.backgroundColor = [UIColor clearColor];
//                                        self.carImg.image = image;
//                                    }
//                                    else
//                                    {
//                                        self.carImg.backgroundColor = [UIColor clearColor];
//                                        self.carImg.image = [UIImage imageNamed:k_defaultCarFullImg];
//                                        
//                                    }
//                                }];
//            [self.indicator stopAnimating];
//            self.indicator.hidden = true;
//            self.carImg.contentMode=UIViewContentModeScaleAspectFit;
//        });
//    });
    [self.bookingBtn setTitle:btnlabel forState:UIControlStateNormal];
    if ([btnlabel isEqualToString:@"BOOK NOW"]) {
       // [self.bookingBtn addTarget:self action:@selector(selector) forControlEvents:(UIControlEvents)
    }
    self.carModelID  = carModelID;
    self.subLocationId = [NSString stringWithFormat:@"%ld",(long)sublocationID];
    self.subLocationAddress = address;
}

@end
