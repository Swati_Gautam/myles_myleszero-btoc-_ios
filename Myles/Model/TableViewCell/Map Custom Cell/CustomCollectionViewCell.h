//
//  CustomCollectionViewCell.h
//  Myles
//
//  Created by Myles   on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>
//#import <SDWebImage/SDWebImageDownloader.h>
//#import <SDWebImage/SDWebImageManager.h>


@interface CustomCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *modelImg;
@property (weak, nonatomic) IBOutlet UILabel *modelName;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) NSString *carModelID;

- (void)setvaluesofObject : (NSString *)modelName ImageName : (NSString *)modelImgName CarModel : (NSString *)carModelId CarImage : (NSString *)fullCarImg;
@property (strong, nonatomic) NSString *FullcarImgName;
@end
