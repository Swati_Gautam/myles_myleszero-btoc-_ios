//
//  CustomCollectionViewCell.m
//  Myles
//
//  Created by Myles   on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "CustomCollectionViewCell.h"

@implementation CustomCollectionViewCell

- (void)awakeFromNib {
    [self.indicator  startAnimating];
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self.indicator startAnimating];
    }
    return self;
}

- (void)setvaluesofObject : (NSString *)modelName ImageName : (NSString *)modelImgName CarModel : (NSString *)carModelId CarImage : (NSString *)fullCarImg
{
    [self.indicator startAnimating];
    self.FullcarImgName = fullCarImg;
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        
        /* Fetch the image from the server... */
        /*
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",modelImgName]];
        dispatch_async(dispatch_get_main_queue(), ^{
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:url
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image)
                                    {
                                        self.modelImg.backgroundColor = [UIColor clearColor];
                                        self.modelImg.image = image;
                                        
                                    }
                                    else
                                    {
                                        self.modelImg.backgroundColor = [UIColor clearColor];
                                        
                                        self.modelImg.image = [UIImage imageNamed:k_defaultCarFullImg];
                                    }
                                }];
            [self.indicator stopAnimating];
            self.indicator.hidden = true;
            self.modelImg.contentMode=UIViewContentModeScaleAspectFit;
        });
    });
         */
    });
    self.modelName.text = modelName;
    self.modelName.adjustsFontSizeToFitWidth = true;
    self.carModelID = carModelId;
}
@end
