
#import <UIKit/UIKit.h>
//#import <SDWebImage/SDWebImageDownloader.h>
//#import <SDWebImage/SDWebImageManager.h>


@interface CarInfoListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *bookingBtn;
@property (weak, nonatomic) IBOutlet UIButton *sublocationBtn;
@property (weak, nonatomic) IBOutlet UIView *sublocationBottomLabel;

@property (weak, nonatomic) IBOutlet UIImageView *carImg;
@property (weak, nonatomic) IBOutlet UILabel *modelPrizeLb;
@property (weak, nonatomic) IBOutlet UILabel *modelNameLb;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *seaterLb;
@property (weak, nonatomic) IBOutlet UILabel *extrafacilitiesLbl;
@property (weak, nonatomic) IBOutlet UILabel *extrafacilitiesValLbl;
@property (weak, nonatomic) IBOutlet UILabel *extraDisplayTxt;
@property (weak, nonatomic) IBOutlet UILabel *subLocationLbl;
@property (weak, nonatomic) IBOutlet UILabel *FuelTypeLbl;

@property (weak, nonatomic) IBOutlet UILabel *lblPromoCode;

@property (weak, nonatomic) IBOutlet UIView *viewPromoCode;




- (void)setvaluesofObject : (NSString *)modelPrize ModelName : (NSString *)modelName ImageName : (NSString *)modelImgName ButtonStatus : (NSString *)btnlabel BookingType : (NSString *)type SeatingCapacity : (NSString *)seating CarModelId : (NSString *)carModelID SubLocationId  : (NSInteger )sublocationID SubLocationAddress : (NSString *)address;

- (void)setvaluesofCarCell : (NSString *)ModelName modelPrize : (NSString *)ModelPrize
                 ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat;

//- (void)setvaluesofCarCellFinal : (NSString *)ModelName modelPrize : (NSString *)ModelPrize modelFacilitiesType: (NSString *)ModelFacilitiesType modelFacilitiesValue: (NSString *)ModelFacilitiesValue ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat;

//- (void)setvaluesofCarCellFinal : (NSString *)ModelName modelPrize : (NSString *)ModelPrize modelFacilitiesType: (NSString *)ModelFacilitiesType modelFacilitiesValue: (NSString *)ModelFacilitiesValue ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat SubLocationId  : (NSInteger )sublocationID;

//- (void)setvaluesofCarCellFinal : (NSString *)ModelName modelPrize : (NSString *)ModelPrize modelFacilitiesType: (NSString *)ModelFacilitiesType modelFacilitiesValue: (NSString *)ModelFacilitiesValue ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat SubLocationId  : (NSInteger )sublocationID SublocationName : (NSString *)sublocationName;

- (void)setvaluesofCarCellFinal : (NSString *)ModelName modelPrize : (NSString *)ModelPrize modelFacilitiesType: (NSString *)ModelFacilitiesType modelFacilitiesValue: (NSString *)ModelFacilitiesValue ImageName : (NSString *)modelImgName BookingType : (NSString *)type SeatingCapacity : (NSString *)seat SubLocationId  : (NSInteger )sublocationID SublocationName : (NSString *)sublocationName  FuelType:(NSString *)fuelType;


@property (strong, nonatomic) NSString *carModelID;
@property (strong, nonatomic) NSString *subLocationId;
@property (strong, nonatomic) NSString *subLocationAddress;
@property (strong, nonatomic) NSString *carImgurl;


@end
