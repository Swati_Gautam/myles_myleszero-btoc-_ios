//
//  ProfileTableViewCell.swift
//  Myles
//
//  Created by Nowfal E Salam on 22/11/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var userLastNametxt: UITextField!
    @IBOutlet weak var userFirstNametxt: UITextField!
    
    @IBOutlet weak var userEmailIdtxt: UITextField!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var userPhonetxt: UITextField!
    
    @IBOutlet weak var userHomeAddressTxtview: UITextView!
    
    @IBOutlet weak var userPasswordtxt: UITextField!
    
    @IBOutlet weak var changePasswordBtn: UIButton!
    
    @IBOutlet weak var userDrivingLicenceImg: UIImageView!
    
    @IBOutlet weak var userAddressProofImg: UIImageView!
    
    @IBOutlet weak var addressProofVerfiedStatus: UILabel!
    @IBOutlet weak var drivingLicenceVerfiedStatus: UILabel!
    
    @IBOutlet weak var btnDob: Button!
    
    @IBOutlet weak var ImgeditButton: UIButton!
    
    @IBOutlet weak var imgAddProfButton: UIButton!
    @IBOutlet weak var imgDeleteButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUserBasicInfomation(firstName:String ,  lastName : String , userIntraction :Bool )
    {
   
    self.userFirstNametxt.text = firstName
    self.userLastNametxt.text = lastName
    self.userFirstNametxt.isUserInteractionEnabled = userIntraction
    self.userLastNametxt.isUserInteractionEnabled = userIntraction
    self.userLastNametxt.autocapitalizationType = .sentences
    self.userFirstNametxt.autocapitalizationType = .sentences
    
    }
    func setEmailIdLb(userEmailId: String , phoneNumber: String , userIntartion: Bool)
    {
        self.userEmailIdtxt.autocapitalizationType = .sentences
        self.userEmailIdtxt.text = userEmailId
        self.userEmailIdtxt.isUserInteractionEnabled = userIntartion
        self.userPhonetxt.text = phoneNumber
        self.userPhonetxt.isUserInteractionEnabled = false
        
        if (userIntartion)
        {
            self.phoneLabel.textColor = UIColor.lightGray
        }
        else{
            self.phoneLabel.textColor = UIColor.darkText

        }
    }
    func setUserHomeAddress(userHomeAddress: String , placeAddress: String, userIntraction: Bool)
    {
        self.userHomeAddressTxtview.isScrollEnabled = true
        self.userHomeAddressTxtview.autocapitalizationType = .sentences
        self.userHomeAddressTxtview.isUserInteractionEnabled = userIntraction
        self.userHomeAddressTxtview.text = userHomeAddress
    }
    
    func setuserProofDetails( drivingLicenceUrl:String, addressproofUrl:String, licenceStatus:Bool,  addressStatus: Bool, userIntraction: Bool, licenceDoc:String, addressDoc:String)
    {
        self.userDrivingLicenceImg.layer.cornerRadius = 8;
        self.userDrivingLicenceImg.layer.borderColor = UIColor(red: 215, green: 218, blue: 220, alpha: 1).cgColor
        self.userDrivingLicenceImg.layer.borderWidth = 1.5;
        self.userAddressProofImg.layer.cornerRadius = 8;
        self.userAddressProofImg.layer.borderColor = UIColor(red: 215, green: 218, blue: 220, alpha: 1).cgColor
        self.userAddressProofImg.layer.borderWidth = 1.5;
    
        if(userIntraction){
            ImgeditButton.isHidden = false
            if(drivingLicenceUrl.contains(".jpg")||drivingLicenceUrl.contains(".png")){
                imgDeleteButton.isHidden = false
            }
            else{
                imgDeleteButton.isHidden = true
            }
            if(addressproofUrl.contains(".jpg")||addressproofUrl.contains(".png")){
                imgAddProfButton.isHidden = false
            }
            else{
                imgAddProfButton.isHidden = true
            }
        }
        else{
            ImgeditButton.isHidden = true
            imgDeleteButton.isHidden = true
            imgAddProfButton.isHidden = true
        }

        if (drivingLicenceUrl.count > 0)
        {
            
            userDrivingLicenceImg.sd_setImage(with: URL(string: drivingLicenceUrl), placeholderImage: UIImage(named: k_defaultCarImg))

        }
        else{
            imgDeleteButton.isHidden = true
            userDrivingLicenceImg.image = UIImage(named: k_defaultCarImg)

        }
        
    
        if (addressproofUrl.count > 0) {
            
            userAddressProofImg.sd_setImage(with: URL(string: addressproofUrl), placeholderImage: UIImage(named: k_defaultCarImg))

    
        }
        else{
            imgAddProfButton.isHidden = true
            userAddressProofImg.image = UIImage(named: k_defaultCarImg)

        }
       
    
        if (licenceStatus) {
            self.drivingLicenceVerfiedStatus.textColor = UIColor.green
            self.drivingLicenceVerfiedStatus.text = "VERIFIED"
        }
        if (addressStatus) {
            self.addressProofVerfiedStatus.textColor = UIColor.green
            self.addressProofVerfiedStatus.text = "VERIFIED"
        }
    
    }

}
