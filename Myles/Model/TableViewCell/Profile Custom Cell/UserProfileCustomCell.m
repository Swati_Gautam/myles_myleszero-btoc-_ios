//
//  UserProfileCustomCell.m
//  Myles
//
//  Created by Myles   on 14/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import "UserProfileCustomCell.h"
#import "Validate.h"

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_()"

#pragma mark - Default Methods

@implementation UserProfileCustomCell

/*
 @property (weak, nonatomic) IBOutlet UIButton *ImgeditButton;
 @property (weak, nonatomic) IBOutlet UIButton *imgDeleteButton;
 @property (weak, nonatomic) IBOutlet UIButton *imgAddProfButton;
 */

- (void)awakeFromNib {
    // Initialization code
    NSLog(@"Enters in this function");
}


- (void )ImgeditButtonAction : (UIButton *)sender
{
    NSLog(@"Image edit button pressed");
}

- (void )imgDeleteButtonAction : (UIButton *)sender
{
    NSLog(@"Image delete button pressed");
}

- (void )imgAddProfButtonButtonAction : (UIButton *)sender
{
    NSLog(@"Image address proof delete button pressed");
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) imageEditAction : (UITapGestureRecognizer *)gesture {
    
    
}

/*

- (void) imageEditAction : (UITapGestureRecognizer *)gesture
{
    if (![self.editView isHidden])
    {
        CGPoint coords = [gesture locationInView:gesture.view];
        CGFloat width = CGRectGetWidth(self.frame)/2;
        if (coords.x < width)
        {
            self.editViewLeadingSpace.constant = CGRectGetMinX(self.userDrivingLicenceImg.frame);
        }
        else
        {
            self.editViewLeadingSpace.constant = CGRectGetMinX(self.userAddressProofImg.frame) + width;
        }
        [self updateConstraintsIfNeeded];
        self.editView.hidden = false;
    }
}
 */

#pragma mark - First Cell

/*
- (void)setUserBasicInfomation : (NSString *)firstName UserIntartion : (BOOL)userIntraction
{
    self.userFirstNametxt.text = firstName;
    self.userFirstNametxt.userInteractionEnabled = userIntraction;
    self.userFirstNametxt.autocapitalizationType = UITextAutocapitalizationTypeSentences;
}
 */





- (void)setUserBasicInfomation : (NSString *)firstName  LastName :(NSString *)lastname UserIntartion : (BOOL)userIntraction
{
    //firstName = [firstName uppercaseString];
    //firstName = [self handleStringCase:firstName];
    //lastname = [lastname uppercaseString];
    //lastname = [self handleStringCase:lastname];
    
    self.userFirstNametxt.text = firstName;
    self.userLastNametxt.text = lastname;
    self.userFirstNametxt.userInteractionEnabled = userIntraction;
    self.userLastNametxt.userInteractionEnabled = userIntraction;
    self.userLastNametxt.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.userFirstNametxt.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
}


-(NSString *)handleStringCase:(NSString *) testString {
    
    NSString *prefix,*postfix;
    NSString *fullString = testString;
    if ([fullString length] > 0)
        prefix = [fullString substringToIndex:1];
    
    NSString *stringSmall = [fullString substringWithRange:NSMakeRange(1, fullString.length - 1)];
    postfix = [stringSmall lowercaseString];
    NSString *completText = [prefix stringByAppendingString:postfix];
    return completText;
}

#pragma mark - Second Cell
-(void)setEmailIdLb:(NSString *)userEmailId Phone : (NSString *)phoneNumber UserIntartion : (BOOL)userIntraction
{
    //userEmailId = [userEmailId uppercaseString];
    //userEmailId = [self handleStringCase:userEmailId];
    self.userEmailIdtxt.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.userEmailIdtxt.text = userEmailId;
    self.userEmailIdtxt.userInteractionEnabled = userIntraction;
    self.userPhonetxt.text = phoneNumber;
    self.userPhonetxt.userInteractionEnabled = userIntraction;
    
    if (userIntraction)
    {
        self.phoneLabel.textColor = [UIColor lightGrayColor];
    }
    else
        self.phoneLabel.textColor = [UIColor darkTextColor];
}
#pragma mark - Third Cell
-(void) setUserHomeAddress:(NSString *)userHomeAddress PlaceAddress : (NSString *) placeAddress UserIntartion : (BOOL)userIntraction
{
    //userHomeAddress = [userHomeAddress uppercaseString];
    self.userHomeAddressTxtview.scrollEnabled = TRUE;
    self.userHomeAddressTxtview.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.userHomeAddressTxtview.userInteractionEnabled = userIntraction;
}



#pragma mark - Forth Cell
-(void) setuserProofDetails : (NSString *)drivingLicenceUrl AddressProof :(NSString *)addressproofUrl LicenceStatus :(NSString *)licenceStatus AddressStatus :(NSString *)addressStatus UserIntartion  : (BOOL)userIntraction LicenceDoc :(NSString *)licenceDoc AddressDoc :(NSString *)addressDoc
{
    
    NSLog(@"userIntraction status =%d",userIntraction);
    
    UIActivityIndicatorView *indicator1 = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    //NSLog(@"f :%@, c : %@",NSStringFromCGPoint(self.userAddressProofImg.center), NSStringFromCGPoint(self.userDrivingLicenceImg.center));
    
    indicator1.center =  self.userDrivingLicenceImg.center;
    indicator1.center = CGPointMake(indicator1.center.x-10, indicator1.center.y-25);
    
    UIActivityIndicatorView *indicator2 = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    indicator2.center =  self.userAddressProofImg.center;
    indicator2.center = CGPointMake(indicator2.center.x-10, indicator2.center.y-25);
    
    [indicator2 startAnimating];
    self.userDrivingLicenceImg.layer.cornerRadius = 8;
    self.userDrivingLicenceImg.layer.borderColor = [UIColor colorWithRed:215 green:218 blue:220 alpha:1].CGColor;
    self.userDrivingLicenceImg.layer.borderWidth = 1.5;
    self.userAddressProofImg.layer.cornerRadius = 8;
    self.userAddressProofImg.layer.borderColor = [UIColor colorWithRed:215 green:218 blue:220 alpha:1].CGColor;
    self.userAddressProofImg.layer.borderWidth = 1.5;
    self.editView.hidden = true;
    
    //NSLog(@"---- f :%@, c : %@",NSStringFromCGPoint(indicator1.center), NSStringFromCGPoint(indicator2.center));
    
    if (drivingLicenceUrl.length > 0)
    {
        [indicator1 startAnimating];
        [[self viewWithTag:1] addSubview:indicator1];
        
    }
    if (addressproofUrl.length > 0)
    {
        [indicator2 startAnimating];
        [[self viewWithTag:2] addSubview:indicator2];
        
    }
    self.userInteractionEnabled = false;
    
    //Image
    if (drivingLicenceUrl.length > 0)
    {
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:drivingLicenceUrl]];
            UIImage *image = [UIImage imageWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (image != nil) {
                    self.userDrivingLicenceImg.image = image;
                    self.drivingLicenceDocName = licenceDoc;
                    [indicator1 removeFromSuperview];
                }
                else
                {
                    self.userDrivingLicenceImg.image = [UIImage imageNamed:k_defaultCarImg];
                    [indicator1 removeFromSuperview];
                }
            });
        });
    }
    if (addressproofUrl.length > 0) {
        
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:addressproofUrl]];
            UIImage *image = [UIImage imageWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (image != nil)
                {
                    self.userAddressProofImg.image = image;
                    self.addressDocName = addressDoc;
                    [indicator2 removeFromSuperview];
                    
                }
                else
                {
                    self.userAddressProofImg.image = [UIImage imageNamed:k_defaultCarImg];
                    [indicator2 removeFromSuperview];
                    
                }
            });
        });
        
    }
    //GEsture
    if (userIntraction)
    {
        self.userInteractionEnabled = true;
        UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageEditAction:)];
        [self addGestureRecognizer:tapgesture];
        self.editView.hidden = false;
    }
    if ([licenceStatus integerValue] == 1) {
        self.drivingLicenceVerfiedStatus.textColor = [UIColor greenColor];
        self.drivingLicenceVerfiedStatus.text = @"VERIFIED";
    }
    if ([addressStatus integerValue] == 1) {
        self.addressProofVerfiedStatus.textColor = [UIColor greenColor];
        self.addressProofVerfiedStatus.text = @"VERIFIED";
    }
    
    
    
    
    //    [indicator1 stopAnimating];
    //    [indicator1 removeFromSuperview];
    //    [indicator2 stopAnimating];
    //    [indicator2 removeFromSuperview];
    
}
- (void)rowOneEditMode : (BOOL)status
{
    self.userFirstNametxt.userInteractionEnabled = status;
    self.userLastNametxt.userInteractionEnabled = status;

}
- (void)rowTwoEditMode : (BOOL)status
{
    self.userEmailIdtxt.userInteractionEnabled = status;
    
    if (status)
    {
        
        NSString *postTxt = [self.userEmailIdtxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(postTxt.length)
        {
            if (![Validate isValidEmailId:postTxt] )
            {
                [self.userEmailIdtxt setText:@""];
                [CommonFunctions alertTitle:KSorry withMessage:KAValidEmailId];
            }
        }
    }
    
    if (status)
    {
        self.phoneLabel.textColor = [UIColor lightGrayColor];
    }
    else
        self.phoneLabel.textColor = [UIColor darkTextColor];
}
- (void)rowThreeEditMode : (BOOL)status
{
    self.userHomeAddressTxtview.userInteractionEnabled = status;
}

#pragma -mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //textField.placeholder = @"Your Placeholdertext";
}
@end
