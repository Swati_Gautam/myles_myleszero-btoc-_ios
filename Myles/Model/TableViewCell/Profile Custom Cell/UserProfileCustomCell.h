//
//  UserProfileCustomCell.h
//  Myles
//
//  Created by Myles   on 14/08/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>
#import "Myles-Swift.h"

@interface UserProfileCustomCell : UITableViewCell<UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate>
//First cell

@property (weak, nonatomic) IBOutlet UITextField *userFirstNametxt;
@property (weak, nonatomic) IBOutlet UITextField *userLastNametxt;

//Second Cell
@property (weak, nonatomic) IBOutlet UITextField *userEmailIdtxt;
@property (weak, nonatomic) IBOutlet UITextField *userPhonetxt;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

//Third Cell
@property (weak, nonatomic) IBOutlet UITextView *userHomeAddressTxtview;


//fourth cell
@property (weak, nonatomic) IBOutlet UITextField *userPasswordtxt;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordBtn;



//Fifth Cell
@property (weak, nonatomic) IBOutlet UIImageView *userDrivingLicenceImg;
@property (weak, nonatomic) IBOutlet UIImageView *userAddressProofImg;
@property (weak, nonatomic) IBOutlet UILabel *drivingLicenceVerfiedStatus;
@property (weak, nonatomic) IBOutlet UILabel *addressProofVerfiedStatus;


@property (weak, nonatomic) IBOutlet Button *btnDob;

// Button ImgeditButton - next to document - on click on this button addressproof and driving license will be edited
//imageDeleteButton and imgAddProfButton - on click on these images documents will be deleted.
// Initially all buttons will be hidden, when user click on the "EDIT" button of th boottom hidden property of these button will change to false
//


@property (weak, nonatomic) IBOutlet UIButton *ImgeditButton;
@property (weak, nonatomic) IBOutlet UIButton *imgDeleteButton;
@property (weak, nonatomic) IBOutlet UIButton *imgAddProfButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;

//Edit View
@property (weak, nonatomic) IBOutlet UIView *editView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *editViewLeadingSpace;

//Document Status


//Document Name
@property (strong, nonatomic) NSString *addressDocName  ;
@property (strong, nonatomic) NSString *drivingLicenceDocName;


//document edit section
- (void )ImgeditButtonAction : (UIButton *)sender;
- (void )imgDeleteButtonAction : (UIButton *)sender;
- (void )imgAddProfButtonButtonAction : (UIButton *)sender;




//Class Methods to set values of component 
- (void)setUserBasicInfomation : (NSString *)firstName  LastName :(NSString *)lastname UserIntartion : (BOOL)userIntraction;
//- (void)setUserBasicInfomation : (NSString *)firstName UserIntartion : (BOOL)userIntraction;


-(void)setEmailIdLb:(NSString *)userEmailId Phone : (NSString *)phoneNumber UserIntartion : (BOOL)userIntraction
;
-(void) setUserHomeAddress:(NSString *)userHomeAddress PlaceAddress : (NSString *) placeAddress UserIntartion : (BOOL)userIntraction;
-(void) setuserProofDetails : (NSString *)drivingLicenceUrl AddressProof :(NSString *)addressproofUrl LicenceStatus :(NSString *)licenceStatus AddressStatus :(NSString *)addressStatus UserIntartion  : (BOOL)userIntraction LicenceDoc :(NSString *)licenceDoc AddressDoc :(NSString *)addressDoc;

- (void)rowOneEditMode : (BOOL)status;
- (void)rowTwoEditMode : (BOOL)status;
- (void)rowThreeEditMode : (BOOL)status;

-(NSString *)handleStringCase:(NSString *) testString;
@end
