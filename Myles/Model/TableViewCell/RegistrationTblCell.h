//
//  RegistrationTblCell.h
//  Myles
//
//  Created by Myles on 30/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <UIKit/UIKit.h>

@interface RegistrationTblCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *txtField;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;

@end
