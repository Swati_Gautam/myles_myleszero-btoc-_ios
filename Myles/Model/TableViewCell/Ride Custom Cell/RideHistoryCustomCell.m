
#import "RideHistoryCustomCell.h"
#import "RideHistoryDetails.h"

@implementation RideHistoryCustomCell

{
    NSString *rideStatusMessage;
    NSAttributedString *attributeStringRegular;
    NSAttributedString * attributeStringRideStatus;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) getCellInfo : (NSDictionary *)dic
{
    RideHistoryDetails *ridedetail = dic.copy;
    self.rideStatus = true;
    
    //NSLog(@"History Trip Status :%@", ridedetail.Tripstatus);
    NSLog(@"ride history page cell data = %@", dic.copy);
    
    
    NSDateFormatter *formet = [[NSDateFormatter alloc]init];
    [formet setDateFormat:@"MM-dd-yyyy"];
    NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    formet.locale = twelveHourLocale;
    NSDate *pickupdate = [formet dateFromString:ridedetail.PickupDate];
    NSString *pickupTime = ridedetail.PickupTime;
    NSTimeInterval interval = 60 * 60 * [[pickupTime substringToIndex:2] integerValue] + 60 * [[pickupTime substringFromIndex:2] integerValue];
    formet = [[NSDateFormatter alloc] init];
    formet.locale = twelveHourLocale;
    [formet setDateFormat:@"hh:mm a"];
    pickupTime = [formet stringFromDate:[pickupdate dateByAddingTimeInterval:interval]];
    formet.dateFormat = @"MM-dd-YYYY hh:mm a";
    
    self.bookingId = ridedetail.Bookingid;
    
    //Get Week Day and month
    [formet setDateFormat:@"MM-dd-yyyy"];
    
    NSCalendar* calender = [NSCalendar currentCalendar];
    NSDateComponents* component = [calender components:NSCalendarUnitWeekday fromDate:pickupdate];
    NSInteger weekDay = [component weekday];
    component = [calender components:NSCalendarUnitDay fromDate:pickupdate];
    NSInteger pickday = [component day];
    component = [calender components:NSCalendarUnitMonth fromDate:pickupdate];
    NSInteger monthday = [component month];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = twelveHourLocale;
    NSString *weekdayString = [[formatter weekdaySymbols] objectAtIndex:weekDay - 1];
    NSString *monthdaySymbol = [[formatter shortMonthSymbols] objectAtIndex:monthday -1 ];
    weekdayString = [weekdayString substringToIndex:3];
    //[self setvaluesofObject:self.rideStatus ModelName:ridedetail.CarModelName BookingLocation:ridedetail.sublocationName Date:[NSString stringWithFormat:@"%@ %ld %@",weekdayString,(long)pickday, monthdaySymbol] Day:pickupTimen Ride];
    
    
   
    //Drop off date calculation Logic
    //DropoffDate
    NSDateFormatter *formetD = [[NSDateFormatter alloc]init];
    [formetD setDateFormat:@"MM-dd-yyyy"];
    NSLocale *twelveHourLocaleD = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    formetD.locale = twelveHourLocaleD;
    NSDate *dropoffDate = [formetD dateFromString:ridedetail.DropoffDate];
    NSString *dropoffTime = ridedetail.DropoffTime;
    NSTimeInterval intervalD = 60 * 60 * [[dropoffTime substringToIndex:2] integerValue] + 60 * [[dropoffTime substringFromIndex:2] integerValue];
    formetD = [[NSDateFormatter alloc] init];
    formetD.locale = twelveHourLocale;
    [formetD setDateFormat:@"hh:mm a"];
    dropoffTime = [formetD stringFromDate:[dropoffDate dateByAddingTimeInterval:intervalD]];
    formetD.dateFormat = @"MM-dd-YYYY hh:mm a";
    
    //Get Week Day and month
    [formetD setDateFormat:@"MM-dd-yyyy"];
    
    NSCalendar* calenderD = [NSCalendar currentCalendar];
    NSDateComponents* componentD = [calenderD components:NSCalendarUnitWeekday fromDate:dropoffDate];
    NSInteger weekDayD = [componentD weekday];
    componentD = [calenderD components:NSCalendarUnitDay fromDate:dropoffDate];
    NSInteger dropOffday = [componentD day];
    componentD = [calenderD components:NSCalendarUnitMonth fromDate:dropoffDate];
    NSInteger monthdayD = [componentD month];
    NSDateFormatter *formatterD = [[NSDateFormatter alloc] init];
    formatterD.locale = twelveHourLocale;
    NSString *weekdayStringD = [[formatterD weekdaySymbols] objectAtIndex:weekDayD - 1];
    NSString *monthdaySymbolD = [[formatterD shortMonthSymbols] objectAtIndex:monthdayD -1 ];
    weekdayString = [weekdayString substringToIndex:3];
    
    
    [self setvaluesofObject:self.rideStatus ModelName:ridedetail.CarModelName BookingLocation:ridedetail.sublocationName Date:[NSString stringWithFormat:@"%@ %ld %@",weekdayString,(long)pickday, monthdaySymbolD] Day:pickupTime Date:[NSString stringWithFormat:@"%@ %ld %@",weekdayStringD,(long)dropOffday, monthdaySymbolD] Day:dropoffTime RideStatusMessageTwo:ridedetail.Tripstatus];

    
}


#pragma mark- Updated Setting Values
//- (void)setUpdatedvaluesofObject :(BOOL)rideStatus ModelName: (NSString *)ridemodelName  BookingLocation : (NSString *)rideLocationName Date : (NSString *)rideEndDate Day :(NSString *) rideEndTime Date : (NSString *)rideStartDate Day :(NSString *) rideStartTimeDay
//{
//    
//    if (rideStatus)
//    {
//        self.carThumnaliImg.image = [UIImage imageNamed:k_highlightCarIcon];
//    }
//    else
//    {
//        self.carThumnaliImg.image = [UIImage imageNamed:k_unhighlightCarIcon];
//    }
//    self.rideModelNameLb.text = [ridemodelName uppercaseString];
//    //self.rideModelNameLb.text = ridemodelName;
//    [self.rideModelNameLb sizeToFit];
//    
//    /*
//    NSAttributedString *attributeString2 = [[NSAttributedString alloc]initWithString:rideStatusMessage attributes:@{ NSForegroundColorAttributeName : [UIColor redColor] , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
//     */
//    
//    
//    //NSLog(@"rideStatusMessage = %@",rideStatusMessage2);
//    self.ridecancelLb.text = rideStatusMessage;
//    //self.ridecancelLb.text = ridedetail.Tripstatus;
//    //NSLog(@"rideStatusMessage = %@",rideStatusMessage);
//    
//    if ([rideStatusMessage isEqualToString:@"UPCOMING"])
//    {
//       // attributeStringRideStatus = [[NSAttributedString alloc]initWithString:rideStatusMessage attributes:@{ NSForegroundColorAttributeName : UICOLOR_UPCOMING , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
//        
//        attributeStringRideStatus = [[NSAttributedString alloc]initWithString:rideStatusMessage attributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
//        
//    }
//    
//    else if ([rideStatusMessage isEqualToString:@"COMPLETED"])
//    {
//        
//        attributeStringRideStatus = [[NSAttributedString alloc]initWithString:rideStatusMessage attributes:@{ NSForegroundColorAttributeName : UICOLOR_COMPLETED , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
//        
//    }
//    
//    
//    else if ([rideStatusMessage isEqualToString:@"CANCELLED"])
//    {
////         attributeStringtmp = [[NSAttributedString alloc]initWithString:rideStatusMessage attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed: 87.5 green:27.5 blue:15.3 alpha:1.0] , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
//        
//        
//        attributeStringRideStatus = [[NSAttributedString alloc]initWithString:rideStatusMessage attributes:@{ NSForegroundColorAttributeName : UICOLOR_CANCELLED , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
//    }
//    else if ([rideStatusMessage isEqualToString:@"IN PROGRESS"])
//    {
//        
//        attributeStringRideStatus = [[NSAttributedString alloc]initWithString:rideStatusMessage attributes:@{ NSForegroundColorAttributeName : UICOLOR_INPROGRESS , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
//    }
//    
//    
//    if (rideStatus)
//    {
//        self.ridelocationNameLb.text = rideLocationName;
//        
//    }
//    else
//    {
//        
//        
//        //NSMutableAttributedString *placeLabel = [[NSMutableAttributedString alloc]initWithString:rideLocationName  attributes:@{NSFontAttributeName : UIFONT_REGULAR_Motor(12)}];
//        
//        //attributeStringRegular = [[NSAttributedString alloc]initWithString:rideLocationName attributes:@{ NSForegroundColorAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
//        
//        attributeStringRegular = [[NSAttributedString alloc]initWithString:rideLocationName attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
//        
//        
//        //self.ridelocationNameLb.attributedText = placeLabel;
//        self.ridelocationNameLb.attributedText = attributeStringRegular;
//        self.ridecancelLb.attributedText = attributeStringRideStatus;
//        
//    }
//    
//    NSString *trimmedRideEndDate = [rideStartDate stringByTrimmingCharactersInSet:
//                                    [NSCharacterSet whitespaceCharacterSet]];
//    
//    [self.rideEndDateLbl setTextAlignment:NSTextAlignmentRight];
//    
//    
//    
//    //Ride start date
//    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideEndDate attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
//    self.ridedateLb.attributedText = attributeStringRegular;
//    
//    
//    //Ride start time
//    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideEndTime attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
//    self.rideDayLb.attributedText = attributeStringRegular;
//    
//    //Ride end date
//    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideStartDate attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
//    self.rideEndDateLbl.attributedText =attributeStringRegular;
//    
//    //Ride start time
//    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideStartTimeDay attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
//    
//    self.rideEndTimeLbl.attributedText = attributeStringRegular;
//    
//}

/*
 self.uploadImg2.image = UIImageName(k_defaultCarFullImg);
 self.uploadImg2.contentMode = UIViewContentModeScaleAspectFit;

 */



#pragma mark- Setting Values
- (void)setvaluesofObject :(BOOL)rideStatus ModelName: (NSString *)ridemodelName  BookingLocation : (NSString *)rideLocationName Date : (NSString *)rideEndDate Day :(NSString *) rideEndTime Date : (NSString *)rideStartDate Day :(NSString *) rideStartTimeDay RideStatusMessageTwo: (NSString *)RideStatusMessage2
{
    
    if (rideStatus)
    {
        self.carThumnaliImg.image = [UIImage imageNamed:k_highlightCarIcon];
    }
    else
    {
        self.carThumnaliImg.image = [UIImage imageNamed:k_unhighlightCarIcon];
    }
    self.rideModelNameLb.text = ridemodelName;
    [self.rideModelNameLb sizeToFit];
    
    if ([RideStatusMessage2 isEqualToString:k_tripstatusClose])
    {
        self.rideStatus = false;
        
        attributeStringRideStatus = [[NSAttributedString alloc]initWithString:@"Completed" attributes:@{ NSForegroundColorAttributeName : UICOLOR_COMPLETED , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
        
        self.ridecancelLb.attributedText = attributeStringRideStatus;
        self.carThumnaliImg.image = [UIImage imageNamed:k_unhighlightCarIcon];

    }
    else if ([RideStatusMessage2 isEqualToString:k_tripstatusCancel])
    {
        attributeStringRideStatus = [[NSAttributedString alloc]initWithString:@"Cancelled" attributes:@{ NSForegroundColorAttributeName : UICOLOR_CANCELLED , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
        self.rideStatus = false;
        self.ridecancelLb.attributedText = attributeStringRideStatus;
        self.carThumnaliImg.image = [UIImage imageNamed:k_unhighlightCarIcon];

    }
    
    else if ([RideStatusMessage2 isEqualToString:k_tripstatusOpen])
    {
        attributeStringRideStatus = [[NSAttributedString alloc]initWithString:@"In Progress" attributes:@{ NSForegroundColorAttributeName : UICOLOR_INPROGRESS , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
        self.rideStatus = false;
         self.ridecancelLb.attributedText = attributeStringRideStatus;
    }
    else if([RideStatusMessage2 isEqualToString:k_tripstatusNew])
    {
         attributeStringRideStatus = [[NSAttributedString alloc]initWithString:@"Upcoming" attributes:@{ NSForegroundColorAttributeName : UICOLOR_UPCOMING , NSFontAttributeName : UIFONT_BOLD_RobotoFONT(12)}];
        self.ridecancelLb.attributedText = attributeStringRideStatus;
    }
    
    if (rideStatus)
    {
         NSMutableAttributedString *placeLabel = [[NSMutableAttributedString alloc]initWithString:rideLocationName  attributes:@{NSFontAttributeName : UIFONT_REGULAR_Motor(12)}];
        self.ridelocationNameLb.attributedText = placeLabel;
        
    }
    else
    {
        
    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideLocationName attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
   
    self.ridelocationNameLb.attributedText = attributeStringRegular;
    self.ridecancelLb.attributedText = attributeStringRideStatus;
        
    }
    
   
    //Ride start date and end date
    
    NSString *trimmedRideEndDate = [rideEndDate stringByTrimmingCharactersInSet:
                                    [NSCharacterSet whitespaceCharacterSet]];
    
    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideEndDate attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
    self.ridedateLb.attributedText = attributeStringRegular;
    
    
    //Ride start time
    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideEndTime attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
    self.rideDayLb.attributedText = attributeStringRegular;
    
    //Ride end date
    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideStartDate attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
    self.rideEndDateLbl.attributedText =attributeStringRegular;
    
    //Ride start time
    attributeStringRegular = [[NSAttributedString alloc]initWithString:rideStartTimeDay attributes:@{ NSForegroundColorAttributeName : UICOLOR_GRAYCELLTEXT , NSFontAttributeName : UIFONT_REGULAR_RobotoFONT(12)}];
    
    self.rideEndTimeLbl.attributedText = attributeStringRegular;
    self.ridedateLb.text = rideEndDate;
    self.rideDayLb.text = rideEndTime;
    [self.rideEndDateLbl setTextAlignment:NSTextAlignmentRight];
}


@end
