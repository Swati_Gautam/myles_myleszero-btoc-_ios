
@interface RideHistoryCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *carThumnaliImg;
@property (weak, nonatomic) IBOutlet UILabel *rideModelNameLb;
@property (weak, nonatomic) IBOutlet UILabel *ridelocationNameLb;
@property (weak, nonatomic) IBOutlet UILabel *ridecancelLb;
@property (weak, nonatomic) IBOutlet UILabel *ridedateLb;
@property (weak, nonatomic) IBOutlet UILabel *rideDayLb;

@property (weak, nonatomic) IBOutlet UILabel *rideEndDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *rideEndTimeLbl;

- (void) getCellInfo : (NSDictionary *)dic;

@property (strong, nonatomic) NSString *bookingId ;
@property (assign) BOOL rideStatus;

@end
