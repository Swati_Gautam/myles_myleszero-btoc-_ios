//
//  VersionServiceResponse.h
//  Myles
//
//  Created by IT Team on 29/09/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <jsonmodel//JSONModel.h>
#import "AppVersionInfo.h"


@interface VersionServiceResponse : JSONModel

@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , AppVersionInfo> *response;

@end




