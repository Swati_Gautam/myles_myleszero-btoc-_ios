
//#import <jsonmodel//JSONModel.h>
#import "JSONModel.h"


@protocol CityListResponse <NSObject>


@end
@interface CityListResponse : JSONModel

@property (assign, nonatomic) NSInteger *CityId;
@property (strong, nonatomic) NSString *CityName;
//@property (assign, nonatomic) double latitude;
//@property (assign, nonatomic) double longitude;

@end
