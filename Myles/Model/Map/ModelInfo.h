//
//  ModelInfo.h
//  Myles
//
//  Created by Myles   on 30/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <jsonmodel//JSONModel.h>


@protocol ModelInfo <NSObject>

@end
@interface ModelInfo : JSONModel


@property (assign, nonatomic) NSString *CarModelID;
@property (assign, nonatomic) NSString *SeatingCapacity;
@property (strong, nonatomic) NSString *WeekdayRate;
@property (strong, nonatomic) NSString *WeekendRate;
@property (strong, nonatomic) NSString *modelName;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *urlFullimage;
@property (strong, nonatomic) NSString *Noofcars;

@end
