
#import "JSONModel.h"

#import "CityListResponse.h"

@interface CityList : JSONModel

@property (strong, nonatomic) NSMutableArray <ConvertOnDemand , CityListResponse> *response;
@property (strong , nonatomic) NSString *status;

@end
