//
//  LocationResponse.h
//  Myles
//
//  Created by Myles   on 31/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <jsonmodel//JSONModel.h>

#import "SublocationDetails.h"

@interface LocationResponse : JSONModel

@property (strong, nonatomic) NSArray<ConvertOnDemand, SublocationDetails >* response;

@end

