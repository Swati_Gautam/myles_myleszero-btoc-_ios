//
//  SublocationDetails.h
//  Myles
//
//  Created by Myles   on 30/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//

#import <Foundation/Foundation.h>
#import <jsonmodel//JSONModel.h>

#import "ModelInfo.h"

@protocol SublocationDetails //<NSObject>

@end
@interface SublocationDetails : JSONModel

@property (strong,nonatomic) NSString *Detail;
@property (strong, nonatomic) NSString *DetailID;
@property (assign, nonatomic) NSInteger SubLocationId;
@property (assign, nonatomic) NSInteger cityId;
@property (assign, nonatomic) double Lat;
@property (assign, nonatomic) double Lon;
@property (strong, nonatomic) NSString *SubLocationName;
@property (strong, nonatomic) NSString *SubLocationAddress;
@property (nonatomic, strong) NSMutableArray < ConvertOnDemand, ModelInfo > *modelImages;

@end
