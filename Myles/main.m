//
//
//  main.m
//  Myles
//
// Created by Myles on 29/07/15.
// Divya Information Technology Pvt. Ltd., Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited//
// Chetan Rajauria - Latest Code


#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]){
    @autoreleasepool {

        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

