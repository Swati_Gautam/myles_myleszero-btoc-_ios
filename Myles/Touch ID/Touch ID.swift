//
//  Touch ID.swift
//  Myles
//
//  Created by Itteam2 on 06/01/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import Foundation
import LocalAuthentication

class LoginWithTouch : UIViewController
{
    @IBOutlet weak var tfMobileNo: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var checkBox: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkBox.layer.borderColor = UIColor.red.cgColor
        checkBox.layer.borderWidth = 1
        
        // 2. Check if the device has a fingerprint sensor
        // If not, show the user an alert view and bail out!
        
        let authenticationContext = LAContext()
        var error:NSError?
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            
            showAlertViewIfNoBiometricSensorHasBeenDetected()
            return
            
        }
        
        authenticationContext.evaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            localizedReason: "Only awesome people are allowed",
            reply: { [unowned self] (success, error) -> Void in
                
                if( success ) {
                    
                    print(success)
                    // Fingerprint recognized
                    // Go to view controller
                    self.navigateToAuthenticatedViewController()
                    
                }else {
                    
                    // Check if there is an error
                    if let error :NSError = error as NSError?  {
                        print(error.code)

                        let message = self.errorMessageForLAErrorCode(error.code)
                        print(message)
                        self.showAlertViewAfterEvaluatingPolicyWithMessage(message)
                        
                    }
                    
                }
                
        })

       
       
    }
    
    @IBAction func loginButtonClicked(sender: UIButton) {
        
        // 1. Create a authentication context
        
        
    }
    func showAlertWithTitle( _ title:String, message:String ) {
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertVC.addAction(okAction)
        
        DispatchQueue.main.async { () -> Void in
            
            self.present(alertVC, animated: true, completion: nil)
            
        }
        
    }
    func showAlertViewIfNoBiometricSensorHasBeenDetected(){
        
        showAlertWithTitle("Error", message: "This device does not have a TouchID sensor.")
        
    }
    
    /**
     This method will present an UIAlertViewController to inform the user that there was a problem with the TouchID sensor.
     
     - parameter error: the error message
     
     */
    func showAlertViewAfterEvaluatingPolicyWithMessage( _ message:String ){
        
        showAlertWithTitle("Error", message: message)
        
    }
    /**
     This method will return an error message string for the provided error code.
     The method check the error code against all cases described in the `LAError` enum.
     If the error code can't be found, a default message is returned.
     
     - parameter errorCode: the error code
     - returns: the error message
     */
    func errorMessageForLAErrorCode( _ errorCode:Int ) -> String{
        
        var message = ""
        
        switch errorCode {
            
        case LAError.Code.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.Code.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.Code.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.Code.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.Code.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.Code.touchIDLockout.rawValue:
            message = "Too many failed attempts."
            
        case LAError.Code.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
            
        case LAError.Code.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.Code.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = "Did not find error code on LAError object"
            
        }
        
        return message
        
    }
    
    func navigateToAuthenticatedViewController(){
        
        
        print("match")
//        if let loggedInVC = storyboard?.instantiateViewController(withIdentifier: "LoggedInViewController") {
//            
//            DispatchQueue.main.async { () -> Void in
//                
//                self.navigationController?.pushViewController(loggedInVC, animated: true)
//                
//            }
//            
//        }
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    

}
