//
//  CommonClass.swift
//  Myles
//
//  Created by Akanksha Singh on 05/04/18.
//  Copyright © 2018 Myles. All rights reserved.
//

import Foundation
import UIKit

class CommonClass{

    static func TableViewCell(cell:UITableViewCell){

        cell.contentView.layer.cornerRadius = 4
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.backgroundColor = UIColor.white
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layoutMargins.left = 10
        cell.contentView.layoutMargins.right = 10
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.26
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:4).cgPath
        
    
        
    }
    
    static func CellLayer(cell:UICollectionViewCell){
        
        cell.contentView.layer.cornerRadius = 4
        //cell.contentView.layer.borderWidth = 1.0
        cell.contentView.backgroundColor = UIColor.white
        //cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.26
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:4).cgPath
        
        
    }
    

    static func attributedForCoupon(string :String) -> NSMutableAttributedString{
    
        
        let myMutableString = NSMutableAttributedString(
            string: string,
            attributes: [ NSAttributedString.Key.font: UIFont(name: "OpenSans-Semibold", size: 12.0)!,
                          NSAttributedString.Key.foregroundColor: UIColor(red: 87.0/255.0, green: 87.0/255.0, blue: 87.0/255.0, alpha: 1.0)   ]
            )
      
        return myMutableString
    
    }
    
    
    static func getLabelHeight(_ text: String, _ width:CGFloat, _ fontSize: CGFloat) -> CGFloat {
        let constraint = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraint, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)], context: nil).size
        return ceil(boundingBox.height)
    }

    
    
}
