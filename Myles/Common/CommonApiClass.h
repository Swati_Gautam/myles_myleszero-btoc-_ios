//
//  CommonApiCall.h
//  Myles
//
//  Created by Myles   on 04/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^ApiResponse)(NSData *data, NSURLResponse *response, NSError *error);

@protocol CommonApiClassDelegate <NSObject>
@optional
- (void)PresentPaymentScreen : (id )paymentVC;
- (void)PaymentSuccessDelagateWithData:(NSMutableDictionary *)dict ;
- (void)PaymentFailureDelagate;
- (void)PaymentCancelDelagte;

@end
@interface CommonApiClass : NSObject
enum resposeNumberExist
{
    NumberExist = 0,
    NumberNotExist = 1,
    NumberDeactiveByAdmin = 2
};

@property (weak, nonatomic)id <CommonApiClassDelegate> delegate;
+ (void)getServiceCenterPhoneNumber : (ApiResponse)handler;
+ (void)getRoadsideSupportCenterPhoneNumber :(ApiResponse)handler;

+ (void)cancleBooking : (NSString *)bookingId User : (NSString *)userid  CompletionHandler :(ApiResponse)handler;
+ (void)ChecklockUnlockApi : (NSString *)bookingId  CompletionHandler :(ApiResponse)handler;
+ (void)LockUnlockCar : (NSString *)gpsDeviceNo LOCKUNLOCK : (NSString *)State CompletionHandler :(ApiResponse)handler;
+ (void)aboutUsApi :(ApiResponse)handler;

//- (void)payNimoController : (NSString *)title TranactionAmount : (NSString *)totalamount Date : (NSString *)date withType:(BOOL)isPreAuthPayment andEndDate:(NSString *)endDate withGap:(NSInteger)daysGap andTrackId:(NSString *)trackId;

+ (void)userLogoutApi :(ApiResponse)handler;

+(void)shareApi :(ApiResponse)handler;
-(void)getLocationFromAddressString: (NSString*) addressStr :(void (^)(CLLocationCoordinate2D center))handler;
+(void)getUserData;
+(void)checkUserExistorNot:(NSString *)strMobileNo :(void(^)(NSMutableDictionary *dict,BOOL isTokenValid))completion;

@end
