 //
//  CommonApiCall.m
//  Myles
//
//  Created by Myles   on 04/10/15.
//  Copyright © 2015 Divya Rai. All rights reserved.
//

#import "CommonApiClass.h"
//#import <Paynimolib/PMGlobal.h>

@implementation CommonApiClass


#pragma mark- GetServiceCenterNumber
+ (void)getServiceCenterPhoneNumber : (ApiResponse)handler
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSURLSession *session = [CommonFunctions defaultSession];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *manager = [[APIOperation alloc]initWithBody:dic session:session url:KmylesCenterNo completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            NSString *statusStr = [headers objectForKey:@"Status"];
            if (statusStr != nil && [statusStr isEqualToString:@"1"])
            {
                handler(data,response,error);

            }
            else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                NSLog(@"AccessToken Invalid");
                handler(nil,response,error);
            }
            
        }];
        [globalOperation addOperation:manager];
        
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:KAInternet];
    }
}

+ (void)cancleBooking : (NSString *)bookingId User : (NSString *)userid  CompletionHandler :(ApiResponse)handler
{
    
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject: userid  forKey:@"userid"];
        [body setObject:bookingId forKey:@"bookingid"];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KcancelBooking completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         handler(data,response,error);
                                                     });
                                                     
                                                 }
                                                 else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     handler(nil,response,error);
                                                     
                                                 }

                                                 
                                                 
                                             }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}


#pragma mark- Check Lock Unlock

+ (void)ChecklockUnlockApi : (NSString *)bookingId  CompletionHandler :(ApiResponse)handler
{
    
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:bookingId forKey:@"bookingid"];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KcheckLockUnlock completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         handler(data,response,error);
                                                     });
                                                     
                                                 }
                                                 else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     handler(nil,response,error);

                                                 }
                                            }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}
+ (void)LockUnlockCar : (NSString *)gpsDeviceNo LOCKUNLOCK : (NSString *)State CompletionHandler :(ApiResponse)handler
{
    if ([CommonFunctions reachabiltyCheck])
    {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:gpsDeviceNo forKey:k_GPSDeviceNo];
        [body setObject:State forKey:k_IsLock];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KlockUnlock completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         handler(data,response,error);
                                                     });
                                                     
                                                 }
                                                 else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     handler(nil,response,error);
                                                     
                                                 }
                                             }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
}



//- (void)payNimoController : (NSString *)title TranactionAmount : (NSString *)totalamount Date : (NSString *)date withType:(BOOL)isPreAuthPayment andEndDate:(NSString *)endDate withGap:(NSInteger)daysGap andTrackId:(NSString *)trackId
//{
//    
//    
//     //Live version 1.01
//    /*
//    if (daysGap > 7)
//    {
//        kMerchantId = @"L41534";
//        kSchemeId   = @"CARZONMOB"; //for Livepayment"
//    }
//    else
//    {
//        kMerchantId       = (isPreAuthPayment)?@"L41534":@"L41531";//T6380
//        kSchemeId         = (isPreAuthPayment)?@"CARZONMOB":@"CARZONMOB";
//    }
//     */
//    
//    
//    
//    //Test version 1.01
//    if (daysGap > 7)
//    {
//        kSchemeId   = @"Test"; //for test it is @"Test"
//    }
//    else
//    {
//        kMerchantId       = (isPreAuthPayment)?@"T6380":@"T5163";//T6380
//        kSchemeId         = (isPreAuthPayment)?@"CARZONMOB":@"CARZONMOB"; 
//    }
// 
//    NSString *merchantTransactionIdentifier = trackId;
//    NSLog(@"%@,%@,%@",[DEFAULTS objectForKey:KUSER_ID],[DEFAULTS objectForKey:KUSER_PHONENUMBER],[DEFAULTS objectForKey:KUSER_EMAIL]);
//    
//    Checkout *checkout = [[Checkout alloc] init];
//    
//        //merchant
//    [checkout setMerchantIdentifier:kMerchantId];
//     //[checkout setMerchantReference:@"ORD001"];
//    [checkout setPaymentTransactionIdentifier:merchantTransactionIdentifier];
//    //NSLog(@"merchantTransactionIdentifier =%@",merchantTransactionIdentifier);
//    
//    //For Live heardcoded payment
//    //[checkout setPaymentTransactionAmount:totalamount];
//    //[checkout setPaymentTransactionAmount:@"1.0"];
//    
//    //for test heardcoded payment
//    [checkout setPaymentTransactionAmount:@"1.01"];
//
//    
//
//    [checkout setMerchantDescription:@""];
//    [checkout setMerchantResponseType:@""];
//    [checkout setMerchantResponseEndpointURL:@""];
//    [checkout setMerchantWebhookType:@""];
//    [checkout setMerchantWebhookEndpointURL:@""];
//    [checkout setPaymentTransactionDateTime:date]; //current Date
//    [checkout setPaymentTransactionCurrency:@"INR"];
//    [checkout setPaymentTransactionReference:trackId];
//         //Consumer
//    [checkout setConsumerIdentifier:[DEFAULTS objectForKey:KUSER_ID]]; //Consumer ID
//    [checkout setConsumerEmailID:[DEFAULTS objectForKey:KUSER_EMAIL]]; //Email ID
//    [checkout setConsumerMobileNumber:[DEFAULTS objectForKey:KUSER_PHONENUMBER]]; //Mobile No
//    [checkout setCartIdentifier:@""];
//    [checkout setCartReference:@""];
//    [checkout setCartDescription:@""];
//    
//    
//    
//    //For live heardcoded payment
//    //[checkout addCartItem:kSchemeId amount:totalamount SurchargeOrDiscount:@"0.0" SKU:@"" Description:@"MYLES BOOKING" ProviderID:@"snapdeal.com" Reference:@"CARZONRENT LTD."];
//     
//   // [checkout addCartItem:kSchemeId amount:@"1.0" SurchargeOrDiscount:@"0.0" SKU:@"" Description:@"MYLES BOOKING" ProviderID:@"snapdeal.com" Reference:@"CARZONRENT LTD."];
//    
//    
//    //For test heardcoded payment
//    
//    [checkout addCartItem:kSchemeId amount:@"1.01" SurchargeOrDiscount:@"0.0" SKU:@"" Description:@"MYLES BOOKING" ProviderID:@"snapdeal.com" Reference:@"Test"];
//         
//    [checkout setPaymentTransactionType:(isPreAuthPayment)?@"PREAUTH":@"SALE"]; //SALE PREAUTH
//    [checkout setPaymentTransactionSubType:(isPreAuthPayment)?@"RESERVE":@"DEBIT"]; //DEBIT /RESERVE
//    
//         //create the payment view controller
//    PMPaymentOptionView *payViewNav = [[PMPaymentOptionView alloc] initWithPublicKey:@"abcd" checkout:checkout paymentType:title success:^(id resObject)
//    {
//            
//            [checkout setResponseDictionary:resObject]; //first set dictonary data
//            
//            NSString *strStatusCode=@"";
//            NSString *strSIStatusCode=@"";
//            
//            
//            if([[checkout getStatusCode] isEqualToString:@"0300"])
//                {
//                    //THIS IS SUCESS BLOCK .
//                strStatusCode = @"TRANSACTION STATUS : -- SUCCESS";
//                }
//            else if([[checkout getStatusCode] isEqualToString:@"0399"]){
//                
//                    //THIS IS FAILURE BLOCK.
//                strStatusCode = @"TRANSACTION STATUS : -- FAILURE";
//                
//                [self.delegate  PaymentFailureDelagate];
//                return ;
//            }
//        
//            if([[checkout getPAymentInstructionErrorStatusCode] isEqualToString:@"0300"])
//                {
//                strSIStatusCode = @"SI STATUS : -- SUCCESS";
//                }
//            else if([[checkout getPAymentInstructionErrorStatusCode] isEqualToString:@"0399"]){
//                
//                    //THIS IS FAILURE BLOCK.
//                strSIStatusCode = @"SI STATUS : -- FAILURE";
//                
//               
//            }
//            
//            //NSLog(@"Transaction Status:- %@ ",strStatusCode);
//            //NSLog(@"SI Status:- %@ ",strSIStatusCode);
//            
//        //NSLog(@"IDENTIFIER ----: %@",[checkout getIdentifier]);
//             NSString *responseStr =   [NSString stringWithFormat:@"Amount: %@ \nDateTime: %@ \nStatusMessage: %@ \nErrorMessage: %@ \nStatusCode: %@ \nIdentifier: %@ \nRefundIdentifier: %@ \nInstrumentToken: %@ \nMerchantCode: %@ \nMerchantTransactionIdentifier: %@ \nMerchantTransactionRequestType: %@", [checkout getAmount], [checkout getDateTime], [checkout getStatusMessage], [checkout getErrorMessage],[checkout getStatusCode],[checkout getIdentifier],[checkout getRefundIdentifier], [checkout getInstrumentToken], [checkout getMerchantCode], [checkout getMerchantTransactionIdentifier], [checkout getMerchantTransactionRequestType]];
//             
//             //NSLog(@"RESPONESE  : %@",responseStr);
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 
//                 NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjects:@[trackId,[checkout getIdentifier]] forKeys:@[@"trackid",@"transactionid"]];
//                 
//                 [self.delegate  PaymentSuccessDelagateWithData:dict];
//                 
//             });
//            
//            
//        } failure:^(NSDictionary *error) {
//            NSLog(@"Payment fail reson = %@", error.description);
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self.delegate  PaymentFailureDelagate];
//            });
//            NSLog(@"error %@",error);
//        } cancel:^(){
//
//            NSLog(@"Back button pressed!");
//
//            [self.delegate  PaymentCancelDelagte];
//
//        }];
//         
////         [self presentViewController:payViewNav animated:YES completion:nil];
//    
//    
//    [self.delegate PresentPaymentScreen:payViewNav];
//}


+(void)userLogoutApi :(ApiResponse)handler
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
        [body setObject: userid forKey:@"userid"];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KdeviceUpdateOnLogout completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         handler(data,response,error);
                                                     });
                                                     
                                                 }
                                                 else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     handler(nil,response,error);
                                                     
                                                 }

                                             }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }

}


+ (void)getRoadsideSupportCenterPhoneNumber :(ApiResponse)handler
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kRoadsideSupport completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                       handler(data,response,error);
                                                   });
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }
    
}


+ (void)aboutUsApi :(ApiResponse)handler
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
       
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kRoadsideSupport completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         handler(data,response,error);
                                                     });
                                                     
                                                 }
                                                 else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     handler(nil,response,error);
                                                     
                                                 }

                                             
                                             }];
        [globalOperation addOperation:registrationOperation];
    }
    else
        {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
        }
    
}

+ (void)shareApi :(ApiResponse)handler
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:kShareApi completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                             {
                                                 NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                 NSString *statusStr = [headers objectForKey:@"Status"];
                                                 if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         handler(data,response,error);
                                                     });
                                                     
                                                 }
                                                 else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"])) {
                                                     NSLog(@"AccessToken Invalid");
                                                     handler(nil,response,error);
                                                     
                                                 }

                                        }];
        [globalOperation addOperation:registrationOperation];
    }
    else
        {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
        }
    
}

/**
 Get lat long through place name.
 - parameter Pass place name(string).
 */
-(void)getLocationFromAddressString: (NSString*) addressStr :(void (^)(CLLocationCoordinate2D center))handler

{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        
        double latitude = 0, longitude = 0;
        
        NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //        http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@
        
        //        https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCnfc6nKRQ4Wo8et8ip_0yCVhtPESa0BWs&new_forward_geocoder=false&address=tidel%20park%20chennai
        
        NSString *req = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCnfc6nKRQ4Wo8et8ip_0yCVhtPESa0BWs&new_forward_geocoder=false&address=%@", esc_addr];
        
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
        
        if (result) {
            
            NSScanner *scanner = [NSScanner scannerWithString:result];
            
            if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
                
                [scanner scanDouble:&latitude];
                
                if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                    
                    [scanner scanDouble:&longitude];
                    
                }
                
            }
            
        }
        
        CLLocationCoordinate2D center;
        
        center.latitude=latitude;
        
        center.longitude = longitude;
        
        NSLog(@"View Controller get Location Logitute : %f",center.latitude);
        
        NSLog(@"View Controller get Location Latitute : %f",center.longitude);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            handler(center);
            
        });
        
    });
}



+(void)getUserData
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        NSString *userid = [DEFAULTS valueForKey: KUSER_ID];
        [body setObject: userid forKey:@"userId"];
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:KUSERINFO_FATCH completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                               {
                                                   if (data != nil) {
                                                NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                                   NSString *statusStr = [headers objectForKey:@"Status"];
                                                   if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                                   {
                                                       if (!error)
                                                       {
                                                           if (response!= nil)
                                                           {
                                                               NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                               
                                                            
                                                               NSLog(@"RES : %@",responseJson);
                                                               
                                                               /*if (responseJson[@"response"][@"LName"]  == (id)[NSNull null] || [responseJson[@"response"][@"LName"] length] == 0 )
                                                               {
                                                                   [DEFAULTS setValue:@"No Name" forKey:KUSER_LNAME];
                                                               }
                                                               else
                                                               {
                                                                   [DEFAULTS setValue:responseJson[@"response"][@"LName"] forKey:KUSER_LNAME];
                                                               }
                                                               
                                                               if (responseJson[@"response"][@"FName"]  == (id)[NSNull null] || [responseJson[@"response"][@"FName"] length] == 0 )
                                                               {
                                                                   [DEFAULTS setValue:@"No Name" forKey:KUSER_FNAME];
                                                               }
                                                               else
                                                               {
                                                                   [DEFAULTS setValue:responseJson[@"response"][@"FName"] forKey:KUSER_FNAME];
                                                               }
                                                               
                                                               if (responseJson[@"response"][@"emailId"]  == (id)[NSNull null] || [responseJson[@"response"][@"emailId"] length] == 0 )
                                                               {
                                                                   [DEFAULTS setValue:@"No Email Id" forKey:KUSER_EMAIL];
                                                               }
                                                               else
                                                               {
                                                                   [DEFAULTS setValue:responseJson[@"response"][@"emailId"] forKey:KUSER_EMAIL];
                                                               }*/
                                                               //Swati Changes All IF To Check NULL
                                                                    
                                                               if ([responseJson[@"response"][@"LName"] length] > 0) {
                                                                   [DEFAULTS setValue:responseJson[@"response"][@"LName"] forKey:KUSER_LNAME];
                                                                   
                                                               }
                                                               if ([responseJson[@"response"][@"FName"] length] > 0) {
                                                                   [DEFAULTS setValue:responseJson[@"response"][@"FName"] forKey:KUSER_FNAME];
                                                                   
                                                               }
                                                               if ([responseJson[@"response"][@"emailId"] length] > 0) {
                                                                   [DEFAULTS setValue:responseJson[@"response"][@"emailId"] forKey:KUSER_EMAIL];
                                                                   
                                                               }

                                                           }
                                                           
//                                                           NSLog(@"adkghsd %@ -%@ -%@",responseJson[@"response"][@"LName"],responseJson[@"response"][@"FName"],responseJson[@"response"][@"emailId"]);
                                                           
                                                       }
                                                       
                                                     else if ((statusStr != nil) && ([statusStr isEqualToString:@"0"]))
                                                    {
                                                       
                                                       
                                                     }
                                                   
                                               }
                                                   }
                                               }];
        [globalOperation addOperation:registrationOperation];
    }
    else
    {
        [CommonFunctions alertTitle:nil withMessage:KAInternet];
    }

}

/**
 @brief Check mobile number exist or not
 @dev Abhishek singh
 */


+(void)checkUserExistorNot:(NSString *)strMobileNo :(void(^)(NSMutableDictionary *dict,BOOL isTokenValid))completion
{
    //   __block enum resposeNumberExist checkNumberResponse;
    
    if ([CommonFunctions reachabiltyCheck]) {
        
        NSURLSession *session = [CommonFunctions defaultSession];
        NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
        APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:[@{@"mobileNo":strMobileNo} mutableCopy] session:session url:@"RestM1sendOTP" completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            NSString *statusStr = [headers objectForKey:@"Status"];
            if (statusStr != nil && [statusStr isEqualToString:@"1"])
            {

            
            if (data != nil) {
                NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                
                NSLog(@"jsonreponse %@",jsonreponse);
                completion(jsonreponse,true);

            }
            else
                completion(nil,true);
            }else
                completion(nil,false);
            
        }];
        [globalOperation addOperation:registrationOperation];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil,true);
            UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:@"Myles"
                                                             message:KAInternet
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            [alert show];

        });
    }
}
@end
