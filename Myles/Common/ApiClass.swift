//
//  ApiClass.swift
//  Myles
//
//  Created by Itteam2 on 30/12/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import Foundation
enum ErrorMessages : String {
    case NoResponse = "No Response from server"
    case SessionExpired = "Session Expired"
    case ErrorMessageFromServer = "Data not Found"
    case NoInternetConnection = "No Internet Connection"
    case SuccessResponse = "Success"
}

enum SuccessResponse : Int {
    case Failure = 0
    case Success = 1
}

@objc class ApiClass: NSObject {
    
//    let userD = UserDefaults.standard
    
    @objc var dictPost = Dictionary<String, String>()
    
    @objc init(dict:Dictionary<String, String>) {
        dictPost = dict
    }
    
    override init() {
        
    }
    
    let headers = ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]
    
    //let headers = ["accessToken": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]
    
    /*
      Call CancellationPolicy Api for cancel booking.
      param pass api name.
      param pass dict with required param.
     */
    @objc public func CancellationPolicyAPI(dict:Dictionary<String,Any>,strAPIName:String,completionHandler: @escaping (Dictionary<String,Any>? ,Bool) ->Void)
    {
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+strAPIName)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            print(dict)
            if JSONSerialization.isValidJSONObject(dict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakSelf = self] data, response, error in
                   
                    guard response != nil else{
                        completionHandler(nil,false)
                        return
                    }
                    
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    print("header: \(headers)")
                    print(NSURL(string: SiteAPIURL+strAPIName))
                    let statusStr = (headers["Status"] as? String)
                    if(statusStr == nil){
                        completionHandler(nil,false)
                        return
                    }
                    guard !statusStr!.isEmpty && (statusStr! == "1")else{
                        completionHandler(nil,false)
                        return
                    }
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        DispatchQueue.main.async{
                            weakSelf.AlertView(message: "Server not responding")
                            completionHandler(nil,true)
                            
                        }
                        
                        return
                    }
                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        DispatchQueue.main.async{
                            weakSelf.AlertView(message: "Server not responding")
                            completionHandler(nil,true)
                            
                        }
                        return
                    }
                    
                    
                    
                    print("data %@",data ?? "json")
                    
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    print(json ?? "json")
                    
                    let iStatus = json?["status"] as! NSInteger
                    if iStatus == 0
                    {
                        DispatchQueue.main.async{
                            if json?["message"] as! String == "Your OTP has expired. Please request New OTP"
                            {
                                weakSelf.AlertView(message: json?["message"] as! String)
                                
                            }
                        }
                        completionHandler(json,true)
                        
                    }else
                    {
                        completionHandler(json,true)
                        
                    }
                    
                    
                    
                }
                task.resume()
                
            }
        }
        else
        {
            completionHandler(nil,true)
            //CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
            AlertView(message: KAInternet)
            
        }
    }
    
  /**
     Search car behalf for your credentials
     Class -Homeviewcontroller
 **/
    
   @objc public func SearchCarApi (completionHandler: @escaping (MylesCarDetailsResponse?,Bool) ->Void)
   {
    if CommonFunctions.reachabiltyCheck()
    {
        print("URL: ", SiteAPIURL+kMylesCarDetailsV1)
        let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+kMylesCarDetailsV1)! as URL)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
        
        if JSONSerialization.isValidJSONObject(dictPost)
        {
            print("dictPost: ", dictPost)
            request.httpBody = try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted)
            let session = URLSession.shared
            session.configuration.timeoutIntervalForRequest = 15
            print("try! request: \(try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted))")
            
            let task = session.dataTask(with: request as URLRequest) { data, response, error in
               
                guard response != nil else{
                    completionHandler(nil,false)
                    return
                }
                var headers = (response as! HTTPURLResponse).allHeaderFields
                print(headers);
                
                // To change the API Request - Sourabh
                let statusStr = (headers["Status"] as! String)
                guard !statusStr.isEmpty && (statusStr == "1")else{
                    self.AlertView(message: "Server not responding")
                    completionHandler(nil,false)
                    return
                }
                
                guard error?.localizedDescription == nil else{
                    print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                    self.AlertView(message: "Server not responding")
                    completionHandler(nil,true)
                    return
                }
                
                
                guard data != nil else {
                    print("no data found: \(String(describing: error))")
                    self.AlertView(message: "Server not responding")
                    completionHandler(nil,true)
                    return
                }
                
                guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                    self.AlertView(message: "Server not responding")
                    completionHandler(nil,true)
                    return
                }
//                let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                print("jsonresponse = \(json) ")
                if (!(json["status"]! as! Bool)) {
                    print(json["status"]!)
                    DispatchQueue.main.async {
                        let apiClass = ApiClass()
                        apiClass.AlertView(message: (json["message"] as? String)!)
                    }
                    completionHandler(nil,true)
                    return
                }
                
                //print("json = \(json)")
                if let sublocation = json["response"]!["CityZones"] as? [[String : AnyObject]]{
                    print("sublocation: ", sublocation)

                    //[[SubLocationData sharedInstance]setSublocation:sublocation];
                    SubLocationData.sharedInstance.setSublocation(sublocation )
                }
                
                if let dorstepData = json["response"]!["DoorStepDelivery"] as? [String : AnyObject]{
                    UserDefaults.standard.set(1, forKey: k_HomePickupAvailable)
                    if let amount = dorstepData["Amount"] as? String{
                        
                        UserDefaults.standard.set(amount, forKey: k_HomePickupCharge)

                    }
                }
                else{
                    UserDefaults.standard.set(0, forKey: k_HomePickupAvailable)

                }

                let mylesCarDetailsResponse = try! MylesCarDetailsResponse(data: data)
                print("mylesCarDetailsResponse: ", mylesCarDetailsResponse)
                
                //let carDetailInfo = try! CarDetailInformation(data: data)
                //print("carDetailInfo: ", carDetailInfo)
                //print(mylesCarDetailsResponse)
                completionHandler(mylesCarDetailsResponse,true)
            }
            task.resume()
        }
    }else
    {
        self.AlertView(message: KAInternet)
        completionHandler(nil,true)
        return
//        CommonFunctions.alertTitle("Myles", withMessage:KAInternet)

    }
  }
    
    /**
     Search car behalf for your credentials
     Class -Homeviewcontroller
     */
    @objc public func doorStepDeleievery(completionHandler: @escaping (Any?,Bool,Any?) ->Void) {
        
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+kMylesCarDetailsWithHomePickUpAndDropOff)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            if JSONSerialization.isValidJSONObject(dictPost)
            {
                print(headers)
                print(dictPost)
                print(SiteAPIURL+kMylesCarDetailsWithHomePickUpAndDropOff)
                request.httpBody = try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dictPost, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                let task = session.dataTask(with: request as URLRequest) { data, response, error in
                    
                    guard response != nil else{
                        completionHandler(nil,false,nil)
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completionHandler(nil,false,nil)
                        return
                    }
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                        completionHandler(nil,true,nil)
                        CommonFunctions.alertTitle("Myles", withMessage: "Server not responding")
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                        completionHandler(nil,true,nil)
                        return
                    }
                    
                    //if JSONSerialization.isValidJSONObject(data!) {
                        print("Valid Json")
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("datastring = \(String(describing: datastring))")
                    
                    guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                        completionHandler(nil,true,nil)
                        return
                    }
//                        let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                        //print("jsonresponse = \(json) ")
                    
                        //print("jsonresponse = \(json) ")
                    if let sublocation = json["response"]!["CityZones"] as? [[String : AnyObject]]{
                        print(sublocation)
                        
                        //[[SubLocationData sharedInstance]setSublocation:sublocation];
                        SubLocationData.sharedInstance.setSublocation(sublocation )
                    }
                    
                    if let dorstepData = json["response"]!["DoorStepDelivery"] as? [String : AnyObject]{
                        UserDefaults.standard.set(1, forKey: k_HomePickupAvailable)
                        if let amount = dorstepData["Amount"] as? String{
                            
                            UserDefaults.standard.set(amount, forKey: k_HomePickupCharge)
                            
                        }
                    }
                    else{

                        UserDefaults.standard.set(0, forKey: k_HomePickupAvailable)
                        
                    }
                    let mylesCarDetailsResponse = try! MylesCarDetailsResponse(data: data)

                    if (!(json["status"]! as! Bool)) {
                        print(json["status"]!)
                        completionHandler(json,true , mylesCarDetailsResponse)
                        return
                    }
                        completionHandler(mylesCarDetailsResponse,true,mylesCarDetailsResponse)

//                    } else {
//                        print("InValid Json")
//                        DispatchQueue.main.async {
//                            let apiClass = ApiClass()
//                            apiClass.AlertView(message: "Response Not in json format")
//                        }
//                        completionHandler(nil,true)
//                        return
//
//                        
//                    }
                }
                task.resume()
                
            }
        }else
        {
            completionHandler(nil,true,nil)
            CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
            
        }
    }


/**
     Call for city list
     Class - Homeviewcontroller
 */

 @objc func cityListApi(completionHandler: @escaping (Array<NSDictionary>?,Bool,String) ->Void) {
    
    if CommonFunctions.reachabiltyCheck()
    {
        print(headers)
        let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+KCITY_LIST)! as URL)
        print("URL: ", request)
        request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 15
        
        let task = session.dataTask(with: request as URLRequest) { data, response, error in
            guard response != nil else {
                completionHandler(nil,false,ErrorMessages.NoResponse.rawValue)
                return
            }
            var headers = (response as! HTTPURLResponse).allHeaderFields
            print("datatoprint = \(headers)")
            print("City Response: ", response)
            //TOCHECKAFTERNEW
            let statusStr = (headers["Status"] as! String)
            guard !statusStr.isEmpty && (statusStr == "1")else{
                completionHandler(nil,false,ErrorMessages.SuccessResponse.rawValue)
                return
            }
            guard error?.localizedDescription == nil else{
                print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                completionHandler(nil,false,error.debugDescription)
                //CommonFunctions.alertTitle("Myles", withMessage: "Server not responding")
                return
            }            
            print(data ?? "NO Data")
            guard data != nil && (data?.count)! > 0 else {
                print("no data found: \(String(describing: error))")
                completionHandler(nil,false,ErrorMessages.ErrorMessageFromServer.rawValue)
                return
            }
            
            guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                completionHandler(nil,false,ErrorMessages.ErrorMessageFromServer.rawValue)
                return
            }
            //let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
            //print("jsonresponse = \(String(describing: json["response"]))")
            completionHandler(json["response"] as! Array<NSDictionary>?,true,ErrorMessages.SuccessResponse.rawValue)
            
            
        }
        task.resume()
    }else
    {
        completionHandler(nil,false,ErrorMessages.NoInternetConnection.rawValue)
        //CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
        
    }
    
    
    
}
    /**
     otp Verivy
     */
    
   @objc public func verifyOtp(dict:Dictionary<String,String>,completionHandler: @escaping (Bool,Bool,Bool) ->Void)
    {
//        RestM1verifyMobileNo
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+kRestM1verifyMobileNo)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            print(dict)
            if JSONSerialization.isValidJSONObject(dict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakSelf = self] data, response, error in
                    
                    guard response != nil else{
                        completionHandler(false,false,true)
                        return
                    }
                    
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completionHandler(false,false,true)
                        return
                    }
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        DispatchQueue.main.async{
                        weakSelf.AlertView(message: "Server not responding")
                            completionHandler(false,true,true)

                        }

                        return
                    }
                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        DispatchQueue.main.async{
                            weakSelf.AlertView(message: "Server not responding")
                            completionHandler(false,true,true)
                            
                        }
                        return
                    }
                    

                   
                    print("data %@",data ?? "json")

                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    print(json ?? "json")
                    
                    let iStatus = json?["status"] as! NSInteger
                    if iStatus == 0
                    {
                        DispatchQueue.main.async{
                    if json?["message"] as! String == "Your OTP has expired. Please request New OTP"
                    {
                        weakSelf.AlertView(message: json?["message"] as! String)

                    }
                   }
                        completionHandler(false,true,true)

                    }else
                    {
                        completionHandler(true,true,true)

                    }
                    
                    
                    
                }
                task.resume()
                
            }
        }
        else
        {
            completionHandler(false,true,false)
            //CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
            AlertView(message: KAInternet)

        }
    }
    
    /**
     Api for login
     */
    @objc public func loginApi(loginDict: Dictionary<String,String>,completionHandler: @escaping (Dictionary<String,Any>?) ->Void)
    {
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+KUSER_LOGIN)! as URL)
            print("Login Url: ", request.url)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            /*let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
            let strMylesZero = DefaultsValues.getStringValueFromUserDefaults_(forKey: kIsMylesZeroClicked)
            
            if strMylesZero == "true"
            {
                request.allHTTPHeaderFields = (dictHeaders) as? [String : String]
            }
            else
            {
                request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            }*/
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            
            if JSONSerialization.isValidJSONObject(loginDict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: loginDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: loginDict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                let task = session.dataTask(with: request as URLRequest) {[weakself = self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        weakself.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    
                    guard response != nil else{
                        completionHandler(nil)
                        return
                    }

                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                                        let statusStr = (headers["Status"] as! String)
                    
                                            print(statusStr)
                                        guard !statusStr.isEmpty && (statusStr == "1")else{
                                            completionHandler(nil)
                                            return
                                        }
                    
                    
//                    print("data %@",data ?? "json")
                    
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    
                    print("json %@",json ?? "json nil")                    
                    
                    print("Login Response:", loginDict)
                    
                    /*let newDictHeaders = ["hashVal": UserDefaults.standard.object(forKey: K_AccessToken) != nil ? UserDefaults.standard.object(forKey: K_AccessToken)! : "", "sessionID": UserDefaults.standard.object(forKey: K_SessionID) != nil ? UserDefaults.standard.object(forKey: K_SessionID)! : "", "customerID": UserDefaults.standard.bool(forKey: IS_LOGIN) ? UserDefaults.standard.object(forKey: KUSER_ID) ?? "-99" : "-99"]
                     //let dictHeaders = DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders)
                     DefaultsValues.setCustomObjToUserDefaults(newDictHeaders, forKey: kHttpHeaders)
                     print("Login Headers: ", newDictHeaders)
                     print("ABCD: ", DefaultsValues.getDictionaryValueFromUserDefaults_(forKey: kHttpHeaders))*/
                    
                    if json != nil
                    {
                        completionHandler(json)

                    }
                    
                }
                task.resume()

            }
        }else{
            completionHandler(nil)
            AlertView(message: KAInternet)
        }
        
    }
    
    @objc public func fetchProfile(loginDict: Dictionary<String,String>,completionHandler: @escaping (UserInfoResponse? , String) ->Void)
    {
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+KUSERINFO_FATCH)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            print(headers)
            print(loginDict)
            if JSONSerialization.isValidJSONObject(loginDict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: loginDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                CommonFunctions.defaultSession()
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: loginDict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakself = self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        weakself.AlertView(message: "Server not responding")
                        completionHandler(nil , "")
                        return
                    }
                    
                    guard response != nil else{
                        completionHandler(nil,"")
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    let statusStr = (headers["Status"] as! String)
                    
                    print(statusStr)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completionHandler(nil ,statusStr )
                        return
                    }

                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    
                    print("json %@",json ?? "json nil")
                    
                    if let jsn = json {
                        if (!(jsn["status"] as! Bool)) {
                            DispatchQueue.main.async {
                                let apiClass = ApiClass()
                                apiClass.AlertView(message: (jsn["message"] as? String)!)
                            }
                            completionHandler(nil , statusStr)
                            return
                        }
                        else{
                            let userinfo = try! UserInfo(data: data)
                            completionHandler(userinfo.response , statusStr)
                        }
                        
                    }
                    else{
                        completionHandler(nil , statusStr)
                        return
                    }
                    
                }
                task.resume()
                
            }
        }else{
            completionHandler(nil,"")
            AlertView(message: KAInternet)
        }
        
    }
    @objc public func updateProfile(updateDict: Dictionary<String,String>,completionHandler: @escaping ( String) ->Void)
    {
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+KUSERINFO_UPDATE)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            print(headers)
            print(updateDict)
            if JSONSerialization.isValidJSONObject(updateDict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: updateDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                CommonFunctions.defaultSession()
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: updateDict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakself = self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        weakself.AlertView(message: "Server not responding")
                        completionHandler("")
                        return
                    }
                    
                    guard response != nil else{
                        completionHandler("")
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    let statusStr = (headers["Status"] as! String)
                    
                    print(statusStr)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completionHandler(statusStr )
                        return
                    }
                    
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    
                    print("json %@",json ?? "json nil")
                    
                    if let jsn = json {
                        if (!(jsn["status"] as! Bool)) {
                            DispatchQueue.main.async {
                                let apiClass = ApiClass()
                                apiClass.AlertView(message: (jsn["message"] as? String)!)
                            }
                            completionHandler(statusStr)
                            return
                        }
                        else{
                            completionHandler(statusStr)
                        }
                        
                    }
                    else{
                        completionHandler(statusStr)
                        return
                    }
                    
                }
                task.resume()
                
            }
        }else{
            completionHandler("")
            AlertView(message: KAInternet)
        }
        
    }
    @objc public func deleteFile(deleteDict: Dictionary<String,String>,completionHandler: @escaping ( String) ->Void)
    {
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+KdeleteFile)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            print(headers)
            print(deleteDict)
            if JSONSerialization.isValidJSONObject(deleteDict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: deleteDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                CommonFunctions.defaultSession()
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: deleteDict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakself = self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        weakself.AlertView(message: "Server not responding")
                        completionHandler("")
                        return
                    }
                    
                    guard response != nil else{
                        completionHandler("")
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    let statusStr = (headers["Status"] as! String)
                    
                    print(statusStr)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completionHandler(statusStr )
                        return
                    }
                    
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    
                    print("json %@",json ?? "json nil")
                    
                    if let jsn = json {
                        if (!(jsn["status"] as! Bool)) {
                            DispatchQueue.main.async {
                                let apiClass = ApiClass()
                                apiClass.AlertView(message: (jsn["message"] as? String)!)
                            }
                            completionHandler(statusStr)
                            return
                        }
                        else{
                            completionHandler(statusStr)
                        }
                        
                    }
                    else{
                        completionHandler(statusStr)
                        return
                    }
                    
                }
                task.resume()
                
            }
        }else{
            completionHandler("")
            AlertView(message: KAInternet)
        }
        
    }
    @objc public func changePassword(passwordDict: Dictionary<String,String>,completionHandler: @escaping ( String) ->Void)
    {
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+KCHANGE_PASSWORD)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            print(headers)
            print(passwordDict)
            if JSONSerialization.isValidJSONObject(passwordDict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: passwordDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                CommonFunctions.defaultSession()
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: passwordDict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakself = self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        weakself.AlertView(message: "Server not responding")
                        completionHandler("")
                        return
                    }
                    
                    guard response != nil else{
                        completionHandler("")
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    let statusStr = (headers["Status"] as! String)
                    
                    print(statusStr)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completionHandler(statusStr )
                        return
                    }
                    
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    
                    print("json %@",json ?? "json nil")
                    
                    if let jsn = json {
                        if (!(jsn["status"] as! Bool)) {
                            DispatchQueue.main.async {
                                let apiClass = ApiClass()
                                apiClass.AlertView(message: (jsn["message"] as? String)!)
                            }
                            completionHandler(statusStr)
                            return
                        }
                        else{
                            completionHandler(statusStr)
                        }
                        
                    }
                    else{
                        completionHandler(statusStr)
                        return
                    }
                    
                }
                task.resume()
                
            }
        }else{
            completionHandler("")
            AlertView(message: KAInternet)
        }
        
    }
    @objc func forgetPassword(forgetDict: Dictionary<String,String>,strUrl: String, completionHandler: @escaping (Dictionary<String,Any>?) ->Void) {
        if CommonFunctions.reachabiltyCheck()
        {
            
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+strUrl)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            if JSONSerialization.isValidJSONObject(forgetDict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: forgetDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: forgetDict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakSelf = self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        completionHandler(nil)
                        CommonFunctions.alertTitle("Myles", withMessage: "Server not responding")
                        return
                    }
                    guard response != nil else{
                        completionHandler(nil)
                        return
                    }
                    guard data != nil else {
                        print("no data found: \(error)")
                        completionHandler(nil)
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        completionHandler(nil)
                        return
                    }
                    
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                    print("forgrt password",json ?? "forgrt password nil")
                        if json?["status"] as! NSInteger  == 1
                        {
                           
                            completionHandler(json)

                        }
                    else
                        {
                            DispatchQueue.main.async {
                                weakSelf.AlertView(message: json?["message"] as! String)
                            }
                            completionHandler(nil)
 
                    }

                }
                task.resume()
            }
        }else{
            completionHandler(nil)
            
            AlertView(message: KAInternet)
        }
    }
    
    
    @objc func AlertView(message:String)  {
//        let alertController = UIAlertController(title: "Myles", message: message, preferredStyle: .alert)
//        
//        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(defaultAction)
//        
//        present(alertController, animated: true, completion: nil)
        DispatchQueue.main.async {
            let alert = UIAlertView(title: "Myles", message: message, delegate: self, cancelButtonTitle: "OK")
            
            alert.show()
        }
    }
    
    @objc func SocialSignInorUP(dict: Dictionary<String,String>,strUrl: String,completionHandler: @escaping (Dictionary<String,Any>?) ->Void)
    {
        if CommonFunctions.reachabiltyCheck()
        {
            print("dict social login: ", dict)
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+strUrl)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            if JSONSerialization.isValidJSONObject(dict)
            {
                request.httpBody = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                
                let task = session.dataTask(with: request as URLRequest) {[weakSelf = self] data, response, error in
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(error?.localizedDescription)")
                        completionHandler(nil)
                        CommonFunctions.alertTitle("Myles", withMessage: "Server not responding")
                        return
                    }
                    
                    guard data != nil else {
                        print("no data found: \(error)")
                        completionHandler(nil)
                        return
                    }
                    
                    guard response != nil else{
                        completionHandler(nil)
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    let statusStr = headers["Status"] as? String ?? nil
                    guard statusStr != nil && (statusStr == "1")else{
                        completionHandler(nil)
                        return
                    }
                    
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>

                    completionHandler(json)
                    
                }
                task.resume()
            }
        }else{
            completionHandler(nil)
            AlertView(message: KAInternet)
        }
    }
    
    
    @objc public func ApplyCouponCode(dict : NSMutableDictionary , strURL:String, completionHandler: @escaping ([String:Any]?) ->Void){
        if CommonFunctions.reachabiltyCheck()
        {
            
            print(SiteAPIURL+strURL)
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+strURL)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            if JSONSerialization.isValidJSONObject(dict)
            {
                print(dict)
                request.httpBody = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                let task = session.dataTask(with: request as URLRequest) { data, response, error in
                    
                    guard response != nil else{
                        completionHandler(nil)
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    
                    // To change the API Request - Sourabh
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    
                    guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! [String:Any] else {
                      
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    //                let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                    print("jsonresponse = \(json) ")
                    if (!(json["status"]! as! Bool)) {
                        print(json["status"]!)
//                        DispatchQueue.main.async {
//                            let apiClass = ApiClass()
//                            apiClass.AlertView(message: (json["Reason"] as? String)!)
//                        }
                        completionHandler(json)
                        return
                    }
                    
                        completionHandler(json)
                }
                task.resume()
                
            }
        }else
        {
            self.AlertView(message: KAInternet)
            completionHandler(nil)
            return
            //        CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
        }
    }
    
    
    @objc public func getCouponListing(dict : [String:Any] , strURL:String, completionHandler: @escaping ([[String:Any]]?) ->Void){
        if CommonFunctions.reachabiltyCheck()
        {
            print(dict)
            print(SiteAPIURL+strURL)
            let request = NSMutableURLRequest(url: NSURL(string: SiteAPIURL+strURL)! as URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = (headers as NSDictionary) as? [String : String]
            if JSONSerialization.isValidJSONObject(dict)
            {
                print(dict)
                request.httpBody = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let session = URLSession.shared
                session.configuration.timeoutIntervalForRequest = 15
                print("try! request: \(try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted))")
                
                let task = session.dataTask(with: request as URLRequest) { data, response, error in
                    
                    guard response != nil else{
                        completionHandler(nil)
                        return
                    }
                    var headers = (response as! HTTPURLResponse).allHeaderFields
                    
                    // To change the API Request - Sourabh
                    let statusStr = (headers["Status"] as! String)
                    guard !statusStr.isEmpty && (statusStr == "1")else{
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    
                    guard error?.localizedDescription == nil else{
                        print("error.debugDescription: \(String(describing: error?.localizedDescription))")
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    
                    
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                    
                    guard let json = try? JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject> else {
                        
                        self.AlertView(message: "Server not responding")
                        completionHandler(nil)
                        return
                    }
                   print("jsonresponse = \(json) ")
                    if (!(json["status"]! as! Bool)) {
                        print(json["status"]!)
                        DispatchQueue.main.async {
                            let apiClass = ApiClass()
                            apiClass.AlertView(message: (json["message"] as? String)!)
                            completionHandler(nil)
                        }
                        
                        return
                    }
                    let response = json["response"]! as? [String:Any]
                    let promoCoupon = response!["promo_coupons"] as? [[String:Any]]
                    print(promoCoupon)
                    completionHandler(promoCoupon)
                    
                   // completionHandler(json)
                }
                task.resume()
                
          }
        }else
        {
            self.AlertView(message: KAInternet)
            completionHandler(nil)
            return
            //        CommonFunctions.alertTitle("Myles", withMessage:KAInternet)
            
        }
    }
    
}



@objc class SearchCarModel : NSObject {
    
    @objc var cityId = String()
    @objc var fromDate = String()
    @objc var toDate = String()
    @objc var pickUpTime = String()
    @objc var dropOffTime = String()
    @objc var subLocations = String()
    @objc var duration = String()
    //For Ladakh
    @objc var customPkgYN = String()
    //var userAppVersion = String()


    @objc init(dict:Dictionary<String, String>) {
        cityId = dict["city"] ?? "0"
        fromDate = dict["FromDate"]!
        toDate = dict["ToDate"]!
        pickUpTime = dict["PickUpTime"]!
        dropOffTime = dict["DropOffTime"]!
        subLocations = dict["SubLocations"]!
        duration = dict["Duration"]!
        customPkgYN = dict["CustomPkgYN"]!
        //userAppVersion = dict["userAppVersion"]!
    }
  }
    /**
     LoginModel
     */
    
    @objc class LoginModel: NSObject
    {
        @objc var status = 0
        @objc var strMessage = ""
        
        @objc var LoginResponseObj : LoginResponse?
        
        @objc init(dict: Dictionary<String, Any>)
        {
            if dict["status"] as! NSInteger == 1 {
                
                status = dict["status"] as! NSInteger
                LoginResponseObj = LoginResponse(dictResponse: dict["response"] as! Dictionary<String, Any>)

            }
            else
            {
                status = dict["status"] as! NSInteger
            }
            
            strMessage = dict["message"] as! String
        }
    }
@objc class LoginResponse: NSObject
    {
    @objc var strEmail = ""
    @objc var strLname = ""
    @objc var strFname = ""
    @objc var strUserId = ""
    @objc var strDob : String?
    @objc var strSubscribe = ""

        
    @objc init(dictResponse: Dictionary<String, Any>)
        {
            if let email = dictResponse["emailId"] {
                strEmail = email as! String
            }
            if let fname = dictResponse["fname"] {
                strFname = fname as! String
            }
            if let lname = dictResponse["lname"] {
                strLname = lname as! String
            }
            if let userId = dictResponse["userId"] {
                strUserId = userId as! String
            }
            
            if var subscribe = dictResponse["subscribe"] {
                subscribe = nullToNil(value: dictResponse["subscribe"]) ?? "null"
                strSubscribe = subscribe as! String
            }
            
            if let dob = dictResponse["DOB"] {
                 strDob = dob as? String ?? nil
            }
        }
        
    }
 func nullToNil(value : Any?) -> Any? {
    if value is NSNull {
        return nil
    } else {
        return value
    }
}



    @objc class CouponCodeClass:NSObject{
    
       
    }

