//
//  Config.M
//  Myles
//
//  Created by Myles on 23/01/16.
//  Copyright (c) 2015 Divya. All rights reserved.
//



#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CommonFunctions.h"

#define LOCATION_NOTIFICATION @"LOCATION_NOTIFICATION"
#define METERS_PER_MILE 1609.344
//#ifndef DEBUG
//#define NSLog(...);
//#endif


//#ifdef DEBUG
//#    define NSLog()
//#else
//#    define NSLog(...)
//#endif

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

// Chetan Rajauria
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#define IS_IPHONE_XS_MAX (IS_IPHONE && SCREEN_MAX_LENGTH == 896.0)


#define DEVICE_SIZE [[[[UIApplication sharedApplication] keyWindow] rootViewController].view convertRect:[[UIScreen mainScreen] bounds] fromView:nil].size

#define k_Citykey           @"City"

#define ACCEPTABLE_CHARECTERS_EMAIL @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
#define ACCEPTABLE_CHARECTERS_NAME @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"


@interface Config : NSObject {

}

//configuration section...


extern  NSString		*SiteAPIURL;
extern NSString		*AboutUsURL;
extern NSString		*ConatctUsURL;
extern NSString *accessTokenURL;
extern NSString *registerCutomerIDURL;
//for current location
//extern CLLocationManager *locationManager; //
//extern CLLocation *currentLocation;        // For Latitude & Longitude
//extern CLLocationCoordinate2D coordinates; //

extern float			 core_latitude;
extern float		     core_longitude;

extern NSString          *deviceToken;

// WEBSERVICES PATH
extern NSString          *KUSER_REGISTRATION ;
extern NSString          *KPHONE_NUMBER_VERIFICATION;
extern NSString          *KUSER_LOGIN;
extern NSString          *KCHANGE_PASSWORD;
extern NSString          *KFORGOT_PASSWORD;
extern NSString          *KCITY_LIST;
extern NSString          *KSUBLOCATION_LIST;
extern NSString          *KRESEND_CODE;
extern NSString          *KUSERINFO_FATCH;
extern NSString          *KUSERINFO_UPDATE;
extern NSString          *KGETPACKAGE_CITYWISE;
extern NSInteger          KTIMEOUT_INTERVAL;
extern NSString           *KActivePkgType ;
extern NSString           *kAdditionalServices;
extern NSString           *KgetCarDetails;
extern NSString           *KbookingHistory;
extern NSString           *KcancelBooking;
extern NSString           *kCreateBooking;
extern NSString           *kMerchantId;
extern NSString           *kSchemeId;
extern NSString           *KuploadDocument;
//extern NSString           *kPanCardUpload;
extern NSString           *KpreAuthCreation;
extern NSString           *KupdateCheckList;
extern NSString           *KgetNextAvailability;
extern NSString           *KdeleteFile ;
extern NSString           *KdeviceUpdateOnLogout ;
extern NSString           *KmylesCenterNo;
extern NSString           *KcustomerRating;
extern NSString           *kapplyDiscount;
extern NSString           *kgetDiscount;
extern NSString           *KcheckLockUnlock ;
extern NSString           *KlockUnlock;
extern NSString           *KgetChecklist;
extern NSString           *KgetActiveBookings;
extern NSMutableDictionary *kActiveBookingData;
extern NSString            *kBookingTermsAndConditions;
//extern NSString            *kAgreementAcceptance;
extern NSString            *kRoadsideSupport;
extern NSString            *kShareApi;
extern NSString            *sharingText;
extern NSInteger          personRecordId;
extern NSString           *insertAddress;
extern NSString           *kFetchAddress;
extern NSString           *kEditAddress;
extern NSString           *kGetZoneandSubLoc;
extern NSString           *kMylesCarDetailsV1;
extern NSString           *kMylesCarDetailsWithHomePickUpAndDropOff;
extern NSString           *kRestM1FetchAppVersions;
extern NSString           *kUserDetailsBeforeBooking;
extern NSString           *kGetSublocationAirportCharges;

//New ocject for login deferment
extern NSString           *kRestM1verifyMobileNo;
extern NSString           *KRestM1sendOTPonForgot;
extern NSString           *kRestM1ResetPassword;






@end
