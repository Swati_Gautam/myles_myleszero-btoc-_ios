//
//  FireBase Events.swift
//  Myles
//
//  Created by Akanksha Singh on 08/01/18.
//  Copyright © 2018 Divya Rai. All rights reserved.
//

import UIKit
import FirebaseAnalytics

enum event:String{
   case mobileVerify = "Submit_contact"
   case otpVerify = "OTP_verified"
   case accountCreated = "create_acc"
    case signUp = "signUp_Booking"
    
}


extension UIViewController {

    
    func BookingMobileNoSubmit(){
        fireEventFirebase(name: event.mobileVerify.rawValue)
    }
    
    func BookingOtpVerifySubmit(){
        fireEventFirebase(name: event.otpVerify.rawValue)
    }
    
    func AccountCreationSubmit(){
        fireEventFirebase(name: event.accountCreated.rawValue)
    }

    func BookingSignUpEvent(){
        fireEventFirebase(name: event.signUp.rawValue)
    }
    
    func fireEventFirebase(name:String){
        FIRAnalytics.logEvent(withName: name, parameters: nil)
    }
}
