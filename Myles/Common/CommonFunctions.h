
#import <Foundation/Foundation.h>
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Additions.h"
#import <objc/runtime.h>
#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>
/*
 *  System Versioning Preprocessor Macros
 */
 

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define kDeviceId [[UIDevice currentDevice] uniqueIdentifier]


#define TimeStamp [NSString stringWithFormat:@"%.0f",[[NSDate date] timeIntervalSince1970] * 1000]

//App Colors

#define UICOLOR_ORANGE  [UIColor colorWithRed:222.0f/255.0f green:70.0f/255.0f blue:39.0f/255.0f alpha:1.0f]
#define UICOLOR_OFFWHITE  [UIColor colorWithRed:238.0f/255.0f green:240.0f/255.0f blue:242.0f/255.0f alpha:1.0f]


//ride history cell custom color
#define UICOLOR_CANCELLED  [UIColor colorWithRed:177.0f/255.0f green:137.0f/255.0f blue:4.0f/255.0f alpha:1.0f]
#define UICOLOR_COMPLETED  [UIColor colorWithRed:223.0f/255.0f green:70.0f/255.0f blue:39.0f/255.0f alpha:1.0f]
#define UICOLOR_INPROGRESS  [UIColor colorWithRed:11.0f/255.0f green:97.0f/255.0f blue:11.0f/255.0f alpha:1.0f]
#define UICOLOR_UPCOMING  [UIColor colorWithRed:229.0f/255.0f green:106.0f/255.0f blue:82.0f/255.0f alpha:1.0f]

//rgb(102, 102, 102)
#define UICOLOR_GRAYCELLTEXT  [UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1.0f]
#define UICOLOR_FontColor  [UIColor colorWithRed:87.0f/255.0f green:87.0f/255.0f blue:87.0f/255.0f alpha:1.0f]
#define UICOLOR_RedFontColor  [UIColor colorWithRed:224.0f/255.0f green:71.0f/255.0f blue:40.0f/255.0f alpha:1.0f]
#define UICOLOR_CouponBGColorR [UIColor colorWithRed:255.0f/255.0f green:250.0f/255.0f blue:249.0f/255.0f alpha:1.0f]
#define UICOLOR_CouponBGColorG [UIColor colorWithRed:249.0f/255.0f green:255.0f/255.0f blue:238.0f/255.0f alpha:1.0f]

#define UICOLOR_CouponBorderColorR [UIColor colorWithRed:234.0f/255.0f green:213.0f/255.0f blue:208.0f/255.0f alpha:1.0f]
#define UICOLOR_CouponBorderColorG [UIColor colorWithRed:217.0f/255.0f green:237.0f/255.0f blue:180.0f/255.0f alpha:1.0f]

//Futura
#define UIFONT_REGULAR_FONT(s) [UIFont fontWithName:@"Futura-Medium" size:s]
#define UIFONT_ITALIC_FONT(s) [UIFont fontWithName:@"Futura-MediumItalic" size:s]
#define UIFONT_BOLD_FONT(s) [UIFont fontWithName:@"Futura-CondensedExtraBold" size:s]
// Motor Oil 1937 M54
#define UIFONT_REGULAR_Motor(s) [UIFont fontWithName:@"Motor Oil 1937 M54" size:s]


//[UIFont fontWithName:@"System" size:36]
#define UIFONT_REGULAR_System(s) [UIFont fontWithName:@"System" size:s]


// openSans font

#define UIFONT_REGULAR_OPENSANS(s) [UIFont fontWithName:@"OpenSans" size:s]
#define UIFONT_BOLD_OPENSANS(s) [UIFont fontWithName:@"OpenSans-Bold" size:s]
#define UIFONT_SEMIBOLD_OPENSANS(s) [UIFont fontWithName:@"OpenSans-Semibold" size:s]
#define UIFONT_ITALIC_OPENSANS(s) [UIFont fontWithName:@"OpenSans-Italic" size:s]

//Roboto
#define UIFONT_Medium_RobotoFONT(s) [UIFont fontWithName:@"Roboto-Medium" size:s]
#define UIFONT_REGULAR_RobotoFONT(s) [UIFont fontWithName:@"Roboto-REGULAR" size:s]
#define UIFONT_BOLD_RobotoFONT(s) [UIFont fontWithName:@"Roboto-Bold" size:s]

//Images
#define UIImageName(s)          [UIImage imageNamed:s]

//StoryBoard Name
#define k_storyBoard(name) [UIStoryboard storyboardWithName:name bundle:nil];
#define k_mainStoryBoard                @"MainStoryboard"
#define k_profileStoryBoard             @"Profile"
#define k_MyRideStoryBoard              @"MyRides"
#define k_registrationStoryBoard         @"RegistrationProcess"
#define k_HelpScreenStoryBoard             @"HelpScreen"

// Navigation Controllers Indetifier

#define k_homeNavigation                @"HomeNavigation"
#define k_profileNavigation              @"ProfileNavigation"
#define k_RidesNavigation                @"RidesNavigation"
#define k_registrationNavigation        @"registrationNavigation"

//COntroller Identifier
#define k_HomeViewController            @"HomeVC"
#define k_CarListViewController         @"CarListViewController"
#define k_DocumentUploadController      @"documentuploadidentifier"
#define k_UserDocumentUploadVC          @"UserDocumentUploadVC"
#define k_BaseViewController            @"BaseViewController"
#define k_MenuViewController             @"MenuViewController"
#define k_TrackMyRideViewController       @"TrackrideVC"
#define k_notificationVCIdentifier      @"notification"
#define k_bookingTimeSelectionVC        @"bookingTimeSelectionVC"
#define k_BookingDetailsVC              @"BookingDetails"
#define k_AboutUsVC                     @"aboutus"
#define k_ContactUsVC                   @"contactus"
#define k_CheckListVC                   @"checklistcontroller"
#define k_feedbackVc                    @"feedback"
#define k_termsVC                    @"termsVC"
#define k_MainScreenVC                    @"idMainScreenVC"


#define k_profileController                    @"kProfileIdentifier"

#define k_application     (UIApplication*)[UIApplication sharedApplication];


// API Constant
#define KAPP_Install @"appStatus"
#define KUSER_ID @"UserId"
#define IS_LOGIN @"login"
#define KAuthToken @"auth_token"
#define KUSER_PASSWORD @"KUserPassword"
#define KUSER_FNAME @"KUserFname"
#define KUSER_LNAME @"KUserLname"

#define KUSER_TYPE @"KUserType"
#define KDB_PATH @"databasePath"
#define KUSER_EMAIL @"useremail"
#define KUSER_NAME @"username"
#define KUSER_PHONENUMBER @"userephonenumber"
#define KUSER_SUBSCRIBE @"subscribe"


#define kUserfeedbackBookingId          @"feedbackBookingid"

#define KRating             @"rating"
#define KRemarks            @"remarks"
#define KBookingid          @"bookingid"
//Base View Controller
#define k_backBtnImg            @"backBtn"


// CAR List View Controller
#define k_HourlyBtnTitle                    @"Hourly"
#define k_DailyBtnTitle                    @"Daily"
#define k_WeeklyBtnTitle                    @"Weekly"
#define k_MonthlyBtnTitle                    @"Monthly"

// MENU View Controller

#define k_HomeTitle                          @"HOME"
#define k_MyRideTitle                          @"MY RIDES"
#define k_WalletTitle                          @"WALLET"
#define k_ProfileTitle                          @"MY PROFILE"
#define k_AboutUsTitle                          @"ROADSIDE ASSISTANCE"
#define k_COnatctUsTitle                         @"CONTACT US"
#define k_SharingTitle                         @"SHARE"
#define k_AmenitiesDetails                         @"Amenities Details"
#define k_DocumentDetails                         @"Document Details"
#define k_RateTitle                         @"RATE US"
#define k_SubscribeCar                          @"SUBSCRIBE A CAR"

// SWUITableViewCell
#define k_SWUITableViewCellIdentifier           @"menuItem"

//Check List
#define k_unlockMyridebtnTitle                  @"UNLOCK MY RIDE"
//UserProfileCustomCell

#define k_basicInfo                 @"UserBasicInfo"
#define k_emailInfo                 @"UserEmailInfo"
#define k_addressInfo               @"UserAddressInfo"
#define k_proofInfo                 @"UserProofsInfo"

#define k_userFullName              @"fullName"
#define k_userEmailId               @"emailId"
#define k_userAddress               @"Address"

//Notifications
#define k_notificationType          @"type"
#define k_feedback                  @"feedback"
#define k_feedbackbookingid         @"bookingid"
#define k_general                   @"general"
#define k_pickup                    @"pickup"

#define k_bookingconfirmation                    @"bookingconfirmation"


#define k_ApiMessageKey               @"message"
/**
 Declaring Login Enum for Checking whether Logged In With Email/FB
 */
typedef enum : NSUInteger {
    kCheck_Email_Availability,
    kLogin_With_Email,
    kLogin_With_FB,
} kServiceType;
typedef void(^myCompletion)(NSDictionary *dictData);
typedef void(^saveAddressComplete)(NSDictionary *SaveAddress);
@interface CommonFunctions : NSObject<NSURLSessionDelegate,NSURLSessionDataDelegate,CLLocationManagerDelegate> {
   
}



/**
 *  Singleton Class shared Instance
 *
 *  @return Common Functions Instance
 */

+(CommonFunctions*) sharedInstance;


/**
 *  Path For Document Directory
 *
 *  @return path
 */
+ (NSString *)documentsDirectory;
/**
 *  Get file Exist in  Document Directory
 *
 *  @return Status
 */
+ (BOOL) documentExistwithName : (NSString *)name;
/**
 *  Defalut OpenURL provided by UIApplication Singleton Class
 *
 *  @param address 
 */
+ (void)openEmail:(NSString *)address;
+ (void)openPhone:(NSString *)number;
+ (void)openSms:(NSString *)number;
+ (void)openBrowser:(NSString *)url;
+ (void)openMap:(NSString *)address;

/**
 * Normal Alert view with only Ok btn
 */

/**
 * Normal Alert view with only Ok btn
 */
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate;
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg ;

+ (UIViewController *)returntopMostController;


+(BOOL)isDeviceTimeFormat_24;

+(void)showNoNetworkAlert;


/**
 * user for check that value is empty or not 
 * shows a alert : server not responding error
 */
+ (BOOL)isValueNotEmpty:(NSString*)aString;

/**
 * shows a alert : server not found , try again later
 */
+ (void)showServerNotFoundError ;

/**
 *  Check whether Device is Retina Display or not
 */
+ (BOOL)isRetineDisplay;


- (BOOL)connected ;

/**
 *  check reachability
 *
 *  @return status (TRUE?FALSE)
 */

+(BOOL) reachabiltyCheck;

/**
 *  Check status whether connected to network or not
 *
 *  @return status (TRUE?FALSE)
 */
+ (BOOL) connectedToNetwork;

/**
 *  PopUp Animation
 *
 *  @return CAKeyframeAnimation instance
 */
+ (CAKeyframeAnimation *) attachPopUpAnimation;

/**
 *  setting Orienatatin In Window
 */
+(void)settingOrienatationInWindow;

/**
 *  Set Current Interface Orientation
 *
 *  @param SetYesOrNo Boolen Value
 */
+(void)setInterfaceOrientation :(BOOL)SetYesOrNo;

/**
 *  Get Current Interface Orientation
 *
 *  @return Boolean Value
 */
+(BOOL)getInterfaceOrientation;

/**
 *  Make JSon from NSDictionary using NSJSonSerialization and convert into NSString
 *
 *  @param dictionary argument
 *
 *  @return NSString instance
 */
+(NSString*)getJSONFromDictionary:(NSDictionary*)dictionary;


/**
 *  Serialize NSData into NSDictionary using NSJSonSerialization
 *
 *  @param thedata data as argument
 *
 *  @return NSDictionary
 */
+(NSDictionary *)getDictionaryFromJsonData:(NSData *)thedata;

/**
 *  Convert hex Color into Uicolor
 *
 *  @param hex hex color format as an argument
 *
 *  @return UIColor instance
 */

+(UIColor*)colorWithHexString:(NSString*)hex;


/**
 *  Calculate Time on the basis of Start and End Time
 *
 *  @param strStartTime startTime
 *  @param strEndTime   endTime
 *
 *  @return time as string
 */
+(NSString *)TimeCalculateWithStartTime:(NSString*)strStartTime andEndTime:(NSString *)strEndTime;

/**
 *  get Height For Text
 *
 *  @param text
 *  @param font
 *  @param size
 *
 *  @return height of text as per size and font specified as Float
 */

+(CGFloat)getHeightForText:(NSString *)text font:(UIFont *)font size:(CGSize)size;

/**
 *  get Width For Text
 *
 *  @param text
 *  @param font
 *  @param size
 *
 *  @return width of text as per size and font specified as Float
 */
+(CGFloat)getWidthForText:(NSString *)text font:(UIFont *)font size:(CGSize)size;

+(UIStatusBarStyle)preferredStatusBarStyle;

+(void)setLeftPaddingToTextField:(UITextField *)txt andPadding:(int)padding;
+(NSString*)formatNumber:(NSString*)mobileNumber;
+(NSInteger)getLength:(NSString*)mobileNumber;
+(void)setTextFieldPlaceHolderText:(UITextField*)txtFeild withText:(NSString*)strPlaceHolder;

+(NSURLSession *)defaultSession;



+ (NSInteger ) getMajorSystemVersion;

/**
 *  GENERATE UNIQUE ID
 *
 */
+ (NSString *)getUuid;
// Open calling Feature
+ (void)callPhone : (NSString *)phNo;
+(NSDate *)dateFromString:(NSString*)string;

+(NSInteger )getPreAuthStatusAsPerStartTime : (NSString *)time Date : (NSString *)date;
+ (NSString *)formatDatePayNimo:(NSDate *)date;


// image Encoding
+ (NSString *)encodeToBase64String:(NSData *)image;

//Add ChildView Controller

+ (void)addFeedBackViewAsChildView : (UIViewController *)childView;

+ (NSArray *) gettimeAndDateinDisplayFormet : (NSString *)fromDate FromTime : (NSString *)fromtime ToDate : (NSString *)todate ToTime : (NSString *)totime DateFormet : (NSString *)dateFormet;

+(NSString *)getKeychainPassword:(NSString *)username;
+(void) writeToKeyChainUserName:(NSString *)username Password:(NSString *)password;
+(void) searchKeyChainForUserName:(NSString *)username :(void (^)(NSString* password))completion1;
+(void) updateKeychainForUsername:(NSString *)username Password:(NSString *)password;
+(void) ifNotExistsKeychainForUsername:(NSString *)username Password:(NSString *)password;
-(void) ecommerceTracking:(NSString *)paymentTypeKeyword TranactionAmount : (NSString *)TrackingAmount TrackId:(NSString *)trackId BookingId:(NSString *)BookingId :(NSString *)strmodel
;
+ (NSString *)getCurrentDate;
+(NSTimeInterval )getMilliSecond;
+(NSString* )finalMilliSecond: (NSTimeInterval )previousData current:(NSTimeInterval )updatedData;
+(NSString *)createStrigForSavingData :(NSString *)fromScreen key:(NSString*)keyVaue startTime:(NSString *)startTime endTime:(NSString*)endTime milliSec:(NSString *)milliSecond;

+(void)GetCurrentLocation :(myCompletion) compblock;
+(void)SavePickUpAddress :(NSDictionary *)dictBodydata PassUrl:(NSString *)Url :(saveAddressComplete)isFinish;

+(void)logoutcommonFunction;
+(void)resetDefaults;
+(void)getAccessTokenWithCompletion:(void (^)(void))completion;
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate :(UINavigationController *)navcontroller;

+(NSString *)convertDate : (NSString *)strDate;
+(NSString *)IndianTimeConverter : (NSDate *)date;
+(void) writeToLogFile:(NSString*)content;
@end
