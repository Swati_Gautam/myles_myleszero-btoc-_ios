//
//  CommonFunctions.m
//  Myles
//  Created by Myles on 23/01/16.
//  Copyright (c) 2015 Divya. All rights reserved.
//

#import "CommonFunctions.h"
#import "AppDelegate.h"
#import "FeedbackVC.h"
#import "SorryAlertViewController.h"
#import <Google/Analytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
//@import GooglePlaces;
//@import GooglePlacePicker;
#import <CoreLocation/CoreLocation.h>
#import "NSString+MD5.h"
#import "Myles-Swift.h"

//#import "AlertConstant.h"

static BOOL YESNO;
static NSString *loaderAssociationKey = @"loaderAssociationKey";

static CommonFunctions * sharedVariable;
@implementation CommonFunctions


+(CommonFunctions*) sharedInstance
{
    if (sharedVariable==nil) {
        sharedVariable=[[CommonFunctions alloc]init];
    }
    return sharedVariable;
}

+ (NSString *)getUuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    
    NSString *str = (__bridge_transfer NSString *)uuidStringRef;
    NSArray *ar = [str componentsSeparatedByString:@"-"];
    //NSLog(@"%@",ar);
    return  [ar lastObject];
}

+ (NSString *)documentsDirectory {
    NSArray *paths =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    return [paths objectAtIndex:0];
    
    //comment from Amzad
}
+ (NSString *)stringFromDeviceToken:(NSData *)deviceToken
{
    NSUInteger length = deviceToken.length;
    if (length == 0) {
        return nil;
    }
    const unsigned char *buffer = deviceToken.bytes;
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(length * 2)];
    for (int i = 0; i < length; ++i) {
        [hexString appendFormat:@"%02x", buffer[i]];
    }
    return [hexString copy];
}
+ (BOOL) documentExistwithName : (NSString *)name
{
    NSString* foofile = [[self documentsDirectory] stringByAppendingPathComponent:@"foo.html"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    return  fileExists;
}

+ (void)openEmail:(NSString *)address {
    NSString *url = [NSString stringWithFormat:@"mailto://%@", address];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openPhone:(NSString *)number {
    NSString *url = [NSString stringWithFormat:@"tel://%@", number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openSms:(NSString *)number {
    NSString *url = [NSString stringWithFormat:@"sms://%@", number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openBrowser:(NSString *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)openMap:(NSString *)address {
    NSString *addressText = [address stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSString *url = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@", addressText];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate{
    
    UIApplication *application = k_application;
    UINavigationController *navcontroller = (UINavigationController *)application.keyWindow.rootViewController;
    
    if ([CommonFunctions getMajorSystemVersion] <= 7.0)
    {
        UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:aTitle
                                                         message:aMsg
                                                        delegate:delegate
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        if(([navcontroller isKindOfClass:[UINavigationController class]]) || ([navcontroller isKindOfClass:[SWRevealViewController class]]))
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:aTitle message:aMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertaction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
            }];
            [alert addAction:alertaction];
            
            
            if (([navcontroller isKindOfClass:[UINavigationController class]]) && ([[navcontroller viewControllers] count]))
            {
                [[[navcontroller viewControllers] lastObject]  presentViewController:alert animated:true completion:nil];
            }
            else
            {
                SWRevealViewController *controller = (SWRevealViewController *)navcontroller;
                [controller.frontViewController presentViewController:alert animated:true completion:nil];
            }
        }
    }
}

/**
  show alert 
 pass navigation controller
 */
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate :(UINavigationController *)navcontroller{
    
    UIApplication *application = k_application;
    
    if ([CommonFunctions getMajorSystemVersion] <= 7.0)
    {
        UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:aTitle
                                                         message:aMsg
                                                        delegate:delegate
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        if(([navcontroller isKindOfClass:[UINavigationController class]]) || ([navcontroller isKindOfClass:[SWRevealViewController class]]))
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:aTitle message:aMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertaction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
            }];
            [alert addAction:alertaction];
            
            
            if (([navcontroller isKindOfClass:[UINavigationController class]]) && ([[navcontroller viewControllers] count]))
            {
                [[[navcontroller viewControllers] lastObject]  presentViewController:alert animated:true completion:nil];
            }
            else
            {
                SWRevealViewController *controller = (SWRevealViewController *)navcontroller;
                [controller.frontViewController presentViewController:alert animated:true completion:nil];
            }
        }
    }
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg
{
    dispatch_async(dispatch_get_main_queue(), ^{

        [self alertTitle:aTitle withMessage:aMsg withDelegate:nil];
    });
}




//Payment Alert


+ (UIViewController *)returntopMostController
{
    UIViewController *controller;
    id rootController = [[[AppDelegate getSharedInstance] window] rootViewController];
    
    
    if( [rootController isKindOfClass:[SWRevealViewController class]])
    {
        
        UINavigationController *navcontroller = (UINavigationController *)[(SWRevealViewController *)rootController frontViewController];
        
        
        if (([navcontroller isKindOfClass:[UINavigationController class]]) && ([[navcontroller viewControllers] count]))
        {
            controller = [[navcontroller viewControllers] lastObject];
        }
        else
        {
            controller = (SWRevealViewController *)navcontroller;
        }
    }
    else if ([rootController isKindOfClass:[UINavigationController class]])
    {
        if ([[rootController viewControllers] count])
        {
            controller = [[rootController viewControllers] lastObject];
        }
    }
    return controller;
}


+ (BOOL)isValueNotEmpty:(NSString*)aString{
    if (aString == nil || [aString length] == 0){
        [CommonFunctions alertTitle:@"Sorry"
                        withMessage:@"Server not responding"];
        return NO;
    }
    return YES;
}

+ (void)showServerNotFoundError{
    [CommonFunctions alertTitle:@"Server Not Found"
                    withMessage:@"Please try again"];
}


+ (BOOL)isRetineDisplay{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        return YES;
    } else {
        // not Retine display
        return NO;
    }
}

//Class.m
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

+ (void)callPhone : (NSString *)phNo
{
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
        
    } else
    {
        [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}

#pragma mark
#pragma mark ReachabiltyCheck methods

+(BOOL) reachabiltyCheck
{
    BOOL status =YES;
    Reachability * reach = [Reachability reachabilityForInternetConnection];
    
    if([reach currentReachabilityStatus]==0)
    {
        status = NO;
    }
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    [reach startNotifier];
    return status;
}

+(void)showNoNetworkAlert{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    SorryAlertViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SorryAlertViewController"];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.alertType    = 4;
    UIViewController *topViewController = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [topViewController presentViewController:controller animated:YES completion:nil];

    });
}

-(BOOL)reachabilityChanged:(NSNotification*)note
{
    
    BOOL status =YES;
    //NSLog(@"reachabilityChanged");
    
    Reachability * reach = [note object];
    
    if([reach isReachable])
    {
        //notificationLabel.text = @"Notification Says Reachable"
        status = YES;
        //NSLog(@"NetWork is Available");
    }
    else
    {
        status = NO;
    }
    return status;
}

+ (BOOL) connectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags)
    {
        //NSLog(@"Error. Could not recover network reachability flags");
        return NO;
    }
    
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    
    NSURL *testURL = [NSURL URLWithString:@"http://www.apple.com/"];
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:self];
    
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
}


#pragma mark -Date Function
+ (NSDate *)getDatefromString : (NSString *)start
{
    NSDateFormatter *formet = [[NSDateFormatter alloc] init];
    [formet setDateFormat:@"MM-dd-yyyy"];
    NSRange range = [start rangeOfString:@" "];
    start = [start substringToIndex:range.location];
    start = [start stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    NSDate *date = [formet dateFromString:start];
    return date;
}

#pragma mark - Other Methods
//add alert like animation

+ (CAKeyframeAnimation *) attachPopUpAnimation
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation
                                      animationWithKeyPath:@"transform"];
    
    CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
    CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
    CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    [animation setValues:frameValues];
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           [NSNumber numberWithFloat:1.0],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = .5;
    return animation;
    
}


+(void)settingOrienatationInWindow
{
    if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        //NSLog(@" ------ LANDSCAPE LEFT ORIENTATION ------- ");
        
    }
    else
    {
        //NSLog(@" ------ LANDSCAPE RIGHT ORIENTATION ------- ");
        
    }
}
// Get Current Interface Orientation
+(BOOL)getInterfaceOrientation
{
    return YESNO;
}

// Set Current Interface Orientation
+(void)setInterfaceOrientation :(BOOL)SetYesOrNo
{
    YESNO	=	SetYesOrNo;
}
+(NSString*)getJSONFromDictionary:(NSDictionary*)dictionary
{
    
    NSError *error;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *resultAsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return resultAsString;
}
+(NSDictionary *)getDictionaryFromJsonData:(NSData *)thedata{
    
    NSDictionary *datadict=nil;
    NSError *error;
    datadict=  [NSJSONSerialization JSONObjectWithData:thedata options:NSJSONReadingMutableContainers error:&error];
    
    
    if (error) {
        ////NSLog(@"%s  ---at line number--- %d",__func__,__LINE__);
    }
    
    return datadict;
}

+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
+(NSString *)TimeCalculateWithStartTime:(NSString*)strStartTime andEndTime:(NSString *)strEndTime
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate = [dateFormat dateFromString:strStartTime];
    NSDate *endDate = [dateFormat dateFromString:strEndTime];
    
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    double secondsInAnHour = 3600;
    
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    //    NSInteger numberOfDays = hoursBetweenDates/24;
    
    
    NSInteger hoursLeft = hoursBetweenDates%24;
    NSInteger minutesLeft = (hoursLeft*3600)%60;
    
    NSString *strTime = [NSString stringWithFormat:@"%ld HRS %ld MINS ",(long)hoursLeft,(long)minutesLeft];
    return strTime;
}
+(CGFloat)getHeightForText:(NSString *)text font:(UIFont *)font size:(CGSize)size {
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSParagraphStyleAttributeName:paragraphStyle};
    CGSize textSize = [text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size ;
    CGFloat textheight = textSize.height ;
    return textheight;
    
}
+(CGFloat)getWidthForText:(NSString *)text font:(UIFont *)font size:(CGSize)size {
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSParagraphStyleAttributeName:paragraphStyle};
    CGSize textSize = [text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size ;
    CGFloat texwidth = textSize.width ;
    
    
    return texwidth;
    
}

+(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
+(void)setLeftPaddingToTextField:(UITextField *)txt andPadding:(int)padding{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, padding, txt.frame.size.height)];
    txt.leftView = paddingView;
    txt.leftViewMode = UITextFieldViewModeAlways;
    
}
+(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    //NSLog(@"%@", mobileNumber);
    
    NSInteger length = [mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        //NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}


+(NSInteger)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSInteger length = [mobileNumber length];
    
    return length;
    
    
}
+(void)setTextFieldPlaceHolderText:(UITextField*)txtFeild withText:(NSString*)strPlaceHolder
{
    NSAttributedString *EmailPlaceholderIs = [[NSAttributedString alloc] initWithString:strPlaceHolder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    txtFeild.attributedPlaceholder=EmailPlaceholderIs;
    
}

+(NSURLSession *)defaultSession
{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    
    return session;
}


+(BOOL)isDeviceTimeFormat_24
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
    //NSLog(@"%@\n",(is24h ? @"YES" : @"NO"));

    return is24h;
}

//Version detail
+ (NSInteger ) getMajorSystemVersion {
    return  [[[UIDevice currentDevice] systemVersion] integerValue];
}

//Date Function

+(NSDate *)dateFromString:(NSString*)string
{
    NSDate *date = nil;
    NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:(NSTextCheckingTypes)NSTextCheckingTypeDate error:&error];
    NSArray *matches = [detector matchesInString:string
                                         options:0
                                           range:NSMakeRange(0, [string length])];
    
    NSLocale* currentLoc = [NSLocale currentLocale];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeDate) {
            //NSLog(@"Date : %@", [[match date] descriptionWithLocale:currentLoc]);
            date = [match date];
            
            break;
        }
    }
    return date;
    
}

+(NSInteger )getPreAuthStatusAsPerStartTime : (NSString *)time Date : (NSString *)date
{
    //NSLog(@"NSdate : %@",[NSDate date]);
    
    //NSLog(@"%@ , %@",time, date);
    
    NSString *timeString = time;
    timeString = [[timeString substringWithRange:NSMakeRange(0, 2)] stringByAppendingFormat:@":%@",[timeString substringWithRange:NSMakeRange(timeString.length-2, 2)]];
    
    
    NSString *completeStartDate = [date stringByAppendingFormat:@" %@",timeString];
    
    //NSLog(@"com : %@",completeStartDate);
    NSDate *formattedDate = [CommonFunctions dateFromString:completeStartDate];
    
    
    NSTimeInterval distanceBetweenDates = [formattedDate timeIntervalSinceDate:[NSDate date]];
    double secondsInAnHour = 3600;
    
    
    double hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    if (distanceBetweenDates > secondsInAnHour*2)
    {
        hoursBetweenDates = ceil(hoursBetweenDates);
    }
    
    //NSLog(@"hoursBetweenDates : %.01f",hoursBetweenDates);
    
    return hoursBetweenDates;
}

+ (NSString *)formatDatePayNimo:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    //    [dateFormatter setDateFormat:@"MM'/'dd'/'yyyy"];
    [dateFormatter setDateFormat:@"dd'-'MM'-'yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}


+ (NSArray *) gettimeAndDateinDisplayFormet : (NSString *)fromDate FromTime : (NSString *)fromtime ToDate : (NSString *)todate ToTime : (NSString *)totime DateFormet : (NSString *)dateFormet
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    NSDateFormatter *formet = [[NSDateFormatter alloc] init];
    NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    formet.locale = twelveHourLocale;
    
    [formet setDateFormat:dateFormet];
    NSDate *pickupdate = [formet dateFromString:fromDate];
    NSDate *dropOffDate = [formet dateFromString:todate];
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents * component =  [calender components:NSCalendarUnitMonth fromDate:pickupdate];
    NSInteger monthday = [component month];
    NSString *monthdaySymbol = [[formet monthSymbols] objectAtIndex:monthday -1 ];
    [formet setDateFormat:@"dd, yyyy"];
    [array addObject: [NSString stringWithFormat:@"%@ %@",monthdaySymbol,[formet stringFromDate:pickupdate]]];
    component =  [calender components:NSCalendarUnitMonth fromDate:dropOffDate];
    monthday = [component month];
    monthdaySymbol = [[formet monthSymbols] objectAtIndex:monthday -1 ];
    [array addObject: [NSString stringWithFormat:@"%@ %@",monthdaySymbol,[formet stringFromDate:dropOffDate]]];
    NSTimeInterval interval = 60 * 60 * [[fromtime substringToIndex:2] integerValue] + 60 * [[fromtime substringFromIndex:2] integerValue];
    [formet setDateFormat:@"hh:mm a"];
    pickupdate = [pickupdate dateByAddingTimeInterval:interval];
    [array addObject: [formet stringFromDate:pickupdate]];
    interval = 60 * 60 * [[totime substringToIndex:2] integerValue] + 60 * [[totime substringFromIndex:2] integerValue];
    dropOffDate = [dropOffDate dateByAddingTimeInterval:interval];
    [array addObject: [formet stringFromDate:dropOffDate]];
    
    return array;
}
+ (NSString *)encodeToBase64String:(NSData *)image {
    return [image base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}



+ (void)addFeedBackViewAsChildView  : (UIViewController *)childView
{
    
    UIViewController *parentController = [CommonFunctions returntopMostController];
    [childView didMoveToParentViewController:parentController];
    childView.view.frame = parentController.view.frame;
    [parentController.view addSubview:childView.view];
    [parentController addChildViewController:childView];
    [parentController.view bringSubviewToFront:childView.view];
    //[parentController.view bringSubviewToFront:childView.view];
}

#pragma mark -
#pragma mark - Get password from keychain -
/*
 - BY DIVYA TO GET PASSWORD FROM KEYCHAIN
 */

+(NSString *)getKeychainPassword:(NSString *)username
{
    /*
     Create a query dictionary to hold data for searching
     */
    
    NSMutableDictionary *keyChainItem = [[NSMutableDictionary alloc] init];
    NSString *userPassword;
    
    [keyChainItem setObject:(__bridge id)(kSecClassInternetPassword) forKey:(__bridge id<NSCopying>)(kSecClass)];
    
    [keyChainItem setObject:(__bridge id)(kSecAttrAccessibleWhenUnlocked) forKey:(__bridge id<NSCopying>)(kSecAttrAccessible)];
    if (username != nil) {
        [keyChainItem setObject:username forKey:(__bridge id<NSCopying>)(kSecAttrAccount)];
    }
    
    [keyChainItem setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id<NSCopying>)(kSecReturnData)];
    
    CFDataRef passwordData = NULL;
    
    /*
     Code to add security item to keychain
     */
    
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)(keyChainItem), (CFTypeRef *)&passwordData);
    if (status == noErr) {
        NSLog(@"Keychain item found");
        
        userPassword = [[NSString alloc] initWithBytes:[(__bridge_transfer NSData *)passwordData bytes]
                                                length:[(__bridge NSData *)passwordData length] encoding:NSUTF8StringEncoding];
        NSLog(@"Password from keychain:%@", userPassword);
    }
    else if(status == errSecItemNotFound){
        NSLog(@"Keychain item could not be found");
    }
    else {
        NSLog(@"Some other error occured");
    }
    return userPassword;
}

#pragma mark -
#pragma mark - SAVE PASSWORD TO KEYCHAIN -
/*
 - BY DIVYA TO SAVE PASSWORD TO KEYCHAIN
 */

+(void) writeToKeyChainUserName:(NSString *)username Password:(NSString *)password {
    
    /*
     Create a dictionary to hold data
     */
    
    NSMutableDictionary *keyChainItem = [[NSMutableDictionary alloc] init];
    [keyChainItem setObject:(__bridge id)(kSecClassInternetPassword) forKey:(__bridge id<NSCopying>)(kSecClass)];
    [keyChainItem setObject:(__bridge id)(kSecAttrAccessibleWhenUnlocked) forKey:(__bridge id<NSCopying>)(kSecAttrAccessible)];
    [keyChainItem setObject:username forKey:(__bridge id<NSCopying>)(kSecAttrAccount)];
    
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [keyChainItem setObject:passwordData forKey:(__bridge id<NSCopying>)(kSecValueData)];
    
    /*
     Code to add security item to keychain
     */
    
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)(keyChainItem), NULL);
    if (status == noErr) {
        NSLog(@"Item added to your keychain");
    }
    else if (status == errSecDuplicateItem){
        NSLog(@"Item Already exists");
    }
    else {
        NSLog(@"Some other error occured");
    }
    
}

#pragma mark -
#pragma mark - SEARCH KEYCAHIN FOR USERNAME -

/*
 - BY DIVYA TO READ PASSWORD FROM KEYCHAIN
 */

+(void) searchKeyChainForUserName:(NSString *)username :(void (^)(NSString* password))completion1 {
    
    /*
     Create a query dictionary to hold data for searching
     */
    
    NSMutableDictionary *keyChainItem = [[NSMutableDictionary alloc] init];
    
    [keyChainItem setObject:(__bridge id)(kSecClassInternetPassword) forKey:(__bridge id<NSCopying>)(kSecClass)];
    
    [keyChainItem setObject:(__bridge id)(kSecAttrAccessibleWhenUnlocked) forKey:(__bridge id<NSCopying>)(kSecAttrAccessible)];
    
    [keyChainItem setObject:username forKey:(__bridge id<NSCopying>)(kSecAttrAccount)];
    
    [keyChainItem setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id<NSCopying>)(kSecReturnData)];
    
    CFDataRef passwordData = NULL;
    
    /*
     Code to add security item to keychain
     */
    
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)(keyChainItem), (CFTypeRef *)&passwordData);
    if (status == noErr) {
        NSLog(@"Keychain item found");
        
        NSString *password = [[NSString alloc] initWithBytes:[(__bridge_transfer NSData *)passwordData bytes]
                                                      length:[(__bridge NSData *)passwordData length] encoding:NSUTF8StringEncoding];
        
        completion1(password);
        NSLog(@"Password from keychain:%@", password);
    }
    else if(status == errSecItemNotFound){
        NSLog(@"Keychain item could not be found");
    }
    else {
        NSLog(@"Some other error occured");
    }
}


#pragma mark -
#pragma mark - UPDATE KEYCAHIN FOR USERNAME -
/*
 - BY DIVYA TO UPDATE PASSWORD IN KEYCHAIN
 */

+(void) updateKeychainForUsername:(NSString *)username Password:(NSString *)password {
    
    NSMutableDictionary *keyChainItem = [[NSMutableDictionary alloc] init];
    
    [keyChainItem setObject:(__bridge id)(kSecClassInternetPassword) forKey:(__bridge id<NSCopying>)(kSecClass)];
    
    [keyChainItem setObject:(__bridge id)(kSecAttrAccessibleWhenUnlocked) forKey:(__bridge id<NSCopying>)(kSecAttrAccessible)];
    
    [keyChainItem setObject:username forKey:(__bridge id<NSCopying>)(kSecAttrAccount)];
    
    
    if (SecItemCopyMatching((__bridge CFDictionaryRef)(keyChainItem), NULL) == noErr) {
        NSLog(@"Item already existe.continue to update");
        
        NSData *passworddata = [password dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *attributeToUpdate = [[NSMutableDictionary alloc] init];
        
        [attributeToUpdate setObject:passworddata forKey:(__bridge id<NSCopying>)(kSecValueData) ];
        OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)(keyChainItem),(__bridge CFDictionaryRef)(attributeToUpdate));
        
        if (status == noErr) {
            NSLog(@"Keychain item found");
        }
        else {
            NSLog(@"Some other error occured");
        }
    } else {
        NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
        [keyChainItem setObject:passwordData forKey:(__bridge id<NSCopying>)(kSecValueData)];
        /*
         
         Code to add security item to keychain
         
         */
        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)(keyChainItem), NULL);
        if (status == noErr) {
            NSLog(@"Item added to your keychain");
        }
        
        else if (status == errSecDuplicateItem){
            NSLog(@"Item Already exists");
        }
        else {
            NSLog(@"Some other error occured");
        }
    }
}

+(void) ifNotExistsKeychainForUsername:(NSString *)username Password:(NSString *)password {
    
    NSMutableDictionary *keyChainItem = [[NSMutableDictionary alloc] init];
    [keyChainItem setObject:(__bridge id)(kSecClassInternetPassword) forKey:(__bridge id<NSCopying>)(kSecClass)];
    [keyChainItem setObject:(__bridge id)(kSecAttrAccessibleWhenUnlocked) forKey:(__bridge id<NSCopying>)(kSecAttrAccessible)];
    [keyChainItem setObject:username forKey:(__bridge id<NSCopying>)(kSecAttrAccount)];
    
    if (SecItemCopyMatching((__bridge CFDictionaryRef)(keyChainItem), NULL) == noErr) {
        NSLog(@"Item already existe.continue to update");
        NSData *passworddata = [password dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *attributeToUpdate = [[NSMutableDictionary alloc] init];
        [attributeToUpdate setObject:passworddata forKey:(__bridge id<NSCopying>)(kSecValueData) ];
        OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)(keyChainItem),(__bridge CFDictionaryRef)(attributeToUpdate));
        if (status == noErr) {
            NSLog(@"Keychain item found");
        }
        else {
            NSLog(@"Some other error occured");
        }
    }
    else {
        /*
         Code to add security item to keychain
         */
        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)(keyChainItem), NULL);
        if (status == noErr) {
            NSLog(@"Item added to your keychain");
        }
        else if (status == errSecDuplicateItem){
            NSLog(@"Item Already exists");
        }
        else {
            NSLog(@"Some other error occured");
        }
    }
    
}
/**
       EcommerceTracking.
     - Developer Abhishek singh
 */
-(void) ecommerceTracking:(NSString *)paymentTypeKeyword TranactionAmount : (NSString *)TrackingAmount TrackId:(NSString *)trackId BookingId:(NSString *)BookingId :(NSString *)strmodel
{
    
    //*******************************Facebook*********************************\\
    
    float amountValuefb = [TrackingAmount floatValue];
    
    [FBSDKAppEvents logPurchase:amountValuefb
                       currency:@"INR"
                     parameters:@{ @"trackId"   : trackId,
                                   @"BookingId" :  BookingId,
                                   @"EventName" :  paymentTypeKeyword
                                   }];
    
    //*******************************GA*********************************\\
    
    NSString *CouponCode;
     NSDecimalNumber *myNumber = [NSDecimalNumber decimalNumberWithString:TrackingAmount];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-64851985-4"];
    
    GAIEcommerceProduct *product = [[GAIEcommerceProduct alloc] init];
    [product setId:trackId];
    [product setName:strmodel];
    [product setPrice:myNumber];
    
    GAIEcommerceProductAction *productAction = [[GAIEcommerceProductAction alloc] init];
    [productAction setAction:kGAIPAPurchase];
    [productAction setTransactionId:BookingId];
    [productAction setRevenue:myNumber];
    [productAction setCouponCode:CouponCode];
    
    //paymentTypeKeyword
    
    /*
     GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:@"MylesTransactionActivity"
     action:@"Purchase"
     label:nil
     value:nil];
     */
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:paymentTypeKeyword
                                                                           action:@"Purchase"
                                                                            label:nil
                                                                            value:nil];
    // Add the transaction data to the event.
    [builder setProductAction:productAction];
    [builder addProduct:product];
    
    [tracker set:kGAIScreenName value:@"Ride Detail Screen"];
    [tracker set:kGAICurrencyCode value:@"INR"];
    
    // Send the transaction data with the event.
    [tracker send:[builder build]];
    
}


+(void)GetCurrentLocation :(myCompletion)compblock
{
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Enable Location"
                                                                 message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                [alert show];
                
                compblock(nil);

            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                CLLocationManager *locationManager;
                locationManager = [[CLLocationManager alloc] init];
                locationManager.delegate = self;
                locationManager.distanceFilter = kCLDistanceFilterNone;
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
                [locationManager startUpdatingLocation];
                CLGeocoder *geocoder = [[CLGeocoder alloc] init];
                
                CLLocation *location = [locationManager location];
                
                
                CLLocationCoordinate2D coordinate = [location coordinate];
                
                if (coordinate.latitude == 0) {
                    compblock(nil);
                } else {
                    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:coordinate.latitude
                                                                        longitude:coordinate.longitude];
                    //assigning global location
                    AppDelegate *appDelegate = [AppDelegate getSharedInstance];
                    appDelegate.currentlocation = newLocation;
                    
                    [geocoder reverseGeocodeLocation:newLocation
                                   completionHandler:^(NSArray *placemarks, NSError *error) {
                                       
                                       if (error) {
                                           NSLog(@"Geocode failed with error: %@", error);
                                           compblock(nil);

                                           return;
                                       }
                                       
                                       if (placemarks && placemarks.count > 0)
                                       {
                                           CLPlacemark *placemark = placemarks[0];
                                           
                                           NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc]init];
                                           [addressDictionary addEntriesFromDictionary:placemark.addressDictionary];
                                           
                                           [addressDictionary setObject:@{@"lat" : [NSString stringWithFormat:@"%f",placemark.location.coordinate.latitude],@"lng" : [NSString stringWithFormat:@"%f",placemark.location.coordinate.longitude]} forKey:@"latlang"];
                                           NSLog(@"%@ ", addressDictionary);
                                           compblock(addressDictionary);
                                           
                                       }
                                       
                                   }];
                }
                
            
            });
        }
        
    }
    
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"didFailWithError: %@", error);
        UIAlertView *errorAlert = [[UIAlertView alloc]
                                   initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];

    });
}

+(void)SavePickUpAddress :(NSDictionary *)dictBodydata PassUrl:(NSString *)Url :(saveAddressComplete)isFinish
{
    NSMutableDictionary *body = [dictBodydata mutableCopy];
    
   // NSString *userid = [DEFAULTS valueForKey: KUSER_PHONENUMBER];
    NSURLSession *session = [CommonFunctions defaultSession];
    NSOperationQueue *globalOperation = [[APIRequestManager sharedInstance] queue];
    APIOperation *registrationOperation = [[APIOperation alloc] initWithBody:body session:session url:Url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                           {
                                               NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                                               NSString *statusStr = [headers objectForKey:@"Status"];
                                               if (statusStr != nil && [statusStr isEqualToString:@"1"])
                                               {
                                                   if (!error)
                                                   {
                                                       NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                       NSLog(@"responseJson %@",responseJson);
                                                       isFinish(responseJson);
                                                   }
                                               }
                                           }];
    [globalOperation addOperation:registrationOperation];
}


+(void)logoutcommonFunction
{
    [DEFAULTS setObject:@"" forKey:K_SessionID];
    [DEFAULTS setObject:@"" forKey:KUSER_PHONENUMBER];
    [DEFAULTS setObject:@"" forKey:K_AccessToken];
    [DEFAULTS setBool:NO forKey:IS_LOGIN];
    [DEFAULTS synchronize];
    
    [self resetDefaults];
    
    
    LoginProcess *lProcess = [[LoginProcess alloc]init];
    [lProcess logoutWithSocialIdWithBSocialType:[DEFAULTS boolForKey:@"socialLoginType"]];
   
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:k_registrationStoryBoard bundle:nil];
//    UINavigationController *registrationNavigation = [storyBoard instantiateViewControllerWithIdentifier:k_registrationNavigation];
//    [AppDelegate getSharedInstance].window.rootViewController = registrationNavigation;
    
  
}

+(void)resetDefaults {
    
     if ([[UIApplication sharedApplication].shortcutItems count] > 0)
     {
         AppDelegate *delegate = [AppDelegate getSharedInstance];
         [delegate configDynamicShortcutItems];
     }
    
    NSString *mylesCenterNumber = [DEFAULTS objectForKey:k_mylesCenterNumber];
    NSString *mylesLastUserLogin = [DEFAULTS objectForKey:KUSER_PHONENUMBER];
    NSString *deviceToken = [DEFAULTS objectForKey:@"devicetoken"];
    
    NSString *roadSideAssistance = [DEFAULTS objectForKey:kRoadsideSupport];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    // adding new values
    if (mylesCenterNumber != nil) {
        [DEFAULTS setValue:mylesCenterNumber forKey:k_mylesCenterNumber];
    }else {
        
    }
    if (roadSideAssistance != nil) {
        [DEFAULTS setValue:roadSideAssistance forKey:kRoadsideSupport];
    } else {
        
    }
    if (deviceToken != nil) {
        [DEFAULTS setValue:deviceToken forKey:@"devicetoken"];
    }else {
        [DEFAULTS setValue:@"" forKey:@"devicetoken"];

    }
    
    if (mylesLastUserLogin != nil) {
        [DEFAULTS setValue:mylesLastUserLogin forKey:KUSER_PHONENUMBER];

    }
    [DEFAULTS synchronize];
    
    [CommonFunctions getAccessTokenWithCompletion:^{
        [[AppDelegate getSharedInstance] getServiceCenterPhone];
        [[AppDelegate getSharedInstance] getRoadsideSupportPhone];
    }];


}

+(void)getAccessTokenWithCompletion:(void (^)(void))completion
{
    if ([CommonFunctions reachabiltyCheck]) {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache" };
        
        NSString *localSessionId = [DEFAULTS objectForKey:K_SessionID];
        localSessionId = [CommonFunctions generateSessionID];
        
        NSDictionary *parameters = @{ @"userID": @"IOS",
                                      @"password": [@"IOS@@MYLESCAR" MD5],
                                      @"IPRestriction": @(FALSE),
                                      @"sessionID": localSessionId,
                                      @"customerID": [DEFAULTS boolForKey:IS_LOGIN] ? [DEFAULTS objectForKey:KUSER_ID]:@"-99"
                                      
                                      };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:accessTokenURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                           timeoutInterval:KTIMEOUT_INTERVAL];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                            completion();
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                [CommonFunctions alertTitle:KATimeOut withMessage:KATryLater];
                                                            });
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableDictionary *jsonreponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                            
                                                            NSString *tokenString = [jsonreponse objectForKey:@"accessToken"];
                                                            NSLog(@"string = %@",tokenString);
                                                            
                                                            if (tokenString != nil)
                                                            {
                                                                NSLog(@"accessToken = %@",tokenString);
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [DEFAULTS setObject:tokenString forKey:K_AccessToken];
                                                                    [DEFAULTS setObject:localSessionId forKey:K_SessionID];
                                                                    [DEFAULTS synchronize];
                                                                    
                                                                    if (completion) {
                                                                        completion();
                                                                    }
                                                                });
                                                            } else {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [DEFAULTS setObject:@"" forKey:K_AccessToken];
                                                                    [DEFAULTS setObject:@"" forKey:K_SessionID];
                                                                    [DEFAULTS synchronize];
                                                                    
                                                                    if (completion) {
                                                                        completion();
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }];
        [dataTask resume];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion();
            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        });
    }
    
}

+(NSString *)generateSessionID {
    int randomID = arc4random() % 9000 + 1000;
    NSString *timestampWithRandomNumber = [NSString stringWithFormat:@"%@%d",TimeStamp,randomID];
    NSString *sessionID = [NSString stringWithFormat:@"sess_%@_ios",timestampWithRandomNumber];
    NSLog(@"sessionid = %@",sessionID);
    return sessionID;
}

+(NSString *)convertDate : (NSString *)strDate
{
    //    NSString *dateString = @"11-13-2013";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:strDate];
    
    // Convert date object into desired format
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *newDateString = [dateFormatter stringFromDate:date];
    return newDateString;
}

/**
 @brief Convert nsdate to string
 @dev Abhishek singh
 */
+(NSString *)IndianTimeConverter : (NSDate *)date
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
    dateFormatter1.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [dateFormatter1 stringFromDate:date];
    return strDate;
}


+ (NSString *)getCurrentDate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    return dateString;
}

+(NSTimeInterval )getMilliSecond{
    NSTimeInterval millisecondedDate = ([[NSDate date] timeIntervalSince1970] * 1000);
   // NSString *intervalString = [NSString stringWithFormat:@"%.f", milisecondedDate];
    return millisecondedDate;
}

+(NSString* )finalMilliSecond: (NSTimeInterval )previousData current:(NSTimeInterval )updatedData{
    
    NSInteger previous = (long)previousData;
    NSInteger updated = (long)updatedData;
    
    NSInteger totalMiliSec;
    totalMiliSec = updated - previous;
    NSString *updatedMilliSec = [NSString stringWithFormat:@"%ld", totalMiliSec];
    return updatedMilliSec;
}
+(NSString *)createStrigForSavingData :(NSString *)fromScreen key:(NSString*)keyVaue startTime:(NSString *)startTime endTime:(NSString*)endTime milliSec:(NSString *)milliSecond{
    NSString * totalString = [fromScreen stringByAppendingString:@" | "];
    if (startTime.length > 0){
        if (endTime.length > 0){
            totalString = [[[[[[[totalString stringByAppendingString:keyVaue] stringByAppendingString:@" | "] stringByAppendingString:startTime] stringByAppendingString:@" | "] stringByAppendingString:endTime] stringByAppendingString:@" | "] stringByAppendingString:milliSecond];
            [self writeToLogFile:totalString];
        }
    }
   
    return totalString;
}

+(void) writeToLogFile:(NSString*)content{
    content = [NSString stringWithFormat:@"%@\n",content];
    
    //get the documents directory:
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:@"TimeLog.txt"];
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:fileName];
    if (fileHandle){
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
        [fileHandle closeFile];
    }
    else{
        [content writeToFile:fileName
                  atomically:NO
                    encoding:NSStringEncodingConversionAllowLossy
                       error:nil];
    }
}
@end
