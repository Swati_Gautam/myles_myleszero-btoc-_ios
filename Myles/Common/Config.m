//
//  Config.M
//  Myles
//
//  Created by Myles on 23/01/16.
//  Copyright (c) 2015 Divya. All rights reserved.
//

#import "Config.h"

@implementation Config

//TEST URL
//http://103.11.84.248:9091/services/mylesService.svc/


//Live URL:
//http://mapi.mylescars.com/Services/MylesService.svc/
//NSString		*SiteAPIURL				= @"http://mapi.mylescars.com/Services/MylesService.svc/";

//site url for android
//NSString		*SiteAPIURL				= @"http://119.226.59.187:8083/Services/MylesService.svc/";

//NSString		*SiteAPIURL				= @"http://10.90.90.32:8083/Services/MylesService.svc/";

//stagingmapiv2.mylescars.com
#ifdef DEBUG
//change to staging build
//NSString        *SiteAPIURL                = @"https://stagingmapiv2.mylescars.com/Services/MylesService.svc/";

NSString        *SiteAPIURL                = @"https://mobileappservice.carzonrent.com/Services/MylesService.svc/";
#else
NSString        *SiteAPIURL                = @"https://mapiv2.mylescars.com/Services/MylesService.svc/";
#endif
//#ifdef DEBUG
//NSString        *SiteAPIURL                = @"https://stagingmapiv2.mylescars.com/Services/MylesService.svc/";
//#else
//NSString        *SiteAPIURL                = @"https://mapiv2.mylescars.com/Services/MylesService.svc/";
//#endif

//temp site url for iOS
//http://119.226.59.187:8085/

//NSString		*SiteAPIURL				= @"http://119.226.59.187:8085/Services/MylesService.svc/";

NSString		*AboutUsURL				= @"http://www.mylescars.com/mobiles/about_us";
NSString		*ConatctUsURL				= @"http://www.mylescars.com/mobiles/contact_us";

#ifdef DEBUG
//change to staging build
//stagingmylesaccesstoken.carzonrent.com


//------------------------------------------------------------------------------------//

// after 28th aug 2018
//NSString        *accessTokenURL         = @"https://stagingmylestoken.mylescars.com/Services/MylesTokenService.svc/V1_RestW1AuthenticateUser";
//NSString *registerCutomerIDURL          = @"https://stagingmylestoken.mylescars.com/Services/MylesTokenService.svc/RestW1UpdateCustomerIDAgainstKey";

//-- before 28th aug 2018
//NSString        *accessTokenURL         = @"https://stagingmylesaccesstoken.carzonrent.com/Services/MylesTokenService.svc/V1_RestW1AuthenticateUser";
//NSString *registerCutomerIDURL          = @"https://stagingmylesaccesstoken.carzonrent.com/Services/MylesTokenService.svc/RestW1UpdateCustomerIDAgainstKey";

//---------------------------------------------------------------------------//

NSString        *accessTokenURL         = @"https://mylesaccesstoken.carzonrent.com/Services/MylesTokenService.svc/V1_RestW1AuthenticateUser";
NSString *registerCutomerIDURL          = @"https://mylesaccesstoken.carzonrent.com/Services/MylesTokenService.svc/RestW1UpdateCustomerIDAgainstKey";
#else
NSString        *accessTokenURL         = @"https://oauth.mylescars.com/Services/MylesTokenService.svc/V1_RestW1AuthenticateUser";
NSString *registerCutomerIDURL          = @"https://oauth.mylescars.com/Services/MylesTokenService.svc/RestW1UpdateCustomerIDAgainstKey";
#endif
//
//for current location
CLLocationManager *locationManager;							//
CLLocation *currentLocation;								// For Latitude & Longitude
CLLocationCoordinate2D coordinates;							//

float			core_latitude;
float			core_longitude;

NSString		*deviceToken = @"";

NSInteger kPendingVoucherCount = 0;

//NSString          *KUSER_REGISTRATION =    @"Rest_App_V2_UserSecureRegistration";

NSString          *KUSER_REGISTRATION =    @"Rest_App_V2_UserSecureRegistration_new";


NSString          *KPHONE_NUMBER_VERIFICATION = @"phoneNumberVerification" ;
NSString          *KRESEND_CODE = @"resendVerificationcode" ;
NSString          *KUSER_LOGIN = @"Rest_App_V2_UserSecureLogin";
NSString          *KCHANGE_PASSWORD = @"V1_Rest_App_V1_ChangeUserPassword";//New RestM1NewchangePassword
NSString          *KFORGOT_PASSWORD = @"V1_forgotPassword";
NSString          *KCITY_LIST = @"V1_fetchCities";
NSString          *KSUBLOCATION_LIST = @"V1_fetchSublocations";
NSString           *KUSERINFO_FATCH   = @"V1_fetchProfileDetailsV2";
NSString           *KUSERINFO_UPDATE   = @"V2_updateProfileDetails";
NSString           *KGETPACKAGE_CITYWISE   = @"V1_packageDetailsCitywise";
NSString           *KActivePkgType   = @"V1_ActivePkgType";
NSString           *KgetCarDetails     = @"V1_MylesCarDetailsV1";
NSString           *kAdditionalServices = @"V1_getAdditionalServicesv2";
NSString           *kgetPaymentOptions = @"GetCarBookingPaymentOptions";
NSString           *kCreateBooking = @"V1_createBookingfromApp";
NSString           *kapplyDiscount = @"V1_applyDiscountNewV2";
NSString           *kgetDiscount   = @"FetchApplicablePromoCoupons";
NSString           *KbookingHistory   = @"V1_bookingHistory";
NSString           *KcancelBooking   = @"V1_cancelBooking";
NSString           *KuploadDocument   = @"V1_RestM1uploadDocument";
//@"V1_uploadDocument";
//NSString           *kPanCardUpload = @"V1_RestM1UploadDocumentPanCard";
NSString           *KpreAuthCreation   = @"V1_preAuthCreation";
NSString           *KupdateCheckList  = @"V1_updateCheckList";
NSString           *KgetNextAvailability  = @"V1_getNextAvailability";
NSString           *KdeleteFile   = @"V1_deleteFile";
NSString           *KdeviceUpdateOnLogout   = @"V1_deviceUpdateOnLogout";
NSString           *KmylesCenterNo   = @"V1_mylesCenterNo";
NSString           *KcustomerRating   = @"V1_customerRating";
NSString           *KcheckLockUnlock   = @"V1_checkLockUnlock";
NSString           *KlockUnlock   = @"V1_lockUnlock";
NSString           *KgetChecklist   = @"V1_getChecklist";
NSString           *KgetActiveBookings   = @"V1_getActiveBookings";
NSString            *kBookingTermsAndConditions = @"getPaymentTerms";
//NSString            *kAgreementAcceptance = @"postTermsResponse";
NSMutableDictionary *kActiveBookingData  ;

NSString            *kRoadsideSupport = @"V1_roadsideSupport";
NSString            *kShareApi        = @"V1_socialSharingForIOS";
NSString            *kFetchAddress    =@"V1_RestM1FetchCustomerAddress";
NSString            *kEditAddress    = @"V1_RestM1deleteAddress";
NSString            *kMylesCarDetailsV1    = @"V1_AvailableCarsForSearch";
//@"V2_MylesCarDetailsWithGST";
//@"V1_MylesCarDetailsV1";
//NSString            *kMylesCarDetailV2GST = @"V2_MylesCarDetailsWithGST";
NSString            *kMylesCarDetailsWithHomePickUpAndDropOff = @"V1_AvailableCarsForSearchOnHomePickup";
//@"V2_MylesCarDetailsHomePickUpWithGST";
//@"V1_MylesCarDetailsHomePickUp";
//NSString            *kMylesCarDetailWithHomePickUpAndDropOffGST = @"V2_MylesCarDetailsHomePickUpWithGST";
//@"V1_MylesCarDetailsHomePickUp";//
NSString            *kRestM1FetchAppVersions    = @"V1_RestM1FetchAppVersions";
NSString            *kUserDetailsBeforeBooking    = @"V1_userDetailsBeforeBookingV1";
NSString            *kGetSublocationAirportCharges    = @"V1_GetSublocationAirportCharges";



NSInteger          personRecordId;
NSString            *sharingText;
NSInteger          KTIMEOUT_INTERVAL  = 30;
NSString           *kMerchantId       = @"T5163";//@"T5163";//
NSString           *kSchemeId         = @"CARZONMOB";//@"CARZONMOB";//
NSString           *insertAddress     = @"V1_InsertCustomerAddress";
//New ocject for login deferment
NSString            *kRestM1verifyMobileNo    = @"RestM1verifyMobileNo";
NSString            *KRestM1sendOTPonForgot    = @"RestM1sendOTPonForgot";
NSString            *kRestM1ResetPassword      = @"RestM2ResetPassword";



@end
