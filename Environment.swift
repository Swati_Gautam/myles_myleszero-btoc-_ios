//
//  Environment.swift
//  Myles Zero
//
//  Created by skpissay on 08/08/19.
//  Copyright © 2019 skpissay. All rights reserved.
//

import Foundation


public enum PlistKey {
    case TargetName
    case ServerURL
    case TimeoutInterval
    case ConnectionProtocol
    
    func value() -> String {
        switch self {
        case .TargetName:
            return "target_name"
        case .ServerURL:
            return "server_url"
        case .TimeoutInterval:
            return "timeout_interval"
        case .ConnectionProtocol:
            return "protocol"
        }
    }
}
public struct Environment {
    
    fileprivate var infoDict: [String: Any]  {
        get {
            if let dict = Bundle.main.infoDictionary {
                return dict
            }else {
                fatalError("Plist file not found")
            }
        }
    }
    public func configuration(_ key: PlistKey) -> String {
        switch key {
        case .TargetName:
            return infoDict[PlistKey.TargetName.value()] as! String
        case .ServerURL:
            return infoDict[PlistKey.ServerURL.value()] as! String
        case .TimeoutInterval:
            return infoDict[PlistKey.TimeoutInterval.value()] as! String
        case .ConnectionProtocol:
            return infoDict[PlistKey.ConnectionProtocol.value()] as! String
        }
    }
}
