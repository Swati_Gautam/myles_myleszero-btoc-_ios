//
//  PickUpAddressList.m
//  Myles
//
//  Created by Itteam2 on 30/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "PickUpAddressList.h"
//#import "PickUpAddressCell.h"
#import "PickupAddressVC.h"
#import "AddHomePickUpAddress.h"
#import "Config.h"

@interface PickUpAddressList ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation PickUpAddressList
{
    NSMutableArray *maAddressList;
}

-(void)showLoader {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setFont:UIFONT_REGULAR_Motor(12)];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor whiteColor] colorWithAlphaComponent:0.4]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    [SVProgressHUD showWithStatus:@"Please wait"];
}

-(void)viewWillAppear:(BOOL)animated
{
//    if (_bReload) {
//        
//        _bReload = false;
//    }
   

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self CallApi:kFetchAddress:@{@"PhoneNo":[DEFAULTS objectForKey:KUSER_PHONENUMBER],@"userId":[DEFAULTS objectForKey:KUSER_ID]} :0];
    // Do any additional setup after loading the view.

    [self customizeLeftRightNavigationBar:@"Address List" RightTItle:nil Controller:self Present:false];
    self.leftbackButton.hidden = false;
    self.leftmenuButton.hidden = true;
    [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    
   
}
-(void)CallApi:(NSString *)strUrl :(NSDictionary *)dictBody :(int)index
{
    if ([CommonFunctions reachabiltyCheck]) {
        [self showLoader];
   
    [CommonFunctions SavePickUpAddress:dictBody PassUrl:strUrl :^(NSDictionary *DictSaveAddress) {
        NSLog(@"DictSaveAddress %@",DictSaveAddress);
        if (maAddressList == nil) {
            maAddressList = [[NSMutableArray alloc]init];
        }
       
       
        if (![strUrl isEqualToString:kEditAddress]) {
             [maAddressList removeAllObjects];
                [maAddressList addObjectsFromArray:DictSaveAddress != nil ? DictSaveAddress[@"response"] != nil ? DictSaveAddress[@"response"] : @[] : @[]];
            
        
            
        }
        else
        {
            [maAddressList removeObjectAtIndex:index];
            if (maAddressList.count==0) {
                [DEFAULTS removeObjectForKey:k_PickupAddress];
            }

        }
      
        dispatch_async(dispatch_get_main_queue(), ^{
            if (maAddressList.count == 0 && ![strUrl isEqualToString:kEditAddress]) {
                
                    [self btnAddAddress:self];
            }
            else
                [_tblPickUpAddress reloadData];
                
           // RestM1deleteAddress
            [SVProgressHUD dismiss];
            
        });
        
    }];
         }
    else
    {
         [CommonFunctions alertTitle:nil withMessage:KAInternet];
        [SVProgressHUD dismiss];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return maAddressList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    PickUpAddressCell *Cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
//    if(Cell == nil)
//    {
//        Cell = [[PickUpAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
//    }
//    return Cell;
    UITableViewCell *cell;
   // [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                           //reuseIdentifier:k_searchTableCellIdentifier]
    cell =  [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                   reuseIdentifier:k_searchTableCellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:k_searchTableCellIdentifier];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"Address %ld",indexPath.row+1];
    NSString *strTemp = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",maAddressList[indexPath.row][@"Housenumber"] != nil ? maAddressList[indexPath.row][@"Housenumber"]:@"",maAddressList[indexPath.row][@"Locality"] != nil ? maAddressList[indexPath.row][@"Locality"]:@"",maAddressList[indexPath.row][@"Street"] != nil ? maAddressList[indexPath.row][@"Street"]:@"",maAddressList[indexPath.row][@"Landmark"] != nil ? maAddressList[indexPath.row][@"Landmark"]:@"",maAddressList[indexPath.row][@"City"] != nil ? maAddressList[indexPath.row][@"City"]:@""];
    
    cell.detailTextLabel.text = strTemp;
    
    cell.detailTextLabel.numberOfLines = 0;
    [cell.detailTextLabel sizeToFit];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * footerView;
    footerView = [[UIView alloc]initWithFrame:CGRectZero];
    footerView.backgroundColor=[UIColor clearColor];
    UIButton *btnAddAddress = [[UIButton alloc]initWithFrame:CGRectMake(0, 5, _tblPickUpAddress.frame.size.width,  50)];
    [btnAddAddress setImage:[UIImage imageNamed:@"more_less_icn"] forState:UIControlStateNormal];
    btnAddAddress.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [btnAddAddress setTitle:@"Add Address" forState:UIControlStateNormal];
    btnAddAddress.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);

    
    [btnAddAddress setTitleColor:[UIColor colorWithRed:223.0/255.0 green:70.0/255.0 blue:39.0/255.0 alpha:1] forState:UIControlStateNormal];
    btnAddAddress.tag = 100;
    [btnAddAddress addTarget:self action:@selector(btnAddAddress:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:btnAddAddress];
    return footerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [DEFAULTS setObject:maAddressList[indexPath.row] forKey:k_PickupAddress];
    [self.navigationController popViewControllerAnimated:TRUE];

}
-(IBAction)btnAddAddress:(id)sender
{
    if (maAddressList.count == 3) {
        [CommonFunctions alertTitle:nil withMessage:@"You can not add more than 3 address."];
    }
    else
    {
        AddHomePickUpAddress *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
        
        vc.delegate = self;
        [[self navigationController] pushViewController:vc animated:YES];
    }
}

#pragma mark- Right Button
- (void)rightButton : (UIButton *)sender
{
    if ([DEFAULTS valueForKey:k_mylesCenterNumber])
    {
        NSString *url = [NSString stringWithFormat:@"tel://%@", [DEFAULTS valueForKey:k_mylesCenterNumber]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        //[CommonFunctions callPhone:[DEFAULTS valueForKey:k_mylesCenterNumber]];
    }
    else
    {
        [CommonFunctions alertTitle:KSorry withMessage:kcallingServiceUnavailble];
    }
}
#define FONT_SIZE 16.0f
#define CELL_CONTENT_MARGIN 28.0f

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *strData =maAddress[indexPath.row][@"formatted_address"];
//    
//    
//    CGSize constraint = CGSizeMake(250 - 66, 20000.0f);
//    CGSize size = [strData sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
//    CGFloat height = MAX(size.height, 66.0);
//    return height;
//    return 0;
//}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Nothing is needed here
//}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *button2 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        AddHomePickUpAddress *vc = [self.storyboard instantiateViewControllerWithIdentifier:k_pickUpAddressIdentifier];
        
           vc.dictEditData = maAddressList[indexPath.row];
           vc.delegate = self;
           [[self navigationController] pushViewController:vc animated:YES];
    }];
    UITableViewRowAction *button3 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Are you sure you want to delete this \"Address\""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"DELETE"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                [self CallApi:kEditAddress:@{@"indexid":maAddressList[indexPath.row][@"AddressId"],@"userID":[DEFAULTS objectForKey:KUSER_ID]} :indexPath.row];

                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"CANCEL"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }];
    
    button2.backgroundColor = [UIColor colorWithRed:184.0/255.0 green:196.0/255.0 blue:210.0/255 alpha:1];
    button3.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:60.0/255.0 blue:47.0/255 alpha:1];

    return @[button3,button2];

}
-(void)ReloadAddress
{
//    _bReload = true;
     [self CallApi:kFetchAddress:@{@"PhoneNo":[DEFAULTS objectForKey:KUSER_PHONENUMBER],@"userId":[DEFAULTS objectForKey:KUSER_ID]} :0];
}
@end
