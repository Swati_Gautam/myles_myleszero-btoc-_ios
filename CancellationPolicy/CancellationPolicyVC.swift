//
//  CancellationPolicyVC.swift
//  Myles
//
//  Created by Abhishek on 3/2/17.
//  Copyright © 2017 Divya Rai. All rights reserved.
//

import UIKit
import SVProgressHUD
import FirebaseAnalytics

class CancellationPolicyVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @objc var strBookingID = String()
    @objc var strSelectedReg = ""
    @objc var reason = ""

    var objCancelResponse : cancelResponseStatusModel?
    
    @IBOutlet weak var btnCancelBooking: UIButton!
    @IBOutlet weak var tblCancellationList: UITableView!
   // var isSelected : Bool = false
    enum CancellationList:String {
        case UnderAge  = "13"
        case PlanCancelled  = "10"
        case DocumentsNotAvailable  = "11"
        case BookingModification  = "9"
        case SecurityDepositNotAvailable  = "12"
        case Others  = "15"


       }
    
     let listOfReason = ["Under Age","Plan cancelled","Documents not available","Booking modification","Security deposit not available","Others"]
    
    let refundDetails = ["Cancellation charges","Processing fees","Refund amount"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cancelEvent(strUrl: "RestW1PreCancellationRequest") { (notReq) in
            
        }

        
        //self.customizeNavigationBar("Cancel Booking", present: false)
        //self.navigationItem.setHidesBackButton(true, animated: false)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tblCancellationList.register(UITableViewCell.self, forCellReuseIdentifier: "CELL")
        
        tblCancellationList.register(UINib(nibName: "CellHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "CellHeader")


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func showLoader () {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
    
   
    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
//        return !isSelected ? 1 : 2
        return 2
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return section == 1 ?  refundDetails.count : listOfReason.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        cell = UITableViewCell(style: indexPath.section == 1 ? UITableViewCell.CellStyle.value1 : UITableViewCell.CellStyle.default, reuseIdentifier: "CELL")
        cell.selectionStyle = .blue

        // Configure the cell...
        cell.textLabel?.text = indexPath.section == 1 ? refundDetails[indexPath.row] : listOfReason[indexPath.row]
        
        if indexPath.section == 1
        {
            cell.isUserInteractionEnabled = false
        }
        
        if objCancelResponse != nil && objCancelResponse?.responseDict != nil && indexPath.section == 1
        {
            cell.selectionStyle = .none
            switch indexPath.row {
            case 0:
               // cell.accessoryType = .detailButton
                 cell.detailTextLabel?.text = objCancelResponse?.responseDict?.deductionAmount
                break
            case 1:
                cell.detailTextLabel?.text = objCancelResponse?.responseDict?.processingFee
                break
            case 2:
                cell.detailTextLabel?.text = objCancelResponse?.responseDict?.refundableAmount
                break
            default: break
                
            }

        }
        else
        {
            cell.detailTextLabel?.text = "Calculating"

        }
        return cell
    }
    
   
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
        guard indexPath.section == 0 else
        {
            return
        }
        
//        guard listOfReason.count != indexPath.row  else
//        {
////            tblCancellationList.deselectRow(at: indexPath, animated: true)
////            strSelectedReg = ""
////            let alertController = UIAlertController(title: "Myles", message: "Please call Myles customer representative to cancel your booking", preferredStyle: .alert)
////            
////            let CallAction = UIAlertAction(title: "Call", style: .default, handler: {
////                call in
////                let url = "tel://08882222222"
////                UIApplication.shared.openURL(NSURL(string: url)! as URL)
////            })
////            
////            alertController.addAction(CallAction)
////            
////            let defaultAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
////            alertController.addAction(defaultAction)
////            
////            self.present(alertController, animated: true, completion: nil)
//            return
//        }
        

        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.3, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
        },completion: { finished in
            UIView.animate(withDuration: 0.1, animations: {
                cell.layer.transform = CATransform3DMakeScale(1,1,1)
            })
        })

        
        reason = listOfReason[indexPath.row]

        switch indexPath.row {
        case 0:
          strSelectedReg = CancellationList.UnderAge.rawValue
            break
        case 1:
            strSelectedReg = CancellationList.PlanCancelled.rawValue
            break
        case 2:
            strSelectedReg = CancellationList.DocumentsNotAvailable.rawValue

            break
        case 3:
            strSelectedReg = CancellationList.BookingModification.rawValue

            break
        case 4:
            strSelectedReg = CancellationList.SecurityDepositNotAvailable.rawValue
            break
        case 5:
            strSelectedReg = CancellationList.Others.rawValue
            break
        default:
           
            break
        }
        let bottomOffset = CGPoint(x: 0, y: tblCancellationList.contentSize.height - tblCancellationList.bounds.size.height)
       // tblCancellationList.setContentOffset(bottomOffset, animated: true)

    }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44  // or whatever
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {

        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellHeader") as! CellHeader
            headerView.lblHeader.text = section == 1 ?  "Refund Details" : "Choose a reason"
            headerView.btnInformation.isHidden = section == 1 ? false : true
        headerView.btnInformation.didTouchUpInside = {(sender) in

            
           // self.strSelectedReg = ""
           // self.reason = ""
            let alertController = UIAlertController(title: "Myles", message: "For any further query, please contact us", preferredStyle: .alert)
            
            let CallAction = UIAlertAction(title: "Call", style: .default, handler: {
                call in
                let url = "tel://08882222222"
                UIApplication.shared.openURL(NSURL(string: url)! as URL)
            })
            
            alertController.addAction(CallAction)
            
            let defaultAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)

            
        }
        return headerView

    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return  section == 1 ?  "Refund Details" : "Choose a reason"
//    }
    
    @IBAction func cancelBtn(_ sender: Any)
    {
        guard strSelectedReg.characters.count > 0 else {
            let alertController = UIAlertController(title: "Myles", message: "Choose a reason", preferredStyle: .alert)
            
            let CallAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(CallAction)
            
            self.present(alertController, animated: true, completion: nil)

            return
        }
        
        cancelEvent(strUrl: "RestW1PostCancellationRequest") { (isSucess) in
            
            if isSucess
            {
                FIRAnalytics.logEvent(withName: "booking_cancelled", parameters: ["Reason" : self.reason as NSObject])

                DispatchQueue.main.async {
                    
                    let alertController = UIAlertController(title: "Myles", message: self.objCancelResponse?.message, preferredStyle: .alert)
                    
                    let CallAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                        self.navigationController?.popToRootViewController(animated: true)

                        
                    })
                    alertController.addAction(CallAction)
                    
                    self.present(alertController, animated: true, completion: nil)


                }
            }
        }
    }
    
     func cancelEvent(strUrl: Any,completionHandler: @escaping (Bool) ->Void)
    {
        
        showLoader()
        let userID = UserDefaults.standard.object(forKey: KUSER_ID)
//
        let objCancelMOdel = CancelMOdel(bookingIDT: self.strBookingID, userIDT: userID as! String, cancelReasonIDT: self.strSelectedReg, remarksT: "").dict
        print(objCancelMOdel)
       let apiClass = ApiClass()
        apiClass.CancellationPolicyAPI(dict: objCancelMOdel, strAPIName: strUrl as! String)
           { (dictResponse, isToken) in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                guard dictResponse != nil else
                {
                    let alertController = UIAlertController(title: "Myles", message: "Server not responding", preferredStyle: .alert)
                    let CallAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                        self.navigationController?.popToRootViewController(animated: true)
                        
                        
                    })
                   // let CallAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(CallAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    completionHandler(false)
                    return
                }

            }
            
            
            guard isToken else
            {
               
                return
            }
            
            print(dictResponse ?? "nil")
            DispatchQueue.main.async
                {
                    
                self.objCancelResponse = cancelResponseStatusModel(dict: dictResponse! )
                    
                 //   strSelectedReg.characters.count == 0
                    guard self.objCancelResponse?.status == 1 else
                    {
                        let alertController = UIAlertController(title: "Myles", message: self.objCancelResponse?.message ?? "", preferredStyle: .alert)
                        
                        let CallAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(CallAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        completionHandler(false)
                        return
                    }
                    
                    guard self.strSelectedReg.characters.count == 0 else
                    {
                        completionHandler(true)
                       return
                    }
                    
                    
                    self.tblCancellationList.reloadData()

                   
               }
           
            
            
           }
           
        
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let vwFooter = UIView(frame: cgrec)
//        
//    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class CancelMOdel
{
//    string BookingID, string userID, string CancelReasonID, string Remarks
//    var bookingID : String = ""
//    var userID : String = ""
//    var cancelReasonID : String = ""
//    var remarks : String = ""
      var dict = [String:String]()

    
    init(bookingIDT:String,userIDT:String,cancelReasonIDT:String,remarksT:String)
    {
//        bookingID = bookingIDT
//        userID = userIDT
//        cancelReasonID = cancelReasonIDT
//        remarks = remarksT
        dict["BookingID"] = bookingIDT
        dict["userID"] = userIDT
        dict["CancelReasonID"] = cancelReasonIDT
        dict["Remarks"] = remarksT
    }
    
//    func CancelReqBody() -> Dictionary<String,String>
//    {
//        var dict = [String:String]()
//            dict["BookingID"] = bookingID
//            dict["userID"] = userID
//            dict["CancelReasonID"] = cancelReasonID
//            dict["Remarks"] = remarks
//
//        
//        return dict
//        
//    }

}

class cancelResponseStatusModel
{
    var message : String = ""
    var responseDict : cancelResponseModel?
    var status : Int
    
    init(dict:Dictionary<String,Any>)
    {
        message = dict["message"] as! String? ?? "null"
       
        status = dict["status"]! as! Int
        
        if status == 1
        {
         responseDict = cancelResponseModel(dict: dict["response"] as! Dictionary<String, String>)
        }

    }
    
}

class CellHeader: UITableViewHeaderFooterView {
    @IBOutlet var btnInformation : Button!

    @IBOutlet weak var lblHeader: UILabel!
    
}

class cancelResponseModel
{
        var deductionAmount : String = ""
        var processingFee : String = ""
        var refundableAmount : String = ""
    
    init(dict:Dictionary<String,String>)
    {
        
        deductionAmount = dict["DeductionAmount"] ?? "null"
        processingFee = dict["ProcessingFee"] ?? "null"
        refundableAmount = dict["RefundableAmount"] ?? "null"

    }
}
