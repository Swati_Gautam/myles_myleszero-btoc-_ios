//
//  PickUpAddress.m
//  Myles
//
//  Created by Itteam2 on 24/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import "PickUpAddress.h"
@import GooglePlaces;
@import GoogleMaps;
@import GooglePlacePicker;
#import <CoreLocation/CoreLocation.h>


@interface PickUpAddress () <GMSAutocompleteViewControllerDelegate>
typedef void(^myCompletionBlock)(NSMutableArray *arrData);
#define GoogleWedAPI @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"
#define GoogleAPIKey @"AIzaSyBH6Yh2cC38KsdJV0sTLsZQH7P990jpbqc"
#define GooglePlacesDetailsWedAPI @"https://maps.googleapis.com/maps/api/place/details/json?"
#define GeoCodeApi @"https://maps.googleapis.com/maps/api/geocode/json?address="
@end

@implementation PickUpAddress {
    GMSPlacesClient *_placesClient;
    //GMSPlacePicker *_placePicker;  //Swati Commented
    CLLocationManager *locationManager;
    NSMutableArray *maAddress;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [locationManager requestWhenInUseAuthorization];
//    [locationManager requestAlwaysAuthorization];
//    _placesClient = [GMSPlacesClient sharedClient];
//    locationManager = [[CLLocationManager alloc] init];
//  //  locationManager.delegate = self;
//    locationManager.distanceFilter = kCLDistanceFilterNone;
//    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
//    [locationManager startUpdatingLocation];
//    self.navigationController.navigationBar.translucent = NO;
    // Do any additional setup after loading the view.
    
    [self customizeLeftRightNavigationBar:@"Search Address" RightTItle:nil Controller:self Present:false];
    self.leftbackButton.hidden = false;
    self.leftmenuButton.hidden = true;
    [self.view removeGestureRecognizer: self.revealViewController.panGestureRecognizer];
    
    maAddress = [[NSMutableArray alloc]init];
}
//- (IBAction)getCurrentPlace:(UIButton *)sender {
//    [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
//        if (error != nil) {
//            NSLog(@"Pick Place error %@", [error localizedDescription]);
//            return;
//        }
//        
//        
//        
//        if (placeLikelihoodList != nil) {
//            GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
//            if (place != nil) {
//                NSLog(@"PALCE NAME -- %@",place.name);
//                NSLog(@"Place Address -- %@",[[place.formattedAddress componentsSeparatedByString:@", "]
//                                              componentsJoinedByString:@"\n"]);
//                ;
//            }
//        }
//    }];
//}
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    NSLog(@"%@", [locations lastObject]);
//}
//- (IBAction)pickPlace:(UIButton *)sender {
//    
//    CLLocation *location = [locationManager location];
//    
//    
//    CLLocationCoordinate2D coordinate = [location coordinate];
//    
//    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
//    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
//                                                                  center.longitude + 0.001);
//    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
//                                                                  center.longitude - 0.001);
//    GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
//                                                                         coordinate:southWest];
//
//
//    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
//    filter.type = kGMSPlacesAutocompleteTypeFilterRegion;
//    [_placesClient autocompleteQuery:@"b-223 sector 55 noida" bounds:viewport filter:nil callback:^(NSArray<GMSAutocompletePrediction *> * _Nullable results, NSError * _Nullable error) {
//        for (GMSAutocompletePrediction* result in results) {
//            NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
//        }
//    }];
//    CLLocation *location = [locationManager location];
//    
//    
//    CLLocationCoordinate2D coordinate = [location coordinate];
//    
//    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
//    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
//                                                                  center.longitude + 0.001);
//    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
//                                                                  center.longitude - 0.001);
//    GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
//                                                                         coordinate:southWest];
//    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
//    _placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
//
//    [_placePicker pickPlaceWithCallback:^(GMSPlace *place, NSError *error) {
//        if (error != nil) {
//            NSLog(@"Pick Place error %@", [error localizedDescription]);
//            return;
//        }
//        
//        if (place != nil) {
//            NSLog(@"PALCE NAME -- %@",place.name);
//            NSLog(@"Place Address -- %@",[[place.formattedAddress
//                                           componentsSeparatedByString:@", "] componentsJoinedByString:@"\n"]);
//        } else {
//           
//             NSLog(@"No place selected");
//            
//        }
//    }];
//}
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    NSLog(@"didUpdateToLocation: %@", newLocation);
//    CLLocation *currentLocation = newLocation;
//    
//    if (currentLocation != nil) {
//       [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
//        [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
//    }
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getPlacesToWebServices :(NSString *)strSearchTxt :(myCompletionBlock) compblock
{

    NSMutableArray *tempArrPlaces=[[NSMutableArray alloc]init];
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        NSString *strUrl=[NSString stringWithFormat:@"%@\(%@ %@)&components=country:IN",GeoCodeApi,strSearchTxt,[DEFAULTS objectForKey:K_LastLocation] != nil ? [DEFAULTS objectForKey:K_LastLocation]:@""];
        NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
        NSURL *url=[NSURL URLWithString:[strUrl stringByAddingPercentEncodingWithAllowedCharacters:set]];
        
        NSMutableData *data = [NSMutableData dataWithContentsOfURL:url];
        if (data!=nil) {
            
            
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0       error:nil];
            
            if ([responseDict[@"status"] isEqualToString:@"ZERO_RESULTS"] && [responseDict[@"results"] count]==0)
            {
                [tempArrPlaces removeAllObjects];
            }
            else
            {
                NSLog(@"last location %@",[DEFAULTS objectForKey:K_LastLocation] );
                for (NSDictionary *addLocation in responseDict[@"results"]) {
                    if ([[addLocation[@"formatted_address"] lowercaseString] rangeOfString:[[DEFAULTS objectForKey:K_LastLocation] != nil ? [DEFAULTS objectForKey:K_LastLocation]:@"" lowercaseString]].location != NSNotFound) {
                    [tempArrPlaces addObject:addLocation];
                    }
                }
                


                }
        }
        else
        {
            
            
            [tempArrPlaces removeAllObjects];
            NSLog(@"Please check internet connection");
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            NSLog(@"responseDict %@",tempArrPlaces);

            compblock(tempArrPlaces);
        }];
    }];


}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (IBAction)onLaunchClicked:(id)sender {
//    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
//    acController.delegate = self;
//    [self presentViewController:acController animated:YES completion:nil];
//}
//
//// Handle the user's selection.
//- (void)viewController:(GMSAutocompleteViewController *)viewController
//didAutocompleteWithPlace:(GMSPlace *)place {
//    [self dismissViewControllerAnimated:YES completion:nil];
//    // Do something with the selected place.
//    NSLog(@"Place name %@", place.name);
//    NSLog(@"Place address %@", place.formattedAddress);
//    NSLog(@"Place attributions %@", place.attributions.string);
//}
//
//- (void)viewController:(GMSAutocompleteViewController *)viewController
//didFailAutocompleteWithError:(NSError *)error {
//    [self dismissViewControllerAnimated:YES completion:nil];
//    // TODO: handle the error.
//    NSLog(@"Error: %@", [error description]);
//}
//
//// User canceled the operation.
//- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//// Turn the network activity indicator on and off again.
//- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//}
//
//- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0) {
        [maAddress removeAllObjects];
        [_tblAddress reloadData];

    }
    else
    {
        if ([CommonFunctions reachabiltyCheck]) {
       
    [self getPlacesToWebServices:searchText :^(NSMutableArray *finished) {
        [maAddress removeAllObjects];

        if(finished.count>0){
            NSLog(@"success %@",finished);
            [maAddress addObjectsFromArray:finished];

        }
        else
        {
            [maAddress removeAllObjects];
        }
        [_tblAddress reloadData];
     
    }];
        }
        else
        {
            [CommonFunctions alertTitle:nil withMessage:KAInternet];
        }
    }
   

    
    //    [self getPlacesToWebServices:searchText];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return maAddress.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:k_searchTableCellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:k_searchTableCellIdentifier];
    }
    cell.textLabel.text = maAddress[indexPath.row][@"formatted_address"];
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    return cell;
}
- (void) tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *responseDict  = maAddress[indexPath.row];
    if(responseDict[@"address_components"]!=nil)
    {
        NSMutableDictionary *dictAddress = [[NSMutableDictionary alloc]init];
        int i;
        NSString *strStreet = @"";
        for (i=0; i<[responseDict[@"address_components"] count]; i++)
        {
            if ([responseDict[@"address_components"][i][@"types"] count]>0) {
                
                 if ([responseDict[@"address_components"][i][@"types"][0] isEqualToString:@"locality"])
                {
                    
                    
                    [dictAddress setValue:responseDict[@"address_components"][i][@"long_name"] forKey:@"City"];

                    NSLog(@"city %@",responseDict[@"address_components"][i][@"long_name"]);
                }
                else if ([responseDict[@"address_components"][i][@"types"] containsObject:@"premise"])
                {
                    [dictAddress setValue:responseDict[@"address_components"][i][@"long_name"] forKey:@"Name"];
                }
                else if ([responseDict[@"address_components"][i][@"types"] containsObject:@"route"] )
                {
                    [dictAddress setValue:responseDict[@"address_components"][i][@"long_name"] forKey:@"Street"];

                }
                else if ( [responseDict[@"address_components"][i][@"types"] containsObject:@"sublocality_level_2"] || [responseDict[@"address_components"][i][@"types"] containsObject:@"sublocality_level_1"])
                {
                    NSLog(@"address %@",responseDict[@"address_components"][i][@"long_name"]);
                    if (strStreet.length == 0) {
                        strStreet = responseDict[@"address_components"][i][@"long_name"];
                    }else
                     strStreet=[strStreet stringByAppendingString:[@" " stringByAppendingString:responseDict[@"address_components"][i][@"long_name"]]];
                    
                    
                    if ([responseDict[@"address_components"][i][@"types"] containsObject:@"sublocality_level_2"])
                    {
                        [dictAddress setValue:responseDict[@"address_components"][i][@"long_name"] forKey:@"Thoroughfare"];
                        
                    }
                }
               
                
            }
            
        }
        [dictAddress setValue:strStreet forKey:@"SubLocality"];
        [dictAddress setObject:responseDict[@"geometry"][@"location"] != nil ? responseDict[@"geometry"][@"location"]: @"" forKey:@"latlong"];
        [self.delegate setAddressThroughApi:dictAddress];
        [self.navigationController popViewControllerAnimated:YES];

    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * footerView;
    footerView = [[UIView alloc]initWithFrame:CGRectZero];
    footerView.backgroundColor=[UIColor clearColor];
    UIImage*imgGoogle =[UIImage imageNamed:@"powered-by-google"];
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-(imgGoogle.size.width/2), 5, imgGoogle.size.width, imgGoogle.size.height)];
    [img setImage:imgGoogle];
    
    [footerView addSubview:img];
    return footerView;
}
#define FONT_SIZE 16.0f
#define CELL_CONTENT_MARGIN 28.0f

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *strData =maAddress[indexPath.row][@"formatted_address"];
    
    
    CGSize constraint = CGSizeMake(250 - 66, 20000.0f);
    CGSize size = [strData sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 66.0);
    return height;
}

@end
