//
//  AddHomePickUpAddress.h
//  Myles
//
//  Created by Itteam2 on 30/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LocationData;

@interface AddHomePickUpAddress : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong , nonatomic) LocationData *locationDataObj;
@property (weak, nonatomic) IBOutlet UITableView *tblAddAddress;
@property (weak, nonatomic) NSDictionary *dictEditData;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchCurrentLoc;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchOnMap;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property id delegate;
@end

@protocol reloadAddress

-(void)ReloadAddress;


@end
