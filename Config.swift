//
//  Config.swift
//  hkScopum
//
//  Created by Anderson Santos Gusmao on 15/09/17.
//  Copyright © 2017 Heuristisk. All rights reserved.
//

import Foundation

class Config {
    
   public static var serverBaseUrl: String  {
        get {
            
            if let path = Bundle.main.path(forResource: "MylesZero", ofType: "plist") {
                if let dic = NSDictionary(contentsOfFile: path) {
                    return dic["server_url"] as? String ?? String.Empty
                }
            }
            return String.Empty
        }
    }
}
