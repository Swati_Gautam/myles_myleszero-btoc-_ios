//
//  offerPage.swift
//  Myles
//
//  Created by Akanksha Singh on 21/02/18.
//  Copyright © 2018 Divya Rai. All rights reserved.
//

import UIKit
import WebKit

var myContext = 0


class offerPage: UIViewController {

     var webView: WKWebView!
    var package: NSDictionary!
    var progressView: UIProgressView!
  //  var backButtonStyle: BackButtonStyle = .done
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
        progressView = UIProgressView(progressViewStyle: .default)
        progressView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        progressView.tintColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        navigationController?.navigationBar.addSubview(progressView)
        let navigationBarBounds = self.navigationController?.navigationBar.bounds
        progressView.frame = CGRect(x: 0, y: navigationBarBounds!.size.height - 2, width: navigationBarBounds!.size.width, height: 2)
    }
    deinit {
        //remove all observers
//        webView.removeObserver(self, forKeyPath: "title")
//        webView.removeObserver(self, forKeyPath: "estimatedProgress")
//        //remove progress bar from navigation bar
//        progressView.removeFromSuperview()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       
        webView.load(myRequest())
        webView.allowsBackForwardNavigationGestures = true
        // // add observer for key path
        webView.addObserver(self, forKeyPath: "title", options: .new, context: &myContext)
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: &myContext)
        // Do any additional setup after loading the view.
    }
    func doneTapped() {
        //Routing is your class handle view routing in your app
      //  Routing.showAnotherVC(fromVC: self)
    }
    
    //observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let change = change else { return }
        if context != &myContext {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == "title" {
            if let title = change[NSKeyValueChangeKey.newKey] as? String {
                self.navigationItem.title = title
            }
            return
        }
        if keyPath == "estimatedProgress" {
            if let progress = (change[NSKeyValueChangeKey.newKey] as AnyObject).floatValue {
                progressView.progress = progress;
            }
            return
        }
    }
    
    //compute your url request
    func myRequest() -> URLRequest {
        return URLRequest(url: (URL(string: "https://google.com")!))
    }
    
    func addDoneButton() {
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        navigationItem.rightBarButtonItems = [done]
    }
    
    func hideBackButton() {
        navigationItem.setHidesBackButton(true, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension offerPage: WKNavigationDelegate {
    
    //WKNavigationDelegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if let url = navigationAction.request.url {
            if url.absoluteString.contains("/something") {
                // if url contains something; take user to native view controller
            //    Routing.showAnotherVC(fromVC: self)
                decisionHandler(.cancel)
            } else if url.absoluteString.contains("done") {
                //in case you want to stop user going back
                hideBackButton()
                addDoneButton()
                decisionHandler(.allow)
            } else if url.absoluteString.contains("AuthError") {
                //in case of erros, show native allerts
            }
            else{
                decisionHandler(.allow)
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        progressView.isHidden = false
}
}
