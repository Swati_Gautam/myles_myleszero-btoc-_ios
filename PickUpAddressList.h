//
//  PickUpAddressList.h
//  Myles
//
//  Created by Itteam2 on 30/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickUpAddressList : BaseViewController
@property (weak , nonatomic) IBOutlet UITableView *tblPickUpAddress;
@property (weak , nonatomic) NSMutableArray *maPickAddress;
// @property BOOL bReload;
@end
