//
//  SelectCity.swift
//  Myles
//
//  Created by Itteam2 on 23/12/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import Foundation
import SVProgressHUD
@objc protocol CityProtocol {
    func selectedCity(cityM:cityModel? , updatedCityList:[cityModel])
}

/*struct cityListing {
    static var maCity = [cityModel]()
}*/

@objc class selectCity :UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate
{
    @IBOutlet weak var tblCity : UITableView?
    @IBOutlet weak var scrCity : UISearchBar?
    @objc weak var delegate : CityProtocol?
    @objc var isSearching = Bool()
    @objc var maCity = [cityModel]()
    @objc var maSearchCity = [cityModel]()
    @objc var array:[[String:String]] = []
    @objc var cityModelArray = [cityModel]()
    var loadMore:Bool!
    @objc var isFirstTime = true
    @objc var previousDate = Date()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblCity?.delegate = self
        tblCity?.dataSource = self
        tblCity?.reloadData()
         //self.calculateTime()
    }
    
    // MARK: UITableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return isSearching ? maSearchCity.count :maCity.count //Swati Commented This (Code For More Cities")
        //return isSearching ? maSearchCity.count :maCity.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CityList")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "CityList")
            }
            
        if(isSearching){
            
                cell?.textLabel?.text = maSearchCity[indexPath.row].cityname            
        }
        else{
            
            cell?.textLabel?.text = maCity[indexPath.row ].cityname
            cell?.textLabel?.textColor = UIColor.black //Swati Added This Line
            
                /*if(indexPath.row > 12){
                    cell?.textLabel?.text = maCity[indexPath.row - 1].cityname

                }
                else if(indexPath.row < 12){
                    cell?.textLabel?.text = maCity[indexPath.row ].cityname
                    
                }
            
                else{
                    cell?.textLabel?.text = "More Cities"

                }*/  //Swati Commented This (Code For More Cities)
        }
            /*if cell?.textLabel?.text == "More Cities"{
                cell?.textLabel?.textColor = UIColor.red
            }
            else{
                 cell?.textLabel?.textColor = UIColor.black
            }*/  //Swati Commented This (Code For More Cities)
            cell?.selectionStyle = .none
          return cell!
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isSearching){

            delegate?.selectedCity(cityM: isSearching ? maSearchCity[indexPath.row]:maCity[indexPath.row], updatedCityList:maCity)
                cancelBtnClicked(self)
        }
        else{
            delegate?.selectedCity(cityM: isSearching ? maSearchCity[indexPath.row]:maCity[indexPath.row], updatedCityList: maCity)
            cancelBtnClicked(self)
            
                /*if(indexPath.row > 12){
                    delegate?.selectedCity(cityM: isSearching ? maSearchCity[indexPath.row]:maCity[indexPath.row - 1], updatedCityList: maCity)
                    cancelBtnClicked(self)
                }
                else if(indexPath.row < 12){
                    delegate?.selectedCity(cityM: isSearching ? maSearchCity[indexPath.row]:maCity[indexPath.row], updatedCityList: maCity)
                    cancelBtnClicked(self)
                }
                else{
                    if(maCity.count < 13){
                        self.callApiForCityList()
                    }
                }*/ //Swati Commented This (Code For More Cities)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrCity?.resignFirstResponder()
    }
    
    // MARK: UISearchBar Delegate
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        scrCity?.resignFirstResponder()
        tblCity?.reloadData()
    }
    
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        isSearching = true
//
//    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            isSearching = false
            tblCity?.reloadData()
        }
        else
        {
            isSearching = true
            // filter city
            // var tempArray = ["abhi"]
              maSearchCity.removeAll()
              maSearchCity =  maCity.filter({ (filterCity) -> Bool in

                 (filterCity.cityname.lowercased().range(of: searchText.lowercased()) != nil)

                })
            
            tblCity!.reloadData()
        }
    }
    
    // MARK: Cancel
    @IBAction func cancelBtnClicked(_ sender: AnyObject) {
        delegate?.selectedCity(cityM: nil, updatedCityList: maCity)
        scrCity?.resignFirstResponder()
        self.dismiss(animated: true) {
        }
    }
    

    func callApiForCityList(){
        if CommonFunctions.reachabiltyCheck(){
            DispatchQueue.main.async {
                self.showLoader()
            }
            
            let callApi = ApiClass()
            callApi.cityListApi(completionHandler: { (arrCityList, isValidToken, responseString) in
                print(responseString)
                if responseString == "Success"{
                    
                    if isValidToken{
                        self.array.removeAll()
                        for  dic in arrCityList!{
                            
                            if self.checkCityId(dic: dic["CityId"] as! String){
                                DispatchQueue.main.async {
                                    self.isSearching = false
                                    self.scrCity?.text = ""
                                }
                                self.maCity.append(cityModel.init(cityN: dic["CityName"] as! String, cityI: dic["CityId"] as! String))
                                self.array.append(["CityName": dic["CityName"] as! String, "CityId": dic["CityId"] as! String])
                            }
                        }
                        self.previousDate = Date()
                       if(self.array.count > 0){
                        UserDefaults.standard.set(self.previousDate, forKey: "previousDate")
                        UserDefaults.standard.set(self.array, forKey: "CityListing")
                    }
                            
                    UserDefaults.standard.synchronize()
                }

                    SVProgressHUD.dismiss()
                    DispatchQueue.main.async {
                        self.tblCity!.reloadData()
                    }
                }
                else{
                    SVProgressHUD.dismiss()
                    DispatchQueue.main.async {
                        
                        self.tblCity!.reloadData()
                    }
                }
                
            })
        }
    }
    
    func checkCityId(dic :String) -> Bool
    {
        if(self.maCity.contains(where: { $0.cityId == dic }))
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    func showLoader()
    {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.5))
        SVProgressHUD.setFont(UIFont.init(name: "Motor Oil 1937 M54", size: 12)!)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.show(withStatus: "Please wait")
    }
}


@objc class cityModel : NSObject {
    
    @objc var cityname = String()
    @objc var cityId = String()
    
    @objc init(cityN:String,cityI:String)
     {
        cityname = cityN
        cityId = cityI
     }
}

