//
//  FAQModel.swift
//  Myles
//
//  Created by Itteam2 on 07/11/16.
//  Copyright © 2016 Divya Rai. All rights reserved.
//

import Foundation


class FAQModel {
    
    var dictFaqSection = [String:AnyObject]()
    
    init(responce : Dictionary<String,AnyObject>)
    {
        
        guard responce.count > 0 else
        {
            return
        }
         dictFaqSection = responce 

    }
    
}
class FAQSubModel {
    
    var FaqSubSection = [Dictionary<String,AnyObject>]()
    
     init(responce : Array<Dictionary<String,AnyObject>>)
    {
        FaqSubSection = responce
    }
    
    
    
}
class FAQSubModelQuestion {
    
    var FaqSubSectionQes = ""
    var FaqSubSectionAns = ""
    var isOpen = Bool()
    var Cellheight = 0.0
     init ()
     {
        
     }
     init(responce : Dictionary<String,String>)
    {
        FaqSubSectionQes = responce["que"]!
        FaqSubSectionAns = responce["ans"]!
        
 
    }
    
    
    
}
